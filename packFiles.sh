#!/bin/bash

targetDir=${1:-build}
mkdir -p $targetDir

function handleGmlFile() {
	local gmlFile=$1

	local pathWithoutExt="${gmlFile%.*}"
	local basename=$(basename $pathWithoutExt)
	
	local tmpDir=$(mktemp -d)
	cp $pathWithoutExt* $tmpDir
	mv $tmpDir/*.gml $tmpDir/xplan.gml

	local zipFile=$targetDir/$basename.zip
	echo "Packing $zipFile..."
	zip -q -j $zipFile $tmpDir/*
	rm -rf $tmpDir
}


while IFS= read -r filename
do
	handleGmlFile $filename
done < <(find . -name '*.gml')


