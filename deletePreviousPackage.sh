#!/bin/bash

token=$1
PROJECT_ID=2152
packageId=`curl "https://gitlab.opencode.de/api/v4/projects/$PROJECT_ID/packages" | jq '.[0].id'`

if [ -n "$packageId" ] && [ "$packageId" -eq "$packageId" ] 2>/dev/null; then
	echo "deleting package $packageId"
	curl --request DELETE --header "PRIVATE-TOKEN: $token" "https://gitlab.opencode.de/api/v4/projects/$PROJECT_ID/packages/$packageId"
else
	echo "no existing package found (packageId=$packageId)" 
fi
 
