﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<!--Erstellt von WS LANDCAD am 27.05.2021-->
<xplan:XPlanAuszug xmlns:xplan="http://www.xplanung.de/xplangml/5/2" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" gml:id="GML_ffd3cf9a-e4a8-42f2-a185-1ce9e46dc59a">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>564984.593 5934515.633</gml:lowerCorner>
      <gml:upperCorner>565201.955 5934696.508</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_b7e8fcfa-cecb-418c-904a-bb5e135d8bcd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564984.593 5934515.633</gml:lowerCorner>
          <gml:upperCorner>565201.955 5934696.508</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan002_5-2</xplan:name>
      <xplan:beschreibung> Der Bebauungsplan BPlan002_5-2 für den Geltungsbereich zwischen der Laeiszhalle 
und den Straßen Dammtorwall, Caffamacherreihe und Valentinskamp (Bezirk 
Hamburg-Mitte, Ortsteil 108) wird festgestellt.
Das Gebiet wird wie folgt begrenzt:
Dammtorwall – Caffamacherreihe – Valentinskamp, über das Flurstück 1495 
(Dragonerstall) der Gemarkung Neustadt Nord.</xplan:beschreibung>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface srsName="EPSG:25832" gml:id="GML_6107a7ef-3dfb-4667-b957-9c6229500573">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25832" gml:id="GML_15a2d9ba-e6c5-4e3a-9cc1-1e6527bea3e8">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsDimension="2" count="26">565201.138 5934696.508 565181.673 5934687.992 565162.935 5934678.746 565150.525 5934672.179 565134.949 5934663.417 565122.782 5934656.845 565110.615 5934648.818 565100.396 5934642.246 565088.592 5934632.149 565081.048 5934625.823 565066.448 5934613.168 565052.337 5934599.057 565036.391 5934581.511 564984.593 5934524.512 564985.062 5934515.633 565009.216 5934517.109 565042.886 5934519.162 565042.773 5934527.073 565044.863 5934527.174 565198.796 5934534.239 565201.955 5934534.333 565201.678 5934550.926 565200.200 5934638.503 565201.137 5934638.501 565201.138 5934689.173 565201.138 5934696.508 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_90d196c6-7704-4ecc-a6d7-394f1c0478c4" />
      <xplan:texte xlink:href="#GML_c180d211-33e9-4580-8e8f-f58eadca8f8a" />
      <xplan:texte xlink:href="#GML_0ad7d318-e915-42be-8739-1ce485872219" />
      <xplan:texte xlink:href="#GML_203cbf95-87be-466f-b869-335011c68542" />
      <xplan:texte xlink:href="#GML_5fab2299-1c39-4f77-8ab8-c12805fd3b4e" />
      <xplan:texte xlink:href="#GML_b9b06b68-91d2-4b39-9bf8-bd42bf8490fb" />
      <xplan:texte xlink:href="#GML_94338043-1aad-4bae-8c47-5ae720ca393e" />
      <xplan:texte xlink:href="#GML_52dee64b-a907-4b1b-892d-61fff34962fc" />
      <xplan:texte xlink:href="#GML_073c07fd-6041-46ee-9271-69585acaf7a7" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>108</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2021-03-08</xplan:rechtsverordnungsDatum>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:versionBauNVOText>Version vom 21.11.2017</xplan:versionBauNVOText>
      <xplan:bereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_70a45912-2871-4085-acf9-d3351cb00b20">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564984.593 5934518.449</gml:lowerCorner>
          <gml:upperCorner>565182.632 5934666.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:rasterBasis xlink:href="#GML_4ac0a688-e026-46da-8ce0-61e54b0f3e8d" />
      <xplan:planinhalt xlink:href="#GML_d096515a-05d4-4279-bb15-feb7c9d2e4b9" />
      <xplan:planinhalt xlink:href="#GML_ca159f3b-db8a-4696-a302-7c0e2fc04cdd" />
      <xplan:planinhalt xlink:href="#GML_a1b82d18-a41b-4881-bd89-89c1d74ab19d" />
      <xplan:planinhalt xlink:href="#GML_f77fee26-ad00-4388-9992-44ec1b78ef98" />
      <xplan:planinhalt xlink:href="#GML_aabbde2d-6fc9-4a96-826d-94268e779c8c" />
      <xplan:planinhalt xlink:href="#GML_f30f55de-adc6-4ddf-b0bb-208af2faec51" />
      <xplan:planinhalt xlink:href="#GML_ebb838c5-f952-42c3-b7a6-f7bd7d7b8384" />
      <xplan:planinhalt xlink:href="#GML_97a0450d-c3e6-4c4c-a9b9-2c87db917f64" />
      <xplan:planinhalt xlink:href="#GML_12af2fdb-69e1-4511-ad33-e78ca923e7df" />
      <xplan:planinhalt xlink:href="#GML_74695132-425d-4934-baea-be62efed31dc" />
      <xplan:planinhalt xlink:href="#GML_6437f13a-9da9-488d-84b5-871c9afec4b8" />
      <xplan:planinhalt xlink:href="#GML_20aa47c4-f8ae-4ef8-8b5b-caefaaebac66" />
      <xplan:planinhalt xlink:href="#GML_d50519c3-ed1b-4ed1-945d-eeb95b49fa97" />
      <xplan:planinhalt xlink:href="#GML_10181d95-090e-44b8-891c-b107dfda3117" />
      <xplan:planinhalt xlink:href="#GML_80335b66-8fb7-46a3-947f-8bddad49ee79" />
      <xplan:planinhalt xlink:href="#GML_225d02a7-5968-4b9f-b767-828a7683e216" />
      <xplan:planinhalt xlink:href="#GML_23e509ba-2f62-4294-834a-3bf97cef25ed" />
      <xplan:planinhalt xlink:href="#GML_528e7bdc-54b9-4b92-afb4-c564854493a3" />
      <xplan:planinhalt xlink:href="#GML_d8192c7f-a252-4050-b474-47694b5008e6" />
      <xplan:planinhalt xlink:href="#GML_00641b3c-05b0-42b1-811b-d7e2368c2d75" />
      <xplan:planinhalt xlink:href="#GML_1a5d5f69-6030-418e-8c50-9beb655cd816" />
      <xplan:planinhalt xlink:href="#GML_c804c892-8e65-4be7-8416-f4e3db65a417" />
      <xplan:planinhalt xlink:href="#GML_f5255a2e-51f5-472b-82a7-c834187ddf9a" />
      <xplan:planinhalt xlink:href="#GML_56261e3a-d8f3-4fa1-940f-b7e614f35aef" />
      <xplan:planinhalt xlink:href="#GML_6684677c-066b-485a-8c89-7db721819c87" />
      <xplan:planinhalt xlink:href="#GML_bf1e5a22-839d-4ef5-aaf3-3b4d97d2e963" />
      <xplan:planinhalt xlink:href="#GML_c57c2d59-3e07-455f-af8a-70e50607f2f1" />
      <xplan:planinhalt xlink:href="#GML_65006077-7996-40d6-bb62-0bd5b81334de" />
      <xplan:planinhalt xlink:href="#GML_cc3f7613-8da2-45ab-8e7c-c6d74c15de60" />
      <xplan:planinhalt xlink:href="#GML_d40b6338-c05d-48bc-b074-4f865f28bf13" />
      <xplan:planinhalt xlink:href="#GML_2c65a70f-1649-4012-b70d-4b61ed3157be" />
      <xplan:planinhalt xlink:href="#GML_e3587eee-0394-4062-b606-3a197d395a39" />
      <xplan:planinhalt xlink:href="#GML_09ca07dc-e432-4d01-a9c1-16816f563962" />
      <xplan:planinhalt xlink:href="#GML_db2debbc-9beb-47a9-b35e-8c6ffb087a9a" />
      <xplan:planinhalt xlink:href="#GML_0ca33bb0-185b-4d14-9359-41d6e9838701" />
      <xplan:planinhalt xlink:href="#GML_5f5fc8e2-f0d6-4132-9774-5dd43f52014a" />
      <xplan:planinhalt xlink:href="#GML_218233aa-c960-4155-a0eb-ea5eee23b45f" />
      <xplan:planinhalt xlink:href="#GML_03835fde-b640-43d4-9ca1-661f64c8458f" />
      <xplan:planinhalt xlink:href="#GML_7fbf04dc-cda8-4acd-831a-31d6327e2e27" />
      <xplan:planinhalt xlink:href="#GML_ad476a3d-d0c4-464d-87b3-da973ee4b9f0" />
      <xplan:planinhalt xlink:href="#GML_d1a4521b-59c9-4321-acd4-3f227535c90d" />
      <xplan:planinhalt xlink:href="#GML_33328a77-91a6-4864-b8a2-c52eab49bcac" />
      <xplan:planinhalt xlink:href="#GML_1b73e286-945e-4f67-b58b-0763bc4c1053" />
      <xplan:planinhalt xlink:href="#GML_10cdd180-6bfc-4872-8199-5869396827ea" />
      <xplan:planinhalt xlink:href="#GML_25f5c07b-1409-439e-8cea-2b89439f782e" />
      <xplan:planinhalt xlink:href="#GML_523a00ad-6533-4e60-b38a-06be2c3623d8" />
      <xplan:praesentationsobjekt xlink:href="#GML_56418f5b-3769-4c8b-8951-9ed4eba0c60b" />
      <xplan:praesentationsobjekt xlink:href="#GML_b37a15fb-1e04-4e1a-b065-e8e59009cce9" />
      <xplan:praesentationsobjekt xlink:href="#GML_e56bff69-5ddb-489a-af95-5c5033272577" />
      <xplan:praesentationsobjekt xlink:href="#GML_dd06a4de-f527-4d88-a87d-bb807bc51497" />
      <xplan:praesentationsobjekt xlink:href="#GML_198bf623-4f2f-4aaf-a21c-33a1b089d4e3" />
      <xplan:praesentationsobjekt xlink:href="#GML_3d97cdff-abfb-4d58-ae33-3c337440e946" />
      <xplan:praesentationsobjekt xlink:href="#GML_5a7d638b-c969-48f6-b532-27283883b07b" />
      <xplan:praesentationsobjekt xlink:href="#GML_809762ae-acf3-4ba0-9760-b988ac532891" />
      <xplan:praesentationsobjekt xlink:href="#GML_1ca60951-ec86-4875-b260-fbbe77da4968" />
      <xplan:praesentationsobjekt xlink:href="#GML_25c63b93-2cfa-4fa3-906d-ca0cc4c5799a" />
      <xplan:praesentationsobjekt xlink:href="#GML_0d135aa7-ebd4-487a-bfed-6c3b94473835" />
      <xplan:praesentationsobjekt xlink:href="#GML_9db49e76-0ff9-4057-89e8-9e677146a085" />
      <xplan:praesentationsobjekt xlink:href="#GML_c245b09b-2893-4014-bf33-b8ce363b1479" />
      <xplan:praesentationsobjekt xlink:href="#GML_a97811ae-add4-443d-9730-22929ea94313" />
      <xplan:praesentationsobjekt xlink:href="#GML_05108ce5-8c8d-4453-a291-6cbe7c7387f4" />
      <xplan:praesentationsobjekt xlink:href="#GML_60127909-667e-411d-b2f5-7176dd1ba297" />
      <xplan:praesentationsobjekt xlink:href="#GML_0284b4a3-f97a-4479-a85a-b09c10bd1b73" />
      <xplan:praesentationsobjekt xlink:href="#GML_c48df557-2637-47be-87a1-57e83f845993" />
      <xplan:praesentationsobjekt xlink:href="#GML_9ad62bbb-e92c-40a0-b728-552b351cfc3e" />
      <xplan:praesentationsobjekt xlink:href="#GML_c63d5ebe-b8d1-47ef-95e3-2cdb99dafe62" />
      <xplan:praesentationsobjekt xlink:href="#GML_24875732-da3e-4e57-9f25-89d74cd41468" />
      <xplan:praesentationsobjekt xlink:href="#GML_3ba582d4-6de2-420b-bb0a-fd8ea26d551f" />
      <xplan:praesentationsobjekt xlink:href="#GML_b68d8a04-fcb4-4fe0-ba1d-7c622be61da9" />
      <xplan:praesentationsobjekt xlink:href="#GML_0227c06e-c9c3-4d61-88cd-f57845c78844" />
      <xplan:praesentationsobjekt xlink:href="#GML_7783fc04-bf01-4b3c-b4bb-3058ca6071e5" />
      <xplan:praesentationsobjekt xlink:href="#GML_bb877354-91fc-4ee7-8255-d91292e4e374" />
      <xplan:praesentationsobjekt xlink:href="#GML_87e60ce7-8f61-4bc7-ad2b-72062166e775" />
      <xplan:gehoertZuPlan xlink:href="#GML_b7e8fcfa-cecb-418c-904a-bb5e135d8bcd" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_d096515a-05d4-4279-bb15-feb7c9d2e4b9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565146.479 5934640.094</gml:lowerCorner>
          <gml:upperCorner>565182.632 5934666.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_1363f3fc-f68a-42e5-acd4-461d17a9b601">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="6">565146.479 5934652.557 565169.702 5934666.699 565182.133 5934659.567 565182.632 5934640.396 565150.142 5934640.094 565146.479 5934652.557 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ca159f3b-db8a-4696-a302-7c0e2fc04cdd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565122.961 5934559.556</gml:lowerCorner>
          <gml:upperCorner>565188.396 5934626.078</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_8aaa4172-76ea-469e-9f75-e69386a10e3f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="10">565136.370 5934626.078 565122.961 5934617.865 565145.834 5934580.501 565138.524 5934567.051 565152.363 5934559.556 565173.263 5934598.038 565188.396 5934598.428 565187.991 5934614.164 565144.374 5934613.026 565136.370 5934626.078 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="GML_a1b82d18-a41b-4881-bd89-89c1d74ab19d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565112.660 5934547.571</gml:lowerCorner>
          <gml:upperCorner>565189.454 5934673.060</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Dammtorwall 15, vor Nr. 15, Valentinskamp 70, Unilever-Haus, Ensemble Dammtorwall 15 (Unilever-Haus), Gebäude mit Außenanlagen und Flügelstern-Plastik</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2fceeda3-f9e6-4e72-a84e-4a7e96cbb8fd">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_0e90cebe-dae5-4cb0-9d30-307db766d4fa">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">565182.655 5934550.058 565189.454 5934557.359 565186.667 5934665.599 565173.663 5934673.060 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">565173.663 5934673.060 565160.101 5934665.258 565146.693 5934657.194 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="8">565146.693 5934657.194 565150.506 5934646.972 565148.472 5934646.921 565112.660 5934624.983 565139.991 5934580.364 565127.690 5934557.732 565128.154 5934547.571 565182.655 5934550.058 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
      <xplan:weltkulturerbe>false</xplan:weltkulturerbe>
      <xplan:nummer>	29977</xplan:nummer>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_f77fee26-ad00-4388-9992-44ec1b78ef98">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565182.204 5934562.410</gml:lowerCorner>
          <gml:upperCorner>565188.889 5934579.296</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_b29cf0b4-f134-4dfa-9697-ea0b099afff5">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565182.337 5934562.410 565182.204 5934567.575 565188.889 5934579.296 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_aabbde2d-6fc9-4a96-826d-94268e779c8c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565042.904 5934548.647</gml:lowerCorner>
          <gml:upperCorner>565119.138 5934623.909</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_192c6f14-fcb2-480a-b206-1c9ac8d71abd">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="20">565099.779 5934589.684 565096.454 5934587.161 565091.525 5934583.397 565086.596 5934579.634 565090.934 5934572.758 565095.725 5934575.761 565101.428 5934579.335 565110.283 5934584.888 565119.138 5934590.440 565108.893 5934606.761 565098.128 5934623.909 565089.853 5934616.124 565081.579 5934608.339 565073.304 5934600.553 565065.029 5934592.768 565055.910 5934584.169 565046.792 5934575.569 565042.904 5934571.727 565043.727 5934557.927 565044.153 5934548.647 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_f30f55de-adc6-4ddf-b0bb-208af2faec51">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565182.241 5934535.476</gml:lowerCorner>
          <gml:upperCorner>565189.980 5934688.241</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_039532a6-bd7c-4c11-8050-5e0bef523285">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="8">565188.985 5934535.476 565189.277 5934548.754 565189.980 5934559.934 565189.896 5934564.909 565188.807 5934599.633 565187.029 5934665.011 565184.798 5934676.723 565182.241 5934688.241 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_ebb838c5-f952-42c3-b7a6-f7bd7d7b8384">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565095.540 5934626.495</gml:lowerCorner>
          <gml:upperCorner>565096.540 5934627.495</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">15.30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b4795d91-011f-48c6-94e8-ca2407015df2">
          <gml:pos>565095.540 5934626.495</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_97a0450d-c3e6-4c4c-a9b9-2c87db917f64">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565048.033 5934563.320</gml:lowerCorner>
          <gml:upperCorner>565111.833 5934615.320</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_7308830a-2e52-4261-a898-1d1482718dcb">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="11">565104.433 5934603.520 565097.033 5934615.320 565072.533 5934592.270 565048.033 5934569.220 565057.233 5934563.320 565074.833 5934583.620 565092.433 5934603.920 565099.633 5934596.320 565106.833 5934588.720 565111.833 5934591.720 565104.433 5934603.520 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_12af2fdb-69e1-4511-ad33-e78ca923e7df">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565146.479 5934640.094</gml:lowerCorner>
          <gml:upperCorner>565182.632 5934666.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_809762ae-acf3-4ba0-9760-b988ac532891" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_94338043-1aad-4bae-8c47-5ae720ca393e" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7dd88edc-7b08-431c-9bf8-85c2d42aa43d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">565182.133 5934659.567 565169.702 5934666.699 565146.479 5934652.557 565150.142 5934640.094 565182.632 5934640.396 565182.133 5934659.567 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_74695132-425d-4934-baea-be62efed31dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565064.743 5934544.677</gml:lowerCorner>
          <gml:upperCorner>565128.134 5934562.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_60127909-667e-411d-b2f5-7176dd1ba297" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_5fab2299-1c39-4f77-8ab8-c12805fd3b4e" />
      <xplan:refTextInhalt xlink:href="#GML_52dee64b-a907-4b1b-892d-61fff34962fc" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_9503ac54-e251-4466-9929-f0495d9fcb19">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="20">565067.573 5934544.807 565070.403 5934544.937 565073.233 5934545.067 565076.063 5934545.197 565084.150 5934545.565 565095.146 5934546.066 565106.142 5934546.567 565117.138 5934547.068 565128.134 5934547.569 565128.000 5934550.830 565127.684 5934557.720 565127.530 5934561.205 565106.642 5934561.553 565085.754 5934561.901 565077.641 5934562.036 565069.528 5934562.172 565069.224 5934561.059 565067.142 5934553.446 565064.743 5934544.677 565067.573 5934544.807 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_6437f13a-9da9-488d-84b5-871c9afec4b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565043.820 5934552.123</gml:lowerCorner>
          <gml:upperCorner>565062.089 5934563.706</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Durchgang</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">3</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5a7d638b-c969-48f6-b532-27283883b07b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_073c07fd-6041-46ee-9271-69585acaf7a7" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_8d134454-0964-4de2-ad27-124b19e9ca9d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">565043.820 5934555.902 565043.993 5934552.123 565045.076 5934552.627 565062.089 5934560.532 565060.614 5934563.706 565043.820 5934555.902 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_20aa47c4-f8ae-4ef8-8b5b-caefaaebac66">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565046.792 5934575.569</gml:lowerCorner>
          <gml:upperCorner>565098.117 5934623.926</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_e5248e55-98d0-421c-8232-cf7afbd3e150">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">565098.117 5934623.926 565084.193 5934612.142 565070.928 5934599.620 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565070.928 5934599.620 565046.792 5934575.569 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_56418f5b-3769-4c8b-8951-9ed4eba0c60b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565119.139 5934610.724</gml:lowerCorner>
          <gml:upperCorner>565119.139 5934610.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>3,5</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7e2abfc6-0469-46d2-9883-512193a2347a">
          <gml:pos>565119.139 5934610.724</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">32.20</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BereichOhneEinAusfahrtLinie gml:id="GML_d50519c3-ed1b-4ed1-945d-eeb95b49fa97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.326 5934542.548</gml:lowerCorner>
          <gml:upperCorner>565189.780 5934549.156</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Gehwegsüberfahrten nicht zugelassen</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_d00a537f-d4fb-4225-9f41-02960b8ce6f1">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565044.326 5934542.548 565189.780 5934549.156 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>3000</xplan:typ>
    </xplan:BP_BereichOhneEinAusfahrtLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_10181d95-090e-44b8-891c-b107dfda3117">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564984.593 5934515.633</gml:lowerCorner>
          <gml:upperCorner>565201.955 5934696.508</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ce7513c6-079a-4f15-975c-3b5088d9b140">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="26">565201.678 5934550.926 565200.200 5934638.503 565201.137 5934638.501 565201.138 5934689.173 565201.138 5934696.508 565181.673 5934687.992 565162.935 5934678.746 565150.525 5934672.179 565134.949 5934663.417 565122.782 5934656.845 565110.615 5934648.818 565100.396 5934642.246 565088.592 5934632.149 565081.048 5934625.823 565066.448 5934613.168 565052.337 5934599.057 565036.391 5934581.511 564984.593 5934524.512 564985.062 5934515.633 565009.216 5934517.109 565042.886 5934519.162 565042.773 5934527.073 565044.863 5934527.174 565198.796 5934534.239 565201.955 5934534.333 565201.678 5934550.926 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_2eda4bd5-501a-4062-b80d-dd8df9c43bf8">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="5">565044.128 5934548.669 565043.202 5934566.241 565042.904 5934571.727 565046.792 5934575.569 565070.928 5934599.620 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565070.928 5934599.620 565095.240 5934621.596 565121.578 5934641.100 565134.061 5934649.263 565146.693 5934657.194 565160.101 5934665.258 565173.663 5934673.060 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="9">565173.663 5934673.060 565186.667 5934665.599 565188.889 5934579.296 565182.204 5934567.575 565182.655 5934550.058 565128.154 5934547.571 565064.743 5934544.677 565049.308 5934543.973 565044.128 5934548.669 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_b37a15fb-1e04-4e1a-b065-e8e59009cce9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565130.305 5934553.439</gml:lowerCorner>
          <gml:upperCorner>565130.305 5934553.439</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>3,5</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_405f6db2-57e9-4ecd-b057-9c2eb5fe6359">
          <gml:pos>565130.305 5934553.439</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_e56bff69-5ddb-489a-af95-5c5033272577">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565191.879 5934572.227</gml:lowerCorner>
          <gml:upperCorner>565191.879 5934572.227</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>E</xplan:schriftinhalt>
      <xplan:skalierung>1.0</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_baf02713-d9d2-475a-8e2d-9c06c8014c7e">
          <gml:pos>565191.879 5934572.227</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_80335b66-8fb7-46a3-947f-8bddad49ee79">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.128 5934543.752</gml:lowerCorner>
          <gml:upperCorner>565049.308 5934548.669</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">43</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9ad62bbb-e92c-40a0-b728-552b351cfc3e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d329bfa7-a1ac-4c03-958c-db3021360945">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">565044.128 5934548.669 565044.377 5934543.752 565049.308 5934543.973 565044.128 5934548.669 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.800</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_225d02a7-5968-4b9f-b767-828a7683e216">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565043.227 5934543.973</gml:lowerCorner>
          <gml:upperCorner>565127.964 5934621.977</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">45.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7783fc04-bf01-4b3c-b4bb-3058ca6071e5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f51f5380-0182-4d46-9621-82458f890ee8">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="46">565093.816 5934599.207 565096.701 5934601.092 565102.846 5934591.688 565099.779 5934589.684 565096.454 5934587.161 565091.525 5934583.397 565089.207 5934581.627 565093.685 5934574.482 565095.725 5934575.761 565101.428 5934579.335 565110.283 5934584.888 565116.773 5934588.957 565096.074 5934621.977 565081.579 5934608.339 565065.029 5934592.768 565046.792 5934575.569 565046.414 5934575.195 565043.227 5934566.311 565043.727 5934557.927 565044.128 5934548.669 565049.308 5934543.973 565057.025 5934544.325 565064.743 5934544.677 565070.403 5934544.937 565076.063 5934545.197 565084.150 5934545.565 565106.142 5934546.567 565123.905 5934547.377 565124.048 5934551.368 565127.964 5934551.561 565127.857 5934554.086 565127.684 5934557.720 565127.657 5934558.322 565125.308 5934558.290 565125.267 5934561.243 565106.642 5934561.553 565085.754 5934561.901 565077.641 5934562.036 565069.528 5934562.172 565068.070 5934562.196 565061.826 5934562.297 565063.218 5934563.904 565073.347 5934575.596 565083.477 5934587.289 565088.647 5934593.248 565093.816 5934599.207 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.800</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_23e509ba-2f62-4294-834a-3bf97cef25ed">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565053.066 5934549.442</gml:lowerCorner>
          <gml:upperCorner>565068.240 5934557.577</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_24875732-da3e-4e57-9f25-89d74cd41468" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0ad7d318-e915-42be-8739-1ce485872219" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_9966f353-0376-46ba-8d8c-6641ff559821">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">565066.208 5934550.034 565067.142 5934553.446 565068.240 5934557.461 565061.337 5934557.577 565053.066 5934549.442 565066.208 5934550.034 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_528e7bdc-54b9-4b92-afb4-c564854493a3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565188.397 5934579.814</gml:lowerCorner>
          <gml:upperCorner>565188.876 5934598.427</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_46349d63-0649-4990-9027-e63ae304bcfb">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565188.876 5934579.814 565188.397 5934598.427 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d8192c7f-a252-4050-b474-47694b5008e6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565086.596 5934572.758</gml:lowerCorner>
          <gml:upperCorner>565093.685 5934581.627</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">48</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c48df557-2637-47be-87a1-57e83f845993" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_52f88bcb-abc2-4e00-a1eb-cdcdef791a86">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565089.207 5934581.627 565086.596 5934579.634 565090.934 5934572.758 565093.685 5934574.482 565089.207 5934581.627 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8000</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_00641b3c-05b0-42b1-811b-d7e2368c2d75">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565098.117 5934614.164</gml:lowerCorner>
          <gml:upperCorner>565187.991 5934673.060</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_131dc66e-128c-4ca6-9e1e-5ee808c6742a">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565187.991 5934614.164 565186.667 5934665.599 565173.663 5934673.060 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="7">565173.663 5934673.060 565160.101 5934665.258 565146.693 5934657.194 565134.061 5934649.263 565121.578 5934641.100 565109.662 5934632.767 565098.117 5934623.926 </gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_1a5d5f69-6030-418e-8c50-9beb655cd816">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565141.180 5934582.565</gml:lowerCorner>
          <gml:upperCorner>565167.346 5934605.732</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_246799be-3d30-4b86-9a36-e84cfb9b7f2f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">565155.344 5934582.565 565167.346 5934605.732 565141.180 5934604.701 565155.344 5934582.565 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_c804c892-8e65-4be7-8416-f4e3db65a417">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565133.659 5934544.572</gml:lowerCorner>
          <gml:upperCorner>565133.659 5934544.572</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">14.35</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_5641ad96-5b16-4ecf-bd4a-3eda3656f98f">
          <gml:pos>565133.659 5934544.572</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f5255a2e-51f5-472b-82a7-c834187ddf9a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565122.961 5934559.556</gml:lowerCorner>
          <gml:upperCorner>565188.396 5934626.078</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">105.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0d135aa7-ebd4-487a-bfed-6c3b94473835" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c245b09b-2893-4014-bf33-b8ce363b1479" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_c323569c-36b5-4780-b4f9-3bc8509e1ff2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">565136.370 5934626.078 565122.961 5934617.865 565145.834 5934580.501 565138.524 5934567.051 565152.363 5934559.556 565173.263 5934598.038 565188.396 5934598.427 565187.991 5934614.164 565144.374 5934613.026 565136.370 5934626.078 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">45.500</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_56261e3a-d8f3-4fa1-940f-b7e614f35aef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565064.743 5934544.677</gml:lowerCorner>
          <gml:upperCorner>565069.528 5934562.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_f0d14f66-465a-4d48-825e-ee1af424b845">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565064.743 5934544.677 565069.528 5934562.172 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_6684677c-066b-485a-8c89-7db721819c87">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565064.743 5934544.677</gml:lowerCorner>
          <gml:upperCorner>565069.528 5934562.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_d97c126b-d749-4967-9408-d49c6f7c49d0">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">565064.743 5934544.677 565069.528 5934562.172 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_bf1e5a22-839d-4ef5-aaf3-3b4d97d2e963">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.153 5934543.752</gml:lowerCorner>
          <gml:upperCorner>565049.308 5934548.647</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">3.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05108ce5-8c8d-4453-a291-6cbe7c7387f4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ac3ba46d-0581-48e2-9c63-e02993eaaea0">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">565044.153 5934548.647 565044.377 5934543.752 565049.308 5934543.973 565044.153 5934548.647 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_dd06a4de-f527-4d88-a87d-bb807bc51497">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565091.472 5934568.124</gml:lowerCorner>
          <gml:upperCorner>565091.472 5934568.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>3,5</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_69d06549-620a-46d8-b0a8-fa4d639623cb">
          <gml:pos>565091.472 5934568.124</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">270.00</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_c57c2d59-3e07-455f-af8a-70e50607f2f1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565060.614 5934547.688</gml:lowerCorner>
          <gml:upperCorner>565141.637 5934632.802</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_073c07fd-6041-46ee-9271-69585acaf7a7" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e65df2de-49d9-4c5c-8669-c095af0935f0">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="14">565141.637 5934580.679 565109.710 5934632.802 565106.885 5934630.712 565137.617 5934580.541 565130.836 5934567.561 565070.193 5934568.157 565060.614 5934563.706 565062.089 5934560.532 565070.950 5934564.649 565129.967 5934564.069 565130.715 5934547.688 565134.212 5934547.847 565133.430 5934564.967 565141.637 5934580.679 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_65006077-7996-40d6-bb62-0bd5b81334de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565109.827 5934568.520</gml:lowerCorner>
          <gml:upperCorner>565136.422 5934592.837</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_b9569869-fafd-4d1a-aa72-11b762a64b9b">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="7">565109.827 5934580.118 565113.890 5934574.371 565117.953 5934568.625 565128.633 5934568.520 565136.422 5934582.493 565130.085 5934592.837 565109.827 5934580.118 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_cc3f7613-8da2-45ab-8e7c-c6d74c15de60">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565123.905 5934547.377</gml:lowerCorner>
          <gml:upperCorner>565128.134 5934551.561</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">36.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a97811ae-add4-443d-9730-22929ea94313" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b2df7b4f-d77d-467f-941e-d8b843eaefcf">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565128.134 5934547.569 565127.964 5934551.561 565124.048 5934551.368 565123.905 5934547.377 565128.134 5934547.569 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.8000</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_d40b6338-c05d-48bc-b074-4f865f28bf13">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565066.208 5934550.034</gml:lowerCorner>
          <gml:upperCorner>565122.243 5934557.461</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0227c06e-c9c3-4d61-88cd-f57845c78844" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0ad7d318-e915-42be-8739-1ce485872219" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_399edc94-6dcb-4a54-a248-a563c9bd8647">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">565087.655 5934551.000 565122.243 5934552.558 565122.153 5934554.545 565122.064 5934556.531 565122.065 5934556.553 565091.715 5934557.065 565068.240 5934557.461 565068.183 5934557.253 565067.141 5934553.446 565066.208 5934550.034 565087.655 5934551.000 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_2c65a70f-1649-4012-b70d-4b61ed3157be">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565141.180 5934582.565</gml:lowerCorner>
          <gml:upperCorner>565167.346 5934605.732</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3ba582d4-6de2-420b-bb0a-fd8ea26d551f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_90d196c6-7704-4ecc-a6d7-394f1c0478c4" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2f829059-319a-437a-b7da-ce4b44895ae1">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">565155.344 5934582.565 565167.346 5934605.732 565141.180 5934604.701 565155.344 5934582.565 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e3587eee-0394-4062-b606-3a197d395a39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565125.267 5934558.290</gml:lowerCorner>
          <gml:upperCorner>565127.657 5934561.243</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">39.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c63d5ebe-b8d1-47ef-95e3-2cdb99dafe62" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_87e60ce7-8f61-4bc7-ad2b-72062166e775" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_45157b9a-81c4-497e-a510-a57c8be9869b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565127.530 5934561.205 565125.267 5934561.243 565125.308 5934558.290 565127.657 5934558.322 565127.530 5934561.205 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.800</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_09ca07dc-e432-4d01-a9c1-16816f563962">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565109.827 5934568.520</gml:lowerCorner>
          <gml:upperCorner>565136.422 5934592.837</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_25c63b93-2cfa-4fa3-906d-ca0cc4c5799a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_b9b06b68-91d2-4b39-9bf8-bd42bf8490fb" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d67cd633-c8bf-4b14-9821-4ac5f7b3c277">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">565109.827 5934580.118 565113.890 5934574.371 565117.953 5934568.625 565128.633 5934568.520 565136.422 5934582.493 565130.085 5934592.837 565109.827 5934580.118 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauLinie gml:id="GML_db2debbc-9beb-47a9-b35e-8c6ffb087a9a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.153 5934543.752</gml:lowerCorner>
          <gml:upperCorner>565049.308 5934548.647</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_2c527709-e5b0-465b-bb6f-28121dd4c12f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">565044.153 5934548.647 565044.377 5934543.752 565049.308 5934543.973 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0ca33bb0-185b-4d14-9359-41d6e9838701">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565049.308 5934543.973</gml:lowerCorner>
          <gml:upperCorner>565128.134 5934599.207</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_ebff095c-383d-4b41-b0a1-d80fc330e21e">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="26">565049.308 5934543.973 565057.025 5934544.325 565064.743 5934544.677 565070.403 5934544.937 565076.063 5934545.197 565084.150 5934545.565 565106.142 5934546.567 565128.134 5934547.569 565127.857 5934554.086 565127.684 5934557.720 565127.530 5934561.205 565106.642 5934561.553 565085.754 5934561.901 565077.641 5934562.036 565069.528 5934562.172 565068.070 5934562.196 565064.948 5934562.247 565061.826 5934562.297 565063.218 5934563.904 565068.283 5934569.750 565073.347 5934575.596 565078.412 5934581.443 565083.477 5934587.289 565088.647 5934593.248 565093.816 5934599.207 565099.779 5934589.684 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_5f5fc8e2-f0d6-4132-9774-5dd43f52014a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565128.134 5934547.570</gml:lowerCorner>
          <gml:upperCorner>565182.815 5934562.410</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_d4646fd0-062a-45ad-94c4-e789d6f895a5">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">565128.134 5934547.570 565128.154 5934547.571 565182.655 5934550.058 565182.815 5934550.065 565182.337 5934562.410 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_218233aa-c960-4155-a0eb-ea5eee23b45f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565053.066 5934549.442</gml:lowerCorner>
          <gml:upperCorner>565122.243 5934557.577</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_cab2eb60-9b0f-4b3d-ae7c-3d007b9ed418">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="15">565053.066 5934549.442 565061.337 5934557.577 565064.788 5934557.519 565068.240 5934557.461 565079.977 5934557.263 565091.715 5934557.065 565106.890 5934556.809 565122.065 5934556.553 565122.153 5934554.545 565122.243 5934552.558 565104.949 5934551.779 565087.655 5934551.000 565076.931 5934550.517 565059.637 5934549.738 565053.066 5934549.442 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_03835fde-b640-43d4-9ca1-661f64c8458f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565044.994 5934579.246</gml:lowerCorner>
          <gml:upperCorner>565045.994 5934580.246</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländeoberfläche bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">16.30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_17044e91-490c-4dbe-a9ae-6729f4533976">
          <gml:pos>565044.994 5934579.246</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7fbf04dc-cda8-4acd-831a-31d6327e2e27">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565093.816 5934589.684</gml:lowerCorner>
          <gml:upperCorner>565102.846 5934601.092</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">49.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3d97cdff-abfb-4d58-ae33-3c337440e946" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2a07e574-28a0-4c56-b456-b8a21b3166d8">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">565096.701 5934601.092 565093.816 5934599.207 565099.779 5934589.684 565102.846 5934591.688 565096.701 5934601.092 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.800</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="GML_ad476a3d-d0c4-464d-87b3-da973ee4b9f0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565171.294 5934621.476</gml:lowerCorner>
          <gml:upperCorner>565171.294 5934621.476</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Dammtorwall 15, vor Nr. 15, Valentinskamp 70, Unilever-Haus, Ensemble Dammtorwall 15 (Unilever-Haus), Gebäude mit Außenanlagen und Flügelstern-Plastik</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c3b887e0-65aa-451e-8a29-a61ce45065e1">
          <gml:pos>565171.294 5934621.476</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
      <xplan:nummer>29977</xplan:nummer>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_d1a4521b-59c9-4321-acd4-3f227535c90d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565042.904 5934543.973</gml:lowerCorner>
          <gml:upperCorner>565188.889 5934673.060</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9db49e76-0ff9-4057-89e8-9e677146a085" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0284b4a3-f97a-4479-a85a-b09c10bd1b73" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c180d211-33e9-4580-8e8f-f58eadca8f8a" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_bab6dc9c-47f1-43b6-8e3d-e3e15414285b">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_cf1edaf0-315f-4c14-8f48-901fa01c7460">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">565046.792 5934575.569 565042.904 5934571.727 565043.202 5934566.241 565044.128 5934548.669 565049.308 5934543.973 565064.743 5934544.677 565128.154 5934547.571 565182.655 5934550.058 565182.204 5934567.575 565188.889 5934579.296 565186.667 5934665.599 565173.663 5934673.060 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">565173.663 5934673.060 565160.101 5934665.258 565146.693 5934657.194 565134.061 5934649.263 565121.578 5934641.100 565095.240 5934621.596 565070.928 5934599.620 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">565070.928 5934599.620 565046.792 5934575.569 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1.0</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1600</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_198bf623-4f2f-4aaf-a21c-33a1b089d4e3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565056.811 5934538.186</gml:lowerCorner>
          <gml:upperCorner>565057.811 5934539.186</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:schriftinhalt>Gehwegüberfahrten nicht zugelassen</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7820e98e-d198-4005-8722-4363b96fd3a7">
          <gml:pos>565056.811 5934538.186</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">2.89</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_33328a77-91a6-4864-b8a2-c52eab49bcac">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565048.033 5934563.320</gml:lowerCorner>
          <gml:upperCorner>565111.833 5934615.320</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_bb877354-91fc-4ee7-8255-d91292e4e374" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0ad7d318-e915-42be-8739-1ce485872219" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_6cb0ec33-571a-4f33-905f-773a3d416246">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">565104.433 5934603.520 565097.033 5934615.320 565072.533 5934592.270 565048.033 5934569.220 565057.233 5934563.320 565074.833 5934583.620 565092.433 5934603.920 565099.633 5934596.320 565106.833 5934588.720 565111.833 5934591.720 565104.433 5934603.520 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_1b73e286-945e-4f67-b58b-0763bc4c1053">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565056.978 5934530.365</gml:lowerCorner>
          <gml:upperCorner>565201.299 5934564.753</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_541018a3-917f-4356-afa7-5a9e3bed5a89">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="20">565182.797 5934564.705 565182.819 5934546.680 565169.258 5934546.020 565169.496 5934541.121 565167.011 5934540.908 565166.691 5934541.469 565153.842 5934540.190 565129.213 5934537.692 565104.916 5934535.228 565088.957 5934533.609 565056.978 5934530.365 565080.130 5934530.915 565117.017 5934531.791 565141.330 5934533.036 565201.299 5934536.107 565201.111 5934547.423 565197.292 5934547.242 565187.920 5934546.799 565187.273 5934564.753 565182.797 5934564.705 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_10cdd180-6bfc-4872-8199-5869396827ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565042.904 5934566.311</gml:lowerCorner>
          <gml:upperCorner>565046.414 5934575.195</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">44</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b68d8a04-fcb4-4fe0-ba1d-7c622be61da9" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_3f4520ef-1c51-4ca9-abe4-6561a9450042">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">565042.904 5934571.727 565043.227 5934566.311 565046.414 5934575.195 565042.904 5934571.727 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.800</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_25f5c07b-1409-439e-8cea-2b89439f782e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>564984.593 5934518.449</gml:lowerCorner>
          <gml:upperCorner>565042.876 5934528.943</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b688ba11-ebfd-4c89-abac-98b88b422171">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="28">564984.913 5934518.449 565013.555 5934519.141 565042.876 5934519.886 565042.773 5934527.073 565042.659 5934528.913 565041.998 5934528.846 565040.528 5934528.720 565036.622 5934528.420 565034.260 5934528.264 565031.694 5934528.116 565029.575 5934528.010 565027.375 5934527.917 565024.628 5934527.823 565022.096 5934527.759 565018.040 5934527.701 565015.289 5934527.693 565013.367 5934527.703 565011.919 5934527.718 565009.463 5934527.761 565006.917 5934527.826 565004.315 5934527.915 565000.321 5934528.097 564996.013 5934528.353 564992.579 5934528.602 564989.968 5934528.819 564988.620 5934528.943 564984.593 5934524.512 564984.913 5934518.449 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_523a00ad-6533-4e60-b38a-06be2c3623d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565096.074 5934588.957</gml:lowerCorner>
          <gml:upperCorner>565119.138 5934623.909</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>1000</xplan:bezugspunkt>
          <xplan:h uom="m">51</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1ca60951-ec86-4875-b260-fbbe77da4968" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_11ee955c-60a0-4e1c-90c9-a6c79405f58d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">565119.138 5934590.440 565108.893 5934606.761 565098.128 5934623.909 565096.074 5934621.977 565116.773 5934588.957 565119.138 5934590.440 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GF uom="m2">23.800</xplan:GF>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_90d196c6-7704-4ecc-a6d7-394f1c0478c4">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Auf der mit „(C)“ bezeichneten Fläche des Kerngebiets darf
die festgesetzte Traufhöhe durch Aufbauten für Nebenanlagen
und Haustechnik um höchstens 8 m überschritten werden.
Die Aufbauten sind gruppiert anzuordnen und durch
Verkleidungen gestalterisch zusammenzufassen. Frei stehende
Antennenanlagen sind nicht zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3d97cdff-abfb-4d58-ae33-3c337440e946">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565088.647 5934593.248</gml:lowerCorner>
          <gml:upperCorner>565089.647 5934594.248</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7fbf04dc-cda8-4acd-831a-31d6327e2e27" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_66bf51a5-12af-4f53-a750-ae139ae54275">
          <gml:pos>565088.647 5934593.248</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5a7d638b-c969-48f6-b532-27283883b07b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565037.590 5934554.450</gml:lowerCorner>
          <gml:upperCorner>565038.590 5934555.450</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6437f13a-9da9-488d-84b5-871c9afec4b8" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_9bd02179-444f-4f0d-9319-f5cfbf07eaba">
          <gml:pos>565037.590 5934554.450</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c180d211-33e9-4580-8e8f-f58eadca8f8a">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im Kerngebiet sind Spielhallen und ähnliche Unternehmen
im Sinne von § 1 Absatz 2 des Hamburgischen Spielhallengesetzes
vom 4. Dezember 2012 (HmbGVBl. S. 505), zuletzt
geändert am 17. Februar 2021 (HmbGVBl. S. 75, 77), die der
Aufstellung von Spielgeräten mit oder ohne Gewinnmöglichkeiten
dienen, Vorführ- und Geschäftsräume, deren
Zweck auf Darstellungen oder auf Handlungen mit sexuellem
Charakter ausgerichtet ist, sowie Tankstellen im Zusammenhang
mit Parkhäusern und Großgaragen unzulässig.
Ausnahmen für Tankstellen nach § 7 Absatz 3 Nummer 1
der Baunutzungsverordnung in der Fassung vom 21. November
2017 (BGBl. I S. 3787) werden ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_809762ae-acf3-4ba0-9760-b988ac532891">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565164.044 5934647.510</gml:lowerCorner>
          <gml:upperCorner>565165.044 5934648.510</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_12af2fdb-69e1-4511-ad33-e78ca923e7df" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c18e308e-6975-47fa-975d-d25f9b2ee642">
          <gml:pos>565164.044 5934647.510</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1ca60951-ec86-4875-b260-fbbe77da4968">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565108.893 5934606.761</gml:lowerCorner>
          <gml:upperCorner>565109.893 5934607.761</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_523a00ad-6533-4e60-b38a-06be2c3623d8" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_efcdc4a3-eaa4-4b96-a939-832752452a03">
          <gml:pos>565108.893 5934606.761</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_25c63b93-2cfa-4fa3-906d-ca0cc4c5799a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565123.322 5934581.185</gml:lowerCorner>
          <gml:upperCorner>565123.322 5934581.185</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_09ca07dc-e432-4d01-a9c1-16816f563962" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_6aef2617-e530-4063-9657-a0f7a10e7ad6">
          <gml:pos>565123.322 5934581.185</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0d135aa7-ebd4-487a-bfed-6c3b94473835">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565149.237 5934609.391</gml:lowerCorner>
          <gml:upperCorner>565149.237 5934609.391</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GF</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f5255a2e-51f5-472b-82a7-c834187ddf9a" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_27d6adbd-e805-45e4-a833-581b5380aa9d">
          <gml:pos>565149.237 5934609.391</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9db49e76-0ff9-4057-89e8-9e677146a085">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565146.134 5934629.132</gml:lowerCorner>
          <gml:upperCorner>565146.134 5934629.132</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d1a4521b-59c9-4321-acd4-3f227535c90d" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_6e44903e-9d8f-4c7f-a318-214ee417b9f3">
          <gml:pos>565146.134 5934629.132</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c245b09b-2893-4014-bf33-b8ce363b1479">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565170.972 5934602.560</gml:lowerCorner>
          <gml:upperCorner>565170.972 5934602.560</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f5255a2e-51f5-472b-82a7-c834187ddf9a" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dbdf9912-928c-4b31-b099-98137ba5dd03">
          <gml:pos>565170.972 5934602.560</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0ad7d318-e915-42be-8739-1ce485872219">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Auf der mit „(B)“ bezeichneten Fläche des Kerngebiets darf
die Höhe der durch die festgesetzten Traufhöhen entstehenden
geneigten Dachfläche durch Aufbauten für Nebenanlagen
und Haustechnik um höchstens 1 m überschritten werden.
Die Aufbauten sind gruppiert anzuordnen und durch
Verkleidungen gestalterisch zusammenzufassen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_Rasterdarstellung gml:id="GML_4ac0a688-e026-46da-8ce0-61e54b0f3e8d">
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan002_5-2.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan002_5-2.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
    </xplan:XP_Rasterdarstellung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_203cbf95-87be-466f-b869-335011c68542">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Stellplätze sind nur in Tiefgaragen zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_5fab2299-1c39-4f77-8ab8-c12805fd3b4e">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Auf der mit „(A)“ bezeichneten Fläche des Kerngebiets sind
oberhalb des ersten Geschosses ausschließlich Wohnungen
zulässig. Mindestens 4 700 m² der Geschossfläche sind für
Wohnungen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a97811ae-add4-443d-9730-22929ea94313">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565138.333 5934552.923</gml:lowerCorner>
          <gml:upperCorner>565139.333 5934553.923</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cc3f7613-8da2-45ab-8e7c-c6d74c15de60" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_04d60843-b455-4b3b-994a-7bd8218fcb62">
          <gml:pos>565138.333 5934552.923</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b9b06b68-91d2-4b39-9bf8-bd42bf8490fb">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Auf der mit „(D)“ bezeichneten Fläche des Kerngebiets
sind mindestens fünf großkronige einheimische, standortgerechte
Laubbäume mit einem Stammumfang von mindestens
25 cm zu pflanzen und dauerhaft zu erhalten. Für
anzupflanzende Bäume auf Tiefgaragen muss auf einer Fläche
von 12 m² je Baum die Schichtstärke des durchwurzelbaren
Substrataufbaus mindestens 1 m betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05108ce5-8c8d-4453-a291-6cbe7c7387f4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565034.975 5934545.528</gml:lowerCorner>
          <gml:upperCorner>565035.975 5934546.528</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bf1e5a22-839d-4ef5-aaf3-3b4d97d2e963" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_eb71b8e3-a394-4e3b-9859-a97dfb50c221">
          <gml:pos>565034.975 5934545.528</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_94338043-1aad-4bae-8c47-5ae720ca393e">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Auf der mit „(E)“ bezeichneten Fläche des Kerngebiets sind
mindestens zehn großkronige einheimische, standortgerechte
Laubbäume mit einem Stammumfang von mindestens
25 cm zu pflanzen und dauerhaft zu erhalten. Für anzupflanzende
Bäume auf Tiefgaragen muss auf einer Fläche
von 12 m² je Baum die Schichtstärke des durchwurzelbaren
Substrataufbaus mindestens 1 m betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_60127909-667e-411d-b2f5-7176dd1ba297">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565119.995 5934558.512</gml:lowerCorner>
          <gml:upperCorner>565119.995 5934558.512</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_74695132-425d-4934-baea-be62efed31dc" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7a40582b-0cb5-41e5-99a2-112fe9d38de7">
          <gml:pos>565119.995 5934558.512</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0284b4a3-f97a-4479-a85a-b09c10bd1b73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565146.585 5934635.470</gml:lowerCorner>
          <gml:upperCorner>565146.585 5934635.470</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d1a4521b-59c9-4321-acd4-3f227535c90d" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ef032dbd-f781-4157-b0fe-29a7b4ee82c3">
          <gml:pos>565146.585 5934635.470</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c48df557-2637-47be-87a1-57e83f845993">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565081.175 5934575.759</gml:lowerCorner>
          <gml:upperCorner>565082.175 5934576.759</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d8192c7f-a252-4050-b474-47694b5008e6" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_724d5ea5-56a9-42f0-9feb-e51b0cf67731">
          <gml:pos>565081.175 5934575.759</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9ad62bbb-e92c-40a0-b728-552b351cfc3e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565041.183 5934540.403</gml:lowerCorner>
          <gml:upperCorner>565042.183 5934541.403</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_80335b66-8fb7-46a3-947f-8bddad49ee79" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0ca1d7e1-d5d7-47b9-9ae4-8df758b9f4b7">
          <gml:pos>565041.183 5934540.403</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c63d5ebe-b8d1-47ef-95e3-2cdb99dafe62">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565136.583 5934559.834</gml:lowerCorner>
          <gml:upperCorner>565137.583 5934560.834</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e3587eee-0394-4062-b606-3a197d395a39" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_09fc4ceb-6477-4cb9-b6a5-e1122a41c43a">
          <gml:pos>565136.583 5934559.834</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_24875732-da3e-4e57-9f25-89d74cd41468">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565061.436 5934554.226</gml:lowerCorner>
          <gml:upperCorner>565061.436 5934554.226</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_23e509ba-2f62-4294-834a-3bf97cef25ed" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_95c019bb-5b87-4779-9334-36eb4deba390">
          <gml:pos>565061.436 5934554.226</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3ba582d4-6de2-420b-bb0a-fd8ea26d551f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565155.708 5934600.194</gml:lowerCorner>
          <gml:upperCorner>565155.708 5934600.194</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_2c65a70f-1649-4012-b70d-4b61ed3157be" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_f78ebf2f-cc78-4b2b-be51-3df44451add6">
          <gml:pos>565155.708 5934600.194</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b68d8a04-fcb4-4fe0-ba1d-7c622be61da9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565033.382 5934571.458</gml:lowerCorner>
          <gml:upperCorner>565034.382 5934572.458</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_10cdd180-6bfc-4872-8199-5869396827ea" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_21f179b6-64d7-463d-ab9a-b8c971cd81ac">
          <gml:pos>565033.382 5934571.458</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0227c06e-c9c3-4d61-88cd-f57845c78844">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565086.209 5934553.331</gml:lowerCorner>
          <gml:upperCorner>565086.209 5934553.331</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d40b6338-c05d-48bc-b074-4f865f28bf13" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2a8fa7ab-5323-4790-90ed-3e53629e07d0">
          <gml:pos>565086.209 5934553.331</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7783fc04-bf01-4b3c-b4bb-3058ca6071e5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565055.805 5934570.327</gml:lowerCorner>
          <gml:upperCorner>565056.805 5934570.327</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_225d02a7-5968-4b9f-b767-828a7683e216" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3cf8980e-b0e6-4e68-b486-cdef98223c37">
          <gml:pos>565055.805 5934570.327</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_52dee64b-a907-4b1b-892d-61fff34962fc">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Auf den mit „(A)“ bezeichneten Flächen des Kerngebiets
sind durch Anordnung der Baukörper beziehungsweise
durch geeignete Grundrissgestaltung die Wohn- und Schlafräume
den lärmabgewandten Gebäudeseiten zuzuordnen.
Sofern eine Anordnung aller Wohn- und Schlafräume einer
Wohnung an den lärmabgewandten Gebäudeseiten nicht
möglich ist, sind vorrangig die Schlafräume den lärmabgewandten
Gebäudeseiten zuzuordnen. Für die Räume an den
lärmzugewandten Gebäudeseiten muss ein ausreichender
Lärmschutz durch bauliche Maßnahmen an Außentüren,
Fenstern, Außenwänden und Dächern der Gebäude geschaffen
werden. Wohn-/Schlafräume in Einzimmerwohnungen
und Kinderzimmer sind wie Schlafräume zu beurteilen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_073c07fd-6041-46ee-9271-69585acaf7a7">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Das festgesetzte Gehrecht umfasst die Befugnis der Freien
und Hansestadt Hamburg, einen allgemein zugänglichen
Weg anzulegen und zu unterhalten. Geringfügige Abweichungen von dem festgesetzten Gehrecht können zugelassen
werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bb877354-91fc-4ee7-8255-d91292e4e374">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565077.280 5934592.501</gml:lowerCorner>
          <gml:upperCorner>565077.280 5934592.501</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_33328a77-91a6-4864-b8a2-c52eab49bcac" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_abf0288f-3228-48c6-ad77-74e8ec7ff929">
          <gml:pos>565077.280 5934592.501</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_87e60ce7-8f61-4bc7-ad2b-72062166e775">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>565113.041 5934571.627</gml:lowerCorner>
          <gml:upperCorner>565114.041 5934572.627</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GF</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_70a45912-2871-4085-acf9-d3351cb00b20" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e3587eee-0394-4062-b606-3a197d395a39" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_480847ad-c958-4990-8d44-dd58856657d1">
          <gml:pos>565113.041 5934571.627</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
</xplan:XPlanAuszug>