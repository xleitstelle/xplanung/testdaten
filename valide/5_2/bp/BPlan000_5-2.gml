﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<xplan:XPlanAuszug xmlns:xplan="http://www.xplanung.de/xplangml/5/2" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/2 http://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/5.2/XPlanung-Operationen.xsd" gml:id="GML_3261e81a-abf7-43b2-b85d-92adbbd8a3c8">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>567015.804 5937951.758</gml:lowerCorner>
      <gml:upperCorner>567582.824 5938562.271</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_9a86c33a-15a6-40ff-bd41-ae8209335c6c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938562.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>Testdaten</xplan:name>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface srsName="EPSG:25832" gml:id="GML_810f6915-f9f7-46c6-b3c2-492d6b08f8bb">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25832" gml:id="GML_fe4b6c17-451a-4449-b226-783bb5be34a8">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsDimension="2" count="40">567015.804 5937951.758 567043.811 5937951.758 567093.783 5937951.758 567118.771 5937951.758 567148.735 5937951.758 567205.735 5937951.758 567267.936 5937951.758 567298.576 5937951.758 567362.511 5937951.758 567396.854 5937951.758 567486.541 5937951.758 567525.548 5937951.758 567547.650 5937951.758 567582.824 5937951.758 567582.824 5938061.568 567582.824 5938088.189 567582.824 5938149.584 567582.824 5938171.809 567582.824 5938193.639 567582.824 5938324.011 567582.824 5938379.257 567582.824 5938510.053 567582.824 5938512.948 567582.824 5938562.271 567416.500 5938562.271 567292.029 5938562.271 567279.922 5938562.271 567258.830 5938562.271 567233.137 5938562.271 567197.979 5938562.271 567127.226 5938562.271 567015.804 5938562.271 567015.804 5938476.821 567015.804 5938447.074 567015.804 5938422.626 567015.804 5938389.110 567015.804 5938258.262 567015.804 5938110.310 567015.804 5938087.012 567015.804 5937951.758 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:verfahrensMerkmale />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:plangeber />
      <xplan:planArt>1000</xplan:planArt>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:bereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_3c383c11-1942-41d9-9866-1f2216925566">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938562.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:MultiSurface srsName="EPSG:25832" gml:id="GML_0a96d396-f5da-4164-98c7-83a2c421f638">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25832" gml:id="GML_b1772b63-c00d-420c-930e-55234939833c">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsDimension="2" count="40">567015.804 5937951.758 567043.811 5937951.758 567093.783 5937951.758 567118.771 5937951.758 567148.735 5937951.758 567205.735 5937951.758 567267.936 5937951.758 567298.576 5937951.758 567362.511 5937951.758 567396.854 5937951.758 567486.541 5937951.758 567525.548 5937951.758 567547.650 5937951.758 567582.824 5937951.758 567582.824 5938061.568 567582.824 5938088.189 567582.824 5938149.584 567582.824 5938171.809 567582.824 5938193.639 567582.824 5938324.011 567582.824 5938379.257 567582.824 5938510.053 567582.824 5938512.948 567582.824 5938562.271 567416.500 5938562.271 567292.029 5938562.271 567279.922 5938562.271 567258.830 5938562.271 567233.137 5938562.271 567197.979 5938562.271 567127.226 5938562.271 567015.804 5938562.271 567015.804 5938476.821 567015.804 5938447.074 567015.804 5938422.626 567015.804 5938389.110 567015.804 5938258.262 567015.804 5938110.310 567015.804 5938087.012 567015.804 5937951.758 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#GML_dc60c672-b6ca-442c-b761-f56982073dd5" />
      <xplan:planinhalt xlink:href="#GML_df5f8049-6e74-4625-9771-3816a8cb488e" />
      <xplan:planinhalt xlink:href="#GML_90b9d6bb-761f-424f-a626-f968f4be24ab" />
      <xplan:planinhalt xlink:href="#GML_466718d3-cad4-4953-9ca0-01200cf301f5" />
      <xplan:planinhalt xlink:href="#GML_8e253cf2-115d-4b5b-8a68-0f6d76bd3e24" />
      <xplan:planinhalt xlink:href="#GML_60fc0a9d-d553-4aa5-ad6f-a7e29c6fa27b" />
      <xplan:planinhalt xlink:href="#GML_93b1097e-32cd-4062-8dc2-a4d47c7d1f21" />
      <xplan:planinhalt xlink:href="#GML_f0464838-d00f-4254-96c6-1390b7e17b89" />
      <xplan:planinhalt xlink:href="#GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6" />
      <xplan:planinhalt xlink:href="#GML_02d77422-eeca-43eb-b9ca-a7f50edf2568" />
      <xplan:planinhalt xlink:href="#GML_82b4c1e5-6ad6-4418-a886-4b8c3d28f6e8" />
      <xplan:planinhalt xlink:href="#GML_611be92c-f6e2-48c5-99a9-563cc1f00241" />
      <xplan:planinhalt xlink:href="#GML_e6cd43b8-806e-4971-be2a-a8d252be2aea" />
      <xplan:planinhalt xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:planinhalt xlink:href="#GML_6e3fef2a-c90f-4938-8220-4927fa563eda" />
      <xplan:planinhalt xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:planinhalt xlink:href="#GML_bdb12f92-c818-4c99-91da-14c3a319192c" />
      <xplan:planinhalt xlink:href="#GML_62517d2f-46c3-4130-b39e-1ed9b30da567" />
      <xplan:planinhalt xlink:href="#GML_f65a30d4-4ab5-46ec-b132-871ff48cd567" />
      <xplan:planinhalt xlink:href="#GML_8fd437f4-91ba-44cc-b5b1-bf484f1e5da1" />
      <xplan:planinhalt xlink:href="#GML_6e3a3a9a-c467-494d-8804-a831a6c47751" />
      <xplan:planinhalt xlink:href="#GML_1e4cc7ea-f4d3-4cba-b4f5-89b217b42f6f" />
      <xplan:planinhalt xlink:href="#GML_dea227da-2b90-49a9-a4c9-d7d5cb4a5c23" />
      <xplan:planinhalt xlink:href="#GML_27ab1190-ce43-4909-944f-3f9b973e53d8" />
      <xplan:planinhalt xlink:href="#GML_dc87fd67-4bd9-4462-b572-50af58e43a25" />
      <xplan:planinhalt xlink:href="#GML_28f122f0-3a41-4053-8b36-94fda694eaf3" />
      <xplan:planinhalt xlink:href="#GML_fe7cf17d-0c8b-40c6-99d3-5b6a9ade911a" />
      <xplan:planinhalt xlink:href="#GML_037a453e-a463-4174-bee1-20c19cf333dc" />
      <xplan:planinhalt xlink:href="#GML_a826790b-3463-40b8-85ef-4ab4c5057bdd" />
      <xplan:planinhalt xlink:href="#GML_8aa5d3a6-ee3a-4319-a322-ab1c541d440c" />
      <xplan:planinhalt xlink:href="#GML_f7b713f4-b5c3-4dd2-bd72-6d3dbaf5f2da" />
      <xplan:planinhalt xlink:href="#GML_457628a7-0103-46a7-8242-f69a1dd08bbc" />
      <xplan:planinhalt xlink:href="#GML_877d54f6-ce5d-4cbb-a54e-bacdc599a058" />
      <xplan:planinhalt xlink:href="#GML_c4ec3a94-59cf-4892-9e34-9a8d602fac0c" />
      <xplan:planinhalt xlink:href="#GML_6ab17d4d-68ce-4dbf-a25a-2cd14506c3e2" />
      <xplan:planinhalt xlink:href="#GML_456f38b7-f280-4949-ad4d-0d4ec2569d0a" />
      <xplan:planinhalt xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:planinhalt xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:planinhalt xlink:href="#GML_d901d022-a730-4f4e-aff3-2b7c90c6b93f" />
      <xplan:planinhalt xlink:href="#GML_285c0249-a226-4329-8adb-f0f344309ebd" />
      <xplan:planinhalt xlink:href="#GML_f6640434-2ab9-4ed4-9762-923bc477c0dc" />
      <xplan:planinhalt xlink:href="#GML_de472a5f-e4cd-45ec-af3b-2ac268232002" />
      <xplan:planinhalt xlink:href="#GML_e1ec8789-e611-491e-994b-d0d14e4d323e" />
      <xplan:planinhalt xlink:href="#GML_74b20dc5-b262-4ed7-8524-7bf1a48c0cf3" />
      <xplan:planinhalt xlink:href="#GML_b9746773-0461-4b84-abca-2190911c06dc" />
      <xplan:planinhalt xlink:href="#GML_7b7c86c2-4f93-44ed-b838-c3a8be12af71" />
      <xplan:planinhalt xlink:href="#GML_d7b0f8ed-6f59-4f38-979e-bbed137cfd24" />
      <xplan:planinhalt xlink:href="#GML_f06e6c1b-7c5e-44ce-ad88-d7c55dc46d14" />
      <xplan:planinhalt xlink:href="#GML_234f2be4-f17c-4d90-8f7c-9869b3105a33" />
      <xplan:planinhalt xlink:href="#GML_5a8e0bd9-c528-406a-993b-e4a573a16017" />
      <xplan:planinhalt xlink:href="#GML_fe0ae921-3ed8-47e7-a248-2ae10edfde1f" />
      <xplan:planinhalt xlink:href="#GML_45321a90-08fe-4932-a184-10b7f15d282f" />
      <xplan:planinhalt xlink:href="#GML_30ccabd6-1c88-4911-a5e0-83d229aefdd6" />
      <xplan:planinhalt xlink:href="#GML_21205338-39ee-4ed0-bce7-708290061eb5" />
      <xplan:planinhalt xlink:href="#GML_1245a1fa-8f64-4a47-9e6b-35cf7fa53ae1" />
      <xplan:planinhalt xlink:href="#GML_9ae668e9-5b2b-4fd0-a15f-01de34b81473" />
      <xplan:planinhalt xlink:href="#GML_599f5ba0-0938-4c55-a193-97bf6d53c0c4" />
      <xplan:planinhalt xlink:href="#GML_06878e37-9e44-4961-92c7-fbdd1cf1e6d0" />
      <xplan:planinhalt xlink:href="#GML_58314478-4223-4d9f-abe9-569150740f5c" />
      <xplan:planinhalt xlink:href="#GML_082e2ebd-438e-4adb-b72f-2931152707fe" />
      <xplan:planinhalt xlink:href="#GML_7d67f197-d2a6-46bb-8246-8152ab098cd7" />
      <xplan:planinhalt xlink:href="#GML_ea776d6f-1f0a-4f00-9fe1-cfdb574de904" />
      <xplan:planinhalt xlink:href="#GML_d07972fb-779d-40de-8e12-520c6368a999" />
      <xplan:planinhalt xlink:href="#GML_bf2606a7-3204-46d1-b709-f780a87d5ff3" />
      <xplan:planinhalt xlink:href="#GML_fd31dc77-0ace-44db-a0d9-1e3b9f4e6ee0" />
      <xplan:planinhalt xlink:href="#GML_9d1872c2-3bbb-4e26-acd1-0e0e7a178ae7" />
      <xplan:planinhalt xlink:href="#GML_b5bf4faa-5bf0-4246-bfcb-6162377c255e" />
      <xplan:planinhalt xlink:href="#GML_bdf97adf-d8a9-4dae-8362-a09a73a51fa0" />
      <xplan:planinhalt xlink:href="#GML_e3cf8a7d-ebe9-4e50-abff-2ccb4ee3d954" />
      <xplan:planinhalt xlink:href="#GML_3110388e-f0dc-4b54-a941-dda61ba75c3c" />
      <xplan:planinhalt xlink:href="#GML_d99b1ba4-8a29-4cc5-8ca3-e99f0767adcf" />
      <xplan:planinhalt xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:planinhalt xlink:href="#GML_a901293d-3d84-462e-98f3-e29fae2ed376" />
      <xplan:planinhalt xlink:href="#GML_c43ed4b4-0754-4ac9-a1cc-35bd57fa6d9c" />
      <xplan:praesentationsobjekt xlink:href="#GML_05ed645d-d470-405a-a390-e2683c21bf4e" />
      <xplan:praesentationsobjekt xlink:href="#GML_24f81e5a-5f25-4070-b617-e9d8f3073680" />
      <xplan:praesentationsobjekt xlink:href="#GML_64200842-66f5-429b-9963-0e83d2e75a1f" />
      <xplan:praesentationsobjekt xlink:href="#GML_7f7068c8-bc67-4e27-a516-6ff1c2f8e9f7" />
      <xplan:praesentationsobjekt xlink:href="#GML_e64e9539-4b62-40b6-b5fc-71d0085bbded" />
      <xplan:praesentationsobjekt xlink:href="#GML_c3c38f42-6c38-4ab1-822f-4f345f7aa2e2" />
      <xplan:praesentationsobjekt xlink:href="#GML_310624ef-717f-49b0-940d-65baa00d56e1" />
      <xplan:praesentationsobjekt xlink:href="#GML_7c0d896e-0f26-40e2-b0db-41ae4312b1ad" />
      <xplan:praesentationsobjekt xlink:href="#GML_3bfb88ba-1a31-45d6-b971-715d3406aced" />
      <xplan:praesentationsobjekt xlink:href="#GML_1af29368-7422-4575-af98-6681f7f61f64" />
      <xplan:praesentationsobjekt xlink:href="#GML_781d3793-ab82-441f-ab28-da69a48aad26" />
      <xplan:praesentationsobjekt xlink:href="#GML_1c64bb25-3c80-4f35-b892-abbf124e1401" />
      <xplan:praesentationsobjekt xlink:href="#GML_2611961f-cc1d-46ce-b049-a48b5f679ba4" />
      <xplan:praesentationsobjekt xlink:href="#GML_0009e672-4509-41d6-9da2-09c36daa0e72" />
      <xplan:praesentationsobjekt xlink:href="#GML_99a87f6d-9ca7-4a11-af2d-16c1afd84c32" />
      <xplan:praesentationsobjekt xlink:href="#GML_522f5f21-778f-469d-af6a-9a06eba37c6e" />
      <xplan:praesentationsobjekt xlink:href="#GML_151402d9-30cc-4c58-838e-3bcc54c787c9" />
      <xplan:praesentationsobjekt xlink:href="#GML_3510055f-d1f0-44c1-b29d-98ccf18e970a" />
      <xplan:praesentationsobjekt xlink:href="#GML_592a57e2-7c92-49c8-8273-fb49ff7c662e" />
      <xplan:praesentationsobjekt xlink:href="#GML_47750462-dbee-4124-a6a2-3f24446b54ee" />
      <xplan:praesentationsobjekt xlink:href="#GML_8499585b-e9b2-4c34-8fc0-9f69407dc54b" />
      <xplan:praesentationsobjekt xlink:href="#GML_28bc8afe-e15d-4e75-85d8-224c50a4af0e" />
      <xplan:praesentationsobjekt xlink:href="#GML_b36639b0-cc62-49d8-a553-52da27715c5f" />
      <xplan:praesentationsobjekt xlink:href="#GML_8eab63a2-fb13-408c-ac41-bbb194b2faae" />
      <xplan:praesentationsobjekt xlink:href="#GML_9333fe15-dd95-4aff-aebe-c37a48e4d2ec" />
      <xplan:praesentationsobjekt xlink:href="#GML_848f0251-7f04-451e-a5c3-5b849761fe28" />
      <xplan:praesentationsobjekt xlink:href="#GML_63eda789-900b-4f83-a52a-e2d7d0cd0faf" />
      <xplan:praesentationsobjekt xlink:href="#GML_ee9a801b-dc41-406b-b406-16f09b4f0499" />
      <xplan:praesentationsobjekt xlink:href="#GML_e96936cf-2271-4af4-ae6b-c0c2382100c2" />
      <xplan:praesentationsobjekt xlink:href="#GML_f5237d5a-b68d-4a1c-8d3c-2c4daa707588" />
      <xplan:praesentationsobjekt xlink:href="#GML_8e3c5fa1-0b8a-4a21-9fea-50e94d65f899" />
      <xplan:praesentationsobjekt xlink:href="#GML_3631c68f-2533-434a-83b5-a6609699f79b" />
      <xplan:praesentationsobjekt xlink:href="#GML_308d9453-aa40-4043-8682-5ed5a84ff118" />
      <xplan:praesentationsobjekt xlink:href="#GML_6396f67c-7150-44f2-9af9-7ea5e03d8f0b" />
      <xplan:praesentationsobjekt xlink:href="#GML_da614353-3e14-4548-a602-1dca72637536" />
      <xplan:praesentationsobjekt xlink:href="#GML_9bc001e7-69c3-4f5e-8146-97955e8d01f1" />
      <xplan:praesentationsobjekt xlink:href="#GML_c4bf1948-787e-4a71-b588-8d2091868fc9" />
      <xplan:praesentationsobjekt xlink:href="#GML_1b8d0589-6bf4-40fb-a071-e38c0e061914" />
      <xplan:praesentationsobjekt xlink:href="#GML_1f6eeb5a-8eb4-40de-b1a7-ff94cbc8b5ee" />
      <xplan:praesentationsobjekt xlink:href="#GML_1cbff272-89c7-412f-bb31-8ddc38544954" />
      <xplan:praesentationsobjekt xlink:href="#GML_4470427b-a9ad-4576-8aa4-d9822d4a12c6" />
      <xplan:praesentationsobjekt xlink:href="#GML_691d25f5-2077-4da5-86f0-d3016d33cdfa" />
      <xplan:praesentationsobjekt xlink:href="#GML_cd84ba26-68f2-45ab-9903-df772de1b93f" />
      <xplan:praesentationsobjekt xlink:href="#GML_e6a04548-9ba4-4b9d-bc5b-0d5a6977fe87" />
      <xplan:praesentationsobjekt xlink:href="#GML_36b30dcd-2d84-4cad-94fa-d6e02c711b16" />
      <xplan:praesentationsobjekt xlink:href="#GML_e9659569-957f-416a-8c7b-346be20371f8" />
      <xplan:praesentationsobjekt xlink:href="#GML_626ddd65-14ee-4532-9d7c-0cdd20fb4a29" />
      <xplan:praesentationsobjekt xlink:href="#GML_1c12ef22-0e3e-4a95-a9d9-75f8669b1f59" />
      <xplan:praesentationsobjekt xlink:href="#GML_1e7dd689-8623-4bd0-b4e4-b03299e77ac0" />
      <xplan:gehoertZuPlan xlink:href="#GML_9a86c33a-15a6-40ff-bd41-ae8209335c6c" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_dc60c672-b6ca-442c-b761-f56982073dd5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567244.926 5938373.562</gml:lowerCorner>
          <gml:upperCorner>567371.125 5938444.409</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3510055f-d1f0-44c1-b29d-98ccf18e970a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_cd84ba26-68f2-45ab-9903-df772de1b93f" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:laermkontingent />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_82576630-de52-46e0-995a-162f901ed639">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_af8211af-7484-4e44-8c3c-ad28cc06debb">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">567369.283 5938423.549 567369.492 5938424.355 567371.125 5938430.644 567363.739 5938432.560 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">567363.739 5938432.560 567318.248 5938441.362 567272.014 5938444.408 567260.092 5938444.250 567248.181 5938443.710 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="13">567248.181 5938443.710 567244.926 5938443.511 567245.668 5938436.045 567246.024 5938432.461 567249.113 5938401.379 567283.555 5938392.442 567319.931 5938383.002 567356.310 5938373.562 567357.373 5938377.661 567360.563 5938389.950 567362.838 5938398.716 567366.304 5938412.072 567369.283 5938423.549 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1700</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_df5f8049-6e74-4625-9771-3816a8cb488e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567454.916 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567525.548 5938008.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8499585b-e9b2-4c34-8fc0-9f69407dc54b" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:laermkontingent />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_826ffe9d-dddc-46a6-8c2d-fba0388c801f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567525.548 5937951.758 567477.970 5938008.959 567454.916 5937989.782 567486.541 5937951.758 567525.548 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:besondereArtDerBaulNutzung>1700</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_90b9d6bb-761f-424f-a626-f968f4be24ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567533.742 5938324.011</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938379.257</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5c75acb4-3b96-40af-a9d3-acf5c8ee1493">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567533.742 5938332.999 567557.061 5938328.505 567582.824 5938324.011 567582.824 5938379.257 567533.742 5938332.999 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AbgrabungsFlaeche gml:id="GML_466718d3-cad4-4953-9ca0-01200cf301f5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567396.854 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567486.541 5937989.782</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_9a504654-0c4b-4c9a-9dd0-d759b63f6054">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567486.541 5937951.758 567454.916 5937989.782 567396.854 5937951.758 567486.541 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_AbgrabungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_8e253cf2-115d-4b5b-8a68-0f6d76bd3e24">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567454.212 5938014.229</gml:lowerCorner>
          <gml:upperCorner>567580.170 5938136.221</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e794900c-d791-4ae1-932d-f248016c133e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567538.681 5938136.221 567454.705 5938068.518 567454.212 5938064.089 567495.689 5938014.229 567579.665 5938085.411 567580.170 5938088.485 567540.999 5938133.554 567538.681 5938136.221 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_SchutzgebietWasserrecht gml:id="GML_60fc0a9d-d553-4aa5-ad6f-a7e29c6fa27b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567281.705 5938045.583</gml:lowerCorner>
          <gml:upperCorner>567318.553 5938070.396</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Festgestelltes Wasserschutzgebiet</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b4434a50-31f1-432e-b969-ca2c1013e4b9">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567281.705 5938057.637 567290.138 5938045.583 567318.553 5938057.440 567313.145 5938070.396 567281.705 5938057.637 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_SchutzgebietWasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_93b1097e-32cd-4062-8dc2-a4d47c7d1f21">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567130.341 5937967.016</gml:lowerCorner>
          <gml:upperCorner>567305.292 5938108.546</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_abcae9a7-c33d-4ea0-b1a3-0b86e3172234">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567302.934 5938108.546 567130.341 5937979.173 567139.114 5937967.016 567305.292 5938091.571 567302.934 5938108.546 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AusgleichsFlaeche gml:id="GML_f0464838-d00f-4254-96c6-1390b7e17b89">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567376.514 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567477.970 5938053.544</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>Z</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ca5be05c-351c-4f8a-b516-1bb42d51464d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">567436.133 5938053.544 567377.746 5938006.460 567376.514 5938001.615 567386.684 5937973.686 567396.854 5937951.758 567454.916 5937989.782 567477.970 5938008.959 567441.231 5938053.131 567436.133 5938053.544 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>3000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1800</xplan:klassifizMassnahme>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_AusgleichsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567410.755 5938443.697</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938562.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_848f0251-7f04-451e-a5c3-5b849761fe28" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8e3c5fa1-0b8a-4a21-9fea-50e94d65f899" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6396f67c-7150-44f2-9af9-7ea5e03d8f0b" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fcac2428-60d0-4c2b-bcb1-328780381c14">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="22">567571.372 5938501.929 567574.969 5938505.207 567582.824 5938512.948 567582.824 5938562.271 567416.500 5938562.271 567412.642 5938497.933 567410.755 5938466.485 567502.824 5938449.130 567508.280 5938451.863 567510.055 5938450.025 567509.422 5938445.019 567510.320 5938444.871 567510.124 5938443.697 567520.169 5938453.613 567531.834 5938456.600 567556.524 5938463.761 567556.653 5938469.831 567566.376 5938469.379 567568.962 5938472.120 567570.910 5938481.073 567570.835 5938487.298 567571.372 5938501.929 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GF uom="m2">15000</xplan:GF>
      <xplan:Zzwingend>3</xplan:Zzwingend>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_02d77422-eeca-43eb-b9ca-a7f50edf2568">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567463.902 5938343.220</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938512.948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_bbe187a1-9cfd-4e8c-926d-665a97190492">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_00387732-6f2b-4903-a6e5-d0f566572087">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="26">567547.713 5938453.521 567555.543 5938457.047 567560.701 5938458.168 567571.986 5938456.701 567574.665 5938498.826 567576.772 5938504.087 567582.824 5938510.053 567582.824 5938512.948 567574.969 5938505.207 567571.372 5938501.929 567570.835 5938487.298 567570.910 5938481.073 567568.962 5938472.120 567566.376 5938469.379 567556.653 5938469.831 567556.524 5938463.761 567531.834 5938456.600 567520.169 5938453.613 567510.124 5938443.697 567503.231 5938436.893 567503.576 5938436.535 567501.170 5938426.383 567499.218 5938420.711 567493.505 5938404.769 567491.125 5938401.775 567487.277 5938398.894 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567487.277 5938398.894 567479.919 5938398.795 567477.928 5938391.711 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="22">567477.928 5938391.711 567477.141 5938388.674 567474.557 5938378.719 567473.809 5938375.834 567466.430 5938347.404 567463.902 5938345.791 567477.939 5938343.220 567476.327 5938345.747 567485.737 5938382.005 567486.542 5938383.695 567495.004 5938400.143 567498.126 5938407.327 567501.037 5938417.096 567503.841 5938424.048 567510.144 5938433.503 567512.585 5938435.553 567524.637 5938433.320 567524.833 5938434.494 567531.732 5938441.297 567532.630 5938441.146 567544.722 5938446.805 567547.713 5938453.521 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_82b4c1e5-6ad6-4418-a886-4b8c3d28f6e8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567366.892 5938001.615</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938193.639</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2109fbe6-8f13-4442-ae4a-4e76cadff81d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">567582.824 5938171.809 567582.824 5938193.639 567371.738 5938023.447 567366.892 5938024.676 567376.514 5938001.615 567377.746 5938006.460 567436.133 5938053.544 567454.705 5938068.518 567538.681 5938136.221 567571.395 5938162.594 567582.824 5938171.809 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_611be92c-f6e2-48c5-99a9-563cc1f00241">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567243.302 5938388.674</gml:lowerCorner>
          <gml:upperCorner>567503.576 5938460.687</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_592a57e2-7c92-49c8-8273-fb49ff7c662e" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_6f3f615d-eefc-4f32-a75c-49e1bf5c5cee">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_d3b170b7-4836-4d60-a078-50cc397c36ae">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="9">567420.143 5938449.083 567321.813 5938460.687 567272.062 5938460.161 567250.619 5938459.935 567243.302 5938459.858 567243.578 5938457.078 567244.460 5938448.206 567244.926 5938443.511 567248.181 5938443.710 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">567248.181 5938443.710 567260.092 5938444.250 567272.014 5938444.408 567318.248 5938441.362 567363.739 5938432.560 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">567363.739 5938432.560 567371.125 5938430.644 567405.587 5938421.702 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567405.587 5938421.702 567440.464 5938409.005 567472.303 5938389.930 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">567472.303 5938389.930 567477.141 5938388.674 567477.928 5938391.711 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567477.928 5938391.711 567479.919 5938398.795 567487.277 5938398.894 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">567487.277 5938398.894 567491.125 5938401.775 567493.505 5938404.769 567499.218 5938420.711 567501.170 5938426.383 567503.576 5938436.535 567503.231 5938436.893 567502.333 5938437.043 567492.012 5938432.190 567488.981 5938432.694 567488.888 5938432.133 567420.143 5938449.083 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:besondereArtDerBaulNutzung>1600</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_e6cd43b8-806e-4971-be2a-a8d252be2aea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567571.395 5938149.584</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938171.809</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Hochwassergefährdeter Bereich</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_71303681-c5dc-4964-9664-465603792880">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567582.824 5938171.809 567571.395 5938162.594 567582.824 5938149.584 567582.824 5938171.809 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>20002</xplan:artDerFestlegung>
      <xplan:istNatuerlichesUberschwemmungsgebiet>false</xplan:istNatuerlichesUberschwemmungsgebiet>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_37375f30-503d-4263-bfd6-2213deddb929">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567233.137 5938486.665</gml:lowerCorner>
          <gml:upperCorner>567292.029 5938562.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3bfb88ba-1a31-45d6-b971-715d3406aced" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1c64bb25-3c80-4f35-b892-abbf124e1401" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_691d25f5-2077-4da5-86f0-d3016d33cdfa" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e6a04548-9ba4-4b9d-bc5b-0d5a6977fe87" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_626ddd65-14ee-4532-9d7c-0cdd20fb4a29" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e0719073-0e82-4ccb-bf02-0fd5602eb07c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="14">567292.029 5938562.271 567279.922 5938562.271 567258.830 5938562.271 567233.137 5938562.271 567234.583 5938547.676 567237.357 5938519.723 567238.391 5938509.312 567239.212 5938501.043 567240.344 5938489.645 567284.812 5938486.665 567285.897 5938498.014 567286.676 5938506.178 567287.646 5938516.351 567292.029 5938562.271 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GR uom="m2">1500</xplan:GR>
      <xplan:GR_Ausn uom="m2">1600</xplan:GR_Ausn>
      <xplan:Zmin>1</xplan:Zmin>
      <xplan:Zmax>3</xplan:Zmax>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_WasserwirtschaftsFlaeche gml:id="GML_6e3fef2a-c90f-4938-8220-4927fa563eda">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567148.735 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567262.546 5938024.163</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_8e09e6dc-b3c0-43b6-88ac-d2c415ddd0f1">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">567206.468 5937951.758 567262.546 5937990.158 567237.045 5938024.163 567204.979 5938000.135 567183.123 5937983.755 567184.803 5937981.517 567186.123 5937979.758 567150.447 5937953.041 567148.735 5937951.758 567205.735 5937951.758 567206.468 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:BP_WasserwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_7c55b006-758e-497e-8d01-b155ef84691c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5938476.821</gml:lowerCorner>
          <gml:upperCorner>567200.034 5938562.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05ed645d-d470-405a-a390-e2683c21bf4e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7c0d896e-0f26-40e2-b0db-41ae4312b1ad" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_47750462-dbee-4124-a6a2-3f24446b54ee" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b36639b0-cc62-49d8-a553-52da27715c5f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ee9a801b-dc41-406b-b406-16f09b4f0499" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1c12ef22-0e3e-4a95-a9d9-75f8669b1f59" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_cfd70bfd-237c-4635-a70b-32504347f0c2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="13">567168.085 5938538.344 567200.034 5938541.518 567199.246 5938549.475 567197.979 5938562.271 567127.226 5938562.271 567015.804 5938562.271 567015.804 5938476.821 567070.634 5938480.460 567110.510 5938483.516 567172.986 5938488.982 567171.402 5938504.931 567170.294 5938516.085 567168.085 5938538.344 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GFZ>0.4</xplan:GFZ>
      <xplan:GRZ>0.2</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1000</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_bdb12f92-c818-4c99-91da-14c3a319192c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567206.468 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567341.849 5938057.440</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1e7dd689-8623-4bd0-b4e4-b03299e77ac0" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7e10db23-93df-470d-a7fd-c3c9d03da41e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="15">567324.161 5938044.003 567318.553 5938057.440 567290.138 5938045.583 567291.170 5938043.110 567291.495 5938032.519 567298.810 5938014.990 567284.670 5938005.562 567262.546 5937990.158 567206.468 5937951.758 567267.936 5937951.758 567341.849 5938001.617 567340.082 5938005.854 567335.111 5938017.763 567329.956 5938030.116 567324.161 5938044.003 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_62517d2f-46c3-4130-b39e-1ed9b30da567">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567476.327 5938332.999</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938510.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_63eda789-900b-4f83-a52a-e2d7d0cd0faf" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e3ea508a-90a5-488b-99b9-115a6a943b7f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="26">567582.824 5938510.053 567576.772 5938504.087 567574.665 5938498.826 567571.986 5938456.701 567560.701 5938458.168 567555.543 5938457.047 567547.713 5938453.521 567544.722 5938446.805 567532.630 5938441.146 567531.732 5938441.297 567524.833 5938434.494 567524.637 5938433.320 567512.585 5938435.553 567510.144 5938433.503 567503.841 5938424.048 567501.037 5938417.096 567498.126 5938407.327 567495.004 5938400.143 567486.542 5938383.695 567485.737 5938382.005 567476.327 5938345.747 567477.939 5938343.220 567526.224 5938334.376 567533.742 5938332.999 567582.824 5938379.257 567582.824 5938510.053 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:besondereArtDerBaulNutzung>1550</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_f65a30d4-4ab5-46ec-b132-871ff48cd567">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567249.113 5938308.143</gml:lowerCorner>
          <gml:upperCorner>567533.742 5938401.379</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_341e4d48-8211-4423-994d-4363b8224ddb">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="20">567356.310 5938373.562 567319.931 5938383.002 567283.555 5938392.442 567249.113 5938401.379 567260.728 5938367.386 567274.604 5938363.786 567307.497 5938355.249 567357.809 5938342.195 567412.960 5938327.880 567429.671 5938323.543 567455.467 5938316.850 567497.150 5938309.214 567502.998 5938308.143 567533.742 5938332.999 567526.224 5938334.376 567477.939 5938343.220 567463.902 5938345.791 567429.069 5938354.679 567392.691 5938364.120 567356.310 5938373.562 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_8fd437f4-91ba-44cc-b5b1-bf484f1e5da1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567297.762 5938154.355</gml:lowerCorner>
          <gml:upperCorner>567378.607 5938224.982</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Festgestellte Abfallbeseitigungsanlage</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_43183b1e-2ce2-4c50-a3fd-e4129deb4c11">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567378.607 5938207.581 567354.153 5938224.982 567314.754 5938201.254 567297.762 5938190.312 567312.768 5938154.355 567378.607 5938207.581 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>2200</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_6e3a3a9a-c467-494d-8804-a831a6c47751">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567396.854 5938422.626</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_489466da-5003-43c5-98f5-8d61de131da8">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_ebf16e17-49d9-4ca1-b88d-4c4c813d2c99">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567263.576 5938255.575 567225.902 5938284.094 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="11">567225.902 5938284.094 567217.448 5938292.976 567208.723 5938301.592 567196.276 5938312.987 567183.350 5938323.836 567177.298 5938328.595 567171.155 5938333.235 567162.558 5938339.413 567153.798 5938345.358 567143.469 5938351.930 567132.940 5938358.178 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">567132.940 5938358.178 567081.813 5938397.885 567018.719 5938421.533 567015.804 5938422.626 567015.804 5938389.110 567023.968 5938386.051 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567023.968 5938386.051 567024.676 5938385.749 567025.354 5938385.384 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="8">567025.354 5938385.384 567049.950 5938370.659 567084.629 5938349.895 567101.343 5938339.886 567102.280 5938339.324 567115.663 5938331.311 567116.526 5938330.794 567123.095 5938326.863 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">567123.095 5938326.863 567123.886 5938326.388 567124.676 5938325.910 567149.792 5938309.211 567173.413 5938290.456 567180.930 5938283.765 567188.257 5938276.866 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">567188.257 5938276.866 567202.086 5938255.114 567220.892 5938238.284 567223.519 5938235.890 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="9">567223.519 5938235.890 567241.031 5938217.362 567255.979 5938196.710 567256.005 5938196.668 567256.032 5938196.625 567258.312 5938192.935 567260.538 5938189.213 567270.308 5938171.230 567278.830 5938152.624 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="40">567278.830 5938152.624 567286.671 5938133.836 567291.371 5938137.361 567292.321 5938135.101 567292.355 5938135.019 567292.537 5938135.094 567292.680 5938135.153 567302.566 5938111.561 567302.299 5938111.449 567302.289 5938111.330 567303.316 5938108.832 567302.934 5938108.546 567305.292 5938091.571 567309.706 5938078.637 567313.145 5938070.396 567318.553 5938057.440 567324.161 5938044.003 567329.956 5938030.116 567335.111 5938017.763 567340.082 5938005.854 567341.849 5938001.617 567350.869 5937980.200 567362.511 5937951.758 567396.854 5937951.758 567386.684 5937973.686 567376.514 5938001.615 567366.892 5938024.676 567348.773 5938068.084 567338.041 5938093.800 567336.035 5938098.606 567330.820 5938111.102 567324.186 5938112.291 567320.605 5938122.124 567318.021 5938128.086 567308.226 5938150.681 567312.768 5938154.355 567297.762 5938190.312 567293.062 5938201.575 567285.485 5938215.452 567263.576 5938255.575 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_1e4cc7ea-f4d3-4cba-b4f5-89b217b42f6f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567208.723 5938255.575</gml:lowerCorner>
          <gml:upperCorner>567263.576 5938375.141</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_33a120d7-74da-421f-86b9-a887da141f6a">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_b616ec73-389b-47d2-9678-5fbfad7125f4">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">567256.632 5938325.593 567255.054 5938341.507 567253.278 5938359.430 567253.134 5938360.885 567260.728 5938367.386 567218.109 5938375.141 567221.900 5938371.886 567223.207 5938358.717 567224.469 5938345.992 567227.773 5938312.707 567227.946 5938310.962 567208.723 5938301.592 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567208.723 5938301.592 567217.448 5938292.976 567225.902 5938284.094 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="5">567225.902 5938284.094 567263.576 5938255.575 567262.348 5938267.909 567258.208 5938309.682 567256.632 5938325.593 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_dea227da-2b90-49a9-a4c9-d7d5cb4a5c23">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567429.671 5938260.380</gml:lowerCorner>
          <gml:upperCorner>567502.998 5938323.543</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_308d9453-aa40-4043-8682-5ed5a84ff118" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:laermkontingent />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_68c3e36d-17db-47b2-841b-3115d746d493">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567502.998 5938308.143 567497.150 5938309.214 567455.467 5938316.850 567429.671 5938323.543 567435.323 5938272.878 567443.921 5938260.380 567502.998 5938308.143 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>14000</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpielSportanlagenFlaeche gml:id="GML_27ab1190-ce43-4909-944f-3f9b973e53d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567538.681 5938085.411</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938162.594</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_77ec3dac-6af0-4309-975f-4546ddd36f4b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567538.681 5938136.221 567540.999 5938133.554 567580.170 5938088.485 567579.665 5938085.411 567582.824 5938088.189 567582.824 5938149.584 567571.395 5938162.594 567538.681 5938136.221 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:BP_SpielSportanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_dc87fd67-4bd9-4462-b572-50af58e43a25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567308.226 5938128.086</gml:lowerCorner>
          <gml:upperCorner>567557.061 5938332.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ef30390d-20e1-4e38-a1b8-d6ac695327d2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">567557.061 5938328.505 567533.742 5938332.999 567502.998 5938308.143 567443.921 5938260.380 567378.607 5938207.581 567312.768 5938154.355 567308.226 5938150.681 567318.021 5938128.086 567557.061 5938328.505 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_28f122f0-3a41-4053-8b36-94fda694eaf3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567308.226 5938128.086</gml:lowerCorner>
          <gml:upperCorner>567557.061 5938332.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_12964979-2e83-4956-a1db-b7a13d74d6be">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">567378.607 5938207.581 567312.768 5938154.355 567308.226 5938150.681 567318.021 5938128.086 567557.061 5938328.505 567533.742 5938332.999 567502.998 5938308.143 567443.921 5938260.380 567378.607 5938207.581 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_fe7cf17d-0c8b-40c6-99d3-5b6a9ade911a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567503.171 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938038.469</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_522f5f21-778f-469d-af6a-9a06eba37c6e" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_085fb967-d96c-4241-b4e6-28466bb2ab12">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567582.824 5937997.351 567551.013 5938033.620 567546.759 5938038.469 567542.883 5938038.270 567503.171 5938005.234 567547.650 5937951.758 567582.824 5937951.758 567582.824 5937997.351 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:besondereArtDerBaulNutzung>1000</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_037a453e-a463-4174-bee1-20c19cf333dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567209.991 5938367.386</gml:lowerCorner>
          <gml:upperCorner>567260.728 5938404.716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Höhengleiche Kreuzung mit Straße</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_63135c57-8063-4832-b792-98b48fb22cfe">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567249.113 5938401.379 567209.991 5938404.716 567218.109 5938375.141 567260.728 5938367.386 567249.113 5938401.379 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_a826790b-3463-40b8-85ef-4ab4c5057bdd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567028.067 5938232.097</gml:lowerCorner>
          <gml:upperCorner>567089.213 5938291.947</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_e373da2a-804d-4986-9a1d-b793e777de2e">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567053.409 5938233.471 567049.954 5938232.097 567046.505 5938233.488 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567046.505 5938233.488 567029.481 5938251.734 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567029.481 5938251.734 567028.069 5938255.075 567029.288 5938258.492 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567029.288 5938258.492 567063.716 5938290.519 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567063.716 5938290.519 567067.491 5938291.939 567071.086 5938290.111 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567071.086 5938290.111 567088.268 5938271.782 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567088.268 5938271.782 567089.209 5938268.663 567088.029 5938265.626 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567088.029 5938265.626 567053.409 5938233.471 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_8aa5d3a6-ee3a-4319-a322-ab1c541d440c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567267.936 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567350.869 5938001.617</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_198c1b67-ea9d-4ffa-89f8-c7c95a683b83">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567298.576 5937951.758 567310.846 5937959.935 567323.815 5937967.212 567350.869 5937980.200 567341.849 5938001.617 567267.936 5937951.758 567298.576 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_f7b713f4-b5c3-4dd2-bd72-6d3dbaf5f2da">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567396.854 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567486.541 5937989.782</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_64200842-66f5-429b-9963-0e83d2e75a1f" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:laermkontingent />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a7a0247a-7936-4cb9-a6fd-6cd81262afb3">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567486.541 5937951.758 567454.916 5937989.782 567396.854 5937951.758 567486.541 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1800</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_457628a7-0103-46a7-8242-f69a1dd08bbc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567546.759 5937997.351</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938061.568</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die GSt, GGa oder GTGa bestimmt sind</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_c6a4ff2e-7014-43ec-9949-fd74953eb285">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567582.824 5938061.568 567560.946 5938042.334 567560.757 5938039.186 567546.759 5938038.469 567551.013 5938033.620 567582.824 5937997.351 567582.824 5938061.568 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Immissionsschutz gml:id="GML_877d54f6-ce5d-4cbb-a54e-bacdc599a058">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567533.742 5938324.011</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938379.257</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_73183422-9920-4b25-b133-9187f068335b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567533.742 5938332.999 567557.061 5938328.505 567582.824 5938324.011 567582.824 5938379.257 567533.742 5938332.999 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Immissionsschutz>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Luftverkehrsrecht gml:id="GML_c4ec3a94-59cf-4892-9e34-9a8d602fac0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.511 5938005.562</gml:lowerCorner>
          <gml:upperCorner>567298.810 5938057.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5015b9d5-8a12-40e9-bdb5-ef277d260c82">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567258.511 5938040.245 567284.670 5938005.562 567298.810 5938014.990 567291.495 5938032.519 567291.170 5938043.110 567290.138 5938045.583 567281.705 5938057.637 567258.511 5938040.245 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:SO_Luftverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AufschuettungsFlaeche gml:id="GML_6ab17d4d-68ce-4dbf-a25a-2cd14506c3e2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567454.916 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567525.548 5938008.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e7c5d403-32f7-4e50-b433-4c774ce90546">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567525.548 5937951.758 567477.970 5938008.959 567454.916 5937989.782 567486.541 5937951.758 567525.548 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_AufschuettungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_LandwirtschaftsFlaeche gml:id="GML_456f38b7-f280-4949-ad4d-0d4ec2569d0a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567041.996 5938003.864</gml:lowerCorner>
          <gml:upperCorner>567166.778 5938152.358</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d52591d9-8427-44a1-b9f4-4553d5f94bc4">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">567060.870 5938129.255 567054.889 5938123.649 567041.996 5938111.566 567113.291 5938003.864 567135.639 5938020.620 567152.069 5938032.938 567166.778 5938043.964 567085.525 5938152.358 567062.933 5938131.188 567060.870 5938129.255 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_LandwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_f60afcda-b66b-424a-a013-9d6009892de2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567168.085 5938488.982</gml:lowerCorner>
          <gml:upperCorner>567204.972 5938541.518</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e64e9539-4b62-40b6-b5fc-71d0085bbded" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c3c38f42-6c38-4ab1-822f-4f345f7aa2e2" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8eab63a2-fb13-408c-ac41-bbb194b2faae" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9333fe15-dd95-4aff-aebe-c37a48e4d2ec" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f5237d5a-b68d-4a1c-8d3c-2c4daa707588" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1f6eeb5a-8eb4-40de-b1a7-ff94cbc8b5ee" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1cbff272-89c7-412f-bb31-8ddc38544954" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_28227528-caa8-415b-855a-a1152a12f36d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">567202.243 5938519.259 567200.034 5938541.518 567168.085 5938538.344 567170.294 5938516.085 567171.402 5938504.931 567172.986 5938488.982 567194.220 5938490.843 567204.972 5938491.786 567202.243 5938519.259 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GFZ>1.2</xplan:GFZ>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:GRZ_Ausn>0.7</xplan:GRZ_Ausn>
      <xplan:Z>1</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>3000</xplan:bauweise>
      <xplan:bebauungsArt>6000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_00c956a7-c970-4a71-ad80-62d0b4c89f52">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5938397.885</gml:lowerCorner>
          <gml:upperCorner>567213.246 5938455.880</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_24f81e5a-5f25-4070-b617-e9d8f3073680" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7f7068c8-bc67-4e27-a516-6ff1c2f8e9f7" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_781d3793-ab82-441f-ab28-da69a48aad26" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3631c68f-2533-434a-83b5-a6609699f79b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c4bf1948-787e-4a71-b588-8d2091868fc9" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4470427b-a9ad-4576-8aa4-d9822d4a12c6" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e9659569-957f-416a-8c7b-346be20371f8" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ebd4e158-59ec-4414-af78-6ff142f2cccd">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="20">567018.719 5938421.533 567081.813 5938397.885 567113.801 5938399.567 567133.878 5938400.621 567153.849 5938401.670 567173.766 5938402.715 567193.214 5938403.789 567201.698 5938404.258 567209.991 5938404.716 567213.246 5938408.509 567210.933 5938431.793 567208.638 5938454.886 567208.539 5938455.880 567198.648 5938455.527 567195.861 5938455.428 567091.844 5938451.744 567038.071 5938448.442 567015.804 5938447.074 567015.804 5938422.626 567018.719 5938421.533 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:DN uom="grad">45</xplan:DN>
          <xplan:dachform>2100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>
      <xplan:DN uom="grad">45</xplan:DN>
      <xplan:MaxZahlWohnungen>4</xplan:MaxZahlWohnungen>
      <xplan:Z>4</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1300</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="GML_d901d022-a730-4f4e-aff3-2b7c90c6b93f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567130.341 5937967.016</gml:lowerCorner>
          <gml:upperCorner>567305.292 5938108.546</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_c0350171-52bb-45c1-969c-e8b2f71e7f20">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567302.934 5938108.546 567130.341 5937979.173 567135.695 5937971.220 567139.114 5937967.016 567305.292 5938091.571 567302.934 5938108.546 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_285c0249-a226-4329-8adb-f0f344309ebd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567356.310 5938345.791</gml:lowerCorner>
          <gml:upperCorner>567477.141 5938430.644</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_310624ef-717f-49b0-940d-65baa00d56e1" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1b8d0589-6bf4-40fb-a071-e38c0e061914" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:laermkontingent />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_c14f518d-75ee-4746-8f23-f898701f0649">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_c9f6652e-d321-4e05-9805-2b3e2e6b491c">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567477.141 5938388.674 567472.303 5938389.930 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567472.303 5938389.930 567440.464 5938409.005 567405.587 5938421.702 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="16">567405.587 5938421.702 567371.125 5938430.644 567369.492 5938424.355 567369.283 5938423.549 567366.304 5938412.072 567362.838 5938398.716 567360.563 5938389.950 567357.373 5938377.661 567356.310 5938373.562 567392.691 5938364.120 567429.069 5938354.679 567463.902 5938345.791 567466.430 5938347.404 567473.809 5938375.834 567474.557 5938378.719 567477.141 5938388.674 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.3</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1800</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_f6640434-2ab9-4ed4-9762-923bc477c0dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567298.576 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567362.511 5937980.200</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fe4c981c-b264-4157-b591-1b8757f64546">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567310.846 5937959.935 567298.576 5937951.758 567362.511 5937951.758 567350.869 5937980.200 567323.815 5937967.212 567310.846 5937959.935 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_de472a5f-e4cd-45ec-af3b-2ac268232002">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567093.783 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567139.114 5937979.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d48cc90e-900a-4782-8a1a-f2bffef13817">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567118.771 5937951.758 567139.114 5937967.016 567130.341 5937979.173 567093.783 5937951.758 567118.771 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Luftverkehrsrecht gml:id="GML_e1ec8789-e611-491e-994b-d0d14e4d323e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567503.171 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938038.469</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7497eaee-7eb0-415a-8c80-2ac9a721c416">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567582.824 5937997.351 567551.013 5938033.620 567546.759 5938038.469 567542.883 5938038.270 567503.171 5938005.234 567547.650 5937951.758 567582.824 5937951.758 567582.824 5937997.351 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>7000</xplan:artDerFestlegung>
    </xplan:SO_Luftverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_74b20dc5-b262-4ed7-8524-7bf1a48c0cf3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567546.759 5937997.351</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938061.568</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0009e672-4509-41d6-9da2-09c36daa0e72" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:laermkontingent />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1bc59ddd-36f3-4899-92a1-b606e130d846">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567582.824 5938061.568 567560.946 5938042.334 567560.757 5938039.186 567546.759 5938038.469 567551.013 5938033.620 567582.824 5937997.351 567582.824 5938061.568 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:besondereArtDerBaulNutzung>2100</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_b9746773-0461-4b84-abca-2190911c06dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567495.689 5938005.234</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938088.189</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_42241834-6cf5-41ba-8ada-c33586419fa5">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">567503.171 5938005.234 567542.883 5938038.270 567546.759 5938038.469 567560.757 5938039.186 567560.946 5938042.334 567582.824 5938061.568 567582.824 5938088.189 567579.665 5938085.411 567495.689 5938014.229 567503.171 5938005.234 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_7b7c86c2-4f93-44ed-b838-c3a8be12af71">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567298.576 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567362.511 5937980.200</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e1daa8e7-ecc6-4416-8f96-0926bfcf0f57">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567310.846 5937959.935 567298.576 5937951.758 567362.511 5937951.758 567350.869 5937980.200 567323.815 5937967.212 567310.846 5937959.935 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_d7b0f8ed-6f59-4f38-979e-bbed137cfd24">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567074.238 5938238.135</gml:lowerCorner>
          <gml:upperCorner>567152.202 5938313.036</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_5b1a8a86-8977-4aac-809c-e4fffa6481ed">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567074.238 5938313.036 567152.202 5938238.135 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_f06e6c1b-7c5e-44ce-ad88-d7c55dc46d14">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567028.067 5938232.097</gml:lowerCorner>
          <gml:upperCorner>567089.213 5938291.947</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_844fcfbe-4063-4a61-a940-1ea10f1f431a">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_7d9dee1a-d89d-4352-8c81-0b8bcfaa358d">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567088.029 5938265.626 567089.209 5938268.663 567088.268 5938271.782 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567088.268 5938271.782 567071.086 5938290.111 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567071.086 5938290.111 567067.491 5938291.939 567063.716 5938290.519 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567063.716 5938290.519 567029.288 5938258.492 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567029.288 5938258.492 567028.069 5938255.075 567029.481 5938251.734 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567029.481 5938251.734 567046.505 5938233.488 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567046.505 5938233.488 567049.954 5938232.097 567053.409 5938233.471 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567053.409 5938233.471 567088.029 5938265.626 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>16000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_234f2be4-f17c-4d90-8f7c-9869b3105a33">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567318.021 5938023.447</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938328.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d53616e1-20f2-48df-a992-89b0e574b6a9">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="13">567371.738 5938023.447 567582.824 5938193.639 567582.824 5938324.011 567557.061 5938328.505 567318.021 5938128.086 567320.605 5938122.124 567324.186 5938112.291 567330.820 5938111.102 567336.035 5938098.606 567338.041 5938093.800 567348.773 5938068.084 567366.892 5938024.676 567371.738 5938023.447 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_5a8e0bd9-c528-406a-993b-e4a573a16017">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5938258.262</gml:lowerCorner>
          <gml:upperCorner>567102.280 5938389.110</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e96936cf-2271-4af4-ae6b-c0c2382100c2" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_dd5b7f93-316a-4fb8-b36d-c3bf65953748">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_695d37ea-095d-4339-a0e9-4cca42276a4b">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">567099.549 5938336.764 567102.280 5938339.324 567101.343 5938339.886 567084.629 5938349.895 567049.950 5938370.659 567025.354 5938385.384 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567025.354 5938385.384 567024.676 5938385.749 567023.968 5938386.051 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">567023.968 5938386.051 567015.804 5938389.110 567015.804 5938258.262 567038.136 5938279.196 567074.238 5938313.036 567099.549 5938336.764 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_fe0ae921-3ed8-47e7-a248-2ae10edfde1f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567130.305 5938301.592</gml:lowerCorner>
          <gml:upperCorner>567227.946 5938375.141</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_0f0c620f-d9e2-406f-99a2-aecab58b4951">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_b12a70f8-575c-4342-8cb5-b2dfa19c645c">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="8">567223.207 5938358.717 567221.900 5938371.886 567218.109 5938375.141 567209.304 5938374.674 567197.028 5938374.021 567181.803 5938373.211 567161.770 5938372.146 567135.933 5938370.772 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="11">567135.933 5938370.772 567130.500 5938365.410 567132.940 5938358.178 567143.469 5938351.930 567153.798 5938345.358 567162.558 5938339.413 567171.155 5938333.235 567177.298 5938328.595 567183.350 5938323.836 567196.276 5938312.987 567208.723 5938301.592 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="5">567208.723 5938301.592 567227.946 5938310.962 567227.773 5938312.707 567224.469 5938345.992 567223.207 5938358.717 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_45321a90-08fe-4932-a184-10b7f15d282f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567093.783 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567139.114 5937979.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_905e0637-58fc-44e0-b485-abb9d113aa8e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567139.114 5937967.016 567135.695 5937971.220 567130.341 5937979.173 567093.783 5937951.758 567118.771 5937951.758 567139.114 5937967.016 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_30ccabd6-1c88-4911-a5e0-83d229aefdd6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567154.964 5938100.700</gml:lowerCorner>
          <gml:upperCorner>567242.466 5938217.431</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_401eb1a3-2a72-471a-9776-3a9d0b5355cf">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567154.964 5938217.431 567242.466 5938100.700 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:zweckbestimmung>1300</xplan:zweckbestimmung>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_21205338-39ee-4ed0-bce7-708290061eb5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567262.348 5938190.312</gml:lowerCorner>
          <gml:upperCorner>567443.921 5938342.195</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_99a87f6d-9ca7-4a11-af2d-16c1afd84c32" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:laermkontingent />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b858d435-28a9-49db-99ff-9743d9b06823">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="15">567357.809 5938342.195 567340.541 5938275.663 567262.348 5938267.909 567263.576 5938255.575 567285.485 5938215.452 567293.062 5938201.575 567297.762 5938190.312 567314.754 5938201.254 567354.153 5938224.982 567378.607 5938207.581 567443.921 5938260.380 567435.323 5938272.878 567429.671 5938323.543 567412.960 5938327.880 567357.809 5938342.195 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:besondereArtDerBaulNutzung>2100</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_WaldFlaeche gml:id="GML_1245a1fa-8f64-4a47-9e6b-35cf7fa53ae1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567085.525 5938043.964</gml:lowerCorner>
          <gml:upperCorner>567286.671 5938255.114</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a532e8c0-b690-4791-b304-6ef2e976f596">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_a05ddb64-1527-4c10-a4e3-5337d89914b3">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="9">567278.830 5938152.624 567270.308 5938171.230 567260.538 5938189.213 567258.312 5938192.935 567256.032 5938196.625 567256.005 5938196.668 567255.979 5938196.710 567241.031 5938217.362 567223.519 5938235.890 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="10">567223.519 5938235.890 567220.892 5938238.284 567202.086 5938255.114 567195.090 5938255.032 567114.837 5938179.831 567085.525 5938152.358 567166.778 5938043.964 567169.066 5938045.679 567286.671 5938133.836 567278.830 5938152.624 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:eigentumsart>2000</xplan:eigentumsart>
    </xplan:BP_WaldFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TechnischeMassnahmenFlaeche gml:id="GML_9ae668e9-5b2b-4fd0-a15f-01de34b81473">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567237.045 5937990.158</gml:lowerCorner>
          <gml:upperCorner>567284.670 5938040.245</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_615b254b-8624-4e07-9e6d-6678e04533ec">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567237.045 5938024.163 567262.546 5937990.158 567284.670 5938005.562 567258.511 5938040.245 567237.045 5938024.163 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_TechnischeMassnahmenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_599f5ba0-0938-4c55-a193-97bf6d53c0c4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567237.045 5937990.158</gml:lowerCorner>
          <gml:upperCorner>567284.670 5938040.245</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_36b30dcd-2d84-4cad-94fa-d6e02c711b16" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:laermkontingent />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_732b0f99-c245-4163-ae21-9eb69dd449b7">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567237.045 5938024.163 567262.546 5937990.158 567284.670 5938005.562 567258.511 5938040.245 567237.045 5938024.163 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:besondereArtDerBaulNutzung>1700</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_06878e37-9e44-4961-92c7-fbdd1cf1e6d0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567194.220 5938401.379</gml:lowerCorner>
          <gml:upperCorner>567249.113 5938562.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ae97dd24-29e6-4463-84d5-14f10a475eac">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_82a68f84-2b7f-4e1f-8717-bd3eae6bdd9b">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="20">567244.460 5938448.206 567243.578 5938457.078 567243.302 5938459.858 567240.344 5938489.645 567239.212 5938501.043 567238.391 5938509.312 567237.357 5938519.723 567234.583 5938547.676 567233.137 5938562.271 567197.979 5938562.271 567199.246 5938549.475 567200.034 5938541.518 567202.243 5938519.259 567204.972 5938491.786 567194.220 5938490.843 567194.626 5938483.469 567195.435 5938483.514 567195.572 5938479.907 567200.476 5938480.203 567201.654 5938480.274 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567201.654 5938480.274 567203.259 5938478.982 567201.970 5938477.375 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567201.970 5938477.375 567202.511 5938468.656 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567202.511 5938468.656 567203.979 5938467.316 567202.688 5938465.804 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="14">567202.688 5938465.804 567201.395 5938465.696 567197.906 5938465.403 567198.648 5938455.527 567208.539 5938455.880 567208.638 5938454.886 567210.933 5938431.793 567213.246 5938408.509 567209.991 5938404.716 567249.113 5938401.379 567246.024 5938432.461 567245.668 5938436.045 567244.926 5938443.511 567244.460 5938448.206 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_58314478-4223-4d9f-abe9-569150740f5c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5938447.074</gml:lowerCorner>
          <gml:upperCorner>567203.982 5938490.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ab518d89-bd8b-4d1d-a3bb-814fa36efb56">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_322aa12e-d0ac-49d6-adc6-8f754bf26ac5">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567201.395 5938465.696 567202.688 5938465.804 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567202.688 5938465.804 567203.979 5938467.316 567202.511 5938468.656 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567202.511 5938468.656 567201.970 5938477.375 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567201.970 5938477.375 567203.259 5938478.982 567201.654 5938480.274 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="17">567201.654 5938480.274 567200.476 5938480.203 567195.572 5938479.907 567195.435 5938483.514 567194.626 5938483.469 567194.220 5938490.843 567172.986 5938488.982 567110.510 5938483.516 567070.634 5938480.460 567015.804 5938476.821 567015.804 5938447.074 567038.071 5938448.442 567091.844 5938451.744 567195.861 5938455.428 567198.648 5938455.527 567197.906 5938465.403 567201.395 5938465.696 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>2000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_082e2ebd-438e-4adb-b72f-2931152707fe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567436.133 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567547.650 5938068.518</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5da8d081-d16e-4b12-b884-0ab4805c6a18">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">567547.650 5937951.758 567503.171 5938005.234 567495.689 5938014.229 567454.212 5938064.089 567454.705 5938068.518 567436.133 5938053.544 567441.231 5938053.131 567477.970 5938008.959 567525.548 5937951.758 567547.650 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_7d67f197-d2a6-46bb-8246-8152ab098cd7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567081.813 5938308.143</gml:lowerCorner>
          <gml:upperCorner>567533.742 5938404.716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a2e6b33c-b27e-4611-b913-3b8722abd655">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_dd2c460e-4b9e-4362-bbc2-66cb2f51f0c9">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">567319.931 5938383.002 567283.555 5938392.442 567249.113 5938401.379 567209.991 5938404.716 567201.698 5938404.258 567193.214 5938403.789 567173.766 5938402.715 567153.849 5938401.670 567133.878 5938400.621 567113.801 5938399.567 567081.813 5938397.885 567132.940 5938358.178 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567132.940 5938358.178 567130.500 5938365.410 567135.933 5938370.772 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="23">567135.933 5938370.772 567161.770 5938372.146 567181.803 5938373.211 567197.028 5938374.021 567209.304 5938374.674 567218.109 5938375.141 567260.728 5938367.386 567274.604 5938363.786 567307.497 5938355.249 567357.809 5938342.195 567412.960 5938327.880 567429.671 5938323.543 567455.467 5938316.850 567497.150 5938309.214 567502.998 5938308.143 567533.742 5938332.999 567526.224 5938334.376 567477.939 5938343.220 567463.902 5938345.791 567429.069 5938354.679 567392.691 5938364.120 567356.310 5938373.562 567319.931 5938383.002 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_ea776d6f-1f0a-4f00-9fe1-cfdb574de904">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567253.134 5938267.909</gml:lowerCorner>
          <gml:upperCorner>567357.809 5938367.386</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1af29368-7422-4575-af98-6681f7f61f64" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_151402d9-30cc-4c58-838e-3bcc54c787c9" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5bedcc47-ace3-4388-8b5e-7095017a6e3b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">567340.541 5938275.663 567357.809 5938342.195 567307.497 5938355.249 567274.604 5938363.786 567260.728 5938367.386 567253.134 5938360.885 567253.278 5938359.430 567255.054 5938341.507 567256.632 5938325.593 567258.208 5938309.682 567262.348 5938267.909 567340.541 5938275.663 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>16000</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
      <xplan:detaillierteZweckbestimmung>Kindergarten</xplan:detaillierteZweckbestimmung>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_LandwirtschaftsFlaeche gml:id="GML_d07972fb-779d-40de-8e12-520c6368a999">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567113.291 5938111.566</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_357054b1-3bfd-4739-bcc4-1a9c2b7a4a75">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567027.629 5938098.101 567015.804 5938087.012 567015.804 5937951.758 567043.811 5937951.758 567096.840 5937991.529 567113.291 5938003.864 567041.996 5938111.566 567027.629 5938098.101 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_LandwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_bf2606a7-3204-46d1-b709-f780a87d5ff3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567118.771 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567313.145 5938091.571</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Neue Straßenverkehrsfläche</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fdcabb7f-2c80-4333-b84c-bf859280a32e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="15">567150.447 5937953.041 567186.123 5937979.758 567184.803 5937981.517 567183.123 5937983.755 567204.979 5938000.135 567237.045 5938024.163 567258.511 5938040.245 567281.705 5938057.637 567313.145 5938070.396 567309.706 5938078.637 567305.292 5938091.571 567139.114 5937967.016 567118.771 5937951.758 567148.735 5937951.758 567150.447 5937953.041 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_fd31dc77-0ace-44db-a0d9-1e3b9f4e6ee0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567281.705 5938045.583</gml:lowerCorner>
          <gml:upperCorner>567318.553 5938070.396</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>4000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ed007be7-ca1f-43b3-bbd9-591fcfee257c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567281.705 5938057.637 567290.138 5938045.583 567318.553 5938057.440 567313.145 5938070.396 567281.705 5938057.637 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_WasserwirtschaftsFlaeche gml:id="GML_9d1872c2-3bbb-4e26-acd1-0e0e7a178ae7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567267.936 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567350.869 5938001.617</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Regelung des Wasserabflusses</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f59b6dc7-2fb3-4366-b992-b2d99b38319c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567298.576 5937951.758 567310.846 5937959.935 567323.815 5937967.212 567350.869 5937980.200 567341.849 5938001.617 567267.936 5937951.758 567298.576 5937951.758 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_WasserwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_b5bf4faa-5bf0-4246-bfcb-6162377c255e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5938110.310</gml:lowerCorner>
          <gml:upperCorner>567188.257 5938339.324</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d241b5a0-b721-416b-afd4-bfd4c93e8b93">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_fc2cf1e1-6fd7-4b81-903b-53f4a9d876c6">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567188.204 5938271.868 567188.257 5938276.866 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">567188.257 5938276.866 567180.930 5938283.765 567173.413 5938290.456 567149.792 5938309.211 567124.676 5938325.910 567123.886 5938326.388 567123.095 5938326.863 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="14">567123.095 5938326.863 567116.526 5938330.794 567115.663 5938331.311 567102.280 5938339.324 567099.549 5938336.764 567074.238 5938313.036 567038.136 5938279.196 567015.804 5938258.262 567015.804 5938110.310 567028.204 5938121.934 567031.044 5938124.596 567081.763 5938172.125 567152.202 5938238.135 567188.204 5938271.868 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_e1180422-7b92-4018-9710-6f2ad7768994">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567053.409 5938233.471 567049.954 5938232.097 567046.505 5938233.488 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567046.505 5938233.488 567029.481 5938251.734 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567029.481 5938251.734 567028.069 5938255.075 567029.288 5938258.492 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567029.288 5938258.492 567063.716 5938290.519 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567063.716 5938290.519 567067.491 5938291.939 567071.086 5938290.111 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567071.086 5938290.111 567088.268 5938271.782 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567088.268 5938271.782 567089.209 5938268.663 567088.029 5938265.626 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567088.029 5938265.626 567053.409 5938233.471 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_bdf97adf-d8a9-4dae-8362-a09a73a51fa0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567081.813 5938358.178</gml:lowerCorner>
          <gml:upperCorner>567218.109 5938404.716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1a2fe21c-2047-479b-88da-fe010e4d922b">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_33ec2fe4-ddd5-4676-af33-1005873382ab">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="9">567209.991 5938404.716 567201.698 5938404.258 567193.214 5938403.789 567173.766 5938402.715 567153.849 5938401.670 567133.878 5938400.621 567113.801 5938399.567 567081.813 5938397.885 567132.940 5938358.178 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567132.940 5938358.178 567130.500 5938365.410 567135.933 5938370.772 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="7">567135.933 5938370.772 567161.770 5938372.146 567181.803 5938373.211 567197.028 5938374.021 567209.304 5938374.674 567218.109 5938375.141 567209.991 5938404.716 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_e3cf8a7d-ebe9-4e50-abff-2ccb4ee3d954">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.804 5938087.012</gml:lowerCorner>
          <gml:upperCorner>567202.086 5938276.866</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1bef9b95-70a1-42e9-989d-739dd6558770">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="18">567114.837 5938179.831 567195.090 5938255.032 567202.086 5938255.114 567188.257 5938276.866 567188.204 5938271.868 567152.202 5938238.135 567081.763 5938172.125 567031.044 5938124.596 567028.204 5938121.934 567015.804 5938110.310 567015.804 5938087.012 567027.629 5938098.101 567041.996 5938111.566 567054.889 5938123.649 567060.870 5938129.255 567062.933 5938131.188 567085.525 5938152.358 567114.837 5938179.831 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:istNatuerlichesUberschwemmungsgebiet>false</xplan:istNatuerlichesUberschwemmungsgebiet>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_3110388e-f0dc-4b54-a941-dda61ba75c3c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567571.395 5938149.584</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938171.809</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_567a5ea1-6fec-427e-bdca-30c483125576">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567582.824 5938171.809 567571.395 5938162.594 567582.824 5938149.584 567582.824 5938171.809 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_d99b1ba4-8a29-4cc5-8ca3-e99f0767adcf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567240.344 5938432.133</gml:lowerCorner>
          <gml:upperCorner>567510.320 5938489.645</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2fc85fc4-fd3f-4f7b-bf15-d62f96153f1f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="23">567410.755 5938466.485 567354.821 5938477.032 567323.173 5938482.997 567298.320 5938485.759 567284.812 5938486.665 567240.344 5938489.645 567243.302 5938459.858 567250.619 5938459.935 567272.062 5938460.161 567321.813 5938460.687 567420.143 5938449.083 567488.888 5938432.133 567488.981 5938432.694 567492.012 5938432.190 567502.333 5938437.043 567503.231 5938436.893 567510.124 5938443.697 567510.320 5938444.871 567509.422 5938445.019 567510.055 5938450.025 567508.280 5938451.863 567502.824 5938449.130 567410.755 5938466.485 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>10003</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567284.812 5938466.485</gml:lowerCorner>
          <gml:upperCorner>567416.500 5938562.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">25</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2611961f-cc1d-46ce-b049-a48b5f679ba4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_28bc8afe-e15d-4e75-85d8-224c50a4af0e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_da614353-3e14-4548-a602-1dca72637536" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9bc001e7-69c3-4f5e-8146-97955e8d01f1" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_357b6a20-d631-478f-9b7d-76bf73a9cb48">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">567416.500 5938562.271 567292.029 5938562.271 567287.646 5938516.351 567286.676 5938506.178 567285.897 5938498.014 567284.812 5938486.665 567298.320 5938485.759 567323.173 5938482.997 567354.821 5938477.032 567410.755 5938466.485 567412.642 5938497.933 567416.500 5938562.271 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:dachform>3900</xplan:dachform>
      <xplan:dachform>1000</xplan:dachform>
      <xplan:dachform>3400</xplan:dachform>
      <xplan:GFZmin>1.2</xplan:GFZmin>
      <xplan:GFZmax>1.4</xplan:GFZmax>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_a901293d-3d84-462e-98f3-e29fae2ed376">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567495.689 5938005.234</gml:lowerCorner>
          <gml:upperCorner>567582.824 5938088.189</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_60c94ceb-7326-409f-a019-19b030d46517">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">567503.171 5938005.234 567542.883 5938038.270 567546.759 5938038.469 567560.757 5938039.186 567560.946 5938042.334 567582.824 5938061.568 567582.824 5938088.189 567579.665 5938085.411 567495.689 5938014.229 567503.171 5938005.234 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1800</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Strassenverkehrsrecht gml:id="GML_c43ed4b4-0754-4ac9-a1cc-35bd57fa6d9c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567043.811 5937951.758</gml:lowerCorner>
          <gml:upperCorner>567303.316 5938137.361</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Festgestellte Bundesfernstraße</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_08ab9fc7-e5f4-4cfc-b7cf-bece522f5b00">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="21">567302.934 5938108.546 567303.316 5938108.832 567302.289 5938111.330 567302.299 5938111.449 567302.566 5938111.561 567292.680 5938135.153 567292.537 5938135.094 567292.355 5938135.019 567292.321 5938135.101 567291.371 5938137.361 567286.671 5938133.836 567169.066 5938045.679 567166.778 5938043.964 567152.069 5938032.938 567135.639 5938020.620 567113.291 5938003.864 567096.840 5937991.529 567043.811 5937951.758 567093.783 5937951.758 567130.341 5937979.173 567302.934 5938108.546 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Strassenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05ed645d-d470-405a-a390-e2683c21bf4e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567093.018 5938531.973</gml:lowerCorner>
          <gml:upperCorner>567094.018 5938532.973</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_547646b8-77bd-4ee1-a6e0-46dd95752321">
          <gml:pos>567093.018 5938531.973</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_24f81e5a-5f25-4070-b617-e9d8f3073680">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567122.851 5938435.930</gml:lowerCorner>
          <gml:upperCorner>567123.851 5938436.930</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_47b3f294-2856-4aef-8da2-dbeb0aed53c4">
          <gml:pos>567122.851 5938435.930</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_64200842-66f5-429b-9963-0e83d2e75a1f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567451.670 5937968.111</gml:lowerCorner>
          <gml:upperCorner>567452.670 5937968.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f7b713f4-b5c3-4dd2-bd72-6d3dbaf5f2da" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dfe2215a-3233-48a0-b300-974975706957">
          <gml:pos>567451.670 5937968.111</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7f7068c8-bc67-4e27-a516-6ff1c2f8e9f7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.973 5938425.134</gml:lowerCorner>
          <gml:upperCorner>567093.973 5938425.134</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_01807fca-716c-48ce-9181-16721623c378">
          <gml:pos>567092.973 5938425.134</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e64e9539-4b62-40b6-b5fc-71d0085bbded">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567186.519 5938515.159</gml:lowerCorner>
          <gml:upperCorner>567187.519 5938515.159</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_053c8ad1-201f-4ee3-8a16-04f948c0142a">
          <gml:pos>567186.519 5938515.159</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c3c38f42-6c38-4ab1-822f-4f345f7aa2e2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.865 5938513.963</gml:lowerCorner>
          <gml:upperCorner>567196.865 5938514.963</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_bc53005e-5591-402e-9fde-2510a6064a8f">
          <gml:pos>567195.865 5938513.963</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_310624ef-717f-49b0-940d-65baa00d56e1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567407.188 5938390.754</gml:lowerCorner>
          <gml:upperCorner>567408.188 5938390.754</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_285c0249-a226-4329-8adb-f0f344309ebd" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0df4fe02-a9e3-400e-8f08-c9c51312cdbb">
          <gml:pos>567407.188 5938390.754</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7c0d896e-0f26-40e2-b0db-41ae4312b1ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567081.105 5938516.868</gml:lowerCorner>
          <gml:upperCorner>567082.105 5938517.868</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bebauungsArt</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_735533f3-90bb-4209-af2e-6cdf1d2ec564">
          <gml:pos>567081.105 5938516.868</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3bfb88ba-1a31-45d6-b971-715d3406aced">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567251.340 5938526.007</gml:lowerCorner>
          <gml:upperCorner>567252.340 5938527.007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GR_Ausn</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_8a3b0bec-89c1-407d-8a9e-ff1e9aab15b1">
          <gml:pos>567251.340 5938526.007</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_1af29368-7422-4575-af98-6681f7f61f64">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567288.480 5938297.957</gml:lowerCorner>
          <gml:upperCorner>567289.480 5938298.957</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>detaillierteZweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ea776d6f-1f0a-4f00-9fe1-cfdb574de904" />
      <xplan:schriftinhalt>Kindergarten</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_25b0c495-6122-476a-8ef9-139b6c83b3de">
          <gml:pos>567288.480 5938297.957</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_781d3793-ab82-441f-ab28-da69a48aad26">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567154.208 5938429.086</gml:lowerCorner>
          <gml:upperCorner>567155.208 5938430.086</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>DN</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_5033e111-a8ae-41ec-bba1-60efe93a93dd">
          <gml:pos>567154.208 5938429.086</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1c64bb25-3c80-4f35-b892-abbf124e1401">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567260.991 5938494.139</gml:lowerCorner>
          <gml:upperCorner>567261.991 5938495.139</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>MaxZahlWohnungen</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_9f9a2e77-26f0-4bb7-afbf-7dbd95548f47">
          <gml:pos>567260.991 5938494.139</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2611961f-cc1d-46ce-b049-a48b5f679ba4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567349.706 5938525.209</gml:lowerCorner>
          <gml:upperCorner>567350.706 5938526.209</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_cbbf07da-fa1f-4fb3-a343-2977c4798692">
          <gml:pos>567349.706 5938525.209</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0009e672-4509-41d6-9da2-09c36daa0e72">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567571.237 5938028.119</gml:lowerCorner>
          <gml:upperCorner>567572.237 5938028.119</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_74b20dc5-b262-4ed7-8524-7bf1a48c0cf3" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c4581d9a-2451-49fc-9035-acf11ae96a80">
          <gml:pos>567571.237 5938028.119</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_99a87f6d-9ca7-4a11-af2d-16c1afd84c32">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.356 5938246.738</gml:lowerCorner>
          <gml:upperCorner>567348.356 5938247.738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_21205338-39ee-4ed0-bce7-708290061eb5" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_de351d5f-3c7c-46eb-acd2-796e45d1ae62">
          <gml:pos>567347.356 5938246.738</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_522f5f21-778f-469d-af6a-9a06eba37c6e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567554.810 5937986.880</gml:lowerCorner>
          <gml:upperCorner>567555.810 5937986.880</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fe7cf17d-0c8b-40c6-99d3-5b6a9ade911a" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b0d84820-8e20-4309-9fe4-ccc22c22d1ce">
          <gml:pos>567554.810 5937986.880</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_151402d9-30cc-4c58-838e-3bcc54c787c9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567297.126 5938320.777</gml:lowerCorner>
          <gml:upperCorner>567298.126 5938321.777</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ea776d6f-1f0a-4f00-9fe1-cfdb574de904" />
      <xplan:schriftinhalt>Schule</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_81bbe67c-d3a9-40eb-b450-7654ebb09bc7">
          <gml:pos>567297.126 5938320.777</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3510055f-d1f0-44c1-b29d-98ccf18e970a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567329.738 5938410.087</gml:lowerCorner>
          <gml:upperCorner>567330.738 5938410.087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dc60c672-b6ca-442c-b761-f56982073dd5" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e0765d30-aabf-4d2c-b49e-d821ddb4a839">
          <gml:pos>567329.738 5938410.087</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_592a57e2-7c92-49c8-8273-fb49ff7c662e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567464.806 5938417.986</gml:lowerCorner>
          <gml:upperCorner>567465.806 5938417.986</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_611be92c-f6e2-48c5-99a9-563cc1f00241" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_efb06e83-0674-405c-89d4-bf1d84d40b5a">
          <gml:pos>567464.806 5938417.986</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_47750462-dbee-4124-a6a2-3f24446b54ee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567073.445 5938532.806</gml:lowerCorner>
          <gml:upperCorner>567074.445 5938533.806</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3a10241e-0f87-4a5a-96fc-1df5d403b11b">
          <gml:pos>567073.445 5938532.806</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8499585b-e9b2-4c34-8fc0-9f69407dc54b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567485.791 5937976.109</gml:lowerCorner>
          <gml:upperCorner>567486.791 5937976.109</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_df5f8049-6e74-4625-9771-3816a8cb488e" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d2bb0f65-47a3-4e20-a49d-95472189bcc6">
          <gml:pos>567485.791 5937976.109</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_28bc8afe-e15d-4e75-85d8-224c50a4af0e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567348.917 5938536.815</gml:lowerCorner>
          <gml:upperCorner>567349.917 5938537.815</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>dachform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d16b7288-741e-467c-8a57-389caf82fe42">
          <gml:pos>567348.917 5938536.815</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b36639b0-cc62-49d8-a553-52da27715c5f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.527 5938501.594</gml:lowerCorner>
          <gml:upperCorner>567076.527 5938502.594</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GFZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dfccfae8-82ca-4deb-aec6-8635687969f9">
          <gml:pos>567075.527 5938501.594</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8eab63a2-fb13-408c-ac41-bbb194b2faae">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567184.874 5938507.164</gml:lowerCorner>
          <gml:upperCorner>567185.874 5938508.164</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GFZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_4daa9089-7d9b-48bf-8040-6e235b02730d">
          <gml:pos>567184.874 5938507.164</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9333fe15-dd95-4aff-aebe-c37a48e4d2ec">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567192.463 5938514.747</gml:lowerCorner>
          <gml:upperCorner>567193.463 5938515.747</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2b88e503-88ff-49e0-b61a-cb1f5d8ab3f4">
          <gml:pos>567192.463 5938514.747</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_848f0251-7f04-451e-a5c3-5b849761fe28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567476.153 5938500.271</gml:lowerCorner>
          <gml:upperCorner>567477.153 5938501.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c48a8916-a329-4c98-9938-78cb5dee9e42">
          <gml:pos>567476.153 5938500.271</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_63eda789-900b-4f83-a52a-e2d7d0cd0faf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567540.598 5938399.674</gml:lowerCorner>
          <gml:upperCorner>567541.598 5938399.674</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_62517d2f-46c3-4130-b39e-1ed9b30da567" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d7954e34-c9b3-4039-8a66-95f32436859e">
          <gml:pos>567540.598 5938399.674</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ee9a801b-dc41-406b-b406-16f09b4f0499">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567087.604 5938534.054</gml:lowerCorner>
          <gml:upperCorner>567088.604 5938535.054</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_358f9e1c-039e-4128-8636-c02ade8ea83e">
          <gml:pos>567087.604 5938534.054</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e96936cf-2271-4af4-ae6b-c0c2382100c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567026.000 5938332.000</gml:lowerCorner>
          <gml:upperCorner>567027.000 5938333.000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_5a8e0bd9-c528-406a-993b-e4a573a16017" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7ac32eac-ccda-483a-a8f7-9cc907e250d0">
          <gml:pos>567026.000 5938332.000</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f5237d5a-b68d-4a1c-8d3c-2c4daa707588">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567183.304 5938533.575</gml:lowerCorner>
          <gml:upperCorner>567184.304 5938534.575</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_42288296-23d8-4a39-b42c-be79b068bdca">
          <gml:pos>567183.304 5938533.575</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8e3c5fa1-0b8a-4a21-9fea-50e94d65f899">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567494.674 5938506.950</gml:lowerCorner>
          <gml:upperCorner>567495.674 5938506.950</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_cf930979-28dc-4ed0-93dd-d4be0c24875a">
          <gml:pos>567494.674 5938506.950</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3631c68f-2533-434a-83b5-a6609699f79b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567099.338 5938423.519</gml:lowerCorner>
          <gml:upperCorner>567100.338 5938424.519</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_4359728b-3ecf-449d-8a36-b898ae51fb20">
          <gml:pos>567099.338 5938423.519</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_308d9453-aa40-4043-8682-5ed5a84ff118">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567443.139 5938291.487</gml:lowerCorner>
          <gml:upperCorner>567444.139 5938292.487</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dea227da-2b90-49a9-a4c9-d7d5cb4a5c23" />
      <xplan:schriftinhalt>Blockheizkraftwerk</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_1ef71d50-8596-4f11-80ba-cb37785f6fa5">
          <gml:pos>567443.139 5938291.487</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6396f67c-7150-44f2-9af9-7ea5e03d8f0b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567480.922 5938491.172</gml:lowerCorner>
          <gml:upperCorner>567481.922 5938492.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GF</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_30956564-b989-4f7c-80d6-2745cc311611">
          <gml:pos>567480.922 5938491.172</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_da614353-3e14-4548-a602-1dca72637536">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.067 5938509.380</gml:lowerCorner>
          <gml:upperCorner>567348.067 5938510.380</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_79cde361-94b5-4e95-87bf-4cd1ee205525">
          <gml:pos>567347.067 5938509.380</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9bc001e7-69c3-4f5e-8146-97955e8d01f1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567349.442 5938517.558</gml:lowerCorner>
          <gml:upperCorner>567350.442 5938518.558</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GFZmax</xplan:art>
      <xplan:art>GFZmin</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_03f69510-4fb6-4c32-88ff-31b7d15dfed1">
          <gml:pos>567349.442 5938517.558</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c4bf1948-787e-4a71-b588-8d2091868fc9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567123.282 5938425.597</gml:lowerCorner>
          <gml:upperCorner>567124.282 5938426.597</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d77fc67e-405a-4efd-b0a2-ce5cec452e33">
          <gml:pos>567123.282 5938425.597</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1b8d0589-6bf4-40fb-a071-e38c0e061914">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567406.000 5938379.000</gml:lowerCorner>
          <gml:upperCorner>567407.000 5938380.000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_285c0249-a226-4329-8adb-f0f344309ebd" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a4bc3ba8-6539-443e-a179-e9a2b448ebaa">
          <gml:pos>567406.000 5938379.000</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1f6eeb5a-8eb4-40de-b1a7-ff94cbc8b5ee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567187.229 5938528.345</gml:lowerCorner>
          <gml:upperCorner>567188.229 5938529.345</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ_Ausn</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a784e9e6-f988-4a3f-a0e0-6010a52cdd94">
          <gml:pos>567187.229 5938528.345</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1cbff272-89c7-412f-bb31-8ddc38544954">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567188.799 5938498.534</gml:lowerCorner>
          <gml:upperCorner>567189.799 5938499.534</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bebauungsArt</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ec600642-fd8c-4f93-80d6-80988746850e">
          <gml:pos>567188.799 5938498.534</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4470427b-a9ad-4576-8aa4-d9822d4a12c6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567121.558 5938414.403</gml:lowerCorner>
          <gml:upperCorner>567122.558 5938415.403</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>MaxZahlWohnungen</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_fc7b213f-2197-4f5e-a3ee-8b74467363b3">
          <gml:pos>567121.558 5938414.403</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_691d25f5-2077-4da5-86f0-d3016d33cdfa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567250.081 5938513.428</gml:lowerCorner>
          <gml:upperCorner>567251.081 5938514.428</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zmax</xplan:art>
      <xplan:art>Zmin</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_271c5afd-b536-4e27-b414-eaa76a85dbeb">
          <gml:pos>567250.081 5938513.428</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cd84ba26-68f2-45ab-9903-df772de1b93f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567329.000 5938401.000</gml:lowerCorner>
          <gml:upperCorner>567330.000 5938402.000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dc60c672-b6ca-442c-b761-f56982073dd5" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_86325012-4ef7-4abb-9dc7-1b4a9e7bda4f">
          <gml:pos>567329.000 5938401.000</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e6a04548-9ba4-4b9d-bc5b-0d5a6977fe87">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567244.450 5938530.974</gml:lowerCorner>
          <gml:upperCorner>567245.450 5938531.974</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GR</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_439ee84d-0279-486d-9723-8506ae0ca866">
          <gml:pos>567244.450 5938530.974</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_36b30dcd-2d84-4cad-94fa-d6e02c711b16">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567265.326 5938008.872</gml:lowerCorner>
          <gml:upperCorner>567266.326 5938008.872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_599f5ba0-0938-4c55-a193-97bf6d53c0c4" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_745ca346-fea4-43ba-bd61-4fff8e68da9b">
          <gml:pos>567265.326 5938008.872</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e9659569-957f-416a-8c7b-346be20371f8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567157.317 5938434.638</gml:lowerCorner>
          <gml:upperCorner>567158.317 5938435.638</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>dachform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_473b8cf7-e68e-490c-a797-74bbad53ad36">
          <gml:pos>567157.317 5938434.638</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_626ddd65-14ee-4532-9d7c-0cdd20fb4a29">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567257.522 5938538.278</gml:lowerCorner>
          <gml:upperCorner>567258.522 5938539.278</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_53ec58f3-8475-42ee-afa4-751351fe8f16">
          <gml:pos>567257.522 5938538.278</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1c12ef22-0e3e-4a95-a9d9-75f8669b1f59">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.527 5938508.252</gml:lowerCorner>
          <gml:upperCorner>567076.527 5938509.252</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3d91b07e-3e1e-4d5a-9880-bdc56d1b7c8d">
          <gml:pos>567075.527 5938508.252</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1e7dd689-8623-4bd0-b4e4-b03299e77ac0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567296.797 5937995.194</gml:lowerCorner>
          <gml:upperCorner>567297.797 5937996.194</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bdb12f92-c818-4c99-91da-14c3a319192c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e9e0e595-2a25-4c92-a40a-bb5555b580b8">
          <gml:pos>567296.797 5937995.194</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
</xplan:XPlanAuszug>