﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<!--Erstellt von WS LANDCAD am 24.03.2021-->
<xplan:XPlanAuszug xmlns:xplan="http://www.xplanung.de/xplangml/5/2" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" gml:id="GML_647da418-d34b-4809-8cf4-aca6350f9f33">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>576416.125 5942410.130</gml:lowerCorner>
      <gml:upperCorner>576662.680 5942588.473</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_1c23cbae-b6e9-4f21-b9c2-f3fc6df46545">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.125 5942410.130</gml:lowerCorner>
          <gml:upperCorner>576662.680 5942588.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan001_5-2</xplan:name>
      <xplan:beschreibung>Der vorhabenbezogene Bebauungsplan BPlan001_5-2 für das Gebiet südlich Schierenberg, westlich Saseler Straße (Bezirk Wandsbek, Ortsteil 526) wird festgestellt. Das Plangebiet wird wie folgt begrenzt: Schierenberg – Saseler Straße – Südostgrenze des Flurstücks 129, über das Flur-stück 5687, Südgrenzen der Flurstücke 5687 und 5684, Ostgrenzen der Flurstücke 4402 und 4403, über die Flurstücke 4403 und 4401 und Westgrenzen der Flurstücke 4401 und 4400 der Gemarkung Meiendorf.</xplan:beschreibung>
      <xplan:technHerstellDatum>2016-07-21</xplan:technHerstellDatum>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface srsName="EPSG:25832" gml:id="GML_1288eb16-2c0d-46d0-91d2-0924f31ccee1">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25832" gml:id="GML_6a6755d6-7ff1-460f-9751-1079e38dacf3">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsDimension="2" count="27">576480.297 5942411.883 576480.165 5942424.848 576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038 576516.148 5942426.171 576516.545 5942413.603 576593.045 5942416.370 576600.834 5942416.652 576599.891 5942440.018 576598.756 5942468.151 576611.810 5942468.677 576626.513 5942487.073 576625.447 5942487.931 576652.088 5942522.116 576656.335 5942527.565 576655.825 5942531.790 576662.680 5942540.274 576601.220 5942588.473 576588.349 5942584.209 576416.125 5942527.154 576416.434 5942514.700 576419.617 5942443.127 576419.938 5942435.927 576421.087 5942410.130 576423.587 5942410.205 576480.297 5942411.883 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_b4003c7b-9bc5-4d29-8de8-ddc80de9f251" />
      <xplan:texte xlink:href="#GML_51a21b00-c38b-4ff7-bc86-766994a242f1" />
      <xplan:texte xlink:href="#GML_c61d212b-e6b2-45c8-8d13-640d1550668e" />
      <xplan:texte xlink:href="#GML_ad4a57ec-d094-4fb7-bf50-96dd5537d474" />
      <xplan:texte xlink:href="#GML_8d149ef7-4b94-40d9-b994-6b222dd4734e" />
      <xplan:texte xlink:href="#GML_abd5458e-92a4-4620-b6ec-944a997c3fcf" />
      <xplan:texte xlink:href="#GML_6c87c405-f456-4a52-ae02-d345333bc7c2" />
      <xplan:texte xlink:href="#GML_28cf5e05-9279-4205-946b-aad02be217dd" />
      <xplan:texte xlink:href="#GML_78445473-8bfb-4234-bcfa-9675e3c39fe7" />
      <xplan:texte xlink:href="#GML_e54b2640-a9f8-4b60-be2b-11345fb076b4" />
      <xplan:texte xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:texte xlink:href="#GML_0b4c3abf-323c-4369-bba0-fdaf90a05e18" />
      <xplan:texte xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:texte xlink:href="#GML_1ed1108a-2204-47f8-822f-08e3ddc697e2" />
      <xplan:texte xlink:href="#GML_854a644c-3f65-4596-8362-c6eb77cb747d" />
      <xplan:texte xlink:href="#GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1" />
      <xplan:texte xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:texte xlink:href="#GML_7c360c93-0ccf-44bd-85cd-3c9c54647cc8" />
      <xplan:texte xlink:href="#GML_c9dc8fb4-db8d-411f-9974-ebd078dc055a" />
      <xplan:texte xlink:href="#GML_ea29c620-e827-4e8c-8fc1-f7f0e605904e" />
      <xplan:texte xlink:href="#GML_a7b715e5-c683-4ac9-ae8c-f6b3960810d9" />
      <xplan:texte xlink:href="#GML_b63ca453-1e1e-4f09-b99f-bc4dec515188" />
      <xplan:texte xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:texte xlink:href="#GML_a3c27752-304e-4220-8eed-a2d91f2531d7" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>526</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>3000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2016-06-21</xplan:rechtsverordnungsDatum>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>true</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:bereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576597.149 5942500.937</gml:lowerCorner>
          <gml:upperCorner>576480.297 5942443.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:rasterBasis xlink:href="#GML_1539c797-3eba-4852-be28-8cebdf47e4a7" />
      <xplan:planinhalt xlink:href="#GML_0a7293a1-8897-42a2-8f7d-8e652a2f3a04" />
      <xplan:planinhalt xlink:href="#GML_f58a0678-d7ea-409c-94ba-a3c3c199f8e3" />
      <xplan:planinhalt xlink:href="#GML_4a804a8a-3125-455e-a89a-bf3783b92da1" />
      <xplan:planinhalt xlink:href="#GML_2aa72a5e-4e29-4258-9969-aca657c6774e" />
      <xplan:planinhalt xlink:href="#GML_8f0e7555-075d-4dd0-957a-bc93c5a42c1e" />
      <xplan:planinhalt xlink:href="#GML_3981919b-339d-47be-8361-fa38881dcb29" />
      <xplan:planinhalt xlink:href="#GML_49ddaa03-f4e7-4847-a4b6-275295694227" />
      <xplan:planinhalt xlink:href="#GML_7f7168b3-920f-4672-85de-df3b8a712c6f" />
      <xplan:planinhalt xlink:href="#GML_c662dafa-b6fc-4115-a0a0-3e53c1094a99" />
      <xplan:planinhalt xlink:href="#GML_59d9124b-f014-4c8b-a9d1-d11638a73bca" />
      <xplan:planinhalt xlink:href="#GML_7a1a96af-d38b-4325-a0bd-f1e1e4ce9680" />
      <xplan:planinhalt xlink:href="#GML_85090559-3c6e-4f29-bb6e-418f2f7bff9c" />
      <xplan:planinhalt xlink:href="#GML_627b957f-ad28-472d-a7c8-5aeb75e823a1" />
      <xplan:planinhalt xlink:href="#GML_6faad647-0a64-405e-a0be-515184414780" />
      <xplan:planinhalt xlink:href="#GML_4c252228-adbf-4027-83ef-4a7373cf8ad9" />
      <xplan:planinhalt xlink:href="#GML_2f681dc9-7be4-43bd-8542-e801008aab39" />
      <xplan:planinhalt xlink:href="#GML_bfa7ceb2-40d5-40ac-9023-7960473efb78" />
      <xplan:planinhalt xlink:href="#GML_4edc6426-2d9d-4408-bbf6-0865c0271e08" />
      <xplan:planinhalt xlink:href="#GML_508ca656-eafb-48ff-9ee4-8cef746898ee" />
      <xplan:planinhalt xlink:href="#GML_df45a20e-be9e-4cb6-8053-21df2169c36b" />
      <xplan:planinhalt xlink:href="#GML_3951a89c-a3f9-4065-aa6d-767e9549235a" />
      <xplan:planinhalt xlink:href="#GML_842bad69-eb2b-43f4-bd1b-45dba4c33ef7" />
      <xplan:planinhalt xlink:href="#GML_038d8a87-7958-4bc0-b9a5-93f725ee77c9" />
      <xplan:planinhalt xlink:href="#GML_432bd289-8860-4325-88de-64896e887095" />
      <xplan:planinhalt xlink:href="#GML_c07fcf83-6309-40dd-86d0-b9ebc8f44055" />
      <xplan:planinhalt xlink:href="#GML_1e89c3a6-8e10-4354-abd2-a518e932c6fb" />
      <xplan:planinhalt xlink:href="#GML_3647946c-2a30-4078-909d-35f765227756" />
      <xplan:planinhalt xlink:href="#GML_ac8bcc7c-996b-403c-beda-0c44e62e5526" />
      <xplan:planinhalt xlink:href="#GML_1d7081c1-9315-478a-b33a-bba36110dd12" />
      <xplan:planinhalt xlink:href="#GML_3bbebe5a-71f8-4d11-b1e9-b8802aa0a2ec" />
      <xplan:planinhalt xlink:href="#GML_dad385e7-637b-40c7-8d96-ca50c85d4c6d" />
      <xplan:planinhalt xlink:href="#GML_631c2328-7244-4952-9fcd-9f7c993901b5" />
      <xplan:planinhalt xlink:href="#GML_f9aebab9-62a1-45e0-bcca-7998ec7156c2" />
      <xplan:planinhalt xlink:href="#GML_1f6bab62-0bb3-4fec-881e-b0cd25c51168" />
      <xplan:planinhalt xlink:href="#GML_47fb016d-dab5-4213-a4c1-deed9fb7dd94" />
      <xplan:planinhalt xlink:href="#GML_1a82a070-de61-4fbe-ac33-6891f7702b34" />
      <xplan:planinhalt xlink:href="#GML_3b8a6af5-457e-4e0f-9e12-60b936cadfdf" />
      <xplan:planinhalt xlink:href="#GML_8047a192-d634-4414-b38a-950438ea177c" />
      <xplan:planinhalt xlink:href="#GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8" />
      <xplan:planinhalt xlink:href="#GML_0e8dfd5a-603d-4c56-b846-5f5c1573c528" />
      <xplan:planinhalt xlink:href="#GML_b1826698-ecc5-4cb0-9c88-d5afe1f31b5d" />
      <xplan:planinhalt xlink:href="#GML_14188ee0-916a-4355-bafc-da98d2825c05" />
      <xplan:planinhalt xlink:href="#GML_bbe87b33-5e0d-44cb-8e1c-79078f6c3bf0" />
      <xplan:planinhalt xlink:href="#GML_60fb20fd-8cd3-4173-ac56-880f4451e4d8" />
      <xplan:planinhalt xlink:href="#GML_11901099-8d74-4850-8177-6541547ab30b" />
      <xplan:planinhalt xlink:href="#GML_f20bca86-bba8-47dc-b32c-2149f1816e55" />
      <xplan:planinhalt xlink:href="#GML_42c7fef5-324b-4253-ada5-725c99cb27cf" />
      <xplan:planinhalt xlink:href="#GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357" />
      <xplan:planinhalt xlink:href="#GML_ba300100-99e7-4741-8fc0-323cbfe283f3" />
      <xplan:planinhalt xlink:href="#GML_269bef7b-7d34-40e6-b48d-611a5001ab73" />
      <xplan:planinhalt xlink:href="#GML_e3d2f053-c8ae-498f-aab5-b497490ec636" />
      <xplan:planinhalt xlink:href="#GML_f352bbb5-f0e0-4059-8c86-61f393a9c732" />
      <xplan:planinhalt xlink:href="#GML_6bd85817-ed09-4b54-8ffa-d1faa982d616" />
      <xplan:planinhalt xlink:href="#GML_1ce434d3-ca84-4476-8cb2-18cd42647531" />
      <xplan:planinhalt xlink:href="#GML_c94e422a-b3ed-4d92-95d5-8dc4c189650e" />
      <xplan:planinhalt xlink:href="#GML_96a7bd77-f199-4cea-84d3-fdbabfe3f643" />
      <xplan:planinhalt xlink:href="#GML_a95a1007-a70f-474c-b791-ffb675af4015" />
      <xplan:planinhalt xlink:href="#GML_bff9a682-a559-4db3-a3b5-4fabc70907a6" />
      <xplan:planinhalt xlink:href="#GML_b021ad8f-e4d0-498b-896a-2db5e334ff4d" />
      <xplan:planinhalt xlink:href="#GML_3264fd82-1958-4a67-8673-42cb63036f17" />
      <xplan:planinhalt xlink:href="#GML_109fb3fb-2e1f-45a1-86af-2bb1dcc6d8ed" />
      <xplan:planinhalt xlink:href="#GML_1a6c9d6a-6e93-4854-9229-bd579e4d0df6" />
      <xplan:planinhalt xlink:href="#GML_a8fcb5cd-7364-46b6-8ad9-beb227979325" />
      <xplan:planinhalt xlink:href="#GML_e36ff7bb-13fd-4216-9ae6-f8b39c1ccdfc" />
      <xplan:praesentationsobjekt xlink:href="#GML_8ded082b-f799-4b8c-ba49-a1aa23a63d8f" />
      <xplan:praesentationsobjekt xlink:href="#GML_4ebd4bb6-0bf4-460b-a23a-bdd149ebef6f" />
      <xplan:praesentationsobjekt xlink:href="#GML_00f7eff9-1faf-4448-8ec7-1f0ef6c16d6d" />
      <xplan:praesentationsobjekt xlink:href="#GML_5ba9de57-cd5c-4658-a2af-afd65f5b25bf" />
      <xplan:praesentationsobjekt xlink:href="#GML_1f29d226-6c44-4dd2-ac8e-d109edb17354" />
      <xplan:praesentationsobjekt xlink:href="#GML_05afd08d-622c-4587-829b-6e25b25c7e97" />     
      <xplan:praesentationsobjekt xlink:href="#GML_c50c09c9-84d0-48ad-8c29-0a823b223514" />
      <xplan:praesentationsobjekt xlink:href="#GML_05d2bf60-cc10-4a38-ab83-d3af0057d8f7" />
      <xplan:praesentationsobjekt xlink:href="#GML_35e891fa-16a4-4ac3-a5a5-c449ad78d271" />
      <xplan:praesentationsobjekt xlink:href="#GML_77b72a48-372d-482a-8355-c966829968ba" />
      <xplan:praesentationsobjekt xlink:href="#GML_b651ba20-7173-4b57-a0f7-cd61928b563b" />
      <xplan:praesentationsobjekt xlink:href="#GML_bc8efcd2-efba-4a41-bd7f-6d3b26f272de" />
      <xplan:praesentationsobjekt xlink:href="#GML_c233cc65-4339-4252-934c-24fb7eb2ad14" />
      <xplan:praesentationsobjekt xlink:href="#GML_e1769717-420a-4fc2-9537-6edb0fd69083" />
      <xplan:praesentationsobjekt xlink:href="#GML_5e9681f5-4c33-46f4-8113-b287cbe57b3a" />
      <xplan:praesentationsobjekt xlink:href="#GML_6a527b3a-3287-42fe-8705-d1e28dcbb3d7" />
      <xplan:praesentationsobjekt xlink:href="#GML_b4c0ed8b-3299-4bc4-bfd6-72a53a6957b8" />
      <xplan:praesentationsobjekt xlink:href="#GML_6bfad91a-e310-49d9-93ac-2677f9578e6d" />
      <xplan:praesentationsobjekt xlink:href="#GML_150032b9-df25-4c7a-abf2-b05693dcccba" />
      <xplan:praesentationsobjekt xlink:href="#GML_83094468-6c4d-4e1a-ad90-8a86710d3363" />    
      <xplan:praesentationsobjekt xlink:href="#GML_0ab64645-e5c3-4661-b57e-ae29122cf610" />
      <xplan:praesentationsobjekt xlink:href="#GML_7796ee9b-c35f-4e4d-9309-269006f6ccf8" />
      <xplan:praesentationsobjekt xlink:href="#GML_0bf9f998-bd70-4fea-9cbc-2f2668f5839a" />
      <xplan:praesentationsobjekt xlink:href="#GML_ac19c4a5-51ea-4718-b2e3-98cff0c05273" />
      <xplan:praesentationsobjekt xlink:href="#GML_5c143714-4150-4f49-8d5a-d2b826e7958d" />
      <xplan:praesentationsobjekt xlink:href="#GML_cf666f15-e87c-4193-b484-68ccd5e72343" />
      <xplan:praesentationsobjekt xlink:href="#GML_94de2016-50e2-4e56-9819-3e80454a2a36" />
      <xplan:praesentationsobjekt xlink:href="#GML_c2a44c83-eb3f-46df-ba27-96f0ad9ab3c8" />
      <xplan:praesentationsobjekt xlink:href="#GML_91d05bc0-9d71-4382-88e4-d1c02c48cdaa" />
      <xplan:praesentationsobjekt xlink:href="#GML_cf3fbe32-bf2e-4dc1-8386-7ec5c0532f1b" />
      <xplan:praesentationsobjekt xlink:href="#GML_916188a0-f5b0-4f17-942f-c25a0d915bd9" />
      <xplan:praesentationsobjekt xlink:href="#GML_0bdc76ef-35d5-4c75-9d5b-fa3ea30ba4d1" />
      <xplan:praesentationsobjekt xlink:href="#GML_a5d113d2-aae0-4d37-be0d-86c438e4381f" />
      <xplan:praesentationsobjekt xlink:href="#GML_d0723b30-7904-45b2-9b6a-8b55bf18b911" />
      <xplan:praesentationsobjekt xlink:href="#GML_15890cb4-3da8-43fc-ab37-2c9ecc3cd74c" />
      <xplan:praesentationsobjekt xlink:href="#GML_231f5f5d-30e9-4114-a1ce-e88cd7e3d2b2" />
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_1c23cbae-b6e9-4f21-b9c2-f3fc6df46545" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="GML_0a7293a1-8897-42a2-8f7d-8e652a2f3a04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576419.618 5942410.130</gml:lowerCorner>
          <gml:upperCorner>576480.297 5942443.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_58a9662f-9c14-43e0-b44b-be90f8def650">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">576480.297 5942411.883 576480.266 5942414.884 576430.410 5942413.408 576429.724 5942428.829 576436.040 5942429.111 576436.179 5942434.919 576431.773 5942443.668 576419.618 5942443.127 576419.938 5942435.927 576421.087 5942410.130 576423.587 5942410.205 576480.297 5942411.883 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>9999</xplan:klassifizMassnahme>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
      <xplan:istAusgleich>false</xplan:istAusgleich>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f58a0678-d7ea-409c-94ba-a3c3c199f8e3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.409 5942439.738</gml:lowerCorner>
          <gml:upperCorner>576456.937 5942513.277</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_94de2016-50e2-4e56-9819-3e80454a2a36" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2385c8b4-515c-447b-a28c-44e5f27ae88b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">576443.446 5942439.738 576456.937 5942440.135 576455.999 5942471.816 576442.120 5942513.277 576429.409 5942509.053 576442.564 5942469.424 576443.446 5942439.738 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_4a804a8a-3125-455e-a89a-bf3783b92da1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576572.789 5942443.581</gml:lowerCorner>
          <gml:upperCorner>576586.817 5942462.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_5c6f802f-e80f-49a9-83f4-991bbea855e1">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576572.789 5942461.663 576573.324 5942443.581 576586.817 5942443.980 576586.283 5942462.063 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_2aa72a5e-4e29-4258-9969-aca657c6774e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576591.660 5942440.947</gml:lowerCorner>
          <gml:upperCorner>576591.660 5942440.947</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">41.07</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_458189e3-be12-406f-806c-8b78d62735f7">
          <gml:pos>576591.660 5942440.947</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8f0e7555-075d-4dd0-957a-bc93c5a42c1e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576492.223 5942512.760</gml:lowerCorner>
          <gml:upperCorner>576510.740 5942534.189</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_aef504a3-b996-45db-b7bf-3f41a708ac52">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576510.740 5942517.017 576505.034 5942534.189 576492.223 5942529.932 576497.929 5942512.760 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3981919b-339d-47be-8361-fa38881dcb29">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576527.157 5942509.662</gml:lowerCorner>
          <gml:upperCorner>576577.155 5942537.673</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_fafaeff2-66f4-4724-882d-cd19c7d6ce3a">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">576531.415 5942509.662 576577.155 5942524.862 576572.897 5942537.673 576527.157 5942522.473 576531.415 5942509.662 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_49ddaa03-f4e7-4847-a4b6-275295694227">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.434 5942474.449</gml:lowerCorner>
          <gml:upperCorner>576655.825 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_e5bef399-cf1b-4548-b828-30574e098a06">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="28">576655.825 5942531.790 576609.257 5942567.967 576594.615 5942570.631 576588.214 5942571.795 576585.605 5942570.928 576580.138 5942569.112 576578.266 5942565.328 576584.585 5942546.633 576586.698 5942540.648 576584.613 5942538.256 576585.286 5942536.147 576587.281 5942530.022 576590.820 5942519.150 576591.610 5942515.589 576592.250 5942494.172 576591.693 5942485.065 576590.737 5942484.009 576589.163 5942482.269 576586.072 5942480.109 576582.576 5942478.696 576578.853 5942478.100 576461.588 5942474.628 576455.548 5942474.449 576442.293 5942514.335 576446.184 5942515.628 576444.447 5942520.855 576440.666 5942522.762 576416.434 5942514.700 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="GML_7f7168b3-920f-4672-85de-df3b8a712c6f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576516.450 5942413.603</gml:lowerCorner>
          <gml:upperCorner>576593.045 5942419.368</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_0ca8e356-5528-4f63-b89c-5f6fea588ffa">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576593.045 5942416.370 576592.923 5942419.368 576516.450 5942416.602 576516.545 5942413.603 576593.045 5942416.370 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:istAusgleich>false</xplan:istAusgleich>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="GML_c662dafa-b6fc-4115-a0a0-3e53c1094a99">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576418.023 5942507.169</gml:lowerCorner>
          <gml:upperCorner>576446.184 5942522.771</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_cf3fbe32-bf2e-4dc1-8386-7ec5c0532f1b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a40a0060-7f9b-4cd8-9558-d06114d18ea1">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">576418.023 5942515.238 576420.704 5942507.169 576442.293 5942514.335 576446.184 5942515.628 576444.447 5942520.855 576440.649 5942522.771 576418.023 5942515.238 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_59d9124b-f014-4c8b-a9d1-d11638a73bca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.434 5942410.130</gml:lowerCorner>
          <gml:upperCorner>576600.834 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b6ff45f7-af77-4997-809f-3e9ddebc14db">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="26">576456.312 5942527.947 576440.649 5942522.771 576416.434 5942514.700 576419.618 5942443.127 576419.938 5942435.927 576421.087 5942410.130 576423.587 5942410.205 576480.297 5942411.883 576480.266 5942414.884 576480.165 5942424.848 576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038 576516.148 5942426.171 576516.450 5942416.602 576516.545 5942413.603 576593.045 5942416.370 576600.834 5942416.652 576599.891 5942440.018 576598.756 5942468.151 576594.615 5942570.631 576588.214 5942571.795 576585.605 5942570.928 576580.138 5942569.112 576566.153 5942564.472 576456.312 5942527.947 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>8000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_7a1a96af-d38b-4325-a0bd-f1e1e4ce9680">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576501.302 5942537.538</gml:lowerCorner>
          <gml:upperCorner>576501.302 5942537.538</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_1c95b056-fb5c-4829-8931-437074212efb">
          <gml:pos>576501.302 5942537.538</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
      <xplan:kronendurchmesser uom="m">1.0</xplan:kronendurchmesser>
      <xplan:istAusgleich>false</xplan:istAusgleich>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_85090559-3c6e-4f29-bb6e-418f2f7bff9c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576569.222 5942517.447</gml:lowerCorner>
          <gml:upperCorner>576584.731 5942558.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e54b2640-a9f8-4b60-be2b-11345fb076b4" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_8f634a65-e807-476d-a246-50c1ca51b327">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576582.833 5942517.447 576584.731 5942518.078 576571.120 5942558.997 576569.222 5942558.366 576582.833 5942517.447 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_627b957f-ad28-472d-a7c8-5aeb75e823a1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.724 5942413.408</gml:lowerCorner>
          <gml:upperCorner>576592.923 5942439.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(E)</xplan:gliederung1>
      <xplan:gliederung2>(D)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1f29d226-6c44-4dd2-ac8e-d109edb17354" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_231f5f5d-30e9-4114-a1ce-e88cd7e3d2b2" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_51a21b00-c38b-4ff7-bc86-766994a242f1" />
      <xplan:refTextInhalt xlink:href="#GML_0b4c3abf-323c-4369-bba0-fdaf90a05e18" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7312a5bd-f35b-464f-b384-402923b407fd">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="16">576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038 576516.148 5942426.171 576516.450 5942416.602 576592.923 5942419.368 576592.094 5942439.776 576462.797 5942435.779 576462.798 5942435.749 576436.179 5942434.919 576436.040 5942429.111 576429.724 5942428.829 576430.410 5942413.408 576480.266 5942414.884 576480.165 5942424.848 576485.589 5942430.139 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6faad647-0a64-405e-a0be-515184414780">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576521.453 5942522.473</gml:lowerCorner>
          <gml:upperCorner>576539.969 5942543.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_7e752146-ddec-4f98-bf73-ad8bc1bd446a">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576539.969 5942526.730 576534.264 5942543.897 576521.453 5942539.640 576527.157 5942522.473 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4c252228-adbf-4027-83ef-4a7373cf8ad9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576572.789 5942443.581</gml:lowerCorner>
          <gml:upperCorner>576586.817 5942462.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7796ee9b-c35f-4e4d-9309-269006f6ccf8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ed5f3947-eaa0-4493-b658-e0ea0f1d31f3">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576572.789 5942461.663 576573.324 5942443.581 576586.817 5942443.980 576586.283 5942462.063 576572.789 5942461.663 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_2f681dc9-7be4-43bd-8542-e801008aab39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576537.705 5942460.636</gml:lowerCorner>
          <gml:upperCorner>576586.283 5942475.557</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_d2c7b28a-b700-4936-83be-7d66fb92525e">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">576586.283 5942462.063 576585.884 5942475.557 576537.705 5942474.130 576538.104 5942460.636 576586.283 5942462.063 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_bfa7ceb2-40d5-40ac-9023-7960473efb78">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576594.615 5942468.151</gml:lowerCorner>
          <gml:upperCorner>576656.335 5942570.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>2</xplan:gliederung1>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_35e891fa-16a4-4ac3-a5a5-c449ad78d271" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e1769717-420a-4fc2-9537-6edb0fd69083" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_91d05bc0-9d71-4382-88e4-d1c02c48cdaa" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ea29c620-e827-4e8c-8fc1-f7f0e605904e" />
      <xplan:refTextInhalt xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:refTextInhalt xlink:href="#GML_a7b715e5-c683-4ac9-ae8c-f6b3960810d9" />
      <xplan:refTextInhalt xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:refTextInhalt xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_4e99673d-c08d-4cf8-bb2f-b1c0f8330098">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">576611.810 5942468.677 576626.513 5942487.073 576625.447 5942487.931 576652.088 5942522.116 576656.335 5942527.565 576655.825 5942531.790 576609.257 5942567.967 576594.615 5942570.631 576598.756 5942468.151 576611.810 5942468.677 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_4edc6426-2d9d-4408-bbf6-0865c0271e08">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576454.397 5942483.772</gml:lowerCorner>
          <gml:upperCorner>576585.945 5942564.472</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_9007ec87-52c4-46ea-8329-16f41b048f39">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="23">576454.397 5942524.162 576465.108 5942491.997 576466.066 5942489.137 576467.009 5942487.194 576468.029 5942485.955 576468.991 5942485.155 576470.103 5942484.509 576471.162 5942484.101 576472.593 5942483.814 576473.672 5942483.772 576476.148 5942483.813 576574.305 5942486.719 576577.618 5942487.564 576580.728 5942489.289 576582.388 5942490.772 576583.807 5942492.573 576585.186 5942495.361 576585.945 5942499.069 576585.446 5942515.928 576569.937 5942562.554 576566.153 5942564.472 576456.312 5942527.947 576454.397 5942524.162 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_508ca656-eafb-48ff-9ee4-8cef746898ee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576526.139 5942547.483</gml:lowerCorner>
          <gml:upperCorner>576526.139 5942547.483</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_9ac3a618-57b8-4d83-a3bd-9d5d689b0673">
          <gml:pos>576526.139 5942547.483</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
      <xplan:kronendurchmesser uom="m">1.0</xplan:kronendurchmesser>
      <xplan:istAusgleich>false</xplan:istAusgleich>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_df45a20e-be9e-4cb6-8053-21df2169c36b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576432.120 5942433.330</gml:lowerCorner>
          <gml:upperCorner>576432.120 5942433.330</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">41.0</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d1e67921-1ae6-490e-b5ce-72a2fdeb7836">
          <gml:pos>576432.120 5942433.330</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_3951a89c-a3f9-4065-aa6d-767e9549235a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576496.670 5942438.659</gml:lowerCorner>
          <gml:upperCorner>576497.670 5942439.659</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">41.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_27cd6101-c5ea-45fd-bd7e-e78de95a6a9d">
          <gml:pos>576496.670 5942438.659</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_842bad69-eb2b-43f4-bd1b-45dba4c33ef7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576459.295 5942501.817</gml:lowerCorner>
          <gml:upperCorner>576477.811 5942523.238</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_d820dbc7-5e58-432a-b543-74c327bf9beb">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576477.811 5942506.074 576472.107 5942523.238 576459.295 5942518.984 576465.000 5942501.817 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_038d8a87-7958-4bc0-b9a5-93f725ee77c9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576472.633 5942440.616</gml:lowerCorner>
          <gml:upperCorner>576486.662 5942459.097</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_b226e631-ebc5-432c-85eb-dcf38b9c65b0">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576472.633 5942458.698 576473.168 5942440.616 576486.662 5942441.015 576486.123 5942459.097 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_432bd289-8860-4325-88de-64896e887095">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.724 5942413.408</gml:lowerCorner>
          <gml:upperCorner>576592.924 5942439.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_0e2acd33-2725-422c-80a0-bbf7728fee44">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="16">576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038 576516.148 5942426.171 576516.450 5942416.602 576592.924 5942419.348 576592.094 5942439.776 576462.797 5942435.779 576462.798 5942435.749 576436.179 5942434.919 576436.040 5942429.111 576429.724 5942428.829 576430.410 5942413.408 576480.266 5942414.884 576480.165 5942424.848 576485.589 5942430.139 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:massnahme>3000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
      <xplan:istAusgleich>false</xplan:istAusgleich>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c07fcf83-6309-40dd-86d0-b9ebc8f44055">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576465.000 5942489.005</gml:lowerCorner>
          <gml:upperCorner>576514.998 5942517.017</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05d2bf60-cc10-4a38-ab83-d3af0057d8f7" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f5b7f2b8-333d-413e-8729-a0df2303a725">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576469.253 5942489.005 576514.998 5942504.206 576510.740 5942517.017 576465.000 5942501.817 576469.253 5942489.005 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_1e89c3a6-8e10-4354-abd2-a518e932c6fb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576465.000 5942489.005</gml:lowerCorner>
          <gml:upperCorner>576514.998 5942517.017</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_afe0c0f0-f6e1-470b-9fba-e951c4048c06">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">576469.253 5942489.005 576514.998 5942504.206 576510.740 5942517.017 576465.000 5942501.817 576469.253 5942489.005 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3647946c-2a30-4078-909d-35f765227756">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576527.157 5942509.662</gml:lowerCorner>
          <gml:upperCorner>576577.155 5942537.673</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b4c0ed8b-3299-4bc4-bfd6-72a53a6957b8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5ec07634-ae78-41b6-87bd-07c8cdecfaba">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576531.415 5942509.662 576577.155 5942524.862 576572.897 5942537.673 576527.157 5942522.473 576531.415 5942509.662 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ac8bcc7c-996b-403c-beda-0c44e62e5526">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576420.864 5942475.609</gml:lowerCorner>
          <gml:upperCorner>576439.570 5942509.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_150032b9-df25-4c7a-abf2-b05693dcccba" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_8d5a2f49-3198-4cfe-a3b2-f1754a8553ba">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576429.409 5942509.053 576420.864 5942506.213 576431.033 5942475.609 576439.570 5942478.445 576429.409 5942509.053 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_1d7081c1-9315-478a-b33a-bba36110dd12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576516.450 5942413.603</gml:lowerCorner>
          <gml:upperCorner>576593.045 5942419.348</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(G)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_8d149ef7-4b94-40d9-b994-6b222dd4734e" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_498f581f-59b3-4e58-bb37-6bf17c6ae546">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576593.045 5942416.370 576592.924 5942419.348 576516.450 5942416.602 576516.545 5942413.603 576593.045 5942416.370 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_3bbebe5a-71f8-4d11-b1e9-b8802aa0a2ec">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.125 5942474.449</gml:lowerCorner>
          <gml:upperCorner>576662.680 5942588.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e6da972f-5255-4463-9bef-ca98129c17f0">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="34">576416.434 5942514.700 576440.649 5942522.771 576444.447 5942520.855 576446.184 5942515.628 576442.293 5942514.335 576455.548 5942474.449 576461.588 5942474.628 576578.853 5942478.100 576582.576 5942478.696 576586.072 5942480.109 576589.163 5942482.269 576590.737 5942484.009 576591.693 5942485.065 576592.250 5942494.172 576591.610 5942515.589 576590.820 5942519.150 576587.281 5942530.022 576585.286 5942536.147 576584.613 5942538.256 576586.701 5942540.649 576585.643 5942543.641 576584.585 5942546.633 576578.266 5942565.328 576580.138 5942569.112 576585.605 5942570.928 576588.214 5942571.795 576594.615 5942570.631 576609.257 5942567.967 576655.825 5942531.790 576662.680 5942540.274 576601.220 5942588.473 576588.349 5942584.209 576416.125 5942527.154 576416.434 5942514.700 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="23">576476.148 5942483.813 576473.672 5942483.772 576472.593 5942483.814 576471.162 5942484.101 576470.103 5942484.509 576468.991 5942485.155 576468.029 5942485.955 576467.009 5942487.194 576466.066 5942489.137 576465.108 5942491.997 576454.397 5942524.162 576456.312 5942527.947 576566.153 5942564.472 576569.937 5942562.554 576585.446 5942515.928 576585.945 5942499.069 576585.186 5942495.361 576583.807 5942492.573 576582.388 5942490.772 576580.728 5942489.289 576577.618 5942487.564 576574.305 5942486.719 576476.148 5942483.813 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_dad385e7-637b-40c7-8d96-ca50c85d4c6d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576419.618 5942410.130</gml:lowerCorner>
          <gml:upperCorner>576436.179 5942443.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(F)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ac19c4a5-51ea-4718-b2e3-98cff0c05273" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_c61d212b-e6b2-45c8-8d13-640d1550668e" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_6df2e85c-3ca7-4c08-ba9f-e4b926691379">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">576430.410 5942413.408 576429.724 5942428.829 576436.040 5942429.111 576436.179 5942434.919 576431.773 5942443.668 576419.618 5942443.127 576421.087 5942410.130 576423.587 5942410.205 576430.543 5942410.410 576430.410 5942413.408 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_631c2328-7244-4952-9fcd-9f7c993901b5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576569.937 5942517.281</gml:lowerCorner>
          <gml:upperCorner>576584.996 5942562.554</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_c7a49be9-698e-4db8-9eaf-da786509a9cc">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">576584.996 5942517.281 576569.937 5942562.554 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_f9aebab9-62a1-45e0-bcca-7998ec7156c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576571.627 5942441.909</gml:lowerCorner>
          <gml:upperCorner>576588.920 5942477.592</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(C)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ad4a57ec-d094-4fb7-bf50-96dd5537d474" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7fd17687-d84d-4b06-a61e-d9a6e868326b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="14">576579.633 5942476.632 576579.633 5942472.712 576583.156 5942472.712 576583.756 5942465.671 576571.867 5942465.511 576572.107 5942459.351 576584.056 5942459.191 576584.357 5942445.989 576571.627 5942445.989 576571.627 5942441.909 576587.719 5942441.909 576588.920 5942441.909 576588.920 5942477.592 576579.633 5942476.632 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1f6bab62-0bb3-4fec-881e-b0cd25c51168">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576492.223 5942512.760</gml:lowerCorner>
          <gml:upperCorner>576510.740 5942534.189</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0bdc76ef-35d5-4c75-9d5b-fa3ea30ba4d1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7b51b031-45fc-49c0-b24a-1cbd2642fdaf">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576510.740 5942517.017 576505.034 5942534.189 576492.223 5942529.932 576497.929 5942512.760 576510.740 5942517.017 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_47fb016d-dab5-4213-a4c1-deed9fb7dd94">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576472.234 5942458.698</gml:lowerCorner>
          <gml:upperCorner>576520.812 5942473.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b651ba20-7173-4b57-a0f7-cd61928b563b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a2c508a5-1f9b-475a-84cf-a94888426ed8">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576520.812 5942460.124 576520.412 5942473.618 576472.234 5942472.192 576472.633 5942458.698 576520.812 5942460.124 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_1a82a070-de61-4fbe-ac33-6891f7702b34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576454.397 5942483.772</gml:lowerCorner>
          <gml:upperCorner>576585.945 5942564.472</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>1</xplan:gliederung1>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0ab64645-e5c3-4661-b57e-ae29122cf610" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c2a44c83-eb3f-46df-ba27-96f0ad9ab3c8" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_d0723b30-7904-45b2-9b6a-8b55bf18b911" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_abd5458e-92a4-4620-b6ec-944a997c3fcf" />
      <xplan:refTextInhalt xlink:href="#GML_6c87c405-f456-4a52-ae02-d345333bc7c2" />
      <xplan:refTextInhalt xlink:href="#GML_854a644c-3f65-4596-8362-c6eb77cb747d" />
      <xplan:refTextInhalt xlink:href="#GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1" />
      <xplan:refTextInhalt xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:refTextInhalt xlink:href="#GML_c9dc8fb4-db8d-411f-9974-ebd078dc055a" />
      <xplan:refTextInhalt xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:refTextInhalt xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f849e399-6ccf-480d-b3b6-73df90c08efe">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="23">576454.397 5942524.162 576465.108 5942491.997 576466.066 5942489.137 576467.009 5942487.194 576468.029 5942485.955 576468.991 5942485.155 576470.103 5942484.509 576471.162 5942484.101 576472.593 5942483.814 576473.672 5942483.772 576476.148 5942483.813 576574.305 5942486.719 576577.618 5942487.564 576580.728 5942489.289 576582.388 5942490.772 576583.807 5942492.573 576585.186 5942495.361 576585.945 5942499.069 576585.446 5942515.928 576569.937 5942562.554 576566.153 5942564.472 576456.312 5942527.947 576454.397 5942524.162 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachform>1000</xplan:dachform>
      <xplan:GRZ>0.35</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3b8a6af5-457e-4e0f-9e12-60b936cadfdf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.409 5942439.738</gml:lowerCorner>
          <gml:upperCorner>576456.937 5942513.277</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_3b755570-a10d-41c5-8607-94fcd9169287">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="7">576443.446 5942439.738 576456.937 5942440.135 576455.999 5942471.816 576442.120 5942513.277 576429.409 5942509.053 576442.564 5942469.424 576443.446 5942439.738 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8047a192-d634-4414-b38a-950438ea177c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576538.104 5942442.549</gml:lowerCorner>
          <gml:upperCorner>576552.134 5942461.036</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_83094468-6c4d-4e1a-ad90-8a86710d3363" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a1de3649-ce85-4509-ae75-f4bdad9344c4">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576538.104 5942460.636 576538.640 5942442.549 576552.134 5942442.955 576551.598 5942461.036 576538.104 5942460.636 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576461.588 5942435.749</gml:lowerCorner>
          <gml:upperCorner>576599.891 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>2</xplan:gliederung1>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5e9681f5-4c33-46f4-8113-b287cbe57b3a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6bfad91a-e310-49d9-93ac-2677f9578e6d" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_cf666f15-e87c-4193-b484-68ccd5e72343" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_abd5458e-92a4-4620-b6ec-944a997c3fcf" />
      <xplan:refTextInhalt xlink:href="#GML_6c87c405-f456-4a52-ae02-d345333bc7c2" />
      <xplan:refTextInhalt xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:refTextInhalt xlink:href="#GML_854a644c-3f65-4596-8362-c6eb77cb747d" />
      <xplan:refTextInhalt xlink:href="#GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1" />
      <xplan:refTextInhalt xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:refTextInhalt xlink:href="#GML_c9dc8fb4-db8d-411f-9974-ebd078dc055a" />
      <xplan:refTextInhalt xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_9c3c46f7-641b-4aa7-a1bd-513bb36f11b3">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="26">576594.615 5942570.631 576588.214 5942571.795 576585.605 5942570.928 576580.138 5942569.112 576578.266 5942565.328 576584.585 5942546.633 576585.643 5942543.641 576586.701 5942540.649 576584.613 5942538.256 576585.286 5942536.147 576587.281 5942530.022 576590.820 5942519.150 576591.610 5942515.589 576592.250 5942494.172 576591.693 5942485.065 576590.737 5942484.009 576589.163 5942482.269 576586.072 5942480.109 576582.576 5942478.696 576578.853 5942478.100 576461.588 5942474.628 576462.798 5942435.749 576592.043 5942439.774 576599.891 5942440.018 576598.756 5942468.151 576594.615 5942570.631 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachform>1000</xplan:dachform>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_0e8dfd5a-603d-4c56-b846-5f5c1573c528">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576429.724 5942413.408</gml:lowerCorner>
          <gml:upperCorner>576600.834 5942440.018</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Schutzgrün</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_916188a0-f5b0-4f17-942f-c25a0d915bd9" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e61c058e-5d3f-4523-9b9e-d716b985644e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="18">576436.040 5942429.111 576429.724 5942428.829 576430.410 5942413.408 576480.266 5942414.884 576480.165 5942424.848 576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038 576516.148 5942426.171 576516.450 5942416.602 576592.923 5942419.368 576593.045 5942416.370 576600.834 5942416.652 576599.891 5942440.018 576592.043 5942439.774 576462.798 5942435.749 576436.179 5942434.919 576436.040 5942429.111 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b1826698-ecc5-4cb0-9c88-d5afe1f31b5d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576538.104 5942442.549</gml:lowerCorner>
          <gml:upperCorner>576552.134 5942461.036</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_2a9aa479-8cc9-4a89-827e-120ae49191cd">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576538.104 5942460.636 576538.640 5942442.549 576552.134 5942442.955 576551.598 5942461.036 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_14188ee0-916a-4355-bafc-da98d2825c05">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576602.018 5942468.433</gml:lowerCorner>
          <gml:upperCorner>576652.088 5942561.023</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_7edfe154-24e7-4eb2-bc36-aa24feb94057">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="7">576605.750 5942468.433 576611.810 5942468.677 576626.513 5942487.073 576625.434 5942487.914 576652.088 5942522.116 576602.018 5942561.023 576605.750 5942468.433 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_bbe87b33-5e0d-44cb-8e1c-79078f6c3bf0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576597.144 5942468.151</gml:lowerCorner>
          <gml:upperCorner>576656.335 5942544.342</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0bf9f998-bd70-4fea-9cbc-2f2668f5839a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_ea29c620-e827-4e8c-8fc1-f7f0e605904e" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_19ab8ac9-9ccc-40f1-ab68-a81dedc317e4">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">576652.088 5942522.116 576656.335 5942527.565 576655.825 5942531.790 576639.667 5942544.342 576605.880 5942500.937 576605.337 5942501.379 576597.144 5942508.047 576598.756 5942468.151 576611.810 5942468.677 576626.513 5942487.073 576625.447 5942487.931 576652.088 5942522.116 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_60fb20fd-8cd3-4173-ac56-880f4451e4d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576543.221 5942428.491</gml:lowerCorner>
          <gml:upperCorner>576543.221 5942428.491</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">48.4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_490ed90d-d8cc-47b6-b1a3-39cb411e891e">
          <gml:pos>576543.221 5942428.491</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_11901099-8d74-4850-8177-6541547ab30b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576585.605 5942419.368</gml:lowerCorner>
          <gml:upperCorner>576600.713 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_59d71b5e-69ea-4b72-91d7-ec6affe96625">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="16">576598.756 5942468.151 576594.615 5942570.631 576588.214 5942571.795 576585.605 5942570.928 576587.244 5942530.136 576590.820 5942519.150 576591.610 5942515.589 576592.250 5942494.172 576591.693 5942485.065 576590.737 5942484.009 576590.328 5942483.557 576592.094 5942439.776 576592.923 5942419.368 576600.713 5942419.649 576599.891 5942440.018 576598.756 5942468.151 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:massnahme>3000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
      <xplan:istAusgleich>false</xplan:istAusgleich>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_f20bca86-bba8-47dc-b32c-2149f1816e55">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576602.018 5942468.433</gml:lowerCorner>
          <gml:upperCorner>576652.088 5942561.023</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_336693f4-29b9-47d4-8630-2afd25ad0f20">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">576605.750 5942468.433 576611.810 5942468.677 576626.513 5942487.073 576625.447 5942487.931 576652.088 5942522.116 576602.018 5942561.023 576605.750 5942468.433 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_42c7fef5-324b-4253-ada5-725c99cb27cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576416.434 5942434.919</gml:lowerCorner>
          <gml:upperCorner>576462.798 5942522.771</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>1</xplan:gliederung1>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_00f7eff9-1faf-4448-8ec7-1f0ef6c16d6d" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05afd08d-622c-4587-829b-6e25b25c7e97" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c233cc65-4339-4252-934c-24fb7eb2ad14" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_b4003c7b-9bc5-4d29-8de8-ddc80de9f251" />
      <xplan:refTextInhalt xlink:href="#GML_6c87c405-f456-4a52-ae02-d345333bc7c2" />
      <xplan:refTextInhalt xlink:href="#GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1" />
      <xplan:refTextInhalt xlink:href="#GML_78445473-8bfb-4234-bcfa-9675e3c39fe7" />
      <xplan:refTextInhalt xlink:href="#GML_5060fe64-2557-4954-af7c-0f79ca0811dd" />
      <xplan:refTextInhalt xlink:href="#GML_a7b715e5-c683-4ac9-ae8c-f6b3960810d9" />
      <xplan:refTextInhalt xlink:href="#GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6" />
      <xplan:refTextInhalt xlink:href="#GML_ba86d815-a484-4530-83fe-ad2003191bdf" />
      <xplan:refTextInhalt xlink:href="#GML_da22f094-ff79-47c3-905d-57125ad15bbe" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_21526956-97ef-4ad1-95de-e2affbdc270c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">576436.179 5942434.919 576462.798 5942435.749 576461.588 5942474.628 576455.548 5942474.449 576442.293 5942514.335 576446.184 5942515.628 576444.447 5942520.855 576440.649 5942522.771 576416.434 5942514.700 576419.618 5942443.127 576431.773 5942443.668 576436.179 5942434.919 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachform>1000</xplan:dachform>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Immissionsschutz gml:id="GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576422.504 5942410.205</gml:lowerCorner>
          <gml:upperCorner>576593.045 5942439.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2000</xplan:hoehenbezug>
          <xplan:bezugspunkt>3000</xplan:bezugspunkt>
          <xplan:h uom="m">9</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />    
      <xplan:wirdDargestelltDurch xlink:href="#GML_6a527b3a-3287-42fe-8705-d1e28dcbb3d7" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_15890cb4-3da8-43fc-ab37-2c9ecc3cd74c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_25a1844d-d3b0-46fb-b617-c5de83878556">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="14">576423.587 5942410.205 576430.543 5942410.410 576480.297 5942411.883 576480.165 5942424.848 576485.589 5942430.139 576505.565 5942430.669 576512.841 5942426.038 576516.148 5942426.171 576516.545 5942413.603 576593.045 5942416.370 576592.923 5942419.368 576592.094 5942439.776 576422.504 5942434.534 576423.587 5942410.205 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
      <xplan:nutzung>Fläche für besondere Anlagen</xplan:nutzung>
      <xplan:typ>2000</xplan:typ>
      <xplan:technVorkehrung>10002</xplan:technVorkehrung>
      <xplan:detaillierteTechnVorkehrung>Schutzwall mit Schutzwand</xplan:detaillierteTechnVorkehrung>
    </xplan:BP_Immissionsschutz>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ba300100-99e7-4741-8fc0-323cbfe283f3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576554.382 5942533.416</gml:lowerCorner>
          <gml:upperCorner>576572.897 5942554.840</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_71b7a30c-c6cb-4bf2-a16b-12a2fe138e30">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576572.897 5942537.673 576567.193 5942554.840 576554.382 5942550.583 576560.087 5942533.416 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_269bef7b-7d34-40e6-b48d-611a5001ab73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576521.453 5942522.473</gml:lowerCorner>
          <gml:upperCorner>576539.969 5942543.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a5d113d2-aae0-4d37-be0d-86c438e4381f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f5f2b8a7-98ff-467b-92ec-0d3e559855c7">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576539.969 5942526.730 576534.264 5942543.897 576521.453 5942539.640 576527.157 5942522.473 576539.969 5942526.730 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_e3d2f053-c8ae-498f-aab5-b497490ec636">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576625.089 5942561.859</gml:lowerCorner>
          <gml:upperCorner>576625.089 5942561.859</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">40.6</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_22ee8d4f-fa23-4cd7-b034-c7d5f9ab1edc">
          <gml:pos>576625.089 5942561.859</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_f352bbb5-f0e0-4059-8c86-61f393a9c732">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576476.841 5942424.932</gml:lowerCorner>
          <gml:upperCorner>576477.841 5942425.932</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">48.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_016e1c82-1fc9-490b-bf87-9ea800024d42">
          <gml:pos>576476.841 5942424.932</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6bd85817-ed09-4b54-8ffa-d1faa982d616">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576537.705 5942460.636</gml:lowerCorner>
          <gml:upperCorner>576586.283 5942475.557</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c50c09c9-84d0-48ad-8c29-0a823b223514" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_90cd4d5b-1560-479d-8637-6c4cff491454">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576586.283 5942462.063 576585.884 5942475.557 576537.705 5942474.130 576538.104 5942460.636 576586.283 5942462.063 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_1ce434d3-ca84-4476-8cb2-18cd42647531">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576585.605 5942419.368</gml:lowerCorner>
          <gml:upperCorner>576600.713 5942571.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(E)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4ebd4bb6-0bf4-460b-a23a-bdd149ebef6f" />     
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_51a21b00-c38b-4ff7-bc86-766994a242f1" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e67bf469-cd38-4544-87cd-dbdb01c09482">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="16">576598.756 5942468.151 576594.615 5942570.631 576588.214 5942571.795 576585.605 5942570.928 576587.244 5942530.136 576590.820 5942519.150 576591.610 5942515.589 576592.250 5942494.172 576591.693 5942485.065 576590.737 5942484.009 576590.328 5942483.557 576592.094 5942439.776 576592.923 5942419.368 576600.713 5942419.649 576599.891 5942440.018 576598.756 5942468.151 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c94e422a-b3ed-4d92-95d5-8dc4c189650e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576472.633 5942440.616</gml:lowerCorner>
          <gml:upperCorner>576486.662 5942459.097</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5c143714-4150-4f49-8d5a-d2b826e7958d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5c6fac01-924a-481e-a36f-3e9fed9aa99f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576472.633 5942458.698 576473.168 5942440.616 576486.662 5942441.015 576486.123 5942459.097 576472.633 5942458.698 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_96a7bd77-f199-4cea-84d3-fdbabfe3f643">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576420.864 5942475.609</gml:lowerCorner>
          <gml:upperCorner>576439.570 5942509.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_c53e3018-3e30-4e5d-aac4-724b27a8f9d3">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576429.409 5942509.053 576420.864 5942506.213 576431.033 5942475.609 576439.570 5942478.445 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a95a1007-a70f-474c-b791-ffb675af4015">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576554.382 5942533.416</gml:lowerCorner>
          <gml:upperCorner>576572.897 5942554.840</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_bc8efcd2-efba-4a41-bd7f-6d3b26f272de" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ad6e4b42-d54c-4c8d-81dd-e828c0b2d514">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576572.897 5942537.673 576567.193 5942554.840 576554.382 5942550.583 576560.087 5942533.416 576572.897 5942537.673 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_bff9a682-a559-4db3-a3b5-4fabc70907a6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576507.318 5942441.643</gml:lowerCorner>
          <gml:upperCorner>576521.347 5942460.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_2ac0a2f4-a247-42c8-9915-cb28658118d6">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="4">576507.318 5942459.725 576507.853 5942441.643 576521.347 5942442.042 576520.812 5942460.124 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b021ad8f-e4d0-498b-896a-2db5e334ff4d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576472.234 5942458.698</gml:lowerCorner>
          <gml:upperCorner>576520.812 5942473.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_2b8e8e9c-5280-41d1-8a24-c4a8edd8c47f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="5">576520.812 5942460.124 576520.412 5942473.618 576472.234 5942472.192 576472.633 5942458.698 576520.812 5942460.124 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_3264fd82-1958-4a67-8673-42cb63036f17">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576430.410 5942410.410</gml:lowerCorner>
          <gml:upperCorner>576480.297 5942414.884</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(G)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8ded082b-f799-4b8c-ba49-a1aa23a63d8f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_8d149ef7-4b94-40d9-b994-6b222dd4734e" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d4bae385-c58d-433e-bc86-369db8b90e81">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576480.266 5942414.884 576430.410 5942413.408 576430.543 5942410.410 576480.297 5942411.883 576480.266 5942414.884 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_109fb3fb-2e1f-45a1-86af-2bb1dcc6d8ed">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576507.318 5942441.643</gml:lowerCorner>
          <gml:upperCorner>576521.347 5942460.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_77b72a48-372d-482a-8355-c966829968ba" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_3e9f69f0-1b7d-4a98-bd64-665777f9b1b4">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576507.318 5942459.725 576507.853 5942441.643 576521.347 5942442.042 576520.812 5942460.124 576507.318 5942459.725 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_1a6c9d6a-6e93-4854-9229-bd579e4d0df6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576418.558 5942521.081</gml:lowerCorner>
          <gml:upperCorner>576418.558 5942521.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">40.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_bfc9bc90-7391-472b-99c9-af1e9cd770e6">
          <gml:pos>576418.558 5942521.081</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">0.000000</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a8fcb5cd-7364-46b6-8ad9-beb227979325">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576459.295 5942501.817</gml:lowerCorner>
          <gml:upperCorner>576477.811 5942523.238</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5ba9de57-cd5c-4658-a2af-afd65f5b25bf" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ae3ad541-3a41-4a48-843a-831266f033d6">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">576477.811 5942506.074 576472.107 5942523.238 576459.295 5942518.984 576465.000 5942501.817 576477.811 5942506.074 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_e36ff7bb-13fd-4216-9ae6-f8b39c1ccdfc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576597.149 5942500.937</gml:lowerCorner>
          <gml:upperCorner>576639.667 5942544.342</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Sonstige Abgrenzung</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_54199088-4359-498e-bb62-cc93659b1edb">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">576639.667 5942544.342 576605.880 5942500.937 576597.149 5942508.043 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b4003c7b-9bc5-4d29-8de8-ddc80de9f251">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>Für festgesetzte Einzelbäume sowie auf den Flächen für
die Erhaltung und zum Anpflanzen von Bäumen und
Sträuchern sind bei Abgang gleichwertige Ersatzpflanzungen
vorzunehmen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_51a21b00-c38b-4ff7-bc86-766994a242f1">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>Auf der mit „(E)“ bezeichneten Fläche für die Erhaltung
und zum Anpflanzen von Bäumen und Sträuchern sind
Lücken mit Bäumen und Sträuchern so zu schließen sowie
Wall-Aufsetzarbeiten so durchzuführen, dass der Charakter
und Aufbau eines Knicks erhalten bleibt.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_8ded082b-f799-4b8c-ba49-a1aa23a63d8f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576438.813 5942411.191</gml:lowerCorner>
          <gml:upperCorner>576439.813 5942412.191</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3264fd82-1958-4a67-8673-42cb63036f17" />
      <xplan:schriftinhalt>(G)</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3d0b1394-36e1-41ec-96ae-5bdc12542372">
          <gml:pos>576438.813 5942411.191</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_4ebd4bb6-0bf4-460b-a23a-bdd149ebef6f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576593.850 5942426.013</gml:lowerCorner>
          <gml:upperCorner>576594.850 5942427.013</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1ce434d3-ca84-4476-8cb2-18cd42647531" />
      <xplan:schriftinhalt>(E)</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_25c7f8b5-4649-47ab-bbb6-e81a4196c18e">
          <gml:pos>576593.850 5942426.013</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c61d212b-e6b2-45c8-8d13-640d1550668e">
      <xplan:schluessel>§2 Nr.23</xplan:schluessel>
      <xplan:text>Auf der mit „(F)“ bezeichneten Fläche für Maßnahmen
zum Schutz, zur Pflege und zur Entwicklung von Boden,
Natur und Landschaft ist eine mindestens 250 m² große
Hochstaudenflur herzustellen. Die Fläche ist allseitig einzuzäunen
und zu dem mit „MI 1“ bezeichneten Mischgebiet zusätzlich mit einer vorgelagerten, dichtwachsenden
zweireihigen Strauchhecke abzupflanzen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_00f7eff9-1faf-4448-8ec7-1f0ef6c16d6d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576427.154 5942469.442</gml:lowerCorner>
          <gml:upperCorner>576428.154 5942470.442</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_42c7fef5-324b-4253-ada5-725c99cb27cf" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c4e3375e-7ceb-4bc6-8336-8e22bba40621">
          <gml:pos>576427.154 5942469.442</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ad4a57ec-d094-4fb7-bf50-96dd5537d474">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Einseitig zu den mit „(B)“ gekennzeichneten Gebäudeseiten
ausgerichtete Wohnungen sind unzulässig. Zu den mit
„(C)“ gekennzeichneten Gebäudeseiten ausgerichtete
offene Außenwohnbereiche (zum Beispiel Balkone, Loggien,
Terrassen) sind unzulässig. An den mit „(C)“ gekennzeichneten
Gebäudeseiten sind entweder vor den Aufenthaltsräumen
verglaste Vorbauten wie zum Beispiel Doppelfassaden,
verglaste Loggien, Wintergärten, verglaste
Laubengänge oder in ihrer Wirkung vergleichbare Maßnahmen
vorzusehen oder in den Aufenthaltsräumen durch
geeignete bauliche Schallschutzmaßnahmen wie zum Beispiel
Doppelfassaden, verglaste Vorbauten, besondere
Fensterkonstruktionen oder in ihrer Wirkung vergleichbare
Maßnahmen sicherzustellen, dass durch diese baulichen
Maßnahmen insgesamt eine Schallpegeldifferenz
erreicht wird, die es ermöglicht, dass in Aufenthaltsräumen
ein lnnenraumpegel von 40 dB(A) während der Tagzeit
und 30 dB(A) während der Nachtzeit bei teilgeöffneten
Fenstern nicht überschritten wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5ba9de57-cd5c-4658-a2af-afd65f5b25bf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576468.553 5942512.528</gml:lowerCorner>
          <gml:upperCorner>576469.553 5942513.528</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a8fcb5cd-7364-46b6-8ad9-beb227979325" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7c9cad79-47ce-4453-889d-8bf9dcac722a">
          <gml:pos>576468.553 5942512.528</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1f29d226-6c44-4dd2-ac8e-d109edb17354">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576568.679 5942428.219</gml:lowerCorner>
          <gml:upperCorner>576569.679 5942429.219</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_627b957f-ad28-472d-a7c8-5aeb75e823a1" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b3c43d26-2fe2-4253-abd1-5ff0737300fc">
          <gml:pos>576568.679 5942428.219</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05afd08d-622c-4587-829b-6e25b25c7e97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576427.882 5942462.534</gml:lowerCorner>
          <gml:upperCorner>576428.882 5942463.534</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_42c7fef5-324b-4253-ada5-725c99cb27cf" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ed670000-7f6d-4acf-85f5-e01bff0da355">
          <gml:pos>576427.882 5942462.534</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember> 
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c50c09c9-84d0-48ad-8c29-0a823b223514">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576561.994 5942468.096</gml:lowerCorner>
          <gml:upperCorner>576562.994 5942469.096</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6bd85817-ed09-4b54-8ffa-d1faa982d616" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_90622d9e-5047-4b8e-81b6-0fa5c7f41840">
          <gml:pos>576561.994 5942468.096</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05d2bf60-cc10-4a38-ab83-d3af0057d8f7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576489.998 5942503.011</gml:lowerCorner>
          <gml:upperCorner>576490.998 5942504.011</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c07fcf83-6309-40dd-86d0-b9ebc8f44055" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_14cdae19-c8e9-4169-92e5-c42ea33378ac">
          <gml:pos>576489.998 5942503.011</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_8d149ef7-4b94-40d9-b994-6b222dd4734e">
      <xplan:schluessel>§2 Nr.24</xplan:schluessel>
      <xplan:text>Auf den mit „(G)“ bezeichneten Flächen für Maßnahmen
zum Schutz, zur Pflege und zur Entwicklung von Boden,
Natur und Landschaft ist jeweils eine dichtwachsende
zweireihige Strauchhecke aus dornenbewehrten Arten
anzupflanzen und auf der Südseite einzuzäunen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_35e891fa-16a4-4ac3-a5a5-c449ad78d271">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576620.053 5942520.948</gml:lowerCorner>
          <gml:upperCorner>576621.053 5942521.948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bfa7ceb2-40d5-40ac-9023-7960473efb78" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_57e8d23a-4f13-4939-9f14-d9ac7171e684">
          <gml:pos>576620.053 5942520.948</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_abd5458e-92a4-4620-b6ec-944a997c3fcf">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und in dem mit „MI 1“
bezeichneten Mischgebiet sind Standplätze für Abfallbehälter
außerhalb von Gebäuden mit Sträuchern oder
Hecken einzugrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_77b72a48-372d-482a-8355-c966829968ba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576514.333 5942450.884</gml:lowerCorner>
          <gml:upperCorner>576515.333 5942451.884</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_109fb3fb-2e1f-45a1-86af-2bb1dcc6d8ed" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_416dffe6-ec7f-4ebf-8b13-3605b8df1db8">
          <gml:pos>576514.333 5942450.884</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b651ba20-7173-4b57-a0f7-cd61928b563b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576496.523 5942466.158</gml:lowerCorner>
          <gml:upperCorner>576497.523 5942467.158</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_47fb016d-dab5-4213-a4c1-deed9fb7dd94" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_461d71f8-f3fc-487f-bc08-92d041f6fd8b">
          <gml:pos>576496.523 5942466.158</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bc8efcd2-efba-4a41-bd7f-6d3b26f272de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576563.640 5942544.128</gml:lowerCorner>
          <gml:upperCorner>576564.640 5942545.128</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a95a1007-a70f-474c-b791-ffb675af4015" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3f85c452-4671-4822-a3c3-5fed209851e0">
          <gml:pos>576563.640 5942544.128</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c233cc65-4339-4252-934c-24fb7eb2ad14">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576426.972 5942457.081</gml:lowerCorner>
          <gml:upperCorner>576427.972 5942458.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>dachform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_42c7fef5-324b-4253-ada5-725c99cb27cf" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_9ba556f4-e7b7-40db-8659-a507d398729a">
          <gml:pos>576426.972 5942457.081</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6c87c405-f456-4a52-ae02-d345333bc7c2">
      <xplan:schluessel>§2 Nr.19</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und in dem mit „MI 1“
bezeichneten Mischgebiet sind die Dachflächen von
Gebäuden und Carports mit einem mindestens 8 cm starken
durchwurzelbaren Substrataufbau zu versehen und
mindestens extensiv zu begrünen. Ausnahmen für erforderliche
befestigte Flächen und anderweitige Nutzungen
können zugelassen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e1769717-420a-4fc2-9537-6edb0fd69083">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576620.962 5942527.931</gml:lowerCorner>
          <gml:upperCorner>576621.962 5942528.931</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bfa7ceb2-40d5-40ac-9023-7960473efb78" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_386927ba-f0a3-4d4d-b07b-3b234758bbe4">
          <gml:pos>576620.962 5942527.931</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_28cf5e05-9279-4205-946b-aad02be217dd">
      <xplan:schluessel>§2 Nr.20</xplan:schluessel>
      <xplan:text>Nicht überbaute Tiefgaragen sind mit einem mindestens
50 cm starken durchwurzelbaren Substrataufbau zu versehen
und dauerhaft zu begrünen. Ausnahmen für erforderliche
befestigte Flächen können zugelassen werden.
Für anzupflanzende Bäume auf Tiefgaragen muss auf einer
Fläche von mindestens 12 m² je Baum die Schichtstärke
des durchwurzelbaren Substrataufbaus mindestens 1 m
betragen. Tiefgaragenzufahrten sind baulich einzufassen
oder mit Rankgerüsten oder Pergolen zu überstellen und
mit Schling- oder Kletterpflanzen zu begrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_78445473-8bfb-4234-bcfa-9675e3c39fe7">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>In den Mischgebieten sind Gartenbaubetriebe und Tankstellen
unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5e9681f5-4c33-46f4-8113-b287cbe57b3a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576524.665 5942454.124</gml:lowerCorner>
          <gml:upperCorner>576525.665 5942455.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>dachform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d53fc714-1c01-4e66-b87a-8f9c1004a5df">
          <gml:pos>576524.665 5942454.124</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_6a527b3a-3287-42fe-8705-d1e28dcbb3d7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576526.885 5942431.723</gml:lowerCorner>
          <gml:upperCorner>576527.885 5942432.723</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>detaillierteTechnVorkehrung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357" />
      <xplan:schriftinhalt>Schutzwall mit Schutzwand</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3e9a4532-dc2e-45e6-a02b-bc8e529cf340">
          <gml:pos>576526.885 5942431.723</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">1.36</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b4c0ed8b-3299-4bc4-bfd6-72a53a6957b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576552.156 5942523.668</gml:lowerCorner>
          <gml:upperCorner>576553.156 5942524.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3647946c-2a30-4078-909d-35f765227756" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b56b7944-2238-446f-be8d-6de6e529898d">
          <gml:pos>576552.156 5942523.668</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6bfad91a-e310-49d9-93ac-2677f9578e6d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576526.983 5942464.840</gml:lowerCorner>
          <gml:upperCorner>576527.983 5942465.840</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2bd1a77c-10c8-4080-92a5-190592725f27">
          <gml:pos>576526.983 5942464.840</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_150032b9-df25-4c7a-abf2-b05693dcccba">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576430.217 5942492.331</gml:lowerCorner>
          <gml:upperCorner>576431.217 5942493.331</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ac8bcc7c-996b-403c-beda-0c44e62e5526" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_4d56f140-dc3f-4ab4-9535-9bbbd39ff54f">
          <gml:pos>576430.217 5942492.331</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_83094468-6c4d-4e1a-ad90-8a86710d3363">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576545.119 5942451.794</gml:lowerCorner>
          <gml:upperCorner>576546.119 5942452.794</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8047a192-d634-4414-b38a-950438ea177c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_5d48d1a0-dd0d-41c3-a558-23900454bb6c">
          <gml:pos>576545.119 5942451.794</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0ab64645-e5c3-4661-b57e-ae29122cf610">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576537.524 5942497.207</gml:lowerCorner>
          <gml:upperCorner>576538.524 5942498.207</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1a82a070-de61-4fbe-ac33-6891f7702b34" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_f84cb67b-de03-47b1-b39f-68963f1430d7">
          <gml:pos>576537.524 5942497.207</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7796ee9b-c35f-4e4d-9309-269006f6ccf8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576579.803 5942452.822</gml:lowerCorner>
          <gml:upperCorner>576580.803 5942453.822</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_4c252228-adbf-4027-83ef-4a7373cf8ad9" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_af82dd5a-5a57-4e7b-a028-ba88453d7523">
          <gml:pos>576579.803 5942452.822</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e54b2640-a9f8-4b60-be2b-11345fb076b4">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Das festgesetzte Gehrecht auf dem Flurstück 4402 der
Gemarkung Meiendorf umfasst die Befugnis der Freien
und Hansestadt Hamburg zu verlangen, dass die bezeichnete
Fläche dem allgemeinen Fußgängerverkehr als Gehweg
zur Verfügung gestellt wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_0bf9f998-bd70-4fea-9cbc-2f2668f5839a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576609.908 5942485.248</gml:lowerCorner>
          <gml:upperCorner>576610.908 5942486.248</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bbe87b33-5e0d-44cb-8e1c-79078f6c3bf0" />
      <xplan:schriftinhalt>(A)</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3803dc6a-8223-4b93-a9ca-adaf4610b53b">
          <gml:pos>576609.908 5942485.248</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_ac19c4a5-51ea-4718-b2e3-98cff0c05273">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576422.832 5942424.648</gml:lowerCorner>
          <gml:upperCorner>576423.832 5942425.648</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dad385e7-637b-40c7-8d96-ca50c85d4c6d" />
      <xplan:schriftinhalt>(F)</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_41f01bfc-b442-477b-ab3a-60bd760b5513">
          <gml:pos>576422.832 5942424.648</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_da22f094-ff79-47c3-905d-57125ad15bbe">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Eine Überschreitung der Baugrenzen durch Balkone um
bis zu 1,5 m, durch Wintergärten um bis zu 1,75 m sowie
durch ebenerdige Terrassen um bis zu 3 m, kann zugelassen
werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0b4c3abf-323c-4369-bba0-fdaf90a05e18">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Auf der mit „(D)“ bezeichneten Fläche für die Erhaltung
und zum Anpflanzen von Bäumen und Sträuchern sind
Lücken mit Bäumen und Sträuchern so zu schließen, dass
der Charakter und Aufbau des dichtwachsenden, gestuften
Schutzgrüns wieder hergestellt und dauerhaft erhalten
wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5c143714-4150-4f49-8d5a-d2b826e7958d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576479.647 5942449.856</gml:lowerCorner>
          <gml:upperCorner>576480.647 5942450.856</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c94e422a-b3ed-4d92-95d5-8dc4c189650e" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_69863af1-d44d-4a0d-8c57-088962cfddbd">
          <gml:pos>576479.647 5942449.856</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cf666f15-e87c-4193-b484-68ccd5e72343">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576525.824 5942459.048</gml:lowerCorner>
          <gml:upperCorner>576526.824 5942460.048</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3f91fd67-0c0b-4b1a-aa98-8753c8199ba8" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2220888f-bfa5-42ed-97a6-1d4b8af62c9a">
          <gml:pos>576525.824 5942459.048</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ba86d815-a484-4530-83fe-ad2003191bdf">
      <xplan:schluessel>§2 Nr.21</xplan:schluessel>
      <xplan:text>Auf den privaten Grundstücksflächen sind ebenerdige,
nicht überdachte Stellplätze in wasser- und luftdurchlässigem
Aufbau herzustellen. Feuerwehrumfahrten und -aufstellflächen
auf zu begrünenden Flächen sind in vegetationsfähigem
Aufbau (Schotterrasen) herzustellen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_1ed1108a-2204-47f8-822f-08e3ddc697e2">
      <xplan:schluessel>§2 Nr.22</xplan:schluessel>
      <xplan:text>Drainagen oder sonstige bauliche oder technische Maßnahmen,
die zu einer dauerhaften Absenkung des vegetationsverfügbaren
Grundwasserspiegels beziehungsweise
von Staunässe führen, sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_854a644c-3f65-4596-8362-c6eb77cb747d">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten sind Stellplätze nur in
Tiefgaragen zulässig. Tiefgaragen sind auch außerhalb der
Baugrenzen zulässig. Für Tiefgaragen und deren Zufahrten
kann die festgesetzte Grundflächenzahl bis zu einer
Grundflächenzahl von 0,8 in dem mit „WA 1“ und bis zu
einer Grundflächenzahl von 0,7 in dem mit „WA 2“
bezeichneten Wohngebiet überschritten werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_bd96f489-ace2-4a87-8731-9ef82e38c8c1">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und in dem mit „MI 1“
bezeichneten Mischgebiet sind Staffelgeschosse unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_5060fe64-2557-4954-af7c-0f79ca0811dd">
      <xplan:schluessel>§2 Nr.17</xplan:schluessel>
      <xplan:text>In den Baugebieten sind an Straßenverkehrsflächen
angrenzende Einfriedigungen nur in Form von Hecken
oder durchbrochenen Zäunen in Verbindung mit Hecken
zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_Rasterdarstellung gml:id="GML_1539c797-3eba-4852-be28-8cebdf47e4a7">
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan001_5-2.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan001_5-2.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
    </xplan:XP_Rasterdarstellung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_7c360c93-0ccf-44bd-85cd-3c9c54647cc8">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Im Rahmen der festgesetzten Nutzungen sind innerhalb
des Vorhabengebiets nur solche Vorhaben zulässig, zu
deren Durchführung sich der Vorhabenträger im Durchführungsvertrag
verpflichtet.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_94de2016-50e2-4e56-9819-3e80454a2a36">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576448.798 5942469.435</gml:lowerCorner>
          <gml:upperCorner>576449.798 5942470.435</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f58a0678-d7ea-409c-94ba-a3c3c199f8e3" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d83c87e4-6301-4f90-93fa-ca6fc0f21887">
          <gml:pos>576448.798 5942469.435</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c9dc8fb4-db8d-411f-9974-ebd078dc055a">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten werden Ausnahmen für
Gartenbaubetriebe und Tankstellen ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c2a44c83-eb3f-46df-ba27-96f0ad9ab3c8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576540.684 5942503.522</gml:lowerCorner>
          <gml:upperCorner>576541.684 5942504.522</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1a82a070-de61-4fbe-ac33-6891f7702b34" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_31da3161-3a20-4f06-a472-fb062b5daf2b">
          <gml:pos>576540.684 5942503.522</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_91d05bc0-9d71-4382-88e4-d1c02c48cdaa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576622.145 5942515.494</gml:lowerCorner>
          <gml:upperCorner>576623.145 5942516.494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bfa7ceb2-40d5-40ac-9023-7960473efb78" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_26ece242-677c-4d29-bef4-5f45881e0595">
          <gml:pos>576622.145 5942515.494</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cf3fbe32-bf2e-4dc1-8386-7ec5c0532f1b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576435.799 5942516.668</gml:lowerCorner>
          <gml:upperCorner>576436.799 5942516.668</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c662dafa-b6fc-4115-a0a0-3e53c1094a99" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ea1f9189-87f5-4ef2-85af-aef9b0331416">
          <gml:pos>576435.799 5942516.668</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ea29c620-e827-4e8c-8fc1-f7f0e605904e">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Auf der mit „(A)“ bezeichneten Fläche des Mischgebiets
ist der dort vorhandene Tischlereibetrieb zulässig. Änderungen
und Erneuerungen der betrieblichen Anlagen können
ausnahmsweise zugelassen werden, wenn durch geeignete
Maßnahmen und bauliche Vorkehrungen, wie zum
Beispiel Einhausungen, sichergestellt wird, dass sich die
vom Betrieb ausgehenden Emissionen nicht erhöhen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_916188a0-f5b0-4f17-942f-c25a0d915bd9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576444.052 5942422.671</gml:lowerCorner>
          <gml:upperCorner>576445.052 5942423.671</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:art>text</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0e8dfd5a-603d-4c56-b846-5f5c1573c528" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_385b8beb-3ebe-4689-bcd2-4ff90fc8bbc2">
          <gml:pos>576444.052 5942422.671</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0bdc76ef-35d5-4c75-9d5b-fa3ea30ba4d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576501.481 5942523.475</gml:lowerCorner>
          <gml:upperCorner>576502.481 5942524.475</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1f6bab62-0bb3-4fec-881e-b0cd25c51168" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_94e04690-13ca-44e7-98b0-63070dedeff0">
          <gml:pos>576501.481 5942523.475</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a5d113d2-aae0-4d37-be0d-86c438e4381f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576530.711 5942533.185</gml:lowerCorner>
          <gml:upperCorner>576531.711 5942534.185</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_269bef7b-7d34-40e6-b48d-611a5001ab73" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a5343a43-7eed-40ba-b0bd-bf702e1fb948">
          <gml:pos>576530.711 5942533.185</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a7b715e5-c683-4ac9-ae8c-f6b3960810d9">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>In den gewerblich geprägten Teilen der Mischgebiete sind
Vergnügungsstätten, insbesondere Spielhallen, Wettbüros
und ähnliche Unternehmen im Sinne von § 1 Absatz 2 des
Hamburgischen Spielhallengesetzes vom 4. Dezember
2012 (HmbGVBl. S. 505), die der Aufstellung von Spielgeräten
mit oder ohne Gewinnmöglichkeiten dienen und
Vorführ- und Geschäftsräume, deren Zweck auf Darstellungen
oder auf Handlungen mit sexuellem Charakter ausgerichtet
ist, unzulässig. In den übrigen Teilen der Mischgebiete
werden Ausnahmen für Vergnügungsstätten ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d0723b30-7904-45b2-9b6a-8b55bf18b911">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576536.662 5942492.615</gml:lowerCorner>
          <gml:upperCorner>576537.662 5942493.615</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>dachform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1a82a070-de61-4fbe-ac33-6891f7702b34" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3e5104eb-f793-4809-bd44-775902e19a51">
          <gml:pos>576536.662 5942492.615</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b63ca453-1e1e-4f09-b99f-bc4dec515188">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Außerhalb von öffentlichen Straßenverkehrsflächen sind
Geländeaufhöhungen und Abgrabungen sowie Ablagerungen
im Kronenbereich zu erhaltender Bäume unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_15890cb4-3da8-43fc-ab37-2c9ecc3cd74c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576523.225 5942422.286</gml:lowerCorner>
          <gml:upperCorner>576524.225 5942423.286</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_438cb8ae-be20-4cce-b7fa-6d5b7326a357" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_6c5ef708-ec27-4b46-8697-ddc10a2530d8">
          <gml:pos>576523.225 5942422.286</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c984e887-2e6f-4cfd-a569-3f54d81c29e6">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>In den allgemeinen Wohngebieten und in den Mischgebieten
ist für je angefangene 250 m² der nicht überbaubaren
Grundstücksfläche, einschließlich der zu begrünenden
unterbauten Flächen, ein kleinkroniger Baum oder für je
angefangene 500 m² mindestens ein großkroniger Baum zu
pflanzen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_231f5f5d-30e9-4114-a1ce-e88cd7e3d2b2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>576469.912 5942419.198</gml:lowerCorner>
          <gml:upperCorner>576470.912 5942420.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_a1587702-61cb-4dc7-a7fc-6b3d8ea2ebe3" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_627b957f-ad28-472d-a7c8-5aeb75e823a1" />
      <xplan:schriftinhalt>(E)</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2516c85e-d32c-4793-9bc0-984fc648b953">
          <gml:pos>576469.912 5942419.198</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_a3c27752-304e-4220-8eed-a2d91f2531d7">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Für festgesetzte Anpflanzungen und für Ersatzpflanzungen
von Bäumen, Sträuchern und Hecken sind standortgerechte,
einheimische Laubgehölzarten zu verwenden
und dauerhaft zu erhalten. Ausnahmen können zugelassen
werden. Bäume müssen einen Stammumfang von mindestens
18 cm, in 1 m Höhe über dem Erdboden gemessen,
aufweisen. Im Kronenbereich jedes Baumes ist eine offene
Vegetationsfläche von mindestens 12 m² anzulegen und zu
begrünen. Sträucher und Heckenpflanzen müssen mindestens
folgende Qualität aufweisen: Zweimal verpflanzt,
Höhe mindestens 60 cm.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
</xplan:XPlanAuszug>