﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_13EB5E5A-3933-45CD-924B-0DA5FCC9F72A" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/0 http://www.xplanungwiki.de/upload/XPlanGML/5.0/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>580577.219 5926915.185</gml:lowerCorner>
      <gml:upperCorner>580716.969 5927028.4</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_0decb756-66dd-4fe7-8cef-44a305f25433">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580577.219 5926915.185</gml:lowerCorner>
          <gml:upperCorner>580716.969 5927028.4</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan002_5-0</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan BPlan002_5-0 für den Geltungsbereich
zwischen der Bergedorfer Straße, der Rektor-Ritter-
Straße und der Töpfertwiete (Bezirk Bergedorf, Ortsteil 603)
wird festgestellt.
Das Gebiet wird wie folgt begrenzt:
Bergedorfer Straße – Am Brink – Hassestraße – Süd- und Ostgrenze
des Flurstücks 2063, über das Flurstück 2063, Süd- und
Ostgrenze des Flurstücks 2063 der Gemarkung Bergedorf –
Rektor-Ritter-Straße – Töpfertwiete.</xplan:beschreibung>
      <xplan:technHerstellDatum>2018-10-09</xplan:technHerstellDatum>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_5379A9EA-480A-45FD-8765-8F3DED9FD075" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580603.6 5927028.4 580590.1 5926979.3 580584.909 5926960.419 
580577.219 5926931.029 580581.355 5926928.063 580584.263 5926925.894 
580595.837 5926917.26 580598.728 5926921.052 580601.062 5926924.253 
580604.904 5926929.521 580605.206 5926929.936 580609.396 5926930.592 
580614.052 5926927.197 580636.742 5926932.788 580647.084 5926928.672 
580650.343 5926925.587 580652.649 5926923.404 580657.31 5926918.1 
580660.068 5926915.185 580661.898 5926916.639 580665.845 5926929.955 
580668.296 5926929.468 580669.794 5926937.007 580669.951 5926936.976 
580674.431 5926960.254 580676.602 5926971.542 580679.031 5926984.164 
580685.002 5926983.919 580685.054 5926984.348 580685.409 5926988.706 
580685.69 5926992.154 580698.329 5926989.642 580700.296 5926989.252 
580706.04 5926988.414 580710.294 5927009.269 580712.563 5927011.002 
580716.969 5927015.8 580706.93 5927023.855 580654.7 5927022.8 
580603.6 5927028.4 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_bcf54f8d-8c51-446b-8b77-6f7cd0400378" />
      <xplan:texte xlink:href="#GML_3e7329c0-0fe4-4537-ac21-532c19f818c3" />
      <xplan:texte xlink:href="#GML_937c9efa-6290-49a8-9867-ffc2309c0dcb" />
      <xplan:texte xlink:href="#GML_ff8301ef-9483-4ac8-9d65-06465ce06ade" />
      <xplan:texte xlink:href="#GML_d81b81ce-2712-4fdb-8536-bd7276c1fa41" />
      <xplan:texte xlink:href="#GML_d39dd5db-6402-486b-84cb-a2d890027b2e" />
      <xplan:texte xlink:href="#GML_f56aa949-4103-4315-b9a3-9acb728ad28c" />
      <xplan:texte xlink:href="#GML_fe93beae-ab11-4ce9-95c8-0468f6debaa7" />
      <xplan:texte xlink:href="#GML_c8e86153-48b0-4851-a9e4-3ba5b683152d" />
      <xplan:texte xlink:href="#GML_92965f23-95db-4a19-87d2-3b4e50c90a54" />
      <xplan:texte xlink:href="#GML_0f4982ac-1dd1-45ef-81fc-accb4a8808f1" />
      <xplan:texte xlink:href="#GML_6227732b-c41d-448f-8bf2-ec341e904ef9" />
      <xplan:texte xlink:href="#GML_e14afa10-ef93-4ab5-9201-9a39d0167e9b" />
      <xplan:texte xlink:href="#GML_00bdf88d-167c-4b19-9753-4975941691fd" />
      <xplan:texte xlink:href="#GML_989d638a-86bc-4ad7-bab9-fe0a42e4f56b" />
      <xplan:texte xlink:href="#GML_b36e4c50-a260-47f8-a0be-bedc34f071d3" />
      <xplan:texte xlink:href="#GML_27742408-29ec-4f29-9d7c-38ffa1e310dd" />
      <xplan:texte xlink:href="#GML_1ece4cf3-8a56-41c0-9dff-f999fb49f75e" />
      <xplan:texte xlink:href="#GML_faae3eb5-5384-4cdb-99bf-ac288dc0868a" />
      <xplan:texte xlink:href="#GML_89f97931-d81e-4b5f-89c6-023299eadfb4" />
      <xplan:texte xlink:href="#GML_f44819a8-d871-411c-b72d-b4b1952ac63e" />
      <xplan:texte xlink:href="#GML_59458fc3-b530-42ac-8fcc-d34d138672e0" />
      <xplan:texte xlink:href="#GML_e0334f01-d01e-42fa-b51a-acf5e1106aeb" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>603</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:plangeber>
        <xplan:XP_Plangeber>
          <xplan:name>603</xplan:name>
        </xplan:XP_Plangeber>
      </xplan:plangeber>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2018-08-31</xplan:rechtsverordnungsDatum>
      <xplan:bereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_bcf54f8d-8c51-446b-8b77-6f7cd0400378">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Die festgesetzten Gebäudehöhen dürfen durch untergeordnete
Bauteile (zum Beispiel Aufzugsüberfahrten) um
bis zu 1 m und durch technische Aufbauten um bis zu 1,5 m
überschritten werden. Technische Aufbauten sind um
mindestens 1,5 m von den Außenfassaden zurückzusetzen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_3e7329c0-0fe4-4537-ac21-532c19f818c3">
      <xplan:schluessel>§2 Nr.17</xplan:schluessel>
      <xplan:text>An der Bergedorfer Straße ist für den Außenbereich einer
Wohnung entweder durch Orientierung an der lärmabgewandten
Gebäudeseite oder durch bauliche Schallschutzmaßnahmen
wie zum Beispiel verglaste Vorbauten mit
teilgeöffneten Bauteilen sicherzustellen, dass durch diese
Maßnahmen insgesamt eine Pegelminderung erreicht
wird, die es ermöglicht, dass in dem der Wohnung zugeordneten
Außenbereich ein Tagpegel von kleiner 65 dB(A)
erreicht wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_937c9efa-6290-49a8-9867-ffc2309c0dcb">
      <xplan:schluessel>§2 Nr.21</xplan:schluessel>
      <xplan:text>Die nicht überbauten und nicht für Erschließungswege
und Terrassen beanspruchten Flächen von Tiefgaragen
sind mit einem mindestens 50 cm starken durchwurzelbaren
Substrataufbau zu versehen und gärtnerisch oder als
Spielplatzflächen anzulegen. Für Bäume muss die Schichtstärke
des durchwurzelbaren Substrataufbaus im Pflanzbereich
auf einer Fläche von mindestens 12 m² je Baum
mindestens 1 m betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ff8301ef-9483-4ac8-9d65-06465ce06ade">
      <xplan:schluessel>§2 Nr.22</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet außerhalb der durch die
Tiefgarage unterbauten Fläche sind Wege in wasser- und
luftdurchlässigem Aufbau herzustellen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d81b81ce-2712-4fdb-8536-bd7276c1fa41">
      <xplan:schluessel>§2 Nr.20</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die Dachflächen der
obersten Geschosse von Gebäuden, die eine Neigung bis zu
20 Grad aufweisen, mit einem mindestens 12 cm starken
durchwurzelbaren Substrataufbau zu versehen und extensiv
zu begrünen. Hiervon ausgenommen sind auf bis zu
50 v. H. dieser Dachflächen Flächen für nicht aufgeständerte
technische Anlagen und zur Belichtung sowie für
deren Wartung notwendige Flächen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d39dd5db-6402-486b-84cb-a2d890027b2e">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Innerhalb der mit „(A)“ bezeichneten Fläche des Kerngebiets
sind Wohnungen unzulässig. Ausnahmen für Wohnungen
nach § 7 Absatz 3 Nummer 2 der Baunutzungsverordnung
werden ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_f56aa949-4103-4315-b9a3-9acb728ad28c">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Die Tiefgaragenzu- und -ausfahrt ist nur an der in der
Planzeichnung festgesetzten Stelle zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_fe93beae-ab11-4ce9-95c8-0468f6debaa7">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Für die Berechnung der Anzahl der zulässigen Vollgeschosse
wird für die Gebäude, bei denen eine Fassade an
die Bergedorfer Straße grenzt, die Bergedorfer Straße als
Bezugsebene festgesetzt. Für die übrigen Gebäude wird die
Töpfertwiete als Bezugsebene festgesetzt.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c8e86153-48b0-4851-a9e4-3ba5b683152d">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind für den mit „c“ bezeichneten
Fassadenabschnitt die Wohn- und Schlafräume der
lärmabgewandten Gebäudeseite zuzuordnen. Sofern eine
Anordnung aller Wohn- und Schlafräume einer Wohnung
an den lärmabgewandten Gebäudeseiten nicht möglich ist,
sind vorrangig die Schlafräume den lärmabgewandten
Gebäudeseiten zuzuordnen. Für Räume an den lärmzugewandten
Gebäudeseiten muss ein ausreichender Schallschutz
durch bauliche Maßnahmen an Außentüren, Fenstern,
Außenwänden und Dächern der Gebäude geschaffen
werden. Wohn-/Schlafräume in Einzimmerwohnungen
und Kinderzimmer sind wie Schlafräume zu beurteilen.
Von der Festsetzung kann abgewichen werden, wenn an
allen Gebäudeseiten einer Wohnung der verkehrslärmbezogene
Tagpegel höchstens 59 dB(A) und der verkehrslärmbezogene
Nachtpegel höchstens 49 dB(A) beträgt.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_92965f23-95db-4a19-87d2-3b4e50c90a54">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Stellplätze nur in Tiefgaragen
zulässig. Tiefgaragen einschließlich Zufahrten
und Fluchttreppen sind auch außerhalb der überbaubaren
Grundstücksflächen zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0f4982ac-1dd1-45ef-81fc-accb4a8808f1">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet darf die festgesetzte Grundflächenzahl
für Anlagen nach § 19 Absatz 4 der Baunutzungsverordnung
bis zu einer Grundflächenzahl von 0,9
überschritten werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6227732b-c41d-448f-8bf2-ec341e904ef9">
      <xplan:schluessel>§2 Nr.23</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind insgesamt 3 Nisthöhlen
für den Hausrotschwanz, 5 Mehrfachquartiere für den
Haussperling und 20 Nisthöhlen für den Mauersegler an
den Gebäudefassaden fachgerecht anzubringen und zu
erhalten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e14afa10-ef93-4ab5-9201-9a39d0167e9b">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet dürfen die Baugrenzen durch
Balkone um bis zu 1,5 m und durch Terrassen um bis zu
4 m überschritten werden. Im Bereich von öffentlichen
Straßenverkehrsflächen müssen Balkone eine lichte Höhe
von mindestens 3,5 m über Geländeoberkante der angrenzenden
Straßenverkehrsfläche einhalten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_00bdf88d-167c-4b19-9753-4975941691fd">
      <xplan:schluessel>§2 Nr.19</xplan:schluessel>
      <xplan:text>In ersten, zweiten und dritten Obergeschossen des mit „a“
bezeichneten Fassadenabschnitts sowie im Erdgeschoss
und ersten Obergeschoss des mit „b“ bezeichneten Fassadenabschnitts
ist durch geeignete bauliche Schallschutzmaßnahmen
wie zum Beispiel Doppelfassaden, verglaste
Vorbauten (zum Beispiel verglaste Loggien, Wintergärten),
besondere Fensterkonstruktionen oder in ihrer
Wirkung vergleichbare Maßnahmen sicherzustellen, dass durch diese baulichen Maßnahmen insgesamt eine Schallpegeldifferenz
erreicht wird, die es ermöglicht, dass in
Schlafräumen ein Innenraumpegel bei teilgeöffneten
Fenstern von 30 dB(A) während der Nachtzeit nicht überschritten
wird. Erfolgt die bauliche Schallschutzmaßnahme
in Form von verglasten Vorbauten, muss dieser
Innenraumpegel bei teilgeöffneten Bauteilen erreicht werden.
Wohn-/Schlafräume in Einzimmerwohnungen und
Kinderzimmer sind wie Schlafräume zu beurteilen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_989d638a-86bc-4ad7-bab9-fe0a42e4f56b">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Betriebe des Beherbergungsgewerbes
und sonstige nicht störende Gewerbebetriebe
allgemein zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b36e4c50-a260-47f8-a0be-bedc34f071d3">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>Auf der festgesetzten Fläche für Gemeinschaftsanlagen mit
der Zweckbestimmung Kinderspiel- und Freizeitfläche
sind oberirdische Gebäude sowie bauliche Anlagen, von
denen Wirkungen wie von Gebäuden ausgehen und die
nicht der Kinderspiel- und Freizeitnutzung dienen, unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_27742408-29ec-4f29-9d7c-38ffa1e310dd">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Im Kerngebiet sind Vergnügungsstätten (insbesondere
Wettbüros, Spielhallen und ähnliche Unternehmen im
Sinne von § 1 Absatz 2 des Hamburgischen Spielhallengesetzes
vom 4. Dezember 2012 [HmbGVBl. S. 505], geändert
am 20. Juli 2016 [HmbGVBl. S. 323]), die der Aufstellung
von Spielgeräten mit Gewinnmöglichkeiten dienen,
Vorführ- und Geschäftsräume, deren Zweck auf Darstellungen
oder Handlungen mit sexuellem Charakter gerichtet
ist, sowie Bordelle und bordellartige Betriebe unzulässig.
Vergnügungsstätten sind nur im Sinne von § 4a Absatz
3 Nummer 2 der Baunutzungsverordnung in der Fassung
vom 21. November 2017 (BGBl. I S. 3787) zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_1ece4cf3-8a56-41c0-9dff-f999fb49f75e">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>An der Bergedorfer Straße sind Schlafräume zur lärmabgewandten
Gebäudeseite zu orientieren. Wohn-/Schlafräume
in Einzimmerwohnungen sind wie Schlafräume zu
beurteilen. Wird an Gebäudeseiten ein Pegel von 70 dB(A)
am Tag erreicht oder überschritten, sind vor den Fenstern
der zu dieser Gebäudeseite orientierten Wohnräume bauliche
Schallschutzmaßnahmen in Form von verglasten Vorbauten
(zum Beispiel verglaste Loggien, Wintergärten)
oder vergleichbare Maßnahmen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_faae3eb5-5384-4cdb-99bf-ac288dc0868a">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Im Kerngebiet sind Tankstellen im Zusammenhang mit
Parkhäusern und Großgaragen sowie Anlagen für sportliche
Zwecke unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_89f97931-d81e-4b5f-89c6-023299eadfb4">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Oberhalb des festgesetzten obersten Vollgeschosses sind
keine weiteren Geschosse zulässig. Das gilt nicht innerhalb
der mit „(B)“ bezeichneten überbaubaren Grundstück sflächen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_f44819a8-d871-411c-b72d-b4b1952ac63e">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet ist eine Wohnnutzung erst
zulässig, wenn durch die Errichtung von Gebäuden entlang
der Bergedorfer Straße der verkehrslärmbezogene
Nachtpegel an den lärmabgewandten Gebäudeseiten der
Wohnungen auf höchstens 49 dB(A) verringert wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_59458fc3-b530-42ac-8fcc-d34d138672e0">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Anlagen für sportliche
Zwecke unzulässig; Ausnahmen für Tankstellen werden
ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e0334f01-d01e-42fa-b51a-acf5e1106aeb">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Das oberste Geschoss in den mit „(B)“ bezeichneten überbaubaren
Grundstücksflächen muss an mindestens einer
Außenwand gegenüber dem darunterliegenden Geschoss
um mindestens 1,5 m zurückgesetzt werden. Die Geschossfläche
des obersten Geschosses darf 66 vom Hundert (v. H.)
der Grundfläche des darunterliegenden Geschosses nicht
überschreiten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_f2cf94b3-0d17-40ee-9603-15511ad06898">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580577.219 5926915.185</gml:lowerCorner>
          <gml:upperCorner>580716.969 5927028.4</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_E440D660-FA17-42E6-AE1F-72330FCE3729" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580603.6 5927028.4 580590.1 5926979.3 580584.909 5926960.419 
580577.219 5926931.029 580581.355 5926928.063 580584.263 5926925.894 
580595.837 5926917.26 580598.728 5926921.052 580601.062 5926924.253 
580604.904 5926929.521 580605.206 5926929.936 580609.396 5926930.592 
580614.052 5926927.197 580636.742 5926932.788 580647.084 5926928.672 
580650.343 5926925.587 580652.649 5926923.404 580657.31 5926918.1 
580660.068 5926915.185 580661.898 5926916.639 580665.845 5926929.955 
580668.296 5926929.468 580669.794 5926937.007 580669.951 5926936.976 
580674.431 5926960.254 580676.602 5926971.542 580679.031 5926984.164 
580685.002 5926983.919 580685.054 5926984.348 580685.409 5926988.706 
580685.69 5926992.154 580698.329 5926989.642 580700.296 5926989.252 
580706.04 5926988.414 580710.294 5927009.269 580712.563 5927011.002 
580716.969 5927015.8 580706.93 5927023.855 580654.7 5927022.8 
580603.6 5927028.4 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:rasterBasis xlink:href="#GML_41cf5746-c0fa-4213-9b71-2afb7230c70a" />
      <xplan:planinhalt xlink:href="#GML_150b61aa-5c56-4ec2-8fe2-3b5d421572de" />
      <xplan:planinhalt xlink:href="#GML_ba9e6710-cca7-4bac-a177-fd7608f59e8b" />
      <xplan:planinhalt xlink:href="#GML_39467d9d-7e72-4d30-b47d-06f2ca2afbc4" />
      <xplan:planinhalt xlink:href="#GML_452e09b6-901f-49e7-bf2d-2ab0e7837880" />
      <xplan:planinhalt xlink:href="#GML_47459da5-21a6-4aa8-87b1-d0d72f8e1ef6" />
      <xplan:planinhalt xlink:href="#GML_64544158-6c70-4971-a599-a2795077636d" />
      <xplan:planinhalt xlink:href="#GML_6b6b24b5-59ef-4e08-912b-446e77c1a561" />
      <xplan:planinhalt xlink:href="#GML_3939245e-fef8-486b-8c46-4258ff93a947" />
      <xplan:planinhalt xlink:href="#GML_744646d0-e0d4-49b7-8dc9-f2fdb77f5acb" />
      <xplan:planinhalt xlink:href="#GML_4c92b4af-2ef6-4450-8871-d2e624ccff9b" />
      <xplan:planinhalt xlink:href="#GML_c71a2958-a903-4a02-929b-5efa14150a30" />
      <xplan:planinhalt xlink:href="#GML_99d7f5aa-c113-4995-aa36-f6bbf6921759" />
      <xplan:planinhalt xlink:href="#GML_9e7b2dd3-7f13-4bba-9f23-20cf7e0a4c95" />
      <xplan:planinhalt xlink:href="#GML_d74c25b2-ad4c-4c7d-93ac-1416fcd7b4b8" />
      <xplan:planinhalt xlink:href="#GML_d4570dfd-7337-4ca4-970b-d4eb57a2df3e" />
      <xplan:planinhalt xlink:href="#GML_60a93207-eb55-4a84-b7ac-de1f0143f825" />
      <xplan:planinhalt xlink:href="#GML_64da494f-69d8-4665-9487-f93a62d3928b" />
      <xplan:planinhalt xlink:href="#GML_2ebc5bd6-659b-44b6-8cca-c0c0f51dea95" />
      <xplan:planinhalt xlink:href="#GML_a73b4c21-c02d-4bb8-90dc-8aa8d0c839a2" />
      <xplan:planinhalt xlink:href="#GML_a96c3e7c-5793-4e27-8765-75def0dac447" />
      <xplan:planinhalt xlink:href="#GML_6d3e1482-cfca-49aa-95d2-396f0532f65a" />
      <xplan:planinhalt xlink:href="#GML_ee1606c4-865c-4414-a461-0c6b64864e44" />
      <xplan:planinhalt xlink:href="#GML_37a348c5-c7d1-4db2-a9be-3da8d86e0d25" />
      <xplan:planinhalt xlink:href="#GML_90c8aba7-2de2-4156-b20d-62755f6a9e8d" />
      <xplan:planinhalt xlink:href="#GML_7e247472-b4d4-46bb-a3ad-56ad477beaeb" />
      <xplan:planinhalt xlink:href="#GML_21f6f5a0-8197-4cd8-8f1d-fbfb8d4b55ee" />
      <xplan:planinhalt xlink:href="#GML_a8385709-8b7e-438e-ab3f-72b9c4904d9a" />
      <xplan:planinhalt xlink:href="#GML_31cdfc92-82aa-4884-9560-74f6babe0357" />
      <xplan:planinhalt xlink:href="#GML_25626ccc-f6af-4523-a37b-8d3503c41bc8" />
      <xplan:planinhalt xlink:href="#GML_8ff6c751-3139-4110-b404-5048dbc78c0c" />
      <xplan:planinhalt xlink:href="#GML_6c7a2054-61cb-4a0d-a705-36f9c88438f2" />
      <xplan:planinhalt xlink:href="#GML_9a32990a-c598-4835-a01e-e014c691e688" />
      <xplan:planinhalt xlink:href="#GML_5c3b26c6-2a48-4dc2-97d6-54b19db7426a" />
      <xplan:planinhalt xlink:href="#GML_1dddfd88-05bd-4adb-ab2a-61d798e718e1" />
      <xplan:planinhalt xlink:href="#GML_dfe19ff2-3d74-4cc9-b5ad-3f53450ac720" />
      <xplan:planinhalt xlink:href="#GML_0a850e29-203f-4033-bed2-c9ebab6faead" />
      <xplan:planinhalt xlink:href="#GML_fadb487b-0464-4589-9a3c-3e5684051175" />
      <xplan:planinhalt xlink:href="#GML_9eb2645a-0815-4bef-8897-81ded5cd3537" />
      <xplan:planinhalt xlink:href="#GML_c1f195ca-9dab-44d1-9bf4-30123358b90a" />
      <xplan:planinhalt xlink:href="#GML_22a8081b-e6cf-4324-abc7-8c5536572e64" />
      <xplan:planinhalt xlink:href="#GML_f27c9a7f-abb0-4cd4-9ba9-9fb20420111c" />
      <xplan:planinhalt xlink:href="#GML_3498c5be-9e1c-4463-848a-8f5e1126d09e" />
      <xplan:planinhalt xlink:href="#GML_9bebac07-0582-42d8-9510-81f1792b6e65" />
      <xplan:planinhalt xlink:href="#GML_5289d0ed-a04f-4c99-98c8-3614db2d2af2" />
      <xplan:planinhalt xlink:href="#GML_364ef867-12ae-42c3-be91-b5bf1f0fe288" />
      <xplan:planinhalt xlink:href="#GML_bec3cde9-11b5-4f15-831c-5fb31c58c993" />
      <xplan:planinhalt xlink:href="#GML_baf88722-7992-44ea-8541-eb3706370cfa" />
      <xplan:planinhalt xlink:href="#GML_a2373422-6820-44b1-ac22-f2591629beb0" />
      <xplan:planinhalt xlink:href="#GML_9ee244f4-e9f0-475e-9c00-82790c1cdae5" />
      <xplan:praesentationsobjekt xlink:href="#GML_2cfc8897-5c5f-4a0c-9473-8d04db3a600e" />
      <xplan:praesentationsobjekt xlink:href="#GML_e9ddfe1a-1274-4805-a8c7-0aafb458448e" />
      <xplan:versionBauNVOText>Version vom21.11.2017</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_0decb756-66dd-4fe7-8cef-44a305f25433" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_Rasterdarstellung gml:id="GML_41cf5746-c0fa-4213-9b71-2afb7230c70a">
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan002_5-0.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan002_5-0.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
    </xplan:XP_Rasterdarstellung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_150b61aa-5c56-4ec2-8fe2-3b5d421572de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580613.468 5926941.147</gml:lowerCorner>
          <gml:upperCorner>580633.019 5926957.604</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1960A703-A843-4647-A926-10FED5B963FD" srsName="EPSG:25832">
          <gml:posList>580616.815 5926957.604 580613.468 5926945.771 580629.671 5926941.147 
580633.019 5926952.999 580616.815 5926957.604 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ba9e6710-cca7-4bac-a177-fd7608f59e8b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580679.431 5926996.771</gml:lowerCorner>
          <gml:upperCorner>580686.101 5927006.774</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_FF49595D-7645-42FE-AA46-2C06DAA0AF91" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580686.101 5927004.91 580683.422 5927004.996 580683.508 5927006.651 
580679.752 5927006.774 580679.431 5926996.98 580686.008 5926996.771 
580686.101 5927004.91 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_39467d9d-7e72-4d30-b47d-06f2ca2afbc4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580644.217 5926937.956</gml:lowerCorner>
          <gml:upperCorner>580662.524 5926957.869</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">18</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F2DDB83D-3223-4746-926C-3AF6DB70236A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580662.524 5926954.666 580647.87 5926957.869 580644.217 5926941.159 
580658.871 5926937.956 580662.524 5926954.666 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_452e09b6-901f-49e7-bf2d-2ab0e7837880">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580652.065 5926973.863</gml:lowerCorner>
          <gml:upperCorner>580667.839 5926982.19</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_41C70856-0E32-4645-9053-E165B590C6D1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580667.839 5926978.987 580653.185 5926982.19 580652.065 5926977.066 
580666.719 5926973.863 580667.839 5926978.987 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_47459da5-21a6-4aa8-87b1-d0d72f8e1ef6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580633.178 5926989.845</gml:lowerCorner>
          <gml:upperCorner>580678.965 5927008.13</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C2427851-9D16-4F6C-A25B-842010C94D29" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580641.449 5926993.57 580641.915 5927008.014 580638.377 5927008.13 
580637.525 5927005.197 580633.178 5926991.464 580678.876 5926989.845 
580678.965 5926992.357 580662.088 5926992.903 580662.554 5927007.338 
580659.111 5927007.451 580658.64 5926993.014 580641.449 5926993.57 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>5</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_64544158-6c70-4971-a599-a2795077636d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580662.524 5926954.666</gml:lowerCorner>
          <gml:upperCorner>580663.067 5926957.152</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7491EF57-9297-4660-A5A5-5D9A7D2A404A" srsName="EPSG:25832">
          <gml:posList>580662.524 5926954.666 580663.067 5926957.152 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">15</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6b6b24b5-59ef-4e08-912b-446e77c1a561">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580641.449 5926993.014</gml:lowerCorner>
          <gml:upperCorner>580659.111 5927008.014</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">27</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_263E6B19-A0C5-4EFF-99E0-8874B08A8F47" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580659.111 5927007.451 580641.915 5927008.014 580641.449 5926993.57 
580658.64 5926993.014 580659.111 5927007.451 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>6</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3939245e-fef8-486b-8c46-4258ff93a947">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580588.913 5926925.672</gml:lowerCorner>
          <gml:upperCorner>580603.821 5926944.565</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B933459D-0251-4683-A53A-0A3416D81354" srsName="EPSG:25832">
          <gml:posList>580593.455 5926944.565 580588.913 5926928.602 580599.338 5926925.672 
580603.821 5926941.62 580593.455 5926944.565 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_744646d0-e0d4-49b7-8dc9-f2fdb77f5acb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580613.868 5926957.604</gml:lowerCorner>
          <gml:upperCorner>580616.815 5926958.442</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_17ABD4B9-BD3E-4E56-A941-5E5764037B3A" srsName="EPSG:25832">
          <gml:posList>580616.815 5926957.604 580613.868 5926958.442 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4c92b4af-2ef6-4450-8871-d2e624ccff9b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580647.87 5926954.666</gml:lowerCorner>
          <gml:upperCorner>580663.067 5926960.355</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3485B006-5776-4A23-AA62-A69F62F466C8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580663.067 5926957.152 580648.413 5926960.355 580647.87 5926957.869 
580662.524 5926954.666 580663.067 5926957.152 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c71a2958-a903-4a02-929b-5efa14150a30">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580598.751 5926991.464</gml:lowerCorner>
          <gml:upperCorner>580637.525 5927015.286</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">27</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3F2282AB-A294-4C8D-A3D4-4A95D12614D7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580637.525 5927005.197 580602.846 5927015.286 580599.989 5927005.465 
580598.751 5927001.196 580608.798 5926998.353 580633.178 5926991.464 
580637.525 5927005.197 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>5</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_99d7f5aa-c113-4995-aa36-f6bbf6921759">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580679.752 5927004.91</gml:lowerCorner>
          <gml:upperCorner>580686.101 5927006.774</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8F80DC47-A834-4ADD-8056-A772529BC0E3" srsName="EPSG:25832">
          <gml:posList>580686.101 5927004.91 580683.422 5927004.996 580683.508 5927006.651 
580679.752 5927006.774 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_9e7b2dd3-7f13-4bba-9f23-20cf7e0a4c95">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580588.913 5926921.052</gml:lowerCorner>
          <gml:upperCorner>580698.811 5927015.286</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2AEB8E46-0653-47F4-8DB5-586D987F71F7" srsName="EPSG:25832">
          <gml:posList>580698.329 5926989.642 580698.811 5927005.437 580686.144 5927005.887 
580686.101 5927004.91 580683.422 5927004.996 580683.508 5927006.651 
580679.752 5927006.774 580638.377 5927008.13 580637.525 5927005.197 
580622.152 5927009.669 580612.446 5927012.493 580602.846 5927015.286 
580599.774 5927004.727 580598.751 5927001.196 580608.798 5926998.353 
580600.456 5926968.995 580609.321 5926966.42 580607.563 5926960.234 
580598.621 5926962.775 580595.176 5926950.65 580588.913 5926928.602 
580598.728 5926921.052 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d74c25b2-ad4c-4c7d-93ac-1416fcd7b4b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580662.088 5926992.347</gml:lowerCorner>
          <gml:upperCorner>580679.752 5927007.338</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">27</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5D1B07EA-B64D-4471-ABB0-8414AA8D2DDA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580679.431 5926996.98 580679.752 5927006.774 580662.554 5927007.338 
580662.088 5926992.903 580678.965 5926992.357 580679.279 5926992.347 
580679.431 5926996.98 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>6</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d4570dfd-7337-4ca4-970b-d4eb57a2df3e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580593.455 5926944.565</gml:lowerCorner>
          <gml:upperCorner>580595.259 5926950.944</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_499CC797-BF66-4035-BF99-6C3F4FDA4559" srsName="EPSG:25832">
          <gml:posList>580595.259 5926950.944 580593.455 5926944.565 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_60a93207-eb55-4a84-b7ac-de1f0143f825">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580613.468 5926941.147</gml:lowerCorner>
          <gml:upperCorner>580633.019 5926957.604</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_B7E1DEBB-2350-4FAC-8334-F73945E7B6FF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580633.019 5926952.999 580616.815 5926957.604 580613.468 5926945.771 
580629.671 5926941.147 580633.019 5926952.999 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_64da494f-69d8-4665-9487-f93a62d3928b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580647.87 5926957.869</gml:lowerCorner>
          <gml:upperCorner>580648.413 5926960.355</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2E850F89-EC41-4DB0-AA78-AC6185DE0BFA" srsName="EPSG:25832">
          <gml:posList>580648.413 5926960.355 580647.87 5926957.869 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">15</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_2ebc5bd6-659b-44b6-8cca-c0c0f51dea95">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580595.259 5926946.612</gml:lowerCorner>
          <gml:upperCorner>580613.868 5926962.775</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_58C708A2-9B9C-4409-AF64-6D6F87DEB71A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580613.862 5926958.42 580613.868 5926958.442 580607.563 5926960.234 
580598.621 5926962.775 580595.259 5926950.944 580610.507 5926946.612 
580613.862 5926958.42 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a73b4c21-c02d-4bb8-90dc-8aa8d0c839a2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580600.456 5926965.075</gml:lowerCorner>
          <gml:upperCorner>580622.27 5926998.353</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(B)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_15755086-93F9-4650-B84A-BC16FA98E423" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580622.27 5926994.547 580608.798 5926998.353 580600.456 5926968.995 
580609.321 5926966.42 580613.951 5926965.075 580622.27 5926994.547 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a96c3e7c-5793-4e27-8765-75def0dac447">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580588.913 5926921.052</gml:lowerCorner>
          <gml:upperCorner>580638.327 5926958.442</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7D2F5038-39D0-4CFE-9F18-64A0BB54D931" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580616.815 5926957.604 580613.868 5926958.442 580610.507 5926946.612 
580595.259 5926950.944 580593.455 5926944.565 580603.821 5926941.62 
580599.338 5926925.672 580588.913 5926928.602 580598.728 5926921.052 
580601.062 5926924.253 580604.904 5926929.521 580605.053 5926929.726 
580602.775 5926931.416 580606.873 5926945.842 580634.494 5926937.999 
580638.327 5926951.49 580633.019 5926952.999 580629.671 5926941.147 
580613.468 5926945.771 580616.815 5926957.604 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6d3e1482-cfca-49aa-95d2-396f0532f65a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580600.456 5926965.075</gml:lowerCorner>
          <gml:upperCorner>580622.27 5926998.353</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_09577F26-CD6C-49C0-B1FB-FB23700CA797" srsName="EPSG:25832">
          <gml:posList>580622.27 5926994.547 580608.798 5926998.353 580604.431 5926983.004 
580600.456 5926968.995 580609.321 5926966.42 580613.951 5926965.075 
580622.27 5926994.547 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_ee1606c4-865c-4414-a461-0c6b64864e44">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580583.9445 5926921.052</gml:lowerCorner>
          <gml:upperCorner>580638.537 5926964.95</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(c)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_0743A797-76ED-4F6D-A8AD-E2DDA01B06B6" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6F827232-CADD-4057-8F42-F27D278DDB5F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580601.0506 5926924.2373 580593.255 5926930.119 580601.495 5926959.331 
580637.637 5926949.063 580638.537 5926952.835 580594.982 5926964.95 
580583.9445 5926931.3953 580598.728 5926921.052 580601.0506 5926924.2373 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_0743A797-76ED-4F6D-A8AD-E2DDA01B06B6">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind für den mit „c“ bezeichneten Fassadenabschnitt die Wohn- und Schlafräume der
lärmabgewandten Gebäudeseite zuzuordnen. Sofern eine
Anordnung aller Wohn- und Schlafräume einer Wohnung
an den lärmabgewandten Gebäudeseiten nicht möglich ist,
sind vorrangig die Schlafräume den lärmabgewandten
Gebäudeseiten zuzuordnen. Für Räume an den lärmzugewandten Gebäudeseiten muss ein ausreichender Schallschutz durch bauliche Maßnahmen an Außentüren, Fenstern, Außenwänden und Dächern der Gebäude geschaffen
werden. Wohn-/Schlafräume in Einzimmerwohnungen
und Kinderzimmer sind wie Schlafräume zu beurteilen.
Von der Festsetzung kann abgewichen werden, wenn an
allen Gebäudeseiten einer Wohnung der verkehrslärmbezogene Tagpegel höchstens 59 dB(A) und der verkehrslärmbezogene Nachtpegel höchstens 49 dB(A) beträgt.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_37a348c5-c7d1-4db2-a9be-3da8d86e0d25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580588.913 5926921.052</gml:lowerCorner>
          <gml:upperCorner>580638.327 5926952.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_EAEBA11E-F38E-4A0F-A8DB-319BC9772397" srsName="EPSG:25832">
          <gml:posList>580588.913 5926928.602 580598.728 5926921.052 580605.053 5926929.726 
580602.775 5926931.416 580606.873 5926945.842 580634.494 5926937.999 
580638.327 5926951.49 580633.019 5926952.999 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_EinfahrtsbereichLinie gml:id="GML_90c8aba7-2de2-4156-b20d-62755f6a9e8d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580591.484 5926945.147</gml:lowerCorner>
          <gml:upperCorner>580593.39 5926951.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Anschluss der Grundstücke</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7784B0CE-98AE-4489-9970-20A7BEFF1B2A" srsName="EPSG:25832">
          <gml:posList>580593.39 5926951.473 580591.484 5926945.147 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>3000</xplan:typ>
    </xplan:BP_EinfahrtsbereichLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7e247472-b4d4-46bb-a3ad-56ad477beaeb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580678.84 5926988.62</gml:lowerCorner>
          <gml:upperCorner>580698.811 5927005.887</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3E77F068-F593-4FF7-B82E-8673C5C9EE51" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580698.811 5927005.437 580686.144 5927005.887 580686.101 5927004.91 
580686.008 5926996.771 580679.431 5926996.98 580679.279 5926992.347 
580678.965 5926992.357 580678.876 5926989.845 580678.84 5926988.83 
580685.402 5926988.62 580685.409 5926988.706 580685.69 5926992.154 
580698.329 5926989.642 580698.811 5927005.437 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_21f6f5a0-8197-4cd8-8f1d-fbfb8d4b55ee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580641.449 5926993.014</gml:lowerCorner>
          <gml:upperCorner>580659.111 5927008.014</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0D29AA4F-82E3-4BE4-85E8-52081942330C" srsName="EPSG:25832">
          <gml:posList>580641.915 5927008.014 580641.449 5926993.57 580658.64 5926993.014 
580659.111 5927007.451 580641.915 5927008.014 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a8385709-8b7e-438e-ab3f-72b9c4904d9a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580659.111 5927007.338</gml:lowerCorner>
          <gml:upperCorner>580662.554 5927007.451</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_89278D85-7A91-4949-A1A5-161DA7650E1B" srsName="EPSG:25832">
          <gml:posList>580662.554 5927007.338 580659.111 5927007.451 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_31cdfc92-82aa-4884-9560-74f6babe0357">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580648.413 5926957.152</gml:lowerCorner>
          <gml:upperCorner>580666.719 5926977.066</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">18</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7787861B-0C3C-4AED-A466-510458DB3A29" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580666.719 5926973.863 580652.065 5926977.066 580648.413 5926960.355 
580663.067 5926957.152 580666.719 5926973.863 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_25626ccc-f6af-4523-a37b-8d3503c41bc8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580686.144 5927005.433</gml:lowerCorner>
          <gml:upperCorner>580699.097 5927012.977</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">3.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_695D2784-4CFA-4BBE-86BC-FA9CAF7FDCA3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580698.924 5927005.478 580699.097 5927012.66 580686.385 5927012.977 
580686.144 5927005.887 580698.922 5927005.433 580698.924 5927005.478 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8ff6c751-3139-4110-b404-5048dbc78c0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580652.065 5926973.863</gml:lowerCorner>
          <gml:upperCorner>580667.839 5926982.19</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E5FDCFAC-B877-49EC-B8E4-42C5B069C3C0" srsName="EPSG:25832">
          <gml:posList>580666.719 5926973.863 580667.839 5926978.987 580653.185 5926982.19 
580652.065 5926977.066 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">15</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="GML_6c7a2054-61cb-4a0d-a705-36f9c88438f2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580618.206 5926957.927</gml:lowerCorner>
          <gml:upperCorner>580645.884 5926990.142</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_b36e4c50-a260-47f8-a0be-bedc34f071d3" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1495EB06-76AB-41CE-A05B-BFE30430CD6C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580645.884 5926984.423 580625.768 5926990.142 580618.206 5926964.172 
580640.154 5926957.927 580645.884 5926984.423 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3000</xplan:zweckbestimmung>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauLinie gml:id="GML_9a32990a-c598-4835-a01e-e014c691e688">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580686.144 5927005.478</gml:lowerCorner>
          <gml:upperCorner>580699.097 5927012.977</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2282411D-D841-4415-AEB0-C2A29C2F0EFA" srsName="EPSG:25832">
          <gml:posList>580698.924 5927005.478 580699.097 5927012.66 580686.385 5927012.977 
580686.144 5927005.887 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_5c3b26c6-2a48-4dc2-97d6-54b19db7426a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580648.413 5926957.152</gml:lowerCorner>
          <gml:upperCorner>580666.719 5926977.066</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B87885F2-ADD9-4C4D-BB54-BCBB0CBD2517" srsName="EPSG:25832">
          <gml:posList>580663.067 5926957.152 580666.719 5926973.863 580652.065 5926977.066 
580648.413 5926960.355 580663.067 5926957.152 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">15</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_1dddfd88-05bd-4adb-ab2a-61d798e718e1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580577.219 5926917.26</gml:lowerCorner>
          <gml:upperCorner>580716.969 5927028.4</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_01CB9B0A-058F-4809-8376-B820C8920BCC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580603.6 5927028.4 580590.1 5926979.3 580584.909 5926960.419 
580577.219 5926931.029 580581.355 5926928.063 580584.263 5926925.894 
580595.837 5926917.26 580598.728 5926921.052 580588.913 5926928.602 
580593.455 5926944.565 580595.259 5926950.944 580598.621 5926962.775 
580607.563 5926960.234 580609.321 5926966.42 580600.456 5926968.995 
580608.798 5926998.353 580601.671 5927000.369 580598.751 5927001.196 
580599.774 5927004.727 580599.989 5927005.465 580602.846 5927015.286 
580612.446 5927012.493 580622.152 5927009.669 580637.195 5927005.293 
580637.525 5927005.197 580638.377 5927008.13 580641.915 5927008.014 
580659.111 5927007.451 580679.752 5927006.774 580683.508 5927006.651 
580683.422 5927004.996 580686.101 5927004.91 580686.144 5927005.887 
580698.811 5927005.437 580698.329 5926989.642 580700.296 5926989.252 
580706.04 5926988.414 580710.294 5927009.269 580712.563 5927011.002 
580716.969 5927015.8 580706.93 5927023.855 580654.7 5927022.8 
580603.6 5927028.4 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_dfe19ff2-3d74-4cc9-b5ad-3f53450ac720">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580589.998 5926915.185</gml:lowerCorner>
          <gml:upperCorner>580698.811 5927008.13</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die eine GKF bestimmt ist</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_23F69C47-CD6D-4280-802D-32E381B49A64" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580683.422 5927004.996 580683.508 5927006.651 580679.752 5927006.774 
580662.554 5927007.338 580659.111 5927007.451 580641.915 5927008.014 
580638.377 5927008.13 580637.525 5927005.197 580633.178 5926991.464 
580632.734 5926989.936 580626.256 5926991.819 580618.117 5926963.865 
580613.951 5926965.075 580609.321 5926966.42 580600.456 5926968.995 
580598.621 5926962.775 580589.998 5926932.421 580601.062 5926924.253 
580604.904 5926929.521 580605.053 5926929.726 580605.206 5926929.936 
580609.396 5926930.592 580614.052 5926927.197 580636.742 5926932.788 
580647.084 5926928.672 580650.343 5926925.587 580652.649 5926923.404 
580657.31 5926918.1 580660.068 5926915.185 580661.898 5926916.639 
580665.845 5926929.955 580668.296 5926929.468 580669.794 5926937.007 
580669.951 5926936.976 580674.431 5926960.254 580676.602 5926971.542 
580679.031 5926984.164 580685.002 5926983.919 580685.402 5926988.62 
580685.69 5926992.154 580698.329 5926989.642 580698.811 5927005.437 
580686.144 5927005.887 580686.101 5927004.91 580683.422 5927004.996 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_0a850e29-203f-4033-bed2-c9ebab6faead">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580678.84 5926988.62</gml:lowerCorner>
          <gml:upperCorner>580698.811 5927006.774</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e9ddfe1a-1274-4805-a8c7-0aafb458448e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_3e7329c0-0fe4-4537-ac21-532c19f818c3" />
      <xplan:refTextInhalt xlink:href="#GML_d39dd5db-6402-486b-84cb-a2d890027b2e" />
      <xplan:refTextInhalt xlink:href="#GML_27742408-29ec-4f29-9d7c-38ffa1e310dd" />
      <xplan:refTextInhalt xlink:href="#GML_1ece4cf3-8a56-41c0-9dff-f999fb49f75e" />
      <xplan:refTextInhalt xlink:href="#GML_faae3eb5-5384-4cdb-99bf-ac288dc0868a" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_16F71F8D-F4D9-4273-82EB-71EBB8D412C1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580683.422 5927004.996 580683.508 5927006.651 580679.752 5927006.774 
580679.279 5926992.347 580678.965 5926992.357 580678.84 5926988.83 
580685.402 5926988.62 580685.69 5926992.154 580698.329 5926989.642 
580698.811 5927005.437 580686.144 5927005.887 580686.101 5927004.91 
580683.422 5927004.996 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GF uom="m2">2300</xplan:GF>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1600</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e9ddfe1a-1274-4805-a8c7-0aafb458448e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580691.134 5926998.44</gml:lowerCorner>
          <gml:upperCorner>580691.134 5926998.44</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0a850e29-203f-4033-bed2-c9ebab6faead" />
      <xplan:position>
        <gml:Point gml:id="Gml_965D85B6-FB9F-4A30-B556-4FB2EDB6BE35" srsName="EPSG:25832">
          <gml:pos>580691.134 5926998.44</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_fadb487b-0464-4589-9a3c-3e5684051175">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580644.217 5926937.956</gml:lowerCorner>
          <gml:upperCorner>580662.524 5926957.869</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0AF24FAA-6DD6-4AC7-A2BB-35D32BE7D655" srsName="EPSG:25832">
          <gml:posList>580644.217 5926941.159 580658.871 5926937.956 580662.524 5926954.666 
580647.87 5926957.869 580644.217 5926941.159 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">15</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9eb2645a-0815-4bef-8897-81ded5cd3537">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580662.088 5926992.347</gml:lowerCorner>
          <gml:upperCorner>580679.752 5927007.338</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2764E11F-83B4-4B34-A3EC-40277CE5D499" srsName="EPSG:25832">
          <gml:posList>580679.397 5926995.98 580679.431 5926996.98 580679.752 5927006.774 
580662.554 5927007.338 580662.088 5926992.903 580678.965 5926992.357 
580679.279 5926992.347 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_c1f195ca-9dab-44d1-9bf4-30123358b90a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580591.484 5926944.249</gml:lowerCorner>
          <gml:upperCorner>580596.503 5926951.473</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(a)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_00bdf88d-167c-4b19-9753-4975941691fd" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_26ED2631-C95C-4925-B31E-F950BB261C34" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580596.503 5926950.591 580593.39 5926951.473 580591.484 5926945.147 
580593.448 5926944.567 580594.566 5926944.249 580596.503 5926950.591 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_22a8081b-e6cf-4324-abc7-8c5536572e64">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580595.259 5926946.612</gml:lowerCorner>
          <gml:upperCorner>580613.868 5926962.775</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4BAA9777-76FD-425E-8447-6747AAA8D84A" srsName="EPSG:25832">
          <gml:posList>580613.862 5926958.42 580613.868 5926958.442 580607.563 5926960.234 
580598.621 5926962.775 580595.259 5926950.944 580610.507 5926946.612 
580613.862 5926958.42 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f27c9a7f-abb0-4cd4-9ba9-9fb20420111c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580633.178 5926989.845</gml:lowerCorner>
          <gml:upperCorner>580678.876 5926991.464</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8B40102F-9E36-45B5-977D-2FFF1E6D7361" srsName="EPSG:25832">
          <gml:posList>580633.178 5926991.464 580678.876 5926989.845 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3498c5be-9e1c-4463-848a-8f5e1126d09e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580643.149 5926933.071</gml:lowerCorner>
          <gml:upperCorner>580658.871 5926941.159</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C71F0929-F4C0-4009-99A5-253A91A8629B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580658.871 5926937.956 580644.217 5926941.159 580643.149 5926936.274 
580657.804 5926933.071 580658.871 5926937.956 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>3</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_9bebac07-0582-42d8-9510-81f1792b6e65">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580588.913 5926915.185</gml:lowerCorner>
          <gml:upperCorner>580685.402 5927015.286</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2cfc8897-5c5f-4a0c-9473-8d04db3a600e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e0334f01-d01e-42fa-b51a-acf5e1106aeb" />
      <xplan:refTextInhalt xlink:href="#GML_bcf54f8d-8c51-446b-8b77-6f7cd0400378" />
      <xplan:refTextInhalt xlink:href="#GML_e14afa10-ef93-4ab5-9201-9a39d0167e9b" />
      <xplan:refTextInhalt xlink:href="#GML_92965f23-95db-4a19-87d2-3b4e50c90a54" />
      <xplan:refTextInhalt xlink:href="#GML_f44819a8-d871-411c-b72d-b4b1952ac63e" />
      <xplan:refTextInhalt xlink:href="#GML_1ece4cf3-8a56-41c0-9dff-f999fb49f75e" />
      <xplan:refTextInhalt xlink:href="#GML_3e7329c0-0fe4-4537-ac21-532c19f818c3" />
      <xplan:refTextInhalt xlink:href="#GML_c8e86153-48b0-4851-a9e4-3ba5b683152d" />
      <xplan:refTextInhalt xlink:href="#GML_59458fc3-b530-42ac-8fcc-d34d138672e0" />
      <xplan:refTextInhalt xlink:href="#GML_d81b81ce-2712-4fdb-8536-bd7276c1fa41" />
      <xplan:refTextInhalt xlink:href="#GML_ff8301ef-9483-4ac8-9d65-06465ce06ade" />
      <xplan:refTextInhalt xlink:href="#GML_6227732b-c41d-448f-8bf2-ec341e904ef9" />
      <xplan:refTextInhalt xlink:href="#GML_0f4982ac-1dd1-45ef-81fc-accb4a8808f1" />
      <xplan:refTextInhalt xlink:href="#GML_fe93beae-ab11-4ce9-95c8-0468f6debaa7" />
      <xplan:refTextInhalt xlink:href="#GML_89f97931-d81e-4b5f-89c6-023299eadfb4" />
      <xplan:refTextInhalt xlink:href="#GML_989d638a-86bc-4ad7-bab9-fe0a42e4f56b" />
      <xplan:refTextInhalt xlink:href="#GML_f56aa949-4103-4315-b9a3-9acb728ad28c" />
      <xplan:refTextInhalt xlink:href="#GML_937c9efa-6290-49a8-9867-ffc2309c0dcb" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0CAEA6A2-9A3E-428F-9895-5F8B84F92249" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580637.195 5927005.293 580622.152 5927009.669 580612.446 5927012.493 
580602.846 5927015.286 580599.989 5927005.465 580599.774 5927004.727 
580598.751 5927001.196 580601.671 5927000.369 580608.798 5926998.353 
580600.456 5926968.995 580609.321 5926966.42 580607.563 5926960.234 
580598.621 5926962.775 580595.259 5926950.944 580593.455 5926944.565 
580588.913 5926928.602 580598.728 5926921.052 580601.062 5926924.253 
580604.904 5926929.521 580605.206 5926929.936 580609.396 5926930.592 
580614.052 5926927.197 580636.742 5926932.788 580647.084 5926928.672 
580650.343 5926925.587 580652.649 5926923.404 580657.31 5926918.1 
580660.068 5926915.185 580661.898 5926916.639 580665.845 5926929.955 
580668.296 5926929.468 580669.794 5926937.007 580669.951 5926936.976 
580674.431 5926960.254 580676.602 5926971.542 580679.031 5926984.164 
580685.002 5926983.919 580685.054 5926984.348 580685.402 5926988.62 
580678.84 5926988.83 580678.965 5926992.357 580679.279 5926992.347 
580679.752 5927006.774 580659.111 5927007.451 580641.915 5927008.014 
580638.377 5927008.13 580637.525 5927005.197 580637.195 5927005.293 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2cfc8897-5c5f-4a0c-9473-8d04db3a600e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580651.552 5926986.757</gml:lowerCorner>
          <gml:upperCorner>580651.552 5926986.757</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9bebac07-0582-42d8-9510-81f1792b6e65" />
      <xplan:position>
        <gml:Point gml:id="Gml_0C4F2962-CFED-45B7-B69B-8156F75DE7EC" srsName="EPSG:25832">
          <gml:pos>580651.552 5926986.757</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_5289d0ed-a04f-4c99-98c8-3614db2d2af2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580598.751 5926991.464</gml:lowerCorner>
          <gml:upperCorner>580637.525 5927015.286</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D6381CB1-8976-4A59-802D-511EEE16EF2A" srsName="EPSG:25832">
          <gml:posList>580598.751 5927001.196 580608.798 5926998.353 580633.178 5926991.464 
580637.525 5927005.197 580602.846 5927015.286 580599.774 5927004.727 
580598.751 5927001.196 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_364ef867-12ae-42c3-be91-b5bf1f0fe288">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580678.84 5926988.62</gml:lowerCorner>
          <gml:upperCorner>580698.924 5927005.887</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_949E1EC3-8033-4EF6-BD0C-0D2B746D31CF" srsName="EPSG:25832">
          <gml:posList>580686.144 5927005.887 580686.101 5927004.91 580686.008 5926996.771 
580679.431 5926996.98 580679.279 5926992.347 580678.965 5926992.357 
580678.876 5926989.845 580678.84 5926988.83 580685.402 5926988.62 
580685.69 5926992.154 580698.329 5926989.642 580698.924 5927005.478 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bec3cde9-11b5-4f15-831c-5fb31c58c993">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580588.913 5926925.672</gml:lowerCorner>
          <gml:upperCorner>580603.821 5926944.565</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BD8156C6-47E0-4308-9929-AF40AC038285" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580603.821 5926941.62 580593.455 5926944.565 580588.913 5926928.602 
580589.559 5926928.42 580599.338 5926925.672 580603.821 5926941.62 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_baf88722-7992-44ea-8541-eb3706370cfa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580643.149 5926933.071</gml:lowerCorner>
          <gml:upperCorner>580658.871 5926941.159</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C6B0ED46-D948-4184-BD50-1D6E57EEBB81" srsName="EPSG:25832">
          <gml:posList>580644.217 5926941.159 580643.149 5926936.274 580657.804 5926933.071 
580658.871 5926937.956 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">15</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_a2373422-6820-44b1-ac22-f2591629beb0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580593.39 5926950.596</gml:lowerCorner>
          <gml:upperCorner>580599.865 5926963.306</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(b)</xplan:gliederung1>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_00bdf88d-167c-4b19-9753-4975941691fd" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_26364B70-7FF5-4B4F-9907-60BE077F56F7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">580599.865 5926962.421 580598.621 5926962.775 580596.752 5926963.306 
580593.39 5926951.473 580595.259 5926950.944 580596.505 5926950.596 
580599.865 5926962.421 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9ee244f4-e9f0-475e-9c00-82790c1cdae5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>580637.525 5927005.197</gml:lowerCorner>
          <gml:upperCorner>580641.915 5927008.13</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_f2cf94b3-0d17-40ee-9603-15511ad06898" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7585434E-4BEB-4157-AA4F-DDBDC0053D60" srsName="EPSG:25832">
          <gml:posList>580641.915 5927008.014 580638.377 5927008.13 580637.525 5927005.197 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:bautiefe uom="m">17</xplan:bautiefe>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
</xplan:XPlanAuszug>