﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<XPlanAuszug xmlns:xplan="http://www.xplanung.de/xplangml/5/0" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/0 file:///D:/Daten/Dev/XPlanGML/Version_5.0/Spezifikation/XPlanGML_5_0_Schema/XPlanung-Operationen.xsd" gml:id="GML_bbcc2b37-5d95-4918-9f27-86e63302a5cc" xmlns="http://www.xplanung.de/xplangml/5/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>566993.556 5941350.650</gml:lowerCorner>
      <gml:upperCorner>567127.096 5941513.779</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_a253b53b-8bb8-45c9-ad35-881550e2d614">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566993.556 5941350.650</gml:lowerCorner>
          <gml:upperCorner>567127.096 5941513.779</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan001_5-0</xplan:name>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface srsName="EPSG:25832" gml:id="GML_234fa225-29e6-41ce-bd63-ea56e061661b">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25832" gml:id="GML_0bc3b58f-e2e4-4fd5-b3b6-c6015472a3f2">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsDimension="2" count="19">567062.494 5941513.779 567001.904 5941478.642 567015.093 5941455.900 567010.769 5941453.393 567013.634 5941448.454 566993.556 5941436.811 566997.678 5941429.704 566998.276 5941430.055 567000.052 5941429.146 567033.048 5941373.402 567033.826 5941373.851 567034.976 5941374.435 567036.209 5941374.811 567037.489 5941374.968 567040.045 5941370.631 567067.268 5941386.380 567089.392 5941350.650 567127.096 5941372.736 567062.494 5941513.779 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>407</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:plangeber>
        <xplan:XP_Plangeber>
          <xplan:name>407</xplan:name>
        </xplan:XP_Plangeber>
      </xplan:plangeber>
      <xplan:planArt>10000</xplan:planArt>
      <xplan:verfahren>2000</xplan:verfahren>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:bereich xlink:href="#GML_0d168307-8101-40ce-9fa6-55e5bfa1aa37" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_0d168307-8101-40ce-9fa6-55e5bfa1aa37">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566993.556 5941350.650</gml:lowerCorner>
          <gml:upperCorner>567127.096 5941513.779</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:rasterBasis xlink:href="#GML_59810859-e620-4d84-882c-6076adc7e2bb" />
      <xplan:gehoertZuPlan xlink:href="#GML_a253b53b-8bb8-45c9-ad35-881550e2d614" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_Rasterdarstellung gml:id="GML_59810859-e620-4d84-882c-6076adc7e2bb">
      <xplan:refScan>
        <xplan:XP_SpezExterneReferenz>
          <xplan:georefURL>BPlan001_5-0.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan001_5-0.png</xplan:referenzURL>
          <xplan:typ>1000</xplan:typ>
        </xplan:XP_SpezExterneReferenz>
      </xplan:refScan>
    </xplan:XP_Rasterdarstellung>
  </gml:featureMember>
</XPlanAuszug>