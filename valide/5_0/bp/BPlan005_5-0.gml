﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_283A1C37-0024-4311-990D-F8B5ED8E765E" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/0 http://www.xplanungwiki.de/upload/XPlanGML/5.0/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
      <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_88bfe952-199f-4bba-bea2-c2b441737144">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan005_5-0</xplan:name>
      <xplan:beschreibung>Der vorhabenbezogene Bebauungsplan BPlan005_5-0
für den Geltungsbereich Knickweg – Barmbeker Straße –
Gertigstraße
(Bezirk Hamburg-Nord, Ortsteil 412) wird festgestellt.</xplan:beschreibung>
      <xplan:technHerstellDatum>2017-03-20</xplan:technHerstellDatum>
      <xplan:aendert />
      <xplan:wurdeGeaendertVon />
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_802D4D8A-5B93-4CD2-8478-715C0614D36D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:externeReferenz>
        <xplan:XP_SpezExterneReferenz>
          <xplan:art>Dokument</xplan:art>
          <xplan:referenzURL>BPlan005_5-0.pdf</xplan:referenzURL>
          <xplan:typ>9999</xplan:typ>
        </xplan:XP_SpezExterneReferenz>
      </xplan:externeReferenz>
      <xplan:texte xlink:href="#GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa" />
      <xplan:texte xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:texte xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:texte xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:texte xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:texte xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:texte xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:texte xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:texte xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>412</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:plangeber />
      <xplan:planArt>3000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2016-10-25</xplan:rechtsverordnungsDatum>
      <xplan:durchfuehrungsVertrag>true</xplan:durchfuehrungsVertrag>
      <xplan:bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Im Plangebiet ist in den Schlafräumen durch geeignete bauliche
Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden,
verglaste Loggien, Wintergärten, besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
sicherzustellen, dass ein Innenraumpegel bei
gekipptem Fenster von 30 dB(A) während der Nachtzeit
nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme
in Form von verglasten Loggien oder Wintergärten
muss dieser Innenraumpegel bei gekippten/teilgeöffneten
Bauteilen erreicht werden. Wohn-/Schlafräume in
Einzimmerwohnungen und Kinderzimmer sind wie Schlafräume
zu beurteilen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0d37aade-f41e-4ec9-b026-79d4709945bf">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die nicht überbauten Flächen
von Tiefgaragen mit einem mindestens 50 cm starken
durchwurzelbaren Substrataufbau zu versehen und gärtnerisch
anzulegen. Hiervon ausgenommen sind die erforderlichen
Flächen für Terrassen, Wege und Freitreppen, Kinderspielflächen
sowie Bereiche, die der Belichtung, Be- und
Entlüftung oder der Aufnahme von technischen Anlagen
dienen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Stellplätze nur in Tiefgaragen
zulässig. Tiefgaragen sind auch außerhalb der überbaubaren
Grundstücksflächen zulässig. Tiefgaragen dürfen
einschließlich ihrer Überdeckung nicht über die natürliche
Geländeoberfläche herausragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die Freiflächen zur öffentlichen
Grünanlage mit einer Hecke abzupflanzen. Für die
Hecke sind standortgerechte einheimische Gehölze zu verwenden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2d0243f5-23e4-4f40-8538-d833e9c168dd">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind mindestens 55 vom Hundert
(v. H.) der Dachflächen und auf der Fläche für den
besonderen Nutzungszweck – Kiosk – mindestens 80 v. H.
der Dachflächen mit einem mindestens 10 cm starken
durchwurzelbaren Substrataufbau zu versehen und extensiv
zu begrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6bac8f46-a325-4392-b115-07bb0e41e8c1">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Terrassen im Anschluss
an die Hauptnutzung bis zu einer Tiefe von 4 m auch außerhalb
der überbaubaren Grundstücksfläche zulässig. Untergeordnete
Bauteile wie Vordächer, Balkone und Erker im
Bereich von öffentlichen Grünflächen sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind innerhalb der mit „(A)“
bezeichneten Fläche (Vorhabengebiet) im Rahmen der festgesetzten
Nutzungen nur solche Vorhaben zulässig, zu
deren Durchführung sich der Vorhabenträger im Durchführungsvertrag
verpflichtet.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet darf die festgesetzte Grundflächenzahl
für Tiefgaragen bis zu einer Grundflächenzahl
von 1,0 überschritten werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_114d1690-3976-42e3-87be-ec97b7ad773d">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind auf den Flurstücken 1477,
1478 und 1480 der Gemarkung Winterhude vor den straßenzugewandten
Fenstern der Wohn- und Schlafräume
lärmgeschützte Außenbereiche durch bauliche Schallschutzmaßnahmen,
wie etwa verglaste Loggien, Wintergärten
oder in ihrer Wirkung vergleichbare Maßnahmen zwingend
vorzusehen. In den lärmgeschützten Außenbereichen
ist bei geöffneten Fenstern/Bauteilen sicherzustellen, dass
ein Tagpegel von weniger als 65 dB(A) erreicht wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_6e871e4c-58e1-4b97-bf2a-a696816b2458">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_1B1B6AE8-6B92-4F30-B631-1075E8F42A23" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>     
      <xplan:planinhalt xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <xplan:planinhalt xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:planinhalt xlink:href="#GML_5276e223-57f7-4596-9c9c-8fb909e11db2" />     
      <xplan:planinhalt xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:planinhalt xlink:href="#GML_354dc6b6-d910-4f29-8aeb-66d238013da6" />
      <xplan:planinhalt xlink:href="#GML_b607f50f-8221-4811-81f0-7f6efef25db3" />
      <xplan:planinhalt xlink:href="#GML_20f74701-9a0b-4c38-89b8-ae81db151e28" />
      <xplan:planinhalt xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:planinhalt xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:planinhalt xlink:href="#GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f" />
      <xplan:planinhalt xlink:href="#GML_9875e68a-2b21-40e4-8f66-ecf5c2290936" />
      <xplan:planinhalt xlink:href="#GML_7b11be88-979b-483f-8042-82c51eff70af" />
      <xplan:planinhalt xlink:href="#GML_be2c01dd-4d15-4a6a-85c0-dc9565324486" />
      <xplan:planinhalt xlink:href="#GML_21c90a12-5886-40dd-8a86-463267b6e201" />
      <xplan:planinhalt xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <xplan:planinhalt xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:planinhalt xlink:href="#GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299" />
      <xplan:planinhalt xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:planinhalt xlink:href="#GML_8376d624-8ccb-473d-81df-fc95b1ef28b3" />    
      <xplan:planinhalt xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:planinhalt xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <xplan:planinhalt xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:planinhalt xlink:href="#GML_5355860b-25fa-401c-a2ea-bb70654d17ab" />
      <xplan:planinhalt xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
      <xplan:praesentationsobjekt xlink:href="#GML_de628759-edb3-4f31-9d43-7d4f816bfb0c" />
      <xplan:praesentationsobjekt xlink:href="#GML_82075a17-ae02-4228-a4ac-debedfd66c1e" />
      <xplan:praesentationsobjekt xlink:href="#GML_a435c756-f459-41ce-b27c-d5992aa8f87e" />
      <xplan:praesentationsobjekt xlink:href="#GML_7d7588c9-1211-471e-ad15-8dd9f6605701" />
      <xplan:praesentationsobjekt xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <xplan:praesentationsobjekt xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <xplan:praesentationsobjekt xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <xplan:praesentationsobjekt xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <xplan:praesentationsobjekt xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <xplan:praesentationsobjekt xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <xplan:praesentationsobjekt xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <xplan:praesentationsobjekt xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <xplan:praesentationsobjekt xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <xplan:praesentationsobjekt xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <xplan:praesentationsobjekt xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <xplan:praesentationsobjekt xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <xplan:praesentationsobjekt xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <xplan:praesentationsobjekt xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <xplan:praesentationsobjekt xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <xplan:praesentationsobjekt xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <xplan:praesentationsobjekt xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <xplan:praesentationsobjekt xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <xplan:praesentationsobjekt xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <xplan:praesentationsobjekt xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <xplan:praesentationsobjekt xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <xplan:praesentationsobjekt xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />
      <xplan:gehoertZuPlan xlink:href="#GML_88bfe952-199f-4bba-bea2-c2b441737144" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2B445C08-E949-4FF9-ADB2-B5089542D385" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567433.287 5937679.305 567416.015 5937672.057 567419.988 5937662.431 
567432.605 5937667.621 567439.105 5937652.001 567428.275 5937631.879 
567422.612 5937629.344 567428.448 5937616.596 567434.138 5937619.1 
567436.484 5937623.475 567442.246 5937634.25 567452.958 5937654.188 
567450.566 5937668.248 567438.775 5937666.233 567433.287 5937679.305 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567437.222 5937662.769</gml:lowerCorner>
          <gml:upperCorner>567437.222 5937662.769</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <xplan:position>
        <gml:Point gml:id="Gml_1307CCE9-CF7F-4D45-910A-F7F32F738E67" srsName="EPSG:25832">
          <gml:pos>567437.222 5937662.769</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_421E1709-BEAC-4126-8B11-33ABB6776D0F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567452.175 5937661.631 567442.18 5937659.754 
567439.277 5937659.277 567439.035 5937655.06 567438.79 5937652.388 
567432.026 5937639.916 567442.758 5937634.17 567453.455 5937654.108 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_045ae35f-52f5-48d9-b5a6-3d5f339418be">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567444.114 5937651.76</gml:lowerCorner>
          <gml:upperCorner>567444.114 5937651.76</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:position>
        <gml:Point gml:id="Gml_C8CEE6CE-4C6C-4B22-B69F-5EB7E0A6633E" srsName="EPSG:25832">
          <gml:pos>567444.114 5937651.76</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5890c370-a3bc-412c-b650-13894b87a426">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.328 5937644.053</gml:lowerCorner>
          <gml:upperCorner>567439.328 5937644.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:position>
        <gml:Point gml:id="Gml_4904AA93-64C0-42F7-B5DD-2F2AC333A0F3" srsName="EPSG:25832">
          <gml:pos>567439.328 5937644.053</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_5276e223-57f7-4596-9c9c-8fb909e11db2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_BA6B8B46-908A-4E9B-863D-703E9385D513" srsName="EPSG:25832">
          <gml:posList>567416.015 5937672.057 567419.988 5937662.431 567432.605 5937667.621 
567439.105 5937652.001 567428.275 5937631.879 567422.612 5937629.344 
567428.448 5937616.596 567434.138 5937619.1 567436.484 5937623.475 
567442.246 5937634.25 567452.958 5937654.188 567450.566 5937668.248 
567438.775 5937666.233 567433.287 5937679.305 567416.015 5937672.057 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>  
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e21981e2-40a7-4dac-84b0-c88d371030af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">23.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_1D5B44C5-69E9-499E-A51D-9573D4140391" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567442.758 5937634.17 567432.026 5937639.916 
567427.969 5937632.352 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_10c25c52-be4f-422d-8171-469001af2bf4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.835 5937628.058</gml:lowerCorner>
          <gml:upperCorner>567426.835 5937628.058</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:position>
        <gml:Point gml:id="Gml_977E3183-DC7F-4FC8-BF6F-EB513B883C57" srsName="EPSG:25832">
          <gml:pos>567426.835 5937628.058</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567433.633 5937633.755</gml:lowerCorner>
          <gml:upperCorner>567433.633 5937633.755</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:position>
        <gml:Point gml:id="Gml_B372299C-8B7F-451E-9D19-A5C0F14DD14D" srsName="EPSG:25832">
          <gml:pos>567433.633 5937633.755</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_354dc6b6-d910-4f29-8aeb-66d238013da6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8EEF4677-22B5-41E2-A9C8-3C3492BB9191" srsName="EPSG:25832">
          <gml:posList>567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
567437.269 5937671.067 567433.538 5937679.955 567415.353 5937672.325 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b607f50f-8221-4811-81f0-7f6efef25db3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_914199BC-62B9-4BC5-9B0E-B512630315F3" srsName="EPSG:25832">
          <gml:posList>567428.223 5937615.867 567434.477 5937618.73 567442.758 5937634.17 
567432.026 5937639.916 567427.969 5937632.352 567421.935 5937629.605 
567428.223 5937615.867 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_20f74701-9a0b-4c38-89b8-ae81db151e28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3A87DB05-2C63-4264-A5A6-5E312FA606BC" srsName="EPSG:25832">
          <gml:posList>567450.952 5937668.824 567440.924 5937667.111 567442.18 5937659.754 
567452.173 5937661.63 567450.952 5937668.824 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B2E22F6C-FF3D-45FE-8566-443C9A86E940" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567451.021 5937668.836 567448.434 5937683.72 
567445.762 5937685.099 567443.507 5937686.264 567432.779 5937681.762 
567433.538 5937679.955 567415.353 5937672.325 567418.938 5937663.64 
567419.705 5937661.78 567432.297 5937667.063 567438.475 5937652.392 
567438.319 5937651.648 567428.755 5937633.817 567427.969 5937632.352 
567424.532 5937630.787 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 567442.758 5937634.17 567453.455 5937654.108 
567452.176 5937661.63 567450.952 5937668.824 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.9</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567425.133 5937653.994</gml:lowerCorner>
          <gml:upperCorner>567425.133 5937653.994</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_A2540255-FB1C-4B02-BCD6-077E912C1174" srsName="EPSG:25832">
          <gml:pos>567425.133 5937653.994</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.301 5937671.953</gml:lowerCorner>
          <gml:upperCorner>567428.301 5937671.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_A75AA363-F3FC-457F-9D09-48303ED1A07F" srsName="EPSG:25832">
          <gml:pos>567428.301 5937671.953</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c34619af-a786-41f0-a899-8a67df7b6750">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.689 5937670.707</gml:lowerCorner>
          <gml:upperCorner>567434.689 5937670.707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_39376590-F37F-47D0-9221-67E152E347EF" srsName="EPSG:25832">
          <gml:pos>567434.689 5937670.707</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">26.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_19B70A84-951E-4A99-BBC7-9605E328F98D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567452.173 5937661.63 567450.952 5937668.824 567440.924 5937667.111 
567442.18 5937659.754 567452.173 5937661.63 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9036c79b-5e09-4030-8c15-e0d9877d084c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567445.543 5937665.568</gml:lowerCorner>
          <gml:upperCorner>567445.543 5937665.568</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:position>
        <gml:Point gml:id="Gml_9ADA73EB-FCF1-4457-9F43-50BA1E4AD3D9" srsName="EPSG:25832">
          <gml:pos>567445.543 5937665.568</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fba093ee-e774-4846-b286-e46649619852">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567443.466 5937662.039</gml:lowerCorner>
          <gml:upperCorner>567443.466 5937662.039</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:position>
        <gml:Point gml:id="Gml_91699F0B-DBE8-421D-B568-E7039FF09588" srsName="EPSG:25832">
          <gml:pos>567443.466 5937662.039</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_28D17969-FDFF-4F95-AB56-F49C807E6278" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567416.408 5937613.826 567412.057 5937623.382 567405.121 5937620.224 
567409.472 5937610.668 567416.408 5937613.826 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9875e68a-2b21-40e4-8f66-ecf5c2290936">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_07979832-0675-47A5-A984-D0571C1BFC23" srsName="EPSG:25832">
          <gml:posList>567439.035 5937655.06 567438.79 5937652.388 567432.026 5937639.916 
567442.758 5937634.17 567453.455 5937654.108 567452.175 5937661.631 
567442.18 5937659.754 567439.277 5937659.277 567439.035 5937655.06 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_7b11be88-979b-483f-8042-82c51eff70af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_BAF0C15B-5F21-4336-90E6-7EFEEE3B820A" srsName="EPSG:25832">
          <gml:posList>567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
567448.434 5937683.72 567443.507 5937686.264 567432.779 5937681.762 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_be2c01dd-4d15-4a6a-85c0-dc9565324486">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567431.741 5937675.181</gml:lowerCorner>
          <gml:upperCorner>567432.929 5937679.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1B38DC73-B815-4DDE-B16E-ACA0F2678DFE" srsName="EPSG:25832">
          <gml:posList>567432.929 5937675.181 567431.741 5937679.201 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_21c90a12-5886-40dd-8a86-463267b6e201">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_826EA6D9-C6CE-40E1-A007-F75A95954647" srsName="EPSG:25832">
          <gml:posList>567407.329 5937656.657 567419.705 5937661.78 567415.353 5937672.325 
567403 5937667.139 567407.329 5937656.657 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UnverbindlicheVormerkung gml:id="GML_cf3318a5-fc25-4f6b-a594-893928f6a3de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.825 5937635.581</gml:lowerCorner>
          <gml:upperCorner>567468.867 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A17CC2D7-8FC0-405F-9921-0941914F9AC3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567468.867 5937663.607 567465.27 5937684.42 567462.07 5937698.959 
567458.005 5937697.252 567460.825 5937687.181 567456.625 5937685.781 
567452.825 5937636.781 567458.625 5937635.581 567459.825 5937647.581 
567467.425 5937647.381 567468.867 5937663.607 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:vormerkung>vorgesehene Bahnanlage</xplan:vormerkung>
    </xplan:BP_UnverbindlicheVormerkung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567457.938 5937683.621</gml:lowerCorner>
          <gml:upperCorner>567457.938 5937683.621</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>vormerkung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <xplan:schriftinhalt>vorgesehene Bahnanlage</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_B0178893-375E-4277-83F6-A7BC98527B54" srsName="EPSG:25832">
          <gml:pos>567457.938 5937683.621</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">282.22</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_9a4506b9-78b2-4dc4-bb55-088a4c627872">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567394.606 5937621.126</gml:lowerCorner>
          <gml:upperCorner>567438.475 5937667.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_79953D84-97EE-45F5-AB1B-35982A0BBAAD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567432.297 5937667.063 567419.705 5937661.78 567405.227 5937655.787 
567401.59 5937657.649 567394.606 5937655.401 567403.316 5937621.126 
567421.935 5937629.605 567424.532 5937630.787 567427.969 5937632.352 
567428.755 5937633.817 567438.319 5937651.648 567438.475 5937652.392 
567432.297 5937667.063 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567409.089 5937633.593</gml:lowerCorner>
          <gml:upperCorner>567409.089 5937633.593</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:position>
        <gml:Point gml:id="Gml_2E9BEC71-9E86-477F-856C-9C51007FD26A" srsName="EPSG:25832">
          <gml:pos>567409.089 5937633.593</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567406.396 5937643.214</gml:lowerCorner>
          <gml:upperCorner>567406.396 5937643.214</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:schriftinhalt>Spielplatz</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_9EB076FE-B5F7-427B-A934-80467698D33A" srsName="EPSG:25832">
          <gml:pos>567406.396 5937643.214</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_39594D79-573F-4764-BCE8-32C60E9A516F" srsName="EPSG:25832">
          <gml:posList>567409.472 5937610.668 567416.408 5937613.826 567412.057 5937623.382 
567405.121 5937620.224 567409.472 5937610.668 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_321bdb15-2400-4c44-a553-35f402f8743e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937655.401</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DCD817A3-A006-4C34-8442-2F14CFA96FC3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567418.938 5937663.64 567415.353 5937672.325 
567403 5937667.139 567395.687 5937664.07 567393.37 5937660.265 
567394.606 5937655.401 567401.59 5937657.649 567405.227 5937655.787 
567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_af37976e-6da1-4e93-a376-ad285c900b4c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.836 5937653.516</gml:lowerCorner>
          <gml:upperCorner>567399.836 5937653.516</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_FFA7CDEC-C94D-4384-BDD7-1909A640B640" srsName="EPSG:25832">
          <gml:pos>567399.836 5937653.516</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.427 5937662.618</gml:lowerCorner>
          <gml:upperCorner>567416.427 5937662.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_1FFBFE44-6616-4B0E-AB80-498F28BA585E" srsName="EPSG:25832">
          <gml:pos>567416.427 5937662.618</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567410.188 5937663.997</gml:lowerCorner>
          <gml:upperCorner>567410.188 5937663.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_DC453FE9-2041-45AF-A8E1-578F10048B57" srsName="EPSG:25832">
          <gml:pos>567410.188 5937663.997</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_8376d624-8ccb-473d-81df-fc95b1ef28b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8FBEA762-3988-465C-96DA-4D28F29514D7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567462.07 5937698.959 567446.882 5937692.583 567390.995 5937669.124 
567386.293 5937667.145 567389.479 5937654.684 567404.151 5937595.479 
567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_A943A156-D599-468D-A6E0-729F67AFF9CE" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CEA48323-AFC3-40DD-BB52-A2013E93F4BF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DC8A9650-E572-40D9-B814-A6F953C77125" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1AAEEFDE-9BB8-4347-98D9-076F82851B46" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_90F50681-7567-4D84-8FD8-4FF183D255CF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2EF12D08-20CE-4313-AA35-76282694CF37" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_25E11F33-50B3-40F2-BFCC-48D43C96FFAF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C30A0811-BAB3-4710-BA4B-A487126B6392" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D4A0A2BA-0B70-472A-AB40-3768ABE3C486" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8CE5256B-8460-4F93-9EC3-5D62BB27D018" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4B6415AC-719C-42A2-AFEC-B2798A052B72" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6E50C4A5-A6D8-4704-9C06-6B24B2689CB3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E42A5C36-B284-4084-AF2A-F2B03DB88AC9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6C1F452F-6FB2-4168-9363-C07577A985E7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4A4C71C6-79AC-44AF-9D1B-DA682B8C8DC2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B4674BFD-BC3A-4479-9FDD-9F7197FBAEEE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_445171E1-01BB-4996-ACCF-581E68846F8D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_64ACEBDD-CB03-4FCF-ADA6-288C92C2F186" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6ED1C6E8-4A8F-4037-BF7F-CE310124AFD6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_757B5B8F-0570-46A6-96D2-B69657CC33F5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BesondererNutzungszweckFlaeche gml:id="GML_9775f239-210a-4d61-ad8f-251f5a31296e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403.316 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567428.223 5937629.605</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BE409F1A-0A68-4B05-A74F-CF9C61A66726" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_04F8D1D4-2F9A-4D77-B493-5AF8D1EFD268" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567407.834857796 5937607.72206805 567410.377 5937607.7 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1AE2DADD-B10C-46C5-8264-1B35E82BA77B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E9106369-6CAA-4F8E-AED0-6D2408FE2FCB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567421.935 5937629.605 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A62AA9D0-6925-4BF1-AB53-98637A92AC26" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567421.935 5937629.605 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2B8FB867-2ED6-4CD5-8EE1-B48B108CD780" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567406.223 5937609.688 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:zweckbestimmung>Kiosk</xplan:zweckbestimmung>
    </xplan:BP_BesondererNutzungszweckFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567412.297 5937615.306</gml:lowerCorner>
          <gml:upperCorner>567412.297 5937615.306</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:position>
        <gml:Point gml:id="Gml_AAF95A84-D78A-4878-9ECB-8134D6183E06" srsName="EPSG:25832">
          <gml:pos>567412.297 5937615.306</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f44449a4-6628-4c84-9dfe-d82b4dafca06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.532 5937621.891</gml:lowerCorner>
          <gml:upperCorner>567416.532 5937621.891</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>rechtscharakter</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:position>
        <gml:Point gml:id="Gml_E17DFDE1-D839-47B7-A202-2B5A8801EE89" srsName="EPSG:25832">
          <gml:pos>567416.532 5937621.891</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_55177fec-7599-4785-a8e2-806742a9b2d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">22.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6173821E-73B0-42AB-AF3C-A365E42007F8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567415.353 5937672.325 567403 5937667.139 
567407.329 5937656.657 567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_796aa811-807f-4550-abab-24b9cc8aa359">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.862 5937659</gml:lowerCorner>
          <gml:upperCorner>567421.862 5937659</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <xplan:position>
        <gml:Point gml:id="Gml_A8F01F30-4F18-4EEC-AA22-13749E13111A" srsName="EPSG:25832">
          <gml:pos>567421.862 5937659</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bff033ce-687f-48fe-b27c-542b545fb292">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_061661B3-137C-4715-870C-B0E7B6D13C82" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567448.434 5937683.72 567443.507 5937686.264 
567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_190f5132-7a3c-4e3f-900e-0705faba810a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.986 5937674.081</gml:lowerCorner>
          <gml:upperCorner>567439.986 5937674.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:position>
        <gml:Point gml:id="Gml_CE01C3F3-142B-4F8E-A60A-8CD7A177F747" srsName="EPSG:25832">
          <gml:pos>567439.986 5937674.081</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.194 5937679.946</gml:lowerCorner>
          <gml:upperCorner>567440.194 5937679.946</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:position>
        <gml:Point gml:id="Gml_F43BA970-BA11-490C-8A1B-518E98930F44" srsName="EPSG:25832">
          <gml:pos>567440.194 5937679.946</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_5355860b-25fa-401c-a2ea-bb70654d17ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_96460C3B-2392-44EF-B888-852FCFA16BB9" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e1568a2b-1768-4889-aedb-95449c029c25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">22.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_30C0B793-9A3E-4778-BBB9-39574BE34D26" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567437.993 5937669.342 567437.269 5937671.067 567433.538 5937679.955 
567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.339 5937668.137</gml:lowerCorner>
          <gml:upperCorner>567421.339 5937668.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
      <xplan:position>
        <gml:Point gml:id="Gml_C5F87EB5-FD28-49F8-A74F-404DFDB8613B" srsName="EPSG:25832">
          <gml:pos>567421.339 5937668.137</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_de628759-edb3-4f31-9d43-7d4f816bfb0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.484 5937661.969</gml:lowerCorner>
          <gml:upperCorner>567427.781 5937668.164</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A18BF9A5-0FD3-4A88-8055-092E2BB217E4" srsName="EPSG:25832">
          <gml:posList>567426.484 5937661.969 567427.781 5937668.164 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_82075a17-ae02-4228-a4ac-debedfd66c1e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.794 5937658.752</gml:lowerCorner>
          <gml:upperCorner>567417.901 5937660.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_AAEF15C8-29C1-4856-A07B-361B9D2569C3" srsName="EPSG:25832">
          <gml:posList>567417.901 5937658.752 567411.794 5937660.897 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_a435c756-f459-41ce-b27c-d5992aa8f87e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.065 5937655.343</gml:lowerCorner>
          <gml:upperCorner>567437.337 5937661.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6ACB5AF8-AD8A-49D3-B299-8493490433EE" srsName="EPSG:25832">
          <gml:posList>567434.065 5937655.343 567437.337 5937661.831 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_7d7588c9-1211-471e-ad15-8dd9f6605701">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567400.372 5937656.059</gml:lowerCorner>
          <gml:upperCorner>567401.041 5937660.34</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_BF1B6A91-C932-4F56-8783-4F9E6A4D5A52" srsName="EPSG:25832">
          <gml:posList>567401.041 5937656.059 567400.372 5937660.34 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>  
</xplan:XPlanAuszug>