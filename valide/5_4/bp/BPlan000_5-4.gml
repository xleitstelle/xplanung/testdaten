﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<xplan:XPlanAuszug xmlns:xplan="http://www.xplanung.de/xplangml/5/4" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:wfs="http://www.opengis.net/wfs" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/4 http://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/5.4/XPlanung-Operationen.xsd" gml:id="GML_92932193-48eb-463a-9f5f-7e4684395ae7">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>567015.8040 5937951.7580</gml:lowerCorner>
      <gml:upperCorner>567582.8240 5938562.2710</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_9a86c33a-15a6-40ff-bd41-ae8209335c6c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>Testdaten</xplan:name>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface srsName="EPSG:25832" gml:id="GML_08c322e2-de26-44d5-9f48-c8a866289259">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25832" gml:id="GML_72a77f47-92b7-45ed-9ab9-46db38dab9d0">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsDimension="2" count="40">567015.8040 5937951.7580 567043.8114 5937951.7580 567093.7835 5937951.7580 567118.7706 5937951.7580 567148.7349 5937951.7580 567205.7353 5937951.7580 567267.9361 5937951.7580 567298.5760 5937951.7580 567362.5115 5937951.7580 567396.8544 5937951.7580 567486.5409 5937951.7580 567525.5484 5937951.7580 567547.6498 5937951.7580 567582.8240 5937951.7580 567582.8240 5938061.5679 567582.8240 5938088.1886 567582.8240 5938149.5842 567582.8240 5938171.8087 567582.8240 5938193.6392 567582.8240 5938324.0110 567582.8240 5938379.2574 567582.8240 5938510.0532 567582.8240 5938512.9477 567582.8240 5938562.2710 567416.5001 5938562.2710 567292.0285 5938562.2710 567279.9220 5938562.2710 567258.8304 5938562.2710 567233.1368 5938562.2710 567197.9791 5938562.2710 567127.2264 5938562.2710 567015.8040 5938562.2710 567015.8040 5938476.8211 567015.8040 5938447.0744 567015.8040 5938422.6255 567015.8040 5938389.1105 567015.8040 5938258.2621 567015.8040 5938110.3099 567015.8040 5938087.0120 567015.8040 5937951.7580 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_745cbce1-ab01-4c1d-8b64-e1c113ac9c27" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:bereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_3c383c11-1942-41d9-9866-1f2216925566">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:MultiSurface srsName="EPSG:25832" gml:id="GML_832d144d-a593-44f6-babc-007fd4d14dc0">
          <gml:surfaceMember>
            <gml:Polygon srsName="EPSG:25832" gml:id="GML_b78319a4-fefc-41b3-8a49-0bf333d791cc">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsDimension="2" count="40">567015.8040 5937951.7580 567043.8114 5937951.7580 567093.7835 5937951.7580 567118.7706 5937951.7580 567148.7349 5937951.7580 567205.7353 5937951.7580 567267.9361 5937951.7580 567298.5760 5937951.7580 567362.5115 5937951.7580 567396.8544 5937951.7580 567486.5409 5937951.7580 567525.5484 5937951.7580 567547.6498 5937951.7580 567582.8240 5937951.7580 567582.8240 5938061.5679 567582.8240 5938088.1886 567582.8240 5938149.5842 567582.8240 5938171.8087 567582.8240 5938193.6392 567582.8240 5938324.0110 567582.8240 5938379.2574 567582.8240 5938510.0532 567582.8240 5938512.9477 567582.8240 5938562.2710 567416.5001 5938562.2710 567292.0285 5938562.2710 567279.9220 5938562.2710 567258.8304 5938562.2710 567233.1368 5938562.2710 567197.9791 5938562.2710 567127.2264 5938562.2710 567015.8040 5938562.2710 567015.8040 5938476.8211 567015.8040 5938447.0744 567015.8040 5938422.6255 567015.8040 5938389.1105 567015.8040 5938258.2621 567015.8040 5938110.3099 567015.8040 5938087.0120 567015.8040 5937951.7580 </gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:geltungsbereich>
      <xplan:planinhalt xlink:href="#GML_dc60c672-b6ca-442c-b761-f56982073dd5" />
      <xplan:planinhalt xlink:href="#GML_df5f8049-6e74-4625-9771-3816a8cb488e" />
      <xplan:planinhalt xlink:href="#GML_90b9d6bb-761f-424f-a626-f968f4be24ab" />
      <xplan:planinhalt xlink:href="#GML_466718d3-cad4-4953-9ca0-01200cf301f5" />
      <xplan:planinhalt xlink:href="#GML_60fc0a9d-d553-4aa5-ad6f-a7e29c6fa27b" />
      <xplan:planinhalt xlink:href="#GML_93b1097e-32cd-4062-8dc2-a4d47c7d1f21" />
      <xplan:planinhalt xlink:href="#GML_f0464838-d00f-4254-96c6-1390b7e17b89" />
      <xplan:planinhalt xlink:href="#GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6" />
      <xplan:planinhalt xlink:href="#GML_82f81025-67e3-479b-89bc-fc71a49b00f7" />
      <xplan:planinhalt xlink:href="#GML_02d77422-eeca-43eb-b9ca-a7f50edf2568" />
      <xplan:planinhalt xlink:href="#GML_82b4c1e5-6ad6-4418-a886-4b8c3d28f6e8" />
      <xplan:planinhalt xlink:href="#GML_611be92c-f6e2-48c5-99a9-563cc1f00241" />
      <xplan:planinhalt xlink:href="#GML_e6cd43b8-806e-4971-be2a-a8d252be2aea" />
      <xplan:planinhalt xlink:href="#GML_0534d970-7943-46c9-a235-e0cb3ccd3ca6" />
      <xplan:planinhalt xlink:href="#GML_580f8406-cb80-4d3e-a4bd-ddde3dd81ed4" />
      <xplan:planinhalt xlink:href="#GML_43a25431-a698-451a-af8d-ea0781ac5662" />
      <xplan:planinhalt xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:planinhalt xlink:href="#GML_0adf1e25-25cc-4d18-9fc5-1a8d5ae0e289" />
      <xplan:planinhalt xlink:href="#GML_6e3fef2a-c90f-4938-8220-4927fa563eda" />
      <xplan:planinhalt xlink:href="#GML_3d82bfa9-cf92-45d6-879f-0f3382017d06" />
      <xplan:planinhalt xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:planinhalt xlink:href="#GML_5f2b1898-c231-416c-8167-8ad36a8edb83" />
      <xplan:planinhalt xlink:href="#GML_bdb12f92-c818-4c99-91da-14c3a319192c" />
      <xplan:planinhalt xlink:href="#GML_62517d2f-46c3-4130-b39e-1ed9b30da567" />
      <xplan:planinhalt xlink:href="#GML_f69ae941-a696-4d86-aa3e-fee4a1fa44eb" />
      <xplan:planinhalt xlink:href="#GML_d373b68e-e6ef-4a46-93ee-ddc72d50fa0c" />
      <xplan:planinhalt xlink:href="#GML_fc4d2da9-d2f0-4117-89a1-bc7e9a715b81" />
      <xplan:planinhalt xlink:href="#GML_f65a30d4-4ab5-46ec-b132-871ff48cd567" />
      <xplan:planinhalt xlink:href="#GML_8fd437f4-91ba-44cc-b5b1-bf484f1e5da1" />
      <xplan:planinhalt xlink:href="#GML_6e3a3a9a-c467-494d-8804-a831a6c47751" />
      <xplan:planinhalt xlink:href="#GML_1e4cc7ea-f4d3-4cba-b4f5-89b217b42f6f" />
      <xplan:planinhalt xlink:href="#GML_dea227da-2b90-49a9-a4c9-d7d5cb4a5c23" />
      <xplan:planinhalt xlink:href="#GML_dc87fd67-4bd9-4462-b572-50af58e43a25" />
      <xplan:planinhalt xlink:href="#GML_28f122f0-3a41-4053-8b36-94fda694eaf3" />
      <xplan:planinhalt xlink:href="#GML_14cd1785-4d2a-47ab-b7f3-449053118429" />
      <xplan:planinhalt xlink:href="#GML_da687575-eca3-4c2a-b1f0-956d8e7292e1" />
      <xplan:planinhalt xlink:href="#GML_fe7cf17d-0c8b-40c6-99d3-5b6a9ade911a" />
      <xplan:planinhalt xlink:href="#GML_8ab4b801-e10c-4040-bbaf-88dbb3f46ce0" />
      <xplan:planinhalt xlink:href="#GML_037a453e-a463-4174-bee1-20c19cf333dc" />
      <xplan:planinhalt xlink:href="#GML_a826790b-3463-40b8-85ef-4ab4c5057bdd" />
      <xplan:planinhalt xlink:href="#GML_f7b713f4-b5c3-4dd2-bd72-6d3dbaf5f2da" />
      <xplan:planinhalt xlink:href="#GML_457628a7-0103-46a7-8242-f69a1dd08bbc" />
      <xplan:planinhalt xlink:href="#GML_877d54f6-ce5d-4cbb-a54e-bacdc599a058" />
      <xplan:planinhalt xlink:href="#GML_20a2e0c8-70bd-4c3c-bee8-af85b53ef29c" />
      <xplan:planinhalt xlink:href="#GML_c4ec3a94-59cf-4892-9e34-9a8d602fac0c" />
      <xplan:planinhalt xlink:href="#GML_b283aaa3-cbe7-4217-bcaa-064fdf4a91ea" />
      <xplan:planinhalt xlink:href="#GML_6ab17d4d-68ce-4dbf-a25a-2cd14506c3e2" />
      <xplan:planinhalt xlink:href="#GML_456f38b7-f280-4949-ad4d-0d4ec2569d0a" />
      <xplan:planinhalt xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:planinhalt xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:planinhalt xlink:href="#GML_d901d022-a730-4f4e-aff3-2b7c90c6b93f" />
      <xplan:planinhalt xlink:href="#GML_285c0249-a226-4329-8adb-f0f344309ebd" />
      <xplan:planinhalt xlink:href="#GML_f6640434-2ab9-4ed4-9762-923bc477c0dc" />
      <xplan:planinhalt xlink:href="#GML_de472a5f-e4cd-45ec-af3b-2ac268232002" />
      <xplan:planinhalt xlink:href="#GML_e1ec8789-e611-491e-994b-d0d14e4d323e" />
      <xplan:planinhalt xlink:href="#GML_74b20dc5-b262-4ed7-8524-7bf1a48c0cf3" />
      <xplan:planinhalt xlink:href="#GML_b9746773-0461-4b84-abca-2190911c06dc" />
      <xplan:planinhalt xlink:href="#GML_7b7c86c2-4f93-44ed-b838-c3a8be12af71" />
      <xplan:planinhalt xlink:href="#GML_d3776f35-766b-422a-a9bf-698f83c0a55c" />
      <xplan:planinhalt xlink:href="#GML_d7b0f8ed-6f59-4f38-979e-bbed137cfd24" />
      <xplan:planinhalt xlink:href="#GML_f06e6c1b-7c5e-44ce-ad88-d7c55dc46d14" />
      <xplan:planinhalt xlink:href="#GML_bcbea016-9b25-4b4b-a208-74fb81dbe347" />
      <xplan:planinhalt xlink:href="#GML_234f2be4-f17c-4d90-8f7c-9869b3105a33" />
      <xplan:planinhalt xlink:href="#GML_50159767-b7e5-4cf4-8cdd-e6ca8dd9d9ed" />
      <xplan:planinhalt xlink:href="#GML_a41fa4d8-54a2-499e-b237-44891a57e95c" />
      <xplan:planinhalt xlink:href="#GML_5a8e0bd9-c528-406a-993b-e4a573a16017" />
      <xplan:planinhalt xlink:href="#GML_fe0ae921-3ed8-47e7-a248-2ae10edfde1f" />
      <xplan:planinhalt xlink:href="#GML_45321a90-08fe-4932-a184-10b7f15d282f" />
      <xplan:planinhalt xlink:href="#GML_30ccabd6-1c88-4911-a5e0-83d229aefdd6" />
      <xplan:planinhalt xlink:href="#GML_21205338-39ee-4ed0-bce7-708290061eb5" />
      <xplan:planinhalt xlink:href="#GML_1245a1fa-8f64-4a47-9e6b-35cf7fa53ae1" />
      <xplan:planinhalt xlink:href="#GML_9ae668e9-5b2b-4fd0-a15f-01de34b81473" />
      <xplan:planinhalt xlink:href="#GML_599f5ba0-0938-4c55-a193-97bf6d53c0c4" />
      <xplan:planinhalt xlink:href="#GML_2ebdd987-d08f-4ec3-973d-8460172cb2d9" />
      <xplan:planinhalt xlink:href="#GML_06878e37-9e44-4961-92c7-fbdd1cf1e6d0" />
      <xplan:planinhalt xlink:href="#GML_58314478-4223-4d9f-abe9-569150740f5c" />
      <xplan:planinhalt xlink:href="#GML_082e2ebd-438e-4adb-b72f-2931152707fe" />
      <xplan:planinhalt xlink:href="#GML_7d67f197-d2a6-46bb-8246-8152ab098cd7" />
      <xplan:planinhalt xlink:href="#GML_ea776d6f-1f0a-4f00-9fe1-cfdb574de904" />
      <xplan:planinhalt xlink:href="#GML_bf2606a7-3204-46d1-b709-f780a87d5ff3" />
      <xplan:planinhalt xlink:href="#GML_fd31dc77-0ace-44db-a0d9-1e3b9f4e6ee0" />
      <xplan:planinhalt xlink:href="#GML_9d1872c2-3bbb-4e26-acd1-0e0e7a178ae7" />
      <xplan:planinhalt xlink:href="#GML_b5bf4faa-5bf0-4246-bfcb-6162377c255e" />
      <xplan:planinhalt xlink:href="#GML_bdf97adf-d8a9-4dae-8362-a09a73a51fa0" />
      <xplan:planinhalt xlink:href="#GML_7022252b-adee-401f-bd9d-e69ce354acd5" />
      <xplan:planinhalt xlink:href="#GML_e3cf8a7d-ebe9-4e50-abff-2ccb4ee3d954" />
      <xplan:planinhalt xlink:href="#GML_3110388e-f0dc-4b54-a941-dda61ba75c3c" />
      <xplan:planinhalt xlink:href="#GML_d99b1ba4-8a29-4cc5-8ca3-e99f0767adcf" />
      <xplan:planinhalt xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:planinhalt xlink:href="#GML_a901293d-3d84-462e-98f3-e29fae2ed376" />
      <xplan:planinhalt xlink:href="#GML_a852eb01-de63-40d5-a585-42115759e776" />
      <xplan:planinhalt xlink:href="#GML_c43ed4b4-0754-4ac9-a1cc-35bd57fa6d9c" />
      <xplan:praesentationsobjekt xlink:href="#GML_05ed645d-d470-405a-a390-e2683c21bf4e" />
      <xplan:praesentationsobjekt xlink:href="#GML_24f81e5a-5f25-4070-b617-e9d8f3073680" />
      <xplan:praesentationsobjekt xlink:href="#GML_64200842-66f5-429b-9963-0e83d2e75a1f" />
      <xplan:praesentationsobjekt xlink:href="#GML_7f7068c8-bc67-4e27-a516-6ff1c2f8e9f7" />
      <xplan:praesentationsobjekt xlink:href="#GML_e64e9539-4b62-40b6-b5fc-71d0085bbded" />
      <xplan:praesentationsobjekt xlink:href="#GML_c3c38f42-6c38-4ab1-822f-4f345f7aa2e2" />
      <xplan:praesentationsobjekt xlink:href="#GML_310624ef-717f-49b0-940d-65baa00d56e1" />
      <xplan:praesentationsobjekt xlink:href="#GML_7c0d896e-0f26-40e2-b0db-41ae4312b1ad" />
      <xplan:praesentationsobjekt xlink:href="#GML_3bfb88ba-1a31-45d6-b971-715d3406aced" />
      <xplan:praesentationsobjekt xlink:href="#GML_1af29368-7422-4575-af98-6681f7f61f64" />
      <xplan:praesentationsobjekt xlink:href="#GML_781d3793-ab82-441f-ab28-da69a48aad26" />
      <xplan:praesentationsobjekt xlink:href="#GML_1c64bb25-3c80-4f35-b892-abbf124e1401" />
      <xplan:praesentationsobjekt xlink:href="#GML_2611961f-cc1d-46ce-b049-a48b5f679ba4" />
      <xplan:praesentationsobjekt xlink:href="#GML_0009e672-4509-41d6-9da2-09c36daa0e72" />
      <xplan:praesentationsobjekt xlink:href="#GML_99a87f6d-9ca7-4a11-af2d-16c1afd84c32" />
      <xplan:praesentationsobjekt xlink:href="#GML_522f5f21-778f-469d-af6a-9a06eba37c6e" />
      <xplan:praesentationsobjekt xlink:href="#GML_151402d9-30cc-4c58-838e-3bcc54c787c9" />
      <xplan:praesentationsobjekt xlink:href="#GML_3510055f-d1f0-44c1-b29d-98ccf18e970a" />
      <xplan:praesentationsobjekt xlink:href="#GML_592a57e2-7c92-49c8-8273-fb49ff7c662e" />
      <xplan:praesentationsobjekt xlink:href="#GML_47750462-dbee-4124-a6a2-3f24446b54ee" />
      <xplan:praesentationsobjekt xlink:href="#GML_8499585b-e9b2-4c34-8fc0-9f69407dc54b" />
      <xplan:praesentationsobjekt xlink:href="#GML_28bc8afe-e15d-4e75-85d8-224c50a4af0e" />
      <xplan:praesentationsobjekt xlink:href="#GML_b36639b0-cc62-49d8-a553-52da27715c5f" />
      <xplan:praesentationsobjekt xlink:href="#GML_8eab63a2-fb13-408c-ac41-bbb194b2faae" />
      <xplan:praesentationsobjekt xlink:href="#GML_9333fe15-dd95-4aff-aebe-c37a48e4d2ec" />
      <xplan:praesentationsobjekt xlink:href="#GML_848f0251-7f04-451e-a5c3-5b849761fe28" />
      <xplan:praesentationsobjekt xlink:href="#GML_63eda789-900b-4f83-a52a-e2d7d0cd0faf" />
      <xplan:praesentationsobjekt xlink:href="#GML_ee9a801b-dc41-406b-b406-16f09b4f0499" />
      <xplan:praesentationsobjekt xlink:href="#GML_e96936cf-2271-4af4-ae6b-c0c2382100c2" />
      <xplan:praesentationsobjekt xlink:href="#GML_f5237d5a-b68d-4a1c-8d3c-2c4daa707588" />
      <xplan:praesentationsobjekt xlink:href="#GML_8e3c5fa1-0b8a-4a21-9fea-50e94d65f899" />
      <xplan:praesentationsobjekt xlink:href="#GML_3631c68f-2533-434a-83b5-a6609699f79b" />
      <xplan:praesentationsobjekt xlink:href="#GML_308d9453-aa40-4043-8682-5ed5a84ff118" />
      <xplan:praesentationsobjekt xlink:href="#GML_6396f67c-7150-44f2-9af9-7ea5e03d8f0b" />
      <xplan:praesentationsobjekt xlink:href="#GML_da614353-3e14-4548-a602-1dca72637536" />
      <xplan:praesentationsobjekt xlink:href="#GML_9bc001e7-69c3-4f5e-8146-97955e8d01f1" />
      <xplan:praesentationsobjekt xlink:href="#GML_c4bf1948-787e-4a71-b588-8d2091868fc9" />
      <xplan:praesentationsobjekt xlink:href="#GML_1b8d0589-6bf4-40fb-a071-e38c0e061914" />
      <xplan:praesentationsobjekt xlink:href="#GML_1f6eeb5a-8eb4-40de-b1a7-ff94cbc8b5ee" />
      <xplan:praesentationsobjekt xlink:href="#GML_1cbff272-89c7-412f-bb31-8ddc38544954" />
      <xplan:praesentationsobjekt xlink:href="#GML_4470427b-a9ad-4576-8aa4-d9822d4a12c6" />
      <xplan:praesentationsobjekt xlink:href="#GML_691d25f5-2077-4da5-86f0-d3016d33cdfa" />
      <xplan:praesentationsobjekt xlink:href="#GML_cd84ba26-68f2-45ab-9903-df772de1b93f" />
      <xplan:praesentationsobjekt xlink:href="#GML_e6a04548-9ba4-4b9d-bc5b-0d5a6977fe87" />
      <xplan:praesentationsobjekt xlink:href="#GML_36b30dcd-2d84-4cad-94fa-d6e02c711b16" />
      <xplan:praesentationsobjekt xlink:href="#GML_e9659569-957f-416a-8c7b-346be20371f8" />
      <xplan:praesentationsobjekt xlink:href="#GML_626ddd65-14ee-4532-9d7c-0cdd20fb4a29" />
      <xplan:praesentationsobjekt xlink:href="#GML_1c12ef22-0e3e-4a95-a9d9-75f8669b1f59" />
      <xplan:praesentationsobjekt xlink:href="#GML_1e7dd689-8623-4bd0-b4e4-b03299e77ac0" />
      <xplan:gehoertZuPlan xlink:href="#GML_9a86c33a-15a6-40ff-bd41-ae8209335c6c" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AbgrabungsFlaeche gml:id="GML_466718d3-cad4-4953-9ca0-01200cf301f5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567396.8544 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567486.5409 5937989.7820</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_9a504654-0c4b-4c9a-9dd0-d759b63f6054">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567486.5409 5937951.7580 567454.9160 5937989.7820 567396.8544 5937951.7580 567486.5409 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_AbgrabungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="GML_8ab4b801-e10c-4040-bbaf-88dbb3f46ce0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5938110.3099</gml:lowerCorner>
          <gml:upperCorner>567188.2570 5938339.3240</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_8212d58c-588c-4d3e-a2cb-32b475e5da2f">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567188.2040 5938271.8680 567188.2570 5938276.8660 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="7">567188.2570 5938276.8660 567180.9301 5938283.7649 567173.4130 5938290.4560 567149.7924 5938309.2111 567124.6760 5938325.9100 567123.8863 5938326.3878 567123.0950 5938326.8630 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="14">567123.0950 5938326.8630 567116.5260 5938330.7940 567115.6630 5938331.3110 567102.2800 5938339.3240 567099.5490 5938336.7640 567074.2380 5938313.0360 567038.1360 5938279.1960 567015.8040 5938258.2621 567015.8040 5938110.3099 567028.2040 5938121.9340 567031.0440 5938124.5960 567081.7630 5938172.1250 567152.2020 5938238.1350 567188.2040 5938271.8680 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>2000</xplan:gegenstand>
      <xplan:istAusgleich>false</xplan:istAusgleich>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AufschuettungsFlaeche gml:id="GML_6ab17d4d-68ce-4dbf-a25a-2cd14506c3e2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567454.9160 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567525.5484 5938008.9590</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e7c5d403-32f7-4e50-b433-4c774ce90546">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567525.5484 5937951.7580 567477.9700 5938008.9590 567454.9160 5937989.7820 567486.5409 5937951.7580 567525.5484 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_AufschuettungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AusgleichsFlaeche gml:id="GML_f0464838-d00f-4254-96c6-1390b7e17b89">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567376.5140 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567477.9700 5938053.5440</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>Z</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ca5be05c-351c-4f8a-b516-1bb42d51464d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">567436.1330 5938053.5440 567377.7460 5938006.4600 567376.5140 5938001.6150 567386.6842 5937973.6865 567396.8544 5937951.7580 567454.9160 5937989.7820 567477.9700 5938008.9590 567441.2310 5938053.1310 567436.1330 5938053.5440 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>3000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1800</xplan:klassifizMassnahme>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_AusgleichsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AusgleichsFlaeche gml:id="GML_bcbea016-9b25-4b4b-a208-74fb81dbe347">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5938258.2621</gml:lowerCorner>
          <gml:upperCorner>567102.2800 5938389.1105</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>Z</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5036d5e2-8126-4014-8e90-2367b325c4dd">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_bccdd5ce-3676-4696-9aa4-f58f87cb3bae">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">567099.5490 5938336.7640 567102.2800 5938339.3240 567101.3430 5938339.8860 567084.6290 5938349.8950 567049.9500 5938370.6590 567025.3542 5938385.3841 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567025.3542 5938385.3841 567024.6761 5938385.7488 567023.9680 5938386.0510 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">567023.9680 5938386.0510 567015.8040 5938389.1105 567015.8040 5938258.2621 567038.1360 5938279.1960 567074.2380 5938313.0360 567099.5490 5938336.7640 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_AusgleichsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_00c956a7-c970-4a71-ad80-62d0b4c89f52">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5938397.8850</gml:lowerCorner>
          <gml:upperCorner>567213.2460 5938455.8800</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_24f81e5a-5f25-4070-b617-e9d8f3073680" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7f7068c8-bc67-4e27-a516-6ff1c2f8e9f7" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_781d3793-ab82-441f-ab28-da69a48aad26" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3631c68f-2533-434a-83b5-a6609699f79b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c4bf1948-787e-4a71-b588-8d2091868fc9" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4470427b-a9ad-4576-8aa4-d9822d4a12c6" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e9659569-957f-416a-8c7b-346be20371f8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ebd4e158-59ec-4414-af78-6ff142f2cccd">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="20">567018.7190 5938421.5330 567081.8130 5938397.8850 567113.8010 5938399.5670 567133.8780 5938400.6210 567153.8490 5938401.6700 567173.7660 5938402.7150 567193.2140 5938403.7890 567201.6980 5938404.2580 567209.9910 5938404.7160 567213.2460 5938408.5090 567210.9330 5938431.7930 567208.6378 5938454.8860 567208.5390 5938455.8800 567198.6480 5938455.5270 567195.8610 5938455.4280 567091.8440 5938451.7440 567038.0710 5938448.4420 567015.8040 5938447.0744 567015.8040 5938422.6255 567018.7190 5938421.5330 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung>
        <xplan:BP_Dachgestaltung>
          <xplan:DN uom="grad">45</xplan:DN>
          <xplan:dachform>2100</xplan:dachform>
        </xplan:BP_Dachgestaltung>
      </xplan:dachgestaltung>
      <xplan:DN uom="grad">45</xplan:DN>
      <xplan:MaxZahlWohnungen>4</xplan:MaxZahlWohnungen>
      <xplan:Z>4</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1300</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_62517d2f-46c3-4130-b39e-1ed9b30da567">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567476.3270 5938332.9990</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938510.0532</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_63eda789-900b-4f83-a52a-e2d7d0cd0faf" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e3ea508a-90a5-488b-99b9-115a6a943b7f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="26">567582.8240 5938510.0532 567576.7720 5938504.0870 567574.6650 5938498.8260 567571.9860 5938456.7010 567560.7010 5938458.1680 567555.5430 5938457.0470 567547.7130 5938453.5210 567544.7220 5938446.8050 567532.6300 5938441.1460 567531.7320 5938441.2970 567524.8330 5938434.4940 567524.6370 5938433.3200 567512.5850 5938435.5530 567510.1440 5938433.5030 567503.8410 5938424.0480 567501.0370 5938417.0960 567498.1260 5938407.3270 567495.0040 5938400.1430 567486.5420 5938383.6950 567485.7370 5938382.0050 567476.3270 5938345.7470 567477.9390 5938343.2200 567526.2240 5938334.3760 567533.7420 5938332.9990 567582.8240 5938379.2574 567582.8240 5938510.0532 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1550</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_bdb12f92-c818-4c99-91da-14c3a319192c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567206.4676 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567341.8490 5938057.4400</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1e7dd689-8623-4bd0-b4e4-b03299e77ac0" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7e10db23-93df-470d-a7fd-c3c9d03da41e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="15">567324.1610 5938044.0030 567318.5530 5938057.4400 567290.1380 5938045.5830 567291.1700 5938043.1100 567291.4950 5938032.5190 567298.8100 5938014.9900 567284.6700 5938005.5620 567262.5460 5937990.1580 567206.4676 5937951.7580 567267.9361 5937951.7580 567341.8490 5938001.6170 567340.0820 5938005.8540 567335.1110 5938017.7630 567329.9560 5938030.1160 567324.1610 5938044.0030 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_f60afcda-b66b-424a-a013-9d6009892de2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567168.0850 5938488.9820</gml:lowerCorner>
          <gml:upperCorner>567204.9720 5938541.5180</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e64e9539-4b62-40b6-b5fc-71d0085bbded" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c3c38f42-6c38-4ab1-822f-4f345f7aa2e2" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8eab63a2-fb13-408c-ac41-bbb194b2faae" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9333fe15-dd95-4aff-aebe-c37a48e4d2ec" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f5237d5a-b68d-4a1c-8d3c-2c4daa707588" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1f6eeb5a-8eb4-40de-b1a7-ff94cbc8b5ee" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1cbff272-89c7-412f-bb31-8ddc38544954" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_28227528-caa8-415b-855a-a1152a12f36d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">567202.2430 5938519.2590 567200.0340 5938541.5180 567168.0850 5938538.3440 567170.2940 5938516.0850 567171.4020 5938504.9310 567172.9860 5938488.9820 567194.2200 5938490.8430 567204.9720 5938491.7860 567202.2430 5938519.2590 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GFZ>1.2</xplan:GFZ>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:GRZ_Ausn>0.7</xplan:GRZ_Ausn>
      <xplan:Z>1</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>3000</xplan:bauweise>
      <xplan:bebauungsArt>6000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_37375f30-503d-4263-bfd6-2213deddb929">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567233.1368 5938486.6650</gml:lowerCorner>
          <gml:upperCorner>567292.0285 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3bfb88ba-1a31-45d6-b971-715d3406aced" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1c64bb25-3c80-4f35-b892-abbf124e1401" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_691d25f5-2077-4da5-86f0-d3016d33cdfa" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e6a04548-9ba4-4b9d-bc5b-0d5a6977fe87" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_626ddd65-14ee-4532-9d7c-0cdd20fb4a29" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e0719073-0e82-4ccb-bf02-0fd5602eb07c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="14">567292.0285 5938562.2710 567279.9220 5938562.2710 567258.8304 5938562.2710 567233.1368 5938562.2710 567234.5830 5938547.6760 567237.3570 5938519.7230 567238.3910 5938509.3120 567239.2120 5938501.0430 567240.3440 5938489.6450 567284.8120 5938486.6650 567285.8970 5938498.0140 567286.6760 5938506.1780 567287.6460 5938516.3510 567292.0285 5938562.2710 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GR uom="m2">1500</xplan:GR>
      <xplan:GR_Ausn uom="m2">1600</xplan:GR_Ausn>
      <xplan:Zmin>1</xplan:Zmin>
      <xplan:Zmax>3</xplan:Zmax>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_599f5ba0-0938-4c55-a193-97bf6d53c0c4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567237.0450 5937990.1580</gml:lowerCorner>
          <gml:upperCorner>567284.6700 5938040.2450</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_36b30dcd-2d84-4cad-94fa-d6e02c711b16" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_732b0f99-c245-4163-ae21-9eb69dd449b7">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567237.0450 5938024.1630 567262.5460 5937990.1580 567284.6700 5938005.5620 567258.5110 5938040.2450 567237.0450 5938024.1630 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1700</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_74b20dc5-b262-4ed7-8524-7bf1a48c0cf3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567546.7590 5937997.3508</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938061.5679</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0009e672-4509-41d6-9da2-09c36daa0e72" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1bc59ddd-36f3-4899-92a1-b606e130d846">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567582.8240 5938061.5679 567560.9460 5938042.3340 567560.7570 5938039.1860 567546.7590 5938038.4690 567551.0130 5938033.6200 567582.8240 5937997.3508 567582.8240 5938061.5679 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>2100</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_7c55b006-758e-497e-8d01-b155ef84691c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5938476.8211</gml:lowerCorner>
          <gml:upperCorner>567200.0340 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_05ed645d-d470-405a-a390-e2683c21bf4e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7c0d896e-0f26-40e2-b0db-41ae4312b1ad" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_47750462-dbee-4124-a6a2-3f24446b54ee" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b36639b0-cc62-49d8-a553-52da27715c5f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ee9a801b-dc41-406b-b406-16f09b4f0499" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1c12ef22-0e3e-4a95-a9d9-75f8669b1f59" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_cfd70bfd-237c-4635-a70b-32504347f0c2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="13">567168.0850 5938538.3440 567200.0340 5938541.5180 567199.2460 5938549.4750 567197.9791 5938562.2710 567127.2264 5938562.2710 567015.8040 5938562.2710 567015.8040 5938476.8211 567070.6340 5938480.4600 567110.5100 5938483.5160 567172.9860 5938488.9820 567171.4020 5938504.9310 567170.2940 5938516.0850 567168.0850 5938538.3440 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GFZ>0.4</xplan:GFZ>
      <xplan:GRZ>0.2</xplan:GRZ>
      <xplan:Z>2</xplan:Z>
      <xplan:besondereArtDerBaulNutzung>1000</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_285c0249-a226-4329-8adb-f0f344309ebd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567356.3100 5938345.7910</gml:lowerCorner>
          <gml:upperCorner>567477.1410 5938430.6440</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_310624ef-717f-49b0-940d-65baa00d56e1" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1b8d0589-6bf4-40fb-a071-e38c0e061914" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_c14f518d-75ee-4746-8f23-f898701f0649">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_c9f6652e-d321-4e05-9805-2b3e2e6b491c">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567477.1410 5938388.6740 567472.3030 5938389.9300 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567472.3030 5938389.9300 567440.4639 5938409.0052 567405.5871 5938421.7015 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="16">567405.5871 5938421.7015 567371.1250 5938430.6440 567369.4920 5938424.3550 567369.2830 5938423.5490 567366.3040 5938412.0720 567362.8380 5938398.7160 567360.5630 5938389.9500 567357.3730 5938377.6610 567356.3100 5938373.5620 567392.6910 5938364.1200 567429.0690 5938354.6790 567463.9020 5938345.7910 567466.4300 5938347.4040 567473.8090 5938375.8340 567474.5570 5938378.7190 567477.1410 5938388.6740 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.3</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1800</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_21205338-39ee-4ed0-bce7-708290061eb5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567262.3480 5938190.3120</gml:lowerCorner>
          <gml:upperCorner>567443.9210 5938342.1950</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_99a87f6d-9ca7-4a11-af2d-16c1afd84c32" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b858d435-28a9-49db-99ff-9743d9b06823">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="15">567357.8090 5938342.1950 567340.5410 5938275.6630 567262.3480 5938267.9090 567263.5760 5938255.5750 567285.4850 5938215.4520 567293.0620 5938201.5750 567297.7620 5938190.3120 567314.7540 5938201.2540 567354.1530 5938224.9820 567378.6070 5938207.5810 567443.9210 5938260.3800 567435.3230 5938272.8780 567429.6710 5938323.5430 567412.9600 5938327.8800 567357.8090 5938342.1950 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>2100</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_f7b713f4-b5c3-4dd2-bd72-6d3dbaf5f2da">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567396.8544 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567486.5409 5937989.7820</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_64200842-66f5-429b-9963-0e83d2e75a1f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a7a0247a-7936-4cb9-a6fd-6cd81262afb3">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567486.5409 5937951.7580 567454.9160 5937989.7820 567396.8544 5937951.7580 567486.5409 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1800</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_dc60c672-b6ca-442c-b761-f56982073dd5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567244.9260 5938373.5620</gml:lowerCorner>
          <gml:upperCorner>567371.1250 5938444.4094</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3510055f-d1f0-44c1-b29d-98ccf18e970a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_cd84ba26-68f2-45ab-9903-df772de1b93f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_82576630-de52-46e0-995a-162f901ed639">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_af8211af-7484-4e44-8c3c-ad28cc06debb">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">567369.2830 5938423.5490 567369.4920 5938424.3550 567371.1250 5938430.6440 567363.7388 5938432.5602 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">567363.7388 5938432.5602 567318.2481 5938441.3616 567272.0140 5938444.4080 567260.0919 5938444.2500 567248.1810 5938443.7100 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="13">567248.1810 5938443.7100 567244.9260 5938443.5110 567245.6680 5938436.0450 567246.0240 5938432.4610 567249.1130 5938401.3790 567283.5550 5938392.4420 567319.9310 5938383.0020 567356.3100 5938373.5620 567357.3730 5938377.6610 567360.5630 5938389.9500 567362.8380 5938398.7160 567366.3040 5938412.0720 567369.2830 5938423.5490 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1700</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_df5f8049-6e74-4625-9771-3816a8cb488e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567454.9160 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567525.5484 5938008.9590</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8499585b-e9b2-4c34-8fc0-9f69407dc54b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_826ffe9d-dddc-46a6-8c2d-fba0388c801f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567525.5484 5937951.7580 567477.9700 5938008.9590 567454.9160 5937989.7820 567486.5409 5937951.7580 567525.5484 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1700</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_611be92c-f6e2-48c5-99a9-563cc1f00241">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567243.3020 5938388.6740</gml:lowerCorner>
          <gml:upperCorner>567503.5760 5938460.6870</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_592a57e2-7c92-49c8-8273-fb49ff7c662e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_6f3f615d-eefc-4f32-a75c-49e1bf5c5cee">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_d3b170b7-4836-4d60-a078-50cc397c36ae">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="9">567420.1430 5938449.0830 567321.8130 5938460.6870 567272.0620 5938460.1610 567250.6190 5938459.9350 567243.3020 5938459.8580 567243.5780 5938457.0780 567244.4600 5938448.2060 567244.9260 5938443.5110 567248.1810 5938443.7100 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">567248.1810 5938443.7100 567260.0919 5938444.2500 567272.0140 5938444.4080 567318.2481 5938441.3616 567363.7388 5938432.5602 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">567363.7388 5938432.5602 567371.1250 5938430.6440 567405.5871 5938421.7015 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567405.5871 5938421.7015 567440.4639 5938409.0052 567472.3030 5938389.9300 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">567472.3030 5938389.9300 567477.1410 5938388.6740 567477.9280 5938391.7110 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567477.9280 5938391.7110 567479.9195 5938398.7946 567487.2770 5938398.8940 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">567487.2770 5938398.8940 567491.1250 5938401.7750 567493.5050 5938404.7690 567499.2180 5938420.7110 567501.1700 5938426.3830 567503.5760 5938436.5350 567503.2310 5938436.8930 567502.3330 5938437.0430 567492.0120 5938432.1900 567488.9810 5938432.6940 567488.8880 5938432.1330 567420.1430 5938449.0830 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1600</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567284.8120 5938466.4850</gml:lowerCorner>
          <gml:upperCorner>567416.5001 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">25</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2611961f-cc1d-46ce-b049-a48b5f679ba4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_28bc8afe-e15d-4e75-85d8-224c50a4af0e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_da614353-3e14-4548-a602-1dca72637536" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9bc001e7-69c3-4f5e-8146-97955e8d01f1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_357b6a20-d631-478f-9b7d-76bf73a9cb48">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">567416.5001 5938562.2710 567292.0285 5938562.2710 567287.6460 5938516.3510 567286.6760 5938506.1780 567285.8970 5938498.0140 567284.8120 5938486.6650 567298.3200 5938485.7590 567323.1730 5938482.9970 567354.8210 5938477.0320 567410.7550 5938466.4850 567412.6420 5938497.9330 567416.5001 5938562.2710 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachform>3900</xplan:dachform>
      <xplan:dachform>1000</xplan:dachform>
      <xplan:dachform>3400</xplan:dachform>
      <xplan:GFZmin>1.2</xplan:GFZmin>
      <xplan:GFZmax>1.4</xplan:GFZmax>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_fe7cf17d-0c8b-40c6-99d3-5b6a9ade911a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567503.1714 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938038.4690</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_522f5f21-778f-469d-af6a-9a06eba37c6e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_085fb967-d96c-4241-b4e6-28466bb2ab12">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567582.8240 5937997.3508 567551.0130 5938033.6200 567546.7590 5938038.4690 567542.8830 5938038.2700 567503.1714 5938005.2345 567547.6498 5937951.7580 567582.8240 5937951.7580 567582.8240 5937997.3508 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:besondereArtDerBaulNutzung>1000</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567410.7550 5938443.6970</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_848f0251-7f04-451e-a5c3-5b849761fe28" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8e3c5fa1-0b8a-4a21-9fea-50e94d65f899" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6396f67c-7150-44f2-9af9-7ea5e03d8f0b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fcac2428-60d0-4c2b-bcb1-328780381c14">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="22">567571.3720 5938501.9290 567574.9690 5938505.2070 567582.8240 5938512.9477 567582.8240 5938562.2710 567416.5001 5938562.2710 567412.6420 5938497.9330 567410.7550 5938466.4850 567502.8240 5938449.1300 567508.2800 5938451.8630 567510.0550 5938450.0250 567509.4220 5938445.0190 567510.3200 5938444.8710 567510.1240 5938443.6970 567520.1690 5938453.6130 567531.8340 5938456.6000 567556.5240 5938463.7610 567556.6530 5938469.8310 567566.3760 5938469.3790 567568.9620 5938472.1200 567570.9100 5938481.0730 567570.8350 5938487.2980 567571.3720 5938501.9290 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GF uom="m2">15000</xplan:GF>
      <xplan:Zzwingend>3</xplan:Zzwingend>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b283aaa3-cbe7-4217-bcaa-064fdf4a91ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567111.4500 5938505.0000</gml:lowerCorner>
          <gml:upperCorner>567111.4500 5938551.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_4ae36068-897f-4db3-a5d6-906c1f467032">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567111.4500 5938551.0000 567111.4500 5938505.0000 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_5f2b1898-c231-416c-8167-8ad36a8edb83">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567111.4500 5938504.5500</gml:lowerCorner>
          <gml:upperCorner>567137.0000 5938551.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_7e154385-7de7-4bee-9ce0-d41e7027c098">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="3">567111.4500 5938504.5500 567137.0000 5938504.5500 567137.0000 5938551.0000 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauLinie gml:id="GML_0adf1e25-25cc-4d18-9fc5-1a8d5ae0e289">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567111.4500 5938551.0000</gml:lowerCorner>
          <gml:upperCorner>567137.0000 5938551.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_659b5e9e-d654-49e5-878d-71eb7b782082">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567137.0000 5938551.0000 567111.4500 5938551.0000 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_BauLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BereichOhneEinAusfahrtLinie gml:id="GML_580f8406-cb80-4d3e-a4bd-ddde3dd81ed4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567210.1230 5938299.7920</gml:lowerCorner>
          <gml:upperCorner>567229.3465 5938309.1621</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_5bbc93d1-f05e-4515-8314-1b4e55d8ba4b">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567210.1230 5938299.7920 567229.3465 5938309.1621 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>3000</xplan:typ>
    </xplan:BP_BereichOhneEinAusfahrtLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BesondererNutzungszweckFlaeche gml:id="GML_82f81025-67e3-479b-89bc-fc71a49b00f7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567113.2912 5938111.5658</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e34eeb64-f645-4ad3-9db5-0e962a7bc434">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567027.6290 5938098.1010 567015.8040 5938087.0120 567015.8040 5937951.7580 567043.8114 5937951.7580 567096.8400 5937991.5290 567113.2912 5938003.8639 567041.9961 5938111.5658 567027.6290 5938098.1010 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:BP_BesondererNutzungszweckFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_ea776d6f-1f0a-4f00-9fe1-cfdb574de904">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567253.1340 5938267.9090</gml:lowerCorner>
          <gml:upperCorner>567357.8090 5938367.3860</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1af29368-7422-4575-af98-6681f7f61f64" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_151402d9-30cc-4c58-838e-3bcc54c787c9" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5bedcc47-ace3-4388-8b5e-7095017a6e3b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="12">567340.5410 5938275.6630 567357.8090 5938342.1950 567307.4970 5938355.2490 567274.6040 5938363.7860 567260.7280 5938367.3860 567253.1340 5938360.8850 567253.2780 5938359.4300 567255.0540 5938341.5070 567256.6320 5938325.5930 567258.2080 5938309.6820 567262.3480 5938267.9090 567340.5410 5938275.6630 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>16000</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
      <xplan:detaillierteZweckbestimmung>Kindergarten</xplan:detaillierteZweckbestimmung>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="GML_dc87fd67-4bd9-4462-b572-50af58e43a25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567308.2260 5938128.0860</gml:lowerCorner>
          <gml:upperCorner>567557.0607 5938332.9990</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ef30390d-20e1-4e38-a1b8-d6ac695327d2">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">567557.0607 5938328.5050 567533.7420 5938332.9990 567502.9980 5938308.1430 567443.9210 5938260.3800 567378.6070 5938207.5810 567312.7680 5938154.3550 567308.2260 5938150.6810 567318.0206 5938128.0860 567557.0607 5938328.5050 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="GML_f69ae941-a696-4d86-aa3e-fee4a1fa44eb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567113.2912 5938111.5658</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_21d814ff-bed2-4fc6-adee-17d9bf9e62c6">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567027.6290 5938098.1010 567015.8040 5938087.0120 567015.8040 5937951.7580 567043.8114 5937951.7580 567096.8400 5937991.5290 567113.2912 5938003.8639 567041.9961 5938111.5658 567027.6290 5938098.1010 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_7022252b-adee-401f-bd9d-e69ce354acd5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567233.1368 5938486.6650</gml:lowerCorner>
          <gml:upperCorner>567292.0285 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die GSt, GGa oder GTGa bestimmt sind</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d373a5a2-bfc0-4b9b-92f8-c94cd0b4b253">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="14">567292.0285 5938562.2710 567279.9220 5938562.2710 567258.8304 5938562.2710 567233.1368 5938562.2710 567234.5830 5938547.6760 567237.3570 5938519.7230 567238.3910 5938509.3120 567239.2120 5938501.0430 567240.3440 5938489.6450 567284.8120 5938486.6650 567285.8970 5938498.0140 567286.6760 5938506.1780 567287.6460 5938516.3510 567292.0285 5938562.2710 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="GML_457628a7-0103-46a7-8242-f69a1dd08bbc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567546.7590 5937997.3508</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938061.5679</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die GSt, GGa oder GTGa bestimmt sind</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_c6a4ff2e-7014-43ec-9949-fd74953eb285">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567582.8240 5938061.5679 567560.9460 5938042.3340 567560.7570 5938039.1860 567546.7590 5938038.4690 567551.0130 5938033.6200 567582.8240 5937997.3508 567582.8240 5938061.5679 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_234f2be4-f17c-4d90-8f7c-9869b3105a33">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567318.0206 5938023.4470</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938328.5050</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d53616e1-20f2-48df-a992-89b0e574b6a9">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="13">567371.7380 5938023.4470 567582.8240 5938193.6392 567582.8240 5938324.0110 567557.0607 5938328.5050 567318.0206 5938128.0860 567320.6050 5938122.1240 567324.1860 5938112.2910 567330.8200 5938111.1020 567336.0350 5938098.6060 567338.0410 5938093.8000 567348.7730 5938068.0840 567366.8920 5938024.6760 567371.7380 5938023.4470 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_f06e6c1b-7c5e-44ce-ad88-d7c55dc46d14">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567028.0670 5938232.0970</gml:lowerCorner>
          <gml:upperCorner>567089.2128 5938291.9467</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_844fcfbe-4063-4a61-a940-1ea10f1f431a">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_7d9dee1a-d89d-4352-8c81-0b8bcfaa358d">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567088.0290 5938265.6260 567089.2090 5938268.6628 567088.2680 5938271.7820 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567088.2680 5938271.7820 567071.0860 5938290.1110 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567071.0860 5938290.1110 567067.4909 5938291.9390 567063.7160 5938290.5190 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567063.7160 5938290.5190 567029.2880 5938258.4920 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567029.2880 5938258.4920 567028.0690 5938255.0754 567029.4810 5938251.7340 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567029.4810 5938251.7340 567046.5050 5938233.4880 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567046.5050 5938233.4880 567049.9536 5938232.0970 567053.4090 5938233.4710 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567053.4090 5938233.4710 567088.0290 5938265.6260 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>16000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_5a8e0bd9-c528-406a-993b-e4a573a16017">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5938258.2621</gml:lowerCorner>
          <gml:upperCorner>567102.2800 5938389.1105</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e96936cf-2271-4af4-ae6b-c0c2382100c2" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_dd5b7f93-316a-4fb8-b36d-c3bf65953748">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_695d37ea-095d-4339-a0e9-4cca42276a4b">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">567099.5490 5938336.7640 567102.2800 5938339.3240 567101.3430 5938339.8860 567084.6290 5938349.8950 567049.9500 5938370.6590 567025.3542 5938385.3841 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567025.3542 5938385.3841 567024.6761 5938385.7488 567023.9680 5938386.0510 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">567023.9680 5938386.0510 567015.8040 5938389.1105 567015.8040 5938258.2621 567038.1360 5938279.1960 567074.2380 5938313.0360 567099.5490 5938336.7640 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1200</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_7b7c86c2-4f93-44ed-b838-c3a8be12af71">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567298.5760 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567362.5115 5937980.2003</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e1daa8e7-ecc6-4416-8f96-0926bfcf0f57">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567310.8460 5937959.9350 567298.5760 5937951.7580 567362.5115 5937951.7580 567350.8692 5937980.2003 567323.8150 5937967.2120 567310.8460 5937959.9350 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_b5bf4faa-5bf0-4246-bfcb-6162377c255e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5938110.3099</gml:lowerCorner>
          <gml:upperCorner>567188.2570 5938339.3240</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d241b5a0-b721-416b-afd4-bfd4c93e8b93">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_fc2cf1e1-6fd7-4b81-903b-53f4a9d876c6">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567188.2040 5938271.8680 567188.2570 5938276.8660 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">567188.2570 5938276.8660 567180.9301 5938283.7649 567173.4130 5938290.4560 567149.7924 5938309.2111 567124.6760 5938325.9100 567123.8863 5938326.3878 567123.0950 5938326.8630 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="14">567123.0950 5938326.8630 567116.5260 5938330.7940 567115.6630 5938331.3110 567102.2800 5938339.3240 567099.5490 5938336.7640 567074.2380 5938313.0360 567038.1360 5938279.1960 567015.8040 5938258.2621 567015.8040 5938110.3099 567028.2040 5938121.9340 567031.0440 5938124.5960 567081.7630 5938172.1250 567152.2020 5938238.1350 567188.2040 5938271.8680 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_e1180422-7b92-4018-9710-6f2ad7768994">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567053.4090 5938233.4710 567049.9536 5938232.0970 567046.5050 5938233.4880 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567046.5050 5938233.4880 567029.4810 5938251.7340 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567029.4810 5938251.7340 567028.0690 5938255.0754 567029.2880 5938258.4920 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567029.2880 5938258.4920 567063.7160 5938290.5190 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567063.7160 5938290.5190 567067.4909 5938291.9390 567071.0860 5938290.1110 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567071.0860 5938290.1110 567088.2680 5938271.7820 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567088.2680 5938271.7820 567089.2090 5938268.6628 567088.0290 5938265.6260 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567088.0290 5938265.6260 567053.4090 5938233.4710 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Immissionsschutz gml:id="GML_877d54f6-ce5d-4cbb-a54e-bacdc599a058">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567533.7420 5938324.0110</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938379.2574</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_73183422-9920-4b25-b133-9187f068335b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567533.7420 5938332.9990 567557.0607 5938328.5050 567582.8240 5938324.0110 567582.8240 5938379.2574 567533.7420 5938332.9990 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Immissionsschutz>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_a852eb01-de63-40d5-a585-42115759e776">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567085.5250 5938043.9637</gml:lowerCorner>
          <gml:upperCorner>567286.6710 5938255.1140</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_04665b09-0d31-4229-be4f-f88fd409ec9a">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_c0002484-62de-47c5-8569-6f070955ff29">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="9">567278.8300 5938152.6240 567270.3078 5938171.2304 567260.5380 5938189.2130 567258.3118 5938192.9353 567256.0320 5938196.6250 567256.0053 5938196.6675 567255.9786 5938196.7100 567241.0312 5938217.3624 567223.5190 5938235.8900 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="10">567223.5190 5938235.8900 567220.8920 5938238.2840 567202.0860 5938255.1140 567195.0900 5938255.0320 567114.8370 5938179.8310 567085.5250 5938152.3580 567166.7777 5938043.9637 567169.0660 5938045.6790 567286.6710 5938133.8360 567278.8300 5938152.6240 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>8000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_LandwirtschaftsFlaeche gml:id="GML_456f38b7-f280-4949-ad4d-0d4ec2569d0a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567041.9961 5938003.8639</gml:lowerCorner>
          <gml:upperCorner>567166.7777 5938152.3580</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d52591d9-8427-44a1-b9f4-4553d5f94bc4">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">567060.8700 5938129.2550 567054.8890 5938123.6490 567041.9961 5938111.5658 567113.2912 5938003.8639 567135.6390 5938020.6200 567152.0690 5938032.9380 567166.7777 5938043.9637 567085.5250 5938152.3580 567062.9330 5938131.1880 567060.8700 5938129.2550 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_LandwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenAusschlussFlaeche gml:id="GML_fc4d2da9-d2f0-4117-89a1-bc7e9a715b81">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567206.4676 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567341.8490 5938057.4400</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_60caff7c-27a4-4b19-a040-b9c507d8b5dc">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="15">567324.1610 5938044.0030 567318.5530 5938057.4400 567290.1380 5938045.5830 567291.1700 5938043.1100 567291.4950 5938032.5190 567298.8100 5938014.9900 567284.6700 5938005.5620 567262.5460 5937990.1580 567206.4676 5937951.7580 567267.9361 5937951.7580 567341.8490 5938001.6170 567340.0820 5938005.8540 567335.1110 5938017.7630 567329.9560 5938030.1160 567324.1610 5938044.0030 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_NebenanlagenAusschlussFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="GML_d901d022-a730-4f4e-aff3-2b7c90c6b93f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567130.3410 5937967.0160</gml:lowerCorner>
          <gml:upperCorner>567305.2920 5938108.5460</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_c0350171-52bb-45c1-969c-e8b2f71e7f20">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567302.9340 5938108.5460 567130.3410 5937979.1730 567135.6950 5937971.2200 567139.1140 5937967.0160 567305.2920 5938091.5710 567302.9340 5938108.5460 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NichtUeberbaubareGrundstuecksflaeche gml:id="GML_d373b68e-e6ef-4a46-93ee-ddc72d50fa0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567396.8544 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567486.5409 5937989.7820</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a3d9f560-6797-49eb-b806-756551764a8b">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567486.5409 5937951.7580 567454.9160 5937989.7820 567396.8544 5937951.7580 567486.5409 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_NichtUeberbaubareGrundstuecksflaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_a826790b-3463-40b8-85ef-4ab4c5057bdd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567028.0670 5938232.0970</gml:lowerCorner>
          <gml:upperCorner>567089.2128 5938291.9467</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_e373da2a-804d-4986-9a1d-b793e777de2e">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567053.4090 5938233.4710 567049.9536 5938232.0970 567046.5050 5938233.4880 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567046.5050 5938233.4880 567029.4810 5938251.7340 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567029.4810 5938251.7340 567028.0690 5938255.0754 567029.2880 5938258.4920 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567029.2880 5938258.4920 567063.7160 5938290.5190 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567063.7160 5938290.5190 567067.4909 5938291.9390 567071.0860 5938290.1110 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567071.0860 5938290.1110 567088.2680 5938271.7820 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points">
              <gml:posList srsDimension="2" count="3">567088.2680 5938271.7820 567089.2090 5938268.6628 567088.0290 5938265.6260 </gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567088.0290 5938265.6260 567053.4090 5938233.4710 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_RichtungssektorGrenze gml:id="GML_14cd1785-4d2a-47ab-b7f3-449053118429">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567297.7620 5938190.3120</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_ea045288-8bc5-4668-8675-38e8bbb8fc6e">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567297.7620 5938190.3120 567582.8240 5938562.2710 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_RichtungssektorGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Sichtflaeche gml:id="GML_da687575-eca3-4c2a-b1f0-956d8e7292e1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567221.9350 5938279.2697</gml:lowerCorner>
          <gml:upperCorner>567251.9350 5938295.2072</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b3848731-1267-4c78-9a58-675f9e944d14">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567236.9350 5938279.2697 567251.9350 5938295.2072 567221.9350 5938295.2072 567236.9350 5938279.2697 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_Sichtflaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_a41fa4d8-54a2-499e-b237-44891a57e95c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567031.0000 5938511.0000</gml:lowerCorner>
          <gml:upperCorner>567054.0000 5938544.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1e12d4b3-7f78-4bc1-acb7-5367512ed733">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567054.0000 5938544.0000 567031.0000 5938544.0000 567031.0000 5938511.0000 567054.0000 5938511.0000 567054.0000 5938544.0000 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1400</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpielSportanlagenFlaeche gml:id="GML_3d82bfa9-cf92-45d6-879f-0f3382017d06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567538.6810 5938085.4110</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938162.5940</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1a7ee0a4-7dea-478d-8d97-51b5ffa4363a">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567580.1406 5938088.5188 567579.6650 5938085.4110 567582.8240 5938088.1886 567582.8240 5938149.5842 567571.3950 5938162.5940 567538.6810 5938136.2210 567580.1406 5938088.5188 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:BP_SpielSportanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_de472a5f-e4cd-45ec-af3b-2ac268232002">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567093.7835 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567139.1140 5937979.1730</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d48cc90e-900a-4782-8a1a-f2bffef13817">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567118.7706 5937951.7580 567139.1140 5937967.0160 567130.3410 5937979.1730 567093.7835 5937951.7580 567118.7706 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_06878e37-9e44-4961-92c7-fbdd1cf1e6d0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567194.2200 5938401.3790</gml:lowerCorner>
          <gml:upperCorner>567249.1130 5938562.2710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ae97dd24-29e6-4463-84d5-14f10a475eac">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_82a68f84-2b7f-4e1f-8717-bd3eae6bdd9b">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="20">567244.4600 5938448.2060 567243.5780 5938457.0780 567243.3020 5938459.8580 567240.3440 5938489.6450 567239.2120 5938501.0430 567238.3910 5938509.3120 567237.3570 5938519.7230 567234.5830 5938547.6760 567233.1368 5938562.2710 567197.9791 5938562.2710 567199.2460 5938549.4750 567200.0340 5938541.5180 567202.2430 5938519.2590 567204.9720 5938491.7860 567194.2200 5938490.8430 567194.6260 5938483.4690 567195.4350 5938483.5140 567195.5720 5938479.9070 567200.4760 5938480.2030 567201.6539 5938480.2738 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567201.6539 5938480.2738 567203.2592 5938478.9822 567201.9700 5938477.3750 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567201.9700 5938477.3750 567202.5110 5938468.6560 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567202.5110 5938468.6560 567203.9791 5938467.3156 567202.6880 5938465.8040 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="14">567202.6880 5938465.8040 567201.3950 5938465.6960 567197.9060 5938465.4030 567198.6480 5938455.5270 567208.5390 5938455.8800 567208.6380 5938454.8860 567210.9330 5938431.7930 567213.2460 5938408.5090 567209.9910 5938404.7160 567249.1130 5938401.3790 567246.0240 5938432.4610 567245.6680 5938436.0450 567244.9260 5938443.5110 567244.4600 5938448.2060 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_0534d970-7943-46c9-a235-e0cb3ccd3ca6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567454.2120 5938014.2290</gml:lowerCorner>
          <gml:upperCorner>567580.1406 5938136.2210</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fd39f2d3-29d3-4026-a012-ecd8d4e990a9">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567495.6890 5938014.2290 567579.6650 5938085.4110 567580.1406 5938088.5188 567538.6810 5938136.2210 567454.7050 5938068.5180 567454.2120 5938064.0890 567495.6890 5938014.2290 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_93b1097e-32cd-4062-8dc2-a4d47c7d1f21">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567130.3410 5937967.0160</gml:lowerCorner>
          <gml:upperCorner>567305.2920 5938108.5460</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_abcae9a7-c33d-4ea0-b1a3-0b86e3172234">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567302.9340 5938108.5460 567130.3410 5937979.1730 567139.1140 5937967.0160 567305.2920 5938091.5710 567302.9340 5938108.5460 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_bf2606a7-3204-46d1-b709-f780a87d5ff3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567118.7706 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567313.1450 5938091.5710</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Neue Straßenverkehrsfläche</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fdcabb7f-2c80-4333-b84c-bf859280a32e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="15">567150.4470 5937953.0410 567186.1230 5937979.7580 567184.8030 5937981.5170 567183.1230 5937983.7550 567204.9790 5938000.1350 567237.0450 5938024.1630 567258.5110 5938040.2450 567281.7050 5938057.6370 567313.1450 5938070.3960 567309.7060 5938078.6370 567305.2920 5938091.5710 567139.1140 5937967.0160 567118.7706 5937951.7580 567148.7349 5937951.7580 567150.4470 5937953.0410 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_1e4cc7ea-f4d3-4cba-b4f5-89b217b42f6f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567208.7230 5938255.5750</gml:lowerCorner>
          <gml:upperCorner>567263.5760 5938375.1410</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_33a120d7-74da-421f-86b9-a887da141f6a">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_b616ec73-389b-47d2-9678-5fbfad7125f4">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">567256.6320 5938325.5930 567255.0540 5938341.5070 567253.2780 5938359.4300 567253.1340 5938360.8850 567260.7280 5938367.3860 567218.1090 5938375.1410 567221.9000 5938371.8860 567223.2070 5938358.7170 567224.4690 5938345.9920 567227.7730 5938312.7070 567227.9465 5938310.9621 567208.7230 5938301.5920 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567208.7230 5938301.5920 567217.4480 5938292.9760 567225.9020 5938284.0940 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="5">567225.9020 5938284.0940 567263.5760 5938255.5750 567262.3480 5938267.9090 567258.2080 5938309.6820 567256.6320 5938325.5930 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_6e3a3a9a-c467-494d-8804-a831a6c47751">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567396.8544 5938422.6255</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_489466da-5003-43c5-98f5-8d61de131da8">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_ebf16e17-49d9-4ca1-b88d-4c4c813d2c99">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567263.5760 5938255.5750 567225.9020 5938284.0940 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="11">567225.9020 5938284.0940 567217.4480 5938292.9760 567208.7230 5938301.5920 567196.2758 5938312.9870 567183.3500 5938323.8360 567177.2982 5938328.5948 567171.1550 5938333.2350 567162.5579 5938339.4131 567153.7980 5938345.3580 567143.4689 5938351.9305 567132.9400 5938358.1780 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="6">567132.9400 5938358.1780 567081.8130 5938397.8850 567018.7190 5938421.5330 567015.8040 5938422.6255 567015.8040 5938389.1105 567023.9680 5938386.0510 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567023.9680 5938386.0510 567024.6761 5938385.7488 567025.3542 5938385.3841 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="8">567025.3542 5938385.3841 567049.9500 5938370.6590 567084.6290 5938349.8950 567101.3430 5938339.8860 567102.2800 5938339.3240 567115.6630 5938331.3110 567116.5260 5938330.7940 567123.0950 5938326.8630 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="7">567123.0950 5938326.8630 567123.8863 5938326.3878 567124.6760 5938325.9100 567149.7924 5938309.2111 567173.4130 5938290.4560 567180.9301 5938283.7649 567188.2570 5938276.8660 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="4">567188.2570 5938276.8660 567202.0860 5938255.1140 567220.8920 5938238.2840 567223.5190 5938235.8900 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="9">567223.5190 5938235.8900 567241.0312 5938217.3624 567255.9786 5938196.7100 567256.0053 5938196.6675 567256.0320 5938196.6250 567258.3118 5938192.9353 567260.5380 5938189.2130 567270.3078 5938171.2304 567278.8300 5938152.6240 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="40">567278.8300 5938152.6240 567286.6710 5938133.8360 567291.3710 5938137.3610 567292.3209 5938135.1008 567292.3555 5938135.0186 567292.5366 5938135.0936 567292.6800 5938135.1530 567302.5660 5938111.5610 567302.2990 5938111.4490 567302.2890 5938111.3300 567303.3160 5938108.8320 567302.9340 5938108.5460 567305.2920 5938091.5710 567309.7060 5938078.6370 567313.1450 5938070.3960 567318.5530 5938057.4400 567324.1610 5938044.0030 567329.9560 5938030.1160 567335.1110 5938017.7630 567340.0820 5938005.8540 567341.8490 5938001.6170 567350.8692 5937980.2003 567362.5115 5937951.7580 567396.8544 5937951.7580 567386.6842 5937973.6865 567376.5140 5938001.6150 567366.8920 5938024.6760 567348.7730 5938068.0840 567338.0410 5938093.8000 567336.0350 5938098.6060 567330.8200 5938111.1020 567324.1860 5938112.2910 567320.6050 5938122.1240 567318.0206 5938128.0860 567308.2260 5938150.6810 567312.7680 5938154.3550 567297.7620 5938190.3120 567293.0620 5938201.5750 567285.4850 5938215.4520 567263.5760 5938255.5750 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TechnischeMassnahmenFlaeche gml:id="GML_9ae668e9-5b2b-4fd0-a15f-01de34b81473">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567237.0450 5937990.1580</gml:lowerCorner>
          <gml:upperCorner>567284.6700 5938040.2450</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_615b254b-8624-4e07-9e6d-6678e04533ec">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567237.0450 5938024.1630 567262.5460 5937990.1580 567284.6700 5938005.5620 567258.5110 5938040.2450 567237.0450 5938024.1630 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_TechnischeMassnahmenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_20a2e0c8-70bd-4c3c-bee8-af85b53ef29c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567243.3020 5938388.6740</gml:lowerCorner>
          <gml:upperCorner>567503.5760 5938460.6870</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_745cbce1-ab01-4c1d-8b64-e1c113ac9c27" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_cdaab713-b2d1-4a19-b6a4-acf4b20e3896">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_4387fe18-dae7-403c-b900-8de3a3aae847">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="9">567420.1430 5938449.0830 567321.8130 5938460.6870 567272.0620 5938460.1610 567250.6190 5938459.9350 567243.3020 5938459.8580 567243.5780 5938457.0780 567244.4600 5938448.2060 567244.9260 5938443.5110 567248.1810 5938443.7100 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">567248.1810 5938443.7100 567260.0919 5938444.2500 567272.0140 5938444.4080 567318.2481 5938441.3616 567363.7388 5938432.5602 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">567363.7388 5938432.5602 567371.1250 5938430.6440 567405.5871 5938421.7015 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567405.5871 5938421.7015 567440.4639 5938409.0052 567472.3030 5938389.9300 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">567472.3030 5938389.9300 567477.1410 5938388.6740 567477.9280 5938391.7110 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567477.9280 5938391.7110 567479.9195 5938398.7946 567487.2770 5938398.8940 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">567487.2770 5938398.8940 567491.1250 5938401.7750 567493.5050 5938404.7690 567499.2180 5938420.7110 567501.1700 5938426.3830 567503.5760 5938436.5350 567503.2310 5938436.8930 567502.3330 5938437.0430 567492.0120 5938432.1900 567488.9810 5938432.6940 567488.8880 5938432.1330 567420.1430 5938449.0830 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_2ebdd987-d08f-4ec3-973d-8460172cb2d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567243.3020 5938388.6740</gml:lowerCorner>
          <gml:upperCorner>567503.5760 5938460.6870</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>§ 1 Absatz 1</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_745cbce1-ab01-4c1d-8b64-e1c113ac9c27" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d1875776-f791-4f2b-8c5e-c5ade8cf90d9">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_3905cfc9-e4b4-40d8-a54e-149fb5ba4ce7">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="9">567420.1430 5938449.0830 567321.8130 5938460.6870 567272.0620 5938460.1610 567250.6190 5938459.9350 567243.3020 5938459.8580 567243.5780 5938457.0780 567244.4600 5938448.2060 567244.9260 5938443.5110 567248.1810 5938443.7100 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="5">567248.1810 5938443.7100 567260.0919 5938444.2500 567272.0140 5938444.4080 567318.2481 5938441.3616 567363.7388 5938432.5602 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">567363.7388 5938432.5602 567371.1250 5938430.6440 567405.5871 5938421.7015 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567405.5871 5938421.7015 567440.4639 5938409.0052 567472.3030 5938389.9300 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="3">567472.3030 5938389.9300 567477.1410 5938388.6740 567477.9280 5938391.7110 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567477.9280 5938391.7110 567479.9195 5938398.7946 567487.2770 5938398.8940 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">567487.2770 5938398.8940 567491.1250 5938401.7750 567493.5050 5938404.7690 567499.2180 5938420.7110 567501.1700 5938426.3830 567503.5760 5938436.5350 567503.2310 5938436.8930 567502.3330 5938437.0430 567492.0120 5938432.1900 567488.9810 5938432.6940 567488.8880 5938432.1330 567420.1430 5938449.0830 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_43a25431-a698-451a-af8d-ea0781ac5662">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567253.1340 5938267.9090</gml:lowerCorner>
          <gml:upperCorner>567357.8090 5938367.3860</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>§ 1 Absatz 1</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_745cbce1-ab01-4c1d-8b64-e1c113ac9c27" />
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_e9bb5704-efde-4c48-801f-cb2f2f8ca774">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="13">567340.5410 5938275.6630 567351.7820 5938318.9680 567357.8090 5938342.1950 567307.4970 5938355.2490 567274.6040 5938363.7860 567260.7280 5938367.3860 567253.1340 5938360.8850 567253.2780 5938359.4300 567255.0540 5938341.5070 567256.6320 5938325.5930 567258.2080 5938309.6820 567262.3480 5938267.9090 567340.5410 5938275.6630 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d3776f35-766b-422a-a9bf-698f83c0a55c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567111.4500 5938504.5500</gml:lowerCorner>
          <gml:upperCorner>567137.0000 5938551.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_d468c554-71b1-4ab0-9acd-e07de187a255">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567111.4500 5938551.0000 567111.4500 5938504.5500 567137.0000 5938504.5500 567137.0000 5938551.0000 567111.4500 5938551.0000 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_a901293d-3d84-462e-98f3-e29fae2ed376">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567495.6890 5938005.2345</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938088.1886</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_60c94ceb-7326-409f-a019-19b030d46517">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">567503.1714 5938005.2345 567542.8830 5938038.2700 567546.7590 5938038.4690 567560.7570 5938039.1860 567560.9460 5938042.3340 567582.8240 5938061.5679 567582.8240 5938088.1886 567579.6650 5938085.4110 567495.6890 5938014.2290 567503.1714 5938005.2345 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1800</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_dea227da-2b90-49a9-a4c9-d7d5cb4a5c23">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567429.6710 5938260.3800</gml:lowerCorner>
          <gml:upperCorner>567502.9980 5938323.5430</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_308d9453-aa40-4043-8682-5ed5a84ff118" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_68c3e36d-17db-47b2-841b-3115d746d493">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567502.9980 5938308.1430 567497.1500 5938309.2140 567455.4670 5938316.8500 567429.6710 5938323.5430 567435.3230 5938272.8780 567443.9210 5938260.3800 567502.9980 5938308.1430 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>14000</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_8fd437f4-91ba-44cc-b5b1-bf484f1e5da1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567297.7620 5938154.3550</gml:lowerCorner>
          <gml:upperCorner>567378.6070 5938224.9820</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_43183b1e-2ce2-4c50-a3fd-e4129deb4c11">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567378.6070 5938207.5810 567354.1530 5938224.9820 567314.7540 5938201.2540 567297.7620 5938190.3120 567312.7680 5938154.3550 567378.6070 5938207.5810 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>2200</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_fe0ae921-3ed8-47e7-a248-2ae10edfde1f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567130.3051 5938301.5920</gml:lowerCorner>
          <gml:upperCorner>567227.9465 5938375.1410</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_0f0c620f-d9e2-406f-99a2-aecab58b4951">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_b12a70f8-575c-4342-8cb5-b2dfa19c645c">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="8">567223.2070 5938358.7170 567221.9000 5938371.8860 567218.1090 5938375.1410 567209.3040 5938374.6740 567197.0280 5938374.0210 567181.8030 5938373.2110 567161.7700 5938372.1460 567135.9330 5938370.7720 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="11">567135.9330 5938370.7720 567130.5002 5938365.4105 567132.9400 5938358.1780 567143.4689 5938351.9305 567153.7980 5938345.3580 567162.5579 5938339.4131 567171.1550 5938333.2350 567177.2982 5938328.5948 567183.3500 5938323.8360 567196.2758 5938312.9870 567208.7230 5938301.5920 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="5">567208.7230 5938301.5920 567227.9465 5938310.9621 567227.7730 5938312.7070 567224.4690 5938345.9920 567223.2070 5938358.7170 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_30ccabd6-1c88-4911-a5e0-83d229aefdd6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567154.9635 5938100.6998</gml:lowerCorner>
          <gml:upperCorner>567242.4659 5938217.4315</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_401eb1a3-2a72-471a-9776-3a9d0b5355cf">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567154.9635 5938217.4315 567242.4659 5938100.6998 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:zweckbestimmung>1300</xplan:zweckbestimmung>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_082e2ebd-438e-4adb-b72f-2931152707fe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567436.1330 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567547.6498 5938068.5180</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5da8d081-d16e-4b12-b884-0ab4805c6a18">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">567547.6498 5937951.7580 567503.1714 5938005.2345 567495.6890 5938014.2290 567454.2120 5938064.0890 567454.7050 5938068.5180 567436.1330 5938053.5440 567441.2310 5938053.1310 567477.9700 5938008.9590 567525.5484 5937951.7580 567547.6498 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_82b4c1e5-6ad6-4418-a886-4b8c3d28f6e8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567366.8920 5938001.6150</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938193.6392</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2109fbe6-8f13-4442-ae4a-4e76cadff81d">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">567582.8240 5938171.8087 567582.8240 5938193.6392 567371.7380 5938023.4470 567366.8920 5938024.6760 567376.5140 5938001.6150 567377.7460 5938006.4600 567436.1330 5938053.5440 567454.7050 5938068.5180 567538.6810 5938136.2210 567571.3950 5938162.5940 567582.8240 5938171.8087 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_WaldFlaeche gml:id="GML_1245a1fa-8f64-4a47-9e6b-35cf7fa53ae1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567085.5250 5938043.9637</gml:lowerCorner>
          <gml:upperCorner>567286.6710 5938255.1140</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a532e8c0-b690-4791-b304-6ef2e976f596">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_a05ddb64-1527-4c10-a4e3-5337d89914b3">
                  <gml:segments>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="9">567278.8300 5938152.6240 567270.3078 5938171.2304 567260.5380 5938189.2130 567258.3118 5938192.9353 567256.0320 5938196.6250 567256.0053 5938196.6675 567255.9786 5938196.7100 567241.0312 5938217.3624 567223.5190 5938235.8900 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="10">567223.5190 5938235.8900 567220.8920 5938238.2840 567202.0860 5938255.1140 567195.0900 5938255.0320 567114.8370 5938179.8310 567085.5250 5938152.3580 567166.7777 5938043.9637 567169.0660 5938045.6790 567286.6710 5938133.8360 567278.8300 5938152.6240 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:eigentumsart>2000</xplan:eigentumsart>
    </xplan:BP_WaldFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_WasserwirtschaftsFlaeche gml:id="GML_e3cf8a7d-ebe9-4e50-abff-2ccb4ee3d954">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5938087.0120</gml:lowerCorner>
          <gml:upperCorner>567202.0860 5938276.8660</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1bef9b95-70a1-42e9-989d-739dd6558770">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="18">567114.8370 5938179.8310 567195.0900 5938255.0320 567202.0860 5938255.1140 567188.2570 5938276.8660 567188.2040 5938271.8680 567152.2020 5938238.1350 567081.7630 5938172.1250 567031.0440 5938124.5960 567028.2040 5938121.9340 567015.8040 5938110.3099 567015.8040 5938087.0120 567027.6290 5938098.1010 567041.9961 5938111.5658 567054.8890 5938123.6490 567060.8700 5938129.2550 567062.9330 5938131.1880 567085.5250 5938152.3580 567114.8370 5938179.8310 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1300</xplan:zweckbestimmung>
    </xplan:BP_WasserwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_WasserwirtschaftsFlaeche gml:id="GML_6e3fef2a-c90f-4938-8220-4927fa563eda">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567148.7349 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567262.5460 5938024.1630</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_8e09e6dc-b3c0-43b6-88ac-d2c415ddd0f1">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="11">567206.4676 5937951.7580 567262.5460 5937990.1580 567237.0450 5938024.1630 567204.9790 5938000.1350 567183.1230 5937983.7550 567184.8030 5937981.5170 567186.1230 5937979.7580 567150.4470 5937953.0410 567148.7349 5937951.7580 567205.7353 5937951.7580 567206.4676 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:BP_WasserwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_WasserwirtschaftsFlaeche gml:id="GML_9d1872c2-3bbb-4e26-acd1-0e0e7a178ae7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567267.9361 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567350.8692 5938001.6170</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Regelung des Wasserabflusses</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_f59b6dc7-2fb3-4366-b992-b2d99b38319c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="7">567298.5760 5937951.7580 567310.8460 5937959.9350 567323.8150 5937967.2120 567350.8692 5937980.2003 567341.8490 5938001.6170 567267.9361 5937951.7580 567298.5760 5937951.7580 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_WasserwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_f6640434-2ab9-4ed4-9762-923bc477c0dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567298.5760 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567362.5115 5937980.2003</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_fe4c981c-b264-4157-b591-1b8757f64546">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567310.8460 5937959.9350 567298.5760 5937951.7580 567362.5115 5937951.7580 567350.8692 5937980.2003 567323.8150 5937967.2120 567310.8460 5937959.9350 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_45321a90-08fe-4932-a184-10b7f15d282f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567093.7835 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567139.1140 5937979.1730</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_905e0637-58fc-44e0-b485-abb9d113aa8e">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="6">567139.1140 5937967.0160 567135.6950 5937971.2200 567130.3410 5937979.1730 567093.7835 5937951.7580 567118.7706 5937951.7580 567139.1140 5937967.0160 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_d7b0f8ed-6f59-4f38-979e-bbed137cfd24">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567074.2380 5938238.1350</gml:lowerCorner>
          <gml:upperCorner>567152.2020 5938313.0360</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve srsName="EPSG:25832" gml:id="GML_5b1a8a86-8977-4aac-809c-e4fffa6481ed">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsDimension="2" count="2">567074.2380 5938313.0360 567152.2020 5938238.1350 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_90b9d6bb-761f-424f-a626-f968f4be24ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567533.7420 5938324.0110</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938379.2574</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5c75acb4-3b96-40af-a9d3-acf5c8ee1493">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567533.7420 5938332.9990 567557.0607 5938328.5050 567582.8240 5938324.0110 567582.8240 5938379.2574 567533.7420 5938332.9990 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_fd31dc77-0ace-44db-a0d9-1e3b9f4e6ee0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567281.7050 5938045.5830</gml:lowerCorner>
          <gml:upperCorner>567318.5530 5938070.3960</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>4000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ed007be7-ca1f-43b3-bbd9-591fcfee257c">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567281.7050 5938057.6370 567290.1380 5938045.5830 567318.5530 5938057.4400 567313.1450 5938070.3960 567281.7050 5938057.6370 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_02d77422-eeca-43eb-b9ca-a7f50edf2568">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567463.9020 5938343.2200</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938512.9477</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_bbe187a1-9cfd-4e8c-926d-665a97190492">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_00387732-6f2b-4903-a6e5-d0f566572087">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="26">567547.7130 5938453.5210 567555.5430 5938457.0470 567560.7010 5938458.1680 567571.9860 5938456.7010 567574.6650 5938498.8260 567576.7720 5938504.0870 567582.8240 5938510.0532 567582.8240 5938512.9477 567574.9690 5938505.2070 567571.3720 5938501.9290 567570.8350 5938487.2980 567570.9100 5938481.0730 567568.9620 5938472.1200 567566.3760 5938469.3790 567556.6530 5938469.8310 567556.5240 5938463.7610 567531.8340 5938456.6000 567520.1690 5938453.6130 567510.1240 5938443.6970 567503.2310 5938436.8930 567503.5760 5938436.5350 567501.1700 5938426.3830 567499.2180 5938420.7110 567493.5050 5938404.7690 567491.1250 5938401.7750 567487.2770 5938398.8940 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567487.2770 5938398.8940 567479.9195 5938398.7946 567477.9280 5938391.7110 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="22">567477.9280 5938391.7110 567477.1410 5938388.6740 567474.5570 5938378.7190 567473.8090 5938375.8340 567466.4300 5938347.4040 567463.9020 5938345.7910 567477.9390 5938343.2200 567476.3270 5938345.7470 567485.7370 5938382.0050 567486.5420 5938383.6950 567495.0040 5938400.1430 567498.1260 5938407.3270 567501.0370 5938417.0960 567503.8410 5938424.0480 567510.1440 5938433.5030 567512.5850 5938435.5530 567524.6370 5938433.3200 567524.8330 5938434.4940 567531.7320 5938441.2970 567532.6300 5938441.1460 567544.7220 5938446.8050 567547.7130 5938453.5210 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_3110388e-f0dc-4b54-a941-dda61ba75c3c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567571.3950 5938149.5842</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938171.8087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_567a5ea1-6fec-427e-bdca-30c483125576">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567582.8240 5938171.8087 567571.3950 5938162.5940 567582.8240 5938149.5842 567582.8240 5938171.8087 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_d99b1ba4-8a29-4cc5-8ca3-e99f0767adcf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567240.3440 5938432.1330</gml:lowerCorner>
          <gml:upperCorner>567510.3200 5938489.6450</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_2fc85fc4-fd3f-4f7b-bf15-d62f96153f1f">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="23">567410.7550 5938466.4850 567354.8210 5938477.0320 567323.1730 5938482.9970 567298.3200 5938485.7590 567284.8120 5938486.6650 567240.3440 5938489.6450 567243.3020 5938459.8580 567250.6190 5938459.9350 567272.0620 5938460.1610 567321.8130 5938460.6870 567420.1430 5938449.0830 567488.8880 5938432.1330 567488.9810 5938432.6940 567492.0120 5938432.1900 567502.3330 5938437.0430 567503.2310 5938436.8930 567510.1240 5938443.6970 567510.3200 5938444.8710 567509.4220 5938445.0190 567510.0550 5938450.0250 567508.2800 5938451.8630 567502.8240 5938449.1300 567410.7550 5938466.4850 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>10003</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_b9746773-0461-4b84-abca-2190911c06dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567495.6890 5938005.2345</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938088.1886</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_42241834-6cf5-41ba-8ada-c33586419fa5">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="10">567503.1714 5938005.2345 567542.8830 5938038.2700 567546.7590 5938038.4690 567560.7570 5938039.1860 567560.9460 5938042.3340 567582.8240 5938061.5679 567582.8240 5938088.1886 567579.6650 5938085.4110 567495.6890 5938014.2290 567503.1714 5938005.2345 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Gewaesser gml:id="GML_58314478-4223-4d9f-abe9-569150740f5c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.8040 5938447.0744</gml:lowerCorner>
          <gml:upperCorner>567203.9818 5938490.8430</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_ab518d89-bd8b-4d1d-a3bb-814fa36efb56">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_322aa12e-d0ac-49d6-adc6-8f754bf26ac5">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567201.3950 5938465.6960 567202.6880 5938465.8040 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567202.6880 5938465.8040 567203.9791 5938467.3156 567202.5110 5938468.6560 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="2">567202.5110 5938468.6560 567201.9700 5938477.3750 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567201.9700 5938477.3750 567203.2592 5938478.9822 567201.6539 5938480.2738 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="17">567201.6539 5938480.2738 567200.4760 5938480.2030 567195.5720 5938479.9070 567195.4350 5938483.5140 567194.6260 5938483.4690 567194.2200 5938490.8430 567172.9860 5938488.9820 567110.5100 5938483.5160 567070.6340 5938480.4600 567015.8040 5938476.8211 567015.8040 5938447.0744 567038.0710 5938448.4420 567091.8440 5938451.7440 567195.8610 5938455.4280 567198.6480 5938455.5270 567197.9060 5938465.4030 567201.3950 5938465.6960 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>2000</xplan:artDerFestlegung>
    </xplan:SO_Gewaesser>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Luftverkehrsrecht gml:id="GML_e1ec8789-e611-491e-994b-d0d14e4d323e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567503.1714 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938038.4690</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_7497eaee-7eb0-415a-8c80-2ac9a721c416">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567582.8240 5937997.3508 567551.0130 5938033.6200 567546.7590 5938038.4690 567542.8830 5938038.2700 567503.1714 5938005.2345 567547.6498 5937951.7580 567582.8240 5937951.7580 567582.8240 5937997.3508 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>7000</xplan:artDerFestlegung>
    </xplan:SO_Luftverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Luftverkehrsrecht gml:id="GML_c4ec3a94-59cf-4892-9e34-9a8d602fac0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.5110 5938005.5620</gml:lowerCorner>
          <gml:upperCorner>567298.8100 5938057.6370</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_5015b9d5-8a12-40e9-bdb5-ef277d260c82">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="8">567258.5110 5938040.2450 567284.6700 5938005.5620 567298.8100 5938014.9900 567291.4950 5938032.5190 567291.1700 5938043.1100 567290.1380 5938045.5830 567281.7050 5938057.6370 567258.5110 5938040.2450 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:SO_Luftverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_50159767-b7e5-4cf4-8cdd-e6ca8dd9d9ed">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567324.0000 5938149.0000</gml:lowerCorner>
          <gml:upperCorner>567325.0000 5938149.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_1278dcb0-22fc-4167-b955-d81bed1842cf">
          <gml:pos>567324.0000 5938149.0000</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:nordwinkel uom="grad">90.000000</xplan:nordwinkel>
      <xplan:artDerFestlegung>14003</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_28f122f0-3a41-4053-8b36-94fda694eaf3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567308.2260 5938128.0860</gml:lowerCorner>
          <gml:upperCorner>567557.0607 5938332.9990</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_12964979-2e83-4956-a1db-b7a13d74d6be">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="9">567378.6070 5938207.5810 567312.7680 5938154.3550 567308.2260 5938150.6810 567318.0206 5938128.0860 567557.0607 5938328.5050 567533.7420 5938332.9990 567502.9980 5938308.1430 567443.9210 5938260.3800 567378.6070 5938207.5810 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_f65a30d4-4ab5-46ec-b132-871ff48cd567">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567249.1130 5938308.1430</gml:lowerCorner>
          <gml:upperCorner>567533.7420 5938401.3790</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_341e4d48-8211-4423-994d-4363b8224ddb">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="20">567356.3100 5938373.5620 567319.9310 5938383.0020 567283.5550 5938392.4420 567249.1130 5938401.3790 567260.7280 5938367.3860 567274.6040 5938363.7860 567307.4970 5938355.2490 567357.8090 5938342.1950 567412.9600 5938327.8800 567429.6710 5938323.5430 567455.4670 5938316.8500 567497.1500 5938309.2140 567502.9980 5938308.1430 567533.7420 5938332.9990 567526.2240 5938334.3760 567477.9390 5938343.2200 567463.9020 5938345.7910 567429.0690 5938354.6790 567392.6910 5938364.1200 567356.3100 5938373.5620 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_037a453e-a463-4174-bee1-20c19cf333dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567209.9910 5938367.3860</gml:lowerCorner>
          <gml:upperCorner>567260.7280 5938404.7160</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Höhengleiche Kreuzung mit Straße</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_63135c57-8063-4832-b792-98b48fb22cfe">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567249.1130 5938401.3790 567209.9910 5938404.7160 567218.1090 5938375.1410 567260.7280 5938367.3860 567249.1130 5938401.3790 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_7d67f197-d2a6-46bb-8246-8152ab098cd7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567081.8130 5938308.1430</gml:lowerCorner>
          <gml:upperCorner>567533.7420 5938404.7160</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_a2e6b33c-b27e-4611-b913-3b8722abd655">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_dd2c460e-4b9e-4362-bbc2-66cb2f51f0c9">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="12">567319.9310 5938383.0020 567283.5550 5938392.4420 567249.1130 5938401.3790 567209.9910 5938404.7160 567201.6980 5938404.2580 567193.2140 5938403.7890 567173.7660 5938402.7150 567153.8490 5938401.6700 567133.8780 5938400.6210 567113.8010 5938399.5670 567081.8130 5938397.8850 567132.9400 5938358.1780 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567132.9400 5938358.1780 567130.5002 5938365.4105 567135.9330 5938370.7720 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="23">567135.9330 5938370.7720 567161.7700 5938372.1460 567181.8030 5938373.2110 567197.0280 5938374.0210 567209.3040 5938374.6740 567218.1090 5938375.1410 567260.7280 5938367.3860 567274.6040 5938363.7860 567307.4970 5938355.2490 567357.8090 5938342.1950 567412.9600 5938327.8800 567429.6710 5938323.5430 567455.4670 5938316.8500 567497.1500 5938309.2140 567502.9980 5938308.1430 567533.7420 5938332.9990 567526.2240 5938334.3760 567477.9390 5938343.2200 567463.9020 5938345.7910 567429.0690 5938354.6790 567392.6910 5938364.1200 567356.3100 5938373.5620 567319.9310 5938383.0020 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_bdf97adf-d8a9-4dae-8362-a09a73a51fa0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567081.8130 5938358.1780</gml:lowerCorner>
          <gml:upperCorner>567218.1090 5938404.7160</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_1a2fe21c-2047-479b-88da-fe010e4d922b">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve srsName="EPSG:25832" gml:id="GML_33ec2fe4-ddd5-4676-af33-1005873382ab">
                  <gml:segments>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="9">567209.9910 5938404.7160 567201.6980 5938404.2580 567193.2140 5938403.7890 567173.7660 5938402.7150 567153.8490 5938401.6700 567133.8780 5938400.6210 567113.8010 5938399.5670 567081.8130 5938397.8850 567132.9400 5938358.1780 </gml:posList>
                    </gml:LineStringSegment>
                    <gml:ArcString interpolation="circularArc3Points">
                      <gml:posList srsDimension="2" count="3">567132.9400 5938358.1780 567130.5002 5938365.4105 567135.9330 5938370.7720 </gml:posList>
                    </gml:ArcString>
                    <gml:LineStringSegment interpolation="linear">
                      <gml:posList srsDimension="2" count="7">567135.9330 5938370.7720 567161.7700 5938372.1460 567181.8030 5938373.2110 567197.0280 5938374.0210 567209.3040 5938374.6740 567218.1090 5938375.1410 567209.9910 5938404.7160 </gml:posList>
                    </gml:LineStringSegment>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_SchutzgebietWasserrecht gml:id="GML_60fc0a9d-d553-4aa5-ad6f-a7e29c6fa27b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567281.7050 5938045.5830</gml:lowerCorner>
          <gml:upperCorner>567318.5530 5938070.3960</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Festgestelltes Wasserschutzgebiet</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_b4434a50-31f1-432e-b969-ca2c1013e4b9">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="5">567281.7050 5938057.6370 567290.1380 5938045.5830 567318.5530 5938057.4400 567313.1450 5938070.3960 567281.7050 5938057.6370 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_SchutzgebietWasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Strassenverkehrsrecht gml:id="GML_c43ed4b4-0754-4ac9-a1cc-35bd57fa6d9c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567043.8114 5937951.7580</gml:lowerCorner>
          <gml:upperCorner>567303.3160 5938137.3610</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Festgestellte Bundesfernstraße</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_08ab9fc7-e5f4-4cfc-b7cf-bece522f5b00">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="21">567302.9340 5938108.5460 567303.3160 5938108.8320 567302.2890 5938111.3300 567302.2990 5938111.4490 567302.5660 5938111.5610 567292.6800 5938135.1530 567292.5366 5938135.0936 567292.3555 5938135.0186 567292.3209 5938135.1008 567291.3710 5938137.3610 567286.6710 5938133.8360 567169.0660 5938045.6790 567166.7777 5938043.9637 567152.0690 5938032.9380 567135.6390 5938020.6200 567113.2912 5938003.8639 567096.8400 5937991.5290 567043.8114 5937951.7580 567093.7835 5937951.7580 567130.3410 5937979.1730 567302.9340 5938108.5460 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
    </xplan:SO_Strassenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_e6cd43b8-806e-4971-be2a-a8d252be2aea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567571.3950 5938149.5842</gml:lowerCorner>
          <gml:upperCorner>567582.8240 5938171.8087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Hochwassergefährdeter Bereich</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon srsName="EPSG:25832" gml:id="GML_71303681-c5dc-4964-9664-465603792880">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsDimension="2" count="4">567582.8240 5938171.8087 567571.3950 5938162.5940 567582.8240 5938149.5842 567582.8240 5938171.8087 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>20002</xplan:artDerFestlegung>
      <xplan:istNatuerlichesUberschwemmungsgebiet>false</xplan:istNatuerlichesUberschwemmungsgebiet>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3631c68f-2533-434a-83b5-a6609699f79b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567099.3383 5938423.5187</gml:lowerCorner>
          <gml:upperCorner>567100.3383 5938424.5187</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bauweise[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_4359728b-3ecf-449d-8a36-b898ae51fb20">
          <gml:pos>567099.3383 5938423.5187</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6396f67c-7150-44f2-9af9-7ea5e03d8f0b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567480.9219 5938491.1720</gml:lowerCorner>
          <gml:upperCorner>567481.9219 5938492.1720</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GF[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_30956564-b989-4f7c-80d6-2745cc311611">
          <gml:pos>567480.9219 5938491.1720</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c4bf1948-787e-4a71-b588-8d2091868fc9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567123.2817 5938425.5968</gml:lowerCorner>
          <gml:upperCorner>567124.2817 5938426.5968</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d77fc67e-405a-4efd-b0a2-ce5cec452e33">
          <gml:pos>567123.2817 5938425.5968</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9bc001e7-69c3-4f5e-8146-97955e8d01f1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567349.4425 5938517.5583</gml:lowerCorner>
          <gml:upperCorner>567350.4425 5938518.5583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GFZmax[1]</xplan:art>
      <xplan:art>xplan:GFZmin[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_03f69510-4fb6-4c32-88ff-31b7d15dfed1">
          <gml:pos>567349.4425 5938517.5583</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_da614353-3e14-4548-a602-1dca72637536">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.0666 5938509.3800</gml:lowerCorner>
          <gml:upperCorner>567348.0666 5938510.3800</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe[1]/xplan:h[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_79cde361-94b5-4e95-87bf-4cd1ee205525">
          <gml:pos>567347.0666 5938509.3800</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ee9a801b-dc41-406b-b406-16f09b4f0499">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567087.6039 5938534.0543</gml:lowerCorner>
          <gml:upperCorner>567088.6039 5938535.0543</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Z[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_358f9e1c-039e-4128-8636-c02ade8ea83e">
          <gml:pos>567087.6039 5938534.0543</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_63eda789-900b-4f83-a52a-e2d7d0cd0faf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567540.5979 5938399.6738</gml:lowerCorner>
          <gml:upperCorner>567541.5979 5938399.6738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_62517d2f-46c3-4130-b39e-1ed9b30da567" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d7954e34-c9b3-4039-8a66-95f32436859e">
          <gml:pos>567540.5979 5938399.6738</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_848f0251-7f04-451e-a5c3-5b849761fe28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567476.1528 5938500.2706</gml:lowerCorner>
          <gml:upperCorner>567477.1528 5938501.2706</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Zzwingend[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c48a8916-a329-4c98-9938-78cb5dee9e42">
          <gml:pos>567476.1528 5938500.2706</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8e3c5fa1-0b8a-4a21-9fea-50e94d65f899">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567494.6736 5938506.9498</gml:lowerCorner>
          <gml:upperCorner>567495.6736 5938506.9498</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ce69c7ab-873a-4dd7-a496-315b14aab2c6" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_cf930979-28dc-4ed0-93dd-d4be0c24875a">
          <gml:pos>567494.6736 5938506.9498</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f5237d5a-b68d-4a1c-8d3c-2c4daa707588">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567183.3043 5938533.5751</gml:lowerCorner>
          <gml:upperCorner>567184.3043 5938534.5751</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_42288296-23d8-4a39-b42c-be79b068bdca">
          <gml:pos>567183.3043 5938533.5751</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e96936cf-2271-4af4-ae6b-c0c2382100c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567026.0000 5938332.0000</gml:lowerCorner>
          <gml:upperCorner>567027.0000 5938333.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:nutzungsform[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_5a8e0bd9-c528-406a-993b-e4a573a16017" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_7ac32eac-ccda-483a-a8f7-9cc907e250d0">
          <gml:pos>567026.0000 5938332.0000</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1b8d0589-6bf4-40fb-a071-e38c0e061914">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567406.0000 5938379.0000</gml:lowerCorner>
          <gml:upperCorner>567407.0000 5938380.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_285c0249-a226-4329-8adb-f0f344309ebd" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a4bc3ba8-6539-443e-a179-e9a2b448ebaa">
          <gml:pos>567406.0000 5938379.0000</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e9659569-957f-416a-8c7b-346be20371f8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567157.3173 5938434.6382</gml:lowerCorner>
          <gml:upperCorner>567158.3173 5938435.6382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:dachgestaltung[1]/xplan:BP_Dachgestaltung[1]/xplan:dachform[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_473b8cf7-e68e-490c-a797-74bbad53ad36">
          <gml:pos>567157.3173 5938434.6382</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_36b30dcd-2d84-4cad-94fa-d6e02c711b16">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567265.3258 5938008.8716</gml:lowerCorner>
          <gml:upperCorner>567266.3258 5938008.8716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_599f5ba0-0938-4c55-a193-97bf6d53c0c4" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_745ca346-fea4-43ba-bd61-4fff8e68da9b">
          <gml:pos>567265.3258 5938008.8716</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_626ddd65-14ee-4532-9d7c-0cdd20fb4a29">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567257.5220 5938538.2776</gml:lowerCorner>
          <gml:upperCorner>567258.5220 5938539.2776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_53ec58f3-8475-42ee-afa4-751351fe8f16">
          <gml:pos>567257.5220 5938538.2776</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1e7dd689-8623-4bd0-b4e4-b03299e77ac0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567296.7971 5937995.1941</gml:lowerCorner>
          <gml:upperCorner>567297.7971 5937996.1941</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bdb12f92-c818-4c99-91da-14c3a319192c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e9e0e595-2a25-4c92-a40a-bb5555b580b8">
          <gml:pos>567296.7971 5937995.1941</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1c12ef22-0e3e-4a95-a9d9-75f8669b1f59">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.5273 5938508.2523</gml:lowerCorner>
          <gml:upperCorner>567076.5273 5938509.2523</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3d91b07e-3e1e-4d5a-9880-bdc56d1b7c8d">
          <gml:pos>567075.5273 5938508.2523</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e6a04548-9ba4-4b9d-bc5b-0d5a6977fe87">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567244.4500 5938530.9744</gml:lowerCorner>
          <gml:upperCorner>567245.4500 5938531.9744</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GR[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_439ee84d-0279-486d-9723-8506ae0ca866">
          <gml:pos>567244.4500 5938530.9744</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1cbff272-89c7-412f-bb31-8ddc38544954">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567188.7995 5938498.5341</gml:lowerCorner>
          <gml:upperCorner>567189.7995 5938499.5341</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bebauungsArt[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_ec600642-fd8c-4f93-80d6-80988746850e">
          <gml:pos>567188.7995 5938498.5341</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1f6eeb5a-8eb4-40de-b1a7-ff94cbc8b5ee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567187.2294 5938528.3451</gml:lowerCorner>
          <gml:upperCorner>567188.2294 5938529.3451</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ_Ausn[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_a784e9e6-f988-4a3f-a0e0-6010a52cdd94">
          <gml:pos>567187.2294 5938528.3451</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4470427b-a9ad-4576-8aa4-d9822d4a12c6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567121.5584 5938414.4026</gml:lowerCorner>
          <gml:upperCorner>567122.5584 5938415.4026</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:MaxZahlWohnungen[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_fc7b213f-2197-4f5e-a3ee-8b74467363b3">
          <gml:pos>567121.5584 5938414.4026</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cd84ba26-68f2-45ab-9903-df772de1b93f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567329.0000 5938401.0000</gml:lowerCorner>
          <gml:upperCorner>567330.0000 5938402.0000</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dc60c672-b6ca-442c-b761-f56982073dd5" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_86325012-4ef7-4abb-9dc7-1b4a9e7bda4f">
          <gml:pos>567329.0000 5938401.0000</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_691d25f5-2077-4da5-86f0-d3016d33cdfa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567250.0813 5938513.4278</gml:lowerCorner>
          <gml:upperCorner>567251.0813 5938514.4278</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Zmax[1]</xplan:art>
      <xplan:art>xplan:Zmin[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_271c5afd-b536-4e27-b414-eaa76a85dbeb">
          <gml:pos>567250.0813 5938513.4278</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9333fe15-dd95-4aff-aebe-c37a48e4d2ec">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567192.4629 5938514.7471</gml:lowerCorner>
          <gml:upperCorner>567193.4629 5938515.7471</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Z[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_2b88e503-88ff-49e0-b61a-cb1f5d8ab3f4">
          <gml:pos>567192.4629 5938514.7471</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3bfb88ba-1a31-45d6-b971-715d3406aced">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567251.3401 5938526.0072</gml:lowerCorner>
          <gml:upperCorner>567252.3401 5938527.0072</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GR_Ausn[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_8a3b0bec-89c1-407d-8a9e-ff1e9aab15b1">
          <gml:pos>567251.3401 5938526.0072</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7c0d896e-0f26-40e2-b0db-41ae4312b1ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567081.1047 5938516.8680</gml:lowerCorner>
          <gml:upperCorner>567082.1047 5938517.8680</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bebauungsArt[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_735533f3-90bb-4209-af2e-6cdf1d2ec564">
          <gml:pos>567081.1047 5938516.8680</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_310624ef-717f-49b0-940d-65baa00d56e1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567407.1882 5938390.7536</gml:lowerCorner>
          <gml:upperCorner>567408.1882 5938390.7536</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_285c0249-a226-4329-8adb-f0f344309ebd" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_0df4fe02-a9e3-400e-8f08-c9c51312cdbb">
          <gml:pos>567407.1882 5938390.7536</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2611961f-cc1d-46ce-b049-a48b5f679ba4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567349.7065 5938525.2089</gml:lowerCorner>
          <gml:upperCorner>567350.7065 5938526.2089</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_cbbf07da-fa1f-4fb3-a343-2977c4798692">
          <gml:pos>567349.7065 5938525.2089</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1c64bb25-3c80-4f35-b892-abbf124e1401">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567260.9907 5938494.1393</gml:lowerCorner>
          <gml:upperCorner>567261.9907 5938495.1393</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:MaxZahlWohnungen[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_37375f30-503d-4263-bfd6-2213deddb929" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_9f9a2e77-26f0-4bb7-afbf-7dbd95548f47">
          <gml:pos>567260.9907 5938494.1393</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_781d3793-ab82-441f-ab28-da69a48aad26">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567154.2078 5938429.0864</gml:lowerCorner>
          <gml:upperCorner>567155.2078 5938430.0864</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:DN[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_5033e111-a8ae-41ec-bba1-60efe93a93dd">
          <gml:pos>567154.2078 5938429.0864</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_64200842-66f5-429b-9963-0e83d2e75a1f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567451.6698 5937968.1108</gml:lowerCorner>
          <gml:upperCorner>567452.6698 5937968.1108</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f7b713f4-b5c3-4dd2-bd72-6d3dbaf5f2da" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dfe2215a-3233-48a0-b300-974975706957">
          <gml:pos>567451.6698 5937968.1108</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_24f81e5a-5f25-4070-b617-e9d8f3073680">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567122.8508 5938435.9299</gml:lowerCorner>
          <gml:upperCorner>567123.8508 5938436.9299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Z[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_47b3f294-2856-4aef-8da2-dbeb0aed53c4">
          <gml:pos>567122.8508 5938435.9299</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_05ed645d-d470-405a-a390-e2683c21bf4e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567093.0176 5938531.9735</gml:lowerCorner>
          <gml:upperCorner>567094.0176 5938532.9735</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bauweise[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_547646b8-77bd-4ee1-a6e0-46dd95752321">
          <gml:pos>567093.0176 5938531.9735</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c3c38f42-6c38-4ab1-822f-4f345f7aa2e2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.8646 5938513.9626</gml:lowerCorner>
          <gml:upperCorner>567196.8646 5938514.9626</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bauweise[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_bc53005e-5591-402e-9fde-2510a6064a8f">
          <gml:pos>567195.8646 5938513.9626</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e64e9539-4b62-40b6-b5fc-71d0085bbded">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567186.5186 5938515.1585</gml:lowerCorner>
          <gml:upperCorner>567187.5186 5938515.1585</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_053c8ad1-201f-4ee3-8a16-04f948c0142a">
          <gml:pos>567186.5186 5938515.1585</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7f7068c8-bc67-4e27-a516-6ff1c2f8e9f7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.9727 5938425.1337</gml:lowerCorner>
          <gml:upperCorner>567093.9727 5938425.1337</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_00c956a7-c970-4a71-ad80-62d0b4c89f52" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_01807fca-716c-48ce-9181-16721623c378">
          <gml:pos>567092.9727 5938425.1337</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8499585b-e9b2-4c34-8fc0-9f69407dc54b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567485.7912 5937976.1086</gml:lowerCorner>
          <gml:upperCorner>567486.7912 5937976.1086</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_df5f8049-6e74-4625-9771-3816a8cb488e" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d2bb0f65-47a3-4e20-a49d-95472189bcc6">
          <gml:pos>567485.7912 5937976.1086</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_47750462-dbee-4124-a6a2-3f24446b54ee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567073.4451 5938532.8058</gml:lowerCorner>
          <gml:upperCorner>567074.4451 5938533.8058</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_3a10241e-0f87-4a5a-96fc-1df5d403b11b">
          <gml:pos>567073.4451 5938532.8058</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_592a57e2-7c92-49c8-8273-fb49ff7c662e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567464.8063 5938417.9859</gml:lowerCorner>
          <gml:upperCorner>567465.8063 5938417.9859</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_611be92c-f6e2-48c5-99a9-563cc1f00241" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_efb06e83-0674-405c-89d4-bf1d84d40b5a">
          <gml:pos>567464.8063 5938417.9859</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8eab63a2-fb13-408c-ac41-bbb194b2faae">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567184.8744 5938507.1636</gml:lowerCorner>
          <gml:upperCorner>567185.8744 5938508.1636</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GFZ[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f60afcda-b66b-424a-a013-9d6009892de2" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_4daa9089-7d9b-48bf-8040-6e235b02730d">
          <gml:pos>567184.8744 5938507.1636</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b36639b0-cc62-49d8-a553-52da27715c5f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.5273 5938501.5938</gml:lowerCorner>
          <gml:upperCorner>567076.5273 5938502.5938</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GFZ[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7c55b006-758e-497e-8d01-b155ef84691c" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_dfccfae8-82ca-4deb-aec6-8635687969f9">
          <gml:pos>567075.5273 5938501.5938</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_28bc8afe-e15d-4e75-85d8-224c50a4af0e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567348.9168 5938536.8146</gml:lowerCorner>
          <gml:upperCorner>567349.9168 5938537.8146</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:dachform[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d636d5b5-e9bf-4bc1-b225-f08a4a7012ff" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_d16b7288-741e-467c-8a57-389caf82fe42">
          <gml:pos>567348.9168 5938536.8146</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3510055f-d1f0-44c1-b29d-98ccf18e970a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567329.7383 5938410.0866</gml:lowerCorner>
          <gml:upperCorner>567330.7383 5938410.0866</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dc60c672-b6ca-442c-b761-f56982073dd5" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_e0765d30-aabf-4d2c-b49e-d821ddb4a839">
          <gml:pos>567329.7383 5938410.0866</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_99a87f6d-9ca7-4a11-af2d-16c1afd84c32">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.3557 5938246.7381</gml:lowerCorner>
          <gml:upperCorner>567348.3557 5938247.7381</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_21205338-39ee-4ed0-bce7-708290061eb5" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_de351d5f-3c7c-46eb-acd2-796e45d1ae62">
          <gml:pos>567347.3557 5938246.7381</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_522f5f21-778f-469d-af6a-9a06eba37c6e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567554.8098 5937986.8797</gml:lowerCorner>
          <gml:upperCorner>567555.8098 5937986.8797</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fe7cf17d-0c8b-40c6-99d3-5b6a9ade911a" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_b0d84820-8e20-4309-9fe4-ccc22c22d1ce">
          <gml:pos>567554.8098 5937986.8797</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0009e672-4509-41d6-9da2-09c36daa0e72">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567571.2366 5938028.1186</gml:lowerCorner>
          <gml:upperCorner>567572.2366 5938028.1186</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_74b20dc5-b262-4ed7-8524-7bf1a48c0cf3" />
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_c4581d9a-2451-49fc-9035-acf11ae96a80">
          <gml:pos>567571.2366 5938028.1186</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
      <xplan:skalierung>1.0</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_308d9453-aa40-4043-8682-5ed5a84ff118">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567443.1391 5938291.4874</gml:lowerCorner>
          <gml:upperCorner>567444.1391 5938292.4874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:zweckbestimmung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dea227da-2b90-49a9-a4c9-d7d5cb4a5c23" />
      <xplan:schriftinhalt>Blockheizkraftwerk</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_1ef71d50-8596-4f11-80ba-cb37785f6fa5">
          <gml:pos>567443.1391 5938291.4874</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_151402d9-30cc-4c58-838e-3bcc54c787c9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567297.1258 5938320.7773</gml:lowerCorner>
          <gml:upperCorner>567298.1258 5938321.7773</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:zweckbestimmung[2]</xplan:art>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ea776d6f-1f0a-4f00-9fe1-cfdb574de904" />
      <xplan:schriftinhalt>Schule</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_81bbe67c-d3a9-40eb-b450-7654ebb09bc7">
          <gml:pos>567297.1258 5938320.7773</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.00</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_1af29368-7422-4575-af98-6681f7f61f64">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567288.4799 5938297.9574</gml:lowerCorner>
          <gml:upperCorner>567289.4799 5938298.9574</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:detaillierteZweckbestimmung[1]</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_3c383c11-1942-41d9-9866-1f2216925566" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ea776d6f-1f0a-4f00-9fe1-cfdb574de904" />
      <xplan:schriftinhalt>Kindergarten</xplan:schriftinhalt>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point srsName="EPSG:25832" gml:id="GML_25b0c495-6122-476a-8ef9-139b6c83b3de">
          <gml:pos>567288.4799 5938297.9574</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0.0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_745cbce1-ab01-4c1d-8b64-e1c113ac9c27">
      <xplan:schluessel>x.x</xplan:schluessel>
      <xplan:text>Dies ist eine textliche Festsetzung</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
</xplan:XPlanAuszug>