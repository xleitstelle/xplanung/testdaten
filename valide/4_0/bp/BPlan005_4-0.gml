<?xml version="1.0"?>
<XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" gml:id="Gml_FB343E32-6C1E-4631-84D8-1543D66E04E5" xmlns="http://www.xplanung.de/xplangml/4/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
      <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <BP_Plan gml:id="GML_88bfe952-199f-4bba-bea2-c2b441737144">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <name>BPlan005_4-0</name>
      <beschreibung>Der vorhabenbezogene Bebauungsplan BPlan005_4-0
für den Geltungsbereich Knickweg – Barmbeker Straße –
Gertigstraße
(Bezirk Hamburg-Nord, Ortsteil 412) wird festgestellt.</beschreibung>
      <technHerstellDatum>2017-03-20</technHerstellDatum>      
      <erstellungsMassstab>1000</erstellungsMassstab>
      <xPlanGMLVersion>4.0</xPlanGMLVersion>
      <raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_CF4575B7-58E4-442B-9238-E946DF38D86D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </raeumlicherGeltungsbereich>
	  <refBegruendung>
        <XP_ExterneReferenz>
          <referenzURL>BPlan005_4-0.pdf</referenzURL>
        </XP_ExterneReferenz>
	  </refBegruendung>
      <texte xlink:href="#GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa" />
      <texte xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <texte xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <texte xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <texte xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <texte xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <texte xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <texte xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <texte xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <gemeinde>
        <XP_Gemeinde>
          <ags>02000000</ags>
          <gemeindeName>Freie und Hansestadt Hamburg</gemeindeName>
          <ortsteilName>412</ortsteilName>
        </XP_Gemeinde>
      </gemeinde>      
      <planArt>3000</planArt>
      <verfahren>1000</verfahren>
      <rechtsstand>3000</rechtsstand>
      <rechtsverordnungsDatum>2016-10-25</rechtsverordnungsDatum>
      <veraenderungssperre>false</veraenderungssperre>
      <staedtebaulicherVertrag>false</staedtebaulicherVertrag>
      <erschliessungsVertrag>false</erschliessungsVertrag>
      <durchfuehrungsVertrag>true</durchfuehrungsVertrag>
      <gruenordnungsplan>false</gruenordnungsplan>
      <bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
    </BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <BP_Bereich gml:id="GML_6e871e4c-58e1-4b97-bf2a-a696816b2458">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <nummer>0</nummer>
      <geltungsbereich>
        <gml:Polygon gml:id="Gml_E0B2D545-824C-4FB4-930B-715956E24C64" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </geltungsbereich>
      <nachrichtlich xlink:href="#GML_be2c01dd-4d15-4a6a-85c0-dc9565324486" />
      <nachrichtlich xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <praesentationsobjekt xlink:href="#GML_de628759-edb3-4f31-9d43-7d4f816bfb0c" />
      <praesentationsobjekt xlink:href="#GML_82075a17-ae02-4228-a4ac-debedfd66c1e" />
      <praesentationsobjekt xlink:href="#GML_a435c756-f459-41ce-b27c-d5992aa8f87e" />
      <praesentationsobjekt xlink:href="#GML_7d7588c9-1211-471e-ad15-8dd9f6605701" />
      <praesentationsobjekt xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <praesentationsobjekt xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <praesentationsobjekt xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <praesentationsobjekt xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <praesentationsobjekt xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <praesentationsobjekt xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <praesentationsobjekt xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <praesentationsobjekt xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <praesentationsobjekt xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <praesentationsobjekt xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <praesentationsobjekt xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <praesentationsobjekt xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <praesentationsobjekt xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <praesentationsobjekt xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <praesentationsobjekt xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <praesentationsobjekt xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <praesentationsobjekt xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <praesentationsobjekt xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <praesentationsobjekt xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <praesentationsobjekt xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <praesentationsobjekt xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <praesentationsobjekt xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />     
      <gehoertZuPlan xlink:href="#GML_88bfe952-199f-4bba-bea2-c2b441737144" />
      <inhaltBPlan xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <inhaltBPlan xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <inhaltBPlan xlink:href="#GML_5276e223-57f7-4596-9c9c-8fb909e11db2" />
      <inhaltBPlan xlink:href="#GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e" />
      <inhaltBPlan xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <inhaltBPlan xlink:href="#GML_354dc6b6-d910-4f29-8aeb-66d238013da6" />
      <inhaltBPlan xlink:href="#GML_b607f50f-8221-4811-81f0-7f6efef25db3" />
      <inhaltBPlan xlink:href="#GML_20f74701-9a0b-4c38-89b8-ae81db151e28" />
      <inhaltBPlan xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <inhaltBPlan xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <inhaltBPlan xlink:href="#GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f" />
      <inhaltBPlan xlink:href="#GML_9875e68a-2b21-40e4-8f66-ecf5c2290936" />
      <inhaltBPlan xlink:href="#GML_7b11be88-979b-483f-8042-82c51eff70af" />
      <inhaltBPlan xlink:href="#GML_21c90a12-5886-40dd-8a86-463267b6e201" />
      <inhaltBPlan xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <inhaltBPlan xlink:href="#GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299" />
      <inhaltBPlan xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <inhaltBPlan xlink:href="#GML_8376d624-8ccb-473d-81df-fc95b1ef28b3" />
      <inhaltBPlan xlink:href="#GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3" />
      <inhaltBPlan xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <inhaltBPlan xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <inhaltBPlan xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <inhaltBPlan xlink:href="#GML_5355860b-25fa-401c-a2ea-bb70654d17ab" />
      <inhaltBPlan xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
    </BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <BP_TextlicheFestsetzungsFlaeche gml:id="GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <gliederung2>(A)</gliederung2>
      <ebene>0</ebene>
      <wirdDargestelltDurch xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_0910C319-FAC3-4545-AE87-0D5E532B708B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567433.287 5937679.305 567416.015 5937672.057 567419.988 5937662.431 
567432.605 5937667.621 567439.105 5937652.001 567428.275 5937631.879 
567422.612 5937629.344 567428.448 5937616.596 567434.138 5937619.1 
567436.484 5937623.475 567442.246 5937634.25 567452.958 5937654.188 
567450.566 5937668.248 567438.775 5937666.233 567433.287 5937679.305 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
    </BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567437.222 5937662.769</gml:lowerCorner>
          <gml:upperCorner>567437.222 5937662.769</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>gliederung2</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <position>
        <gml:Point gml:id="Gml_F44FBCBD-6F19-4B48-ABAB-4FB07ABDE642" srsName="EPSG:25832">
          <gml:pos>567437.222 5937662.769</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">30</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <wirdDargestelltDurch xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_C8BC44E3-C087-43F6-B5EB-AEAB79FBD53F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567452.175 5937661.631 567442.18 5937659.754 
567439.277 5937659.277 567439.035 5937655.06 567438.79 5937652.388 
567432.026 5937639.916 567442.758 5937634.17 567453.455 5937654.108 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>7</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_045ae35f-52f5-48d9-b5a6-3d5f339418be">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567444.114 5937651.76</gml:lowerCorner>
          <gml:upperCorner>567444.114 5937651.76</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <position>
        <gml:Point gml:id="Gml_6921C7FC-05E0-4D77-82D3-E1E735C2BC2E" srsName="EPSG:25832">
          <gml:pos>567444.114 5937651.76</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_5890c370-a3bc-412c-b650-13894b87a426">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.328 5937644.053</gml:lowerCorner>
          <gml:upperCorner>567439.328 5937644.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>hoehenangabe</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <position>
        <gml:Point gml:id="Gml_C8454BCA-EF9B-4C08-83F0-D8B828831091" srsName="EPSG:25832">
          <gml:pos>567439.328 5937644.053</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_NutzungsartenGrenze gml:id="GML_5276e223-57f7-4596-9c9c-8fb909e11db2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_48BA478D-E7E1-48D1-8E6A-D246CBAC22EF" srsName="EPSG:25832">
          <gml:posList>567416.015 5937672.057 567419.988 5937662.431 567432.605 5937667.621 
567439.105 5937652.001 567428.275 5937631.879 567422.612 5937629.344 
567428.448 5937616.596 567434.138 5937619.1 567436.484 5937623.475 
567442.246 5937634.25 567452.958 5937654.188 567450.566 5937668.248 
567438.775 5937666.233 567433.287 5937679.305 567416.015 5937672.057 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_HoehenMass gml:id="GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.23 5937680.271</gml:lowerCorner>
          <gml:upperCorner>567428.23 5937680.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <text>Straßenhöhe bezogen auf NHN</text>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <h uom="m">7</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Point gml:id="Gml_F6080F5D-0500-4B5F-B157-0186158E51E0" srsName="EPSG:25832">
          <gml:pos>567428.23 5937680.271</gml:pos>
        </gml:Point>
      </position>
      <flaechenschluss>false</flaechenschluss>
    </BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e21981e2-40a7-4dac-84b0-c88d371030af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">23.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <wirdDargestelltDurch xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_B3F7FCDF-6F87-45A5-99AD-24EFF7788896" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567442.758 5937634.17 567432.026 5937639.916 
567427.969 5937632.352 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>5</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_10c25c52-be4f-422d-8171-469001af2bf4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.835 5937628.058</gml:lowerCorner>
          <gml:upperCorner>567426.835 5937628.058</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>hoehenangabe</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <position>
        <gml:Point gml:id="Gml_E7555280-7DC0-4A8E-B814-3236E2AFD615" srsName="EPSG:25832">
          <gml:pos>567426.835 5937628.058</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567433.633 5937633.755</gml:lowerCorner>
          <gml:upperCorner>567433.633 5937633.755</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <position>
        <gml:Point gml:id="Gml_F9CA6335-4F6D-40CA-B6E1-C1D727FF1B97" srsName="EPSG:25832">
          <gml:pos>567433.633 5937633.755</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_354dc6b6-d910-4f29-8aeb-66d238013da6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_7CB2D14F-CA6A-47D9-A67C-11C860B354B6" srsName="EPSG:25832">
          <gml:posList>567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
567437.269 5937671.067 567433.538 5937679.955 567415.353 5937672.325 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_b607f50f-8221-4811-81f0-7f6efef25db3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_FCBC2CB5-66F0-4B51-892A-573558B43905" srsName="EPSG:25832">
          <gml:posList>567428.223 5937615.867 567434.477 5937618.73 567442.758 5937634.17 
567432.026 5937639.916 567427.969 5937632.352 567421.935 5937629.605 
567428.223 5937615.867 </gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_20f74701-9a0b-4c38-89b8-ae81db151e28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_33CA9902-6CAA-46F2-A293-11D9EDB46407" srsName="EPSG:25832">
          <gml:posList>567450.952 5937668.824 567440.924 5937667.111 567442.18 5937659.754 
567452.173 5937661.63 567450.952 5937668.824 </gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BaugebietsTeilFlaeche gml:id="GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <wirdDargestelltDurch xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_EFC13801-F405-450A-B31C-848E91FA20BB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567451.021 5937668.836 567448.434 5937683.72 
567445.762 5937685.099 567443.507 5937686.264 567432.779 5937681.762 
567433.538 5937679.955 567415.353 5937672.325 567418.938 5937663.64 
567419.705 5937661.78 567432.297 5937667.063 567438.475 5937652.392 
567438.319 5937651.648 567428.755 5937633.817 567427.969 5937632.352 
567424.532 5937630.787 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 567442.758 5937634.17 567453.455 5937654.108 
567452.176 5937661.63 567450.952 5937668.824 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <GRZ>0.9</GRZ>
      <besondereArtDerBaulNutzung>1200</besondereArtDerBaulNutzung>
      <bauweise>2000</bauweise>
      <vertikaleDifferenzierung>false</vertikaleDifferenzierung>
    </BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567425.133 5937653.994</gml:lowerCorner>
          <gml:upperCorner>567425.133 5937653.994</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>GRZ</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <position>
        <gml:Point gml:id="Gml_7F7444E5-74B6-4561-9C85-8B1D83109EC6" srsName="EPSG:25832">
          <gml:pos>567425.133 5937653.994</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.301 5937671.953</gml:lowerCorner>
          <gml:upperCorner>567428.301 5937671.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>besondereArtDerBaulNutzung</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <position>
        <gml:Point gml:id="Gml_5F273C68-F749-4424-9683-0677CEC84068" srsName="EPSG:25832">
          <gml:pos>567428.301 5937671.953</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_c34619af-a786-41f0-a899-8a67df7b6750">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.689 5937670.707</gml:lowerCorner>
          <gml:upperCorner>567434.689 5937670.707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>bauweise</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <position>
        <gml:Point gml:id="Gml_9722EAF6-028E-43AE-829F-F0331A90D7D9" srsName="EPSG:25832">
          <gml:pos>567434.689 5937670.707</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">26.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <wirdDargestelltDurch xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_A571EE10-714C-421C-9C87-C3CA833CD481" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567452.173 5937661.63 567450.952 5937668.824 567440.924 5937667.111 
567442.18 5937659.754 567452.173 5937661.63 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>6</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_9036c79b-5e09-4030-8c15-e0d9877d084c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567445.543 5937665.568</gml:lowerCorner>
          <gml:upperCorner>567445.543 5937665.568</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <position>
        <gml:Point gml:id="Gml_1AB2B837-C370-41EC-8F54-7DA25270F155" srsName="EPSG:25832">
          <gml:pos>567445.543 5937665.568</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_fba093ee-e774-4846-b286-e46649619852">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567443.466 5937662.039</gml:lowerCorner>
          <gml:upperCorner>567443.466 5937662.039</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>hoehenangabe</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <position>
        <gml:Point gml:id="Gml_2926E049-0098-47E5-88CD-0C12854D4E08" srsName="EPSG:25832">
          <gml:pos>567443.466 5937662.039</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_A3D068E1-27DA-49B6-B91F-DC410D0AE93F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567416.408 5937613.826 567412.057 5937623.382 567405.121 5937620.224 
567409.472 5937610.668 567416.408 5937613.826 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>1</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_9875e68a-2b21-40e4-8f66-ecf5c2290936">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_E8C3D60F-32BA-4C8E-B55E-BAAB188BB4C4" srsName="EPSG:25832">
          <gml:posList>567439.035 5937655.06 567438.79 5937652.388 567432.026 5937639.916 
567442.758 5937634.17 567453.455 5937654.108 567452.175 5937661.631 
567442.18 5937659.754 567439.277 5937659.277 567439.035 5937655.06 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_7b11be88-979b-483f-8042-82c51eff70af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_E0E8D9E6-C0EE-4384-97A4-F755983CB819" srsName="EPSG:25832">
          <gml:posList>567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
567448.434 5937683.72 567443.507 5937686.264 567432.779 5937681.762 
</gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_VerEntsorgung gml:id="GML_be2c01dd-4d15-4a6a-85c0-dc9565324486">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567431.741 5937675.181</gml:lowerCorner>
          <gml:upperCorner>567432.929 5937679.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <rechtsstand>1000</rechtsstand>
      <ebene>-1</ebene>
      <gehoertNachrichtlichZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <rechtscharakter>5000</rechtscharakter>
      <position>
        <gml:LineString gml:id="Gml_1F09C59A-BC63-42C1-A23E-0AB7861D133D" srsName="EPSG:25832">
          <gml:posList>567432.929 5937675.181 567431.741 5937679.201 </gml:posList>
        </gml:LineString>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <zweckbestimmung>1000</zweckbestimmung>
    </BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_21c90a12-5886-40dd-8a86-463267b6e201">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_F2AB8FB4-AD40-4A21-B58B-E997D57DA98D" srsName="EPSG:25832">
          <gml:posList>567407.329 5937656.657 567419.705 5937661.78 567415.353 5937672.325 
567403 5937667.139 567407.329 5937656.657 </gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UnverbindlicheVormerkung gml:id="GML_cf3318a5-fc25-4f6b-a594-893928f6a3de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.825 5937635.581</gml:lowerCorner>
          <gml:upperCorner>567468.867 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <gehoertNachrichtlichZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <wirdDargestelltDurch xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <rechtscharakter>5000</rechtscharakter>
      <position>
        <gml:Polygon gml:id="Gml_8D755616-6F1A-4DAD-9A68-8ECFAF28945E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567468.867 5937663.607 567465.27 5937684.42 567462.07 5937698.959 
567458.005 5937697.252 567460.825 5937687.181 567456.625 5937685.781 
567452.825 5937636.781 567458.625 5937635.581 567459.825 5937647.581 
567467.425 5937647.381 567468.867 5937663.607 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <vormerkung>vorgesehene Bahnanlage</vormerkung>
    </BP_UnverbindlicheVormerkung>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PTO gml:id="GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567457.938 5937683.621</gml:lowerCorner>
          <gml:upperCorner>567457.938 5937683.621</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>vormerkung</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <schriftinhalt>vorgesehene Bahnanlage</schriftinhalt>
      <fontSperrung>0</fontSperrung>
      <skalierung>1</skalierung>
      <position>
        <gml:Point gml:id="Gml_069B96D9-1DE3-4327-8CEA-4FBE8DEEC6FD" srsName="EPSG:25832">
          <gml:pos>567457.938 5937683.621</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">282.22</drehwinkel>
    </XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_GruenFlaeche gml:id="GML_9a4506b9-78b2-4dc4-bb55-088a4c627872">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567394.606 5937621.126</gml:lowerCorner>
          <gml:upperCorner>567438.475 5937667.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <wirdDargestelltDurch xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_665CA725-3A74-4ED1-AF50-7BD3B06C4BF8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567432.297 5937667.063 567419.705 5937661.78 567405.227 5937655.787 
567401.59 5937657.649 567394.606 5937655.401 567403.316 5937621.126 
567421.935 5937629.605 567424.532 5937630.787 567427.969 5937632.352 
567428.755 5937633.817 567438.319 5937651.648 567438.475 5937652.392 
567432.297 5937667.063 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <zweckbestimmung>1600</zweckbestimmung>
      <nutzungsform>2000</nutzungsform>
      <zugunstenVon>(FHH)</zugunstenVon>
    </BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567409.089 5937633.593</gml:lowerCorner>
          <gml:upperCorner>567409.089 5937633.593</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>zugunstenVon</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <position>
        <gml:Point gml:id="Gml_F9453A0D-7148-4BA2-9AC5-F1A2A74F9A6B" srsName="EPSG:25832">
          <gml:pos>567409.089 5937633.593</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PTO gml:id="GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567406.396 5937643.214</gml:lowerCorner>
          <gml:upperCorner>567406.396 5937643.214</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>zweckbestimmung</art>
      <art>nutzungsform</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <schriftinhalt>Spielplatz</schriftinhalt>
      <fontSperrung>0</fontSperrung>
      <skalierung>1</skalierung>
      <position>
        <gml:Point gml:id="Gml_675DA9C6-DEDA-4296-93D2-21A11907D7E6" srsName="EPSG:25832">
          <gml:pos>567406.396 5937643.214</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
    </XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BauGrenze gml:id="GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_2B62EE0F-8B60-46B3-BEDC-0FFBCDC73ED1" srsName="EPSG:25832">
          <gml:posList>567409.472 5937610.668 567416.408 5937613.826 567412.057 5937623.382 
567405.121 5937620.224 567409.472 5937610.668 </gml:posList>
        </gml:LineString>
      </position>
    </BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BaugebietsTeilFlaeche gml:id="GML_321bdb15-2400-4c44-a553-35f402f8743e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937655.401</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <wirdDargestelltDurch xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_EEAF9A7C-958B-4A3D-88D5-F69F50E020C4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567418.938 5937663.64 567415.353 5937672.325 
567403 5937667.139 567395.687 5937664.07 567393.37 5937660.265 
567394.606 5937655.401 567401.59 5937657.649 567405.227 5937655.787 
567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <GRZ>0.6</GRZ>
      <besondereArtDerBaulNutzung>1200</besondereArtDerBaulNutzung>
      <bauweise>2000</bauweise>
      <vertikaleDifferenzierung>false</vertikaleDifferenzierung>
    </BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_af37976e-6da1-4e93-a376-ad285c900b4c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.836 5937653.516</gml:lowerCorner>
          <gml:upperCorner>567399.836 5937653.516</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>GRZ</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <position>
        <gml:Point gml:id="Gml_5FE24776-5C95-427B-BA68-7C88FD8871D7" srsName="EPSG:25832">
          <gml:pos>567399.836 5937653.516</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.427 5937662.618</gml:lowerCorner>
          <gml:upperCorner>567416.427 5937662.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>bauweise</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <position>
        <gml:Point gml:id="Gml_1C884399-AF4A-4A97-A715-0AECF187D2EC" srsName="EPSG:25832">
          <gml:pos>567416.427 5937662.618</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567410.188 5937663.997</gml:lowerCorner>
          <gml:upperCorner>567410.188 5937663.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>besondereArtDerBaulNutzung</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <position>
        <gml:Point gml:id="Gml_A257104E-34E6-48C4-83B6-0A7E8C8142EF" srsName="EPSG:25832">
          <gml:pos>567410.188 5937663.997</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_StrassenVerkehrsFlaeche gml:id="GML_8376d624-8ccb-473d-81df-fc95b1ef28b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_A3A5F3FE-9D16-435D-99A4-81CEAB272320" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567462.07 5937698.959 567446.882 5937692.583 567390.995 5937669.124 
567386.293 5937667.145 567389.479 5937654.684 567404.151 5937595.479 
567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_4AF47C3A-BC37-4EF2-ACC5-3F0E501ECE11" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9DEDB52D-3562-4133-901B-FE0E1595888C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FBF516BB-CD90-491A-8A3B-6FF30BC539B6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_41AC2498-B3DB-42E9-B4A8-D91A23547C6F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5E4D9A68-07AC-461B-BE4F-E52CC2ACB8BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_269808FB-4B84-4D6E-AAE0-91EF4F551503" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9D044C33-E794-49DC-807C-CD6529D747A4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EC68DBEA-A629-4DC9-B33C-E11BC7AC5790" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CB45E8F9-EF98-49BC-B1E1-57600FE2A5B0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E323A59F-819B-4172-9831-ECF2F1C0B51C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CD6CDE93-5A52-425D-A048-130A24BA9160" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_172CF8E2-00BA-4B36-ABB8-F938055F669D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9D4016A4-F6FA-4A21-821D-0C046B5B6BC7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_096EF15B-1E47-4843-9D42-605DB7B9E2C2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2D779172-FB8D-4E14-A7DE-6297575E488E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1B12C8FD-4F8B-4A36-8514-E53C44F4390E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9F100C2F-D155-43A5-8E6C-F03957F09B86" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5E516871-268D-4996-B94A-894FD2D93EAE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_34EB8582-B6CF-4305-A885-EAD7063FFA54" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FE47D410-DFD6-4623-B8F7-F27D4E3B0CEE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <nutzungsform>2000</nutzungsform>
    </BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <BP_HoehenMass gml:id="GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567453.759 5937669.764</gml:lowerCorner>
          <gml:upperCorner>567453.759 5937669.764</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <text>Straßenhöhe bezogen auf NHN</text>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <h uom="m">6.7</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Point gml:id="Gml_45054493-46D6-4918-B959-E4DB6ACF447A" srsName="EPSG:25832">
          <gml:pos>567453.759 5937669.764</gml:pos>
        </gml:Point>
      </position>
      <flaechenschluss>false</flaechenschluss>
    </BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <BP_BesondererNutzungszweckFlaeche gml:id="GML_9775f239-210a-4d61-ad8f-251f5a31296e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403.316 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567428.223 5937629.605</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <wirdDargestelltDurch xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_FFE3EC99-72C2-4127-9BA5-CD212DFE3B9D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_2060A677-1E76-4979-835A-FB6F34103317" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567407.834857796 5937607.72206805 567410.377 5937607.7 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0FC8CC25-1A66-4C0F-B3E2-CF825C74157B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3115A875-C3F4-48FE-8F9F-BC553E390550" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567421.935 5937629.605 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B495E794-874E-4D82-8EBF-18095FB26815" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567421.935 5937629.605 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CAE6C872-63AB-46BC-B816-BCB248A9887F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567406.223 5937609.688 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>true</flaechenschluss>
      <GRZ>1</GRZ>
      <zweckbestimmung>Kiosk</zweckbestimmung>
    </BP_BesondererNutzungszweckFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567412.297 5937615.306</gml:lowerCorner>
          <gml:upperCorner>567412.297 5937615.306</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>GRZ</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <position>
        <gml:Point gml:id="Gml_F2447F4B-1EDA-46DF-B68E-6B476AEFF755" srsName="EPSG:25832">
          <gml:pos>567412.297 5937615.306</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_f44449a4-6628-4c84-9dfe-d82b4dafca06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.532 5937621.891</gml:lowerCorner>
          <gml:upperCorner>567416.532 5937621.891</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>GRZ</art>
      <art>zweckbestimmung</art>
      <art>rechtscharakter</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <position>
        <gml:Point gml:id="Gml_0AADD3C8-5536-41DC-ACF4-FEF45771CA28" srsName="EPSG:25832">
          <gml:pos>567416.532 5937621.891</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_55177fec-7599-4785-a8e2-806742a9b2d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">22.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <wirdDargestelltDurch xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_5D033015-4E5F-47B4-90B1-0EE88405BEA6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567415.353 5937672.325 567403 5937667.139 
567407.329 5937656.657 567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>5</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_796aa811-807f-4550-abab-24b9cc8aa359">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.862 5937659</gml:lowerCorner>
          <gml:upperCorner>567421.862 5937659</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>hoehenangabe</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <position>
        <gml:Point gml:id="Gml_9E01498D-AD59-48AD-B0C2-FCCFB7ED7185" srsName="EPSG:25832">
          <gml:pos>567421.862 5937659</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bff033ce-687f-48fe-b27c-542b545fb292">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">30</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <wirdDargestelltDurch xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_FE8016DE-C75B-43CE-B1CE-E250B05A30B1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567448.434 5937683.72 567443.507 5937686.264 
567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>6</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_190f5132-7a3c-4e3f-900e-0705faba810a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.986 5937674.081</gml:lowerCorner>
          <gml:upperCorner>567439.986 5937674.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>hoehenangabe</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <position>
        <gml:Point gml:id="Gml_F369174F-54D6-46BA-8E75-5B19A6EF9CD4" srsName="EPSG:25832">
          <gml:pos>567439.986 5937674.081</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.194 5937679.946</gml:lowerCorner>
          <gml:upperCorner>567440.194 5937679.946</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <position>
        <gml:Point gml:id="Gml_E98CD3E3-EDEE-4A13-8373-59819546A301" srsName="EPSG:25832">
          <gml:pos>567440.194 5937679.946</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <BP_StrassenbegrenzungsLinie gml:id="GML_5355860b-25fa-401c-a2ea-bb70654d17ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Curve gml:id="Gml_AAB946E1-EDDC-4AFF-A111-695AF11F4C5B" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </position>
    </BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e1568a2b-1768-4889-aedb-95449c029c25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <ebene>0</ebene>
      <hoehenangabe>
        <XP_Hoehenangabe>
          <hoehenbezug>1000</hoehenbezug>
          <bezugspunkt>6000</bezugspunkt>
          <h uom="m">22.5</h>
        </XP_Hoehenangabe>
      </hoehenangabe>
      <wirdDargestelltDurch xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <rechtscharakter>1000</rechtscharakter>
      <gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:Polygon gml:id="Gml_C7F01CF1-ACF9-4A21-8269-E4A885FC3D23" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567437.993 5937669.342 567437.269 5937671.067 567433.538 5937679.955 
567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </position>
      <flaechenschluss>false</flaechenschluss>
      <Z>4</Z>
    </BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <XP_PPO gml:id="GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.339 5937668.137</gml:lowerCorner>
          <gml:upperCorner>567421.339 5937668.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <art>Z</art>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <dientZurDarstellungVon xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
      <position>
        <gml:Point gml:id="Gml_789630DB-D337-4BFE-B629-EB6E1004B45E" srsName="EPSG:25832">
          <gml:pos>567421.339 5937668.137</gml:pos>
        </gml:Point>
      </position>
      <drehwinkel uom="grad">0</drehwinkel>
      <skalierung>1</skalierung>
    </XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_LPO gml:id="GML_de628759-edb3-4f31-9d43-7d4f816bfb0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.484 5937661.969</gml:lowerCorner>
          <gml:upperCorner>567427.781 5937668.164</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_90AC086F-5BD7-4517-B4DF-6F025E2A3F84" srsName="EPSG:25832">
          <gml:posList>567426.484 5937661.969 567427.781 5937668.164 </gml:posList>
        </gml:LineString>
      </position>
    </XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_LPO gml:id="GML_82075a17-ae02-4228-a4ac-debedfd66c1e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.794 5937658.752</gml:lowerCorner>
          <gml:upperCorner>567417.901 5937660.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_ECFA3027-F78D-4597-A02E-B0B2BBAA80FA" srsName="EPSG:25832">
          <gml:posList>567417.901 5937658.752 567411.794 5937660.897 </gml:posList>
        </gml:LineString>
      </position>
    </XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_LPO gml:id="GML_a435c756-f459-41ce-b27c-d5992aa8f87e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.065 5937655.343</gml:lowerCorner>
          <gml:upperCorner>567437.337 5937661.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_41E291AF-BA3D-4EC7-8CF9-2831EDF1E7A2" srsName="EPSG:25832">
          <gml:posList>567434.065 5937655.343 567437.337 5937661.831 </gml:posList>
        </gml:LineString>
      </position>
    </XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <XP_LPO gml:id="GML_7d7588c9-1211-471e-ad15-8dd9f6605701">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567400.372 5937656.059</gml:lowerCorner>
          <gml:upperCorner>567401.041 5937660.34</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <position>
        <gml:LineString gml:id="Gml_6612D2F3-528B-4D21-B68F-EE7FFC788AF4" srsName="EPSG:25832">
          <gml:posList>567401.041 5937656.059 567400.372 5937660.34 </gml:posList>
        </gml:LineString>
      </position>
    </XP_LPO>
  </gml:featureMember>  
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa">
      <schluessel>§2 Nr.6</schluessel>
      <text>Im Plangebiet ist in den Schlafräumen durch geeignete bauliche
Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden,
verglaste Loggien, Wintergärten, besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
sicherzustellen, dass ein Innenraumpegel bei
gekipptem Fenster von 30 dB(A) während der Nachtzeit
nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme
in Form von verglasten Loggien oder Wintergärten
muss dieser Innenraumpegel bei gekippten/teilgeöffneten
Bauteilen erreicht werden. Wohn-/Schlafräume in
Einzimmerwohnungen und Kinderzimmer sind wie Schlafräume
zu beurteilen.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_0d37aade-f41e-4ec9-b026-79d4709945bf">
      <schluessel>§2 Nr.8</schluessel>
      <text>Im allgemeinen Wohngebiet sind die nicht überbauten Flächen
von Tiefgaragen mit einem mindestens 50 cm starken
durchwurzelbaren Substrataufbau zu versehen und gärtnerisch
anzulegen. Hiervon ausgenommen sind die erforderlichen
Flächen für Terrassen, Wege und Freitreppen, Kinderspielflächen
sowie Bereiche, die der Belichtung, Be- und
Entlüftung oder der Aufnahme von technischen Anlagen
dienen.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc">
      <schluessel>§2 Nr.3</schluessel>
      <text>Im allgemeinen Wohngebiet sind Stellplätze nur in Tiefgaragen
zulässig. Tiefgaragen sind auch außerhalb der überbaubaren
Grundstücksflächen zulässig. Tiefgaragen dürfen
einschließlich ihrer Überdeckung nicht über die natürliche
Geländeoberfläche herausragen.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628">
      <schluessel>§2 Nr.9</schluessel>
      <text>Im allgemeinen Wohngebiet sind die Freiflächen zur öffentlichen
Grünanlage mit einer Hecke abzupflanzen. Für die
Hecke sind standortgerechte einheimische Gehölze zu verwenden.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_2d0243f5-23e4-4f40-8538-d833e9c168dd">
      <schluessel>§2 Nr.7</schluessel>
      <text>Im allgemeinen Wohngebiet sind mindestens 55 vom Hundert
(v. H.) der Dachflächen und auf der Fläche für den
besonderen Nutzungszweck – Kiosk – mindestens 80 v. H.
der Dachflächen mit einem mindestens 10 cm starken
durchwurzelbaren Substrataufbau zu versehen und extensiv
zu begrünen.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_6bac8f46-a325-4392-b115-07bb0e41e8c1">
      <schluessel>§2 Nr.2</schluessel>
      <text>Im allgemeinen Wohngebiet sind Terrassen im Anschluss
an die Hauptnutzung bis zu einer Tiefe von 4 m auch außerhalb
der überbaubaren Grundstücksfläche zulässig. Untergeordnete
Bauteile wie Vordächer, Balkone und Erker im
Bereich von öffentlichen Grünflächen sind unzulässig.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603">
      <schluessel>§2 Nr.1</schluessel>
      <text>Im allgemeinen Wohngebiet sind innerhalb der mit „(A)“
bezeichneten Fläche (Vorhabengebiet) im Rahmen der festgesetzten
Nutzungen nur solche Vorhaben zulässig, zu
deren Durchführung sich der Vorhabenträger im Durchführungsvertrag
verpflichtet.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90">
      <schluessel>§2 Nr.4</schluessel>
      <text>Im allgemeinen Wohngebiet darf die festgesetzte Grundflächenzahl
für Tiefgaragen bis zu einer Grundflächenzahl
von 1,0 überschritten werden.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <XP_TextAbschnitt gml:id="GML_114d1690-3976-42e3-87be-ec97b7ad773d">
      <schluessel>§2 Nr.5</schluessel>
      <text>Im allgemeinen Wohngebiet sind auf den Flurstücken 1477,
1478 und 1480 der Gemarkung Winterhude vor den straßenzugewandten
Fenstern der Wohn- und Schlafräume
lärmgeschützte Außenbereiche durch bauliche Schallschutzmaßnahmen,
wie etwa verglaste Loggien, Wintergärten
oder in ihrer Wirkung vergleichbare Maßnahmen zwingend
vorzusehen. In den lärmgeschützten Außenbereichen
ist bei geöffneten Fenstern/Bauteilen sicherzustellen, dass
ein Tagpegel von weniger als 65 dB(A) erreicht wird.</text>
    </XP_TextAbschnitt>
  </gml:featureMember>
</XPlanAuszug>