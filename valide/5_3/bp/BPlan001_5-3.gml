﻿<?xml version="1.0" encoding="utf-8"?>
<!--
Testplan für Version 5.3, der folgende Neuerungen beinhaltet: 
BP_ZusatzkontingentLaerm ohne richtungssektor
BP_ZentralerVersorgungsbereich
BP_Sichtflaeche (neue Attribute)
BP_BaugebietsTeilflaeche mit Sondernutzung 23000 und 2720 (CR-042)
BP_HoehenMass und BP_AbstandsMass außerhalb des Geltungsbereiches (CR-057)
-->
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_0F4F48F4-AE47-4A3B-B50C-822E016617BA" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/3 http://www.xplanungwiki.de/upload/XPlanGML/5.3/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/3">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>571390.6148 5940791.8555</gml:lowerCorner>
      <gml:upperCorner>571879.6987 5941267.9508</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_DAB2AD4D-E959-40B0-8D25-A8556C9DF4C8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571390.6148 5940791.8555</gml:lowerCorner>
          <gml:upperCorner>571879.6987 5941267.9508</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan001_5-3</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan BPlan001_5-3 für den Geltungsbereich Mützendorpsteed — Trittauer Amtsweg — Im Soll — Reembusch — Ostgrenze des Flurstücks 1875 sowie Nord- und Ostgrenze des Flurstücks 1869 der Gemarkung Bramfeld — Fahrenkrön — Heukoppel (Bezirk Wandsbek, Ortsteil 515) wird festgestellt.</xplan:beschreibung>
      <xplan:technHerstellDatum>2016-02-19</xplan:technHerstellDatum>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_1F784332-8D74-4983-957B-7DDF23C3068A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571738.898 5941007.659 571743.915 5941020.58 571745.007 5941026.838 
571745.295 5941028.497 571748.353 5941074.099 571730.384 5941083.885 
571763.393 5941146.919 571766.3487 5941157.9173 571550.525 5941265.2783 
571545.1525 5941267.9508 571401.0379 5941040.7312 571390.6148 5941024.2975 
571403.8688 5941002.6807 571416.7243 5940978.8399 571429.5702 5940955.0031 
571447.8811 5940925.5254 571456.1255 5940911.6552 571479.797 5940879.9531 
571503.3909 5940856.2837 571519.3766 5940841.6917 571525.2867 5940836.8637 
571531.3581 5940832.3671 571539.5919 5940826.9734 571545.7762 5940823.3733 
571552.5643 5940819.7659 571559.865 5940816.4374 571570.1882 5940812.8682 
571580.6895 5940809.9177 571591.9866 5940807.3061 571603.3717 5940805.3746 
571615.0084 5940804.0672 571753.0188 5940791.8555 571753.7625 5940799.4538 
571757.5895 5940838.5548 571758.8384 5940842.3961 571760.5179 5940845.936 
571762.2556 5940848.686 571764.2505 5940851.2 571766.7355 5940853.7102 
571769.4316 5940855.8647 571772.6171 5940857.844 571775.9802 5940859.4064 
571779.5632 5940860.5542 571783.1854 5940861.2515 571879.6987 5940863.7972 
571879.263 5940868.812 571879.145 5940870.172 571876.324 5940902.636 
571874.385 5940924.949 571864.215 5940924.267 571846.125 5940923.055 
571821.181 5940921.383 571796.236 5940919.711 571771.0654 5940918.0208 
571767.914 5940917.812 571726.694 5940915.049 571718.812 5940914.526 
571752.616 5940996.03 571740.642 5940995.097 571735.09 5940994.665 
571733.815 5940994.566 571738.898 5941007.659 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#Gml_E6D193D1-8259-480B-B95C-06872449D605" />
      <xplan:texte xlink:href="#Gml_103ED703-06EF-4A47-AFC5-C100DC6D87B0" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>515</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>1970-03-02</xplan:rechtsverordnungsDatum>
      <xplan:versionBauNVODatum>1968-01-01</xplan:versionBauNVODatum>
      <xplan:bereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_E6D193D1-8259-480B-B95C-06872449D605">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Außer den im Plan festgesetzten Garagen unter Erdgleiche sind weitere auch auf den nicht überbaubaren Teilen von Baugrundstücken zulässig, wenn Wohnruhe und Gartenanlagen nicht erheblich beeinträchtigt werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="Gml_103ED703-06EF-4A47-AFC5-C100DC6D87B0">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Das festgesetzte Leitungsrecht umfaßt die Befugnis der Freien und Hansestadt Hamburg, unterirdische öffentliche Sielleitungen anzulegen und zu unterhalten. Nutzungen, welche die Unterhaltung beeinträchtigen können, sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571390.6148 5940791.8555</gml:lowerCorner>
          <gml:upperCorner>571879.6987 5941267.9508</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_30914508-4A26-4109-AA11-9F13E7B03C63" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571738.898 5941007.659 571743.915 5941020.58 571745.007 5941026.838 
571745.295 5941028.497 571748.353 5941074.099 571730.384 5941083.885 
571763.393 5941146.919 571766.3487 5941157.9173 571550.525 5941265.2783 
571545.1525 5941267.9508 571401.0379 5941040.7312 571390.6148 5941024.2975 
571403.8688 5941002.6807 571416.7243 5940978.8399 571429.5702 5940955.0031 
571447.8811 5940925.5254 571456.1255 5940911.6552 571479.797 5940879.9531 
571503.3909 5940856.2837 571519.3766 5940841.6917 571525.2867 5940836.8637 
571531.3581 5940832.3671 571539.5919 5940826.9734 571545.7762 5940823.3733 
571552.5643 5940819.7659 571559.865 5940816.4374 571570.1882 5940812.8682 
571580.6895 5940809.9177 571591.9866 5940807.3061 571603.3717 5940805.3746 
571615.0084 5940804.0672 571753.0188 5940791.8555 571753.7625 5940799.4538 
571757.5895 5940838.5548 571758.8384 5940842.3961 571760.5179 5940845.936 
571762.2556 5940848.686 571764.2505 5940851.2 571766.7355 5940853.7102 
571769.4316 5940855.8647 571772.6171 5940857.844 571775.9802 5940859.4064 
571779.5632 5940860.5542 571783.1854 5940861.2515 571879.6987 5940863.7972 
571879.263 5940868.812 571879.145 5940870.172 571876.324 5940902.636 
571874.385 5940924.949 571864.215 5940924.267 571846.125 5940923.055 
571821.181 5940921.383 571796.236 5940919.711 571771.0654 5940918.0208 
571767.914 5940917.812 571726.694 5940915.049 571718.812 5940914.526 
571752.616 5940996.03 571740.642 5940995.097 571735.09 5940994.665 
571733.815 5940994.566 571738.898 5941007.659 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan001_5-3.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan001_5-3.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
      <xplan:planinhalt xlink:href="#Gml_39042CDB-CF6E-47F9-913F-C36D25E6C448" />
      <xplan:planinhalt xlink:href="#Gml_CC9FA4A6-C1D2-4210-B674-FDCC2FD3AB88" />
      <xplan:planinhalt xlink:href="#Gml_2C8AD2C1-360C-418B-8544-1869EA95CC1A" />
      <xplan:planinhalt xlink:href="#Gml_CAB8CECD-74F4-43B3-A7CB-8BB5F4D009FB" />
      <xplan:planinhalt xlink:href="#Gml_DFE9C204-34BA-4150-B097-25177EEEA0D8" />
      <xplan:planinhalt xlink:href="#Gml_EDBFBB8C-1401-411F-AC63-C23343CF63DA" />
      <xplan:planinhalt xlink:href="#Gml_E22E4945-5572-488B-AA4E-AD3CEC98D070" />
      <xplan:planinhalt xlink:href="#Gml_87201A3E-ACB8-4DB0-8649-3B5574E92604" />
      <xplan:planinhalt xlink:href="#Gml_C4742AF5-4507-46EE-A420-8FB725A2F70A" />
      <xplan:planinhalt xlink:href="#Gml_40DC081F-5876-43E0-BD0D-F0BEADAD4CFE" />
      <xplan:planinhalt xlink:href="#Gml_AF269A46-80E3-484F-B475-A6999B3B1E31" />
      <xplan:planinhalt xlink:href="#Gml_8D2771E1-AED7-4C7A-9446-CDDB052C5DF0" />
      <xplan:planinhalt xlink:href="#Gml_6B185E18-5BDB-4EB4-A95C-1538B2A1C740" />
      <xplan:planinhalt xlink:href="#Gml_B9FC6910-7DE3-4225-AC1B-C746C62319E0" />
      <xplan:planinhalt xlink:href="#Gml_990BF2D2-DF61-4EB0-9FB2-020F90510F39" />
      <xplan:planinhalt xlink:href="#Gml_6BFA87B3-9B85-42DA-AA7A-E8BA283900EE" />
      <xplan:planinhalt xlink:href="#Gml_91278177-1AE0-4786-AC68-AE0C2C73853A" />
      <xplan:planinhalt xlink:href="#Gml_595FB811-14AA-40AF-9585-DAE76246A730" />
      <xplan:planinhalt xlink:href="#Gml_20222DB9-0510-4900-BCDC-9A6B7BF85BCC" />
      <xplan:planinhalt xlink:href="#Gml_0CE69A91-8CE5-47A6-BB52-F6C8C9DF3D85" />
      <xplan:planinhalt xlink:href="#Gml_061DA8C1-EEAE-4831-A413-430FFD68B9C4" />
      <xplan:planinhalt xlink:href="#Gml_5B3AC77E-AEF9-48FC-91B8-88151C574231" />
      <xplan:planinhalt xlink:href="#Gml_5E0DD5B7-2B37-4BD2-BB87-5A41C7DC3134" />
      <xplan:planinhalt xlink:href="#Gml_285C54D6-5263-429E-9575-70AD592C2B1E" />
      <xplan:planinhalt xlink:href="#Gml_9BD50710-B77C-4AE7-9005-E7292404D07C" />
      <xplan:planinhalt xlink:href="#Gml_83D9C627-2D86-4351-8BD3-57D520BF6008" />
      <xplan:planinhalt xlink:href="#Gml_5C6F4ACE-43C2-4717-9663-C4EAD9F8B2A4" />
      <xplan:planinhalt xlink:href="#Gml_70F63516-2498-4198-A05B-D545638EE2FF" />
      <xplan:planinhalt xlink:href="#Gml_3EDFD15D-354A-41E6-B0AB-F19CDFC49E8C" />
      <xplan:planinhalt xlink:href="#Gml_972C0F1B-D04A-46F9-8C24-78A7C086F0D5" />
      <xplan:planinhalt xlink:href="#Gml_4C38E51A-20E8-4AB3-88E6-FF2F81EAEABF" />
      <xplan:planinhalt xlink:href="#Gml_A37F8794-500A-4E71-ACF2-DEABA18C138C" />
      <xplan:planinhalt xlink:href="#Gml_FADDA039-62CD-4DAC-B952-C65BE8F49498" />
      <xplan:planinhalt xlink:href="#Gml_15193351-99AB-4BD0-A512-2E93D97E0274" />
      <xplan:planinhalt xlink:href="#Gml_5FED20C0-7AB6-465F-A769-5DAAB226CA18" />
      <xplan:planinhalt xlink:href="#Gml_5BD5646E-C9E1-4B37-B915-719847EBA52B" />
      <xplan:planinhalt xlink:href="#Gml_8F5CD6AC-9D06-4BC1-A102-7A9204BFF0F4" />
      <xplan:planinhalt xlink:href="#Gml_AFD27643-44D1-4839-AC2C-3E4A1258CFC8" />
      <xplan:planinhalt xlink:href="#Gml_B6E1BDC8-B083-4959-9186-D9D18B7C8FBE" />
      <xplan:planinhalt xlink:href="#Gml_491C73BA-5CA4-45CA-976A-EF81966EF142" />
      <xplan:planinhalt xlink:href="#Gml_AA419380-CD51-4806-81DD-4B3A63C15D3C" />
      <xplan:planinhalt xlink:href="#Gml_467EB779-348E-491A-8467-88591931BB91" />
      <xplan:planinhalt xlink:href="#Gml_4C1FBF3B-6844-4344-9595-A96C4A2EF1FE" />
      <xplan:planinhalt xlink:href="#Gml_6E32EE04-FAAD-4EE8-89CE-EF70FDDBEAA2" />
      <xplan:planinhalt xlink:href="#Gml_0F3930FF-4111-4D56-B472-FA874CBDF635" />
      <xplan:planinhalt xlink:href="#Gml_BC82DCAB-5342-42F5-ABC5-14527481C7C0" />
      <xplan:planinhalt xlink:href="#Gml_02910E9F-5EC5-48D4-8416-003A9DFE183B" />
      <xplan:planinhalt xlink:href="#Gml_EB358689-98A2-4405-9D57-36A313410626" />
      <xplan:planinhalt xlink:href="#Gml_AE79B48E-C801-4C8A-86A4-E36A9B806BA4" />
      <xplan:planinhalt xlink:href="#Gml_146F3015-DDB8-4788-BA7D-7F2F46EBA185" />
      <xplan:planinhalt xlink:href="#Gml_756FCD5D-5258-4C49-B206-F58BCEA275A5" />
      <xplan:planinhalt xlink:href="#Gml_03C75832-58D3-4938-A063-5EEBFFF06E2C" />
      <xplan:planinhalt xlink:href="#Gml_C22633CC-849A-43FD-8243-6BB7482CE058" />
      <xplan:planinhalt xlink:href="#Gml_4E28E35C-7825-4906-989F-3ACE10C5F451" />
      <xplan:planinhalt xlink:href="#Gml_BE766E6F-5297-4D9E-863C-F433FF5E9268" />
      <xplan:planinhalt xlink:href="#Gml_2E4341C3-4E8F-41D5-B220-C774FC12EFC9" />
      <xplan:planinhalt xlink:href="#Gml_871A0656-C649-4F35-B980-0F870C36B4D6" />
      <xplan:planinhalt xlink:href="#Gml_B48221D5-C690-497E-8C2F-95CF3A97ECDD" />
      <xplan:planinhalt xlink:href="#Gml_166B3A05-09C3-4287-BFC1-79DC725B7438" />
      <xplan:planinhalt xlink:href="#Gml_29089196-C74C-41C8-B0F1-789CD58EA588" />
      <xplan:planinhalt xlink:href="#Gml_427CD7AE-BFC7-468E-A4EA-003173DB0D5B" />
      <xplan:planinhalt xlink:href="#Gml_AA27010E-4D18-4E8F-B923-6EABABB1998C" />
      <xplan:planinhalt xlink:href="#Gml_FD698672-398B-4D44-BFF9-334C58E22D0D" />
      <xplan:planinhalt xlink:href="#Gml_B8F59B5D-F1BC-4294-BCE8-F07313C5E47E" />
      <xplan:planinhalt xlink:href="#Gml_7EC6E90C-7864-4A64-8B2E-15218E29752C" />
      <xplan:planinhalt xlink:href="#Gml_534139AD-4068-455A-A15B-5F3CDE1B519F" />
      <xplan:planinhalt xlink:href="#Gml_16537152-CD4E-4580-B129-3A11C7C2124B" />
      <xplan:planinhalt xlink:href="#Gml_9E8370DB-25A1-49AE-938F-E91E7E1092AF" />
      <xplan:planinhalt xlink:href="#Gml_889FC623-91B2-4255-BA85-990BC72069E5" />
      <xplan:planinhalt xlink:href="#Gml_5CAD6BF3-412A-41F7-8448-4EF9D2ADCF37" />
      <xplan:planinhalt xlink:href="#Gml_A429D483-D91F-488A-9519-8E04B5C8CDD2" />
      <xplan:planinhalt xlink:href="#Gml_5DFC33B7-DDF5-44C5-A54E-5280ABBDF7EA" />
      <xplan:planinhalt xlink:href="#Gml_068D3EBD-3701-42FC-9CED-C9DB9947B5CE" />
	  <xplan:planinhalt xlink:href="#Gml_951A3969-DDA8-44CC-81C2-0503217742C1" />
	  <xplan:planinhalt xlink:href="#Gml_720B069D-37D3-469D-8993-2FE55DE5899A" />
      <xplan:praesentationsobjekt xlink:href="#Gml_81EC2680-6134-47FA-8E32-A2B168FFD152" />
      <xplan:praesentationsobjekt xlink:href="#Gml_5079A015-277D-4509-86AE-2AAA91783853" />
      <xplan:praesentationsobjekt xlink:href="#Gml_1838059A-0A7A-43B8-B57A-E4A765591EE5" />
      <xplan:praesentationsobjekt xlink:href="#Gml_CA990959-915E-4AE2-81E3-C76167C2EFC0" />
      <xplan:praesentationsobjekt xlink:href="#Gml_7A6BADD5-1A0A-4A31-B036-07919DCB9C39" />
      <xplan:praesentationsobjekt xlink:href="#Gml_749603FD-8A14-401F-9C37-CB1034CABC61" />
      <xplan:praesentationsobjekt xlink:href="#Gml_4C345CFE-0B3E-4F3F-AA0C-926FB6C951FC" />
      <xplan:praesentationsobjekt xlink:href="#Gml_28B73FA7-0487-4958-984A-F979E005EE8E" />
      <xplan:praesentationsobjekt xlink:href="#Gml_FBE6643D-CC01-4138-ABC3-E14E33045745" />
      <xplan:praesentationsobjekt xlink:href="#Gml_B3ECC6AD-2049-4F6A-8BB9-C9084FC12272" />
      <xplan:praesentationsobjekt xlink:href="#Gml_E5C63FCB-D4DE-42CD-85A9-A67A1AC121B6" />
      <xplan:praesentationsobjekt xlink:href="#Gml_0FF5B0A4-3938-4E44-B9AA-F68DDD659E8D" />
      <xplan:praesentationsobjekt xlink:href="#Gml_6F76B7FF-8EBF-43DF-948A-64912D9877D7" />
      <xplan:praesentationsobjekt xlink:href="#Gml_1146156A-5FFD-42A0-85A8-89C6E64E62D4" />
      <xplan:praesentationsobjekt xlink:href="#Gml_FC3473E7-C32E-4723-A88B-93DF11CB9BB5" />
      <xplan:praesentationsobjekt xlink:href="#Gml_24A87720-AF7C-4D01-AA1D-5A8D3D20484E" />
      <xplan:praesentationsobjekt xlink:href="#Gml_F0F359AB-1517-4AE6-912C-D902228120B1" />
      <xplan:praesentationsobjekt xlink:href="#Gml_52E118BF-ABA3-4E8F-9ECB-01F7DA4BD145" />
      <xplan:praesentationsobjekt xlink:href="#Gml_13427370-16F2-4F78-A091-2448130E7D88" />
      <xplan:praesentationsobjekt xlink:href="#Gml_8A91D0EF-8917-4F32-B678-0E66F8AF498D" />
      <xplan:praesentationsobjekt xlink:href="#Gml_7F66421F-D2EC-437C-90BF-944208F53E31" />
      <xplan:praesentationsobjekt xlink:href="#Gml_5A5126FF-6D7A-4266-A6A0-198BC872F1BC" />
      <xplan:praesentationsobjekt xlink:href="#Gml_E325506B-3F26-49E0-AF06-70FB4C104A49" />
      <xplan:praesentationsobjekt xlink:href="#Gml_699F86D5-17D4-4459-9F30-F29BC9D3F8F9" />
      <xplan:praesentationsobjekt xlink:href="#Gml_B93FAD0B-B679-4DF7-9027-685407AE29F0" />
      <xplan:praesentationsobjekt xlink:href="#Gml_67FB0215-73C1-45DD-98D8-DB1926613216" />
      <xplan:praesentationsobjekt xlink:href="#Gml_B1BB52DB-AC9C-4BAF-9927-7AFE0C9E6BB7" />
      <xplan:praesentationsobjekt xlink:href="#Gml_050723E9-BB40-4CA9-BA36-558D001609F2" />
      <xplan:praesentationsobjekt xlink:href="#Gml_206DF956-C4CE-4107-A483-46BD7F82B2DA" />
      <xplan:praesentationsobjekt xlink:href="#Gml_C35CDF6E-7B1F-44D7-8804-EF3E01D2DDDB" />
      <xplan:praesentationsobjekt xlink:href="#Gml_EE0619D5-44B7-405B-B4EC-720C0D08ECB3" />
      <xplan:praesentationsobjekt xlink:href="#Gml_3803F294-A0C0-4749-AD30-69B5D34E6B5A" />
      <xplan:praesentationsobjekt xlink:href="#Gml_251409B3-FAB3-4A6C-95CA-B95852B07C06" />
      <xplan:praesentationsobjekt xlink:href="#Gml_C98EF6A1-899E-4291-9D69-50CF4147DE85" />
      <xplan:praesentationsobjekt xlink:href="#Gml_BB320064-5F3E-48D1-BE46-321A39F57EF9" />
      <xplan:praesentationsobjekt xlink:href="#Gml_3E2B7350-2719-4255-927B-689903DF2A87" />
      <xplan:praesentationsobjekt xlink:href="#Gml_25F47CC3-3680-4E2F-A6F3-4D6E8E47D032" />
      <xplan:praesentationsobjekt xlink:href="#Gml_E2771EBC-585C-4A1C-9E1A-8AF30EA10999" />
      <xplan:praesentationsobjekt xlink:href="#Gml_2AC2077B-874E-4068-9498-1098F7A98586" />
      <xplan:praesentationsobjekt xlink:href="#Gml_C96C2D3A-2070-4A54-B767-EBE82AF8D19C" />
      <xplan:praesentationsobjekt xlink:href="#Gml_A0A6F371-9630-4472-9480-C1AD008C83BA" />
      <xplan:praesentationsobjekt xlink:href="#Gml_ABAD532E-8EB5-4F8F-9F6D-036CC00A2375" />
      <xplan:praesentationsobjekt xlink:href="#Gml_1C57A3FC-2A1A-490D-828C-239DB57CB385" />
      <xplan:praesentationsobjekt xlink:href="#Gml_1F14841A-9F43-4F00-AC47-2D3B652DCB4B" />
      <xplan:praesentationsobjekt xlink:href="#Gml_4FCA9B37-8266-4986-A863-8B5C75C74324" />
      <xplan:praesentationsobjekt xlink:href="#Gml_93920BF0-A245-4BF1-ABBE-5F68DB798B64" />
      <xplan:versionBauNVODatum>1968-01-01</xplan:versionBauNVODatum>
      <xplan:gehoertZuPlan xlink:href="#Gml_DAB2AD4D-E959-40B0-8D25-A8556C9DF4C8" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinbedarfsFlaeche gml:id="Gml_39042CDB-CF6E-47F9-913F-C36D25E6C448">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571522.043 5940948.352</gml:lowerCorner>
          <gml:upperCorner>571730.384 5941162.24</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_F0F359AB-1517-4AE6-912C-D902228120B1" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_52E118BF-ABA3-4E8F-9ECB-01F7DA4BD145" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A6355FDF-7EE2-4DF5-BEB6-49C874A8CBA9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571585.2286 5941157.0724 571577.581 5941161.0847 571575.379 5941162.24 
571575.204 5941161.91 571574.768 5941161.0847 571573.278 5941158.264 
571591.263 5941148.828 571583.4409 5941134.4266 571578.183 5941124.746 
571573.073 5941127.887 571559.9901 5941103.1938 571547.884 5941080.344 
571548.766 5941079.878 571547.9059 5941078.2497 571547.3915 5941077.2759 
571526.9246 5941038.5295 571522.043 5941029.288 571523.2646 5941028.5282 
571525.916 5941026.879 571523.32 5941022.019 571549.2419 5941005.8989 
571580.126 5940986.693 571597.725 5940978.02 571614.827 5940969.592 
571631.3352 5940961.4562 571649.362 5940952.572 571654.025 5940948.352 
571687.0827 5941009.3702 571707.413 5941046.896 571708.366 5941048.4305 
571730.384 5941083.885 571687.7492 5941107.1021 571661.666 5941121.306 
571653.7294 5941125.6279 571650.7661 5941127.2416 571636.2334 5941135.1555 
571633.4194 5941136.6879 571633.0521 5941136.8879 571632.4569 5941137.2121 
571618.9365 5941144.5747 571595.796 5941157.176 571593.41 5941152.78 
571585.2286 5941157.0724 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>12000</xplan:zweckbestimmung>
      <xplan:zugunstenVon>Freie und Hansestadt Hamburg</xplan:zugunstenVon>
    </xplan:BP_GemeinbedarfsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_F0F359AB-1517-4AE6-912C-D902228120B1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571622.2531 5941062.3231</gml:lowerCorner>
          <gml:upperCorner>571622.2531 5941062.3231</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_39042CDB-CF6E-47F9-913F-C36D25E6C448" />
      <xplan:position>
        <gml:Point gml:id="Gml_EEF94493-471A-408A-9AB9-3DBB7742FEA8" srsName="EPSG:25832">
          <gml:pos>571622.2531 5941062.3231</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_52E118BF-ABA3-4E8F-9ECB-01F7DA4BD145">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571622.2531 5941054.3856</gml:lowerCorner>
          <gml:upperCorner>571622.2531 5941054.3856</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_39042CDB-CF6E-47F9-913F-C36D25E6C448" />
      <xplan:position>
        <gml:Point gml:id="Gml_90318CE2-055F-4C50-A4CE-F6CFC01C9229" srsName="EPSG:25832">
          <gml:pos>571622.2531 5941054.3856</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_CC9FA4A6-C1D2-4210-B674-FDCC2FD3AB88">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571512.401 5941083.885</gml:lowerCorner>
          <gml:upperCorner>571763.7092 5941251.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_4FCA9B37-8266-4986-A863-8B5C75C74324" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_93920BF0-A245-4BF1-ABBE-5F68DB798B64" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_DF1E3E26-0353-4BDC-9A8F-38299B249438" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_2D5BB871-546B-4D79-8FAF-C79AF08AB74A" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571763.7092 5941148.0955 571752.656192777 5941154.48418099 571741.342 5941160.398 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_97145ADD-F8F0-45FA-AB57-1E22E47D12B9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571741.342 5941160.398 571696.563 5941182.597 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F28EC81A-59FC-44A7-B12D-FCCED0FAB53A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571696.563 5941182.597 571645.686 5941208.1467 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7EDB89C6-80D6-45CA-B1CB-DB3B1C367367" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571645.686 5941208.1467 571605.7868 5941228.1836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DB8D55DD-0E76-4EA7-90AA-DF9AC8C1212E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571605.7868 5941228.1836 571595.9616 5941233.1177 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_87D489F2-EBDB-4165-BEE0-6CD219131908" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.9616 5941233.1177 571579.7342 5941241.2668 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B10B1B3C-497D-4F06-B6D5-DF2A0A8F4E7C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571579.7342 5941241.2668 571558.451 5941251.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3325C34D-1B0E-48A7-A74A-4E670810B60E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571558.451 5941251.955 571544.709 5941249.393 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7CF0398F-1A6E-4B91-80D2-50FDA10704AD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.709 5941249.393 571539.5452 5941241.2668 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_50ADE0C6-C3B2-4E17-AD64-A8BAE6BF55E4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571539.5452 5941241.2668 571534.3667 5941233.1177 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_484F5FC4-28C1-4C4F-9488-EB5036091DBB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571534.3667 5941233.1177 571519.5071 5941209.7336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B61BE571-948C-4716-8F76-31B537AF1557" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571519.5071 5941209.7336 571512.401 5941198.551 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42A6BAB0-15D3-4CBA-94E2-E7796914497B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571512.401 5941198.551 571512.4258 5941198.466 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2C71FCDD-9C68-44E6-9AAF-4BD4204BD6C4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571512.4258 5941198.466 571575.379 5941162.24 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E8E838C7-A4A7-4345-A37A-DF3199BD4BED" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571575.379 5941162.24 571577.581 5941161.0847 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1C283A93-3BB6-4572-BFCD-619E48AA2794" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571577.581 5941161.0847 571585.2286 5941157.0724 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C6C779AD-01BF-4EEB-945C-5CB1DDA898F6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571585.2286 5941157.0724 571593.41 5941152.78 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_841F0D98-16EF-4345-8747-3D8BA6051BB0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571593.41 5941152.78 571595.796 5941157.176 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_463087E6-A867-4BFA-A8C1-13F28E2267E8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.796 5941157.176 571618.9365 5941144.5747 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D09373A6-083A-4632-AB4B-DBF5F84276E2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571618.9365 5941144.5747 571632.4569 5941137.2121 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D7C98CE0-FCE7-4F08-9766-C52A9AAF9621" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571632.4569 5941137.2121 571633.0521 5941136.8879 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_642BC3E8-D26F-4A8E-9314-B132F42EAFBA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571633.0521 5941136.8879 571633.4194 5941136.6879 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D473251D-1AF9-43E6-AC11-085ED6F81B13" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571633.4194 5941136.6879 571636.2334 5941135.1555 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3759F376-8305-4DB4-9CC1-C794D231BA48" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571636.2334 5941135.1555 571650.7661 5941127.2416 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_15EE694C-9478-4237-BCF0-21E4FF6437EB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571650.7661 5941127.2416 571653.7294 5941125.6279 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5435E780-76AB-4FDD-A508-21F97B52E687" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571653.7294 5941125.6279 571661.666 5941121.306 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E4542055-2404-42A4-95AE-82A334DD2A75" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571661.666 5941121.306 571687.7492 5941107.1021 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_487258B4-F566-4145-A942-6516ABE8613B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571687.7492 5941107.1021 571730.384 5941083.885 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BF6A3DF7-6504-49FE-9696-6E2F91BFEF87" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571730.384 5941083.885 571763.393 5941146.919 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9FCC923E-3010-49C3-94A0-9BC5389085E0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571763.393 5941146.919 571763.7092 5941148.0955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_4FCA9B37-8266-4986-A863-8B5C75C74324">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571602.0388 5941171.0671</gml:lowerCorner>
          <gml:upperCorner>571602.0388 5941171.0671</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_CC9FA4A6-C1D2-4210-B674-FDCC2FD3AB88" />
      <xplan:position>
        <gml:Point gml:id="Gml_8B6F39CF-FE2C-4044-B3B2-82024DB4E0D8" srsName="EPSG:25832">
          <gml:pos>571602.0388 5941171.0671</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_93920BF0-A245-4BF1-ABBE-5F68DB798B64">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571607.7539 5941170.8554</gml:lowerCorner>
          <gml:upperCorner>571607.7539 5941170.8554</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_CC9FA4A6-C1D2-4210-B674-FDCC2FD3AB88" />
      <xplan:position>
        <gml:Point gml:id="Gml_7DD77174-6040-4637-95F3-F13DF7E881D7" srsName="EPSG:25832">
          <gml:pos>571607.7539 5941170.8554</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_2C8AD2C1-360C-418B-8544-1869EA95CC1A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571451.7196 5941029.288</gml:lowerCorner>
          <gml:upperCorner>571591.263 5941170.4713</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_BB320064-5F3E-48D1-BE46-321A39F57EF9" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_3E2B7350-2719-4255-927B-689903DF2A87" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_315B15B8-DE1C-45E9-9E3E-F56FF0488299" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571527.3996 5941152.8541 571497.67 5941168.963 571495.1378 5941170.4713 
571493.274 5941167.692 571474.5422 5941138.466 571453.139 5941105.072 
571451.7196 5941102.3674 571455.468 5941099.937 571514.021 5941063.103 
571518.041 5941031.776 571522.043 5941029.288 571526.9246 5941038.5295 
571547.3915 5941077.2759 571547.9059 5941078.2497 571548.766 5941079.878 
571547.884 5941080.344 571559.9901 5941103.1938 571573.073 5941127.887 
571578.183 5941124.746 571583.4409 5941134.4266 571591.263 5941148.828 
571573.278 5941158.264 571561.11 5941135.239 571560.843 5941134.733 
571527.3996 5941152.8541 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_BB320064-5F3E-48D1-BE46-321A39F57EF9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571506.577 5941114.7636</gml:lowerCorner>
          <gml:upperCorner>571506.577 5941114.7636</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_2C8AD2C1-360C-418B-8544-1869EA95CC1A" />
      <xplan:position>
        <gml:Point gml:id="Gml_5E2238E0-7711-4196-9BD3-9E8B1377DDE4" srsName="EPSG:25832">
          <gml:pos>571506.577 5941114.7636</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_3E2B7350-2719-4255-927B-689903DF2A87">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571512.927 5941114.552</gml:lowerCorner>
          <gml:upperCorner>571512.927 5941114.552</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_2C8AD2C1-360C-418B-8544-1869EA95CC1A" />
      <xplan:position>
        <gml:Point gml:id="Gml_FCFB22C4-665C-431D-9FC6-22A64429FF81" srsName="EPSG:25832">
          <gml:pos>571512.927 5941114.552</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_CAB8CECD-74F4-43B3-A7CB-8BB5F4D009FB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571412.7096 5940925.202</gml:lowerCorner>
          <gml:upperCorner>571561.1056 5941086.8815</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_251409B3-FAB3-4A6C-95CA-B95852B07C06" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_C98EF6A1-899E-4291-9D69-50CF4147DE85" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_0392B421-E370-415C-BB4D-0C1CEE84D7A8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DC0B5CDC-B7BA-4C16-88B8-F7C484B66A5B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571550.505 5940952.898 571554.501 5940964.238 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F6F181E-1599-4704-BFF6-C0B356A61635" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571554.501 5940964.238 571554.843 5940965.208 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_80A38DC7-FBD0-47EF-8A4E-C882CD3293D5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571554.843 5940965.208 571558.256 5940974.902 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6EE724DD-7D45-418A-AC98-7914AD746EC3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571558.256 5940974.902 571560.9534 5940982.557 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A5CF3E2C-67AA-404D-9468-0F87C432412F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571560.9534 5940982.557 571561.1056 5940982.9892 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E02F9006-9816-4D75-A185-B0E604BC3DE7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571561.1056 5940982.9892 571496.838 5941022.596 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_770C1651-A59C-498D-992F-B1CE0155B867" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571496.838 5941022.596 571492.577 5941055.472 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D3E0DC47-92C3-4E26-AA58-528D387CB7E4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571492.577 5941055.472 571442.0736 5941086.8815 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_049A3782-8C88-4FED-A39E-93E9E8C486AC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571442.0736 5941086.8815 571417.7245 5941048.2058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D2BADE1C-4B81-4A19-A720-0E258F50F982" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571417.7245 5941048.2058 571415.802 5941042.715 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_21E9281D-5B1B-4974-85C2-988935443EAB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571415.802 5941042.715 571413.648 5941032.583 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_D27CE8B8-9BA7-4769-8ABF-DCB8D9F03C23" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571413.648 5941032.583 571412.897905876 5941027.59698333 571412.72 5941022.558 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_07AFF9DD-F3D9-421A-89B8-E4E28E52CE4B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571412.72 5941022.558 571413.601009238 5941014.73695479 571415.858 5941007.197 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5CBEBABA-184E-45FB-8BBD-894323AA7061" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571415.858 5941007.197 571418.9449 5940999.5501 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_65B46B13-62C5-4A96-8628-7BAEEDF80C4E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571418.9449 5940999.5501 571421.407 5940993.451 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_689E976D-4305-4F6C-A441-B6F9D96E9160" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571421.407 5940993.451 571456.09 5940928.817 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2209E4C0-DE12-401F-BE21-707B56419D0C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571456.09 5940928.817 571458.0696 5940925.202 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1E7643D2-3F8D-4D4E-B070-849CA7EFD72E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571458.0696 5940925.202 571512.5739 5940945.1781 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C7883C89-0FC2-406F-B084-23BC2CEBF657" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571512.5739 5940945.1781 571543.9461 5940934.2769 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_366DCAC4-4AEF-40D7-951B-4E675DF3D29E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571543.9461 5940934.2769 571544.553 5940936 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_13823662-9EA4-4AE6-BD04-E695388A1686" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.553 5940936 571546.388 5940941.209 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8E312889-3258-4B43-80E4-320B228FE3E2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571546.388 5940941.209 571550.505 5940952.898 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_251409B3-FAB3-4A6C-95CA-B95852B07C06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571466.9952 5940994.5367</gml:lowerCorner>
          <gml:upperCorner>571466.9952 5940994.5367</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_CAB8CECD-74F4-43B3-A7CB-8BB5F4D009FB" />
      <xplan:position>
        <gml:Point gml:id="Gml_96D08348-7407-4FE6-B0F9-D0BA8017FB41" srsName="EPSG:25832">
          <gml:pos>571466.9952 5940994.5367</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_C98EF6A1-899E-4291-9D69-50CF4147DE85">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571473.7686 5940993.9017</gml:lowerCorner>
          <gml:upperCorner>571473.7686 5940993.9017</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_CAB8CECD-74F4-43B3-A7CB-8BB5F4D009FB" />
      <xplan:position>
        <gml:Point gml:id="Gml_9AB978C1-BD41-45A7-AC48-D4357D3A1695" srsName="EPSG:25832">
          <gml:pos>571473.7686 5940993.9017</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_DFE9C204-34BA-4150-B097-25177EEEA0D8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571514.961 5940833.4008</gml:lowerCorner>
          <gml:upperCorner>571597.5319 5940982.9892</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_13427370-16F2-4F78-A091-2448130E7D88" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_7F66421F-D2EC-437C-90BF-944208F53E31" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_E325506B-3F26-49E0-AF06-70FB4C104A49" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_B93FAD0B-B679-4DF7-9027-685407AE29F0" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_B1BB52DB-AC9C-4BAF-9927-7AFE0C9E6BB7" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BDF34248-45F4-406D-A1FE-EE74B547D721" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3352E906-23A1-4243-AB00-71BCB05550F9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571551.944 5940835.8521 571597.5319 5940953.6105 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D4F36D5F-8C94-4CD7-846E-355EB1C694A0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571597.5319 5940953.6105 571595.125 5940960.265 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_09CEE787-9246-4980-B1C5-5F453581FECA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.125 5940960.265 571591.398 5940961.711 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7459D077-045E-46B3-91D2-DC887D98038B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571591.398 5940961.711 571592.7304 5940965.146 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DD3C118F-C315-4619-A8BA-4C5202A69534" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571592.7304 5940965.146 571575.2569 5940973.5186 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BD107ED7-C13E-474B-84A5-5ECBE48AF8E3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571575.2569 5940973.5186 571561.1056 5940982.9892 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3324C76D-C5C1-48BD-99CD-16DA7424D98F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571561.1056 5940982.9892 571560.9534 5940982.557 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F33424AC-A4EE-4BE8-B795-8D74432955E4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571560.9534 5940982.557 571558.256 5940974.902 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4344B5B5-7933-4C8C-A1B8-A30A964CEE0E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571558.256 5940974.902 571554.843 5940965.208 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_05E47EB9-507D-4DD3-B870-0194DBCD65E2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571554.843 5940965.208 571554.501 5940964.238 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5ED55B3E-B52F-4C3B-A6F3-0104D4E77370" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571554.501 5940964.238 571550.505 5940952.898 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B86740E7-E60C-48DD-95DE-869FA958E1E0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571550.505 5940952.898 571546.388 5940941.209 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F5D41D40-A0E2-46BD-BE90-EFD44EF582B2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571546.388 5940941.209 571544.553 5940936 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D5CC2020-D387-4B90-A196-7B01DB5003BB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.553 5940936 571543.9461 5940934.2769 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DFAA233B-04AC-4E94-B838-A92D4648EBE9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571543.9461 5940934.2769 571542.378 5940929.825 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DCB0051E-0940-473A-81F0-9A0E25AE4824" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571542.378 5940929.825 571538.971 5940920.153 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_256CB09C-7C92-4417-B7D6-C7D2B88FEF25" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571538.971 5940920.153 571538.269 5940918.161 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40F77428-E59F-48CE-A7BA-AC302AE7C63A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571538.269 5940918.161 571534.037 5940906.148 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4E3A3181-A08A-4620-A697-AC1809AF9452" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571534.037 5940906.148 571532.782 5940902.583 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_58B7BB47-56F6-493D-97C6-F969B5BF174A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571532.782 5940902.583 571531.534 5940899.04 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0582C759-DECB-4666-97F4-6B9D2AFA0621" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571531.534 5940899.04 571530.193 5940895.441 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0A5E86F3-2518-4596-876E-E57624303B44" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571530.193 5940895.441 571527.5396 5940888.319 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4AA392EE-4C6F-4846-9616-985AA52B1A54" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571527.5396 5940888.319 571526.167 5940884.635 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7914C9B1-A2D4-42F8-ABEB-958D54B3F300" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571526.167 5940884.635 571522.548 5940874.917 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_04A68FC8-35D8-43AA-8BFC-33F6E893BC8F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571522.548 5940874.917 571518.849 5940864.987 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2846A68B-7332-4199-B969-E2CFB131B7CC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571518.849 5940864.987 571514.961 5940854.55 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_52C6B7EF-7346-4333-8049-3D903CABFB41" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571514.961 5940854.55 571523.696046098 5940847.24885667 571532.8995 5940840.5478 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AA821E7D-058C-46D6-9C89-E33EFB7A9697" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571532.8995 5940840.5478 571533.8441 5940839.9119 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_E887F204-5AE7-488E-8600-E4C592502C4D" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571533.8441 5940839.9119 571537.664163823 5940837.43181529 571541.5492 5940835.0548 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_1756B49B-866F-45C0-9440-E59D763B8786" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571541.5492 5940835.0548 571542.324147143 5940834.59754904 571543.1015 5940834.1444 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_AAB564EF-EEF8-41EE-9C94-624B060D164C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571543.1015 5940834.1444 571543.74888407 5940833.7711775 571544.3979 5940833.4008 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42D8292E-6B7A-4C42-93A7-986871F4BE9E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.3979 5940833.4008 571551.944 5940835.8521 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GFZ>1</xplan:GFZ>
      <xplan:GRZ>0.3</xplan:GRZ>
      <xplan:Z>4</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_13427370-16F2-4F78-A091-2448130E7D88">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571555.8954 5940913.0449</gml:lowerCorner>
          <gml:upperCorner>571555.8954 5940913.0449</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_DFE9C204-34BA-4150-B097-25177EEEA0D8" />
      <xplan:position>
        <gml:Point gml:id="Gml_02452041-4D3A-4CCA-B6C8-944524EEE516" srsName="EPSG:25832">
          <gml:pos>571555.8954 5940913.0449</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_7F66421F-D2EC-437C-90BF-944208F53E31">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571562.2454 5940913.0449</gml:lowerCorner>
          <gml:upperCorner>571562.2454 5940913.0449</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_DFE9C204-34BA-4150-B097-25177EEEA0D8" />
      <xplan:position>
        <gml:Point gml:id="Gml_89BD022D-63ED-470C-B398-4FE8F01CAD43" srsName="EPSG:25832">
          <gml:pos>571562.2454 5940913.0449</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_E325506B-3F26-49E0-AF06-70FB4C104A49">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571567.7488 5940913.2566</gml:lowerCorner>
          <gml:upperCorner>571567.7488 5940913.2566</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_DFE9C204-34BA-4150-B097-25177EEEA0D8" />
      <xplan:position>
        <gml:Point gml:id="Gml_111FC17B-3066-4212-A906-1F9638AB68C6" srsName="EPSG:25832">
          <gml:pos>571567.7488 5940913.2566</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_B93FAD0B-B679-4DF7-9027-685407AE29F0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571559.0704 5940908.1765</gml:lowerCorner>
          <gml:upperCorner>571559.0704 5940908.1765</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_DFE9C204-34BA-4150-B097-25177EEEA0D8" />
      <xplan:position>
        <gml:Point gml:id="Gml_7F08D757-FBE7-4BA2-AC9A-9664F045A23A" srsName="EPSG:25832">
          <gml:pos>571559.0704 5940908.1765</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_B1BB52DB-AC9C-4BAF-9927-7AFE0C9E6BB7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571558.8588 5940903.3082</gml:lowerCorner>
          <gml:upperCorner>571558.8588 5940903.3082</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GFZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_DFE9C204-34BA-4150-B097-25177EEEA0D8" />
      <xplan:position>
        <gml:Point gml:id="Gml_8056A064-496C-4834-9B9B-F6CA4025BF55" srsName="EPSG:25832">
          <gml:pos>571558.8588 5940903.3082</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_EDBFBB8C-1401-411F-AC63-C23343CF63DA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571561.5484 5940814.089</gml:lowerCorner>
          <gml:upperCorner>571654.025 5940969.2448</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_8A91D0EF-8917-4F32-B678-0E66F8AF498D" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_5A5126FF-6D7A-4266-A6A0-198BC872F1BC" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_699F86D5-17D4-4459-9F30-F29BC9D3F8F9" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_67FB0215-73C1-45DD-98D8-DB1926613216" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_050723E9-BB40-4CA9-BA36-558D001609F2" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_35CBD2D3-61B7-4BA1-A300-FBEBC5180FFE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B9085271-3A57-4CDE-AD7A-C73DF52E39AB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571603.199 5940827.834 571606.2169 5940834.9877 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1C3C7340-EF57-4F93-B476-1A8820FB6C1B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571606.2169 5940834.9877 571607.24 5940837.413 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8134ECC8-732E-4F37-8722-FB873387EA46" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571607.24 5940837.413 571611.729 5940848.058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A07C33E5-C887-4949-AFC0-34765412A2F2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571611.729 5940848.058 571613.7034 5940852.74 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DB533380-7F39-4095-9C24-EF8F04038AFA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571613.7034 5940852.74 571614.7267 5940855.1667 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A7C64175-8CC2-4303-B418-CC376940F6AB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571614.7267 5940855.1667 571617.289 5940861.243 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_96BA8D0E-2BD3-4B6E-AD11-0BA276AA4D47" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571617.289 5940861.243 571618.004 5940862.939 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9A2BFF06-C2F1-4D27-A728-D56A0FDB7541" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571618.004 5940862.939 571622.606 5940873.853 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_90717585-04EE-40C0-9182-6D2D29828914" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571622.606 5940873.853 571627.698 5940885.928 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_28B49121-05D0-4781-8515-95A9C6685D43" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571627.698 5940885.928 571632.597 5940897.542 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BA5AD6FF-4B43-4AAB-B295-8789EE64E7C5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571632.597 5940897.542 571637.509 5940909.189 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_32A1EDEC-05B8-4998-8472-04D6107FA717" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571637.509 5940909.189 571638.7949 5940912.2374 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ED10AE1D-D6A3-402E-990C-D3FB123B6B37" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571638.7949 5940912.2374 571642.406 5940920.798 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_144DC8DF-F773-4F72-A566-C771B466B3AA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571642.406 5940920.798 571645.4713 5940928.0679 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7B7995AB-0AEF-4B8C-B6BF-AE0D05E8D380" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571645.4713 5940928.0679 571647.338 5940932.495 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3349182E-CDC8-4607-8196-35A30DD3EC59" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571647.338 5940932.495 571652.245 5940944.129 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_968F8FD5-0E44-4D6A-8437-8439FF783E94" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571652.245 5940944.129 571654.025 5940948.352 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0AD61078-06C6-4254-A4A0-A7F84019B7C6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571654.025 5940948.352 571649.362 5940952.572 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2C6F7D84-12CB-4EC5-9115-6490208602FB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571649.362 5940952.572 571631.3352 5940961.4562 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1B073C18-37FC-4057-B6D1-55204B0E2040" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571631.3352 5940961.4562 571615.5314 5940969.2448 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8D09A029-B62C-414C-BD8D-441C3B0DFAC0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571615.5314 5940969.2448 571607.79 5940949.6392 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EEB3500C-B104-4DBA-B1CE-223DE0F14EFD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571607.79 5940949.6392 571562.2021 5940831.8809 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2D9AC0C3-575D-46DF-9EF0-D4CCE0D8029B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571562.2021 5940831.8809 571561.5484 5940830.4016 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F133ED37-654F-4273-8552-70F0638A1100" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571561.5484 5940830.4016 571565.0567 5940823.4561 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E94EB81-78D7-452F-BE6A-F4770CC15A8D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571565.0567 5940823.4561 571565.0977 5940823.4397 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_925AB7F8-C958-41D5-99D2-A860C3C54476" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571565.0977 5940823.4397 571579.348031269 5940818.45720426 571593.9839 5940814.7574 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_1034282D-EA2A-4B7E-9996-88ACB204DC85" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571593.9839 5940814.7574 571595.122376176 5940814.52681125 571596.2624 5940814.304 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D5FF3540-CEB0-4BD6-A70C-099FC5B80046" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571596.2624 5940814.304 571597.404 5940814.089 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B902698C-C28A-44CE-80C7-1441E8FEC09F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571597.404 5940814.089 571603.199 5940827.834 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GFZ>1</xplan:GFZ>
      <xplan:GRZ>0.3</xplan:GRZ>
      <xplan:Z>4</xplan:Z>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_8A91D0EF-8917-4F32-B678-0E66F8AF498D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571600.5572 5940894.2065</gml:lowerCorner>
          <gml:upperCorner>571600.5572 5940894.2065</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_EDBFBB8C-1401-411F-AC63-C23343CF63DA" />
      <xplan:position>
        <gml:Point gml:id="Gml_4D73AA46-986A-4DCB-9E68-4E4D663BEBFB" srsName="EPSG:25832">
          <gml:pos>571600.5572 5940894.2065</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_5A5126FF-6D7A-4266-A6A0-198BC872F1BC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571606.9072 5940893.9948</gml:lowerCorner>
          <gml:upperCorner>571606.9072 5940893.9948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_EDBFBB8C-1401-411F-AC63-C23343CF63DA" />
      <xplan:position>
        <gml:Point gml:id="Gml_DFB5E0AD-5956-4F94-AECF-9720537BB826" srsName="EPSG:25832">
          <gml:pos>571606.9072 5940893.9948</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_699F86D5-17D4-4459-9F30-F29BC9D3F8F9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571612.6222 5940893.9948</gml:lowerCorner>
          <gml:upperCorner>571612.6222 5940893.9948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_EDBFBB8C-1401-411F-AC63-C23343CF63DA" />
      <xplan:position>
        <gml:Point gml:id="Gml_25C43EA4-ACEA-46B9-8540-E5EDDE1BF48F" srsName="EPSG:25832">
          <gml:pos>571612.6222 5940893.9948</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_67FB0215-73C1-45DD-98D8-DB1926613216">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571606.0605 5940888.2798</gml:lowerCorner>
          <gml:upperCorner>571606.0605 5940888.2798</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_EDBFBB8C-1401-411F-AC63-C23343CF63DA" />
      <xplan:position>
        <gml:Point gml:id="Gml_E2D3F3E1-7ED5-4317-B987-63207818C721" srsName="EPSG:25832">
          <gml:pos>571606.0605 5940888.2798</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_050723E9-BB40-4CA9-BA36-558D001609F2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571605.6372 5940883.6232</gml:lowerCorner>
          <gml:upperCorner>571605.6372 5940883.6232</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GFZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_EDBFBB8C-1401-411F-AC63-C23343CF63DA" />
      <xplan:position>
        <gml:Point gml:id="Gml_67A254ED-1749-41C3-AEF6-140ECBA7A56F" srsName="EPSG:25832">
          <gml:pos>571605.6372 5940883.6232</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="Gml_E22E4945-5572-488B-AA4E-AD3CEC98D070">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571458.0696 5940854.55</gml:lowerCorner>
          <gml:upperCorner>571543.9461 5940945.1781</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_206DF956-C4CE-4107-A483-46BD7F82B2DA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_47D4E15A-5D79-4099-A6B1-774564B16A6E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571518.849 5940864.987 571522.548 5940874.917 571526.167 5940884.635 
571527.5396 5940888.319 571530.193 5940895.441 571531.534 5940899.04 
571532.782 5940902.583 571534.037 5940906.148 571538.269 5940918.161 
571538.971 5940920.153 571542.378 5940929.825 571543.9461 5940934.2769 
571512.5739 5940945.1781 571458.0696 5940925.202 571475.834 5940892.108 
571476.9431 5940890.9015 571485.634 5940881.448 571498.052 5940869.46 
571514.961 5940854.55 571518.849 5940864.987 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_206DF956-C4CE-4107-A483-46BD7F82B2DA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571503.8253 5940913.4682</gml:lowerCorner>
          <gml:upperCorner>571503.8253 5940913.4682</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_E22E4945-5572-488B-AA4E-AD3CEC98D070" />
      <xplan:position>
        <gml:Point gml:id="Gml_4CB24316-F3DB-45D1-A491-66C3917467CB" srsName="EPSG:25832">
          <gml:pos>571503.8253 5940913.4682</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_87201A3E-ACB8-4DB0-8649-3B5574E92604">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571597.404 5940812.2318</gml:lowerCorner>
          <gml:upperCorner>571752.616 5941083.885</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_FC3473E7-C32E-4723-A88B-93DF11CB9BB5" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_24A87720-AF7C-4D01-AA1D-5A8D3D20484E" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5F525F8A-71B7-4CCA-B918-9DB274EEAB9B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571738.898 5941007.659 571743.915 5941020.58 571745.007 5941026.838 
571745.295 5941028.497 571748.353 5941074.099 571730.384 5941083.885 
571708.366 5941048.4305 571707.413 5941046.896 571687.0827 5941009.3702 
571654.025 5940948.352 571652.245 5940944.129 571647.338 5940932.495 
571645.4713 5940928.0679 571642.406 5940920.798 571638.7949 5940912.2374 
571637.509 5940909.189 571632.597 5940897.542 571627.698 5940885.928 
571622.606 5940873.853 571618.004 5940862.939 571617.289 5940861.243 
571614.7267 5940855.1667 571613.7034 5940852.74 571611.729 5940848.058 
571607.24 5940837.413 571606.2169 5940834.9877 571603.199 5940827.834 
571597.404 5940814.089 571610.1455 5940812.2318 571618.8228 5940833.8121 
571642.5359 5940853.5137 571665.1779 5940876.1455 571677.4546 5940889.6922 
571704.3364 5940921.654 571741.4932 5940969.2121 571752.616 5940996.03 
571740.642 5940995.097 571735.09 5940994.665 571733.815 5940994.566 
571738.898 5941007.659 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>Freie und Hansestadt Hamburg</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_FC3473E7-C32E-4723-A88B-93DF11CB9BB5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571707.7136 5940987.975</gml:lowerCorner>
          <gml:upperCorner>571707.7136 5940987.975</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_87201A3E-ACB8-4DB0-8649-3B5574E92604" />
      <xplan:position>
        <gml:Point gml:id="Gml_B39DC36C-BA20-4DC1-921A-0EB0270A80AA" srsName="EPSG:25832">
          <gml:pos>571707.7136 5940987.975</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_24A87720-AF7C-4D01-AA1D-5A8D3D20484E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571707.1845 5940979.7729</gml:lowerCorner>
          <gml:upperCorner>571707.1845 5940979.7729</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_87201A3E-ACB8-4DB0-8649-3B5574E92604" />
      <xplan:position>
        <gml:Point gml:id="Gml_640F3F2D-F303-4872-8BDC-4B5B6A62DF2A" srsName="EPSG:25832">
          <gml:pos>571707.1845 5940979.7729</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_C4742AF5-4507-46EE-A420-8FB725A2F70A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571640.8152 5940800.348</gml:lowerCorner>
          <gml:upperCorner>571879.263 5940924.949</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_6F76B7FF-8EBF-43DF-948A-64912D9877D7" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_1146156A-5FFD-42A0-85A8-89C6E64E62D4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_AA617FC7-3441-421F-B835-CAC14C64AD83" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571879.263 5940868.812 571879.145 5940870.172 571876.324 5940902.636 
571874.385 5940924.949 571864.215 5940924.267 571846.125 5940923.055 
571821.181 5940921.383 571796.236 5940919.711 571771.0654 5940918.0208 
571767.914 5940917.812 571726.694 5940915.049 571689.5197 5940870.4834 
571693.3297 5940859.5297 571681.6345 5940846.8123 571672.134 5940838.1235 
571640.8152 5940809.4808 571662.36 5940807.568 571714.9348 5940802.9003 
571743.683 5940800.348 571748.64 5940805.165 571751.342 5940834.669 
571752.133 5940840.62 571754.031 5940846.311 571756.874 5940851.589 
571760.687 5940856.22 571765.419 5940859.907 571770.71 5940862.728 
571777.441 5940864.649 571781.662 5940865.166 571784.387 5940865.5 
571785.0873 5940865.526 571871.475 5940868.733 571879.263 5940868.812 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>1000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1100</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_6F76B7FF-8EBF-43DF-948A-64912D9877D7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571729.4095 5940857.5352</gml:lowerCorner>
          <gml:upperCorner>571729.4095 5940857.5352</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_C4742AF5-4507-46EE-A420-8FB725A2F70A" />
      <xplan:position>
        <gml:Point gml:id="Gml_AE44A96A-5FAC-4A4B-919E-85DFA50C49F7" srsName="EPSG:25832">
          <gml:pos>571729.4095 5940857.5352</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_1146156A-5FFD-42A0-85A8-89C6E64E62D4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571734.9658 5940857.006</gml:lowerCorner>
          <gml:upperCorner>571734.9658 5940857.006</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_C4742AF5-4507-46EE-A420-8FB725A2F70A" />
      <xplan:position>
        <gml:Point gml:id="Gml_BD8EAE71-8876-4F34-91A7-1A280083484A" srsName="EPSG:25832">
          <gml:pos>571734.9658 5940857.006</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_40DC081F-5876-43E0-BD0D-F0BEADAD4CFE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571451.7196 5940800.348</gml:lowerCorner>
          <gml:upperCorner>571879.263 5941251.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_733D8C81-E87A-4D7F-A77C-7E7FF8B052DF" srsName="EPSG:25832">
          <gml:segments>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571763.7092 5941148.0955 571752.656192788 5941154.48418101 571741.342 5941160.398 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571741.342 5941160.398 571696.563 5941182.597 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571696.563 5941182.597 571645.686 5941208.1467 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571645.686 5941208.1467 571605.7868 5941228.1836 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571605.7868 5941228.1836 571595.9616 5941233.1177 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571595.9616 5941233.1177 571579.7342 5941241.2668 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571579.7342 5941241.2668 571558.451 5941251.955 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571558.451 5941251.955 571544.709 5941249.393 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571544.709 5941249.393 571539.5452 5941241.2668 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571539.5452 5941241.2668 571534.3667 5941233.1177 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571534.3667 5941233.1177 571519.5071 5941209.7336 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571519.5071 5941209.7336 571512.401 5941198.551 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571512.401 5941198.551 571512.4258 5941198.466 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571512.4258 5941198.466 571575.379 5941162.24 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571575.379 5941162.24 571575.204 5941161.91 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571575.204 5941161.91 571574.768 5941161.0847 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571574.768 5941161.0847 571573.278 5941158.264 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571573.278 5941158.264 571561.11 5941135.239 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571561.11 5941135.239 571560.843 5941134.733 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571560.843 5941134.733 571527.3996 5941152.8541 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571527.3996 5941152.8541 571497.67 5941168.963 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571497.67 5941168.963 571495.1378 5941170.4713 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571495.1378 5941170.4713 571493.274 5941167.692 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571493.274 5941167.692 571474.5422 5941138.466 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571474.5422 5941138.466 571453.139 5941105.072 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571453.139 5941105.072 571451.7196 5941102.3674 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571451.7196 5941102.3674 571455.468 5941099.937 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571455.468 5941099.937 571514.021 5941063.103 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571514.021 5941063.103 571518.041 5941031.776 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571518.041 5941031.776 571522.043 5941029.288 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571522.043 5941029.288 571523.2646 5941028.5282 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571523.2646 5941028.5282 571525.916 5941026.879 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571525.916 5941026.879 571523.32 5941022.019 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571523.32 5941022.019 571549.2419 5941005.8989 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571549.2419 5941005.8989 571580.126 5940986.693 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571580.126 5940986.693 571597.725 5940978.02 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571597.725 5940978.02 571614.827 5940969.592 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571614.827 5940969.592 571615.5314 5940969.2448 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571615.5314 5940969.2448 571607.79 5940949.6392 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571607.79 5940949.6392 571562.2021 5940831.8809 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571562.2021 5940831.8809 571561.5484 5940830.4016 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571561.5484 5940830.4016 571565.0567 5940823.4561 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571565.0567 5940823.4561 571565.0977 5940823.4397 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571565.0977 5940823.4397 571579.348031311 5940818.4572044 571593.9839 5940814.7574 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571593.9839 5940814.7574 571595.122380237 5940814.52683166 571596.2624 5940814.304 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571596.2624 5940814.304 571597.404 5940814.089 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571597.404 5940814.089 571610.1455 5940812.2318 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571610.1455 5940812.2318 571640.8152 5940809.4808 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571640.8152 5940809.4808 571662.36 5940807.568 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571662.36 5940807.568 571714.9348 5940802.9003 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571714.9348 5940802.9003 571743.683 5940800.348 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571743.683 5940800.348 571748.64 5940805.165 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571748.64 5940805.165 571751.342 5940834.669 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571751.342 5940834.669 571752.133 5940840.62 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571752.133 5940840.62 571754.031 5940846.311 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571754.031 5940846.311 571756.874 5940851.589 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571756.874 5940851.589 571760.687 5940856.22 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571760.687 5940856.22 571765.419 5940859.907 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571765.419 5940859.907 571770.71 5940862.728 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571770.71 5940862.728 571777.441 5940864.649 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571777.441 5940864.649 571781.662 5940865.166 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571781.662 5940865.166 571784.387 5940865.5 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571784.387 5940865.5 571785.0873 5940865.526 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571785.0873 5940865.526 571871.475 5940868.733 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571871.475 5940868.733 571879.263 5940868.812 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_AF269A46-80E3-484F-B475-A6999B3B1E31">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571412.7096 5940833.4008</gml:lowerCorner>
          <gml:upperCorner>571597.5319 5941086.8815</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_A369863C-1CE4-4C69-8DB1-07504C359DA2" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571458.0696 5940925.202 571475.834 5940892.108 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571475.834 5940892.108 571476.9431 5940890.9015 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571476.9431 5940890.9015 571485.634 5940881.448 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571485.634 5940881.448 571498.052 5940869.46 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571498.052 5940869.46 571514.961 5940854.55 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571514.961 5940854.55 571523.696046121 5940847.2488567 571532.8995 5940840.5478 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571532.8995 5940840.5478 571533.8441 5940839.9119 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571533.8441 5940839.9119 571537.66416383 5940837.4318153 571541.5492 5940835.0548 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571541.5492 5940835.0548 571542.324153613 5940834.59756007 571543.1015 5940834.1444 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571543.1015 5940834.1444 571543.748890564 5940833.77118882 571544.3979 5940833.4008 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571544.3979 5940833.4008 571551.944 5940835.8521 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571551.944 5940835.8521 571597.5319 5940953.6105 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571597.5319 5940953.6105 571595.125 5940960.265 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571595.125 5940960.265 571591.398 5940961.711 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571591.398 5940961.711 571592.7304 5940965.146 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571592.7304 5940965.146 571575.2569 5940973.5186 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571575.2569 5940973.5186 571561.1056 5940982.9892 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571561.1056 5940982.9892 571496.838 5941022.596 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571496.838 5941022.596 571492.577 5941055.472 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571492.577 5941055.472 571442.0736 5941086.8815 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571442.0736 5941086.8815 571417.7245 5941048.2058 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571417.7245 5941048.2058 571415.802 5941042.715 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571415.802 5941042.715 571413.648 5941032.583 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571413.648 5941032.583 571412.897905634 5941027.59698335 571412.72 5941022.558 
</gml:posList>
            </gml:ArcString>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">571412.72 5941022.558 571413.601008887 5941014.73695472 571415.858 5941007.197 
</gml:posList>
            </gml:ArcString>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571415.858 5941007.197 571418.9449 5940999.5501 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571418.9449 5940999.5501 571421.407 5940993.451 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571421.407 5940993.451 571456.09 5940928.817 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">571456.09 5940928.817 571458.0696 5940925.202 </gml:posList>
            </gml:LineStringSegment>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_8D2771E1-AED7-4C7A-9446-CDDB052C5DF0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571458.0696 5940854.55</gml:lowerCorner>
          <gml:upperCorner>571543.9461 5940945.1781</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_78674656-3703-47F3-984D-99A40E16C044" srsName="EPSG:25832">
          <gml:posList>571458.0696 5940925.202 571485.3373 5940935.1958 571497.097 5940939.5057 
571498.128 5940939.8836 571512.5739 5940945.1781 571543.9461 5940934.2769 
571542.378 5940929.825 571538.971 5940920.153 571538.269 5940918.161 
571534.037 5940906.148 571532.782 5940902.583 571531.534 5940899.04 
571530.193 5940895.441 571527.5396 5940888.319 571526.167 5940884.635 
571522.548 5940874.917 571518.849 5940864.987 571514.961 5940854.55 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_6B185E18-5BDB-4EB4-A95C-1538B2A1C740">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571543.9461 5940934.2769</gml:lowerCorner>
          <gml:upperCorner>571561.1056 5940982.9892</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_71269470-6F76-4C25-BEBD-D1BE1A773D1A" srsName="EPSG:25832">
          <gml:posList>571561.1056 5940982.9892 571543.9461 5940934.2769 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="Gml_B9FC6910-7DE3-4225-AC1B-C746C62319E0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571390.6148 5940805.7176</gml:lowerCorner>
          <gml:upperCorner>571766.3487 5941267.9508</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C48540B5-19D4-46C0-B6B9-B6F7F60D498A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B33C3C3D-8AC0-4F14-8B7F-5D22BE1F8428" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571763.7092 5941148.0955 571766.3487 5941157.9173 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8E7EFB62-E54F-4DDC-A9E6-2F0B067ACCED" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571766.3487 5941157.9173 571550.525 5941265.2783 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1FB886FA-77C7-4B3B-90AD-043CECC9ED21" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571550.525 5941265.2783 571545.1525 5941267.9508 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_83059644-169E-4E04-8362-39E5C58BFDEB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571545.1525 5941267.9508 571401.0379 5941040.7312 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4FA661ED-6C62-42F3-95E2-8F6649EDF936" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571401.0379 5941040.7312 571390.6148 5941024.2975 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_83A53396-1420-4653-A9EC-5F5FAC19C2C4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571390.6148 5941024.2975 571403.8688 5941002.6807 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A6AC00B2-D0A8-41A4-834C-AEA04D40E5F5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571403.8688 5941002.6807 571416.7243 5940978.8399 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7DA7BEBC-66C3-4B44-BADD-2EE9A7FB0276" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571416.7243 5940978.8399 571429.5702 5940955.0031 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6410DFD5-0A02-4116-95D5-1014F455B3F4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571429.5702 5940955.0031 571447.8811 5940925.5254 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4A804598-8A14-4CEE-8C45-BF097D098D82" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571447.8811 5940925.5254 571456.1255 5940911.6552 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9D6190B2-CC11-42A6-B3BE-681223B45128" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571456.1255 5940911.6552 571479.797 5940879.9531 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F45C5485-E615-4BA7-A6D1-927529C7981B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571479.797 5940879.9531 571503.3909 5940856.2837 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_805EF35F-FAA6-46FB-B6AF-A4138F5D8A9E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571503.3909 5940856.2837 571519.3766 5940841.6917 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9A56FB27-7946-47FA-BDBF-A6F290C8EF31" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571519.3766 5940841.6917 571525.2867 5940836.8637 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3D96DA08-A6C6-4378-88E4-B1B82F0C3525" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571525.2867 5940836.8637 571531.3581 5940832.3671 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2FFC6EF9-88D2-4533-BDC5-11E88107C2C4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571531.3581 5940832.3671 571539.5919 5940826.9734 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_37F9BA59-152E-4273-9C21-0B8A0B420F6A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571539.5919 5940826.9734 571545.7762 5940823.3733 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7F82A4E8-417C-4D1F-BD85-5AECE83E4AF9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571545.7762 5940823.3733 571552.5643 5940819.7659 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_32B38AE7-B7E8-49FF-9BB6-32938F4FAC4F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571552.5643 5940819.7659 571559.865 5940816.4374 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_80DDEDE7-E3E1-4179-A4AB-9B9A7BDEE996" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571559.865 5940816.4374 571570.1882 5940812.8682 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1AC66A81-379D-4252-A9E2-5CC6090A9C3C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571570.1882 5940812.8682 571580.6895 5940809.9177 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B740C06D-A07F-46E9-992C-6CE51B28ECBE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571580.6895 5940809.9177 571591.9866 5940807.3061 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BB2C95A9-57E2-4608-AAEF-521AB2B761FF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571591.9866 5940807.3061 571601.3501 5940805.7176 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_07879F9A-B483-4EC2-8DCB-EEAC58F62DD5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571601.3501 5940805.7176 571610.1455 5940812.2318 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ED67DC58-8AB1-44EE-8464-6C643A69CAE1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571610.1455 5940812.2318 571597.404 5940814.089 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_20E7A53E-A97D-44F3-8B25-BA356126DFB4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571597.404 5940814.089 571596.2624 5940814.304 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_D03F4D27-26E9-4E03-9067-FC1B8DEC288E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571596.2624 5940814.304 571595.122376176 5940814.52681125 571593.9839 5940814.7574 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_704A3FD9-2995-4F87-892F-686DF0D9AF2C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571593.9839 5940814.7574 571579.348031269 5940818.45720426 571565.0977 5940823.4397 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CBFF6172-B688-44BC-998A-F5B79BDBD330" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571565.0977 5940823.4397 571565.0567 5940823.4561 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_31F0F402-C3A7-447D-94C4-A6C3F3FAD4EA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571565.0567 5940823.4561 571561.5484 5940830.4016 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0A621009-252A-40E0-AFED-ECAD78D97A4B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571561.5484 5940830.4016 571562.2021 5940831.8809 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2DC0EA28-1964-4A7D-9BD5-1A0F54EA18E9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571562.2021 5940831.8809 571607.79 5940949.6392 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_947DF9C0-2CB6-4377-A6A2-35F39B08E54E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571607.79 5940949.6392 571615.5314 5940969.2448 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E42F2A1-2E44-4DAF-A5DB-ACDEC8630B16" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571615.5314 5940969.2448 571614.827 5940969.592 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A47D08A5-9493-4BC1-B62B-8F669FACEDD4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571614.827 5940969.592 571597.725 5940978.02 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C7232947-0738-4FA1-85C8-EA08AE5FEB10" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571597.725 5940978.02 571580.126 5940986.693 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3E83E026-F2A7-4C1F-8105-60DDB59492C0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571580.126 5940986.693 571549.2419 5941005.8989 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8EF757C2-6CDE-43C3-87FD-FE6A946FFB29" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571549.2419 5941005.8989 571523.32 5941022.019 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2B6B50FD-E894-4872-98FD-912CEC77FF15" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571523.32 5941022.019 571525.916 5941026.879 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1EFD8C68-9927-44DE-9450-AFA0CDB7DEC5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571525.916 5941026.879 571523.2646 5941028.5282 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6A11A29E-F789-4CA5-A059-650004676E38" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571523.2646 5941028.5282 571522.043 5941029.288 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8A150A63-60A7-4131-8220-04673AAE062D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571522.043 5941029.288 571518.041 5941031.776 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C96D733F-5EA1-4E74-9337-ADCCC89F7117" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571518.041 5941031.776 571514.021 5941063.103 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40C40630-9583-478E-B8CF-ED328A91832C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571514.021 5941063.103 571455.468 5941099.937 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F0E4088A-0B25-48DE-9592-5BA738DD4609" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571455.468 5941099.937 571451.7196 5941102.3674 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4CAF0620-C643-441C-A206-E7F9CC51FA04" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571451.7196 5941102.3674 571453.139 5941105.072 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_112FE767-50D5-4D92-9469-D687926B516D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571453.139 5941105.072 571474.5422 5941138.466 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_829B9591-28D3-48D6-A5E1-D44DB7E096EC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571474.5422 5941138.466 571493.274 5941167.692 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9D54587A-5B1C-461C-9545-D87411664E61" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571493.274 5941167.692 571495.1378 5941170.4713 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F2C3327A-C3DD-4372-93B0-03B3521814B0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571495.1378 5941170.4713 571497.67 5941168.963 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_326A2907-09B8-4670-A8CB-E5EF0CD634D1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571497.67 5941168.963 571527.3996 5941152.8541 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2EBFB86B-A175-4B67-9D41-D8AC9D10DCAA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571527.3996 5941152.8541 571560.843 5941134.733 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_87F9B6A0-AA7A-4583-8EE6-F35A17F044B7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571560.843 5941134.733 571561.11 5941135.239 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_046E4291-4949-4321-80AA-5BC56FDFD9BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571561.11 5941135.239 571573.278 5941158.264 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1B0DFCC5-1917-4D85-91ED-504D201E16D0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571573.278 5941158.264 571574.768 5941161.0847 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C5D5DE4C-D9D1-443A-B9BA-701889361B88" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571574.768 5941161.0847 571575.204 5941161.91 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5A91BF6D-1AA5-4DF0-94E3-95C23B1F006E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571575.204 5941161.91 571575.379 5941162.24 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E12407A2-8AD5-49FB-A7E9-22EB4244EF76" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571575.379 5941162.24 571512.4258 5941198.466 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8BB485F1-DC70-452F-AFA9-4DB0143B6E13" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571512.4258 5941198.466 571512.401 5941198.551 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5481C5A7-DACF-4D7D-A836-D481E5B5338A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571512.401 5941198.551 571519.5071 5941209.7336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42D60BB2-78A0-4A79-9FAE-C2FE634B0EB2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571519.5071 5941209.7336 571534.3667 5941233.1177 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_873E0498-235F-4219-93DA-62CC3F98F28B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571534.3667 5941233.1177 571539.5452 5941241.2668 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B101F379-333B-4AA7-8D8B-D44F07499AEE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571539.5452 5941241.2668 571544.709 5941249.393 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7CF39011-48FD-4B76-B705-381744784F55" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.709 5941249.393 571558.451 5941251.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_68736E41-338C-4E64-BBB9-4FF80178B2BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571558.451 5941251.955 571579.7342 5941241.2668 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_759356A9-EB66-4F4A-96E8-C9EF21F26951" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571579.7342 5941241.2668 571595.9616 5941233.1177 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F69695D8-A014-4DEA-8345-76614C6DB7F6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.9616 5941233.1177 571605.7868 5941228.1836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_54B756BE-2A55-4115-866D-F6A351166A5D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571605.7868 5941228.1836 571645.686 5941208.1467 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_61E36E54-3405-4CED-BAA5-B64832B7055E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571645.686 5941208.1467 571696.563 5941182.597 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B26605A2-72D8-4811-9332-E37D9A57FDD9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571696.563 5941182.597 571741.342 5941160.398 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F02E2326-3F06-4570-9687-1A49495DD9C5" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571741.342 5941160.398 571752.656192776 5941154.48418099 571763.7092 5941148.0955 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0475DF53-73EC-4782-A127-0236EF24C731" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571458.0696 5940925.202 571456.09 5940928.817 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B2E3798E-3827-44CE-86CD-F1EF950AFFA5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571456.09 5940928.817 571421.407 5940993.451 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_861D26CE-484E-4D5C-AC96-31052E389E3C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571421.407 5940993.451 571418.9449 5940999.5501 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8CCC5F6F-0C84-473B-BFBF-EA9FE395AAE7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571418.9449 5940999.5501 571415.858 5941007.197 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_3FE1E84C-3E8D-4028-969D-170741A58604" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571415.858 5941007.197 571413.601009238 5941014.73695479 571412.72 5941022.558 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_8E932914-B564-4A3F-A0CE-EB948D2BBD9D" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571412.72 5941022.558 571412.897905876 5941027.59698333 571413.648 5941032.583 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5B511F61-32BC-4F09-9A53-96C697D446F8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571413.648 5941032.583 571415.802 5941042.715 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BF67899D-091D-4534-87AC-6F250DFFB5BD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571415.802 5941042.715 571417.7245 5941048.2058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B9EF9460-697D-4C95-89D3-A1CF5005C45F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571417.7245 5941048.2058 571442.0736 5941086.8815 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DD00D3A1-2048-4CAF-8C82-9C7C1BA0D5C1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571442.0736 5941086.8815 571492.577 5941055.472 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F7B35547-D10A-4F13-83CB-4918BC319115" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571492.577 5941055.472 571496.838 5941022.596 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_05A9B73F-2154-4399-B23A-E8AFF3AD037B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571496.838 5941022.596 571561.1056 5940982.9892 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_50559DB3-3152-4EFA-B233-43BB0AF88406" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571561.1056 5940982.9892 571575.2569 5940973.5186 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ECC8C7B6-6474-4467-9DA9-5A8FE0E84215" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571575.2569 5940973.5186 571592.7304 5940965.146 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B3188722-2F21-4072-8F4A-4B3A6604E9A9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571592.7304 5940965.146 571591.398 5940961.711 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C1DEE520-FBA0-4E2D-A308-FB8606D4113E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571591.398 5940961.711 571595.125 5940960.265 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_28C1A98C-A1BA-4F3A-96C2-CE2B9D209FD7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.125 5940960.265 571597.5319 5940953.6105 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_64D71F7C-D51E-457A-A728-94AA5C7DD62E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571597.5319 5940953.6105 571551.944 5940835.8521 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6F87F7AD-E2DE-403C-85F3-65DFF2357764" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571551.944 5940835.8521 571544.3979 5940833.4008 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_9370822D-1F6A-4E4A-AA6C-7D62F33CC72D" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571544.3979 5940833.4008 571543.74888407 5940833.7711775 571543.1015 5940834.1444 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_766EAA6A-6EFB-4722-A874-40F94F56802F" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571543.1015 5940834.1444 571542.324147143 5940834.59754904 571541.5492 5940835.0548 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_7A8909A8-4140-4CE5-872E-A30186BC239B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571541.5492 5940835.0548 571537.664163823 5940837.43181529 571533.8441 5940839.9119 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C4A2C4AE-E9C8-4323-BD1E-E29E248EB09D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571533.8441 5940839.9119 571532.8995 5940840.5478 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F5A09106-1917-4237-B19B-28CE3D3658A8" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571532.8995 5940840.5478 571523.696046098 5940847.24885667 571514.961 5940854.55 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BE750B23-3361-4C20-AC30-948E968C3B60" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571514.961 5940854.55 571498.052 5940869.46 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_450536C4-939B-4A05-A3A4-CDEC389A1B34" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571498.052 5940869.46 571485.634 5940881.448 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BC268A0D-FBFF-4C38-BE94-B3BED9950194" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571485.634 5940881.448 571476.9431 5940890.9015 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0B8BAF2C-AB6B-4A58-9B56-86BFB93B0679" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571476.9431 5940890.9015 571475.834 5940892.108 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DA63C012-1337-4565-B822-148B261D4EA3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571475.834 5940892.108 571458.0696 5940925.202 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="Gml_990BF2D2-DF61-4EB0-9FB2-020F90510F39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571601.3501 5940804.0537</gml:lowerCorner>
          <gml:upperCorner>571623.6549 5940812.2318</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_76E6D63F-8B6B-47AF-8225-210FD9C8A4D4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571623.6549 5940811.02 571610.1455 5940812.2318 571601.3501 5940805.7176 
571603.3717 5940805.3746 571615.0084 5940804.0672 571615.161 5940804.0537 
571623.6549 5940811.02 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="Gml_6BFA87B3-9B85-42DA-AA7A-E8BA283900EE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571615.161 5940791.8555</gml:lowerCorner>
          <gml:upperCorner>571879.6987 5940868.812</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_AC27660B-777D-49DC-9038-03E1021C03A9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571757.5895 5940838.5548 571758.8384 5940842.3961 571760.5179 5940845.936 
571762.2556 5940848.686 571764.2505 5940851.2 571766.7355 5940853.7102 
571769.4316 5940855.8647 571772.6171 5940857.844 571775.9802 5940859.4064 
571779.5632 5940860.5542 571783.1854 5940861.2515 571879.6987 5940863.7972 
571879.263 5940868.812 571871.475 5940868.733 571785.0873 5940865.526 
571784.387 5940865.5 571781.662 5940865.166 571777.441 5940864.649 
571770.71 5940862.728 571765.419 5940859.907 571760.687 5940856.22 
571756.874 5940851.589 571754.031 5940846.311 571752.133 5940840.62 
571751.342 5940834.669 571748.64 5940805.165 571743.683 5940800.348 
571714.9348 5940802.9003 571662.36 5940807.568 571640.8152 5940809.4808 
571623.6549 5940811.02 571615.161 5940804.0537 571753.0188 5940791.8555 
571753.7625 5940799.4538 571757.5895 5940838.5548 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_91278177-1AE0-4786-AC68-AE0C2C73853A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571678.145 5941106.203</gml:lowerCorner>
          <gml:upperCorner>571743.216 5941150.114</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_1F14841A-9F43-4F00-AC47-2D3B652DCB4B" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_1CD605C0-27C2-4019-B15F-AB5D5D724D9F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571743.216 5941116.647 571684.05 5941150.114 571678.145 5941139.676 
571737.306 5941106.203 571743.216 5941116.647 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>6</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_1F14841A-9F43-4F00-AC47-2D3B652DCB4B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571711.6824 5941127.252</gml:lowerCorner>
          <gml:upperCorner>571711.6824 5941127.252</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_91278177-1AE0-4786-AC68-AE0C2C73853A" />
      <xplan:position>
        <gml:Point gml:id="Gml_3E5F23EA-54A3-48D3-A8C0-78B3A284502F" srsName="EPSG:25832">
          <gml:pos>571711.6824 5941127.252</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_595FB811-14AA-40AF-9585-DAE76246A730">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571527.7162 5940956.5878</gml:lowerCorner>
          <gml:upperCorner>571721.3122 5941150.8982</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8E720829-C974-407D-86BC-EF5B5A0C1B7F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571597.6986 5941150.8982 571580.236 5941118.5396 571574.8121 5941121.45 
571527.7162 5941031.4651 571532.3199 5941028.6076 571529.8593 5941023.7657 
571582.2469 5940991.3012 571652.5204 5940956.5878 571721.3122 5941083.1648 
571597.6986 5941150.8982 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_20222DB9-0510-4900-BCDC-9A6B7BF85BCC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571539.5091 5941163.5727</gml:lowerCorner>
          <gml:upperCorner>571592.4598 5941201.3279</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_2AC2077B-874E-4068-9498-1098F7A98586" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6978797A-0DFF-4235-842A-85D2CD937CB4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571592.4598 5941176.801 571546.581 5941201.3279 571539.5091 5941188.0996 
571585.3879 5941163.5727 571592.4598 5941176.801 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>4</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_2AC2077B-874E-4068-9498-1098F7A98586">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571560.9754 5941184.1904</gml:lowerCorner>
          <gml:upperCorner>571560.9754 5941184.1904</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_20222DB9-0510-4900-BCDC-9A6B7BF85BCC" />
      <xplan:position>
        <gml:Point gml:id="Gml_2A9A728E-F7F5-4F6D-984E-1DAA36B2081A" srsName="EPSG:25832">
          <gml:pos>571560.9754 5941184.1904</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_0CE69A91-8CE5-47A6-BB52-F6C8C9DF3D85">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571525.2423 5940839.655</gml:lowerCorner>
          <gml:upperCorner>571591.547 5940966.4965</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6788B3A2-6B48-4C88-A9AA-A793251171CF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571591.547 5940954.5902 571568.052 5940966.4965 571525.2423 5940854.895 
571547.5732 5940839.655 571591.547 5940954.5902 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_061DA8C1-EEAE-4831-A413-430FFD68B9C4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571568.1843 5940821.9279</gml:lowerCorner>
          <gml:upperCorner>571644.446 5940957.9769</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A76A9B22-0139-4860-A402-0F5DCD4B794B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571644.446 5940944.8079 571617.7408 5940957.9769 571568.1843 5940829.7993 
571580.0712 5940825.5155 571592.7112 5940821.9279 571644.446 5940944.8079 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_5B3AC77E-AEF9-48FC-91B8-88151C574231">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571781.972 5940869.088</gml:lowerCorner>
          <gml:upperCorner>571800.9 5940914.371</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_EB38E50B-231F-431C-B67F-272153FEDB30" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571790.259 5940914.371 571781.972 5940912.708 571786.806 5940888.624 
571788.446 5940888.953 571792.432 5940869.088 571800.9 5940870.787 
571797.088 5940889.789 571795.265 5940889.423 571790.259 5940914.371 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>2</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_ZentralerVersorgungsbereich gml:id="Gml_5E0DD5B7-2B37-4BD2-BB87-5A41C7DC3134">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571723.864 5940861.698</gml:lowerCorner>
          <gml:upperCorner>571752.726 5940909.314</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8DB8488D-9A88-43E6-90F8-351FB3222A18" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571733.616 5940909.314 571723.864 5940905.953 571729.036 5940890.946 
571732.57 5940892.164 571743.07 5940861.698 571752.726 5940865.029 
571742.175 5940895.324 571738.877 5940894.186 571733.616 5940909.314 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>      
    </xplan:BP_ZentralerVersorgungsbereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_285C54D6-5263-429E-9575-70AD592C2B1E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571815.043 5940870.504</gml:lowerCorner>
          <gml:upperCorner>571834.126 5940917.667</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_822057EB-20CA-4CCA-9AD9-72CBC8070BB3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571823.715 5940917.667 571815.043 5940916.028 571819.895 5940890.347 
571821.654 5940890.68 571825.465 5940870.504 571834.126 5940872.141 
571830.281 5940892.511 571828.528 5940892.18 571823.715 5940917.667 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>2</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_9BD50710-B77C-4AE7-9005-E7292404D07C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571850.367 5940869.456</gml:lowerCorner>
          <gml:upperCorner>571862.794 5940921.73</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C604EFAD-D3F3-4D1D-9436-1E59797D20C9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571858.906 5940921.73 571850.367 5940921.385 571851.431 5940895.076 
571852.996 5940895.14 571854.036 5940869.456 571862.794 5940869.813 
571861.742 5940895.84 571859.956 5940895.768 571858.906 5940921.73 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>2</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_83D9C627-2D86-4351-8BD3-57D520BF6008">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571532.0521 5941203.4736</gml:lowerCorner>
          <gml:upperCorner>571613.8739 5941248.189</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_C96C2D3A-2070-4A54-B767-EBE82AF8D19C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E375A242-19E1-4AE3-9064-328881136FC4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571552.6565 5941248.189 571532.0521 5941216.1152 571542.0552 5941209.4844 
571555.1688 5941229.8976 571607.2399 5941203.4736 571613.8739 5941216.9504 
571552.6565 5941248.189 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>4</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_C96C2D3A-2070-4A54-B767-EBE82AF8D19C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571580.4488 5941225.2538</gml:lowerCorner>
          <gml:upperCorner>571580.4488 5941225.2538</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_83D9C627-2D86-4351-8BD3-57D520BF6008" />
      <xplan:position>
        <gml:Point gml:id="Gml_815E56A1-21C6-435F-82B7-25F3EED4324F" srsName="EPSG:25832">
          <gml:pos>571580.4488 5941225.2538</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_5C6F4ACE-43C2-4717-9663-C4EAD9F8B2A4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571607.2399 5941190.2569</gml:lowerCorner>
          <gml:upperCorner>571634.1907 5941216.9504</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_A0A6F371-9630-4472-9480-C1AD008C83BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_DBCAC314-B722-4B62-A048-92C8F38B8A9C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571634.1907 5941195.9777 571629.8091 5941208.8189 571613.8739 5941216.9504 
571607.2399 5941203.4736 571614.9642 5941199.5539 571618.1457 5941190.2569 
571634.1907 5941195.9777 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>6</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_A0A6F371-9630-4472-9480-C1AD008C83BA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571620.6655 5941204.2988</gml:lowerCorner>
          <gml:upperCorner>571620.6655 5941204.2988</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_5C6F4ACE-43C2-4717-9663-C4EAD9F8B2A4" />
      <xplan:position>
        <gml:Point gml:id="Gml_FC809B3D-851F-4064-8A19-056D5A394DD1" srsName="EPSG:25832">
          <gml:pos>571620.6655 5941204.2988</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_70F63516-2498-4198-A05B-D545638EE2FF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571618.1457 5941168.9137</gml:lowerCorner>
          <gml:upperCorner>571641.4194 5941195.9777</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_ABAD532E-8EB5-4F8F-9F6D-036CC00A2375" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_4B68321A-69D1-4347-A390-5B5BCDFF1F3C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571641.4194 5941174.7923 571634.1907 5941195.9777 571618.1457 5941190.2569 
571625.4493 5941168.9137 571641.4194 5941174.7923 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>7</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_ABAD532E-8EB5-4F8F-9F6D-036CC00A2375">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571628.4972 5941183.9788</gml:lowerCorner>
          <gml:upperCorner>571628.4972 5941183.9788</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_70F63516-2498-4198-A05B-D545638EE2FF" />
      <xplan:position>
        <gml:Point gml:id="Gml_56134308-9857-41BB-A81F-17B795140EBE" srsName="EPSG:25832">
          <gml:pos>571628.4972 5941183.9788</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_3EDFD15D-354A-41E6-B0AB-F19CDFC49E8C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571625.4493 5941140.2947</gml:lowerCorner>
          <gml:upperCorner>571665.999500001 5941174.7923</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_1C57A3FC-2A1A-490D-828C-239DB57CB385" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_D81CB668-0772-40FF-9A6C-5B5C1A15306F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571665.9995 5941153.6671 571644.9061 5941164.5738 571641.4194 5941174.7923 
571625.4493 5941168.9137 571630.1255 5941155.2487 571659.2032 5941140.2947 
571665.9995 5941153.6671 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>9</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_1C57A3FC-2A1A-490D-828C-239DB57CB385">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571642.4673 5941157.3087</gml:lowerCorner>
          <gml:upperCorner>571642.4673 5941157.3087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_3EDFD15D-354A-41E6-B0AB-F19CDFC49E8C" />
      <xplan:position>
        <gml:Point gml:id="Gml_17DB84E8-A6C3-4F8D-BA03-A5D16D4B0158" srsName="EPSG:25832">
          <gml:pos>571642.4673 5941157.3087</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_972C0F1B-D04A-46F9-8C24-78A7C086F0D5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571522.4306 5941106.9012</gml:lowerCorner>
          <gml:upperCorner>571565.5188 5941139.0482</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_E2771EBC-585C-4A1C-9E1A-8AF30EA10999" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_466000AA-71E0-419E-8D82-C29E4C6DF4A2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571565.5188 5941119.9869 571529.8494 5941139.0482 571522.4306 5941126.0113 
571558.1863 5941106.9012 571565.5188 5941119.9869 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>7</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_E2771EBC-585C-4A1C-9E1A-8AF30EA10999">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571544.4654 5941122.3836</gml:lowerCorner>
          <gml:upperCorner>571544.4654 5941122.3836</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_972C0F1B-D04A-46F9-8C24-78A7C086F0D5" />
      <xplan:position>
        <gml:Point gml:id="Gml_A6C5D31C-03FE-4C36-B777-D49529D98F29" srsName="EPSG:25832">
          <gml:pos>571544.4654 5941122.3836</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_4C38E51A-20E8-4AB3-88E6-FF2F81EAEABF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571460.5019 5941072.4775</gml:lowerCorner>
          <gml:upperCorner>571533.423 5941161.661</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_25F47CC3-3680-4E2F-A6F3-4D6E8E47D032" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F9037217-3E55-4D73-A0B1-7BDE940E5EFF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571497.383 5941161.661 571460.5019 5941103.2764 571509.1479 5941072.4775 
571517.4712 5941084.9615 571481.1863 5941107.9342 571504.0524 5941144.1017 
571526.0139 5941132.3082 571529.8494 5941139.0482 571533.423 5941145.4053 
571504.7404 5941160.8499 571502.8922 5941158.1714 571497.383 5941161.661 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>4</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_25F47CC3-3680-4E2F-A6F3-4D6E8E47D032">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571484.7753 5941128.0986</gml:lowerCorner>
          <gml:upperCorner>571484.7753 5941128.0986</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_4C38E51A-20E8-4AB3-88E6-FF2F81EAEABF" />
      <xplan:position>
        <gml:Point gml:id="Gml_895E0AFD-C856-44FA-A17B-26F6114C5EAC" srsName="EPSG:25832">
          <gml:pos>571484.7753 5941128.0986</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A37F8794-500A-4E71-ACF2-DEABA18C138C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571511.0226 5940975.8049</gml:lowerCorner>
          <gml:upperCorner>571543.7576 5941004.1167</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_EE0619D5-44B7-405B-B4EC-720C0D08ECB3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F4D27CA3-9F5B-4B41-B086-F2681E632BE7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571543.7576 5940988.6172 571518.9785 5941004.1167 571511.0226 5940991.4003 
571535.955 5940975.8049 571543.7576 5940988.6172 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>4</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_EE0619D5-44B7-405B-B4EC-720C0D08ECB3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571525.2037 5940990.9384</gml:lowerCorner>
          <gml:upperCorner>571525.2037 5940990.9384</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_A37F8794-500A-4E71-ACF2-DEABA18C138C" />
      <xplan:position>
        <gml:Point gml:id="Gml_1476AAA6-E840-4D5B-A6B6-737C85730BDE" srsName="EPSG:25832">
          <gml:pos>571525.2037 5940990.9384</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_FADDA039-62CD-4DAC-B952-C65BE8F49498">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571464.7707 5940991.4003</gml:lowerCorner>
          <gml:upperCorner>571518.9785 5941063.4523</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_3803F294-A0C0-4749-AD30-69B5D34E6B5A" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_CB8290BC-8747-4D6B-B71D-3C4B2F3779D2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571518.9785 5941004.1167 571493.7866 5941019.8743 571487.9657 5941054.5877 
571472.9715 5941063.4523 571464.7707 5941050.8753 571474.3261 5941045.2261 
571480.1094 5941010.7367 571511.0226 5940991.4003 571518.9785 5941004.1167 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>6</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_3803F294-A0C0-4749-AD30-69B5D34E6B5A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571485.8336 5941021.8418</gml:lowerCorner>
          <gml:upperCorner>571485.8336 5941021.8418</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_FADDA039-62CD-4DAC-B952-C65BE8F49498" />
      <xplan:position>
        <gml:Point gml:id="Gml_C45AF8F2-5562-425A-B776-24F2738CC55E" srsName="EPSG:25832">
          <gml:pos>571485.8336 5941021.8418</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_15193351-99AB-4BD0-A512-2E93D97E0274">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571421.0289 5940945.364</gml:lowerCorner>
          <gml:upperCorner>571499.2382 5941067.1952</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_C35CDF6E-7B1F-44D7-8804-EF3E01D2DDDB" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BF769FB6-EDCD-43D2-B3A0-A1CFB3BA1563" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571436.5306 5941011.9368 571436.1073 5941035.5377 571450.979 5941058.9468 
571438.4481 5941067.1952 571421.0289 5941039.7761 571421.5995 5941007.9614 
571454.5459 5940948.0097 571479.5751 5940953.197 571491.0211 5940945.364 
571499.2382 5940957.9169 571482.7799 5940969.18 571462.354 5940964.9467 
571436.5306 5941011.9368 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Zzwingend>4</xplan:Zzwingend>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_C35CDF6E-7B1F-44D7-8804-EF3E01D2DDDB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571437.5735 5940992.8434</gml:lowerCorner>
          <gml:upperCorner>571437.5735 5940992.8434</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Zzwingend</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_15193351-99AB-4BD0-A512-2E93D97E0274" />
      <xplan:position>
        <gml:Point gml:id="Gml_AC8ED08C-6FE5-4182-A4F3-417280A443A1" srsName="EPSG:25832">
          <gml:pos>571437.5735 5940992.8434</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_5FED20C0-7AB6-465F-A769-5DAAB226CA18">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571678.145 5941106.203</gml:lowerCorner>
          <gml:upperCorner>571743.216 5941150.114</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B8B07DCB-CE1B-442D-BBD8-AE75CCDE0DE1" srsName="EPSG:25832">
          <gml:posList>571684.05 5941150.114 571743.216 5941116.647 571737.306 5941106.203 
571678.145 5941139.676 571684.05 5941150.114 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_5BD5646E-C9E1-4B37-B915-719847EBA52B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571527.7162 5940956.5878</gml:lowerCorner>
          <gml:upperCorner>571721.3122 5941150.8982</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C288D9E1-8A06-4DFE-BCCD-E91491C7B9B3" srsName="EPSG:25832">
          <gml:posList>571597.6986 5941150.8982 571721.3122 5941083.1648 571652.5204 5940956.5878 
571582.2469 5940991.3012 571529.8593 5941023.7657 571532.3199 5941028.6076 
571527.7162 5941031.4651 571574.8121 5941121.45 571580.236 5941118.5396 
571597.6986 5941150.8982 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_8F5CD6AC-9D06-4BC1-A102-7A9204BFF0F4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571539.5091 5941163.5727</gml:lowerCorner>
          <gml:upperCorner>571592.4598 5941201.3279</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0FEC67C3-1CE6-45FB-BB39-4686979BBD95" srsName="EPSG:25832">
          <gml:posList>571546.581 5941201.3279 571592.4598 5941176.801 571585.3879 5941163.5727 
571539.5091 5941188.0996 571546.581 5941201.3279 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_AFD27643-44D1-4839-AC2C-3E4A1258CFC8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571525.2423 5940839.655</gml:lowerCorner>
          <gml:upperCorner>571591.547 5940966.4965</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_BC24DD5C-1223-4D65-A011-9765EE8B8D74" srsName="EPSG:25832">
          <gml:posList>571568.052 5940966.4965 571591.547 5940954.5902 571547.5732 5940839.655 
571525.2423 5940854.895 571568.052 5940966.4965 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_B6E1BDC8-B083-4959-9186-D9D18B7C8FBE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571568.1843 5940821.9279</gml:lowerCorner>
          <gml:upperCorner>571644.446 5940957.9769</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_87408D3D-0FAA-4FEB-8B8E-054A3B675511" srsName="EPSG:25832">
          <gml:posList>571617.7408 5940957.9769 571644.446 5940944.8079 571592.7112 5940821.9279 
571580.0712 5940825.5155 571568.1843 5940829.7993 571617.7408 5940957.9769 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_491C73BA-5CA4-45CA-976A-EF81966EF142">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571781.972 5940869.088</gml:lowerCorner>
          <gml:upperCorner>571800.9 5940914.371</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_F748F83F-3653-4BE5-B5DE-D7508F2B4514" srsName="EPSG:25832">
          <gml:posList>571781.972 5940912.708 571790.259 5940914.371 571795.265 5940889.423 
571797.088 5940889.789 571800.9 5940870.787 571792.432 5940869.088 
571788.446 5940888.953 571786.806 5940888.624 571781.972 5940912.708 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_AA419380-CD51-4806-81DD-4B3A63C15D3C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571815.043 5940870.504</gml:lowerCorner>
          <gml:upperCorner>571834.126 5940917.667</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0E958271-4217-49A6-8194-8A1EA6D89164" srsName="EPSG:25832">
          <gml:posList>571815.043 5940916.028 571823.715 5940917.667 571828.528 5940892.18 
571830.281 5940892.511 571834.126 5940872.141 571825.465 5940870.504 
571821.654 5940890.68 571819.895 5940890.347 571815.043 5940916.028 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_467EB779-348E-491A-8467-88591931BB91">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571850.367 5940869.456</gml:lowerCorner>
          <gml:upperCorner>571862.794 5940921.73</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_A34D6CEF-AB14-4137-8B5D-485A67C07AD2" srsName="EPSG:25832">
          <gml:posList>571850.367 5940921.385 571858.906 5940921.73 571859.956 5940895.768 
571861.742 5940895.84 571862.794 5940869.813 571854.036 5940869.456 
571852.996 5940895.14 571851.431 5940895.076 571850.367 5940921.385 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_4C1FBF3B-6844-4344-9595-A96C4A2EF1FE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571532.0521 5941203.4736</gml:lowerCorner>
          <gml:upperCorner>571613.8739 5941248.189</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_FE595D6A-F309-486A-9BB3-E2403F8DEB04" srsName="EPSG:25832">
          <gml:posList>571552.6565 5941248.189 571613.8739 5941216.9504 571607.2399 5941203.4736 
571555.1688 5941229.8976 571542.0552 5941209.4844 571532.0521 5941216.1152 
571552.6565 5941248.189 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_6E32EE04-FAAD-4EE8-89CE-EF70FDDBEAA2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571607.2399 5941190.2569</gml:lowerCorner>
          <gml:upperCorner>571634.1907 5941216.9504</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B0BC3588-1D2C-4292-8A05-07B47237C1A1" srsName="EPSG:25832">
          <gml:posList>571634.1907 5941195.9777 571618.1457 5941190.2569 571614.9642 5941199.5539 
571607.2399 5941203.4736 571613.8739 5941216.9504 571629.8091 5941208.8189 
571634.1907 5941195.9777 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_0F3930FF-4111-4D56-B472-FA874CBDF635">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571618.1457 5941168.9137</gml:lowerCorner>
          <gml:upperCorner>571641.4194 5941195.9777</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_CF9EBD23-B568-4A7A-8573-545901DEE051" srsName="EPSG:25832">
          <gml:posList>571641.4194 5941174.7923 571625.4493 5941168.9137 571618.1457 5941190.2569 
571634.1907 5941195.9777 571641.4194 5941174.7923 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_BC82DCAB-5342-42F5-ABC5-14527481C7C0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571625.4493 5941140.2947</gml:lowerCorner>
          <gml:upperCorner>571665.999500001 5941174.7923</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_84D922C9-7B3C-42B9-98F2-4A6C1D31AA6F" srsName="EPSG:25832">
          <gml:posList>571665.9995 5941153.6671 571659.2032 5941140.2947 571630.1255 5941155.2487 
571625.4493 5941168.9137 571641.4194 5941174.7923 571644.9061 5941164.5738 
571665.9995 5941153.6671 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_02910E9F-5EC5-48D4-8416-003A9DFE183B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571522.4306 5941106.9012</gml:lowerCorner>
          <gml:upperCorner>571565.5188 5941139.0482</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_03997EF8-02B9-4BBF-B1E6-029EDDC4FE5F" srsName="EPSG:25832">
          <gml:posList>571529.8494 5941139.0482 571565.5188 5941119.9869 571558.1863 5941106.9012 
571522.4306 5941126.0113 571529.8494 5941139.0482 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_EB358689-98A2-4405-9D57-36A313410626">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571460.5019 5941072.4775</gml:lowerCorner>
          <gml:upperCorner>571533.423 5941161.661</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C0C73DCF-7674-4BA0-B687-E418E465EC96" srsName="EPSG:25832">
          <gml:posList>571533.423 5941145.4053 571529.8494 5941139.0482 571526.0139 5941132.3082 
571504.0524 5941144.1017 571481.1863 5941107.9342 571517.4712 5941084.9615 
571509.1479 5941072.4775 571460.5019 5941103.2764 571497.383 5941161.661 
571502.8922 5941158.1714 571504.7404 5941160.8499 571533.423 5941145.4053 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_AE79B48E-C801-4C8A-86A4-E36A9B806BA4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571511.0226 5940975.8049</gml:lowerCorner>
          <gml:upperCorner>571543.7576 5941004.1167</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_074DE6F4-26B9-4320-8F23-1B1E1C9298E6" srsName="EPSG:25832">
          <gml:posList>571543.7576 5940988.6172 571535.955 5940975.8049 571511.0226 5940991.4003 
571518.9785 5941004.1167 571543.7576 5940988.6172 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_146F3015-DDB8-4788-BA7D-7F2F46EBA185">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571464.7707 5940991.4003</gml:lowerCorner>
          <gml:upperCorner>571518.9785 5941063.4523</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E377008B-C5FE-48A0-B0E5-CCC776880977" srsName="EPSG:25832">
          <gml:posList>571518.9785 5941004.1167 571511.0226 5940991.4003 571480.1094 5941010.7367 
571474.3261 5941045.2261 571464.7707 5941050.8753 571472.9715 5941063.4523 
571487.9657 5941054.5877 571493.7866 5941019.8743 571518.9785 5941004.1167 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_756FCD5D-5258-4C49-B206-F58BCEA275A5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571421.0289 5940945.364</gml:lowerCorner>
          <gml:upperCorner>571499.2382 5941067.1952</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B8005734-2E5C-47E1-8BBE-67C147A13A57" srsName="EPSG:25832">
          <gml:posList>571499.2382 5940957.9169 571491.0211 5940945.364 571479.5751 5940953.197 
571454.5459 5940948.0097 571421.5995 5941007.9614 571421.0289 5941039.7761 
571438.4481 5941067.1952 571450.979 5941058.9468 571436.1073 5941035.5377 
571436.5306 5941011.9368 571462.354 5940964.9467 571482.7799 5940969.18 
571499.2382 5940957.9169 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="Gml_03C75832-58D3-4938-A063-5EEBFFF06E2C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571650.5208 5941145.498</gml:lowerCorner>
          <gml:upperCorner>571737.0889 5941204.866</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_FBE6643D-CC01-4138-ABC3-E14E33045745" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_B3ECC6AD-2049-4F6A-8BB9-C9084FC12272" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_D03851CC-47F5-4587-BA2C-2177A4E9F8AF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571660.522 5941192.7367 571657.4212 5941202.2534 571652.2189 5941204.866 
571656.5533 5941190.6729 571650.5208 5941179.5604 571715.918 5941145.498 
571720.8472 5941156.3828 571737.0889 5941162.5065 571732.0613 5941164.9989 
571722.505 5941160.767 571660.522 5941192.7367 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>3200</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_FBE6643D-CC01-4138-ABC3-E14E33045745">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571673.8469 5941176.6233</gml:lowerCorner>
          <gml:upperCorner>571673.8469 5941176.6233</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_03C75832-58D3-4938-A063-5EEBFFF06E2C" />
      <xplan:position>
        <gml:Point gml:id="Gml_8C90EB35-F9E9-4838-B627-5DB6D96E08EF" srsName="EPSG:25832">
          <gml:pos>571673.8469 5941176.6233</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_B3ECC6AD-2049-4F6A-8BB9-C9084FC12272">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571684.5625 5941172.6546</gml:lowerCorner>
          <gml:upperCorner>571684.5625 5941172.6546</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_03C75832-58D3-4938-A063-5EEBFFF06E2C" />
      <xplan:position>
        <gml:Point gml:id="Gml_FE22905C-56F6-47F6-9B3A-C8BBBB74794D" srsName="EPSG:25832">
          <gml:pos>571684.5625 5941172.6546</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="Gml_C22633CC-849A-43FD-8243-6BB7482CE058">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571515.7417 5941186.4263</gml:lowerCorner>
          <gml:upperCorner>571546.581 5941213.6123</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_4C345CFE-0B3E-4F3F-AA0C-926FB6C951FC" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_02C97AB2-CAB2-497B-94FF-D93FC5114F60" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571525.2668 5941213.6123 571520.835 5941207.2623 571518.7495 5941208.5413 
571516.4666 5941204.9489 571518.7183 5941203.6243 571515.7417 5941198.7956 
571538.6283 5941186.4263 571539.5091 5941188.0996 571546.581 5941201.3279 
571525.2668 5941213.6123 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_4C345CFE-0B3E-4F3F-AA0C-926FB6C951FC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571531.3685 5941200.039</gml:lowerCorner>
          <gml:upperCorner>571531.3685 5941200.039</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_C22633CC-849A-43FD-8243-6BB7482CE058" />
      <xplan:position>
        <gml:Point gml:id="Gml_D6A2A9E6-B7E0-4DAD-A44E-9CFA90D67CCF" srsName="EPSG:25832">
          <gml:pos>571531.3685 5941200.039</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="Gml_4E28E35C-7825-4906-989F-3ACE10C5F451">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571522.8363 5941176.801</gml:lowerCorner>
          <gml:upperCorner>571607.2399 5941229.8976</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_28B73FA7-0487-4958-984A-F979E005EE8E" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_823D8E2C-1089-49FD-AD97-D8B8A331DEFB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571555.1688 5941229.8976 571542.0552 5941209.4844 571536.1036 5941213.4296 
571525.5081 5941219.1772 571522.8363 5941214.9727 571525.2668 5941213.6123 
571546.581 5941201.3279 571592.4598 5941176.801 571607.2399 5941203.4736 
571555.1688 5941229.8976 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3200</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_28B73FA7-0487-4958-984A-F979E005EE8E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571569.8654 5941206.7859</gml:lowerCorner>
          <gml:upperCorner>571569.8654 5941206.7859</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_4E28E35C-7825-4906-989F-3ACE10C5F451" />
      <xplan:position>
        <gml:Point gml:id="Gml_9B40256C-9EAF-4DE2-A484-85C0FB82B4D7" srsName="EPSG:25832">
          <gml:pos>571569.8654 5941206.7859</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="Gml_BE766E6F-5297-4D9E-863C-F433FF5E9268">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571562.8509 5941126.551</gml:lowerCorner>
          <gml:upperCorner>571590.288 5941157.3792</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_749603FD-8A14-401F-9C37-CB1034CABC61" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_AE34453B-5300-4F5F-ADE0-5EAC9C1CE479" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571590.288 5941149.3395 571574.9644 5941157.3792 571566.7535 5941141.8307 
571565.1315 5941142.8487 571563.0541 5941138.9178 571564.703 5941137.9281 
571562.8509 5941134.1578 571578.1306 5941126.551 571590.288 5941149.3395 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_749603FD-8A14-401F-9C37-CB1034CABC61">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571576.2155 5941141.6983</gml:lowerCorner>
          <gml:upperCorner>571576.2155 5941141.6983</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_BE766E6F-5297-4D9E-863C-F433FF5E9268" />
      <xplan:position>
        <gml:Point gml:id="Gml_D89755B6-4974-456A-99B9-241C3672AF49" srsName="EPSG:25832">
          <gml:pos>571576.2155 5941141.6983</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="Gml_2E4341C3-4E8F-41D5-B220-C774FC12EFC9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571507.6587 5941032.2533</gml:lowerCorner>
          <gml:upperCorner>571550.9182 5941101.3891</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_7A6BADD5-1A0A-4A31-B036-07919DCB9C39" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E5CDF20A-55FD-4C45-9640-7D7723F24565" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571548.2864 5941081.1035 571547.1082 5941081.7834 571550.9182 5941089.0859 
571527.82 5941101.3891 571517.4712 5941084.9615 571509.1479 5941072.4775 
571507.6587 5941069.7184 571515.3581 5941064.7971 571518.6919 5941042.4927 
571516.7053 5941042.1851 571517.2405 5941038.0142 571519.4062 5941038.3652 
571520.2 5941033.2852 571522.8987 5941032.2533 571548.2864 5941081.1035 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_7A6BADD5-1A0A-4A31-B036-07919DCB9C39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571528.9872 5941075.0231</gml:lowerCorner>
          <gml:upperCorner>571528.9872 5941075.0231</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_2E4341C3-4E8F-41D5-B220-C774FC12EFC9" />
      <xplan:position>
        <gml:Point gml:id="Gml_DCB3CAFB-EC1D-4D2A-BBF6-63AFB57132F1" srsName="EPSG:25832">
          <gml:pos>571528.9872 5941075.0231</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="Gml_871A0656-C649-4F35-B980-0F870C36B4D6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571433.8244 5941049.9385</gml:lowerCorner>
          <gml:upperCorner>571473.5913 5941083.7524</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_81EC2680-6134-47FA-8E32-A2B168FFD152" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_781612E2-532D-490C-8094-BC279991A9A6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571442.8731 5941083.7524 571439.6188 5941078.9105 571437.7392 5941079.9969 
571435.4264 5941076.3231 571437.3963 5941075.0211 571433.8244 5941069.8617 
571438.4481 5941067.1952 571450.979 5941058.9468 571464.2251 5941049.9385 
571472.9715 5941063.4523 571473.5913 5941064.3054 571442.8731 5941083.7524 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_81EC2680-6134-47FA-8E32-A2B168FFD152">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571452.7871 5941068.2762</gml:lowerCorner>
          <gml:upperCorner>571452.7871 5941068.2762</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_871A0656-C649-4F35-B980-0F870C36B4D6" />
      <xplan:position>
        <gml:Point gml:id="Gml_424FA7CB-5DFD-42AD-87CD-9E4ADAE60FF4" srsName="EPSG:25832">
          <gml:pos>571452.7871 5941068.2762</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="Gml_B48221D5-C690-497E-8C2F-95CF3A97ECDD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571526.6287 5940934.2769</gml:lowerCorner>
          <gml:upperCorner>571559.8673 5940988.6902</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_1838059A-0A7A-43B8-B57A-E4A765591EE5" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_CA990959-915E-4AE2-81E3-C76167C2EFC0" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8F259A37-9194-486A-B1A0-021E1A8EB3C1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571555.2369 5940986.606 571551.8549 5940988.6902 571549.7121 5940985.3272 
571544.5527 5940987.8672 571526.6287 5940940.2943 571543.9461 5940934.2769 
571559.8673 5940979.474 571553.284 5940983.0253 571555.2369 5940986.606 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
      <xplan:zweckbestimmung>3200</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_1838059A-0A7A-43B8-B57A-E4A765591EE5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571545.2591 5940967.4698</gml:lowerCorner>
          <gml:upperCorner>571545.2591 5940967.4698</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_B48221D5-C690-497E-8C2F-95CF3A97ECDD" />
      <xplan:position>
        <gml:Point gml:id="Gml_DFD3F6E2-B083-48B2-B3F4-9688610A705A" srsName="EPSG:25832">
          <gml:pos>571545.2591 5940967.4698</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_CA990959-915E-4AE2-81E3-C76167C2EFC0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571539.7029 5940954.3729</gml:lowerCorner>
          <gml:upperCorner>571539.7029 5940954.3729</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>1</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_B48221D5-C690-497E-8C2F-95CF3A97ECDD" />
      <xplan:position>
        <gml:Point gml:id="Gml_F130BF41-ECEB-4D69-9627-279D6C2F1FC4" srsName="EPSG:25832">
          <gml:pos>571539.7029 5940954.3729</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenFlaeche gml:id="Gml_166B3A05-09C3-4287-BFC1-79DC725B7438">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571512.5739 5940940.2943</gml:lowerCorner>
          <gml:upperCorner>571534.2414 5940964.6896</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_5079A015-277D-4509-86AE-2AAA91783853" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9C48A57C-FBF8-4B91-A48C-096893C688AE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571534.2414 5940960.4995 571521.2958 5940964.6896 571512.5739 5940945.1781 
571526.6287 5940940.2943 571534.2414 5940960.4995 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_GemeinschaftsanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_5079A015-277D-4509-86AE-2AAA91783853">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571525.0185 5940953.5791</gml:lowerCorner>
          <gml:upperCorner>571525.0185 5940953.5791</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_166B3A05-09C3-4287-BFC1-79DC725B7438" />
      <xplan:position>
        <gml:Point gml:id="Gml_C3F36A5C-89FB-4286-8547-1D0A07BCBC35" srsName="EPSG:25832">
          <gml:pos>571525.0185 5940953.5791</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="Gml_29089196-C74C-41C8-B0F1-789CD58EA588">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571749.128 5940862.728</gml:lowerCorner>
          <gml:upperCorner>571781.1553 5940913.4566</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_E5C63FCB-D4DE-42CD-85A9-A67A1AC121B6" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C8462620-20FB-476F-80DA-314C18824DCF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571771.9805 5940913.4566 571749.128 5940911.699 571750.1467 5940900.2132 
571756.4306 5940900.4116 571763.3759 5940864.2298 571770.0567 5940865.3543 
571770.71 5940862.728 571774.4955 5940863.8084 571773.9593 5940866.3464 
571781.1553 5940867.6933 571771.9805 5940913.4566 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_E5C63FCB-D4DE-42CD-85A9-A67A1AC121B6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571766.7158 5940889.2853</gml:lowerCorner>
          <gml:upperCorner>571766.7158 5940889.2853</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_29089196-C74C-41C8-B0F1-789CD58EA588" />
      <xplan:position>
        <gml:Point gml:id="Gml_A8921FB3-8FFE-4058-8A19-076446C82F2A" srsName="EPSG:25832">
          <gml:pos>571766.7158 5940889.2853</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="Gml_427CD7AE-BFC7-468E-A4EA-003173DB0D5B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571791.4822 5940866.2293</gml:lowerCorner>
          <gml:upperCorner>571809.2106 5940919.7494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#Gml_103ED703-06EF-4A47-AFC5-C100DC6D87B0" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_077AB8C0-1E11-465B-9DB1-2082805294D6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571809.2106 5940866.4215 571796.8083 5940919.7494 571791.4822 5940919.3918 
571804.0319 5940866.2293 571809.2106 5940866.4215 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>4000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="Gml_AA27010E-4D18-4E8F-B923-6EABABB1998C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571644.419 5940807.9995</gml:lowerCorner>
          <gml:upperCorner>571673.682 5940829.0004</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:wirdDargestelltDurch xlink:href="#Gml_0FF5B0A4-3938-4E44-B9AA-F68DDD659E8D" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_4AAF9F64-CEED-419F-95E0-1E3384A91380" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571673.682 5940813.0196 571664.792 5940829.0004 571644.419 5940811.2733 
571653.5736 5940810.3208 571653.2713 5940808.3749 571657.4995 5940807.9995 
571657.7011 5940809.4212 571667.5966 5940808.4158 571673.682 5940813.0196 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="Gml_0FF5B0A4-3938-4E44-B9AA-F68DDD659E8D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571661.1469 5940816.2601</gml:lowerCorner>
          <gml:upperCorner>571661.1469 5940816.2601</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:dientZurDarstellungVon xlink:href="#Gml_AA27010E-4D18-4E8F-B923-6EABABB1998C" />
      <xplan:position>
        <gml:Point gml:id="Gml_F140D308-7ADC-4A53-8E49-81560E9C41B0" srsName="EPSG:25832">
          <gml:pos>571661.1469 5940816.2601</gml:pos>
        </gml:Point>
      </xplan:position>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="Gml_FD698672-398B-4D44-BFF9-334C58E22D0D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571665.1779 5940870.4834</gml:lowerCorner>
          <gml:upperCorner>571741.4932 5940969.2121</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_782C3F94-DBD2-4EF7-A80E-9D1B73234157" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571741.4932 5940969.2121 571704.3364 5940921.654 571677.4546 5940889.6922 
571665.1779 5940876.1455 571689.5197 5940870.4834 571726.694 5940915.049 
571718.812 5940914.526 571741.4932 5940969.2121 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="Gml_B8F59B5D-F1BC-4294-BCE8-F07313C5E47E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571601.3501 5940804.0537</gml:lowerCorner>
          <gml:upperCorner>571623.6549 5940812.2318</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_0FD0DC06-2284-4C0E-BFB9-8EA55A3C738C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571623.6549 5940811.02 571610.1455 5940812.2318 571601.3501 5940805.7176 
571603.3717 5940805.3746 571615.0084 5940804.0672 571615.161 5940804.0537 
571623.6549 5940811.02 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_7EC6E90C-7864-4A64-8B2E-15218E29752C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571412.7096 5940814.089</gml:lowerCorner>
          <gml:upperCorner>571763.393 5941260.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_EE35CEF6-D449-4DBC-9103-44171230E25F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4633CD22-EA31-4C25-B075-CA124B726E22" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571748.353 5941074.099 571730.384 5941083.885 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_15B92279-A9F4-496C-B93C-E45DD2A8DD40" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571730.384 5941083.885 571763.393 5941146.919 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A94291BB-4191-488B-86C5-2A3D896E5466" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571763.393 5941146.919 571762.6623 5941148.7277 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_4FDD95CA-D63A-4AA0-A539-1F5DB886383B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571762.6623 5941148.7277 571760.703920803 5941149.89506041 571758.7369 5941151.0478 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_5CBD601E-D7CF-43C0-9B32-6035BED2A2C9" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571758.7369 5941151.0478 571750.116150365 5941155.86559162 571741.342 5941160.398 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A29BB529-F34E-4C36-BA37-9673EB822B17" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571741.342 5941160.398 571737.0889 5941162.5065 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_089D9343-8E9E-4436-93F6-26B17F0FBCCE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571737.0889 5941162.5065 571732.0613 5941164.9989 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3614C7EC-619D-4C2C-9F06-1E53E36073B5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571732.0613 5941164.9989 571696.563 5941182.597 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A803A295-AC3B-49F3-BC4B-BC609132C49F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571696.563 5941182.597 571697.9099 5941186.0299 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E71564C6-8833-4EDE-8FC5-45A6F7BED33D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571697.9099 5941186.0299 571639.3311 5941214.9225 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F53CFDB5-232E-4298-94FB-A9DB1E266871" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571639.3311 5941214.9225 571550.7484 5941260.325 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E3BA17AD-2473-4D8D-861B-D10A7B2FA128" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571550.7484 5941260.325 571544.5571 5941258.42 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A2956505-C066-4D2E-89FE-324876BC3764" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.5571 5941258.42 571497.251 5941182.275 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2F86F4EA-2386-401C-988D-6D8A7591EB36" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571497.251 5941182.275 571462.9595 5941128.2448 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B0764C54-31E7-47F5-B006-2CB172C0B595" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571462.9595 5941128.2448 571420.2556 5941061.7284 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3BD61642-E092-4D24-8736-2BD0BA59BA4B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571420.2556 5941061.7284 571417.7245 5941048.2058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E766F17E-4D62-4331-9AF4-4351FA6B032F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571417.7245 5941048.2058 571415.802 5941042.715 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BE15D083-3C4B-4889-9829-1183633F623A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571415.802 5941042.715 571413.648 5941032.583 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_E3D7032C-2E7D-4B08-8731-85A4F9FBD3A2" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571413.648 5941032.583 571412.897905634 5941027.59698335 571412.72 5941022.558 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_553325CC-6141-4383-AC82-6526E3A1B08E" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571412.72 5941022.558 571413.601008887 5941014.73695472 571415.858 5941007.197 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_30D47B01-D69B-4B76-992F-7321A774AB09" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571415.858 5941007.197 571418.9449 5940999.5501 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A26857CD-A2F5-4807-BC26-2D00F5036601" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571418.9449 5940999.5501 571421.407 5940993.451 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_33FDA7D7-094F-4F9B-A643-5F1CC3B96708" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571421.407 5940993.451 571421.8387 5940992.6464 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A1E03D76-E8FB-4D90-BFDE-2463593D7BB5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571421.8387 5940992.6464 571434.3844 5940962.9857 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_91A56BB8-CBB2-40B1-AE90-29F273A16FD2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571434.3844 5940962.9857 571432.6382 5940961.0807 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_43A7A825-066A-4941-9067-BF7E246E97DE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571432.6382 5940961.0807 571436.9244 5940949.8094 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_43AF8B0C-7D25-4087-90EA-EC1EA7E29DAE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571436.9244 5940949.8094 571453.104 5940926.817 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EEC46B6D-8AAE-4905-9F60-BE3B9D7FBE80" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571453.104 5940926.817 571459.311 5940917.706 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8473FAF1-50C5-4185-A6FE-F8544F77C545" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571459.311 5940917.706 571465.01 5940908.934 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EBA110E7-458F-4C56-AB96-AF18FFB2F6DE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571465.01 5940908.934 571467.563 5940905.004 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6F44C10B-59C6-40E3-AA01-66F8118EAC52" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571467.563 5940905.004 571475.834 5940892.108 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_45C3F132-BC2A-4322-8964-F1D7D7833F70" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571475.834 5940892.108 571476.9431 5940890.9015 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F29BE4C3-2E44-44CA-9993-C5F415CE5E05" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571476.9431 5940890.9015 571485.634 5940881.448 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FFE40EA4-C331-4591-AC41-6D84823F6D4E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571485.634 5940881.448 571498.052 5940869.46 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E203DB38-1E1F-4D31-840D-C4537FFC9131" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571498.052 5940869.46 571514.961 5940854.55 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_F7E8CD7A-6F84-4D12-822B-B2AFB96CCB89" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571514.961 5940854.55 571523.69604612 5940847.2488567 571532.8995 5940840.5478 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9AE7A33E-4879-4D37-9E09-C3DAA66C6120" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571532.8995 5940840.5478 571532.9075 5940840.5424 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1EEDF120-8E56-46B6-96C6-7D18BC07D5A2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571532.9075 5940840.5424 571533.8441 5940839.9119 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_12A60877-D45C-4E64-A76A-F6797FD37620" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571533.8441 5940839.9119 571537.66416383 5940837.4318153 571541.5492 5940835.0548 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F846678C-AAA1-4F4C-885F-6302C35F546C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571541.5492 5940835.0548 571542.2644 5940834.6327 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_473B3800-0440-4FA9-B4CF-8CA3030A4098" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571542.2644 5940834.6327 571543.1015 5940834.1444 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5BEAD568-DF6F-4AE2-917B-C2BE18FACB8A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571543.1015 5940834.1444 571543.2558 5940834.0552 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7BE918D6-130C-4216-9D4B-F4EC7113C122" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571543.2558 5940834.0552 571543.7394 5940833.777 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E118AA65-48C8-4B62-8992-F44EAE44E870" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571543.7394 5940833.777 571543.8263 5940833.7272 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A3F64F13-8E81-4027-A7AE-3B39CF8DC4D1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571543.8263 5940833.7272 571544.2238 5940833.5 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A6B320F2-1C4B-46A3-89EA-315567C77CF6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.2238 5940833.5 571544.3979 5940833.4008 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_44266DC3-7DE4-4268-A3B0-050E27FCD88C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571544.3979 5940833.4008 571545.351731744 5940832.86332762 571546.309 5940832.332 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_482C80D1-6BC9-4C80-9CB2-A85F06581442" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571546.309 5940832.332 571554.824435527 5940827.95331204 571563.572 5940824.059 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_07ADA301-2C1A-4C4D-8AC4-4CCAACE954F9" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571563.572 5940824.059 571564.3136434 5940823.75580993 571565.0567 5940823.4561 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_594281B7-DC10-47EC-AAB2-1A33125842D5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571565.0567 5940823.4561 571565.0977 5940823.4397 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_8E6C4570-5E83-466C-9D86-AD482F9C2E53" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571565.0977 5940823.4397 571571.590036051 5940820.99344852 571578.1767 5940818.8139 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_32247196-7428-4FA6-9336-F5C84E3249B4" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571578.1767 5940818.8139 571586.031639119 5940816.59603031 571593.9839 5940814.7574 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9C267E44-762F-4CBD-AAAF-9E5843A79D80" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571593.9839 5940814.7574 571594.423 5940814.6675 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9AE2CC9B-4C42-4A3D-8173-FF4F10E56DA3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571594.423 5940814.6675 571594.5609 5940814.6397 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_814B4C38-828E-43D2-A1D8-A42F97AC10C4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571594.5609 5940814.6397 571594.9783 5940814.5556 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DA966FE3-6F30-4BF6-8A6C-40ED06AC9DB3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571594.9783 5940814.5556 571594.9815 5940814.5549 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2571970C-4006-49A1-B6D5-FCB1C6427DD9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571594.9815 5940814.5549 571594.9942 5940814.5524 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DFD114E6-044A-41F6-A8CD-A2C6F4FD7ABB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571594.9942 5940814.5524 571595.0693 5940814.5375 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FF13958A-ED07-4CDA-A24A-8ED05CEC1314" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.0693 5940814.5375 571595.1383 5940814.5238 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_380853AB-6419-4E0B-BD4C-03B71A7A990D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.1383 5940814.5238 571595.4543 5940814.4611 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F2693EA6-AEEF-4C6C-B6E6-98913E2794A5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.4543 5940814.4611 571595.463 5940814.4594 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_26514614-A2DC-4DE2-BB59-D293D71C00E9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.463 5940814.4594 571595.4813 5940814.4558 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_59FADF08-6801-4FB3-8DD4-991C8E70C62F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.4813 5940814.4558 571595.6488 5940814.4231 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_66892F0A-AFE1-4738-A60F-FAC569E739D9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.6488 5940814.4231 571595.7161 5940814.41 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E74614A4-4451-4CC9-A34E-BD185F90484E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.7161 5940814.41 571596.2624 5940814.304 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DDCE8DFF-ABEF-420D-9CD1-E9225B87B35A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571596.2624 5940814.304 571596.2659 5940814.3033 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_591A4CD2-D1D4-4C6C-BEFE-403FACFB22D9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571596.2659 5940814.3033 571596.8349 5940814.1959 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_65DECB7C-2FD0-4B85-82D7-759891806C83" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571596.8349 5940814.1959 571597.404 5940814.089 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0F95516A-5D10-4965-9E88-D7B001B06732" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571597.404 5940814.089 571603.199 5940827.834 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_43B16B19-2086-4714-9C3B-86992142AF83" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571603.199 5940827.834 571611.729 5940848.058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D09CAB5D-3CFB-4D83-BEFB-23DF3605F79D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571611.729 5940848.058 571613.7034 5940852.74 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6E858A50-8008-4919-9536-113A734BC1BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571613.7034 5940852.74 571614.7267 5940855.1667 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F2958484-70A6-4F7D-A16D-9D39A2B92D19" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571614.7267 5940855.1667 571617.289 5940861.243 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3E02440A-5DDB-4B3A-BD9A-E8CB4416F9F9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571617.289 5940861.243 571618.004 5940862.939 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BD4E6426-4714-43C8-A703-E941E6D092F4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571618.004 5940862.939 571622.606 5940873.853 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3D758F1C-CF14-4CB0-830C-C4C590757F97" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571622.606 5940873.853 571627.698 5940885.928 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6527F65F-1605-4FB3-9A3A-D5D5381D5009" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571627.698 5940885.928 571632.597 5940897.542 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C78282A3-35AF-4DB3-AE77-61E03599E142" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571632.597 5940897.542 571637.509 5940909.189 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_469FB827-788D-4D79-856D-145D48FA3227" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571637.509 5940909.189 571638.7949 5940912.2374 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7D0B2B92-CAD6-463A-ABCA-6445712ADDE9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571638.7949 5940912.2374 571642.406 5940920.798 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D9787331-2223-445F-9C5F-C87F3291A826" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571642.406 5940920.798 571645.4713 5940928.0679 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2A20B2C8-32BD-40E2-8C96-8ACDA938045D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571645.4713 5940928.0679 571647.338 5940932.495 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EC103178-3C48-4085-A5C7-D6CE0715BADC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571647.338 5940932.495 571652.245 5940944.129 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0F9A8495-A30A-47B3-B7A6-C52E9298FFD3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571652.245 5940944.129 571654.025 5940948.352 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AE47D646-8D89-438A-8BFF-314765F8BEC7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571654.025 5940948.352 571687.0827 5941009.3702 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A7492492-049E-43D8-BA4B-E10BF7BC5599" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571687.0827 5941009.3702 571693.4886 5941021.1942 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ADAF9E1E-CD68-49F2-BA87-45F34A74FADB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571693.4886 5941021.1942 571707.413 5941046.896 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A2AC52D3-25EA-48CA-9163-6B60F689B230" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571707.413 5941046.896 571745.007 5941026.838 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_20D32E73-A3E7-4C0A-A0E9-7580777EB576" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571745.007 5941026.838 571745.295 5941028.497 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FC53E3DB-7258-4235-BBF6-BCDE7AD1D1A6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571745.295 5941028.497 571748.353 5941074.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>7000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="Gml_534139AD-4068-455A-A15B-5F3CDE1B519F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571412.7096 5940925.202</gml:lowerCorner>
          <gml:upperCorner>571561.1056 5941086.8815</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die Gemeinschaftsstellplätze und Gemeinschaftstiefgaragen bestimmt sind</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_63D4DC1F-7F1B-4A21-827D-2C16C09BCEBF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0BBCA4EE-3B95-4DC7-A37E-67594D6BC859" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571550.505 5940952.898 571554.5007 5940964.2381 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_12211264-3CD2-4225-8A73-C8E4B49FCB8B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571554.5007 5940964.2381 571554.8426 5940965.2081 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A265D755-1A1E-4908-9F13-AC13C376FED7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571554.8426 5940965.2081 571558.2563 5940974.9019 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ADC4B3FA-E564-4CAB-952A-17193EADBE73" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571558.2563 5940974.9019 571559.8671 5940979.4741 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B25FF1AF-8647-4FFE-9174-B0E7B20ACF26" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571559.8671 5940979.4741 571560.9534 5940982.557 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C56DBB6C-9E3E-4142-B3D2-9347006B3C96" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571560.9534 5940982.557 571561.1056 5940982.9892 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C41A7A22-F2F4-4493-A479-E0EBC42C6591" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571561.1056 5940982.9892 571555.2369 5940986.606 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CF884D91-AF15-4362-BF7F-62F603C37D21" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571555.2369 5940986.606 571551.8549 5940988.6902 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_564AACB6-3F6D-4E87-93BD-FD99640BD5D9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571551.8549 5940988.6902 571539.5911 5940996.2481 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_47FA00E8-98DD-4B81-AAC3-A2167C07F1A4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571539.5911 5940996.2481 571532.759 5941000.4586 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6510C5A3-0F0D-4DF1-BF32-97EFB610B4B4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571532.759 5941000.4586 571526.2404 5941004.4759 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_13C03148-39E1-41C9-850E-B2D9639664DD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571526.2404 5941004.4759 571526.1058 5941004.5589 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_08DCA602-61B8-4D5A-836F-3821E0ED0684" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571526.1058 5941004.5589 571526.0026 5941004.6225 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_12D2EE17-0E35-4D5B-A49E-4D37FD2C0566" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571526.0026 5941004.6225 571496.838 5941022.596 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8C574FCE-623D-422E-9F4B-1D5C0E024437" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571496.838 5941022.596 571492.577 5941055.472 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_952AE290-9C85-4274-B1F6-563F89CDA59B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571492.577 5941055.472 571476.8053 5941065.2809 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F8901839-1A91-4A17-8C71-96E5687D4B6E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571476.8053 5941065.2809 571442.0736 5941086.8815 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3C312D08-C927-49E7-81D9-E87BFF077509" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571442.0736 5941086.8815 571437.7392 5941079.9969 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D103FF9D-516D-4B7A-AB54-452F117170DF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571437.7392 5941079.9969 571435.4264 5941076.3231 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_522759C8-4D8B-44BB-9B69-E1675275F636" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571435.4264 5941076.3231 571433.9014 5941073.901 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F5E4247-F70B-46BE-AADE-EACF57F166DE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571433.9014 5941073.901 571424.9024 5941059.607 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E128AEC8-CD20-436A-9885-65E18F21D510" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571424.9024 5941059.607 571424.4675 5941058.9162 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5F014F9D-EB1E-4D5E-808B-1D93B805C250" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571424.4675 5941058.9162 571417.7245 5941048.2058 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F534543B-567E-418C-8AC2-692978B8550F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571417.7245 5941048.2058 571415.802 5941042.715 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DB869348-736A-47B4-9399-64ABEDA63FD5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571415.802 5941042.715 571413.648 5941032.583 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_679CCE59-AD99-4353-8AAB-3380522A3D7F" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571413.648 5941032.583 571412.897905634 5941027.59698335 571412.72 5941022.558 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_D2DA02D1-8718-4861-B339-54DD2C993C3D" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571412.72 5941022.558 571413.601008887 5941014.73695472 571415.858 5941007.197 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_AD0713F9-2CDF-4B8B-B6B5-3EE1EBD6D9B2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571415.858 5941007.197 571416.923 5941004.5589 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_79291799-160F-4113-BD93-DF9BA30E2746" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571416.923 5941004.5589 571418.9449 5940999.5501 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DA699AE1-0CFC-4BF3-88C7-253097EF67B4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571418.9449 5940999.5501 571421.407 5940993.451 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9D985723-75AC-4DE7-BD9D-1F2CD79AA84B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571421.407 5940993.451 571421.8387 5940992.6464 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4D388BD8-253E-4BC8-9076-46165C44125E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571421.8387 5940992.6464 571440.516 5940957.8401 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7859E828-B5C0-4260-938F-7371BC483773" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571440.516 5940957.8401 571453.9933 5940932.7244 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ED74B686-66C7-44F5-8F41-12DA22E9D09B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571453.9933 5940932.7244 571456.09 5940928.817 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EB53E691-3422-4567-B6A7-2809A124BDCF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571456.09 5940928.817 571458.0696 5940925.202 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4E838061-2A68-4DD7-BE99-8268239014C6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571458.0696 5940925.202 571478.5941 5940932.7244 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_086E7199-4081-4C31-B2E3-A4DC6C1D8C42" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571478.5941 5940932.7244 571485.3373 5940935.1958 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1FA26505-E45B-49BF-A881-8E16F4E74559" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571485.3373 5940935.1958 571497.097 5940939.5057 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9EB94DFE-1C43-48CA-859F-6D5186E1101F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571497.097 5940939.5057 571497.408 5940940.199 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_048E1AA5-C8B0-4F99-9124-05DC3B48FE04" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571497.408 5940940.199 571498.128 5940939.8836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9063CDCE-DDB7-4C34-9766-97F457A4A6A3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571498.128 5940939.8836 571507.0288 5940943.1458 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F7DC047F-941B-484E-97C3-5A566E7F9EF9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571507.0288 5940943.1458 571512.5739 5940945.1781 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D3C6903E-0201-42E0-BB66-30477FE2C2D4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571512.5739 5940945.1781 571518.1742 5940943.2321 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0D953496-F529-4428-BAC2-2E9F87DC23BE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571518.1742 5940943.2321 571518.605 5940943.0824 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F5632547-849C-4056-952D-A9BD9B07B936" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571518.605 5940943.0824 571526.6287 5940940.2943 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4A26D7CE-070D-48CA-BE87-1FD268E0E372" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571526.6287 5940940.2943 571543.9461 5940934.2769 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B4F6CFE1-E8D1-4B77-9AF2-10DC2D92B0AF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571543.9461 5940934.2769 571544.553 5940936 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E7359CD2-B816-4722-B1C0-A0F103044E58" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.553 5940936 571546.388 5940941.209 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8777351C-A086-4F62-9F3C-45B5AB872EEB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571546.388 5940941.209 571550.505 5940952.898 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="Gml_16537152-CD4E-4580-B129-3A11C7C2124B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571451.7196 5941029.288</gml:lowerCorner>
          <gml:upperCorner>571591.263 5941170.4713</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die Gemeinschaftsstellplätze bestimmt sind</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_EC8E9182-99B7-4B2E-AE02-EC25E2D390F6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571527.3996 5941152.8541 571497.67 5941168.963 571495.1378 5941170.4713 
571493.274 5941167.692 571474.5422 5941138.466 571453.139 5941105.072 
571451.7196 5941102.3674 571455.468 5941099.937 571514.021 5941063.103 
571516.7053 5941042.1851 571517.2405 5941038.0142 571518.041 5941031.776 
571522.043 5941029.288 571526.9246 5941038.5295 571547.3915 5941077.2759 
571547.9059 5941078.2497 571548.766 5941079.878 571547.89 5941080.3408 
571548.2864 5941081.1035 571559.9901 5941103.1938 571573.073 5941127.887 
571578.183 5941124.746 571583.4409 5941134.4266 571591.263 5941148.828 
571590.288 5941149.3395 571574.9644 5941157.3792 571573.278 5941158.264 
571565.1315 5941142.8487 571563.0541 5941138.9178 571561.11 5941135.239 
571560.843 5941134.733 571527.3996 5941152.8541 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GemeinschaftsanlagenZuordnung gml:id="Gml_9E8370DB-25A1-49AE-938F-E91E7E1092AF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571512.401 5941083.885</gml:lowerCorner>
          <gml:upperCorner>571763.393 5941251.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Umgrenzung der Grundstücke, für die Gemeinschaftsstellplätze und Gemeinschaftstiefgaragen bestimmt sind</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_0D5C8CB3-2082-400B-9528-4BEF683DAF50" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_0DF94D3A-BBA1-4269-8224-D0702B847D74" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571751.4876 5941155.1235 571746.439879064 5941157.80899005 571741.342 5941160.398 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D81CC07A-CCF5-4CCA-A710-171AF477E43F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571741.342 5941160.398 571737.0889 5941162.5065 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B0C0EDD3-908A-428A-91B9-BDD8D3F7724A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571737.0889 5941162.5065 571732.0613 5941164.9989 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2AD43AF4-F48E-48F6-A56F-F459B696C67F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571732.0613 5941164.9989 571696.563 5941182.597 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9DA3A27B-4096-4D3F-BD95-BA0BCC4D19BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571696.563 5941182.597 571657.4212 5941202.2535 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4A0281EB-6B1C-4F15-8E15-37D5256DF1CD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571657.4212 5941202.2535 571652.2189 5941204.866 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_351DEEE4-6A6A-4D2B-8103-83730E3DD6D5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571652.2189 5941204.866 571645.686 5941208.1467 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8A12DE3C-637B-4B14-910D-D6C9447CDAA2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571645.686 5941208.1467 571605.7868 5941228.1836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_16CF9066-CFAC-48AE-93FC-D1008FFD4C47" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571605.7868 5941228.1836 571595.9616 5941233.1177 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_19385771-AEF8-436B-A1FE-CD1CB7C931C5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.9616 5941233.1177 571579.7342 5941241.2668 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0E1FC985-58C5-4955-9E61-568AFF2DC688" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571579.7342 5941241.2668 571558.451 5941251.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CAF5E429-49CC-41A2-AE94-F4005A238561" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571558.451 5941251.955 571544.709 5941249.393 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8329FBD6-BAC6-4470-AC03-F898BFA9B86D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571544.709 5941249.393 571539.5452 5941241.2668 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3BECF098-B16D-443A-B5FA-9B5950515FF8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571539.5452 5941241.2668 571534.3667 5941233.1177 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C14D1DF6-8DD3-4A97-AA07-08BC8441A9B2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571534.3667 5941233.1177 571525.5081 5941219.1772 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_40957749-54BD-4B8F-8CBA-4984EF424EB9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571525.5081 5941219.1772 571522.8363 5941214.9727 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_226BB289-C081-4215-A795-EA82A6CF1807" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571522.8363 5941214.9727 571519.5071 5941209.7336 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_49BE54C0-3C68-45ED-B121-387B85CF3E0B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571519.5071 5941209.7336 571518.7495 5941208.5413 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0069D032-5526-4CD6-9F20-FE6F5D5D7E87" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571518.7495 5941208.5413 571516.4666 5941204.9489 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C13E0A99-E9BD-410A-998C-6018B3B35E34" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571516.4666 5941204.9489 571512.401 5941198.551 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_103C6CFC-4D0D-480E-B134-A476183774E2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571512.401 5941198.551 571512.4258 5941198.466 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_88215979-6CFA-47D9-927A-49EBB73C9EB5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571512.4258 5941198.466 571575.379 5941162.24 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8220233E-1012-47ED-AAF7-CFF9CA27A0E5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571575.379 5941162.24 571577.581 5941161.0847 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EE94C4BA-89D0-4BA7-85DF-6A3AF5946772" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571577.581 5941161.0847 571585.2286 5941157.0724 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BFE4CD2A-6632-4322-BB14-6504E71A7C4A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571585.2286 5941157.0724 571593.41 5941152.78 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3C8C5E85-A593-4FE6-8D9B-872B487F508C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571593.41 5941152.78 571595.796 5941157.176 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_03604DCF-EDE0-420C-8A3B-52365C25976F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571595.796 5941157.176 571618.9365 5941144.5747 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B1D835AD-422B-435B-AA5A-42F83370E962" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571618.9365 5941144.5747 571632.4569 5941137.2121 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E184D656-209C-4664-90CC-C588C5B345EF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571632.4569 5941137.2121 571633.0521 5941136.8879 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_062F1447-8C31-41BD-9829-C9EC98F56797" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571633.0521 5941136.8879 571633.4194 5941136.6879 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BA97554B-20D9-4F34-9962-772E8AFD3199" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571633.4194 5941136.6879 571636.2334 5941135.1555 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_87FDE2F3-CB41-44A1-B801-89B103AE64F4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571636.2334 5941135.1555 571650.7661 5941127.2416 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B76AF418-D7E3-480B-9D9B-B86208CCC8C2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571650.7661 5941127.2416 571653.7294 5941125.6279 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6422EB87-FFAE-4A28-9E69-75D794ED5E0C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571653.7294 5941125.6279 571661.666 5941121.306 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2D9E142E-E474-4690-8693-1DDC654F6ADD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571661.666 5941121.306 571687.7492 5941107.1021 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_199A8011-3E15-4E7C-9A6C-8645ED221797" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571687.7492 5941107.1021 571730.384 5941083.885 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_52A74264-1289-40ED-A003-76A74C0E77C2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571730.384 5941083.885 571763.393 5941146.919 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8A784DF8-4FC4-44B2-AD96-A95B142E4698" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571763.393 5941146.919 571762.6623 5941148.7277 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E2BF63DE-92DE-4DAB-B291-F45DE9B1D738" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571762.6623 5941148.7277 571762.661 5941148.731 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_C7C87270-0B63-4998-A8E4-862359CB9CE9" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571762.661 5941148.731 571761.87960994 5941149.19847773 571761.0964 5941149.6629 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A6E8284E-E31B-4BB4-A959-7387B77CF85B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571761.0964 5941149.6629 571760.8468 5941149.8109 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_21A50A95-5B43-4B8A-9EF2-46B44C9A43EF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571760.8468 5941149.8109 571760.3112 5941150.1267 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F25F6239-DBEC-4F9F-8203-937410BF5D82" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571760.3112 5941150.1267 571759.9361 5941150.3475 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_FF8D78B3-B084-4758-9385-441A20F019BE" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571759.9361 5941150.3475 571759.73035 5941150.46785 571759.5246 5941150.5882 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8F308E90-C028-4CC2-B185-4ECA69DB647A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571759.5246 5941150.5882 571758.7367 5941151.0474 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_2F703356-6BF3-4EC2-8237-1325DA665AE4" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571758.7367 5941151.0474 571755.669977402 5941152.80401153 571752.5829 5941154.5246 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_60AC0C82-A2FC-45FE-B663-99A6B5756107" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571752.5829 5941154.5246 571752.3999 5941154.6249 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_0AC7E4CE-DB7E-48B8-9903-39B4A9CDAD2A" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">571752.3999 5941154.6249 571752.0279 5941154.8285 571751.6559 5941155.0321 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_59511529-3289-478C-90B4-12609411BFB3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">571751.6559 5941155.0321 571751.4876 5941155.1235 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_GemeinschaftsanlagenZuordnung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_ZusatzkontingentLaermFlaeche gml:id="Gml_889FC623-91B2-4255-BA85-990BC72069E5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571689.5197 5940859.5297</gml:lowerCorner>
          <gml:upperCorner>571726.694 5940915.049</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5C565D79-1FC4-48BD-8383-4384DA8576D4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571689.5197 5940870.4834 571693.3297 5940859.5297 571726.694 5940915.049 
571689.5197 5940870.4834 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:richtungssektor>
        <xplan:BP_Richtungssektor>
          <xplan:winkelAnfang uom="grad">45</xplan:winkelAnfang>
          <xplan:winkelEnde uom="grad">50</xplan:winkelEnde>
          <xplan:zkWertTag uom="db">30</xplan:zkWertTag>
          <xplan:zkWertNacht uom="db">21</xplan:zkWertNacht>
        </xplan:BP_Richtungssektor>
      </xplan:richtungssektor>
    </xplan:BP_ZusatzkontingentLaermFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_ZusatzkontingentLaerm gml:id="Gml_5CAD6BF3-412A-41F7-8448-4EF9D2ADCF37">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571675.6919 5940834.9832</gml:lowerCorner>
          <gml:upperCorner>571675.6919 5940834.9832</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_4EB8D512-A8DB-4303-A85D-EA86B40C5D45" srsName="EPSG:25832">
          <gml:pos>571675.6919 5940834.9832</gml:pos>
        </gml:Point>
      </xplan:position>      
    </xplan:BP_ZusatzkontingentLaerm>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Sichtflaeche gml:id="Gml_A429D483-D91F-488A-9519-8E04B5C8CDD2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571678.6685 5940807.8765</gml:lowerCorner>
          <gml:upperCorner>571703.5923 5940845.3416</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8910DC08-B7FC-4E3A-8A14-544B7E223809" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571678.6685 5940809.3053 571697.7185 5940807.8765 571703.5923 5940844.0716 
571685.336 5940845.3416 571678.6685 5940809.3053 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
	  <xplan:art>1000</xplan:art>
	  <xplan:knotenpunkt>6000</xplan:knotenpunkt>
	  <xplan:geschwindigkeit uom="km/h">70</xplan:geschwindigkeit>
	  <xplan:schenkellaenge uom="m">100</xplan:schenkellaenge>
    </xplan:BP_Sichtflaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_5DFC33B7-DDF5-44C5-A54E-5280ABBDF7EA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571637.5739 5940838.1235</gml:lowerCorner>
          <gml:upperCorner>571693.3297 5940876.1455</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_DC2904E7-1EBB-4197-9A9A-9BD87D009FCB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571672.134 5940838.1235 571681.6345 5940846.8123 571693.3297 5940859.5297 
571689.5197 5940870.4834 571665.1779 5940876.1455 571642.5359 5940853.5137 
571637.5739 5940849.3911 571672.134 5940838.1235 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>4000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>2100</xplan:besondereArtDerBaulNutzung>
      <xplan:sondernutzung>2720</xplan:sondernutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_068D3EBD-3701-42FC-9CED-C9DB9947B5CE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571610.1455 5940809.4808</gml:lowerCorner>
          <gml:upperCorner>571672.134 5940849.3911</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E45E3106-C95A-497F-84A1-E9E2AFB282D9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">571610.1455 5940812.2318 571623.6549 5940811.02 571640.8152 5940809.4808 
571672.134 5940838.1235 571637.5739 5940849.3911 571618.8228 5940833.8121 
571610.1455 5940812.2318 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:allgArtDerBaulNutzung>4000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>2100</xplan:besondereArtDerBaulNutzung>
      <xplan:sondernutzung>23000</xplan:sondernutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AbstandsMass gml:id="Gml_951A3969-DDA8-44CC-81C2-0503217742C1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571792.432 5940848.9821</gml:lowerCorner>
          <gml:upperCorner>571796.4663 5940869.088</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1E9C6A3E-A3CB-4B4A-ABA7-338E99B6889F" srsName="EPSG:25832">
          <gml:posList>571792.432 5940869.088 571796.4663 5940848.9821 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_AbstandsMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="Gml_720B069D-37D3-469D-8993-2FE55DE5899A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>571803.4187 5940853.104</gml:lowerCorner>
          <gml:upperCorner>571803.4187 5940853.104</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">25</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#Gml_97F3E133-8F43-4ECC-8B91-0C0E8FAE863F" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_6964015E-350A-4AB0-BB84-A65220188E3B" srsName="EPSG:25832">
          <gml:pos>571803.4187 5940853.104</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
</xplan:XPlanAuszug>