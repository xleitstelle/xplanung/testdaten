﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_BB5DE7A0-60D6-4DC8-8F5C-2C60B4371E40" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/1 http://www.xplanungwiki.de/upload/XPlanGML/5.1/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/1">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
      <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_88bfe952-199f-4bba-bea2-c2b441737144">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan004_5-1</xplan:name>
      <xplan:beschreibung>Der vorhabenbezogene Bebauungsplan BPlan004_5-1
für den Geltungsbereich Knickweg – Barmbeker Straße –
Gertigstraße
(Bezirk Hamburg-Nord, Ortsteil 412) wird festgestellt.</xplan:beschreibung>
      <xplan:technHerstellDatum>2017-03-20</xplan:technHerstellDatum>
      <xplan:aendert />
      <xplan:wurdeGeaendertVon />
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_5AE96C8F-3997-49ED-A7DB-9C2CD89FCBF4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:externeReferenz>
        <xplan:XP_SpezExterneReferenz>
          <xplan:art>Dokument</xplan:art>
          <xplan:referenzURL>BPlan004_5-1.pdf</xplan:referenzURL>
          <xplan:typ>1065</xplan:typ>
        </xplan:XP_SpezExterneReferenz>
      </xplan:externeReferenz>
      <xplan:texte xlink:href="#GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa" />
      <xplan:texte xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:texte xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:texte xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:texte xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:texte xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:texte xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:texte xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:texte xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>412</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:plangeber />
      <xplan:planArt>3000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2016-10-25</xplan:rechtsverordnungsDatum>
      <xplan:durchfuehrungsVertrag>true</xplan:durchfuehrungsVertrag>
      <xplan:bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Im Plangebiet ist in den Schlafräumen durch geeignete bauliche
Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden,
verglaste Loggien, Wintergärten, besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
sicherzustellen, dass ein Innenraumpegel bei
gekipptem Fenster von 30 dB(A) während der Nachtzeit
nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme
in Form von verglasten Loggien oder Wintergärten
muss dieser Innenraumpegel bei gekippten/teilgeöffneten
Bauteilen erreicht werden. Wohn-/Schlafräume in
Einzimmerwohnungen und Kinderzimmer sind wie Schlafräume
zu beurteilen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0d37aade-f41e-4ec9-b026-79d4709945bf">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die nicht überbauten Flächen
von Tiefgaragen mit einem mindestens 50 cm starken
durchwurzelbaren Substrataufbau zu versehen und gärtnerisch
anzulegen. Hiervon ausgenommen sind die erforderlichen
Flächen für Terrassen, Wege und Freitreppen, Kinderspielflächen
sowie Bereiche, die der Belichtung, Be- und
Entlüftung oder der Aufnahme von technischen Anlagen
dienen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Stellplätze nur in Tiefgaragen
zulässig. Tiefgaragen sind auch außerhalb der überbaubaren
Grundstücksflächen zulässig. Tiefgaragen dürfen
einschließlich ihrer Überdeckung nicht über die natürliche
Geländeoberfläche herausragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die Freiflächen zur öffentlichen
Grünanlage mit einer Hecke abzupflanzen. Für die
Hecke sind standortgerechte einheimische Gehölze zu verwenden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2d0243f5-23e4-4f40-8538-d833e9c168dd">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind mindestens 55 vom Hundert
(v. H.) der Dachflächen und auf der Fläche für den
besonderen Nutzungszweck – Kiosk – mindestens 80 v. H.
der Dachflächen mit einem mindestens 10 cm starken
durchwurzelbaren Substrataufbau zu versehen und extensiv
zu begrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6bac8f46-a325-4392-b115-07bb0e41e8c1">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Terrassen im Anschluss
an die Hauptnutzung bis zu einer Tiefe von 4 m auch außerhalb
der überbaubaren Grundstücksfläche zulässig. Untergeordnete
Bauteile wie Vordächer, Balkone und Erker im
Bereich von öffentlichen Grünflächen sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind innerhalb der mit „(A)“
bezeichneten Fläche (Vorhabengebiet) im Rahmen der festgesetzten
Nutzungen nur solche Vorhaben zulässig, zu
deren Durchführung sich der Vorhabenträger im Durchführungsvertrag
verpflichtet.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet darf die festgesetzte Grundflächenzahl
für Tiefgaragen bis zu einer Grundflächenzahl
von 1,0 überschritten werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_114d1690-3976-42e3-87be-ec97b7ad773d">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind auf den Flurstücken 1477,
1478 und 1480 der Gemarkung Winterhude vor den straßenzugewandten
Fenstern der Wohn- und Schlafräume
lärmgeschützte Außenbereiche durch bauliche Schallschutzmaßnahmen,
wie etwa verglaste Loggien, Wintergärten
oder in ihrer Wirkung vergleichbare Maßnahmen zwingend
vorzusehen. In den lärmgeschützten Außenbereichen
ist bei geöffneten Fenstern/Bauteilen sicherzustellen, dass
ein Tagpegel von weniger als 65 dB(A) erreicht wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_6e871e4c-58e1-4b97-bf2a-a696816b2458">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_9B24F87A-E8B5-471C-8129-22520053C2FD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan004_5-1.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan004_5-1.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
      <xplan:planinhalt xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <xplan:planinhalt xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:planinhalt xlink:href="#GML_5276e223-57f7-4596-9c9c-8fb909e11db2" />
      <xplan:planinhalt xlink:href="#GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e" />
      <xplan:planinhalt xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:planinhalt xlink:href="#GML_354dc6b6-d910-4f29-8aeb-66d238013da6" />
      <xplan:planinhalt xlink:href="#GML_b607f50f-8221-4811-81f0-7f6efef25db3" />
      <xplan:planinhalt xlink:href="#GML_20f74701-9a0b-4c38-89b8-ae81db151e28" />
      <xplan:planinhalt xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:planinhalt xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:planinhalt xlink:href="#GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f" />
      <xplan:planinhalt xlink:href="#GML_9875e68a-2b21-40e4-8f66-ecf5c2290936" />
      <xplan:planinhalt xlink:href="#GML_7b11be88-979b-483f-8042-82c51eff70af" />
      <xplan:planinhalt xlink:href="#GML_be2c01dd-4d15-4a6a-85c0-dc9565324486" />
      <xplan:planinhalt xlink:href="#GML_21c90a12-5886-40dd-8a86-463267b6e201" />
      <xplan:planinhalt xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <xplan:planinhalt xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:planinhalt xlink:href="#GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299" />
      <xplan:planinhalt xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:planinhalt xlink:href="#GML_8376d624-8ccb-473d-81df-fc95b1ef28b3" />
      <xplan:planinhalt xlink:href="#GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3" />
      <xplan:planinhalt xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:planinhalt xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <xplan:planinhalt xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:planinhalt xlink:href="#GML_5355860b-25fa-401c-a2ea-bb70654d17ab" />
      <xplan:planinhalt xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
      <xplan:praesentationsobjekt xlink:href="#GML_de628759-edb3-4f31-9d43-7d4f816bfb0c" />
      <xplan:praesentationsobjekt xlink:href="#GML_82075a17-ae02-4228-a4ac-debedfd66c1e" />
      <xplan:praesentationsobjekt xlink:href="#GML_a435c756-f459-41ce-b27c-d5992aa8f87e" />
      <xplan:praesentationsobjekt xlink:href="#GML_7d7588c9-1211-471e-ad15-8dd9f6605701" />
      <xplan:praesentationsobjekt xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <xplan:praesentationsobjekt xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <xplan:praesentationsobjekt xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <xplan:praesentationsobjekt xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <xplan:praesentationsobjekt xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <xplan:praesentationsobjekt xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <xplan:praesentationsobjekt xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <xplan:praesentationsobjekt xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <xplan:praesentationsobjekt xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <xplan:praesentationsobjekt xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <xplan:praesentationsobjekt xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <xplan:praesentationsobjekt xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <xplan:praesentationsobjekt xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <xplan:praesentationsobjekt xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <xplan:praesentationsobjekt xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <xplan:praesentationsobjekt xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <xplan:praesentationsobjekt xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <xplan:praesentationsobjekt xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <xplan:praesentationsobjekt xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <xplan:praesentationsobjekt xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <xplan:praesentationsobjekt xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <xplan:praesentationsobjekt xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />
      <xplan:gehoertZuPlan xlink:href="#GML_88bfe952-199f-4bba-bea2-c2b441737144" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_02146971-F1C5-4D70-8FE6-6B4B3E959748" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567433.287 5937679.305 567416.015 5937672.057 567419.988 5937662.431 
567432.605 5937667.621 567439.105 5937652.001 567428.275 5937631.879 
567422.612 5937629.344 567428.448 5937616.596 567434.138 5937619.1 
567436.484 5937623.475 567442.246 5937634.25 567452.958 5937654.188 
567450.566 5937668.248 567438.775 5937666.233 567433.287 5937679.305 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567437.222 5937662.769</gml:lowerCorner>
          <gml:upperCorner>567437.222 5937662.769</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <xplan:position>
        <gml:Point gml:id="Gml_D71A1375-BCE4-40D1-AB2D-58D63E8838BC" srsName="EPSG:25832">
          <gml:pos>567437.222 5937662.769</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7AB5EB2E-8541-4CB4-A88C-0FB437FFFE40" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567452.175 5937661.631 567442.18 5937659.754 
567439.277 5937659.277 567439.035 5937655.06 567438.79 5937652.388 
567432.026 5937639.916 567442.758 5937634.17 567453.455 5937654.108 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_045ae35f-52f5-48d9-b5a6-3d5f339418be">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567444.114 5937651.76</gml:lowerCorner>
          <gml:upperCorner>567444.114 5937651.76</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:position>
        <gml:Point gml:id="Gml_FC9C6FDB-9AB2-4C82-8C58-592FB317C6FA" srsName="EPSG:25832">
          <gml:pos>567444.114 5937651.76</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5890c370-a3bc-412c-b650-13894b87a426">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.328 5937644.053</gml:lowerCorner>
          <gml:upperCorner>567439.328 5937644.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:position>
        <gml:Point gml:id="Gml_333AFB4F-856F-48D1-B9A5-C25B922FF416" srsName="EPSG:25832">
          <gml:pos>567439.328 5937644.053</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_5276e223-57f7-4596-9c9c-8fb909e11db2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1AF12B35-E725-4E48-9514-E6E9757FE71B" srsName="EPSG:25832">
          <gml:posList>567416.015 5937672.057 567419.988 5937662.431 567432.605 5937667.621 
567439.105 5937652.001 567428.275 5937631.879 567422.612 5937629.344 
567428.448 5937616.596 567434.138 5937619.1 567436.484 5937623.475 
567442.246 5937634.25 567452.958 5937654.188 567450.566 5937668.248 
567438.775 5937666.233 567433.287 5937679.305 567416.015 5937672.057 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.23 5937680.271</gml:lowerCorner>
          <gml:upperCorner>567428.23 5937680.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_57D63D88-7FDE-47AB-9B9A-FE2D9967B6F1" srsName="EPSG:25832">
          <gml:pos>567428.23 5937680.271</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e21981e2-40a7-4dac-84b0-c88d371030af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">23.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E1E892FE-8635-491A-9586-C7E87374CB63" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567442.758 5937634.17 567432.026 5937639.916 
567427.969 5937632.352 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_10c25c52-be4f-422d-8171-469001af2bf4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.835 5937628.058</gml:lowerCorner>
          <gml:upperCorner>567426.835 5937628.058</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:position>
        <gml:Point gml:id="Gml_97695201-D061-4BAF-9FBE-4DA4DD259493" srsName="EPSG:25832">
          <gml:pos>567426.835 5937628.058</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567433.633 5937633.755</gml:lowerCorner>
          <gml:upperCorner>567433.633 5937633.755</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:position>
        <gml:Point gml:id="Gml_E4051B90-2FFC-4604-9B4C-14776B4FF820" srsName="EPSG:25832">
          <gml:pos>567433.633 5937633.755</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_354dc6b6-d910-4f29-8aeb-66d238013da6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_BDFC6020-B07E-4FF9-8D79-E3D367EF0183" srsName="EPSG:25832">
          <gml:posList>567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
567437.269 5937671.067 567433.538 5937679.955 567415.353 5937672.325 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b607f50f-8221-4811-81f0-7f6efef25db3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9FD23656-990B-4821-B563-84AB32CC698F" srsName="EPSG:25832">
          <gml:posList>567428.223 5937615.867 567434.477 5937618.73 567442.758 5937634.17 
567432.026 5937639.916 567427.969 5937632.352 567421.935 5937629.605 
567428.223 5937615.867 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_20f74701-9a0b-4c38-89b8-ae81db151e28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_EF7AA4C7-D4C2-4D88-B38C-6AA02AD3CE40" srsName="EPSG:25832">
          <gml:posList>567450.952 5937668.824 567440.924 5937667.111 567442.18 5937659.754 
567452.173 5937661.63 567450.952 5937668.824 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9A1AA181-DADB-42E4-B044-1C9D6B88F033" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567451.021 5937668.836 567448.434 5937683.72 
567445.762 5937685.099 567443.507 5937686.264 567432.779 5937681.762 
567433.538 5937679.955 567415.353 5937672.325 567418.938 5937663.64 
567419.705 5937661.78 567432.297 5937667.063 567438.475 5937652.392 
567438.319 5937651.648 567428.755 5937633.817 567427.969 5937632.352 
567424.532 5937630.787 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 567442.758 5937634.17 567453.455 5937654.108 
567452.176 5937661.63 567450.952 5937668.824 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GRZ>0.9</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567425.133 5937653.994</gml:lowerCorner>
          <gml:upperCorner>567425.133 5937653.994</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_5EF8DE81-046B-484D-9DC9-3D1CB8B2BB18" srsName="EPSG:25832">
          <gml:pos>567425.133 5937653.994</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.301 5937671.953</gml:lowerCorner>
          <gml:upperCorner>567428.301 5937671.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_0D871956-9B4E-425F-9A37-989FDAE670E4" srsName="EPSG:25832">
          <gml:pos>567428.301 5937671.953</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c34619af-a786-41f0-a899-8a67df7b6750">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.689 5937670.707</gml:lowerCorner>
          <gml:upperCorner>567434.689 5937670.707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_6ED00EA0-4CF0-42AB-A359-2B5F9F82F3B3" srsName="EPSG:25832">
          <gml:pos>567434.689 5937670.707</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">26.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7D2F9A5E-19DD-446C-AB2B-76F8B297C846" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567452.173 5937661.63 567450.952 5937668.824 567440.924 5937667.111 
567442.18 5937659.754 567452.173 5937661.63 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9036c79b-5e09-4030-8c15-e0d9877d084c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567445.543 5937665.568</gml:lowerCorner>
          <gml:upperCorner>567445.543 5937665.568</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:position>
        <gml:Point gml:id="Gml_8DD20087-EC1B-4C0D-B460-1DFBD9A21173" srsName="EPSG:25832">
          <gml:pos>567445.543 5937665.568</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fba093ee-e774-4846-b286-e46649619852">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567443.466 5937662.039</gml:lowerCorner>
          <gml:upperCorner>567443.466 5937662.039</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:position>
        <gml:Point gml:id="Gml_424F19D4-0213-4FF7-A6AA-AB0A0796451D" srsName="EPSG:25832">
          <gml:pos>567443.466 5937662.039</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_FDB38868-53F9-4C8B-93A0-98CC783B03C0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567416.408 5937613.826 567412.057 5937623.382 567405.121 5937620.224 
567409.472 5937610.668 567416.408 5937613.826 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9875e68a-2b21-40e4-8f66-ecf5c2290936">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E466C1A0-F5AC-4A8B-A11A-0CD37659F613" srsName="EPSG:25832">
          <gml:posList>567439.035 5937655.06 567438.79 5937652.388 567432.026 5937639.916 
567442.758 5937634.17 567453.455 5937654.108 567452.175 5937661.631 
567442.18 5937659.754 567439.277 5937659.277 567439.035 5937655.06 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_7b11be88-979b-483f-8042-82c51eff70af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1B7E349B-0C19-40CD-890D-15CADC1FBE48" srsName="EPSG:25832">
          <gml:posList>567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
567448.434 5937683.72 567443.507 5937686.264 567432.779 5937681.762 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_be2c01dd-4d15-4a6a-85c0-dc9565324486">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567431.741 5937675.181</gml:lowerCorner>
          <gml:upperCorner>567432.929 5937679.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_60C7A2C9-500F-4958-958D-4C4A3A46769F" srsName="EPSG:25832">
          <gml:posList>567432.929 5937675.181 567431.741 5937679.201 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_21c90a12-5886-40dd-8a86-463267b6e201">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_AB73AED6-062D-4C1C-B0EF-7A82D135B997" srsName="EPSG:25832">
          <gml:posList>567407.329 5937656.657 567419.705 5937661.78 567415.353 5937672.325 
567403 5937667.139 567407.329 5937656.657 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UnverbindlicheVormerkung gml:id="GML_cf3318a5-fc25-4f6b-a594-893928f6a3de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.825 5937635.581</gml:lowerCorner>
          <gml:upperCorner>567468.867 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_087CA0C1-235E-4B34-90E7-0E3D0FFE0185" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567468.867 5937663.607 567465.27 5937684.42 567462.07 5937698.959 
567458.005 5937697.252 567460.825 5937687.181 567456.625 5937685.781 
567452.825 5937636.781 567458.625 5937635.581 567459.825 5937647.581 
567467.425 5937647.381 567468.867 5937663.607 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:vormerkung>vorgesehene Bahnanlage</xplan:vormerkung>
    </xplan:BP_UnverbindlicheVormerkung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567457.938 5937683.621</gml:lowerCorner>
          <gml:upperCorner>567457.938 5937683.621</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>vormerkung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <xplan:schriftinhalt>vorgesehene Bahnanlage</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_61E8ABE7-5AFE-4608-B9AC-1F7AAB9D52A2" srsName="EPSG:25832">
          <gml:pos>567457.938 5937683.621</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">282.22</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_9a4506b9-78b2-4dc4-bb55-088a4c627872">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567394.606 5937621.126</gml:lowerCorner>
          <gml:upperCorner>567438.475 5937667.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_04E8666A-2284-4C1B-A0A1-4C4A9A539216" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567432.297 5937667.063 567419.705 5937661.78 567405.227 5937655.787 
567401.59 5937657.649 567394.606 5937655.401 567403.316 5937621.126 
567421.935 5937629.605 567424.532 5937630.787 567427.969 5937632.352 
567428.755 5937633.817 567438.319 5937651.648 567438.475 5937652.392 
567432.297 5937667.063 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567409.089 5937633.593</gml:lowerCorner>
          <gml:upperCorner>567409.089 5937633.593</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:position>
        <gml:Point gml:id="Gml_D8C312A2-BFCF-4B02-9BC9-5B5A904E2EA2" srsName="EPSG:25832">
          <gml:pos>567409.089 5937633.593</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567406.396 5937643.214</gml:lowerCorner>
          <gml:upperCorner>567406.396 5937643.214</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:schriftinhalt>Spielplatz</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_782D9605-CD1F-45B6-A0E0-157A05F42BDD" srsName="EPSG:25832">
          <gml:pos>567406.396 5937643.214</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_1D7CE55D-3038-4084-AD63-3BCBA690B719" srsName="EPSG:25832">
          <gml:posList>567409.472 5937610.668 567416.408 5937613.826 567412.057 5937623.382 
567405.121 5937620.224 567409.472 5937610.668 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_321bdb15-2400-4c44-a553-35f402f8743e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937655.401</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FA6A60A7-FE77-417C-9FE2-B0A7FEA86C90" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567418.938 5937663.64 567415.353 5937672.325 
567403 5937667.139 567395.687 5937664.07 567393.37 5937660.265 
567394.606 5937655.401 567401.59 5937657.649 567405.227 5937655.787 
567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_af37976e-6da1-4e93-a376-ad285c900b4c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.836 5937653.516</gml:lowerCorner>
          <gml:upperCorner>567399.836 5937653.516</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_A7F61BDB-0324-494E-9C53-262045300617" srsName="EPSG:25832">
          <gml:pos>567399.836 5937653.516</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.427 5937662.618</gml:lowerCorner>
          <gml:upperCorner>567416.427 5937662.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_DF4F0F53-D319-4D22-819C-F44EFA7A7BF6" srsName="EPSG:25832">
          <gml:pos>567416.427 5937662.618</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567410.188 5937663.997</gml:lowerCorner>
          <gml:upperCorner>567410.188 5937663.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_87DEAC66-8272-4DCB-B0A9-7965A67F8562" srsName="EPSG:25832">
          <gml:pos>567410.188 5937663.997</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_8376d624-8ccb-473d-81df-fc95b1ef28b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_B43C1960-6D40-4EB3-B16B-0848914DBA66" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567462.07 5937698.959 567446.882 5937692.583 567390.995 5937669.124 
567386.293 5937667.145 567389.479 5937654.684 567404.151 5937595.479 
567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_B5D4A01A-2E3C-4015-90D5-D27CFF57822D" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D0E1BF72-EEB6-4638-AAA0-F3830ACD1DC5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CFF8FA77-6D8B-41F7-ABE8-9CCA25DB3189" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_986862B1-807D-47B0-86D7-DD89F1F483CE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9B114842-8450-4774-B406-00DBB080855F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FD7591AC-1A3D-4394-851B-4339BDC1E6AE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_705EA066-4D1D-4FF7-9AA8-180D69FF081D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_575AD370-9F79-4E30-8ED2-17703B5152A5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_ACEF367F-F767-4913-8A26-AF98D027332B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F8500C33-2824-45A6-82C1-31BD2E061E53" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C621A8A1-BB7B-4973-9127-4DB3DC54996C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_37D82ACD-7B7C-4B4D-BEBD-5D52A6575A54" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B07838E9-8ECE-436E-B426-81064FB9D071" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D8E658D3-BA37-460A-B8C4-FE479E6E62A3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E4FF67AF-13DA-49E0-9FA1-1BB19B7C122F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_2D6CD438-0B68-4869-94E4-E30D3949253C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CCFA04F9-481E-41F6-930D-ED0450E65EBD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_156ED393-5698-49B7-904E-98EF428FF691" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0644178E-8817-4BC0-A022-59E64763023F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6AD7DF2F-892D-4491-8867-2ADCD9C8AE14" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567453.759 5937669.764</gml:lowerCorner>
          <gml:upperCorner>567453.759 5937669.764</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">6.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_A6A249A7-5A97-45A8-B33A-BEAF68ACB826" srsName="EPSG:25832">
          <gml:pos>567453.759 5937669.764</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BesondererNutzungszweckFlaeche gml:id="GML_9775f239-210a-4d61-ad8f-251f5a31296e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403.316 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567428.223 5937629.605</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_1F717778-4E7B-44D1-9EC3-777714B23118" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_04D9B3F6-DF8B-4EFA-9910-3BB6557264A0" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567407.834857796 5937607.72206805 567410.377 5937607.7 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_016C0243-CD9D-4A48-88FD-32B2E7704BFF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C91ED45A-6282-48C3-9C6A-FF8A6751F098" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567421.935 5937629.605 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BBFF0AC2-903D-472A-9FCB-C11026A5343B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567421.935 5937629.605 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4F8DBF5D-15C7-4AFE-88AB-C15B0843E6F5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567406.223 5937609.688 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:zweckbestimmung>Kiosk</xplan:zweckbestimmung>
    </xplan:BP_BesondererNutzungszweckFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567412.297 5937615.306</gml:lowerCorner>
          <gml:upperCorner>567412.297 5937615.306</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:position>
        <gml:Point gml:id="Gml_D8BB1C9F-64F7-4C03-A0A7-B78B204FAA42" srsName="EPSG:25832">
          <gml:pos>567412.297 5937615.306</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f44449a4-6628-4c84-9dfe-d82b4dafca06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.532 5937621.891</gml:lowerCorner>
          <gml:upperCorner>567416.532 5937621.891</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>rechtscharakter</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:position>
        <gml:Point gml:id="Gml_C6D5D13E-EABC-4BBD-B8CA-8BA9457B3D10" srsName="EPSG:25832">
          <gml:pos>567416.532 5937621.891</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_55177fec-7599-4785-a8e2-806742a9b2d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">22.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_2479480F-4460-4A89-82D4-64900F406F12" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567415.353 5937672.325 567403 5937667.139 
567407.329 5937656.657 567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_796aa811-807f-4550-abab-24b9cc8aa359">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.862 5937659</gml:lowerCorner>
          <gml:upperCorner>567421.862 5937659</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <xplan:position>
        <gml:Point gml:id="Gml_B0776E87-0841-402C-8297-2BFB84439CFF" srsName="EPSG:25832">
          <gml:pos>567421.862 5937659</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bff033ce-687f-48fe-b27c-542b545fb292">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_58D45166-892B-4797-B477-EBEBC74A2E76" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567448.434 5937683.72 567443.507 5937686.264 
567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_190f5132-7a3c-4e3f-900e-0705faba810a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.986 5937674.081</gml:lowerCorner>
          <gml:upperCorner>567439.986 5937674.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:position>
        <gml:Point gml:id="Gml_DD73C31C-0EFD-4E21-BC66-4162190A8F83" srsName="EPSG:25832">
          <gml:pos>567439.986 5937674.081</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.194 5937679.946</gml:lowerCorner>
          <gml:upperCorner>567440.194 5937679.946</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:position>
        <gml:Point gml:id="Gml_99E6D3D2-E7A0-481D-89F2-191D6A1D3955" srsName="EPSG:25832">
          <gml:pos>567440.194 5937679.946</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_5355860b-25fa-401c-a2ea-bb70654d17ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_3DEF01F1-6E62-4C96-8E85-020FFF4C5FB8" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e1568a2b-1768-4889-aedb-95449c029c25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">22.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BA318178-22DB-474F-868D-E96C2028EB0B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567437.993 5937669.342 567437.269 5937671.067 567433.538 5937679.955 
567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.339 5937668.137</gml:lowerCorner>
          <gml:upperCorner>567421.339 5937668.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
      <xplan:position>
        <gml:Point gml:id="Gml_9F0EE1DC-C44D-4855-9E93-6D3ADC886262" srsName="EPSG:25832">
          <gml:pos>567421.339 5937668.137</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_de628759-edb3-4f31-9d43-7d4f816bfb0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.484 5937661.969</gml:lowerCorner>
          <gml:upperCorner>567427.781 5937668.164</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_06C27EFC-4B5A-40EE-BC31-C6178675A3EF" srsName="EPSG:25832">
          <gml:posList>567426.484 5937661.969 567427.781 5937668.164 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_82075a17-ae02-4228-a4ac-debedfd66c1e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.794 5937658.752</gml:lowerCorner>
          <gml:upperCorner>567417.901 5937660.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6FD2C6F5-1707-4B9A-B066-F517A208A4D8" srsName="EPSG:25832">
          <gml:posList>567417.901 5937658.752 567411.794 5937660.897 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_a435c756-f459-41ce-b27c-d5992aa8f87e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.065 5937655.343</gml:lowerCorner>
          <gml:upperCorner>567437.337 5937661.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_5EAEE2DB-BC22-463E-9A8E-07527A9B2847" srsName="EPSG:25832">
          <gml:posList>567434.065 5937655.343 567437.337 5937661.831 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_7d7588c9-1211-471e-ad15-8dd9f6605701">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567400.372 5937656.059</gml:lowerCorner>
          <gml:upperCorner>567401.041 5937660.34</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_7C087CAD-73D8-4FFF-8F80-8D191B4C4320" srsName="EPSG:25832">
          <gml:posList>567401.041 5937656.059 567400.372 5937660.34 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
</xplan:XPlanAuszug>