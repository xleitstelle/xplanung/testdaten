﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_D22F89B8-B9F0-40BE-8360-4D2143F1070A" xsi:schemaLocation="http://www.xplanung.de/xplangml/5/1 http://www.xplanungwiki.de/upload/XPlanGML/5.1/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/5/1">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>566878.799 5928556.513</gml:lowerCorner>
      <gml:upperCorner>567066.074 5929011.434</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_443c3ad5-4b47-4a31-92c0-c5301ba795d2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566878.799 5928556.513</gml:lowerCorner>
          <gml:upperCorner>567066.074 5929011.434</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan002_5-1</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan BPlan002_5-1 für den Bereich
zwischen Bahntrasse und Korallusstraße (Bezirk Hamburg-
Mitte, Ortsteil 136) besteht aus den Teilflächen „A“ und „B“
und wird festgestellt.
Die Teilfläche „A“ des Plangebiets wird wie folgt begrenzt:
Thielenstraße – Westgrenzen der Flurstücke 7920 und 12289,
Südgrenzen der Flurstücke 12288 und 1224 (Parallelstraße),
über das Flurstück 1224, Westgrenze des Flurstücks 1224
(Parallelstraße), über das Flurstück 1224, Nordgrenze des
Flurstücks 12287, über das Flurstück 6853 (Korallusstraße),
Ostgrenzen der Flurstücke 13252, 13253 und 13257, Südgrenze
des Flurstücks 13257, Ostgrenzen der Flurstücke 12189 und
13321, Nordgrenze des Flurstücks 13324, Ostgrenzen der Flurstücke
13324 und 13326, Nordgrenzen der Flurstücke 12296
und 12295, Westgrenze des Flurstücks 13332, West- und Nordgrenze
des Flurstücks 12119, über das Flurstück 6853 (Korallusstraße)
der Gemarkung Wilhelmsburg.
Die Teilfläche „B“ des Plangebiets wird wie folgt begrenzt:
Westgrenze des Flurstücks 12334, Nordwestgrenzen der Flurstücke
12334 und 13150, Ostgrenze des Flurstücks 13150,
Südgrenzen der Flurstücke 13150 und 12334 der Gemarkung
Wilhelmsburg.</xplan:beschreibung>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:MultiSurface gml:id="Gml_AB442DEE-240F-46C7-90F3-D3B13B196FD5" srsName="EPSG:25832">
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_045E3A4C-2950-469C-81B6-3B68A0A6F671" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">566989.995 5928819.309 566991.874 5928835.45 566991.98 5928836.361 
566991.949 5928843.372 566973.25 5928844.271 566944.741 5928845.642 
566930.642 5928846.32 566930.846 5928850.794 566906.416 5928851.969 
566902.012 5928816.787 566902.856 5928816.553 566902.574 5928814.255 
566901.609 5928814.263 566892.384 5928754.204 566887.713 5928720.433 
566885.967 5928707.672 566885.457 5928703.927 566882.926 5928685.455 
566881.71 5928676.568 566880.008 5928663.883 566878.799 5928656.69 
566884.66 5928655.995 566895.6 5928654.6 566904.351 5928653.486 
566901.331 5928629.555 566900.362 5928621.875 566899.327 5928613.667 
566898.531 5928607.364 566896.791 5928593.672 567055.545 5928557.101 
567061.825 5928556.513 567064.294 5928580.853 567065.107 5928589.831 
567065.46 5928594.606 567065.73 5928599.027 567065.968 5928602.918 
567066.023 5928606.369 567066.074 5928609.776 567065.872 5928614.249 
567065.672 5928618.433 567065.313 5928622.817 567064.944 5928628.24 
567064.461 5928631.632 567063.773 5928635.263 567057.847 5928634.303 
567054.65 5928633.366 567051.538 5928633.522 567048.481 5928634.285 
567045.46 5928635.342 567042.911 5928637.237 567040.48 5928639.274 
567038.662 5928641.832 567037.096 5928627.726 567035.641 5928614.614 
566986.4 5928620.631 566978.81 5928621.558 566973.083 5928619.447 
566974.89 5928625.319 566977.994 5928651.425 566981.413 5928682.684 
566971.403 5928683.841 566979.782 5928753.468 567000.508 5928751.068 
567004.059 5928782.148 566985.912 5928784.23 566989.995 5928819.309 
</gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_04C70239-A206-45F9-B5E6-CF2B1F347999" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">566984.153 5929000.331 566971.214 5929011.434 566955.549 5928999.355 
566936.961 5928985.021 566934.561 5928932.363 566950.718 5928930.642 
566973.755 5928928.189 566977.742 5928927.764 566984.153 5929000.331 
</gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_460eb6e5-a1fc-420f-b78a-12002ba09d82" />
      <xplan:texte xlink:href="#GML_cf5e49ce-9e68-4596-861d-25f91f294cf1" />
      <xplan:texte xlink:href="#GML_ac3f5786-7676-49c1-9bfb-619b6e96153a" />
      <xplan:texte xlink:href="#GML_033c66c2-9bdf-477e-9115-e4fa7acf7483" />
      <xplan:texte xlink:href="#GML_ce5d4b3b-1f77-4649-87d3-d8e2aae0c0f9" />
      <xplan:texte xlink:href="#GML_864f30d7-31e2-480b-a8be-90f30ac58579" />
      <xplan:texte xlink:href="#GML_e088f923-cc26-4084-a05e-29831ae23397" />
      <xplan:texte xlink:href="#GML_b1deae92-3045-416a-9101-bc9990957e76" />
      <xplan:texte xlink:href="#GML_2baf0b93-f279-405c-8a93-0f039968bf23" />
      <xplan:texte xlink:href="#GML_ffb8185b-ff88-4d39-85c5-23d840ed2702" />
      <xplan:texte xlink:href="#GML_10f1bbc4-6090-4a0f-ac37-4a948a466972" />
      <xplan:texte xlink:href="#GML_fbbe13d6-5b10-4bc1-96c2-e030e42767c3" />
      <xplan:texte xlink:href="#GML_64060638-90c4-4dd9-8e64-787a46e192be" />
      <xplan:texte xlink:href="#GML_6dc70ef2-66fc-4bf1-9e20-a8b2fd6b0e9b" />
      <xplan:texte xlink:href="#GML_73c41f4a-8d57-4239-b1d2-d440315b55ea" />
      <xplan:texte xlink:href="#GML_8839799e-5e0a-4a2e-a88b-c6e18047a0d4" />
      <xplan:texte xlink:href="#GML_90bd7307-fe86-44ce-bb2f-9bc6cbd2ed95" />
      <xplan:texte xlink:href="#GML_2100a7ca-a80f-469c-92a9-b489692f93ce" />
      <xplan:texte xlink:href="#GML_d1eb639d-bae0-435d-a53e-5f43b6242d37" />
      <xplan:texte xlink:href="#GML_d6bdeec1-3419-4451-adb2-21aa355c5785" />
      <xplan:texte xlink:href="#GML_d9a9455e-da7e-468b-94d7-9985b9f1cfb7" />
      <xplan:texte xlink:href="#GML_e7713d5e-1b95-4b15-abf8-628ec4d14bad" />
      <xplan:texte xlink:href="#GML_f40af8dd-3f2c-4531-851c-add7fb18f336" />
      <xplan:texte xlink:href="#GML_e0ab737d-782e-4b19-94a1-d1b1716dc9ca" />
      <xplan:texte xlink:href="#GML_0272306f-1eb9-4f65-b515-6eb7857883a8" />
      <xplan:texte xlink:href="#GML_eca1dcff-a5d5-486b-8b60-a4c14159518b" />
      <xplan:texte xlink:href="#GML_788057ec-8264-473f-905b-bc1c6ec71f77" />
      <xplan:texte xlink:href="#GML_11776302-234f-4422-9f4c-a5e9252c889a" />
      <xplan:texte xlink:href="#GML_360ccf4b-fcff-41e9-830f-98efc562c45d" />
      <xplan:texte xlink:href="#GML_eb4982a4-8e29-49fa-9c6b-856b261ccb5a" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>136</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2019-12-02</xplan:rechtsverordnungsDatum>
      <xplan:staedtebaulicherVertrag>true</xplan:staedtebaulicherVertrag>
      <xplan:versionBauNVOText>Version vom 21.11.2017</xplan:versionBauNVOText>
      <xplan:bereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_460eb6e5-a1fc-420f-b78a-12002ba09d82">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>Die Aufnahme einer Wohnnutzung im allgemeinen Wohngebiet ist erst zulässig, wenn die im Planbild mit „(A)“ bezeichnete Bebauung errichtet ist.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_cf5e49ce-9e68-4596-861d-25f91f294cf1">
      <xplan:schluessel>§2 Nr.25</xplan:schluessel>
      <xplan:text>Auf den privaten Grundstücksflächen sind Geh- und Fahrwege, Terrassen sowie Platzflächen in wasser- und luftdurchlässigem Aufbau herzustellen. Feuerwehrumfahrten und -aufstellflächen auf zu begrünenden Flächen sind in vegetationsfähigem Aufbau (Schotterrasen) herzustellen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ac3f5786-7676-49c1-9bfb-619b6e96153a">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Überschreitungen der Baugrenzen durch Balkone und Loggien auf je einem Drittel der Fassadenlänge bis zu 2 m und durch zum Hauptgebäude zugehörige Terrassen bis zu 4 m zulässig. Balkone und Loggien, die in den öffentlichen Straßenraum ragen, sind nur oberhalb einer lichten Höhe von 3,5 m zulässig. Ausgenommen von der Beschränkung auf je ein Drittel der Fassadenlänge sind Balkone, die die Baugrenze des untersten Geschosses nicht überschreiten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_033c66c2-9bdf-477e-9115-e4fa7acf7483">
      <xplan:schluessel>§2 Nr.26.3</xplan:schluessel>
      <xplan:text>Die Gewässerböschungen sowie die übrigen Flächen außerhalb der Gehölzanpflanzungen sind als artenreiche, standorttypische Gräser-Stauden-Fluren zu entwickeln und dürfen höchstens einmal im Jahr in der Zeit von Mitte September bis Ende Oktober gemäht werden, wobei das Mähgut abzuräumen ist.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ce5d4b3b-1f77-4649-87d3-d8e2aae0c0f9">
      <xplan:schluessel>§2 Nr.26.2</xplan:schluessel>
      <xplan:text>Auf der gesamten Fläche für Maßnahmen zum Schutz, zur Pflege und zur Entwicklung von Boden, Natur und Landschaft sind mindestens 15 kleinkronige und zehn großkronige Bäume, mindestens 500 m² Strauchflächen sowie als Ersatzlebensraum für die Dorn- und Gartengrasmücke zwei dichtwachsende dornenbewehrte Gebüsche mit jeweils mindestens 5 m Breite und 10 m Länge anzupflanzen. Für die festgesetzten Anpflanzungen sind insgesamt mindestens 15 unterschiedliche Laubgehölzarten zu verwenden. Für die Strauchflächen sind je 1,5 m² mindestens eine Pflanze und für die dornenbewehrten Gebüsche je 1 m² mindestens zwei Pflanzen zu verwenden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_864f30d7-31e2-480b-a8be-90f30ac58579">
      <xplan:schluessel>§2 Nr.20</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet ist für je 150 m² der zu begrünenden Grundstücksfläche mindestens ein kleinkroniger Baum oder für je 300 m² der zu begrünenden Grundstücksfläche mindestens ein großkroniger Baum zu pflanzen. Als Vegetationsflächen hergerichtete unterbaute Flächen sind dabei mitzurechnen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e088f923-cc26-4084-a05e-29831ae23397">
      <xplan:schluessel>§2 Nr.19</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind mindestens 25 v. H. der Grundstücksflächen als Vegetationsflächen herzurichten. Als Vegetationsflächen hergerichtete unterbaute Flächen sind dabei mitzurechnen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_b1deae92-3045-416a-9101-bc9990957e76">
      <xplan:schluessel>§2 Nr.26</xplan:schluessel>
      <xplan:text>Die festgesetzte Fläche für Maßnahmen zum Schutz, zur Pflege und zur Entwicklung von Boden, Natur und Landschaft ist mindestens 1,20 m hoch einzuzäunen und wie folgt herzurichten:</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2baf0b93-f279-405c-8a93-0f039968bf23">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet ist eine Aufhöhung des Geländes zulässig. Die als Höchstmaß zulässigen Geländehöhen ergeben sich aus den in der Nebenzeichnung festgesetzten Geländehöhen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_ffb8185b-ff88-4d39-85c5-23d840ed2702">
      <xplan:schluessel>§2 Nr.24</xplan:schluessel>
      <xplan:text>Bauliche und technische Maßnahmen, die zu einer dauerhaften Absenkung des Grundwasserspiegels führen, sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_10f1bbc4-6090-4a0f-ac37-4a948a466972">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet ist eine Überschreitung der festgesetzten Gebäudehöhe durch Dach- und Technikaufbauten sowie Brüstungen und Einhausungen um 1,5 m zulässig. Dach- und Technikaufbauten mit Ausnahme von Solaranlagen und Windsegeln sind zusammenzufassen und auf maximal 20 vom Hundert (v. H.) zusammenhängende Dachfläche eines Gebäudes begrenzt anzuordnen und einzuhausen oder durch eine allseitige Attika zu verdecken.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_fbbe13d6-5b10-4bc1-96c2-e030e42767c3">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Das festgesetzte Gehrecht umfasst die Befugnis der Freien und Hansestadt Hamburg, einen allgemein zugänglichen Gehweg anzulegen und zu unterhalten. Geringfügige Abweichungen von dem festgesetzten Gehrecht können zugelassen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_64060638-90c4-4dd9-8e64-787a46e192be">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind innerhalb der mit „(1)“ bezeichneten Flächen die der Versorgung des Gebiets dienenden Läden und nicht störende Handwerksbetriebe unzulässig. Ausnahmen für sonstige nicht störende Gewerbebetriebe und Anlagen für Verwaltungen werden innerhalb der mit „(1)“ bezeichneten Flächen ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_6dc70ef2-66fc-4bf1-9e20-a8b2fd6b0e9b">
      <xplan:schluessel>§2 Nr.27</xplan:schluessel>
      <xplan:text>Im Plangebiet sind an geeigneten Großbäumen drei künstliche Nisthilfen für Halbhöhlenbrüter und drei künstliche Nisthilfen für Höhlenbrüter für die Arten Feldsperling, Gartenrotschwanz und Grauschnäpper in mindestens 3 m Höhe anzubringen und zu unterhalten. Im allgemeinen Wohngebiet sind an den nach Süden oder Südosten ausgerichteten Wänden der Neubebauung insgesamt drei Gruppen mit je zwei Fledermausflachkästen für Gebäude bewohnende Fledermausarten anzubringen oder in die Gebäudefassade zu integrieren und zu unterhalten. Die exakten Anbringungsorte sind durch fachliche Begleitung festzulegen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_73c41f4a-8d57-4239-b1d2-d440315b55ea">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet ist für einen Außenbereich einer Wohnung entweder durch Orientierung an lärmabgewandten Seiten oder durch bauliche Schallschutzmaßnahmen wie zum Beispiel verglaste Vorbauten sicherzustellen, dass durch diese baulichen Maßnahmen insgesamt eine Schallpegelminderung erreicht wird, die es ermöglicht, dass in dem der Wohnung zugehörigen Außenbereich ein Tagpegel von kleiner 65 dB(A) erreicht wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_8839799e-5e0a-4a2e-a88b-c6e18047a0d4">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet ist der Erschütterungsschutz der Gebäude durch bauliche oder technische Maßnahmen (zum Beispiel an Wänden, Decken und Fundamenten) so sicherzustellen, dass die Anhaltswerte der DIN 4150 (Erschütterungen im Bauwesen), Teil 2 (Einwirkungen auf Menschen in Gebäuden), Tabelle 1, Zeile 3 (Mischgebiete nach BauNVO) eingehalten werden. Zusätzlich ist durch die baulichen und technischen Maßnahmen zu gewährleisten, dass der sekundäre Luftschall die Immissionsrichtwerte der Technischen Anleitung zum Schutz gegen Lärm vom 26. August 1998 (Gemeinsames Ministerialblatt S. 503), geändert am 1. Juni 2017 (BAnz. AT 08.06.17 B5), Nummer 6.2, nicht überschreitet. Einsichtnahmestelle der DIN 4150: Freie und Hansestadt Hamburg, Behörde für Umwelt und Energie, Amt für Immissionsschutz und Betriebe, Bezugsquelle der DIN 4150: Beuth Verlag GmbH, Berlin.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_90bd7307-fe86-44ce-bb2f-9bc6cbd2ed95">
      <xplan:schluessel>§2 Nr.21</xplan:schluessel>
      <xplan:text>Nicht überbaute Garagen und Tiefgaragen sind mit einem mindestens 50 cm starken durchwurzelbaren Substrataufbau zu versehen und zu begrünen. Hiervon ausgenommen sind die erforderlichen befestigten Flächen für Terrassen, Wege, Freitreppen, Platz- und Kinderspielflächen. Für Bäume auf Garagen oder Tiefgaragen muss auf einer Fläche von mindestens 12 m² je Baum die Schichtstärke des durchwurzelbaren Substrataufbaus für kleinkronige Bäume mindestens 70 cm und für großkronige Bäume mindestens 1 m betragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_2100a7ca-a80f-469c-92a9-b489692f93ce">
      <xplan:schluessel>§2 Nr.22</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die Dachflächen der festgesetzten Gebäude mindestens zu 50 v. H. der jeweiligen Gebäudegrundfläche mit einem mindestens 8 cm starken durchwurzelbaren Substrataufbau zu versehen und extensiv zu begrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d1eb639d-bae0-435d-a53e-5f43b6242d37">
      <xplan:schluessel>§2 Nr.17</xplan:schluessel>
      <xplan:text>Im Plangebiet sind bauliche Maßnahmen vorzusehen, die Gasansammlungen unter den baulichen Anlagen und den befestigten Flächen sowie Gaseintritte in die baulichen Anlagen durch Bodengase verhindern.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d6bdeec1-3419-4451-adb2-21aa355c5785">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet ist das Überschreiten der festgesetzten Grundflächenzahl (GRZ) von 0,5 für in § 19 Absatz 4 Satz 1 der Baunutzungsverordnung bezeichnete Anlagen bis zu einer GRZ von 0,9 zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_d9a9455e-da7e-468b-94d7-9985b9f1cfb7">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>Im Plangebiet sind für festgesetzte Baum- und Strauchpflanzungen standortgerechte einheimische Gehölze zu verwenden und zu erhalten. Anzupflanzende Bäume müssen einen Stammumfang von mindestens 18 cm, in 1 m Höhe über dem Erdboden gemessen, aufweisen. Im Kronenbereich jedes Baumes ist eine offene Vegetationsfläche von mindestens 12 m² anzulegen und zu begrünen. Anzupflanzende Sträucher müssen mindestens folgende Qualität aufweisen: Zwei mal verpflanzt, Höhe mindestens 60 cm. Bei Abgang sind Ersatzpflanzungen so vorzunehmen, dass der jeweilige Charakter und Umfang der Gehölzpflanzung erhalten bleiben.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e7713d5e-1b95-4b15-abf8-628ec4d14bad">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Stellplätze sind nur in Garagen und Tiefgaragen zulässig. Garagen und Tiefgaragen sind nur innerhalb der Flächen für Garagen und Tiefgaragen sowie innerhalb der überbaubaren Grundstücksflächen zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_f40af8dd-3f2c-4531-851c-add7fb18f336">
      <xplan:schluessel>§2 Nr.23</xplan:schluessel>
      <xplan:text>Außenwände von Gebäuden, deren Fensterabstand mehr als 5 m beträgt, sowie fensterlose Fassaden sind mit Schling- oder Kletterpflanzen zu begrünen; je 2 m Wandlänge ist mindestens eine Pflanze zu verwenden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_e0ab737d-782e-4b19-94a1-d1b1716dc9ca">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>Innerhalb der mit „(A)“ bezeichneten überbaubaren Flächen sind die Schlafräume den lärmabgewandten Seiten zuzuordnen. Wird an Gebäudeseiten ein Pegel von
70 dB(A) am Tag erreicht oder überschritten, sind vor den Fenstern der zu dieser Gebäudeseite orientierten Wohnräume bauliche Schallschutzmaßnahmen in Form von verglasten Vorbauten oder vergleichbare Maßnahmen vorzusehen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_0272306f-1eb9-4f65-b515-6eb7857883a8">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Die festgesetzten Geh- und Fahrrechte umfassen die Befugnis des Eigentümers des Flurstücks 12120 der Gemarkung Wilhelmsburg, für das Flurstück jeweils an die Thielenstraße und die Korallusstraße eine Zu- und Abfahrt anzulegen und zu unterhalten. Geringfügige Abweichungen von den festgesetzten Geh- und Fahrrechten können zugelassen werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_eca1dcff-a5d5-486b-8b60-a4c14159518b">
      <xplan:schluessel>§2 Nr.26.1</xplan:schluessel>
      <xplan:text>Auf der als private Fläche für die Regelung des Wasserabflusses festgesetzten Teilfläche ist für die Sammlung und Rückhaltung von Niederschlagswasser sowie als Ersatz für ein gesetzlich geschütztes Kleingewässerbiotop und als Ersatzlebensraum für die Teichralle und die Stockente ein dauerhaft wasserführendes naturnahes Kleingewässer mit einer Fläche von mindestens 1 000 m², einer Wassertiefe von mindestens 1,5 m im Mittelbereich, wechselnden Böschungsneigungen sowie mit Initialbepflanzungen aus standortheimischen Sumpfpflanzen in der Wasserwechselzone anzulegen und dauerhaft zu erhalten.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_788057ec-8264-473f-905b-bc1c6ec71f77">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Werbeanlagen sind an Fassaden nur an der Stätte der Leistung unterhalb der Fenster des zweiten Vollgeschosses zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_11776302-234f-4422-9f4c-a5e9252c889a">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet werden Ausnahmen für Betriebe des Beherbergungsgewerbes, Gartenbaubetriebe und Tankstellen nach § 4 Absatz 3 Nummern 1, 4 und 5 der Baunutzungsverordnung in der Fassung vom
21. November 2017 (BGBl. I S. 3787) ausgeschlossen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_360ccf4b-fcff-41e9-830f-98efc562c45d">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind innerhalb der mit „(2)“ bezeichneten Flächen die der Versorgung des Gebiets dienenden Läden nur in den Erdgeschossen zulässig. Sonstige nicht störende Gewerbebetriebe und Anlagen für Verwaltungen sind innerhalb der mit „(2)“ bezeichneten Flächen allgemein zulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnitt gml:id="GML_eb4982a4-8e29-49fa-9c6b-856b261ccb5a">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet ist durch geeignete bauliche Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden, verglaste Vorbauten (zum Beispiel verglaste Loggien, Wintergärten), besondere Fensterkonstruktionen oder in ihrer Wirkung vergleichbare Maßnahmen sicherzustellen, dass durch diese baulichen Maßnahmen insgesamt eine Schallpegeldifferenz erreicht wird, die es ermöglicht, dass in Schlafräumen ein Innenraumpegel bei teilgeöffneten Fenster von 30 dB(A) während der Nachtzeit nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme in Form von verglasten Vorbauten, muss dieser Innenraumpegel bei teilgeöffneten Bauteilen erreicht werden. Wohn-/Schlafräume in Einzimmerwohnungen und Kinderzimmer sind wie Schlafräume zu beurteilen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:BP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566878.799 5928556.513</gml:lowerCorner>
          <gml:upperCorner>567066.074 5929011.434</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:MultiSurface gml:id="Gml_E7B2C71A-C561-43F6-9186-1674167B944C" srsName="EPSG:25832">
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_8469BE20-4311-43B2-994E-1A9A0AB63E02" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">566989.995 5928819.309 566991.874 5928835.45 566991.98 5928836.361 
566991.949 5928843.372 566973.25 5928844.271 566944.741 5928845.642 
566930.642 5928846.32 566930.846 5928850.794 566906.416 5928851.969 
566902.012 5928816.787 566902.856 5928816.553 566902.574 5928814.255 
566901.609 5928814.263 566892.384 5928754.204 566887.713 5928720.433 
566885.967 5928707.672 566885.457 5928703.927 566882.926 5928685.455 
566881.71 5928676.568 566880.008 5928663.883 566878.799 5928656.69 
566884.66 5928655.995 566895.6 5928654.6 566904.351 5928653.486 
566901.331 5928629.555 566900.362 5928621.875 566899.327 5928613.667 
566898.531 5928607.364 566896.791 5928593.672 567055.545 5928557.101 
567061.825 5928556.513 567064.294 5928580.853 567065.107 5928589.831 
567065.46 5928594.606 567065.73 5928599.027 567065.968 5928602.918 
567066.023 5928606.369 567066.074 5928609.776 567065.872 5928614.249 
567065.672 5928618.433 567065.313 5928622.817 567064.944 5928628.24 
567064.461 5928631.632 567063.773 5928635.263 567057.847 5928634.303 
567054.65 5928633.366 567051.538 5928633.522 567048.481 5928634.285 
567045.46 5928635.342 567042.911 5928637.237 567040.48 5928639.274 
567038.662 5928641.832 567037.096 5928627.726 567035.641 5928614.614 
566986.4 5928620.631 566978.81 5928621.558 566973.083 5928619.447 
566974.89 5928625.319 566977.994 5928651.425 566981.413 5928682.684 
566971.403 5928683.841 566979.782 5928753.468 567000.508 5928751.068 
567004.059 5928782.148 566985.912 5928784.23 566989.995 5928819.309 
</gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
          <gml:surfaceMember>
            <gml:Polygon gml:id="Gml_F1766F14-9916-433A-B29F-6EF0966ABBEA" srsName="EPSG:25832">
              <gml:exterior>
                <gml:LinearRing>
                  <gml:posList srsName="EPSG:25832">566984.153 5929000.331 566971.214 5929011.434 566955.549 5928999.355 
566936.961 5928985.021 566934.561 5928932.363 566950.718 5928930.642 
566973.755 5928928.189 566977.742 5928927.764 566984.153 5929000.331 
</gml:posList>
                </gml:LinearRing>
              </gml:exterior>
            </gml:Polygon>
          </gml:surfaceMember>
        </gml:MultiSurface>
      </xplan:geltungsbereich>
	  <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan002_5-1TeilA.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan002_5-1TeilA.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan002_5-1TeilB.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan002_5-1TeilB.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
      <xplan:planinhalt xlink:href="#GML_b8afcd6b-4f8b-4b10-89d7-9760fa6f7ca1" />
      <xplan:planinhalt xlink:href="#GML_bc2af5b6-4e50-4378-a3e1-38056b69f0ff" />
      <xplan:planinhalt xlink:href="#GML_cf45ec68-2391-4bcd-92a6-ffbe3e37639f" />
      <xplan:planinhalt xlink:href="#GML_3d7fed22-6e30-444e-9669-ab605aaaeca8" />
      <xplan:planinhalt xlink:href="#GML_131f97cc-0eac-4442-9fa5-d34b844b0673" />
      <xplan:planinhalt xlink:href="#GML_60f2804a-f211-4bff-b725-a60c898c1d9d" />
      <xplan:planinhalt xlink:href="#GML_657b776e-647e-40e0-9e01-9e12d2a2d4d6" />
      <xplan:planinhalt xlink:href="#GML_9bd2cb90-23c9-416c-ab7e-73235aaf86c2" />
      <xplan:planinhalt xlink:href="#GML_d304d680-9e91-4922-8a2a-bc06f90ab2a1" />
      <xplan:planinhalt xlink:href="#GML_7766968d-ac1e-4f13-88f5-a228f7f96316" />
      <xplan:planinhalt xlink:href="#GML_33c4107a-0e08-45b1-aefa-824aea8e0f6c" />
      <xplan:planinhalt xlink:href="#GML_e5ab0e24-8b45-4963-b2af-5fcc75d586bf" />
      <xplan:planinhalt xlink:href="#GML_7f7cef32-430d-4cdf-8bc7-68f138409141" />
      <xplan:planinhalt xlink:href="#GML_bf8af583-199a-49f2-a4b5-0be3096bde4b" />
      <xplan:planinhalt xlink:href="#GML_828c0102-4fef-4a6a-b8b0-ede795f90741" />
      <xplan:planinhalt xlink:href="#GML_5c301b2c-9587-4996-a0f6-eded200050e4" />
      <xplan:planinhalt xlink:href="#GML_c42bc820-6d19-4e09-9090-43e019eadbf0" />
      <xplan:planinhalt xlink:href="#GML_2034e12e-a85e-4064-8d63-29b5a28c6ced" />
      <xplan:planinhalt xlink:href="#GML_18078e7c-abf5-4f54-9c7a-47df29410333" />
      <xplan:planinhalt xlink:href="#GML_5b8c2dcd-2f47-4a89-b4ec-4e9848bf50c1" />
      <xplan:planinhalt xlink:href="#GML_9c8edf84-80dc-40f5-a256-db5f3eff529b" />
      <xplan:planinhalt xlink:href="#GML_40e2cc4c-3e65-4e43-8bff-0bba9dbafb25" />
      <xplan:planinhalt xlink:href="#GML_e261d60e-a41f-4fa2-ab0e-515b1a084a8b" />
      <xplan:planinhalt xlink:href="#GML_1ef2650e-7c6e-4acf-8fcc-faef2ed42761" />
      <xplan:planinhalt xlink:href="#GML_969a90ce-82eb-4de1-884b-a463ba3fb1d7" />
      <xplan:planinhalt xlink:href="#GML_b960b890-a89c-450c-8cc0-8980f70cd275" />
      <xplan:planinhalt xlink:href="#GML_f4aec1d4-ef45-46bd-bde9-3cc20b3cb224" />
      <xplan:planinhalt xlink:href="#GML_a86495d4-e31f-4801-834b-32b68ce227a2" />
      <xplan:planinhalt xlink:href="#GML_245062d3-054e-4354-a45e-01b0e5414780" />
      <xplan:planinhalt xlink:href="#GML_f3b35e0d-48ca-406f-9cee-8e8d17e5c148" />
      <xplan:planinhalt xlink:href="#GML_eaf477b8-ab9d-48a6-9870-3c4521ada6dc" />
      <xplan:planinhalt xlink:href="#GML_c782b8a5-281d-429b-b8ed-46fc203cd1c0" />
      <xplan:planinhalt xlink:href="#GML_f04bb30b-0945-418d-8adb-648fc101e6b3" />
      <xplan:planinhalt xlink:href="#GML_4cbd12b1-a8e7-4487-832d-378f36217887" />
      <xplan:planinhalt xlink:href="#GML_9b264a2a-e531-4c4d-a73f-f9fa5346ac0c" />
      <xplan:planinhalt xlink:href="#GML_a0967d34-3528-4271-907a-23bf5d8556ce" />
      <xplan:planinhalt xlink:href="#GML_a21dba91-7d5b-4cfb-927e-f15d29154587" />
      <xplan:planinhalt xlink:href="#GML_afb4a59f-bfb1-4388-b381-f3c763fe5f97" />
      <xplan:planinhalt xlink:href="#GML_7000ab9c-4e86-4fe0-9fa2-be8ae1227a75" />
      <xplan:planinhalt xlink:href="#GML_0b23f08b-8481-45b0-8652-9686874c6a5e" />
      <xplan:planinhalt xlink:href="#GML_50fadcf8-16f2-4331-b241-92bb254d09a6" />
      <xplan:planinhalt xlink:href="#GML_cfda5888-7df1-4600-ae9f-391bf2d9d0fe" />
      <xplan:planinhalt xlink:href="#GML_bf31bfb7-10d7-4baf-9ece-ffca4e13e789" />
      <xplan:planinhalt xlink:href="#GML_ac8c6d30-6f6f-4712-8783-84a1a8cd8742" />
      <xplan:planinhalt xlink:href="#GML_3a7ef67e-271e-4050-b92a-a007914381b0" />
      <xplan:planinhalt xlink:href="#GML_7375a1d7-2dd7-448b-83a9-ee7e642ab442" />
      <xplan:planinhalt xlink:href="#GML_49a5a716-9059-458d-bc62-404aed47710a" />
      <xplan:planinhalt xlink:href="#GML_4910f238-faca-446f-94f1-e0c2bc33e30c" />
      <xplan:planinhalt xlink:href="#GML_11d34213-60fa-4f45-926c-81362faebbb8" />
      <xplan:planinhalt xlink:href="#GML_ffa05cf8-6ad1-4cbb-8306-aabae383f865" />
      <xplan:planinhalt xlink:href="#GML_40cd9bfb-4670-41f0-af09-badb6cdb3ebb" />
      <xplan:planinhalt xlink:href="#GML_03c16333-1596-49d0-bb2a-8dbd105fccdb" />
      <xplan:planinhalt xlink:href="#GML_7cf9abe5-518d-4ea5-9216-c05116082abe" />
      <xplan:planinhalt xlink:href="#GML_72b8fcde-50cf-443f-915d-6931542f01c3" />
      <xplan:planinhalt xlink:href="#GML_af984b81-84e4-4be0-8511-9604089ff7b2" />
      <xplan:planinhalt xlink:href="#GML_d789a998-4a33-4c4b-8b03-3c992b063d13" />
      <xplan:planinhalt xlink:href="#GML_58714239-2266-45d0-bcfb-f94bf0bef6d1" />
      <xplan:planinhalt xlink:href="#GML_82c6ec87-d913-4b9d-bf22-c0acc96d3c64" />
      <xplan:planinhalt xlink:href="#GML_4e70711b-3273-4c69-8995-72bfc714b950" />
      <xplan:planinhalt xlink:href="#GML_4ec32bf4-26e7-42a3-a59b-ec2445258328" />
      <xplan:planinhalt xlink:href="#GML_d3385c6e-88a2-458e-89cf-7ef8bf7f5958" />
      <xplan:planinhalt xlink:href="#GML_01b7ba87-01fc-4591-bfcd-0a21bae148d3" />
      <xplan:planinhalt xlink:href="#GML_ceaa274c-e83e-4ed9-9786-d62325d95626" />
      <xplan:planinhalt xlink:href="#GML_564f36e0-385a-4d23-865f-75ff1aed0b56" />
      <xplan:planinhalt xlink:href="#GML_79e5aab9-6a2b-4697-811e-7b1ae912fdaf" />
      <xplan:planinhalt xlink:href="#GML_67612504-5754-4c90-b521-17cea9fab62e" />
      <xplan:planinhalt xlink:href="#GML_4a787277-f3ee-440b-b16a-600cb5419d26" />
      <xplan:planinhalt xlink:href="#GML_4a7df185-6df5-474f-98fa-ae74b8f0d637" />
      <xplan:planinhalt xlink:href="#GML_599e8034-059f-4f61-8772-25e8ce204fc9" />
      <xplan:planinhalt xlink:href="#GML_67104753-3c83-4dbb-ae0f-73ab6554b481" />
      <xplan:planinhalt xlink:href="#GML_1b6b653b-ccd5-462b-9135-c1f704399899" />
      <xplan:planinhalt xlink:href="#GML_1d919560-a0de-47fb-8303-5ef33913bac0" />
      <xplan:planinhalt xlink:href="#GML_0aafc0be-c225-4b1f-a4ac-1d0de3e9347d" />
      <xplan:planinhalt xlink:href="#GML_41be559e-6a93-4ad6-8539-816be658196c" />
      <xplan:planinhalt xlink:href="#GML_5369874b-e5f5-486e-90cb-5429a0b02078" />
      <xplan:planinhalt xlink:href="#GML_7138be9d-bc5a-45e3-a6d4-a940d9924bb9" />
      <xplan:planinhalt xlink:href="#GML_ccc84186-804a-48c9-b099-3bbe0f111452" />
      <xplan:planinhalt xlink:href="#GML_0a76c5d8-54dd-45df-b7f5-311e4d3de4cc" />
      <xplan:planinhalt xlink:href="#GML_155a2ab3-4d55-441a-a4f0-a94dcd9dc221" />
      <xplan:planinhalt xlink:href="#GML_500702aa-4cdc-486a-9f80-bce091bf4844" />
      <xplan:planinhalt xlink:href="#GML_348fa459-47c8-4b02-9bb7-dd542f064eee" />
      <xplan:planinhalt xlink:href="#GML_eca1a491-90da-4418-a750-02244bf75941" />
      <xplan:planinhalt xlink:href="#GML_218706d3-7382-404e-b3c6-d01478b2a006" />
      <xplan:planinhalt xlink:href="#GML_f743be71-2921-427c-9db3-efd595dda84f" />
      <xplan:planinhalt xlink:href="#GML_5970e72d-6406-4baf-be5f-f66401d5e05b" />
      <xplan:planinhalt xlink:href="#GML_c8ba83c0-814c-4957-9477-1f7bd1399d88" />
      <xplan:planinhalt xlink:href="#GML_dca1ce58-f29c-4de4-b94d-5b76b3c1839c" />
      <xplan:planinhalt xlink:href="#GML_e30a7ecf-77af-4399-be3f-6dc36130908e" />
      <xplan:planinhalt xlink:href="#GML_408a2db3-b3fc-4e7e-9042-b7ec0d7ab52e" />
      <xplan:planinhalt xlink:href="#GML_049edc40-a357-47a3-ab04-77fcef27142a" />
      <xplan:planinhalt xlink:href="#GML_8e33947b-6e48-4473-9aab-ea6bdb887959" />
      <xplan:planinhalt xlink:href="#GML_343e543b-dd39-4403-ba7f-b3d91820c7d6" />
      <xplan:planinhalt xlink:href="#GML_87107f3d-7fa8-47f2-b7a3-5f778d068f39" />
      <xplan:planinhalt xlink:href="#GML_eae29f33-edb4-416e-901f-10d787888388" />
      <xplan:planinhalt xlink:href="#GML_8522ed2f-2da1-446c-b7e2-c81c3e840252" />
      <xplan:planinhalt xlink:href="#GML_92d6616b-cccb-4c25-81f9-c132e16703fb" />
      <xplan:planinhalt xlink:href="#GML_58dca3ea-a5ac-4e9c-80b9-7dd0e36c09e9" />
      <xplan:planinhalt xlink:href="#GML_88d02fab-40aa-4e3c-9668-d82f0adcddee" />
      <xplan:planinhalt xlink:href="#GML_cd16e448-f2a9-4544-8a4e-f9a7021f93e3" />
      <xplan:planinhalt xlink:href="#GML_dff52785-cbf2-4510-b279-8acb077d88f9" />
      <xplan:planinhalt xlink:href="#GML_80587e28-d7ea-4f07-a861-1dd151704ea4" />
      <xplan:planinhalt xlink:href="#GML_05863e5d-a599-4f3c-9d62-d85479fea110" />
      <xplan:planinhalt xlink:href="#GML_9d2de821-f95e-48f0-8631-56b16734d5e5" />
      <xplan:planinhalt xlink:href="#GML_0f6d8c8d-1993-4b47-8503-06e9d907d248" />
      <xplan:planinhalt xlink:href="#GML_54b8d8a6-5846-4160-8c96-1960cd987eb4" />
      <xplan:planinhalt xlink:href="#GML_29d0a4c8-7a50-4ffe-bc06-8c4f61a8a0ef" />
      <xplan:planinhalt xlink:href="#GML_708a73b8-f756-4be8-84c7-78ae2267183f" />
      <xplan:planinhalt xlink:href="#GML_d1413c00-fb27-438c-a682-e325229d9c57" />
      <xplan:planinhalt xlink:href="#GML_6c415562-5879-482e-a006-38edcd21cd82" />
      <xplan:planinhalt xlink:href="#GML_6428bb15-14b7-4674-8a25-18ae66143a05" />
      <xplan:planinhalt xlink:href="#GML_265ba814-b5d6-4561-b317-496d53378093" />
      <xplan:planinhalt xlink:href="#GML_de3baf7c-3441-4345-8f1d-ae6c56d3d2a2" />
      <xplan:planinhalt xlink:href="#GML_83014b7f-1c28-40f1-bedc-3d83f4b80ad7" />
      <xplan:planinhalt xlink:href="#GML_91a139e2-bc14-4531-a02b-f49ad0994463" />
      <xplan:planinhalt xlink:href="#GML_e8974ae2-33cc-4e1c-97e2-4696b926fa8a" />
      <xplan:planinhalt xlink:href="#GML_d3f56d13-67cc-41a7-929c-d648bfd14033" />
      <xplan:planinhalt xlink:href="#GML_de545afe-b6a8-4485-8cd9-d49f51b04ff2" />
      <xplan:planinhalt xlink:href="#GML_3760c456-736e-403e-a38a-971952b0594e" />
      <xplan:planinhalt xlink:href="#GML_8aaab618-b7bc-4150-b7d2-671701ec3c2e" />
      <xplan:planinhalt xlink:href="#GML_e6760a0d-d68d-4cc1-a705-80eeb049d156" />
      <xplan:planinhalt xlink:href="#GML_8ea26516-2cf0-46ea-9caa-9d7368f387bb" />
      <xplan:planinhalt xlink:href="#GML_49a29acf-2b0b-414d-8473-761bef59b5d6" />
      <xplan:planinhalt xlink:href="#GML_6f52a583-7dc6-4848-98e5-15c69a696bc8" />
      <xplan:planinhalt xlink:href="#GML_b0dd6b6a-9c84-4245-b7ce-037b046d9c70" />
      <xplan:planinhalt xlink:href="#GML_4de9b2f8-05fe-4ff3-8743-5f9776321fa4" />
      <xplan:planinhalt xlink:href="#GML_9a1818fe-3cec-465f-9b2b-54eb07bbe782" />
      <xplan:planinhalt xlink:href="#GML_05940b4e-11ef-4201-80b6-193cbace90ac" />
      <xplan:planinhalt xlink:href="#GML_be80a6a4-5264-4073-b4ed-0ca54aa43346" />
      <xplan:planinhalt xlink:href="#GML_970e5696-1175-460c-9e9a-d96a69319df2" />
      <xplan:planinhalt xlink:href="#GML_6236ba59-ce22-4d8c-899d-317089c3a6a3" />
      <xplan:planinhalt xlink:href="#GML_4f2e8b68-cf52-47c9-876a-f99ee2c72acb" />
      <xplan:planinhalt xlink:href="#GML_d2559c4a-5f23-47c6-8207-714db0f03198" />
      <xplan:planinhalt xlink:href="#GML_5b04d7de-74fb-4901-827b-5b1d6be6aa3f" />
      <xplan:planinhalt xlink:href="#GML_b0522273-e5e3-4d72-8481-7422595cf1ad" />
      <xplan:planinhalt xlink:href="#GML_037ce5e0-932b-49b2-8063-250774d18fca" />
      <xplan:planinhalt xlink:href="#GML_8425be48-80ec-4db8-a269-7fa317f1992a" />
      <xplan:planinhalt xlink:href="#GML_189ae434-a656-4164-a3ed-1d32805d0b2e" />
      <xplan:planinhalt xlink:href="#GML_40f9062f-45bb-47d3-9b5b-6e37b9266076" />
      <xplan:planinhalt xlink:href="#GML_95d8d47b-e7a3-471a-8a2a-4217813909d2" />
      <xplan:praesentationsobjekt xlink:href="#GML_f4429e34-caaf-4ac0-9bc1-9bf1e54aced1" />
      <xplan:praesentationsobjekt xlink:href="#GML_c51ec6c6-6db2-4ee5-a288-73d871577816" />
      <xplan:praesentationsobjekt xlink:href="#GML_e89666dd-2e3c-4ae6-9810-6a0e19862650" />
      <xplan:praesentationsobjekt xlink:href="#GML_26c8d1ad-96e2-424a-8a11-b14bdbbd66b8" />
      <xplan:praesentationsobjekt xlink:href="#GML_ca170499-5b72-4e52-8165-39283beed034" />
      <xplan:praesentationsobjekt xlink:href="#GML_192deb22-5627-4fb3-a43d-f44e8ceddd02" />
      <xplan:praesentationsobjekt xlink:href="#GML_baade317-ec9c-4a2c-86c3-fa9af52f8d7f" />
      <xplan:praesentationsobjekt xlink:href="#GML_4f65e9f6-20df-4476-b32c-5f1fd5c9fb7d" />
      <xplan:praesentationsobjekt xlink:href="#GML_9bbf7c14-02f9-43e4-882c-d9f5bea03225" />
      <xplan:praesentationsobjekt xlink:href="#GML_52427f6b-ca18-4dfd-8c73-0edb82799d3d" />
      <xplan:praesentationsobjekt xlink:href="#GML_48ba3f43-ffd8-45e3-a789-24d042a61a2a" />
      <xplan:praesentationsobjekt xlink:href="#GML_98135858-2103-47b7-8c37-5c600f468aea" />
      <xplan:praesentationsobjekt xlink:href="#GML_888933f0-a52f-4cc8-b9b1-ec17f9722cb4" />
      <xplan:praesentationsobjekt xlink:href="#GML_ecd8fdcd-601d-47c4-9fe6-3f0f7d062c67" />
      <xplan:praesentationsobjekt xlink:href="#GML_f8033363-21de-424e-964d-dbaeb7de952c" />
      <xplan:praesentationsobjekt xlink:href="#GML_358420ca-8922-49bc-b400-dffbcb5307f9" />
      <xplan:praesentationsobjekt xlink:href="#GML_e27ffc6c-0035-49e0-a0a6-39e7d320bba3" />
      <xplan:praesentationsobjekt xlink:href="#GML_dfdfa5d3-6d06-420d-8845-27b43ed07cf6" />
      <xplan:praesentationsobjekt xlink:href="#GML_572e6ba3-b24d-420e-a1d8-92a0d191a24e" />
      <xplan:praesentationsobjekt xlink:href="#GML_ba744328-7c63-47eb-a28d-8e58ff00ce90" />
      <xplan:praesentationsobjekt xlink:href="#GML_5d9b3838-d18d-429e-9741-481aab1fdefe" />
      <xplan:praesentationsobjekt xlink:href="#GML_8c05acb3-8a8d-4b71-ab66-73960ee93d49" />
      <xplan:praesentationsobjekt xlink:href="#GML_3a666524-25ea-4852-953e-10a9ebd2f4db" />
      <xplan:praesentationsobjekt xlink:href="#GML_7bf9b41f-19e6-4264-9242-1d33a317e542" />
      <xplan:praesentationsobjekt xlink:href="#GML_bad8ffc7-8436-40a3-afe8-468d13e6ab1c" />
      <xplan:praesentationsobjekt xlink:href="#GML_eb93214b-9a54-4652-a042-0e16c10189b8" />
      <xplan:praesentationsobjekt xlink:href="#GML_c42a65e1-8f94-4df8-b8aa-02ee62cc078c" />
      <xplan:praesentationsobjekt xlink:href="#GML_b38ec1cc-4e10-4c44-95d0-58a5df29be24" />
      <xplan:praesentationsobjekt xlink:href="#GML_4c071bb8-6f16-482b-a590-f85e5a5c4c30" />
      <xplan:praesentationsobjekt xlink:href="#GML_0402b39e-a67b-4d10-b3d0-273032a6b411" />
      <xplan:praesentationsobjekt xlink:href="#GML_9134ac06-f447-4b4c-9a53-34d7f1c5ba80" />
      <xplan:praesentationsobjekt xlink:href="#GML_d34074c0-0d4c-4cac-b27e-059d2828bf75" />
      <xplan:praesentationsobjekt xlink:href="#GML_cf47acbb-a42e-4b3d-88a5-1f11ada5195b" />
      <xplan:praesentationsobjekt xlink:href="#GML_027f1de0-fc2a-462e-874d-73e05739a770" />
      <xplan:praesentationsobjekt xlink:href="#GML_7d01bce0-b053-4fd1-a80c-2792c5e31f65" />
      <xplan:praesentationsobjekt xlink:href="#GML_a4f56831-ff7f-40e4-b347-0610722db7b3" />
      <xplan:praesentationsobjekt xlink:href="#GML_5a6b4b50-618d-49fd-bcf3-da3bd5583cf4" />
      <xplan:praesentationsobjekt xlink:href="#GML_be29347b-da63-4285-9a75-5aedf925862c" />
      <xplan:praesentationsobjekt xlink:href="#GML_0946a484-70bb-4303-9280-c24acadb9286" />
      <xplan:praesentationsobjekt xlink:href="#GML_3b1a562b-9654-47ff-89cc-52c46c71bc74" />
      <xplan:praesentationsobjekt xlink:href="#GML_17bb3a32-7fe5-469d-ad8d-76e21a3707a2" />
      <xplan:praesentationsobjekt xlink:href="#GML_87b36973-eedf-47af-b03f-c364657a5523" />
      <xplan:praesentationsobjekt xlink:href="#GML_f189e79e-d7e8-4b66-8d89-a8eebf6843c8" />
      <xplan:praesentationsobjekt xlink:href="#GML_89b8cf42-e83b-4d9d-939d-c014bf704e70" />
      <xplan:praesentationsobjekt xlink:href="#GML_cc84d7dd-9b5c-4f57-b247-e07a706c07a5" />
      <xplan:praesentationsobjekt xlink:href="#GML_e5c1e368-63b3-481a-a0df-90e90f79222a" />
      <xplan:praesentationsobjekt xlink:href="#GML_8ca0f3b7-0f55-4d7f-a06d-dc30f46bfea3" />
      <xplan:praesentationsobjekt xlink:href="#GML_81f7e70f-7cd0-478f-ba88-dcbb3f4a6135" />
      <xplan:praesentationsobjekt xlink:href="#GML_ed778106-c04d-4c5b-bcb4-b88f7f3a3a31" />
      <xplan:praesentationsobjekt xlink:href="#GML_7b0ca2ee-f83b-475e-b96d-008c2350f92c" />
      <xplan:praesentationsobjekt xlink:href="#GML_6074029e-bb03-487d-987b-392b228c70f8" />
      <xplan:versionBauNVOText>Version vom 21.11.2017</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_443c3ad5-4b47-4a31-92c0-c5301ba795d2" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UnverbindlicheVormerkung gml:id="GML_b8afcd6b-4f8b-4b10-89d7-9760fa6f7ca1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566928.837 5928597.35</gml:lowerCorner>
          <gml:upperCorner>566967.357 5928836.406</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_98135858-2103-47b7-8c37-5c600f468aea" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_64301F37-45FD-4BD2-86A7-068EBD9FE490" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566967.357 5928836.165 566959.446 5928836.406 566950.889 5928800.508 
566948.909 5928792.001 566948.003 5928788.431 566945.711 5928778.788 
566945.042 5928775.899 566944.686 5928774.349 566942.047 5928752.288 
566939.16 5928728.217 566937.941 5928717.596 566935.476 5928697.066 
566932.969 5928676.424 566932.211 5928670.197 566931.611 5928659.072 
566929.689 5928620.076 566928.837 5928601.224 566929.791 5928601.076 
566946.143 5928597.35 566950.913 5928621.254 566952.471 5928634.201 
566937.004 5928636.066 566938.394 5928663.983 566949.953 5928760.154 
566958.094 5928759.191 566967.357 5928836.165 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:vormerkung>vorgesehene Oberflächenentwässerung</xplan:vormerkung>
    </xplan:BP_UnverbindlicheVormerkung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_98135858-2103-47b7-8c37-5c600f468aea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.564 5928674.563</gml:lowerCorner>
          <gml:upperCorner>566938.564 5928674.563</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>vormerkung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_b8afcd6b-4f8b-4b10-89d7-9760fa6f7ca1" />
      <xplan:schriftinhalt>vorgesehene Oberflächenentwässerung</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_70A391AC-1722-4E2D-A2F8-790F9315537E" srsName="EPSG:25832">
          <gml:pos>566938.564 5928674.563</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">83.04</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_bc2af5b6-4e50-4378-a3e1-38056b69f0ff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.653 5928753.468</gml:lowerCorner>
          <gml:upperCorner>566979.782 5928760.799</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0DDD3516-F205-4C40-A322-6AB5D06780AD" srsName="EPSG:25832">
          <gml:posList>566916.653 5928760.799 566917.069 5928760.751 566946.166 5928757.372 
566946.776 5928757.301 566975.166 5928754.004 566979.782 5928753.468 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_cf45ec68-2391-4bcd-92a6-ffbe3e37639f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566908.806 5928668.369</gml:lowerCorner>
          <gml:upperCorner>566912.968 5928685.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_027f1de0-fc2a-462e-874d-73e05739a770" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_ED3BDDE5-AA2F-4A1D-B630-A254A197FEBA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566912.7175 5928683.541 566912.968 5928685.632 566910.864 5928685.724 
566908.806 5928668.622 566910.903 5928668.369 566911.145 5928670.483 
566911.156 5928670.482 566912.7175 5928683.541 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_027f1de0-fc2a-462e-874d-73e05739a770">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566910.485 5928677.279</gml:lowerCorner>
          <gml:upperCorner>566910.485 5928677.279</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cf45ec68-2391-4bcd-92a6-ffbe3e37639f" />
      <xplan:position>
        <gml:Point gml:id="Gml_5AFF40CC-1313-4363-8CF7-8DFF78E33A27" srsName="EPSG:25832">
          <gml:pos>566910.485 5928677.279</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3d7fed22-6e30-444e-9669-ab605aaaeca8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566949.603 5928739.574</gml:lowerCorner>
          <gml:upperCorner>566975.162 5928756.838</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">14.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f4429e34-caaf-4ac0-9bc1-9bf1e54aced1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_194623F8-A639-41C0-B786-A7274C8AC1A0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566951.917 5928744.933 566953.321 5928756.599 566951.336 5928756.838 
566949.603 5928742.442 566973.43 5928739.574 566975.162 5928753.97 
566969.801 5928754.615 566968.792 5928746.236 566969.487 5928746.152 
566969.092 5928742.866 566951.917 5928744.933 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f4429e34-caaf-4ac0-9bc1-9bf1e54aced1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566960.991 5928742.231</gml:lowerCorner>
          <gml:upperCorner>566960.991 5928742.231</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3d7fed22-6e30-444e-9669-ab605aaaeca8" />
      <xplan:position>
        <gml:Point gml:id="Gml_B5E58A2E-00F9-4EB9-9A5F-FFC15BFEF05A" srsName="EPSG:25832">
          <gml:pos>566960.991 5928742.231</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_131f97cc-0eac-4442-9fa5-d34b844b0673">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566911.156 5928623.432</gml:lowerCorner>
          <gml:upperCorner>566930.219 5928670.482</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_C0E0DAE5-86CE-4078-8B48-953F96E53528" srsName="EPSG:25832">
          <gml:posList>566911.156 5928670.482 566928.324 5928668.414 566928.22 5928666.275 
566930.219 5928666.177 566929.812 5928657.863 566928.313 5928657.936 
566927.485 5928640.956 566928.983 5928640.863 566928.503 5928631.025 
566927.004 5928631.098 566926.63 5928623.432 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_60f2804a-f211-4bff-b725-a60c898c1d9d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566898.531 5928575.496</gml:lowerCorner>
          <gml:upperCorner>567054.901 5928657.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(2)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8ca0f3b7-0f55-4d7f-a06d-dc30f46bfea3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_11776302-234f-4422-9f4c-a5e9252c889a" />
      <xplan:refTextInhalt xlink:href="#GML_e7713d5e-1b95-4b15-abf8-628ec4d14bad" />
      <xplan:refTextInhalt xlink:href="#GML_eb4982a4-8e29-49fa-9c6b-856b261ccb5a" />
      <xplan:refTextInhalt xlink:href="#GML_73c41f4a-8d57-4239-b1d2-d440315b55ea" />
      <xplan:refTextInhalt xlink:href="#GML_8839799e-5e0a-4a2e-a88b-c6e18047a0d4" />
      <xplan:refTextInhalt xlink:href="#GML_e088f923-cc26-4084-a05e-29831ae23397" />
      <xplan:refTextInhalt xlink:href="#GML_64060638-90c4-4dd9-8e64-787a46e192be" />
      <xplan:refTextInhalt xlink:href="#GML_864f30d7-31e2-480b-a8be-90f30ac58579" />
      <xplan:refTextInhalt xlink:href="#GML_90bd7307-fe86-44ce-bb2f-9bc6cbd2ed95" />
      <xplan:refTextInhalt xlink:href="#GML_f40af8dd-3f2c-4531-851c-add7fb18f336" />
      <xplan:refTextInhalt xlink:href="#GML_cf5e49ce-9e68-4596-861d-25f91f294cf1" />
      <xplan:refTextInhalt xlink:href="#GML_360ccf4b-fcff-41e9-830f-98efc562c45d" />
      <xplan:refTextInhalt xlink:href="#GML_ac3f5786-7676-49c1-9bfb-619b6e96153a" />
      <xplan:refTextInhalt xlink:href="#GML_d6bdeec1-3419-4451-adb2-21aa355c5785" />
      <xplan:refTextInhalt xlink:href="#GML_10f1bbc4-6090-4a0f-ac37-4a948a466972" />
      <xplan:refTextInhalt xlink:href="#GML_788057ec-8264-473f-905b-bc1c6ec71f77" />
      <xplan:refTextInhalt xlink:href="#GML_2baf0b93-f279-405c-8a93-0f039968bf23" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C06D2940-2FDE-4D0E-AEF6-434DE6474EB3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566971.145 5928649.093 566929.808 5928654.069 566929.808 5928655.271 
566914.007 5928657.173 566912.169 5928619.496 566903.817 5928621.184 
566900.362 5928621.875 566899.327 5928613.667 566898.531 5928607.364 
566902.662 5928606.537 566911.162 5928604.845 566925.462 5928602 
566929.402 5928601.144 566934.312 5928600.047 566956.712 5928594.942 
566965.812 5928592.878 566973.493 5928591.154 566992.262 5928586.796 
566997.782 5928585.507 567016.362 5928581.658 567017.463 5928581.435 
567022.017 5928580.492 567022.893 5928580.311 567023.611 5928580.191 
567024.212 5928580.069 567030.739 5928578.993 567031.628 5928578.85 
567038.561 5928577.69 567052.858 5928575.496 567053.456 5928575.994 
567054.901 5928592.944 567052.362 5928593.249 567040.56 5928594.705 
567026.57 5928597.496 566966.384 5928609.517 566971.145 5928649.093 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8ca0f3b7-0f55-4d7f-a06d-dc30f46bfea3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566929.924 5928628.572</gml:lowerCorner>
          <gml:upperCorner>566929.924 5928628.572</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_60f2804a-f211-4bff-b725-a60c898c1d9d" />
      <xplan:position>
        <gml:Point gml:id="Gml_EC78E960-48F0-4C2E-8A79-D8240B8CB61C" srsName="EPSG:25832">
          <gml:pos>566929.924 5928628.572</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_657b776e-647e-40e0-9e01-9e12d2a2d4d6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566895.6 5928654.6</gml:lowerCorner>
          <gml:upperCorner>566991.874 5928837.693</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6CE064A0-6CB3-4E12-932A-96A2EFF61109" srsName="EPSG:25832">
          <gml:posList>566895.6 5928654.6 566897.78 5928686.413 566900.803 5928719.935 
566905.732 5928755.922 566908.447 5928761.753 566916.653 5928760.799 
566921.405 5928780.721 566918.72 5928781.621 566929.725 5928826.17 
566931.97 5928835.26 566935.236 5928837.693 566967.855 5928836.124 
566971.645 5928835.942 566975.151 5928836.043 566977.758 5928836.119 
566982.013 5928835.917 566991.874 5928835.45 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_9bd2cb90-23c9-416c-ab7e-73235aaf86c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566982.166 5928758.822</gml:lowerCorner>
          <gml:upperCorner>566982.166 5928758.822</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_68AB5AB8-87A3-4FC7-B779-AF149F400D53" srsName="EPSG:25832">
          <gml:pos>566982.166 5928758.822</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d304d680-9e91-4922-8a2a-bc06f90ab2a1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566912.718 5928683.162</gml:lowerCorner>
          <gml:upperCorner>566922.854 5928696.142</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E82769B0-A595-41B1-975E-1DEA2779D39E" srsName="EPSG:25832">
          <gml:posList>566914.234 5928696.142 566922.854 5928694.428 566921.498 5928683.162 
566912.718 5928683.541 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7766968d-ac1e-4f13-88f5-a228f7f96316">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566915.909 5928709.678</gml:lowerCorner>
          <gml:upperCorner>566926.046 5928722.66</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a4f56831-ff7f-40e4-b347-0610722db7b3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_D0C7B01C-86CE-4CE8-9889-57687AD8006D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566926.046 5928720.946 566917.426 5928722.66 566917.222 5928720.967 
566923.875 5928719.644 566922.889 5928711.458 566916.112 5928711.752 
566915.909 5928710.059 566924.69 5928709.678 566926.046 5928720.946 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a4f56831-ff7f-40e4-b347-0610722db7b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566924.149 5928714.182</gml:lowerCorner>
          <gml:upperCorner>566924.149 5928714.182</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7766968d-ac1e-4f13-88f5-a228f7f96316" />
      <xplan:position>
        <gml:Point gml:id="Gml_F39E3465-625D-4AD3-AB44-CE5934FC1465" srsName="EPSG:25832">
          <gml:pos>566924.149 5928714.182</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_33c4107a-0e08-45b1-aefa-824aea8e0f6c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.48 5928593.603</gml:lowerCorner>
          <gml:upperCorner>566938.48 5928593.603</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.42</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_57EA35FF-76AB-4228-9DF8-AD3A872439E2" srsName="EPSG:25832">
          <gml:pos>566938.48 5928593.603</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_e5ab0e24-8b45-4963-b2af-5fcc75d586bf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567058.521 5928560.604</gml:lowerCorner>
          <gml:upperCorner>567058.521 5928560.604</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_06A1005F-62D8-480B-B65D-28202CA390F7" srsName="EPSG:25832">
          <gml:pos>567058.521 5928560.604</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7f7cef32-430d-4cdf-8bc7-68f138409141">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566937.317 5928801.551</gml:lowerCorner>
          <gml:upperCorner>566944.048 5928821.785</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">23.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0402b39e-a67b-4d10-b3d0-273032a6b411" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_18232D2D-AA81-4AFD-B2C2-09FCC1B7BC17" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566939.717 5928803.21 566943.655 5928819.798 566944.048 5928821.457 
566941.968 5928821.785 566937.317 5928802.193 566939.323 5928801.551 
566939.717 5928803.21 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0402b39e-a67b-4d10-b3d0-273032a6b411">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566940.37 5928809.062</gml:lowerCorner>
          <gml:upperCorner>566940.37 5928809.062</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7f7cef32-430d-4cdf-8bc7-68f138409141" />
      <xplan:position>
        <gml:Point gml:id="Gml_13E4AB91-6453-4137-8B00-0173D82C5070" srsName="EPSG:25832">
          <gml:pos>566940.37 5928809.062</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bf8af583-199a-49f2-a4b5-0be3096bde4b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566946.477 5928713.61</gml:lowerCorner>
          <gml:upperCorner>566972.037 5928730.874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">14.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f8033363-21de-424e-964d-dbaeb7de952c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9B9B9F23-7F5D-4DFD-8C2C-19433F5C750D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566948.792 5928718.97 566950.196 5928730.635 566948.21 5928730.874 
566946.477 5928716.478 566970.304 5928713.61 566972.037 5928728.006 
566966.676 5928728.652 566965.667 5928720.272 566966.362 5928720.188 
566965.967 5928716.902 566948.792 5928718.97 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f8033363-21de-424e-964d-dbaeb7de952c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566962.772 5928716.067</gml:lowerCorner>
          <gml:upperCorner>566962.772 5928716.067</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bf8af583-199a-49f2-a4b5-0be3096bde4b" />
      <xplan:position>
        <gml:Point gml:id="Gml_7AFC5081-17F7-40FF-B704-60E9EAF01E68" srsName="EPSG:25832">
          <gml:pos>566962.772 5928716.067</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_828c0102-4fef-4a6a-b8b0-ede795f90741">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566956.371 5928609.517</gml:lowerCorner>
          <gml:upperCorner>566971.145 5928649.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">18</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6074029e-bb03-487d-987b-392b228c70f8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7B0FB37A-5BB7-402D-ABCB-3E43C8CED3C7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566971.145 5928649.093 566965.128 5928649.817 566963.756 5928638.423 
566959.671 5928638.914 566956.371 5928611.498 566956.525 5928611.467 
566966.384 5928609.517 566971.145 5928649.093 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6074029e-bb03-487d-987b-392b228c70f8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566962.117 5928634.343</gml:lowerCorner>
          <gml:upperCorner>566962.117 5928634.343</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_828c0102-4fef-4a6a-b8b0-ede795f90741" />
      <xplan:position>
        <gml:Point gml:id="Gml_26C64C6A-EF9A-46CB-9366-BB371E1BC354" srsName="EPSG:25832">
          <gml:pos>566962.117 5928634.343</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_5c301b2c-9587-4996-a0f6-eded200050e4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566929.791 5928597.35</gml:lowerCorner>
          <gml:upperCorner>566967.357 5928836.498</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_fbbe13d6-5b10-4bc1-96c2-e030e42767c3" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8258FC04-0743-41C3-88C3-E21F14B9D0BD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566967.357 5928836.165 566960.425 5928836.498 566945.686 5928774.309 
566944.606 5928765.366 566933.158 5928670.157 566933.043 5928667.812 
566932.871 5928664.268 566932.279 5928652.138 566931.519 5928636.532 
566929.791 5928601.076 566946.143 5928597.35 566950.913 5928621.254 
566952.471 5928634.201 566937.004 5928636.066 566938.394 5928663.983 
566949.953 5928760.154 566958.094 5928759.191 566967.357 5928836.165 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c42bc820-6d19-4e09-9090-43e019eadbf0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566944.111 5928619.117</gml:lowerCorner>
          <gml:upperCorner>566959.671 5928652.146</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">15</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e27ffc6c-0035-49e0-a0a6-39e7d320bba3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_84462157-7477-4FE8-91CA-D266C8ED352B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566959.671 5928638.914 566946.399 5928640.515 566947.77 5928651.907 
566945.784 5928652.146 566944.111 5928638.249 566955.572 5928636.87 
566953.491 5928619.574 566957.288 5928619.117 566959.671 5928638.914 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e27ffc6c-0035-49e0-a0a6-39e7d320bba3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566955.032 5928623.982</gml:lowerCorner>
          <gml:upperCorner>566955.032 5928623.982</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c42bc820-6d19-4e09-9090-43e019eadbf0" />
      <xplan:position>
        <gml:Point gml:id="Gml_F80D8518-8ADC-4972-A270-DAB8F8C1606D" srsName="EPSG:25832">
          <gml:pos>566955.032 5928623.982</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">83.78</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_2034e12e-a85e-4064-8d63-29b5a28c6ced">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566975.166 5928753.468</gml:lowerCorner>
          <gml:upperCorner>566979.782 5928754.004</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_496AA03D-F284-412D-856F-5CF373A0C4AD" srsName="EPSG:25832">
          <gml:posList>566975.166 5928754.004 566979.782 5928753.468 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_18078e7c-abf5-4f54-9c7a-47df29410333">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566913.6 5928672.077</gml:lowerCorner>
          <gml:upperCorner>566913.6 5928672.077</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_55E5486A-4D81-45E0-BA49-6AF53D56889E" srsName="EPSG:25832">
          <gml:pos>566913.6 5928672.077</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_5b8c2dcd-2f47-4a89-b4ec-4e9848bf50c1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566967.962 5928598.75</gml:lowerCorner>
          <gml:upperCorner>567020.877 5928625.334</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0272306f-1eb9-4f65-b515-6eb7857883a8" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6234B5BB-40B4-4028-A1E2-3F83AF3530B0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566974.605 5928624.392 566969.864 5928625.334 566967.962 5928609.183 
567020.192 5928598.75 567020.877 5928602.182 566971.814 5928611.983 
566972.889 5928621.164 566973.57 5928621.029 566974.605 5928624.392 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9c8edf84-80dc-40f5-a256-db5f3eff529b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566912.718 5928683.162</gml:lowerCorner>
          <gml:upperCorner>566922.854 5928696.142</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4c071bb8-6f16-482b-a590-f85e5a5c4c30" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BA9A5EF3-9E97-48C1-8E99-B5CAA0F59E73" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566922.854 5928694.428 566914.234 5928696.142 566913.982 5928694.055 
566920.635 5928692.73 566919.745 5928685.337 566912.968 5928685.632 
566912.718 5928683.541 566921.498 5928683.162 566922.854 5928694.428 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4c071bb8-6f16-482b-a590-f85e5a5c4c30">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566921.193 5928687.452</gml:lowerCorner>
          <gml:upperCorner>566921.193 5928687.452</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9c8edf84-80dc-40f5-a256-db5f3eff529b" />
      <xplan:position>
        <gml:Point gml:id="Gml_E8FAF67E-7569-4671-98F2-DDA72FE142BD" srsName="EPSG:25832">
          <gml:pos>566921.193 5928687.452</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_40e2cc4c-3e65-4e43-8bff-0bba9dbafb25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566943.496 5928986.485</gml:lowerCorner>
          <gml:upperCorner>566984.153 5929011.434</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_d34074c0-0d4c-4cac-b27e-059d2828bf75" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_05B44A7E-B6B5-4DC7-80BC-D85F98A98F14" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566984.153 5929000.331 566971.214 5929011.434 566955.549 5928999.355 
566943.496 5928990.061 566982.93 5928986.485 566984.153 5929000.331 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d34074c0-0d4c-4cac-b27e-059d2828bf75">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566962.386 5928995.045</gml:lowerCorner>
          <gml:upperCorner>566962.386 5928995.045</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_40e2cc4c-3e65-4e43-8bff-0bba9dbafb25" />
      <xplan:position>
        <gml:Point gml:id="Gml_611D3665-F372-4EAA-9E73-6E64349F7510" srsName="EPSG:25832">
          <gml:pos>566962.386 5928995.045</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e261d60e-a41f-4fa2-ab0e-515b1a084a8b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566940.227 5928661.682</gml:lowerCorner>
          <gml:upperCorner>566965.787 5928678.946</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">14.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9bbf7c14-02f9-43e4-882c-d9f5bea03225" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9DA4FC77-6441-4C04-B6CF-57E537D99BD3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566942.541 5928667.042 566943.945 5928678.707 566941.96 5928678.946 
566940.227 5928664.55 566964.054 5928661.682 566965.787 5928676.078 
566960.425 5928676.724 566959.417 5928668.344 566960.112 5928668.26 
566959.716 5928664.974 566942.541 5928667.042 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9bbf7c14-02f9-43e4-882c-d9f5bea03225">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566952.139 5928663.919</gml:lowerCorner>
          <gml:upperCorner>566952.139 5928663.919</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e261d60e-a41f-4fa2-ab0e-515b1a084a8b" />
      <xplan:position>
        <gml:Point gml:id="Gml_9F9E127E-C8D1-4C6F-84F8-E2397636F405" srsName="EPSG:25832">
          <gml:pos>566952.139 5928663.919</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_1ef2650e-7c6e-4acf-8fcc-faef2ed42761">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567016.762 5928580.462</gml:lowerCorner>
          <gml:upperCorner>567022.847 5928585.021</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0272306f-1eb9-4f65-b515-6eb7857883a8" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C7A400DF-5E24-4E41-AC98-9F40C187E535" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567022.847 5928583.963 567017.449 5928585.021 567016.762 5928581.577 
567017.463 5928581.435 567022.147 5928580.462 567022.847 5928583.963 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_969a90ce-82eb-4de1-884b-a463ba3fb1d7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566908.802 5928778.536</gml:lowerCorner>
          <gml:upperCorner>566908.802 5928778.536</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">1.32</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_5C334E33-4E6C-4FE0-93C6-4A69A1179FA1" srsName="EPSG:25832">
          <gml:pos>566908.802 5928778.536</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_b960b890-a89c-450c-8cc0-8980f70cd275">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566930.448 5928772.614</gml:lowerCorner>
          <gml:upperCorner>566937.179 5928792.848</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">23.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_be29347b-da63-4285-9a75-5aedf925862c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7D40316F-A8FE-4ECA-AECF-99897B03C403" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566932.848 5928774.273 566937.179 5928792.52 566935.099 5928792.848 
566930.448 5928773.255 566932.454 5928772.614 566932.848 5928774.273 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_be29347b-da63-4285-9a75-5aedf925862c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566934.194 5928787.598</gml:lowerCorner>
          <gml:upperCorner>566934.194 5928787.598</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_b960b890-a89c-450c-8cc0-8980f70cd275" />
      <xplan:position>
        <gml:Point gml:id="Gml_9DB6B0DD-7873-4FFB-95ED-BC0067D89615" srsName="EPSG:25832">
          <gml:pos>566934.194 5928787.598</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f4aec1d4-ef45-46bd-bde9-3cc20b3cb224">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566956.371 5928611.498</gml:lowerCorner>
          <gml:upperCorner>566959.671 5928638.915</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3C526472-81C8-4832-B081-75F4E307F60D" srsName="EPSG:25832">
          <gml:posList>566956.371 5928611.498 566959.671 5928638.915 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a86495d4-e31f-4801-834b-32b68ce227a2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566975.239 5928766.981</gml:lowerCorner>
          <gml:upperCorner>566978.021 5928777.901</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">14.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ba744328-7c63-47eb-a28d-8e58ff00ce90" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6DB5889A-252D-475B-AFB5-FD9B65BCFDF7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566978.021 5928777.722 566976.532 5928777.901 566975.239 5928767.16 
566976.728 5928766.981 566978.021 5928777.722 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ba744328-7c63-47eb-a28d-8e58ff00ce90">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.196 5928772.763</gml:lowerCorner>
          <gml:upperCorner>566976.196 5928772.763</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a86495d4-e31f-4801-834b-32b68ce227a2" />
      <xplan:position>
        <gml:Point gml:id="Gml_1C40FAA2-79C3-415B-919C-93006B52AEF0" srsName="EPSG:25832">
          <gml:pos>566976.196 5928772.763</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_245062d3-054e-4354-a45e-01b0e5414780">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566988.135 5928835.758</gml:lowerCorner>
          <gml:upperCorner>566988.135 5928835.758</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_733ADB9F-ECBD-4D92-8371-2AE73CFCDC77" srsName="EPSG:25832">
          <gml:pos>566988.135 5928835.758</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_f3b35e0d-48ca-406f-9cee-8e8d17e5c148">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.653 5928757.071</gml:lowerCorner>
          <gml:upperCorner>566967.855 5928837.693</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(1)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_89b8cf42-e83b-4d9d-939d-c014bf704e70" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_11776302-234f-4422-9f4c-a5e9252c889a" />
      <xplan:refTextInhalt xlink:href="#GML_e7713d5e-1b95-4b15-abf8-628ec4d14bad" />
      <xplan:refTextInhalt xlink:href="#GML_eb4982a4-8e29-49fa-9c6b-856b261ccb5a" />
      <xplan:refTextInhalt xlink:href="#GML_73c41f4a-8d57-4239-b1d2-d440315b55ea" />
      <xplan:refTextInhalt xlink:href="#GML_8839799e-5e0a-4a2e-a88b-c6e18047a0d4" />
      <xplan:refTextInhalt xlink:href="#GML_e088f923-cc26-4084-a05e-29831ae23397" />
      <xplan:refTextInhalt xlink:href="#GML_64060638-90c4-4dd9-8e64-787a46e192be" />
      <xplan:refTextInhalt xlink:href="#GML_864f30d7-31e2-480b-a8be-90f30ac58579" />
      <xplan:refTextInhalt xlink:href="#GML_90bd7307-fe86-44ce-bb2f-9bc6cbd2ed95" />
      <xplan:refTextInhalt xlink:href="#GML_2100a7ca-a80f-469c-92a9-b489692f93ce" />
      <xplan:refTextInhalt xlink:href="#GML_f40af8dd-3f2c-4531-851c-add7fb18f336" />
      <xplan:refTextInhalt xlink:href="#GML_cf5e49ce-9e68-4596-861d-25f91f294cf1" />
      <xplan:refTextInhalt xlink:href="#GML_360ccf4b-fcff-41e9-830f-98efc562c45d" />
      <xplan:refTextInhalt xlink:href="#GML_ac3f5786-7676-49c1-9bfb-619b6e96153a" />
      <xplan:refTextInhalt xlink:href="#GML_d6bdeec1-3419-4451-adb2-21aa355c5785" />
      <xplan:refTextInhalt xlink:href="#GML_10f1bbc4-6090-4a0f-ac37-4a948a466972" />
      <xplan:refTextInhalt xlink:href="#GML_788057ec-8264-473f-905b-bc1c6ec71f77" />
      <xplan:refTextInhalt xlink:href="#GML_2baf0b93-f279-405c-8a93-0f039968bf23" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_511A690F-9558-4DF9-A30F-0BA957AB84EC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566967.855 5928836.124 566935.236 5928837.693 566931.97 5928835.26 
566929.725 5928826.17 566918.72 5928781.621 566921.405 5928780.721 
566917.587 5928765.033 566916.653 5928760.799 566946.166 5928757.372 
566946.776 5928757.301 566951.371 5928757.416 566958.452 5928757.071 
566967.855 5928836.124 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_89b8cf42-e83b-4d9d-939d-c014bf704e70">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566929.863 5928770.472</gml:lowerCorner>
          <gml:upperCorner>566929.863 5928770.472</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_f3b35e0d-48ca-406f-9cee-8e8d17e5c148" />
      <xplan:position>
        <gml:Point gml:id="Gml_964A4764-A443-498E-860A-9F3F546B0F86" srsName="EPSG:25832">
          <gml:pos>566929.863 5928770.472</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_eaf477b8-ab9d-48a6-9870-3c4521ada6dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566895.6 5928592.944</gml:lowerCorner>
          <gml:upperCorner>567060.062 5928761.753</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_11776302-234f-4422-9f4c-a5e9252c889a" />
      <xplan:refTextInhalt xlink:href="#GML_e7713d5e-1b95-4b15-abf8-628ec4d14bad" />
      <xplan:refTextInhalt xlink:href="#GML_eb4982a4-8e29-49fa-9c6b-856b261ccb5a" />
      <xplan:refTextInhalt xlink:href="#GML_73c41f4a-8d57-4239-b1d2-d440315b55ea" />
      <xplan:refTextInhalt xlink:href="#GML_8839799e-5e0a-4a2e-a88b-c6e18047a0d4" />
      <xplan:refTextInhalt xlink:href="#GML_e088f923-cc26-4084-a05e-29831ae23397" />
      <xplan:refTextInhalt xlink:href="#GML_64060638-90c4-4dd9-8e64-787a46e192be" />
      <xplan:refTextInhalt xlink:href="#GML_864f30d7-31e2-480b-a8be-90f30ac58579" />
      <xplan:refTextInhalt xlink:href="#GML_90bd7307-fe86-44ce-bb2f-9bc6cbd2ed95" />
      <xplan:refTextInhalt xlink:href="#GML_2100a7ca-a80f-469c-92a9-b489692f93ce" />
      <xplan:refTextInhalt xlink:href="#GML_f40af8dd-3f2c-4531-851c-add7fb18f336" />
      <xplan:refTextInhalt xlink:href="#GML_cf5e49ce-9e68-4596-861d-25f91f294cf1" />
      <xplan:refTextInhalt xlink:href="#GML_360ccf4b-fcff-41e9-830f-98efc562c45d" />
      <xplan:refTextInhalt xlink:href="#GML_ac3f5786-7676-49c1-9bfb-619b6e96153a" />
      <xplan:refTextInhalt xlink:href="#GML_d6bdeec1-3419-4451-adb2-21aa355c5785" />
      <xplan:refTextInhalt xlink:href="#GML_10f1bbc4-6090-4a0f-ac37-4a948a466972" />
      <xplan:refTextInhalt xlink:href="#GML_788057ec-8264-473f-905b-bc1c6ec71f77" />
      <xplan:refTextInhalt xlink:href="#GML_2baf0b93-f279-405c-8a93-0f039968bf23" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_84B0E1CD-99EB-4739-A42D-D06D0768A912" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566945.886 5928746.812 566944.949 5928746.923 566946.166 5928757.372 
566916.653 5928760.799 566908.447 5928761.753 566905.732 5928755.922 
566900.803 5928719.935 566897.78 5928686.413 566895.6 5928654.6 
566904.351 5928653.486 566901.331 5928629.555 566900.362 5928621.875 
566903.817 5928621.184 566912.169 5928619.496 566914.007 5928657.173 
566929.808 5928655.271 566929.808 5928654.069 566971.145 5928649.093 
566966.384 5928609.517 567026.57 5928597.496 567040.56 5928594.705 
567052.362 5928593.249 567054.901 5928592.944 567055.259 5928597.17 
567055.504 5928600.913 567055.539 5928601.632 567056.469 5928609.412 
567060.062 5928612.442 567059.537 5928619.895 567059.222 5928625.202 
567059.157 5928626.277 567058.563 5928630.623 567057.847 5928634.303 
567054.65 5928633.366 567051.538 5928633.522 567048.481 5928634.285 
567045.46 5928635.342 567042.911 5928637.237 567040.48 5928639.274 
567038.662 5928641.832 567037.096 5928627.726 567035.641 5928614.614 
566986.4 5928620.631 566978.81 5928621.558 566973.083 5928619.447 
566974.89 5928625.319 566977.994 5928651.425 566935.463 5928656.417 
566936.294 5928672.939 566942.996 5928730.239 566943.902 5928730.131 
566945.886 5928746.812 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_c782b8a5-281d-429b-b8ed-46fc203cd1c0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566928.72 5928669.245</gml:lowerCorner>
          <gml:upperCorner>566928.72 5928669.245</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_7DB8E289-C544-4404-B3B9-4A5244BDF39F" srsName="EPSG:25832">
          <gml:pos>566928.72 5928669.245</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f04bb30b-0945-418d-8adb-648fc101e6b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566965.128 5928609.517</gml:lowerCorner>
          <gml:upperCorner>566971.145 5928649.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_AEE320CC-820C-469A-880A-73BFD09DAA13" srsName="EPSG:25832">
          <gml:posList>566965.128 5928649.817 566971.145 5928649.093 566966.384 5928609.517 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_4cbd12b1-a8e7-4487-832d-378f36217887">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.463 5928651.425</gml:lowerCorner>
          <gml:upperCorner>566977.994 5928836.124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_FB7B1C0C-71E4-47CB-B87C-5C51EEA285AB" srsName="EPSG:25832">
          <gml:posList>566977.994 5928651.425 566963.03 5928653.181 566935.463 5928656.417 
566936.294 5928672.939 566942.996 5928730.239 566943.902 5928730.131 
566945.886 5928746.812 566944.949 5928746.923 566946.166 5928757.372 
566951.371 5928757.416 566958.452 5928757.071 566965.415 5928815.612 
566967.855 5928836.124 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9b264a2a-e531-4c4d-a73f-f9fa5346ac0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566932.454 5928772.614</gml:lowerCorner>
          <gml:upperCorner>566937.179 5928792.52</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_22024D78-17BA-4D5C-9769-54B8ECEEBF84" srsName="EPSG:25832">
          <gml:posList>566937.179 5928792.52 566932.454 5928772.614 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_a0967d34-3528-4271-907a-23bf5d8556ce">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567034.788 5928607.591</gml:lowerCorner>
          <gml:upperCorner>567034.788 5928607.591</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_81FB6F3F-BD0B-4680-B368-FE245E7E21B3" srsName="EPSG:25832">
          <gml:pos>567034.788 5928607.591</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a21dba91-7d5b-4cfb-927e-f15d29154587">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567038.637 5928578.039</gml:lowerCorner>
          <gml:upperCorner>567058.097 5928627.012</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">19.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_baade317-ec9c-4a2c-86c3-fa9af52f8d7f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_6F992AF7-F65B-4992-925F-63005D05767E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567058.097 5928625.339 567044.201 5928627.012 567040.316 5928594.734 
567052.362 5928593.249 567050.872 5928582.934 567039.176 5928585.27 
567038.637 5928580.789 567052.403 5928578.039 567058.097 5928625.339 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_baade317-ec9c-4a2c-86c3-fa9af52f8d7f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567045.226 5928601.747</gml:lowerCorner>
          <gml:upperCorner>567045.226 5928601.747</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a21dba91-7d5b-4cfb-927e-f15d29154587" />
      <xplan:position>
        <gml:Point gml:id="Gml_DCEFB05B-8D17-40D7-A8DA-4361551E55F4" srsName="EPSG:25832">
          <gml:pos>567045.226 5928601.747</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_afb4a59f-bfb1-4388-b381-f3c763fe5f97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566901.511 5928603.061</gml:lowerCorner>
          <gml:upperCorner>566927.425 5928623.922</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:hMin uom="m">24.5</xplan:hMin>
          <xplan:hMax uom="m">26.5</xplan:hMax>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ecd8fdcd-601d-47c4-9fe6-3f0f7d062c67" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e5c1e368-63b3-481a-a0df-90e90f79222a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_460eb6e5-a1fc-420f-b78a-12002ba09d82" />
      <xplan:refTextInhalt xlink:href="#GML_e0ab737d-782e-4b19-94a1-d1b1716dc9ca" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_084579D0-70C4-45F9-87D8-63C6DF875388" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566926.63 5928623.432 566916.598 5928623.922 566916.46 5928621.105 
566914.662 5928621.192 566914.556 5928619.019 566912.169 5928619.496 
566903.077 5928621.313 566901.511 5928608.175 566927.139 5928603.061 
566927.425 5928608.941 566925.927 5928609.014 566926.63 5928623.432 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ecd8fdcd-601d-47c4-9fe6-3f0f7d062c67">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566901.893 5928611.383</gml:lowerCorner>
          <gml:upperCorner>566901.893 5928611.383</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_afb4a59f-bfb1-4388-b381-f3c763fe5f97" />
      <xplan:position>
        <gml:Point gml:id="Gml_4A149719-94C5-404B-8865-977D06CC4C87" srsName="EPSG:25832">
          <gml:pos>566901.893 5928611.383</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e5c1e368-63b3-481a-a0df-90e90f79222a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566920.278 5928617.772</gml:lowerCorner>
          <gml:upperCorner>566920.278 5928617.772</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_afb4a59f-bfb1-4388-b381-f3c763fe5f97" />
      <xplan:position>
        <gml:Point gml:id="Gml_FFB1770A-DD75-48C7-8793-FD36506765A9" srsName="EPSG:25832">
          <gml:pos>566920.278 5928617.772</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7000ab9c-4e86-4fe0-9fa2-be8ae1227a75">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566898.604 5928660.88</gml:lowerCorner>
          <gml:upperCorner>566927.019 5928757.981</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:hMin uom="m">20.5</xplan:hMin>
          <xplan:hMax uom="m">26.5</xplan:hMax>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ca170499-5b72-4e52-8165-39283beed034" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b38ec1cc-4e10-4c44-95d0-58a5df29be24" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_460eb6e5-a1fc-420f-b78a-12002ba09d82" />
      <xplan:refTextInhalt xlink:href="#GML_e0ab737d-782e-4b19-94a1-d1b1716dc9ca" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E7495AA9-6790-4465-B33B-267139A626F6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566927.019 5928745.763 566918.3 5928747.497 566919.411 5928756.731 
566915.82 5928757.163 566909.025 5928757.981 566908.78 5928744.21 
566905.827 5928731.115 566905.588 5928717.693 566902.635 5928704.597 
566902.396 5928691.175 566899.444 5928678.08 566899.205 5928664.658 
566898.604 5928661.996 566907.874 5928660.88 566908.806 5928668.622 
566910.864 5928685.724 566912.968 5928685.632 566919.745 5928685.337 
566920.635 5928692.73 566911.916 5928694.462 566914.008 5928711.843 
566916.112 5928711.752 566922.889 5928711.458 566923.875 5928719.644 
566917.222 5928720.967 566915.156 5928721.378 566916.419 5928731.87 
566917.248 5928738.758 566918.257 5928738.715 566919.352 5928738.667 
566926.129 5928738.374 566927.019 5928745.763 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ca170499-5b72-4e52-8165-39283beed034">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566912.371 5928741.333</gml:lowerCorner>
          <gml:upperCorner>566912.371 5928741.333</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7000ab9c-4e86-4fe0-9fa2-be8ae1227a75" />
      <xplan:position>
        <gml:Point gml:id="Gml_AA00E9CA-7E62-4A3D-9C86-9B3FF4D469B9" srsName="EPSG:25832">
          <gml:pos>566912.371 5928741.333</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b38ec1cc-4e10-4c44-95d0-58a5df29be24">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566906.249 5928683.033</gml:lowerCorner>
          <gml:upperCorner>566906.249 5928683.033</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7000ab9c-4e86-4fe0-9fa2-be8ae1227a75" />
      <xplan:position>
        <gml:Point gml:id="Gml_D1498E0B-8315-441D-B12A-F9122242C088" srsName="EPSG:25832">
          <gml:pos>566906.249 5928683.033</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">82.65</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_0b23f08b-8481-45b0-8652-9686874c6a5e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566947.948 5928757.305</gml:lowerCorner>
          <gml:upperCorner>566947.948 5928757.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_87BB4BB3-3B85-4E77-9FAC-AABA2573F9FF" srsName="EPSG:25832">
          <gml:pos>566947.948 5928757.305</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_50fadcf8-16f2-4331-b241-92bb254d09a6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566946.27 5928580.789</gml:lowerCorner>
          <gml:upperCorner>567039.176 5928603.652</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_48ba3f43-ffd8-45e3-a789-24d042a61a2a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_26D70CDC-920B-427F-85CF-23E875CC52E7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567039.176 5928585.27 566947.152 5928603.652 566946.27 5928599.239 
567038.637 5928580.789 567039.176 5928585.27 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_48ba3f43-ffd8-45e3-a789-24d042a61a2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566985.729 5928592.38</gml:lowerCorner>
          <gml:upperCorner>566985.729 5928592.38</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_50fadcf8-16f2-4331-b241-92bb254d09a6" />
      <xplan:position>
        <gml:Point gml:id="Gml_8851B8D1-7665-44AB-8F10-144B44C74515" srsName="EPSG:25832">
          <gml:pos>566985.729 5928592.38</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_cfda5888-7df1-4600-ae9f-391bf2d9d0fe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.989 5928762.526</gml:lowerCorner>
          <gml:upperCorner>566952.653 5928833.18</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2B5F1943-A10D-49D7-AEB8-BB47BC1AF0EC" srsName="EPSG:25832">
          <gml:posList>566930.448 5928773.255 566938.915 5928770.547 566937.011 5928762.526 
566930.251 5928763.592 566918.989 5928766.265 566920.531 5928770.703 
566922.439 5928785.382 566927.394 5928799.613 566929.364 5928814.552 
566934.263 5928828.55 566934.874 5928833.18 566945.863 5928830.571 
566952.653 5928828.422 566950.749 5928820.399 566944.048 5928821.457 
566941.968 5928821.785 566937.317 5928802.193 566939.323 5928801.551 
566945.784 5928799.484 566943.879 5928791.462 566937.179 5928792.52 
566935.099 5928792.848 566930.448 5928773.255 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_bf31bfb7-10d7-4baf-9ece-ffca4e13e789">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566947.668 5928776.879</gml:lowerCorner>
          <gml:upperCorner>566947.668 5928776.879</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_83095E43-F280-475C-BD85-893235A29E72" srsName="EPSG:25832">
          <gml:pos>566947.668 5928776.879</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ac8c6d30-6f6f-4712-8783-84a1a8cd8742">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566947.152 5928582.934</gml:lowerCorner>
          <gml:upperCorner>567052.362 5928612.968</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5A280A90-25CC-4605-AA66-9F525A8D79F2" srsName="EPSG:25832">
          <gml:posList>567040.316 5928594.734 567052.362 5928593.249 567050.872 5928582.934 
566947.152 5928603.652 566949.013 5928612.968 566956.371 5928611.498 
566956.371 5928611.498 567040.316 5928594.734 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3a7ef67e-271e-4050-b92a-a007914381b0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566942.541 5928664.974</gml:lowerCorner>
          <gml:upperCorner>566960.425 5928678.707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_572e6ba3-b24d-420e-a1d8-92a0d191a24e" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_766B8E98-888C-4343-ACDD-29EB275E5425" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566960.112 5928668.26 566959.417 5928668.344 566960.425 5928676.724 
566943.945 5928678.707 566942.541 5928667.042 566959.716 5928664.974 
566960.112 5928668.26 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_572e6ba3-b24d-420e-a1d8-92a0d191a24e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566945.814 5928670.368</gml:lowerCorner>
          <gml:upperCorner>566945.814 5928670.368</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3a7ef67e-271e-4050-b92a-a007914381b0" />
      <xplan:position>
        <gml:Point gml:id="Gml_D7CB3003-9387-4F33-AD8D-B6C7CBBBF8E0" srsName="EPSG:25832">
          <gml:pos>566945.814 5928670.368</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_7375a1d7-2dd7-448b-83a9-ee7e642ab442">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566915.909 5928709.678</gml:lowerCorner>
          <gml:upperCorner>566926.046 5928722.66</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_139E283F-3787-4A79-AE14-2A0A653CA83F" srsName="EPSG:25832">
          <gml:posList>566917.426 5928722.66 566926.046 5928720.946 566924.69 5928709.678 
566915.909 5928710.059 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_49a5a716-9059-458d-bc62-404aed47710a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566953.688 5928801.765</gml:lowerCorner>
          <gml:upperCorner>566953.688 5928801.765</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">3.6</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_0BDF6AC7-DEDF-4E82-A858-2498CF43DD08" srsName="EPSG:25832">
          <gml:pos>566953.688 5928801.765</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_4910f238-faca-446f-94f1-e0c2bc33e30c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566970.593 5928623.273</gml:lowerCorner>
          <gml:upperCorner>566970.593 5928623.273</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">2.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_F4CB352A-3D38-45F3-8011-FCD8BB05E47C" srsName="EPSG:25832">
          <gml:pos>566970.593 5928623.273</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_11d34213-60fa-4f45-926c-81362faebbb8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566896.791 5928556.513</gml:lowerCorner>
          <gml:upperCorner>567066.074 5928635.263</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_76F5A83A-EEEF-427B-838E-E9EF10059DB7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567064.294 5928580.853 567065.107 5928589.831 567065.46 5928594.606 
567065.73 5928599.027 567065.968 5928602.918 567066.023 5928606.369 
567066.074 5928609.776 567065.872 5928614.249 567065.672 5928618.433 
567065.313 5928622.817 567064.944 5928628.24 567064.461 5928631.632 
567063.773 5928635.263 567057.847 5928634.303 567058.563 5928630.623 
567059.157 5928626.277 567059.222 5928625.202 567059.537 5928619.895 
567060.062 5928612.442 567056.469 5928609.412 567055.539 5928601.632 
567055.504 5928600.913 567055.259 5928597.17 567054.901 5928592.944 
567053.456 5928575.994 567052.858 5928575.496 567038.561 5928577.69 
567031.628 5928578.85 567030.739 5928578.993 567024.212 5928580.069 
567023.611 5928580.191 567022.893 5928580.311 567022.017 5928580.492 
567017.463 5928581.435 567016.362 5928581.658 566997.782 5928585.507 
566992.262 5928586.796 566973.493 5928591.154 566965.812 5928592.878 
566956.712 5928594.942 566934.312 5928600.047 566929.402 5928601.144 
566925.462 5928602 566911.162 5928604.845 566902.662 5928606.537 
566898.531 5928607.364 566896.791 5928593.672 567055.545 5928557.101 
567061.825 5928556.513 567064.294 5928580.853 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ffa05cf8-6ad1-4cbb-8306-aabae383f865">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.3 5928736.196</gml:lowerCorner>
          <gml:upperCorner>566929.238 5928756.731</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f189e79e-d7e8-4b66-8d89-a8eebf6843c8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_DA5CCD50-AD4B-41DA-8380-CD329CD11165" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566929.238 5928747.463 566920.617 5928749.177 566921.496 5928756.48 
566919.411 5928756.731 566918.3 5928747.497 566927.019 5928745.763 
566926.129 5928738.374 566919.352 5928738.667 566919.101 5928736.576 
566927.881 5928736.196 566929.238 5928747.463 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f189e79e-d7e8-4b66-8d89-a8eebf6843c8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566927.516 5928740.876</gml:lowerCorner>
          <gml:upperCorner>566927.516 5928740.876</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ffa05cf8-6ad1-4cbb-8306-aabae383f865" />
      <xplan:position>
        <gml:Point gml:id="Gml_F11B1F13-18C3-4AAF-8548-F10EDDF93F6F" srsName="EPSG:25832">
          <gml:pos>566927.516 5928740.876</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_40cd9bfb-4670-41f0-af09-badb6cdb3ebb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566915.156 5928720.967</gml:lowerCorner>
          <gml:upperCorner>566919.352 5928738.758</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_4f65e9f6-20df-4476-b32c-5f1fd5c9fb7d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E59285DF-4091-4DDA-BD77-CC28C7420F77" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566919.352 5928738.667 566918.257 5928738.715 566917.248 5928738.758 
566916.419 5928731.87 566915.156 5928721.378 566917.222 5928720.967 
566919.352 5928738.667 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_4f65e9f6-20df-4476-b32c-5f1fd5c9fb7d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566917.027 5928728.415</gml:lowerCorner>
          <gml:upperCorner>566917.027 5928728.415</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_40cd9bfb-4670-41f0-af09-badb6cdb3ebb" />
      <xplan:position>
        <gml:Point gml:id="Gml_498DAFA4-89B9-467A-B279-624AB2E0E4D5" srsName="EPSG:25832">
          <gml:pos>566917.027 5928728.415</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_03c16333-1596-49d0-bb2a-8dbd105fccdb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567020.192 5928597.676</gml:lowerCorner>
          <gml:upperCorner>567029.128 5928616.079</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0272306f-1eb9-4f65-b515-6eb7857883a8" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_714C92C8-CF59-4B8F-821C-2EDBE62BA07E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567029.128 5928615.41 567023.653 5928616.079 567020.877 5928602.182 
567020.192 5928598.75 567025.586 5928597.676 567029.128 5928615.41 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_7cf9abe5-518d-4ea5-9216-c05116082abe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566878.799 5928556.513</gml:lowerCorner>
          <gml:upperCorner>567066.074 5928851.969</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_55F10200-0213-4E51-8F80-CB015A054A07" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566989.995 5928819.309 566991.874 5928835.45 566991.98 5928836.361 
566991.949 5928843.372 566973.25 5928844.271 566944.741 5928845.642 
566930.642 5928846.32 566930.846 5928850.794 566906.416 5928851.969 
566902.012 5928816.787 566902.856 5928816.553 566902.574 5928814.255 
566901.609 5928814.263 566892.384 5928754.204 566887.713 5928720.433 
566885.967 5928707.672 566885.457 5928703.927 566882.926 5928685.455 
566881.71 5928676.568 566880.008 5928663.883 566878.799 5928656.69 
566884.66 5928655.995 566895.6 5928654.6 566904.351 5928653.486 
566901.331 5928629.555 566900.362 5928621.875 566899.327 5928613.667 
566898.531 5928607.364 566896.791 5928593.672 567055.545 5928557.101 
567061.825 5928556.513 567064.294 5928580.853 567065.107 5928589.831 
567065.46 5928594.606 567065.73 5928599.027 567065.968 5928602.918 
567066.023 5928606.369 567066.074 5928609.776 567065.872 5928614.249 
567065.672 5928618.433 567065.313 5928622.817 567064.944 5928628.24 
567064.461 5928631.632 567063.773 5928635.263 567057.847 5928634.303 
567054.65 5928633.366 567051.538 5928633.522 567048.481 5928634.285 
567045.46 5928635.342 567042.911 5928637.237 567040.48 5928639.274 
567038.662 5928641.832 567037.096 5928627.726 567035.641 5928614.614 
566986.4 5928620.631 566978.81 5928621.558 566973.083 5928619.447 
566974.89 5928625.319 566977.994 5928651.425 566981.413 5928682.684 
566971.403 5928683.841 566979.782 5928753.468 567000.508 5928751.068 
567004.059 5928782.148 566985.912 5928784.23 566989.995 5928819.309 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>4000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_72b8fcde-50cf-443f-915d-6931542f01c3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566948.792 5928716.902</gml:lowerCorner>
          <gml:upperCorner>566966.676 5928730.635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_cf47acbb-a42e-4b3d-88a5-1f11ada5195b" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_91F120FB-2A72-4419-BF88-D5D35AFA6486" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566966.362 5928720.188 566965.667 5928720.272 566966.676 5928728.652 
566950.196 5928730.635 566948.792 5928718.97 566965.967 5928716.902 
566966.362 5928720.188 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cf47acbb-a42e-4b3d-88a5-1f11ada5195b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566952.251 5928722.486</gml:lowerCorner>
          <gml:upperCorner>566952.251 5928722.486</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_72b8fcde-50cf-443f-915d-6931542f01c3" />
      <xplan:position>
        <gml:Point gml:id="Gml_FEB563E4-1A6E-40D9-B703-5360453C3B77" srsName="EPSG:25832">
          <gml:pos>566952.251 5928722.486</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_af984b81-84e4-4be0-8511-9604089ff7b2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566911.916 5928694.052</gml:lowerCorner>
          <gml:upperCorner>566916.112 5928711.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3a666524-25ea-4852-953e-10a9ebd2f4db" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_D4A79F2C-5544-49D9-910F-24364F3F8C09" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566914.234 5928696.142 566915.909 5928710.059 566916.112 5928711.752 
566914.008 5928711.843 566911.916 5928694.462 566913.982 5928694.052 
566914.234 5928696.142 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3a666524-25ea-4852-953e-10a9ebd2f4db">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566913.754 5928702.879</gml:lowerCorner>
          <gml:upperCorner>566913.754 5928702.879</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_af984b81-84e4-4be0-8511-9604089ff7b2" />
      <xplan:position>
        <gml:Point gml:id="Gml_3DA8E13E-7897-42C0-864C-F2CDA2258783" srsName="EPSG:25832">
          <gml:pos>566913.754 5928702.879</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d789a998-4a33-4c4b-8b03-3c992b063d13">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566946.399 5928638.423</gml:lowerCorner>
          <gml:upperCorner>566965.128 5928651.907</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9931FC55-9B85-4BD6-8B54-25053DC0C048" srsName="EPSG:25832">
          <gml:posList>566963.756 5928638.423 566946.399 5928640.515 566947.77 5928651.907 
566965.128 5928649.817 566963.756 5928638.423 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_58714239-2266-45d0-bcfb-f94bf0bef6d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566917.222 5928720.967</gml:lowerCorner>
          <gml:upperCorner>566919.352 5928738.667</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_91F6052F-9A45-4A95-ACA5-CF8B91AC388C" srsName="EPSG:25832">
          <gml:posList>566919.352 5928738.667 566917.222 5928720.967 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_82c6ec87-d913-4b9d-bf22-c0acc96d3c64">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566911.466 5928620.932</gml:lowerCorner>
          <gml:upperCorner>566911.466 5928620.932</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_F481D091-2A1F-484F-A4A7-54BD779128AA" srsName="EPSG:25832">
          <gml:pos>566911.466 5928620.932</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_4e70711b-3273-4c69-8995-72bfc714b950">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567019.911 5928583.294</gml:lowerCorner>
          <gml:upperCorner>567019.911 5928583.294</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">2.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_85323AB0-B0BC-4858-B48D-26BAC2E34A4E" srsName="EPSG:25832">
          <gml:pos>567019.911 5928583.294</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_4ec32bf4-26e7-42a3-a59b-ec2445258328">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566971.107 5928832.145</gml:lowerCorner>
          <gml:upperCorner>566971.107 5928832.145</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_85EC95B1-AFE3-40B8-ACAF-A44CDC4C4EDE" srsName="EPSG:25832">
          <gml:pos>566971.107 5928832.145</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_d3385c6e-88a2-458e-89cf-7ef8bf7f5958">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566925.519 5928750.943</gml:lowerCorner>
          <gml:upperCorner>566925.519 5928750.943</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_A9C7C73E-BD79-4C7F-AA99-10CBA79C1526" srsName="EPSG:25832">
          <gml:pos>566925.519 5928750.943</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_01b7ba87-01fc-4591-bfcd-0a21bae148d3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566955.802 5928703.772</gml:lowerCorner>
          <gml:upperCorner>566955.802 5928703.772</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_5E835617-D591-400A-B743-9E7C92F3B9FD" srsName="EPSG:25832">
          <gml:pos>566955.802 5928703.772</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_ceaa274c-e83e-4ed9-9786-d62325d95626">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567037.111 5928625.202</gml:lowerCorner>
          <gml:upperCorner>567059.222 5928633.332</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c42a65e1-8f94-4df8-b8aa-02ee62cc078c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0272306f-1eb9-4f65-b515-6eb7857883a8" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_973FDB56-5243-40F9-8747-C9DB6478C326" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567059.157 5928626.277 567058.563 5928630.623 567058.524 5928630.826 
567037.718 5928633.332 567037.111 5928627.865 567059.222 5928625.202 
567059.157 5928626.277 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_c42a65e1-8f94-4df8-b8aa-02ee62cc078c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567039.869 5928629.134</gml:lowerCorner>
          <gml:upperCorner>567039.869 5928629.134</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>typ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ceaa274c-e83e-4ed9-9786-d62325d95626" />
      <xplan:schriftinhalt>Geh- und Fahrrecht</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_198150C1-2C0C-4520-810B-BF2403110174" srsName="EPSG:25832">
          <gml:pos>567039.869 5928629.134</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">353.11</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="GML_564f36e0-385a-4d23-865f-75ff1aed0b56">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566897.952 5928578.039</gml:lowerCorner>
          <gml:upperCorner>567058.097 5928760.751</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_BF57188D-5A3C-4770-B2F2-2963CE230614" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566946.776 5928757.301 566946.166 5928757.372 566917.069 5928760.751 
566915.82 5928757.163 566909.025 5928757.981 566909.02 5928757.653 
566906.64 5928757.939 566905.699 5928755.919 566905.036 5928751.081 
566908.895 5928750.617 566908.78 5928744.21 566905.827 5928731.115 
566905.588 5928717.693 566902.635 5928704.597 566902.396 5928691.175 
566899.444 5928678.08 566899.205 5928664.658 566897.952 5928659.105 
566909.607 5928657.703 566914.007 5928657.173 566912.169 5928619.496 
566903.077 5928621.313 566901.511 5928608.175 566927.139 5928603.061 
566946.27 5928599.239 566946.953 5928599.103 566946.583 5928597.25 
566955.546 5928595.208 566955.965 5928597.302 567039.621 5928580.593 
567052.403 5928578.039 567058.097 5928625.339 567044.201 5928627.012 
567044.159 5928626.664 567040.316 5928594.734 566966.382 5928609.499 
566971.145 5928649.093 566965.128 5928649.817 566962.661 5928650.114 
566964.054 5928661.682 566975.166 5928754.004 566946.776 5928757.301 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3200</xplan:zweckbestimmung>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_79e5aab9-6a2b-4697-811e-7b1ae912fdaf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.989 5928762.526</gml:lowerCorner>
          <gml:upperCorner>566952.653 5928833.18</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:hMin uom="m">20.5</xplan:hMin>
          <xplan:hMax uom="m">26.5</xplan:hMax>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8c05acb3-8a8d-4b71-ab66-73960ee93d49" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_cc84d7dd-9b5c-4f57-b247-e07a706c07a5" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_e0ab737d-782e-4b19-94a1-d1b1716dc9ca" />
      <xplan:refTextInhalt xlink:href="#GML_460eb6e5-a1fc-420f-b78a-12002ba09d82" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1C74CAFF-8924-4DC8-9AD1-49537CC3CAAD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566952.653 5928828.422 566945.863 5928830.571 566934.874 5928833.18 
566934.263 5928828.55 566929.364 5928814.552 566927.394 5928799.613 
566922.439 5928785.382 566920.531 5928770.703 566918.989 5928766.265 
566930.251 5928763.592 566937.011 5928762.526 566938.915 5928770.547 
566930.448 5928773.255 566935.099 5928792.848 566937.179 5928792.52 
566943.879 5928791.462 566945.784 5928799.484 566939.323 5928801.551 
566937.317 5928802.193 566941.968 5928821.785 566944.048 5928821.457 
566950.749 5928820.399 566952.653 5928828.422 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8c05acb3-8a8d-4b71-ab66-73960ee93d49">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566927.575 5928787.019</gml:lowerCorner>
          <gml:upperCorner>566927.575 5928787.019</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_79e5aab9-6a2b-4697-811e-7b1ae912fdaf" />
      <xplan:position>
        <gml:Point gml:id="Gml_B53A4756-1128-42C0-A43A-4D9AD8B555DA" srsName="EPSG:25832">
          <gml:pos>566927.575 5928787.019</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cc84d7dd-9b5c-4f57-b247-e07a706c07a5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566931.573 5928797.587</gml:lowerCorner>
          <gml:upperCorner>566931.573 5928797.587</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_79e5aab9-6a2b-4697-811e-7b1ae912fdaf" />
      <xplan:position>
        <gml:Point gml:id="Gml_CD7CE989-BC57-4961-B455-03E940EA5040" srsName="EPSG:25832">
          <gml:pos>566931.573 5928797.587</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">75.96</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_67612504-5754-4c90-b521-17cea9fab62e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567017.453 5928583.963</gml:lowerCorner>
          <gml:upperCorner>567025.586 5928598.75</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Durchfahrt und Durchgang</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:abweichenderHoehenbezug>als Mindestmaß</xplan:abweichenderHoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">3.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_0946a484-70bb-4303-9280-c24acadb9286" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0272306f-1eb9-4f65-b515-6eb7857883a8" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F42B73D2-FD53-42EF-BE8C-1907BEBE685C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567025.586 5928597.676 567020.192 5928598.75 567017.453 5928585.04 
567022.847 5928583.963 567025.586 5928597.676 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0946a484-70bb-4303-9280-c24acadb9286">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567021.526 5928591.436</gml:lowerCorner>
          <gml:upperCorner>567021.526 5928591.436</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_67612504-5754-4c90-b521-17cea9fab62e" />
      <xplan:position>
        <gml:Point gml:id="Gml_DD383204-B849-4F2F-8FFD-EEF130521C63" srsName="EPSG:25832">
          <gml:pos>567021.526 5928591.436</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_4a787277-f3ee-440b-b16a-600cb5419d26">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566936.785 5928789.49</gml:lowerCorner>
          <gml:upperCorner>566948.088 5928803.21</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_63731A81-8F50-4DAF-A04E-7A5706FFCF61" srsName="EPSG:25832">
          <gml:posList>566939.717 5928803.21 566948.088 5928800.532 566945.467 5928789.49 
566936.785 5928790.861 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_4a7df185-6df5-474f-98fa-ae74b8f0d637">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566915.82 5928757.163</gml:lowerCorner>
          <gml:upperCorner>566918.989 5928766.265</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9AC3A2E8-B74C-4F68-B83C-0615DC5E23B5" srsName="EPSG:25832">
          <gml:posList>566915.82 5928757.163 566918.989 5928766.265 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_599e8034-059f-4f61-8772-25e8ce204fc9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566946.776 5928751.068</gml:lowerCorner>
          <gml:upperCorner>567004.059 5928836.1382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_dfdfa5d3-6d06-420d-8845-27b43ed07cf6" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7bf9b41f-19e6-4264-9242-1d33a317e542" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_11776302-234f-4422-9f4c-a5e9252c889a" />
      <xplan:refTextInhalt xlink:href="#GML_e7713d5e-1b95-4b15-abf8-628ec4d14bad" />
      <xplan:refTextInhalt xlink:href="#GML_eb4982a4-8e29-49fa-9c6b-856b261ccb5a" />
      <xplan:refTextInhalt xlink:href="#GML_73c41f4a-8d57-4239-b1d2-d440315b55ea" />
      <xplan:refTextInhalt xlink:href="#GML_8839799e-5e0a-4a2e-a88b-c6e18047a0d4" />
      <xplan:refTextInhalt xlink:href="#GML_e088f923-cc26-4084-a05e-29831ae23397" />
      <xplan:refTextInhalt xlink:href="#GML_64060638-90c4-4dd9-8e64-787a46e192be" />
      <xplan:refTextInhalt xlink:href="#GML_864f30d7-31e2-480b-a8be-90f30ac58579" />
      <xplan:refTextInhalt xlink:href="#GML_90bd7307-fe86-44ce-bb2f-9bc6cbd2ed95" />
      <xplan:refTextInhalt xlink:href="#GML_2100a7ca-a80f-469c-92a9-b489692f93ce" />
      <xplan:refTextInhalt xlink:href="#GML_f40af8dd-3f2c-4531-851c-add7fb18f336" />
      <xplan:refTextInhalt xlink:href="#GML_cf5e49ce-9e68-4596-861d-25f91f294cf1" />
      <xplan:refTextInhalt xlink:href="#GML_360ccf4b-fcff-41e9-830f-98efc562c45d" />
      <xplan:refTextInhalt xlink:href="#GML_ac3f5786-7676-49c1-9bfb-619b6e96153a" />
      <xplan:refTextInhalt xlink:href="#GML_d6bdeec1-3419-4451-adb2-21aa355c5785" />
      <xplan:refTextInhalt xlink:href="#GML_10f1bbc4-6090-4a0f-ac37-4a948a466972" />
      <xplan:refTextInhalt xlink:href="#GML_788057ec-8264-473f-905b-bc1c6ec71f77" />
      <xplan:refTextInhalt xlink:href="#GML_2baf0b93-f279-405c-8a93-0f039968bf23" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_36737B5E-9F5C-494C-8D5E-BF52C56F616F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_09528A01-AA82-4141-B25D-E8E0581B0567" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566982.013 5928835.917 566977.758 5928836.119 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_8F8A67DD-F743-4254-93AE-5AC002173F18" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566977.758 5928836.119 566976.45304235 5928836.13100124 566975.151 5928836.043 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_959BFCCA-2139-4E6D-9D53-15888E824469" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566975.151 5928836.043 566973.399944569 5928835.9249984 566971.645 5928835.942 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_88D2CB65-CEFF-4BD2-8B96-EA182F40E15D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566971.645 5928835.942 566967.855 5928836.124 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E29B08BB-DC10-4585-816E-3013977DA8FF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566967.855 5928836.124 566958.452 5928757.071 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_283F9EE9-A522-43E8-B340-85E67E83A8F9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566958.452 5928757.071 566951.371 5928757.416 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CCC39989-CCB5-41DC-B25A-C46C112C10D6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566951.371 5928757.416 566946.776 5928757.301 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7F96EA25-7C37-4AAA-800F-49C277E4155F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566946.776 5928757.301 566979.782 5928753.468 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7BF44673-3F71-4AAA-87DA-55E78A1011FD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566979.782 5928753.468 567000.508 5928751.068 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7D1F78DE-A089-4F6F-B8AC-6781513A9363" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567000.508 5928751.068 567004.059 5928782.148 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3A5B4B48-CB3D-4956-B46F-8B99777526A0" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567004.059 5928782.148 566985.912 5928784.23 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B61C5BBF-EF47-4D10-8BE4-3EB2153E968E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566985.912 5928784.23 566989.995 5928819.309 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_02A55F12-9A16-4958-9D3F-21143C9A5C01" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566989.995 5928819.309 566991.874 5928835.45 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6E000CEA-CB7E-4C7C-A560-7618A0EB0AEF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566991.874 5928835.45 566982.013 5928835.917 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dfdfa5d3-6d06-420d-8845-27b43ed07cf6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566986.262 5928763.54</gml:lowerCorner>
          <gml:upperCorner>566986.262 5928763.54</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_599e8034-059f-4f61-8772-25e8ce204fc9" />
      <xplan:position>
        <gml:Point gml:id="Gml_9A1D91FC-F001-410D-9361-2529F7E9CCC6" srsName="EPSG:25832">
          <gml:pos>566986.262 5928763.54</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7bf9b41f-19e6-4264-9242-1d33a317e542">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566993.839 5928769.22</gml:lowerCorner>
          <gml:upperCorner>566993.839 5928769.22</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_599e8034-059f-4f61-8772-25e8ce204fc9" />
      <xplan:position>
        <gml:Point gml:id="Gml_308721E2-DD93-44D9-831D-A7A7498FE00B" srsName="EPSG:25832">
          <gml:pos>566993.839 5928769.22</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_67104753-3c83-4dbb-ae0f-73ab6554b481">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566940.227 5928661.682</gml:lowerCorner>
          <gml:upperCorner>566965.787 5928678.946</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_935B7362-D9BD-427B-A7D2-E9A3198BA74E" srsName="EPSG:25832">
          <gml:posList>566960.425 5928676.724 566965.787 5928676.078 566964.054 5928661.682 
566940.227 5928664.55 566941.96 5928678.946 566943.945 5928678.707 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_1b6b653b-ccd5-462b-9135-c1f704399899">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566897.952 5928621.105</gml:lowerCorner>
          <gml:upperCorner>566926.13 5928668.622</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_EEA2E7AF-C568-4893-A0B0-E664D56CDC0B" srsName="EPSG:25832">
          <gml:posList>566924.034 5928623.559 566916.598 5928623.922 566916.46 5928621.105 
566914.662 5928621.192 566916.537 5928659.634 566909.935 5928660.428 
566909.607 5928657.703 566897.952 5928659.105 566898.604 5928661.996 
566907.874 5928660.88 566908.806 5928668.622 566910.903 5928668.369 
566926.13 5928666.536 566924.034 5928623.559 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_1d919560-a0de-47fb-8303-5ef33913bac0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566933.428 5928648.407</gml:lowerCorner>
          <gml:upperCorner>566933.428 5928648.407</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_B9476072-D662-4048-A75D-5DF9C1D9C959" srsName="EPSG:25832">
          <gml:pos>566933.428 5928648.407</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0aafc0be-c225-4b1f-a4ac-1d0de3e9347d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566945.666 5928690.938</gml:lowerCorner>
          <gml:upperCorner>566963.55 5928704.671</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_26c8d1ad-96e2-424a-8a11-b14bdbbd66b8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7AD15057-359E-4537-B4E2-A955638BA0F6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566963.237 5928694.224 566962.542 5928694.308 566963.55 5928702.688 
566947.071 5928704.671 566945.666 5928693.006 566962.841 5928690.938 
566963.237 5928694.224 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_26c8d1ad-96e2-424a-8a11-b14bdbbd66b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566949.544 5928696.852</gml:lowerCorner>
          <gml:upperCorner>566949.544 5928696.852</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0aafc0be-c225-4b1f-a4ac-1d0de3e9347d" />
      <xplan:position>
        <gml:Point gml:id="Gml_E548267F-BB07-4866-ACAF-825D74488CB9" srsName="EPSG:25832">
          <gml:pos>566949.544 5928696.852</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_41be559e-6a93-4ad6-8539-816be658196c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566898.604 5928660.88</gml:lowerCorner>
          <gml:upperCorner>566927.019 5928757.981</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5015D33B-593C-4558-8AFA-71126E84B408" srsName="EPSG:25832">
          <gml:posList>566907.874 5928660.88 566898.604 5928661.996 566899.205 5928664.658 
566899.444 5928678.08 566902.396 5928691.175 566902.635 5928704.597 
566905.588 5928717.693 566905.827 5928731.115 566908.78 5928744.21 
566909.025 5928757.981 566915.82 5928757.163 566919.411 5928756.731 
566918.3 5928747.497 566927.019 5928745.763 566926.129 5928738.374 
566919.352 5928738.667 566918.257 5928738.715 566917.248 5928738.758 
566916.419 5928731.87 566915.156 5928721.378 566917.222 5928720.967 
566923.875 5928719.644 566922.889 5928711.458 566916.112 5928711.752 
566914.008 5928711.843 566911.916 5928694.462 566920.635 5928692.73 
566919.745 5928685.337 566912.968 5928685.632 566910.864 5928685.724 
566908.806 5928668.622 566907.874 5928660.88 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_5369874b-e5f5-486e-90cb-5429a0b02078">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566944.111 5928619.117</gml:lowerCorner>
          <gml:upperCorner>566957.288 5928652.146</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_AEF5D3EA-3BC2-454E-9E2A-57776986DC02" srsName="EPSG:25832">
          <gml:posList>566957.288 5928619.117 566953.491 5928619.574 566955.572 5928636.87 
566944.111 5928638.249 566945.784 5928652.146 566947.77 5928651.907 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_7138be9d-bc5a-45e3-a6d4-a940d9924bb9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566944.78 5928805.823</gml:lowerCorner>
          <gml:upperCorner>566944.78 5928805.823</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_313DC60F-7DE5-4E19-9976-3255C1E218D8" srsName="EPSG:25832">
          <gml:pos>566944.78 5928805.823</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ccc84186-804a-48c9-b099-3bbe0f111452">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566951.917 5928742.866</gml:lowerCorner>
          <gml:upperCorner>566969.801 5928756.599</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_201F16C9-696C-40CF-BC79-2142F8608336" srsName="EPSG:25832">
          <gml:posList>566968.792 5928746.236 566969.487 5928746.152 566969.092 5928742.866 
566951.917 5928744.933 566953.321 5928756.599 566969.801 5928754.615 
566968.792 5928746.236 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_0a76c5d8-54dd-45df-b7f5-311e4d3de4cc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566913.982 5928694.055</gml:lowerCorner>
          <gml:upperCorner>566916.112 5928711.752</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3A4901A2-CC43-41C9-8316-116C23FE8325" srsName="EPSG:25832">
          <gml:posList>566916.112 5928711.752 566913.982 5928694.055 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_155a2ab3-4d55-441a-a4f0-a94dcd9dc221">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567018.408 5928598.145</gml:lowerCorner>
          <gml:upperCorner>567018.408 5928598.145</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">2.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_36495B7D-63F2-4B30-AD31-CB4FB9A98516" srsName="EPSG:25832">
          <gml:pos>567018.408 5928598.145</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_500702aa-4cdc-486a-9f80-bce091bf4844">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.639 5928781.029</gml:lowerCorner>
          <gml:upperCorner>566938.639 5928781.029</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_3CBCD592-B9A9-4851-8C96-3A8389C235DD" srsName="EPSG:25832">
          <gml:pos>566938.639 5928781.029</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_348fa459-47c8-4b02-9bb7-dd542f064eee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566933.107 5928601.787</gml:lowerCorner>
          <gml:upperCorner>566933.107 5928601.787</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_45D4193A-880E-49BB-91DE-D87EBE1E7849" srsName="EPSG:25832">
          <gml:pos>566933.107 5928601.787</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_eca1a491-90da-4418-a750-02244bf75941">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.965 5928754.002</gml:lowerCorner>
          <gml:upperCorner>566976.965 5928754.002</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">3.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_29B29C4E-04D1-4729-B156-E615C7F4A85F" srsName="EPSG:25832">
          <gml:pos>566976.965 5928754.002</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_SchutzgebietNaturschutzrecht gml:id="GML_218706d3-7382-404e-b3c6-d01478b2a006">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566898.083 5928687.765</gml:lowerCorner>
          <gml:upperCorner>566924.683 5928765.665</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e89666dd-2e3c-4ae6-9810-6a0e19862650" />
      <xplan:rechtscharakter>2000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_D7E4EF2C-D302-47A1-A52E-DE81B498E30A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566915.783 5928763.265 566915.183 5928763.965 566914.283 5928764.465 
566912.383 5928765.465 566911.183 5928765.565 566909.483 5928765.665 
566907.783 5928765.565 566906.583 5928765.165 566904.983 5928764.165 
566904.083 5928763.065 566902.883 5928760.565 566902.283 5928758.365 
566901.983 5928756.165 566901.683 5928753.765 566901.183 5928745.265 
566900.683 5928737.865 566899.083 5928710.865 566898.083 5928693.365 
566898.183 5928691.165 566898.483 5928690.665 566900.383 5928688.665 
566902.383 5928687.765 566902.983 5928687.965 566905.783 5928690.565 
566907.683 5928692.565 566913.883 5928699.665 566918.683 5928705.065 
566920.483 5928708.565 566921.483 5928712.765 566921.183 5928713.065 
566910.483 5928717.965 566908.983 5928726.965 566909.983 5928739.865 
566916.383 5928743.765 566924.683 5928747.765 566924.283 5928756.765 
566924.083 5928759.665 566923.583 5928760.865 566922.983 5928762.165 
566922.283 5928763.065 566920.983 5928763.965 566919.983 5928764.165 
566918.783 5928764.065 566917.783 5928763.665 566916.383 5928762.365 
566915.783 5928763.265 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:artDerFestlegung>1700</xplan:artDerFestlegung>
    </xplan:SO_SchutzgebietNaturschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e89666dd-2e3c-4ae6-9810-6a0e19862650">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566911.398 5928752.194</gml:lowerCorner>
          <gml:upperCorner>566911.398 5928752.194</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>artDerFestlegung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_218706d3-7382-404e-b3c6-d01478b2a006" />
      <xplan:position>
        <gml:Point gml:id="Gml_95AE4D1A-3FFD-4B7E-8AD1-F89B43226982" srsName="EPSG:25832">
          <gml:pos>566911.398 5928752.194</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_f743be71-2921-427c-9db3-efd595dda84f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566920.025 5928724.897</gml:lowerCorner>
          <gml:upperCorner>566920.025 5928724.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_39320F5F-0C9D-42CA-AE7C-77B8BE3EBBA7" srsName="EPSG:25832">
          <gml:pos>566920.025 5928724.897</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_5970e72d-6406-4baf-be5f-f66401d5e05b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.879 5928758.625</gml:lowerCorner>
          <gml:upperCorner>566924.097 5928764.926</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_0272306f-1eb9-4f65-b515-6eb7857883a8" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F9A094E9-DC1F-47B9-8611-B20EFF1AC073" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566924.097 5928763.305 566918.523 5928764.926 566916.879 5928760.206 
566922.314 5928758.625 566924.097 5928763.305 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:typ>1000</xplan:typ>
      <xplan:typ>2000</xplan:typ>
      <xplan:istSchmal>false</xplan:istSchmal>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_c8ba83c0-814c-4957-9477-1f7bd1399d88">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566878.799 5928654.6</gml:lowerCorner>
          <gml:upperCorner>566991.98 5928851.969</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3A1EBA83-930E-422A-ABE5-B24DDA3DC57C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1A709E5A-C17A-4E5B-814A-53074A9C077B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566991.949 5928843.372 566973.25 5928844.271 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_42FDB3AF-1971-4BAB-9DE9-693FAAC113C5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566973.25 5928844.271 566944.741 5928845.642 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_08614DAB-B652-460A-9F6C-A9BD3AA3A12C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566944.741 5928845.642 566930.642 5928846.32 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_27129FE7-0895-45B6-A911-E2F730F18CF7" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566930.642 5928846.32 566930.846 5928850.794 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1FD26124-DB68-4386-88D0-A8095B5247C9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566930.846 5928850.794 566906.416 5928851.969 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_87E2F7F0-2B8D-4CB9-B022-ED1289177DD2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566906.416 5928851.969 566902.012 5928816.787 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B73382E0-9F44-4E00-9F9E-30346638BB93" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566902.012 5928816.787 566902.856 5928816.553 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_DC2DE09E-45B7-418A-B35D-34DEF5E7804F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566902.856 5928816.553 566902.574 5928814.255 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1F24113B-36F4-4672-94A0-3BE7E7927726" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566902.574 5928814.255 566901.609 5928814.263 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_320A432D-8830-473B-85CD-AB60C5D9C4DA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566901.609 5928814.263 566892.384 5928754.204 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_D44833C5-762A-429C-947C-C9351404588D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566892.384 5928754.204 566887.713 5928720.433 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A177036A-C5EC-4F2D-8D80-EC291AD4840B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566887.713 5928720.433 566885.967 5928707.672 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E9DC949D-F08A-46B5-9A30-42DB9FFD5EEB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566885.967 5928707.672 566885.457 5928703.927 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6860E37F-216C-4050-B7A0-6B7CFAC88984" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566885.457 5928703.927 566882.926 5928685.455 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_470C72E2-936C-43B0-BB8F-A54C4DB79C76" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566882.926 5928685.455 566881.71 5928676.568 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1FDB7E13-BDF8-40D3-8B07-424601E57B1E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566881.71 5928676.568 566880.008 5928663.883 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8A4BFF7F-3BC4-42FB-BE73-DF15D4AE36D4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566880.008 5928663.883 566878.799 5928656.69 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_230F2ED5-3DAD-4611-9619-428BABF4F7BC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566878.799 5928656.69 566884.66 5928655.995 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CD7DC159-2CB4-4202-B368-014F71C4932A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566884.66 5928655.995 566895.6 5928654.6 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_57E3E04D-8D49-4A93-9EAC-B8B223406DDD" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566895.6 5928654.6 566897.78 5928686.413 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_73A979A3-1769-4713-8C93-F633F5E768A8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566897.78 5928686.413 566900.803 5928719.935 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7F274087-8127-478D-8F2C-B2C81EB580CF" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566900.803 5928719.935 566905.732 5928755.922 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_125923E6-EEE9-4CA6-9A01-5D21E334C0CA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566905.732 5928755.922 566908.447 5928761.753 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_21ACBDA2-0466-48FA-9DEE-7911C8EFCA7C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566908.447 5928761.753 566916.653 5928760.799 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FABE8D32-5F9D-41AB-8903-097B61F09D57" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566916.653 5928760.799 566917.587 5928765.033 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_859DC155-A9A2-4671-9A0C-53C1AEB6805D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566917.587 5928765.033 566921.405 5928780.721 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E032481A-AEA9-4D57-8C2E-2B3C06729954" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566921.405 5928780.721 566918.72 5928781.621 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_874902EF-2EB9-46DF-881F-534A7A4CFAC9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566918.72 5928781.621 566929.725 5928826.17 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3E801D99-3F79-40ED-9261-D05B1BA7A022" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566929.725 5928826.17 566931.97 5928835.26 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_76708E66-0B92-403E-9B7A-8EF98C3FDEAC" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566931.97 5928835.26 566935.236 5928837.693 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0E0A2090-1745-4742-938D-586747E0330D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566935.236 5928837.693 566967.855 5928836.124 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_899A6153-77A0-4B5F-98E4-FB1C2A1F8157" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566967.855 5928836.124 566971.645 5928835.942 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_31B64555-8A49-4EEC-A406-31FD6412F4DC" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566971.645 5928835.942 566973.399944569 5928835.9249984 566975.151 5928836.043 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_0FE2A426-1147-4E74-B690-6CAD19836EA0" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">566975.151 5928836.043 566976.45304235 5928836.13100124 566977.758 5928836.119 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_37DC0B50-D9EB-4AEE-B185-0920BD08485A" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566977.758 5928836.119 566982.013 5928835.917 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0B55F6A1-2AD9-4AD8-918A-45D8FB38FB64" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566982.013 5928835.917 566991.874 5928835.45 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7E5A102E-8DA7-4464-BBBF-F81A8C7F443E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566991.874 5928835.45 566991.98 5928836.361 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_F6F2CACB-DA12-477F-93F2-66040A53D756" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">566991.98 5928836.361 566991.949 5928843.372 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_dca1ce58-f29c-4de4-b94d-5b76b3c1839c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566909.607 5928619.496</gml:lowerCorner>
          <gml:upperCorner>566914.007 5928657.703</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B685B0C9-7EA3-46C5-92DC-473E2B64FABF" srsName="EPSG:25832">
          <gml:posList>566912.169 5928619.496 566914.007 5928657.173 566909.607 5928657.703 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_e30a7ecf-77af-4399-be3f-6dc36130908e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566961.886 5928754.713</gml:lowerCorner>
          <gml:upperCorner>566961.886 5928754.713</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_0A0F2B2E-BAEF-4E9E-9231-F2FAD0D22863" srsName="EPSG:25832">
          <gml:pos>566961.886 5928754.713</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_408a2db3-b3fc-4e7e-9042-b7ec0d7ab52e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566964.271 5928772.613</gml:lowerCorner>
          <gml:upperCorner>566964.271 5928772.613</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_F9DEA04A-BA6F-45CD-A8CE-682998A8C74D" srsName="EPSG:25832">
          <gml:pos>566964.271 5928772.613</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_049edc40-a357-47a3-ab04-77fcef27142a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566951.167 5928830.681</gml:lowerCorner>
          <gml:upperCorner>566951.167 5928830.681</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_E09AD0AD-9497-44F9-952C-F27F8DFB45FA" srsName="EPSG:25832">
          <gml:pos>566951.167 5928830.681</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="GML_8e33947b-6e48-4473-9aab-ea6bdb887959">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566917.069 5928754.004</gml:lowerCorner>
          <gml:upperCorner>566984.257 5928836.141</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5d9b3838-d18d-429e-9741-481aab1fdefe" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_92D09A7A-9251-4F65-BF6F-1CE519B0A8D5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566977.497 5928836.122 566976.209 5928836.141 566974.976 5928836.043 
566973.843 5928835.96 566972.245 5928835.93 566971.435 5928835.969 
566970.854 5928831.144 566968.934 5928815.189 566943.293 5928818.274 
566943.655 5928819.798 566952.336 5928818.428 566954.957 5928829.469 
566946.591 5928832.145 566935.097 5928834.874 566934.263 5928828.55 
566929.364 5928814.552 566927.394 5928799.613 566922.439 5928785.382 
566920.531 5928770.703 566918.989 5928766.265 566917.069 5928760.751 
566946.166 5928757.372 566946.776 5928757.301 566975.166 5928754.004 
566976.728 5928766.981 566984.257 5928829.531 566976.811 5928830.427 
566977.497 5928836.122 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3200</xplan:zweckbestimmung>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5d9b3838-d18d-429e-9741-481aab1fdefe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.139 5928776.176</gml:lowerCorner>
          <gml:upperCorner>566938.139 5928776.176</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8e33947b-6e48-4473-9aab-ea6bdb887959" />
      <xplan:position>
        <gml:Point gml:id="Gml_E1CAAE4A-A852-4E11-96CD-3AF516283610" srsName="EPSG:25832">
          <gml:pos>566938.139 5928776.176</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_343e543b-dd39-4403-ba7f-b3d91820c7d6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566913.065 5928653.416</gml:lowerCorner>
          <gml:upperCorner>566913.065 5928653.416</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_A0A885FC-B1B8-4D5B-A0AB-99BBEC28CE3C" srsName="EPSG:25832">
          <gml:pos>566913.065 5928653.416</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_87107f3d-7fa8-47f2-b7a3-5f778d068f39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567038.637 5928578.039</gml:lowerCorner>
          <gml:upperCorner>567058.097 5928627.012</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_97DF26A2-874D-439E-B315-3B61998BC653" srsName="EPSG:25832">
          <gml:posList>567040.316 5928594.734 567044.201 5928627.012 567058.097 5928625.339 
567052.403 5928578.039 567038.637 5928580.789 567039.176 5928585.27 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_eae29f33-edb4-416e-901f-10d787888388">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566917.182 5928752.989</gml:lowerCorner>
          <gml:upperCorner>566999.473 5928762.121</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_69A2A5E3-B3C3-47D1-95BB-3B1FF39D8F13" srsName="EPSG:25832">
          <gml:posList>566999.473 5928752.989 566958.327 5928757.555 566929.452 5928760.759 
566917.182 5928762.121 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:zweckbestimmung>18000</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_WasserwirtschaftsFlaeche gml:id="GML_8522ed2f-2da1-446c-b7e2-c81c3e840252">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566934.561 5928927.764</gml:lowerCorner>
          <gml:upperCorner>566982.93 5928990.061</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Regelung des Wasserabflusses</xplan:text>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_eca1dcff-a5d5-486b-8b60-a4c14159518b" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_723946D3-464B-4E67-B408-A0557C7F69FD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566982.93 5928986.485 566943.496 5928990.061 566936.961 5928985.021 
566934.561 5928932.363 566950.718 5928930.642 566973.755 5928928.189 
566977.742 5928927.764 566982.93 5928986.485 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_WasserwirtschaftsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_92d6616b-cccb-4c25-81f9-c132e16703fb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566921.496 5928756.48</gml:lowerCorner>
          <gml:upperCorner>566941.219 5928774.273</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_45281784-681E-4BDF-9920-E0719B75F3A3" srsName="EPSG:25832">
          <gml:posList>566932.848 5928774.273 566941.219 5928771.595 566938.598 5928760.553 
566929.921 5928761.923 566924.097 5928763.305 566921.496 5928756.48 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_58dca3ea-a5ac-4e9c-80b9-7dd0e36c09e9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566964.755 5928767.16</gml:lowerCorner>
          <gml:upperCorner>566984.257 5928830.972</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_9F80AAE3-D267-4937-9B36-CE623994C241" srsName="EPSG:25832">
          <gml:posList>566976.954 5928781.411 566978.444 5928781.232 566978.021 5928777.722 
566976.532 5928777.901 566975.239 5928767.16 566964.755 5928768.422 
566972.284 5928830.972 566984.257 5928829.531 566982.991 5928819.012 
566981.502 5928819.191 566979.432 5928801.996 566980.921 5928801.817 
566980.499 5928798.307 566979.01 5928798.487 566976.954 5928781.411 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_88d02fab-40aa-4e3c-9668-d82f0adcddee">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566945.666 5928690.938</gml:lowerCorner>
          <gml:upperCorner>566963.55 5928704.671</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B11D3129-27B3-4BF5-B236-17C23E34C09F" srsName="EPSG:25832">
          <gml:posList>566962.542 5928694.308 566963.237 5928694.224 566962.841 5928690.938 
566945.666 5928693.006 566947.071 5928704.671 566963.55 5928702.688 
566962.542 5928694.308 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_cd16e448-f2a9-4544-8a4e-f9a7021f93e3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566919.1 5928736.196</gml:lowerCorner>
          <gml:upperCorner>566929.238 5928756.731</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_DAA36936-8A58-4071-B3DE-ED66B73E2367" srsName="EPSG:25832">
          <gml:posList>566919.411 5928756.731 566921.496 5928756.48 566920.617 5928749.177 
566929.238 5928747.463 566927.881 5928736.196 566919.1 5928736.576 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NebenanlagenFlaeche gml:id="GML_dff52785-cbf2-4510-b279-8acb077d88f9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566966.382 5928594.734</gml:lowerCorner>
          <gml:upperCorner>567044.201 5928627.678</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ed778106-c04d-4c5b-bcb4-b88f7f3a3a31" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_37F067B6-E4DB-4FDD-B779-FB07F19B6F0F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567044.201 5928627.012 567038.661 5928627.678 567036.181 5928607.066 
566974.644 5928619.354 566974.478 5928618.52 566967.631 5928619.887 
566966.382 5928609.499 567040.316 5928594.734 567041.067 5928600.98 
567044.201 5928627.012 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>3200</xplan:zweckbestimmung>
    </xplan:BP_NebenanlagenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ed778106-c04d-4c5b-bcb4-b88f7f3a3a31">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567003.779 5928608.717</gml:lowerCorner>
          <gml:upperCorner>567003.779 5928608.717</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_dff52785-cbf2-4510-b279-8acb077d88f9" />
      <xplan:position>
        <gml:Point gml:id="Gml_2B49F15C-316E-4941-8CA8-56C9A5381F1F" srsName="EPSG:25832">
          <gml:pos>567003.779 5928608.717</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_80587e28-d7ea-4f07-a861-1dd151704ea4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566963.325 5928768.422</gml:lowerCorner>
          <gml:upperCorner>566972.284 5928831.144</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_76411A45-DEE6-414E-8D3B-BE63D9A37491" srsName="EPSG:25832">
          <gml:posList>566964.755 5928768.422 566963.325 5928768.594 566970.854 5928831.144 
566972.284 5928830.972 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_05863e5d-a599-4f3c-9d62-d85479fea110">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566946.399 5928638.423</gml:lowerCorner>
          <gml:upperCorner>566965.128 5928651.907</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c51ec6c6-6db2-4ee5-a288-73d871577816" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_19401993-44A9-4566-9375-16F2E5DAC306" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566965.128 5928649.817 566947.77 5928651.907 566946.399 5928640.515 
566963.756 5928638.423 566965.128 5928649.817 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c51ec6c6-6db2-4ee5-a288-73d871577816">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566950.89 5928644.865</gml:lowerCorner>
          <gml:upperCorner>566950.89 5928644.865</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_05863e5d-a599-4f3c-9d62-d85479fea110" />
      <xplan:position>
        <gml:Point gml:id="Gml_4C280B32-BC72-4093-90E4-BF9D6CF4AC2C" srsName="EPSG:25832">
          <gml:pos>566950.89 5928644.865</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9d2de821-f95e-48f0-8631-56b16734d5e5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566947.152 5928582.934</gml:lowerCorner>
          <gml:upperCorner>567052.362 5928612.968</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_358420ca-8922-49bc-b400-dffbcb5307f9" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_4FF79C4E-581D-47CC-B12B-6075570A9D7C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567052.362 5928593.249 567040.316 5928594.734 566956.371 5928611.498 
566949.013 5928612.968 566947.152 5928603.652 567050.872 5928582.934 
567052.362 5928593.249 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_358420ca-8922-49bc-b400-dffbcb5307f9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567003.358 5928594.941</gml:lowerCorner>
          <gml:upperCorner>567003.358 5928594.941</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d2de821-f95e-48f0-8631-56b16734d5e5" />
      <xplan:position>
        <gml:Point gml:id="Gml_9C7ED02C-60BC-4626-A4F0-CFE1250D7A82" srsName="EPSG:25832">
          <gml:pos>567003.358 5928594.941</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="GML_0f6d8c8d-1993-4b47-8503-06e9d907d248">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566934.561 5928927.764</gml:lowerCorner>
          <gml:upperCorner>566984.153 5929011.434</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_b1deae92-3045-416a-9101-bc9990957e76" />
      <xplan:refTextInhalt xlink:href="#GML_eca1dcff-a5d5-486b-8b60-a4c14159518b" />
      <xplan:refTextInhalt xlink:href="#GML_ce5d4b3b-1f77-4649-87d3-d8e2aae0c0f9" />
      <xplan:refTextInhalt xlink:href="#GML_033c66c2-9bdf-477e-9115-e4fa7acf7483" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8A7A9159-49B9-4C91-A741-C0BDA7484AA2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566984.153 5929000.331 566971.214 5929011.434 566955.549 5928999.355 
566936.961 5928985.021 566934.561 5928932.363 566950.718 5928930.642 
566973.755 5928928.189 566977.742 5928927.764 566984.153 5929000.331 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_54b8d8a6-5846-4160-8c96-1960cd987eb4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.954 5928781.232</gml:lowerCorner>
          <gml:upperCorner>566980.499 5928798.487</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">14.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_52427f6b-ca18-4dfd-8c73-0edb82799d3d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8BEEBDB2-0B10-45D7-A576-205120545621" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566980.499 5928798.307 566979.01 5928798.487 566976.954 5928781.411 
566978.444 5928781.232 566980.499 5928798.307 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_52427f6b-ca18-4dfd-8c73-0edb82799d3d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566978.331 5928790.911</gml:lowerCorner>
          <gml:upperCorner>566978.331 5928790.911</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_54b8d8a6-5846-4160-8c96-1960cd987eb4" />
      <xplan:position>
        <gml:Point gml:id="Gml_6D9EF681-B227-40E5-9AAB-30BAC05BF435" srsName="EPSG:25832">
          <gml:pos>566978.331 5928790.911</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_29d0a4c8-7a50-4ffe-bc06-8c4f61a8a0ef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566909.607 5928619.019</gml:lowerCorner>
          <gml:upperCorner>566916.537 5928660.428</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">9</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9134ac06-f447-4b4c-9a53-34d7f1c5ba80" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_07394957-0EBE-432C-AE9D-44EAC0FB4354" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566914.662 5928621.192 566916.537 5928659.634 566909.935 5928660.428 
566909.607 5928657.703 566914.007 5928657.173 566912.169 5928619.496 
566914.556 5928619.019 566914.662 5928621.192 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9134ac06-f447-4b4c-9a53-34d7f1c5ba80">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566914.022 5928637.725</gml:lowerCorner>
          <gml:upperCorner>566914.022 5928637.725</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_29d0a4c8-7a50-4ffe-bc06-8c4f61a8a0ef" />
      <xplan:position>
        <gml:Point gml:id="Gml_3D5B05B8-F18E-4B3F-9663-EC0F40774D97" srsName="EPSG:25832">
          <gml:pos>566914.022 5928637.725</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">85.09</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_708a73b8-f756-4be8-84c7-78ae2267183f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566934.78 5928720.122</gml:lowerCorner>
          <gml:upperCorner>566934.78 5928720.122</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_4A5DFB69-850B-4E52-9697-1B85A509D0DB" srsName="EPSG:25832">
          <gml:pos>566934.78 5928720.122</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d1413c00-fb27-438c-a682-e325229d9c57">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566946.27 5928580.789</gml:lowerCorner>
          <gml:upperCorner>567038.637 5928603.652</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D0064A8E-B7FB-40FD-BD2B-E90D98FEFA51" srsName="EPSG:25832">
          <gml:posList>567038.637 5928580.789 566946.27 5928599.239 566947.152 5928603.652 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_6c415562-5879-482e-a006-38edcd21cd82">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566958.852 5928729.233</gml:lowerCorner>
          <gml:upperCorner>566958.852 5928729.233</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_8AF18D97-E2FD-4F2B-A36D-E1617D2E6B5A" srsName="EPSG:25832">
          <gml:pos>566958.852 5928729.233</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6428bb15-14b7-4674-8a25-18ae66143a05">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566901.511 5928603.061</gml:lowerCorner>
          <gml:upperCorner>566927.425 5928623.922</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_723B0290-8C87-4DD3-BC50-E0CD64222571" srsName="EPSG:25832">
          <gml:posList>566925.927 5928609.014 566927.425 5928608.941 566927.139 5928603.061 
566901.511 5928608.175 566903.077 5928621.313 566912.169 5928619.496 
566914.556 5928619.019 566914.662 5928621.192 566916.46 5928621.105 
566916.598 5928623.922 566926.63 5928623.432 566925.927 5928609.014 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_265ba814-b5d6-4561-b317-496d53378093">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566936.785 5928789.49</gml:lowerCorner>
          <gml:upperCorner>566948.088 5928803.21</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">20.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_81f7e70f-7cd0-478f-ba88-dcbb3f4a6135" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_33E508E7-90C8-4C51-A764-C927774F07ED" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566948.088 5928800.532 566939.717 5928803.21 566939.323 5928801.551 
566945.784 5928799.484 566943.879 5928791.462 566937.179 5928792.52 
566936.785 5928790.861 566945.467 5928789.49 566948.088 5928800.532 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_81f7e70f-7cd0-478f-ba88-dcbb3f4a6135">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566945.833 5928794.439</gml:lowerCorner>
          <gml:upperCorner>566945.833 5928794.439</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_265ba814-b5d6-4561-b317-496d53378093" />
      <xplan:position>
        <gml:Point gml:id="Gml_04163F21-6086-4708-B08F-24DF7A5FADD4" srsName="EPSG:25832">
          <gml:pos>566945.833 5928794.439</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_de3baf7c-3441-4345-8f1d-ae6c56d3d2a2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566952.78 5928678.299</gml:lowerCorner>
          <gml:upperCorner>566952.78 5928678.299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_1C504CAF-45A3-490F-82F9-152192C5DCF6" srsName="EPSG:25832">
          <gml:pos>566952.78 5928678.299</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_83014b7f-1c28-40f1-bedc-3d83f4b80ad7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566934.874 5928818.428</gml:lowerCorner>
          <gml:upperCorner>566954.957 5928834.874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3766225D-F758-471D-AEA1-876B370CBDFE" srsName="EPSG:25832">
          <gml:posList>566934.874 5928833.18 566935.097 5928834.874 566946.591 5928832.145 
566954.957 5928829.469 566952.336 5928818.428 566943.655 5928819.798 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_91a139e2-bc14-4531-a02b-f49ad0994463">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566915.82 5928756.48</gml:lowerCorner>
          <gml:upperCorner>566941.219 5928774.273</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">20.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_17bb3a32-7fe5-469d-ad8d-76e21a3707a2" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_C482DE15-089A-4B13-8C3B-13AD7F1AFBDE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566941.219 5928771.595 566932.848 5928774.273 566932.454 5928772.614 
566938.915 5928770.547 566937.011 5928762.526 566930.251 5928763.592 
566918.989 5928766.265 566915.82 5928757.163 566919.411 5928756.731 
566921.496 5928756.48 566924.097 5928763.305 566929.921 5928761.923 
566938.598 5928760.553 566941.219 5928771.595 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_17bb3a32-7fe5-469d-ad8d-76e21a3707a2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566937.091 5928761.639</gml:lowerCorner>
          <gml:upperCorner>566937.091 5928761.639</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_91a139e2-bc14-4531-a02b-f49ad0994463" />
      <xplan:position>
        <gml:Point gml:id="Gml_F7759A47-0C2D-4693-A4C4-2B7357E7DACE" srsName="EPSG:25832">
          <gml:pos>566937.091 5928761.639</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_e8974ae2-33cc-4e1c-97e2-4696b926fa8a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566946.477 5928713.61</gml:lowerCorner>
          <gml:upperCorner>566972.037 5928730.874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_8C4FA773-00A5-405E-9F42-E7BABECC1A70" srsName="EPSG:25832">
          <gml:posList>566966.676 5928728.652 566972.037 5928728.006 566970.304 5928713.61 
566946.477 5928716.478 566948.21 5928730.874 566950.196 5928730.635 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d3f56d13-67cc-41a7-929c-d648bfd14033">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566942.541 5928664.974</gml:lowerCorner>
          <gml:upperCorner>566960.425 5928678.707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_3C5313EF-9768-4F61-9024-865FA44B5D27" srsName="EPSG:25832">
          <gml:posList>566959.417 5928668.344 566960.112 5928668.26 566959.716 5928664.974 
566942.541 5928667.042 566943.945 5928678.707 566960.425 5928676.724 
566959.417 5928668.344 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_de545afe-b6a8-4485-8cd9-d49f51b04ff2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566948.792 5928716.902</gml:lowerCorner>
          <gml:upperCorner>566966.676 5928730.635</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B74F00C3-47FD-40DF-A07B-72E51EF1B0B6" srsName="EPSG:25832">
          <gml:posList>566965.667 5928720.272 566966.362 5928720.188 566965.967 5928716.902 
566948.792 5928718.97 566950.196 5928730.635 566966.676 5928728.652 
566965.667 5928720.272 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3760c456-736e-403e-a38a-971952b0594e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566949.603 5928739.574</gml:lowerCorner>
          <gml:upperCorner>566975.166 5928756.838</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_D727DC63-00BE-4305-BC7C-8F8CD65DAAEE" srsName="EPSG:25832">
          <gml:posList>566969.801 5928754.615 566975.166 5928754.004 566973.43 5928739.574 
566949.603 5928742.442 566951.336 5928756.838 566953.321 5928756.599 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_8aaab618-b7bc-4150-b7d2-671701ec3c2e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.463 5928651.425</gml:lowerCorner>
          <gml:upperCorner>566981.413 5928757.372</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(1)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5a6b4b50-618d-49fd-bcf3-da3bd5583cf4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_11776302-234f-4422-9f4c-a5e9252c889a" />
      <xplan:refTextInhalt xlink:href="#GML_e7713d5e-1b95-4b15-abf8-628ec4d14bad" />
      <xplan:refTextInhalt xlink:href="#GML_eb4982a4-8e29-49fa-9c6b-856b261ccb5a" />
      <xplan:refTextInhalt xlink:href="#GML_73c41f4a-8d57-4239-b1d2-d440315b55ea" />
      <xplan:refTextInhalt xlink:href="#GML_8839799e-5e0a-4a2e-a88b-c6e18047a0d4" />
      <xplan:refTextInhalt xlink:href="#GML_e088f923-cc26-4084-a05e-29831ae23397" />
      <xplan:refTextInhalt xlink:href="#GML_64060638-90c4-4dd9-8e64-787a46e192be" />
      <xplan:refTextInhalt xlink:href="#GML_864f30d7-31e2-480b-a8be-90f30ac58579" />
      <xplan:refTextInhalt xlink:href="#GML_90bd7307-fe86-44ce-bb2f-9bc6cbd2ed95" />
      <xplan:refTextInhalt xlink:href="#GML_2100a7ca-a80f-469c-92a9-b489692f93ce" />
      <xplan:refTextInhalt xlink:href="#GML_f40af8dd-3f2c-4531-851c-add7fb18f336" />
      <xplan:refTextInhalt xlink:href="#GML_cf5e49ce-9e68-4596-861d-25f91f294cf1" />
      <xplan:refTextInhalt xlink:href="#GML_360ccf4b-fcff-41e9-830f-98efc562c45d" />
      <xplan:refTextInhalt xlink:href="#GML_ac3f5786-7676-49c1-9bfb-619b6e96153a" />
      <xplan:refTextInhalt xlink:href="#GML_d6bdeec1-3419-4451-adb2-21aa355c5785" />
      <xplan:refTextInhalt xlink:href="#GML_10f1bbc4-6090-4a0f-ac37-4a948a466972" />
      <xplan:refTextInhalt xlink:href="#GML_788057ec-8264-473f-905b-bc1c6ec71f77" />
      <xplan:refTextInhalt xlink:href="#GML_2baf0b93-f279-405c-8a93-0f039968bf23" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E11AA1B0-EA4C-4295-9FB3-110C9FED1B08" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566979.782 5928753.468 566946.776 5928757.301 566946.166 5928757.372 
566944.949 5928746.923 566945.886 5928746.812 566943.902 5928730.131 
566942.996 5928730.239 566936.294 5928672.939 566935.463 5928656.417 
566977.994 5928651.425 566981.413 5928682.684 566971.403 5928683.841 
566979.782 5928753.468 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.5</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5a6b4b50-618d-49fd-bcf3-da3bd5583cf4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566942.38 5928660.356</gml:lowerCorner>
          <gml:upperCorner>566942.38 5928660.356</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8aaab618-b7bc-4150-b7d2-671701ec3c2e" />
      <xplan:position>
        <gml:Point gml:id="Gml_6CFFB4BE-7062-42E0-BB16-B2D986BBDA56" srsName="EPSG:25832">
          <gml:pos>566942.38 5928660.356</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_e6760a0d-d68d-4cc1-a705-80eeb049d156">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566898.531 5928575.496</gml:lowerCorner>
          <gml:upperCorner>567060.062 5928634.303</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6A2D3189-35EF-47C6-9CDE-FE84966BCB2E" srsName="EPSG:25832">
          <gml:posList>567057.847 5928634.303 567057.847 5928634.303 567058.563 5928630.623 
567059.157 5928626.277 567059.222 5928625.202 567059.537 5928619.895 
567060.062 5928612.442 567056.469 5928609.412 567055.539 5928601.632 
567055.504 5928600.913 567055.259 5928597.17 567054.901 5928592.944 
567053.456 5928575.994 567052.858 5928575.496 567038.561 5928577.69 
567031.628 5928578.85 567030.739 5928578.993 567024.212 5928580.069 
567023.611 5928580.191 567022.893 5928580.311 567022.017 5928580.492 
567017.463 5928581.435 567016.362 5928581.658 566997.782 5928585.507 
566992.262 5928586.796 566973.493 5928591.154 566965.812 5928592.878 
566956.712 5928594.942 566934.312 5928600.047 566929.402 5928601.144 
566925.462 5928602 566911.162 5928604.845 566902.662 5928606.537 
566898.531 5928607.364 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8ea26516-2cf0-46ea-9caa-9d7368f387bb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566980.921 5928801.817</gml:lowerCorner>
          <gml:upperCorner>566982.991 5928819.012</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_B37DA3C4-E02F-42B3-B434-0AD2F8C81090" srsName="EPSG:25832">
          <gml:posList>566982.991 5928819.012 566980.921 5928801.817 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_49a29acf-2b0b-414d-8473-761bef59b5d6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566939.323 5928801.551</gml:lowerCorner>
          <gml:upperCorner>566944.048 5928821.457</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_93BDFFDE-D4B1-4299-8DEF-6F4BD624B7CC" srsName="EPSG:25832">
          <gml:posList>566944.048 5928821.457 566939.323 5928801.551 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6f52a583-7dc6-4848-98e5-15c69a696bc8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566910.903 5928668.369</gml:lowerCorner>
          <gml:upperCorner>566912.968 5928685.632</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_E2796A01-B754-4F2E-89A3-956AA7B4109C" srsName="EPSG:25832">
          <gml:posList>566912.968 5928685.632 566910.903 5928668.369 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_b0dd6b6a-9c84-4245-b7ce-037b046d9c70">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566933.578 5928833.914</gml:lowerCorner>
          <gml:upperCorner>566991.495 5928836.494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_BAF6A8B7-F8A1-42D9-9FCB-791F43981CA4" srsName="EPSG:25832">
          <gml:posList>566933.578 5928836.494 566953.565 5928835.603 566960.143 5928835.31 
566967.718 5928834.973 566974.39 5928834.676 566991.495 5928833.914 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
      <xplan:zweckbestimmung>18000</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4de9b2f8-05fe-4ff3-8743-5f9776321fa4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566979.432 5928801.817</gml:lowerCorner>
          <gml:upperCorner>566982.991 5928819.191</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">14.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7b0ca2ee-f83b-475e-b96d-008c2350f92c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E7A21C08-6954-45DE-AE9E-ED375D56FC06" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566982.991 5928819.012 566981.502 5928819.191 566979.432 5928801.996 
566980.921 5928801.817 566982.991 5928819.012 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7b0ca2ee-f83b-475e-b96d-008c2350f92c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566982.918 5928809.942</gml:lowerCorner>
          <gml:upperCorner>566982.918 5928809.942</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_4de9b2f8-05fe-4ff3-8743-5f9776321fa4" />
      <xplan:position>
        <gml:Point gml:id="Gml_DDD07CBE-4475-4D75-A48C-C02060278D78" srsName="EPSG:25832">
          <gml:pos>566982.918 5928809.942</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_9a1818fe-3cec-465f-9b2b-54eb07bbe782">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566900.362 5928592.944</gml:lowerCorner>
          <gml:upperCorner>567054.901 5928657.143</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_471EC9B7-393E-45A6-8491-9FEF7381D995" srsName="EPSG:25832">
          <gml:posList>566900.362 5928621.875 566903.817 5928621.184 566912.169 5928619.496 
566914.251 5928657.143 566929.808 5928655.271 566929.808 5928654.069 
566971.145 5928649.093 566966.384 5928609.517 567026.57 5928597.496 
567040.56 5928594.705 567054.901 5928592.944 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_05940b4e-11ef-4201-80b6-193cbace90ac">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566943.352 5928687.646</gml:lowerCorner>
          <gml:upperCorner>566968.912 5928704.91</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">14.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_888933f0-a52f-4cc8-b9b1-ec17f9722cb4" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7F1974D5-B9EE-4BCD-AE0B-18D6F8A67039" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566945.666 5928693.006 566947.071 5928704.671 566945.085 5928704.91 
566943.352 5928690.514 566967.179 5928687.646 566968.912 5928702.042 
566963.55 5928702.688 566962.542 5928694.308 566963.237 5928694.224 
566962.841 5928690.938 566945.666 5928693.006 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_888933f0-a52f-4cc8-b9b1-ec17f9722cb4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566955.057 5928689.801</gml:lowerCorner>
          <gml:upperCorner>566955.057 5928689.801</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_05940b4e-11ef-4201-80b6-193cbace90ac" />
      <xplan:position>
        <gml:Point gml:id="Gml_2512CC48-B736-46CA-B7E9-73B0783FD5FD" srsName="EPSG:25832">
          <gml:pos>566955.057 5928689.801</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_be80a6a4-5264-4073-b4ed-0ca54aa43346">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566951.917 5928742.866</gml:lowerCorner>
          <gml:upperCorner>566969.801 5928756.599</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7d01bce0-b053-4fd1-a80c-2792c5e31f65" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E742A47D-C394-4C79-BE32-C3152E4F0334" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566969.487 5928746.152 566968.792 5928746.236 566969.801 5928754.615 
566953.321 5928756.599 566951.917 5928744.933 566969.092 5928742.866 
566969.487 5928746.152 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7d01bce0-b053-4fd1-a80c-2792c5e31f65">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566956.625 5928748.584</gml:lowerCorner>
          <gml:upperCorner>566956.625 5928748.584</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_be80a6a4-5264-4073-b4ed-0ca54aa43346" />
      <xplan:position>
        <gml:Point gml:id="Gml_49EF9925-B94F-4E7E-8D02-24AC6B46CA11" srsName="EPSG:25832">
          <gml:pos>566956.625 5928748.584</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_970e5696-1175-460c-9e9a-d96a69319df2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566943.352 5928687.646</gml:lowerCorner>
          <gml:upperCorner>566968.912 5928704.91</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_7744801F-B9BA-4A51-B6B2-2140AC0862E8" srsName="EPSG:25832">
          <gml:posList>566963.55 5928702.688 566968.912 5928702.042 566967.179 5928687.646 
566943.352 5928690.514 566945.085 5928704.91 566947.071 5928704.671 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_6236ba59-ce22-4d8c-899d-317089c3a6a3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566965.128 5928652.263</gml:lowerCorner>
          <gml:upperCorner>566965.128 5928652.263</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">3.3</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_5A851003-B511-4260-BFA4-61D4CF02FA81" srsName="EPSG:25832">
          <gml:pos>566965.128 5928652.263</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4f2e8b68-cf52-47c9-876a-f99ee2c72acb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566964.755 5928767.16</gml:lowerCorner>
          <gml:upperCorner>566984.257 5928830.972</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">20.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_87b36973-eedf-47af-b03f-c364657a5523" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8608B17A-4C22-4328-B5A3-98603A9E01CA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566984.257 5928829.531 566972.284 5928830.972 566964.755 5928768.422 
566975.239 5928767.16 566976.532 5928777.901 566978.021 5928777.722 
566978.444 5928781.232 566976.954 5928781.411 566979.01 5928798.487 
566980.499 5928798.307 566980.921 5928801.817 566979.432 5928801.996 
566981.502 5928819.191 566982.991 5928819.012 566984.257 5928829.531 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_87b36973-eedf-47af-b03f-c364657a5523">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566971.076 5928785.661</gml:lowerCorner>
          <gml:upperCorner>566971.076 5928785.661</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_4f2e8b68-cf52-47c9-876a-f99ee2c72acb" />
      <xplan:position>
        <gml:Point gml:id="Gml_5788C0A0-53D5-4CFB-9C4C-4943AD31A22E" srsName="EPSG:25832">
          <gml:pos>566971.076 5928785.661</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">83.09</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_d2559c4a-5f23-47c6-8207-714db0f03198">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566978.444 5928781.232</gml:lowerCorner>
          <gml:upperCorner>566980.499 5928798.307</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_4F916736-D757-4F17-8089-EA95C5E21B52" srsName="EPSG:25832">
          <gml:posList>566980.499 5928798.307 566978.444 5928781.232 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_5b04d7de-74fb-4901-827b-5b1d6be6aa3f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566990.937 5928834.257</gml:lowerCorner>
          <gml:upperCorner>566990.937 5928834.257</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.2</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_BA00A8A7-1FC8-4A34-9864-A6B244776CFD" srsName="EPSG:25832">
          <gml:pos>566990.937 5928834.257</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_b0522273-e5e3-4d72-8481-7422595cf1ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566934.874 5928818.428</gml:lowerCorner>
          <gml:upperCorner>566954.957 5928834.874</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">20.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_eb93214b-9a54-4652-a042-0e16c10189b8" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_72B73BA6-172A-4C9E-BF08-183119EBFBB0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566954.957 5928829.469 566946.591 5928832.145 566935.097 5928834.874 
566934.874 5928833.18 566945.863 5928830.571 566952.653 5928828.422 
566950.749 5928820.399 566944.048 5928821.457 566943.655 5928819.798 
566952.336 5928818.428 566954.957 5928829.469 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_eb93214b-9a54-4652-a042-0e16c10189b8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566952.497 5928822.085</gml:lowerCorner>
          <gml:upperCorner>566952.497 5928822.085</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_b0522273-e5e3-4d72-8481-7422595cf1ad" />
      <xplan:position>
        <gml:Point gml:id="Gml_2F5310E7-C5A4-4F39-B2F2-034000D1FE16" srsName="EPSG:25832">
          <gml:pos>566952.497 5928822.085</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_037ce5e0-932b-49b2-8063-250774d18fca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566975.239 5928766.981</gml:lowerCorner>
          <gml:upperCorner>566978.021 5928777.722</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_AEAA3FCF-8894-4302-8463-E8603DC1FB85" srsName="EPSG:25832">
          <gml:posList>566978.021 5928777.722 566976.728 5928766.981 566975.239 5928767.16 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8425be48-80ec-4db8-a269-7fa317f1992a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566897.952 5928621.105</gml:lowerCorner>
          <gml:upperCorner>566926.13 5928668.622</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:hMin uom="m">21.5</xplan:hMin>
          <xplan:hMax uom="m">23.5</xplan:hMax>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_192deb22-5627-4fb3-a43d-f44e8ceddd02" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_bad8ffc7-8436-40a3-afe8-468d13e6ab1c" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_460eb6e5-a1fc-420f-b78a-12002ba09d82" />
      <xplan:refTextInhalt xlink:href="#GML_e0ab737d-782e-4b19-94a1-d1b1716dc9ca" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_039AE481-8D11-476C-B585-510125E2EF7C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566926.13 5928666.536 566910.903 5928668.369 566908.806 5928668.622 
566907.874 5928660.88 566898.604 5928661.996 566897.952 5928659.105 
566909.607 5928657.703 566909.935 5928660.428 566916.537 5928659.634 
566914.662 5928621.192 566916.46 5928621.105 566916.598 5928623.922 
566924.034 5928623.559 566926.13 5928666.536 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_192deb22-5627-4fb3-a43d-f44e8ceddd02">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566920.341 5928633.643</gml:lowerCorner>
          <gml:upperCorner>566920.341 5928633.643</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8425be48-80ec-4db8-a269-7fa317f1992a" />
      <xplan:position>
        <gml:Point gml:id="Gml_89511A61-157C-452E-AFA5-2DB803A02AED" srsName="EPSG:25832">
          <gml:pos>566920.341 5928633.643</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">87.66</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bad8ffc7-8436-40a3-afe8-468d13e6ab1c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566921.184 5928663.707</gml:lowerCorner>
          <gml:upperCorner>566921.184 5928663.707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8425be48-80ec-4db8-a269-7fa317f1992a" />
      <xplan:position>
        <gml:Point gml:id="Gml_13EC756E-73CA-476E-ACDD-F76C0F12D93C" srsName="EPSG:25832">
          <gml:pos>566921.184 5928663.707</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_189ae434-a656-4164-a3ed-1d32805d0b2e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566930.93 5928694.746</gml:lowerCorner>
          <gml:upperCorner>566930.93 5928694.746</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_80DB1965-49A2-4B93-9400-55ADD42A0FC8" srsName="EPSG:25832">
          <gml:pos>566930.93 5928694.746</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_40f9062f-45bb-47d3-9b5b-6e37b9266076">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.001 5928698.495</gml:lowerCorner>
          <gml:upperCorner>566916.001 5928698.495</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Geländepunkt</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">4.8</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_8DCB7BB7-C05C-4052-A56B-585A9950D0B2" srsName="EPSG:25832">
          <gml:pos>566916.001 5928698.495</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_95d8d47b-e7a3-471a-8a2a-4217813909d2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566910.903 5928623.432</gml:lowerCorner>
          <gml:upperCorner>566930.219 5928670.482</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">21.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3b1a562b-9654-47ff-89cc-52c46c71bc74" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_F1503FC9-CD64-4AC1-AFA3-A162E00B3A93" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566930.219 5928666.177 566928.22 5928666.275 566928.324 5928668.414 
566911.156 5928670.482 566910.903 5928668.369 566926.13 5928666.536 
566924.034 5928623.559 566926.63 5928623.432 566927.004 5928631.098 
566928.503 5928631.025 566928.983 5928640.863 566927.485 5928640.956 
566928.313 5928657.936 566929.812 5928657.863 566930.219 5928666.177 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3b1a562b-9654-47ff-89cc-52c46c71bc74">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566927.181 5928635.714</gml:lowerCorner>
          <gml:upperCorner>566927.181 5928635.714</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_9cde8c71-4d59-4bd5-ae5b-17e3f8d29b6a" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_95d8d47b-e7a3-471a-8a2a-4217813909d2" />
      <xplan:position>
        <gml:Point gml:id="Gml_B0B4F1C2-1FE5-483D-8A9F-63D139B632FB" srsName="EPSG:25832">
          <gml:pos>566927.181 5928635.714</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
</xplan:XPlanAuszug>