<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_4376F22B-72C0-46AC-9C68-B3AE9059A8E7" xsi:schemaLocation="http://www.xplanung.de/xplangml/6/0 http://repository.gdi-de.org/schemas/de.xleitstelle.xplanung/6.0/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/6/0">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
      <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_88bfe952-199f-4bba-bea2-c2b441737144">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan005_6-0</xplan:name>
      <xplan:beschreibung>Der vorhabenbezogene Bebauungsplan BPlan005_6-0
für den Geltungsbereich Knickweg – Barmbeker Straße –
Gertigstraße
(Bezirk Hamburg-Nord, Ortsteil 412) wird festgestellt.</xplan:beschreibung>
      <xplan:technHerstellDatum>2017-03-20</xplan:technHerstellDatum>
      <xplan:aendertPlan />
      <xplan:wurdeGeaendertVonPlan />
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_561FC2BE-7789-4C85-98FA-4875D6CDEA95" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:externeReferenz>
        <xplan:XP_SpezExterneReferenz>
          <xplan:art>Dokument</xplan:art>
          <xplan:referenzName>BPlan005_6-0_Dok</xplan:referenzName>
          <xplan:referenzURL>BPlan005_6-0.pdf</xplan:referenzURL>
          <xplan:typ>1065</xplan:typ>
        </xplan:XP_SpezExterneReferenz>
      </xplan:externeReferenz>
      <xplan:texte xlink:href="#GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa" />
      <xplan:texte xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:texte xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:texte xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:texte xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:texte xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:texte xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:texte xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:texte xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>412</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:plangeber />
      <xplan:planArt>3000</xplan:planArt>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2016-10-25</xplan:rechtsverordnungsDatum>
      <xplan:durchfuehrungsVertrag>true</xplan:durchfuehrungsVertrag>
      <xplan:bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Im Plangebiet ist in den Schlafräumen durch geeignete bauliche
Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden,
verglaste Loggien, Wintergärten, besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
sicherzustellen, dass ein Innenraumpegel bei
gekipptem Fenster von 30 dB(A) während der Nachtzeit
nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme
in Form von verglasten Loggien oder Wintergärten
muss dieser Innenraumpegel bei gekippten/teilgeöffneten
Bauteilen erreicht werden. Wohn-/Schlafräume in
Einzimmerwohnungen und Kinderzimmer sind wie Schlafräume
zu beurteilen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_0d37aade-f41e-4ec9-b026-79d4709945bf">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die nicht überbauten Flächen
von Tiefgaragen mit einem mindestens 50 cm starken
durchwurzelbaren Substrataufbau zu versehen und gärtnerisch
anzulegen. Hiervon ausgenommen sind die erforderlichen
Flächen für Terrassen, Wege und Freitreppen, Kinderspielflächen
sowie Bereiche, die der Belichtung, Be- und
Entlüftung oder der Aufnahme von technischen Anlagen
dienen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Stellplätze nur in Tiefgaragen
zulässig. Tiefgaragen sind auch außerhalb der überbaubaren
Grundstücksflächen zulässig. Tiefgaragen dürfen
einschließlich ihrer Überdeckung nicht über die natürliche
Geländeoberfläche herausragen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die Freiflächen zur öffentlichen
Grünanlage mit einer Hecke abzupflanzen. Für die
Hecke sind standortgerechte einheimische Gehölze zu verwenden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_2d0243f5-23e4-4f40-8538-d833e9c168dd">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind mindestens 55 vom Hundert
(v. H.) der Dachflächen und auf der Fläche für den
besonderen Nutzungszweck – Kiosk – mindestens 80 v. H.
der Dachflächen mit einem mindestens 10 cm starken
durchwurzelbaren Substrataufbau zu versehen und extensiv
zu begrünen.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_6bac8f46-a325-4392-b115-07bb0e41e8c1">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Terrassen im Anschluss
an die Hauptnutzung bis zu einer Tiefe von 4 m auch außerhalb
der überbaubaren Grundstücksfläche zulässig. Untergeordnete
Bauteile wie Vordächer, Balkone und Erker im
Bereich von öffentlichen Grünflächen sind unzulässig.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind innerhalb der mit „(A)“
bezeichneten Fläche (Vorhabengebiet) im Rahmen der festgesetzten
Nutzungen nur solche Vorhaben zulässig, zu
deren Durchführung sich der Vorhabenträger im Durchführungsvertrag
verpflichtet.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet darf die festgesetzte Grundflächenzahl
für Tiefgaragen bis zu einer Grundflächenzahl
von 1,0 überschritten werden.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_114d1690-3976-42e3-87be-ec97b7ad773d">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind auf den Flurstücken 1477,
1478 und 1480 der Gemarkung Winterhude vor den straßenzugewandten
Fenstern der Wohn- und Schlafräume
lärmgeschützte Außenbereiche durch bauliche Schallschutzmaßnahmen,
wie etwa verglaste Loggien, Wintergärten
oder in ihrer Wirkung vergleichbare Maßnahmen zwingend
vorzusehen. In den lärmgeschützten Außenbereichen
ist bei geöffneten Fenstern/Bauteilen sicherzustellen, dass
ein Tagpegel von weniger als 65 dB(A) erreicht wird.</xplan:text>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_6e871e4c-58e1-4b97-bf2a-a696816b2458">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_1C0E2A1F-D5C4-4E3B-920E-108AEDBA4413" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>      
      <xplan:planinhalt xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <xplan:planinhalt xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:planinhalt xlink:href="#GML_5276e223-57f7-4596-9c9c-8fb909e11db2" />
      <xplan:planinhalt xlink:href="#GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e" />
      <xplan:planinhalt xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:planinhalt xlink:href="#GML_354dc6b6-d910-4f29-8aeb-66d238013da6" />
      <xplan:planinhalt xlink:href="#GML_b607f50f-8221-4811-81f0-7f6efef25db3" />
      <xplan:planinhalt xlink:href="#GML_20f74701-9a0b-4c38-89b8-ae81db151e28" />
      <xplan:planinhalt xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:planinhalt xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:planinhalt xlink:href="#GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f" />
      <xplan:planinhalt xlink:href="#GML_9875e68a-2b21-40e4-8f66-ecf5c2290936" />
      <xplan:planinhalt xlink:href="#GML_7b11be88-979b-483f-8042-82c51eff70af" />
      <xplan:planinhalt xlink:href="#GML_be2c01dd-4d15-4a6a-85c0-dc9565324486" />
      <xplan:planinhalt xlink:href="#GML_21c90a12-5886-40dd-8a86-463267b6e201" />
      <xplan:planinhalt xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <xplan:planinhalt xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:planinhalt xlink:href="#GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299" />
      <xplan:planinhalt xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:planinhalt xlink:href="#GML_8376d624-8ccb-473d-81df-fc95b1ef28b3" />
      <xplan:planinhalt xlink:href="#GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3" />
      <xplan:planinhalt xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:planinhalt xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <xplan:planinhalt xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:planinhalt xlink:href="#GML_5355860b-25fa-401c-a2ea-bb70654d17ab" />
      <xplan:planinhalt xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
      <xplan:praesentationsobjekt xlink:href="#GML_de628759-edb3-4f31-9d43-7d4f816bfb0c" />
      <xplan:praesentationsobjekt xlink:href="#GML_82075a17-ae02-4228-a4ac-debedfd66c1e" />
      <xplan:praesentationsobjekt xlink:href="#GML_a435c756-f459-41ce-b27c-d5992aa8f87e" />
      <xplan:praesentationsobjekt xlink:href="#GML_7d7588c9-1211-471e-ad15-8dd9f6605701" />
      <xplan:praesentationsobjekt xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <xplan:praesentationsobjekt xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <xplan:praesentationsobjekt xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <xplan:praesentationsobjekt xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <xplan:praesentationsobjekt xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <xplan:praesentationsobjekt xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <xplan:praesentationsobjekt xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <xplan:praesentationsobjekt xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <xplan:praesentationsobjekt xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <xplan:praesentationsobjekt xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <xplan:praesentationsobjekt xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <xplan:praesentationsobjekt xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <xplan:praesentationsobjekt xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <xplan:praesentationsobjekt xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <xplan:praesentationsobjekt xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <xplan:praesentationsobjekt xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <xplan:praesentationsobjekt xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <xplan:praesentationsobjekt xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <xplan:praesentationsobjekt xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <xplan:praesentationsobjekt xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <xplan:praesentationsobjekt xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <xplan:praesentationsobjekt xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:gehoertZuPlan xlink:href="#GML_88bfe952-199f-4bba-bea2-c2b441737144" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextAbschnittFlaeche gml:id="GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_5335FBE5-AA5C-4107-B906-259A2F9800CB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567433.287 5937679.305 567416.015 5937672.057 567419.988 5937662.431 
567432.605 5937667.621 567439.105 5937652.001 567428.275 5937631.879 
567422.612 5937629.344 567428.448 5937616.596 567434.138 5937619.1 
567436.484 5937623.475 567442.246 5937634.25 567452.958 5937654.188 
567450.566 5937668.248 567438.775 5937666.233 567433.287 5937679.305 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextAbschnittFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567437.222 5937662.769</gml:lowerCorner>
          <gml:upperCorner>567437.222 5937662.769</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:gliederung2</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <xplan:position>
        <gml:Point gml:id="Gml_DBD9FD8F-1668-4C3F-AF18-0C753F48D0B3" srsName="EPSG:25832">
          <gml:pos>567437.222 5937662.769</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_483256A5-A875-47AA-8D8F-B11DDF14F20A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567452.175 5937661.631 567442.18 5937659.754 
567439.277 5937659.277 567439.035 5937655.06 567438.79 5937652.388 
567432.026 5937639.916 567442.758 5937634.17 567453.455 5937654.108 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_045ae35f-52f5-48d9-b5a6-3d5f339418be">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567444.114 5937651.76</gml:lowerCorner>
          <gml:upperCorner>567444.114 5937651.76</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Z</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:position>
        <gml:Point gml:id="Gml_6AF626DE-5E1B-4951-9F3B-1056C9B74B0C" srsName="EPSG:25832">
          <gml:pos>567444.114 5937651.76</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5890c370-a3bc-412c-b650-13894b87a426">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.328 5937644.053</gml:lowerCorner>
          <gml:upperCorner>567439.328 5937644.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:position>
        <gml:Point gml:id="Gml_A5A1818D-D69E-4DBC-9427-613C9D9A0612" srsName="EPSG:25832">
          <gml:pos>567439.328 5937644.053</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_5276e223-57f7-4596-9c9c-8fb909e11db2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_5112EE71-897C-4DA8-BC7D-895A9AB11DE9" srsName="EPSG:25832">
          <gml:posList>567416.015 5937672.057 567419.988 5937662.431 567432.605 5937667.621 
567439.105 5937652.001 567428.275 5937631.879 567422.612 5937629.344 
567428.448 5937616.596 567434.138 5937619.1 567436.484 5937623.475 
567442.246 5937634.25 567452.958 5937654.188 567450.566 5937668.248 
567438.775 5937666.233 567433.287 5937679.305 567416.015 5937672.057 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.23 5937680.271</gml:lowerCorner>
          <gml:upperCorner>567428.23 5937680.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_CFE5E2DE-5F5E-4C61-BD0E-8956CCC52D29" srsName="EPSG:25832">
          <gml:pos>567428.23 5937680.271</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e21981e2-40a7-4dac-84b0-c88d371030af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">23.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E9336163-36D4-44D0-B4BD-AA0641C0CC56" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567442.758 5937634.17 567432.026 5937639.916 
567427.969 5937632.352 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_10c25c52-be4f-422d-8171-469001af2bf4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.835 5937628.058</gml:lowerCorner>
          <gml:upperCorner>567426.835 5937628.058</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:position>
        <gml:Point gml:id="Gml_64CB95CF-0693-488E-B7A3-FA5A0213C43D" srsName="EPSG:25832">
          <gml:pos>567426.835 5937628.058</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567433.633 5937633.755</gml:lowerCorner>
          <gml:upperCorner>567433.633 5937633.755</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Z</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:position>
        <gml:Point gml:id="Gml_0B2376CC-DC18-4157-90DF-F3EEA410FD96" srsName="EPSG:25832">
          <gml:pos>567433.633 5937633.755</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_354dc6b6-d910-4f29-8aeb-66d238013da6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_BB3EF2E7-46A1-4CC6-BEB5-38C4F847F4A4" srsName="EPSG:25832">
          <gml:posList>567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
567437.269 5937671.067 567433.538 5937679.955 567415.353 5937672.325 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b607f50f-8221-4811-81f0-7f6efef25db3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_65880CCF-820C-41CC-9E4A-F6442124FCCA" srsName="EPSG:25832">
          <gml:posList>567428.223 5937615.867 567434.477 5937618.73 567442.758 5937634.17 
567432.026 5937639.916 567427.969 5937632.352 567421.935 5937629.605 
567428.223 5937615.867 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_20f74701-9a0b-4c38-89b8-ae81db151e28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6A1049F1-BFF8-432B-8632-BAB0DCC64052" srsName="EPSG:25832">
          <gml:posList>567450.952 5937668.824 567440.924 5937667.111 567442.18 5937659.754 
567452.173 5937661.63 567450.952 5937668.824 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E5D43F83-64B9-4804-88FE-4AA5C78EF581" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567451.021 5937668.836 567448.434 5937683.72 
567445.762 5937685.099 567443.507 5937686.264 567432.779 5937681.762 
567433.538 5937679.955 567415.353 5937672.325 567418.938 5937663.64 
567419.705 5937661.78 567432.297 5937667.063 567438.475 5937652.392 
567438.319 5937651.648 567428.755 5937633.817 567427.969 5937632.352 
567424.532 5937630.787 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 567442.758 5937634.17 567453.455 5937654.108 
567452.176 5937661.63 567450.952 5937668.824 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GRZ>0.9</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567425.133 5937653.994</gml:lowerCorner>
          <gml:upperCorner>567425.133 5937653.994</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_B5F7D362-49AA-4A60-9532-C14B95727DB0" srsName="EPSG:25832">
          <gml:pos>567425.133 5937653.994</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.301 5937671.953</gml:lowerCorner>
          <gml:upperCorner>567428.301 5937671.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_17752895-A422-4B84-A8A1-909A0C7DBA71" srsName="EPSG:25832">
          <gml:pos>567428.301 5937671.953</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c34619af-a786-41f0-a899-8a67df7b6750">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.689 5937670.707</gml:lowerCorner>
          <gml:upperCorner>567434.689 5937670.707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bauweise</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_B3D1EA95-CE56-485E-AAB0-894EB2BE19AE" srsName="EPSG:25832">
          <gml:pos>567434.689 5937670.707</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">26.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_3271B086-42A4-48C0-9B89-55341B14BC9B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567452.173 5937661.63 567450.952 5937668.824 567440.924 5937667.111 
567442.18 5937659.754 567452.173 5937661.63 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9036c79b-5e09-4030-8c15-e0d9877d084c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567445.543 5937665.568</gml:lowerCorner>
          <gml:upperCorner>567445.543 5937665.568</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Z</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:position>
        <gml:Point gml:id="Gml_5EC02D35-2A93-4E3B-8F86-6760DB6156EC" srsName="EPSG:25832">
          <gml:pos>567445.543 5937665.568</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fba093ee-e774-4846-b286-e46649619852">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567443.466 5937662.039</gml:lowerCorner>
          <gml:upperCorner>567443.466 5937662.039</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
       <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:position>
        <gml:Point gml:id="Gml_D1D59358-B286-4F5D-8A6B-FF9536AD7A1B" srsName="EPSG:25832">
          <gml:pos>567443.466 5937662.039</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_549A34CF-4031-4782-89AF-2497A33EB1A3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567416.408 5937613.826 567412.057 5937623.382 567405.121 5937620.224 
567409.472 5937610.668 567416.408 5937613.826 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9875e68a-2b21-40e4-8f66-ecf5c2290936">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_0EF61FF0-B52F-43A0-BE28-9FE17EC99F42" srsName="EPSG:25832">
          <gml:posList>567439.035 5937655.06 567438.79 5937652.388 567432.026 5937639.916 
567442.758 5937634.17 567453.455 5937654.108 567452.175 5937661.631 
567442.18 5937659.754 567439.277 5937659.277 567439.035 5937655.06 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_7b11be88-979b-483f-8042-82c51eff70af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_81CF4757-D969-4B01-9B96-52520DD65F05" srsName="EPSG:25832">
          <gml:posList>567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
567448.434 5937683.72 567443.507 5937686.264 567432.779 5937681.762 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_be2c01dd-4d15-4a6a-85c0-dc9565324486">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567431.741 5937675.181</gml:lowerCorner>
          <gml:upperCorner>567432.929 5937679.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>7000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_22F7B936-EBF1-4F48-8CC2-FF49C21B10D9" srsName="EPSG:25832">
          <gml:posList>567432.929 5937675.181 567431.741 5937679.201 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>
        <xplan:BP_KomplexeZweckbestVerEntsorgung>
          <xplan:allgemein>1000</xplan:allgemein>
        </xplan:BP_KomplexeZweckbestVerEntsorgung>
      </xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_21c90a12-5886-40dd-8a86-463267b6e201">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_83DA3CDE-312E-4204-8F0D-B7E1538F6BAF" srsName="EPSG:25832">
          <gml:posList>567407.329 5937656.657 567419.705 5937661.78 567415.353 5937672.325 
567403 5937667.139 567407.329 5937656.657 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UnverbindlicheVormerkung gml:id="GML_cf3318a5-fc25-4f6b-a594-893928f6a3de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.825 5937635.581</gml:lowerCorner>
          <gml:upperCorner>567468.867 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <xplan:rechtscharakter>7000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8D7FE986-9FF6-4081-BF00-20237825A192" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567468.867 5937663.607 567465.27 5937684.42 567462.07 5937698.959 
567458.005 5937697.252 567460.825 5937687.181 567456.625 5937685.781 
567452.825 5937636.781 567458.625 5937635.581 567459.825 5937647.581 
567467.425 5937647.381 567468.867 5937663.607 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:vormerkung>vorgesehene Bahnanlage</xplan:vormerkung>
    </xplan:BP_UnverbindlicheVormerkung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567457.938 5937683.621</gml:lowerCorner>
          <gml:upperCorner>567457.938 5937683.621</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:vormerkung</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <xplan:schriftinhalt>vorgesehene Bahnanlage</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_78C36E53-F16F-4CEF-849C-47CEA8C528A6" srsName="EPSG:25832">
          <gml:pos>567457.938 5937683.621</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">282.22</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_9a4506b9-78b2-4dc4-bb55-088a4c627872">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567394.606 5937621.126</gml:lowerCorner>
          <gml:upperCorner>567438.475 5937667.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_2AEA8B56-EA98-49D8-8B11-C160474A920D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567432.297 5937667.063 567419.705 5937661.78 567405.227 5937655.787 
567401.59 5937657.649 567394.606 5937655.401 567403.316 5937621.126 
567421.935 5937629.605 567424.532 5937630.787 567427.969 5937632.352 
567428.755 5937633.817 567438.319 5937651.648 567438.475 5937652.392 
567432.297 5937667.063 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>
        <xplan:BP_KomplexeZweckbestGruen>
          <xplan:allgemein>1600</xplan:allgemein>
        </xplan:BP_KomplexeZweckbestGruen>
      </xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567409.089 5937633.593</gml:lowerCorner>
          <gml:upperCorner>567409.089 5937633.593</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
	  <xplan:art>xplan:zugunstenVon</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:position>
        <gml:Point gml:id="Gml_DDB637E3-E1E0-45EE-A89B-EC3B31C0959C" srsName="EPSG:25832">
          <gml:pos>567409.089 5937633.593</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567406.396 5937643.214</gml:lowerCorner>
          <gml:upperCorner>567406.396 5937643.214</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:zweckbestimmung[1]/xplan:BP_KomplexeZweckbestGruen/xplan:allgemein</xplan:art>
      <xplan:art>xplan:nutzungsform</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:schriftinhalt>Spielplatz</xplan:schriftinhalt>
      <xplan:position>
        <gml:Point gml:id="Gml_15F11F8A-25C9-407A-B232-7AD9C18565E8" srsName="EPSG:25832">
          <gml:pos>567406.396 5937643.214</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_6B55A63E-E827-404F-AF43-1DF9B26F7629" srsName="EPSG:25832">
          <gml:posList>567409.472 5937610.668 567416.408 5937613.826 567412.057 5937623.382 
567405.121 5937620.224 567409.472 5937610.668 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_321bdb15-2400-4c44-a553-35f402f8743e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937655.401</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_39E3642D-07A8-49CE-A20D-22B1E04BFD63" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567418.938 5937663.64 567415.353 5937672.325 
567403 5937667.139 567395.687 5937664.07 567393.37 5937660.265 
567394.606 5937655.401 567401.59 5937657.649 567405.227 5937655.787 
567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_af37976e-6da1-4e93-a376-ad285c900b4c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.836 5937653.516</gml:lowerCorner>
          <gml:upperCorner>567399.836 5937653.516</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_E2100FF1-174A-445F-BC1F-0E4DD2E0B077" srsName="EPSG:25832">
          <gml:pos>567399.836 5937653.516</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.427 5937662.618</gml:lowerCorner>
          <gml:upperCorner>567416.427 5937662.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:bauweise</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_AE4EBC01-C901-4902-804A-6F6B982DE8DC" srsName="EPSG:25832">
          <gml:pos>567416.427 5937662.618</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567410.188 5937663.997</gml:lowerCorner>
          <gml:upperCorner>567410.188 5937663.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:besondereArtDerBaulNutzung</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_199FB364-B954-491F-88CD-DE7300BDEE1D" srsName="EPSG:25832">
          <gml:pos>567410.188 5937663.997</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Strassenverkehr gml:id="GML_8376d624-8ccb-473d-81df-fc95b1ef28b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_A8808148-445E-4C2A-8E80-6166A5E40DEF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567462.07 5937698.959 567446.882 5937692.583 567390.995 5937669.124 
567386.293 5937667.145 567389.479 5937654.684 567404.151 5937595.479 
567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_E32F862A-03A3-4631-AC97-AACC25AADB0A" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_9BCFDC9E-A2D0-4244-A05A-8AFBFA8F0912" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CDA688A0-BC60-4F41-A570-3B65C368E3B6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1CF8947E-9984-4D04-89D7-6EB4C3D8F49C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6B4A2C69-BD29-47D3-B66B-8EFE543D581C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_196D241D-91EC-4E67-98C7-448A3BAF125D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_660CFB40-A45A-471C-AC9A-0EE3644E24EA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_28BCD624-D53C-4D5F-859B-8C8FBE9D7C65" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_09464C74-0B61-407B-AC76-997FE51C042D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_87C4F666-9456-4342-A812-6DA363B5BAFB" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_B3FECA0C-C1D2-4E2F-8635-00C00347E7A6" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4B3C2606-13D0-464C-A0D6-A8659D8162E9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8ABE2BFE-0476-40B1-AB14-6697311163D5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_99689F37-6FBC-4DDF-9ABD-F38006182DD5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FBC55882-5DB1-4749-A17E-FB75E6BD08F2" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CD5CE215-530C-4689-AF9E-78648710F385" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C196321D-9027-4C5D-9585-083638247543" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C4504559-EE9F-44A5-883A-7869326C0805" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_557AB784-BE34-4046-83AB-93EF31531FE3" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_A633F708-D325-4E4C-B894-3F63253F533D" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:hatDarstellungMitBesondZweckbest>false</xplan:hatDarstellungMitBesondZweckbest>
    </xplan:SO_Strassenverkehr>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567453.759 5937669.764</gml:lowerCorner>
          <gml:upperCorner>567453.759 5937669.764</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">6.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Point gml:id="Gml_02B145D9-294E-4210-90B5-261254F3A275" srsName="EPSG:25832">
          <gml:pos>567453.759 5937669.764</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:nordwinkel uom="grad">0</xplan:nordwinkel>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BesondererNutzungszweckFlaeche gml:id="GML_9775f239-210a-4d61-ad8f-251f5a31296e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403.316 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567428.223 5937629.605</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_CB76C570-8DA2-4AB6-88C6-31148A666340" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_53FF7E13-D34E-47D9-A22B-4F3B8BE97D8C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567407.834857796 5937607.72206805 567410.377 5937607.7 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_8E343AF7-80B4-4F25-ADBD-170D078AC5E5" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E6811ABF-050B-46CA-86F2-EE44C30C48A9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567421.935 5937629.605 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_EFBE798B-6495-44E8-8BCB-AC490D99853F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567421.935 5937629.605 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_54BB1311-ADAF-40A9-8D5C-7CC80F8C15B1" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567406.223 5937609.688 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:dachgestaltung />
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:zweckbestimmung>Kiosk</xplan:zweckbestimmung>
    </xplan:BP_BesondererNutzungszweckFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567412.297 5937615.306</gml:lowerCorner>
          <gml:upperCorner>567412.297 5937615.306</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:position>
        <gml:Point gml:id="Gml_5809AAFC-79F1-41E1-A48B-CA37D5BE416C" srsName="EPSG:25832">
          <gml:pos>567412.297 5937615.306</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f44449a4-6628-4c84-9dfe-d82b4dafca06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.532 5937621.891</gml:lowerCorner>
          <gml:upperCorner>567416.532 5937621.891</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:GRZ</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:position>
        <gml:Point gml:id="Gml_A0311632-94B8-41B4-A4E6-88498E51AC82" srsName="EPSG:25832">
          <gml:pos>567416.532 5937621.891</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_55177fec-7599-4785-a8e2-806742a9b2d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">22.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_188DCB70-1F4D-463B-8016-BD95D78B3D96" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567415.353 5937672.325 567403 5937667.139 
567407.329 5937656.657 567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_796aa811-807f-4550-abab-24b9cc8aa359">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.862 5937659</gml:lowerCorner>
          <gml:upperCorner>567421.862 5937659</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <xplan:position>
        <gml:Point gml:id="Gml_4FF5F0A8-7D9E-423F-AA9F-3665761B4815" srsName="EPSG:25832">
          <gml:pos>567421.862 5937659</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bff033ce-687f-48fe-b27c-542b545fb292">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9863EDB2-EE32-4FA7-A5E3-3D9882160FCA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567448.434 5937683.72 567443.507 5937686.264 
567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_190f5132-7a3c-4e3f-900e-0705faba810a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.986 5937674.081</gml:lowerCorner>
          <gml:upperCorner>567439.986 5937674.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:bezugspunkt</xplan:art>
       <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:h</xplan:art>
      <xplan:art>xplan:hoehenangabe[1]/xplan:XP_Hoehenangabe/xplan:hoehenbezug</xplan:art>
       <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:position>
        <gml:Point gml:id="Gml_B171C650-7D7B-417B-AC7C-831E873B5102" srsName="EPSG:25832">
          <gml:pos>567439.986 5937674.081</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.194 5937679.946</gml:lowerCorner>
          <gml:upperCorner>567440.194 5937679.946</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Z</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:position>
        <gml:Point gml:id="Gml_F62DFF48-A68D-4EE5-B02A-A2B3A645FB17" srsName="EPSG:25832">
          <gml:pos>567440.194 5937679.946</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_5355860b-25fa-401c-a2ea-bb70654d17ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:startBedingung />
      <xplan:endeBedingung />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Curve gml:id="Gml_F67004B5-0AAE-4CE7-BBA9-AD9A1513F57F" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e1568a2b-1768-4889-aedb-95449c029c25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">22.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_E11B4795-57B5-4DD5-A31F-1574D7A74C3A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567437.993 5937669.342 567437.269 5937671.067 567433.538 5937679.955 
567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.339 5937668.137</gml:lowerCorner>
          <gml:upperCorner>567421.339 5937668.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>xplan:Z</xplan:art>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
      <xplan:position>
        <gml:Point gml:id="Gml_F55FFDF6-6E1C-4C41-90A1-EECFAE4A7D59" srsName="EPSG:25832">
          <gml:pos>567421.339 5937668.137</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_de628759-edb3-4f31-9d43-7d4f816bfb0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.484 5937661.969</gml:lowerCorner>
          <gml:upperCorner>567427.781 5937668.164</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_9A3CDC87-1B96-42E5-8F81-529670AACB01" srsName="EPSG:25832">
          <gml:posList>567426.484 5937661.969 567427.781 5937668.164 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_82075a17-ae02-4228-a4ac-debedfd66c1e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.794 5937658.752</gml:lowerCorner>
          <gml:upperCorner>567417.901 5937660.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_929190C8-A692-4990-813D-63978C4626CC" srsName="EPSG:25832">
          <gml:posList>567417.901 5937658.752 567411.794 5937660.897 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_a435c756-f459-41ce-b27c-d5992aa8f87e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.065 5937655.343</gml:lowerCorner>
          <gml:upperCorner>567437.337 5937661.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_04689D82-25C3-4C6F-AC7E-EA2559C79A59" srsName="EPSG:25832">
          <gml:posList>567434.065 5937655.343 567437.337 5937661.831 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_7d7588c9-1211-471e-ad15-8dd9f6605701">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567400.372 5937656.059</gml:lowerCorner>
          <gml:upperCorner>567401.041 5937660.34</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_9961460A-03EC-41CD-89AE-852E2CC263A3" srsName="EPSG:25832">
          <gml:posList>567401.041 5937656.059 567400.372 5937660.34 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
</xplan:XPlanAuszug>