﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_D4D07E1E-4450-47E2-90D6-872E50B9EC94" xsi:schemaLocation="http://www.xplanung.de/xplangml/4/1 http://www.xplanungwiki.de/upload/XPlanGML/4.1-Kernmodell/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/4/1">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
      <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_0602cd45-6a1a-4b24-8786-30862f1fabca">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan001_4-1</xplan:name>
      <xplan:beschreibung>Der Bebauungsplan BPlan001_4-1 für das Gebiet am
Nordufer des Baakenhafens (Bezirk Hamburg-Mitte, Ortsteil
104) wird festgestellt.
Das Gebiet wird wie folgt begrenzt:
Über die Flurstücke 2348 (alt: 2236 – Pfeilerbahn), 1635,
1637 (Versmannstraße) und 2367 (alt: 1639) der Gemarkung
Altstadt Süd – Baakenhafen – über das Flurstück 2367,
Westgrenze des Flurstücks 2361, über das Flurstück 2358
(alt: 1021 – Versmannstraße), Nordgrenzen der Flurstücke
2358 und 1636 (Versmannstraße), über das Flurstück 1634
(Versmannstraße) der Gemarkung Altstadt Süd.</xplan:beschreibung>
      <xplan:technHerstellDatum>2017-03-29</xplan:technHerstellDatum>
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:xPlanGMLVersion>4.1</xplan:xPlanGMLVersion>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_E140F289-7D07-4216-9296-0B13B748072A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567071.225 5932875.978 566952.478 5932905.509 566941.899 5932887.112 
566926.109 5932859.655 566925.011 5932857.747 566850.623 5932868.84 
566849.428 5932859.158 566848.719 5932853.417 566848.591 5932852.383 
566842.603 5932805.784 566841.516 5932797.098 566839.305 5932779.633 
566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 
567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 
567476.043 5932642.107 567477.061 5932645.289 567475.978 5932645.649 
567489.76 5932696.417 567494.443 5932713.666 567502.35 5932743.169 
567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 
567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2" />
      <xplan:texte xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a" />
      <xplan:texte xlink:href="#GML_382e6441-314e-443a-a5a2-290c68c53794" />
      <xplan:texte xlink:href="#GML_4f96ce33-c275-4311-9ea1-737d64b48b78" />
      <xplan:texte xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf" />
      <xplan:texte xlink:href="#GML_6a75e8bb-f2d6-4867-8a26-f6c5df60cae1" />
      <xplan:texte xlink:href="#GML_d7e44f97-b0f8-43cb-9ed6-b76d768f9071" />
      <xplan:texte xlink:href="#GML_e6ef7a8a-cdde-4f34-a748-a84cb7831fd4" />
      <xplan:texte xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de" />
      <xplan:texte xlink:href="#GML_b7112969-6eac-47c8-9443-cd9a0ac271ba" />
      <xplan:texte xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:texte xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:texte xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e" />
      <xplan:texte xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:texte xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1" />
      <xplan:texte xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af" />
      <xplan:texte xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699" />
      <xplan:texte xlink:href="#GML_fd656471-9a50-4e8b-8da2-af82666cde09" />
      <xplan:texte xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62" />
      <xplan:texte xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:texte xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9" />
      <xplan:texte xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d" />
      <xplan:texte xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830" />
      <xplan:texte xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e" />
      <xplan:texte xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c" />
      <xplan:texte xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8" />
      <xplan:texte xlink:href="#GML_8ade82cf-d047-4e3c-a7b8-426429ae8f2a" />
      <xplan:texte xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd" />
      <xplan:texte xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a" />
      <xplan:texte xlink:href="#GML_a5bbf517-2698-419f-a93a-1996ca8c450c" />
      <xplan:texte xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:texte xlink:href="#GML_2d7be849-3d58-4bec-a554-3d752d9b2ab9" />
      <xplan:texte xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>104</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2014-12-23</xplan:rechtsverordnungsDatum>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>false</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_B5FCF4F9-40E1-46CD-9191-A2CAE91851E1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567071.225 5932875.978 566952.478 5932905.509 566941.899 5932887.112 
566926.109 5932859.655 566925.011 5932857.747 566850.623 5932868.84 
566849.428 5932859.158 566848.719 5932853.417 566848.591 5932852.383 
566842.603 5932805.784 566841.516 5932797.098 566839.305 5932779.633 
566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 
567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 
567476.043 5932642.107 567477.061 5932645.289 567475.978 5932645.649 
567489.76 5932696.417 567494.443 5932713.666 567502.35 5932743.169 
567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 
567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:nachrichtlich xlink:href="#GML_3fc11cdb-b7eb-4c39-a0b8-7f4cf383708d" />
      <xplan:nachrichtlich xlink:href="#GML_ca345e0b-f841-411d-8983-4a72343e6cd1" />
      <xplan:nachrichtlich xlink:href="#GML_303e9eb8-189e-485e-b658-913d8dd568f6" />
      <xplan:nachrichtlich xlink:href="#GML_1957130e-eb44-4fe1-9900-057e7b1656b3" />
      <xplan:nachrichtlich xlink:href="#GML_2ca68aee-6fe5-4f9b-adb8-50541a1ebad6" />
      <xplan:nachrichtlich xlink:href="#GML_c0a8d0b0-6902-46d0-b3d5-c220e29edc97" />
      <xplan:praesentationsobjekt xlink:href="#GML_23c8e2cb-eea0-4ede-8380-e4bd444a2c27" />
      <xplan:praesentationsobjekt xlink:href="#GML_a82100c3-cc2a-47ed-82b6-a1ad280cbdc3" />
      <xplan:praesentationsobjekt xlink:href="#GML_ec583607-38e1-4e4b-81e1-59510e28139d" />
      <xplan:praesentationsobjekt xlink:href="#GML_ba047098-ec96-43b6-aed7-9906e6a1bfff" />
      <xplan:praesentationsobjekt xlink:href="#GML_6b8daf18-657d-40aa-89c0-029534abea4b" />
      <xplan:praesentationsobjekt xlink:href="#GML_ef5f32ed-c552-4708-b957-8f71d639d5a6" />
      <xplan:praesentationsobjekt xlink:href="#GML_71b00e63-fe33-4ba5-b6f1-0529b9525728" />
      <xplan:praesentationsobjekt xlink:href="#GML_0a93742a-3a7a-4ebb-a203-26f465b0c0d8" />
      <xplan:praesentationsobjekt xlink:href="#GML_e54969cc-46f2-4361-9965-5040f418f2a5" />
      <xplan:praesentationsobjekt xlink:href="#GML_163f2452-281f-402a-b37b-94ceb90c2cbe" />
      <xplan:praesentationsobjekt xlink:href="#GML_08948a85-5183-4b8d-98c1-c493f155491d" />
      <xplan:praesentationsobjekt xlink:href="#GML_35cf4346-ed26-4a9f-b266-1a4ed64089a9" />
      <xplan:praesentationsobjekt xlink:href="#GML_32c05c56-5bfe-4302-b070-239b322ba5a4" />
      <xplan:praesentationsobjekt xlink:href="#GML_387b0ade-f5d2-4c53-80cc-6ad7bcf40811" />
      <xplan:praesentationsobjekt xlink:href="#GML_b50cd749-911b-4edc-a389-6e54140a69c2" />
      <xplan:praesentationsobjekt xlink:href="#GML_a44c6563-dd2f-4734-8c68-1353654452a1" />
      <xplan:praesentationsobjekt xlink:href="#GML_0772db52-ce5c-46fd-828d-edd7297e9816" />
      <xplan:praesentationsobjekt xlink:href="#GML_3ea9a702-2f78-4b09-9500-ab448539d957" />
      <xplan:praesentationsobjekt xlink:href="#GML_c6d432bc-5dcc-481d-b7c1-a806cd3d767f" />
      <xplan:praesentationsobjekt xlink:href="#GML_dbf21e70-22fa-4917-8444-2918a72b9516" />
      <xplan:praesentationsobjekt xlink:href="#GML_436cc005-c54a-46db-87b4-0009f6121f90" />
      <xplan:praesentationsobjekt xlink:href="#GML_d9086011-c3c5-4582-8d8b-41df2492e8c5" />
      <xplan:praesentationsobjekt xlink:href="#GML_8da28984-778e-4e95-b90d-64afa790714a" />
      <xplan:praesentationsobjekt xlink:href="#GML_f3822531-3f07-45c9-9251-1a538887b558" />
      <xplan:praesentationsobjekt xlink:href="#GML_d30778b9-209d-4939-a2cd-64f03313fc15" />
      <xplan:praesentationsobjekt xlink:href="#GML_38f73e57-9eef-4065-b564-07292912b163" />
      <xplan:praesentationsobjekt xlink:href="#GML_b0f15ed7-64ee-4979-b353-8e8c7a36c673" />
      <xplan:praesentationsobjekt xlink:href="#GML_d5d2be62-0d59-42f6-a64a-84924879b589" />
      <xplan:praesentationsobjekt xlink:href="#GML_8ef2b2e5-3f88-4fb5-aee9-2741d7305301" />
      <xplan:praesentationsobjekt xlink:href="#GML_f87f4c4f-5ff8-45b3-b9e5-2001ce450f37" />
      <xplan:praesentationsobjekt xlink:href="#GML_1149c0ca-dbaf-49f3-b297-d4313ddefa67" />
      <xplan:praesentationsobjekt xlink:href="#GML_babf4409-ca64-454b-8629-74ddfc39a247" />
      <xplan:praesentationsobjekt xlink:href="#GML_a9970c7a-79d6-44ed-b6bf-5dbf42d1819a" />
      <xplan:praesentationsobjekt xlink:href="#GML_6f2da464-0064-4c8f-8b74-1970b9476e33" />
      <xplan:praesentationsobjekt xlink:href="#GML_dfa7c1d9-239c-44d0-9e82-827dc9b09876" />
      <xplan:praesentationsobjekt xlink:href="#GML_b5a075b7-a142-418c-8d84-312f843cb25b" />
      <xplan:praesentationsobjekt xlink:href="#GML_33e3de60-aecb-4593-aa85-bebac94e0bcb" />
      <xplan:praesentationsobjekt xlink:href="#GML_220f7fba-87d5-4b75-bbfa-5bb53f6f4f92" />
      <xplan:praesentationsobjekt xlink:href="#GML_27f09a05-2f77-4f71-8daa-237957f2d1ef" />
      <xplan:praesentationsobjekt xlink:href="#GML_d433c17c-7f10-4790-8edf-99e8de7dd6d9" />
      <xplan:praesentationsobjekt xlink:href="#GML_cb7de8de-5680-4187-9c99-4f195bfeb807" />
      <xplan:praesentationsobjekt xlink:href="#GML_69a35e30-7c8a-4da1-867b-beacd1d91a98" />
      <xplan:praesentationsobjekt xlink:href="#GML_5e016c20-61a9-45a7-af55-41b9815bf89c" />
      <xplan:praesentationsobjekt xlink:href="#GML_e27efe51-548d-405a-b6f3-3d39fa616b34" />
      <xplan:praesentationsobjekt xlink:href="#GML_c5cc02f9-a56f-430e-90a5-4e22c26b5593" />
      <xplan:praesentationsobjekt xlink:href="#GML_a9ddcedf-e590-476d-bc67-720015f9a23e" />
      <xplan:praesentationsobjekt xlink:href="#GML_85603634-4129-4520-9d5d-0f36ba4ec383" />
      <xplan:praesentationsobjekt xlink:href="#GML_ee00dc13-7e0e-44a6-b4c6-990c40489501" />
      <xplan:praesentationsobjekt xlink:href="#GML_c74c5ea3-a4cf-416f-a0d5-f6b39041a9c7" />
      <xplan:praesentationsobjekt xlink:href="#GML_9cb6a2f4-84b8-4323-94ec-9fcdc3bca5c4" />
      <xplan:praesentationsobjekt xlink:href="#GML_dad5dbd9-c264-4272-a68c-86bcd2d75a10" />
      <xplan:praesentationsobjekt xlink:href="#GML_7f19c2df-be1d-42b6-bae1-a6907a19a10b" />
      <xplan:praesentationsobjekt xlink:href="#GML_7c2071c8-7621-4fb4-8943-57d771fa0d20" />
      <xplan:praesentationsobjekt xlink:href="#GML_7cb373be-48bc-4644-87be-eaf3969051a0" />
      <xplan:praesentationsobjekt xlink:href="#GML_bacae243-8786-4292-94fc-651f36ff6494" />
      <xplan:praesentationsobjekt xlink:href="#GML_f6746f2f-5c38-4a9a-897b-bdf35dc2f0c5" />
      <xplan:praesentationsobjekt xlink:href="#GML_7ae7199c-8628-485a-8a8d-bc43f81e4f66" />
      <xplan:praesentationsobjekt xlink:href="#GML_30e32b73-dff9-483e-9e12-8bcc9200146b" />
      <xplan:praesentationsobjekt xlink:href="#GML_3e94ac43-7796-4f08-a888-b77863b192dc" />
      <xplan:praesentationsobjekt xlink:href="#GML_dcec9cc3-ede0-40aa-8327-1983aab6647a" />
      <xplan:praesentationsobjekt xlink:href="#GML_1afbafa0-d4f6-4227-8797-4f2c7bfd93a2" />
      <xplan:praesentationsobjekt xlink:href="#GML_81f9c336-b916-4cd2-81db-3bb9973748b9" />
      <xplan:praesentationsobjekt xlink:href="#GML_a1ad661d-e2c5-48b3-b576-88ab29efcbaa" />
      <xplan:praesentationsobjekt xlink:href="#GML_eab8154e-4634-4d14-adb1-7b51e589f122" />
      <xplan:praesentationsobjekt xlink:href="#GML_e7af056f-472c-47be-848d-7b7c5f3c41a0" />
      <xplan:praesentationsobjekt xlink:href="#GML_44ee80ca-f0f0-4aee-acde-ef863d6fedc6" />
      <xplan:praesentationsobjekt xlink:href="#GML_c69485a9-5ff7-4557-9648-fd65bd7e140e" />
      <xplan:praesentationsobjekt xlink:href="#GML_b10a18ef-f593-4b72-a792-57081b41793a" />
      <xplan:rasterBasis xlink:href="#Gml_5CF4FC84-A8A1-4113-AF42-63641E92805E" />
      <xplan:versionBauNVO>4000</xplan:versionBauNVO>
      <xplan:versionBauNVOText>Version vom 11.06.2013</xplan:versionBauNVOText>
      <xplan:gehoertZuPlan xlink:href="#GML_0602cd45-6a1a-4b24-8786-30862f1fabca" />
      <xplan:inhaltBPlan xlink:href="#GML_82d36cb7-9ef4-4f3a-b7f0-694ffaa9dcc4" />
      <xplan:inhaltBPlan xlink:href="#GML_01e35ac7-7837-4b85-8d85-e79ee17191ac" />
      <xplan:inhaltBPlan xlink:href="#GML_5b9f4938-413d-43fb-b861-720b1d5e0601" />
      <xplan:inhaltBPlan xlink:href="#GML_4372a665-a76e-41eb-bbb3-e4899011d132" />
      <xplan:inhaltBPlan xlink:href="#GML_9d909301-e146-48ee-a590-c3982c2a5cdf" />
      <xplan:inhaltBPlan xlink:href="#GML_7803ad38-1d7f-40eb-acd0-9f61d0149d94" />
      <xplan:inhaltBPlan xlink:href="#GML_087a1120-98af-4731-9053-a097ba5abec0" />
      <xplan:inhaltBPlan xlink:href="#GML_625ce698-8502-4018-ace9-a65c622f4c02" />
      <xplan:inhaltBPlan xlink:href="#GML_7413c4ed-9475-45fd-9492-7a29739d0ef8" />
      <xplan:inhaltBPlan xlink:href="#GML_287dc3f6-c180-4e5b-98bb-1deab55eb469" />
      <xplan:inhaltBPlan xlink:href="#GML_55bfc4f0-7895-42aa-8497-417dd95d70d9" />
      <xplan:inhaltBPlan xlink:href="#GML_bc4f775e-aebc-4cc0-a5f0-84eef160b876" />
      <xplan:inhaltBPlan xlink:href="#GML_361d84aa-b947-414a-8d43-f26733aed4d5" />
      <xplan:inhaltBPlan xlink:href="#GML_8b10198c-064b-45c4-992a-4c6739d79849" />
      <xplan:inhaltBPlan xlink:href="#GML_51cc1bc7-c630-4562-92e3-ac760070eda2" />
      <xplan:inhaltBPlan xlink:href="#GML_9fec54ea-0a52-48aa-9554-04ace7a1e2cf" />
      <xplan:inhaltBPlan xlink:href="#GML_88948c97-8b3d-46fc-a104-60857bb84be4" />
      <xplan:inhaltBPlan xlink:href="#GML_6f779b16-20fe-4043-a328-c828dba674a7" />
      <xplan:inhaltBPlan xlink:href="#GML_2aa18469-6fc9-4128-86e5-0c27d251ca8e" />
      <xplan:inhaltBPlan xlink:href="#GML_ca655909-b5cd-4dc4-9fb5-0145ec5a3724" />
      <xplan:inhaltBPlan xlink:href="#GML_11c0cdfb-2b02-4518-8b2e-1458a8f6c9bd" />
      <xplan:inhaltBPlan xlink:href="#GML_ee877546-6cf7-4e4d-bcd2-7a419263dc61" />
      <xplan:inhaltBPlan xlink:href="#GML_2ade7eaf-560c-4a1a-a46b-5b3bf3e3b469" />
      <xplan:inhaltBPlan xlink:href="#GML_8341fc3c-a7c7-440d-9be7-99259d8c216e" />
      <xplan:inhaltBPlan xlink:href="#GML_ff172cb9-5612-475f-ad24-913df1c76eb1" />
      <xplan:inhaltBPlan xlink:href="#GML_13fdf7ef-5200-4b91-995c-c5de0a4f0093" />
      <xplan:inhaltBPlan xlink:href="#GML_5b15ec94-09cd-4e34-8d26-e06cf33a1191" />
      <xplan:inhaltBPlan xlink:href="#GML_8ac68803-4f31-48da-ab0e-3752c4d3ddad" />
      <xplan:inhaltBPlan xlink:href="#GML_c45e5acb-2fab-43f3-880d-30526f4c94eb" />
      <xplan:inhaltBPlan xlink:href="#GML_2934b32c-6d07-4d3d-9d73-00d133e28839" />
      <xplan:inhaltBPlan xlink:href="#GML_ca387c49-a139-4e28-8c5c-e6c506d9ba9c" />
      <xplan:inhaltBPlan xlink:href="#GML_798d558b-73d7-416d-8862-237cb57c9540" />
      <xplan:inhaltBPlan xlink:href="#GML_0d0ea6ef-50a9-47c7-8e8a-4c16fd4fbaf4" />
      <xplan:inhaltBPlan xlink:href="#GML_6f1f3557-da54-47ca-89d0-248d8b9c76ae" />
      <xplan:inhaltBPlan xlink:href="#GML_c5f6c94c-2390-4ee6-a167-73416b289a03" />
      <xplan:inhaltBPlan xlink:href="#GML_f750899c-e632-46e7-9864-f2403dfdf334" />
      <xplan:inhaltBPlan xlink:href="#GML_1fec35eb-3032-4ed6-a9cf-3c36ed5b4652" />
      <xplan:inhaltBPlan xlink:href="#GML_8dbfebc6-0a1c-4ee6-be68-f52f642e5991" />
      <xplan:inhaltBPlan xlink:href="#GML_63fb2da6-f94c-4b26-9823-38cfc2d3bfb6" />
      <xplan:inhaltBPlan xlink:href="#GML_d6b5bede-11cd-4e5f-b074-23920a818290" />
      <xplan:inhaltBPlan xlink:href="#GML_7f8e5623-8052-47a2-8e6f-455baa906726" />
      <xplan:inhaltBPlan xlink:href="#GML_16a5aba2-9c83-4175-a28b-c8d6e5a4a640" />
      <xplan:inhaltBPlan xlink:href="#GML_32ac26a6-25b6-472a-9bd0-095fbb07bdf6" />
      <xplan:inhaltBPlan xlink:href="#GML_cfbbc7f5-d158-4fb1-a87c-f795f0011c86" />
      <xplan:inhaltBPlan xlink:href="#GML_e4e2251e-6329-4655-9aa4-f4bbaa18be04" />
      <xplan:inhaltBPlan xlink:href="#GML_a059a827-7a77-4a4e-b930-f77ed029eee0" />
      <xplan:inhaltBPlan xlink:href="#GML_45452be9-af94-408b-87c8-bfb37aeaae0d" />
      <xplan:inhaltBPlan xlink:href="#GML_f0acca72-d01e-4a95-ba82-db3675ed653b" />
      <xplan:inhaltBPlan xlink:href="#GML_366f3f01-dd6c-45ca-97ee-4d6dc1372c2c" />
      <xplan:inhaltBPlan xlink:href="#GML_d0188aa3-4f7b-4d35-bd8f-a39cc777b93c" />
      <xplan:inhaltBPlan xlink:href="#GML_d137f77a-1da0-4217-ba41-6dae7c3ebf5e" />
      <xplan:inhaltBPlan xlink:href="#GML_288452c4-3611-4e09-84c0-2029abab6a39" />
      <xplan:inhaltBPlan xlink:href="#GML_8ebf4ef9-132a-43c2-ac70-4a5a95a49351" />
      <xplan:inhaltBPlan xlink:href="#GML_992fe65b-4d1b-41a4-834d-49236d0b9d11" />
      <xplan:inhaltBPlan xlink:href="#GML_f303aa68-317d-4992-a7b2-ae920695dbfb" />
      <xplan:inhaltBPlan xlink:href="#GML_cc91cd1a-a10f-4344-ae85-2ad7682a6fcf" />
      <xplan:inhaltBPlan xlink:href="#GML_1dfc27bb-4892-4c38-aed1-42eabc4a55dd" />
      <xplan:inhaltBPlan xlink:href="#GML_b99f5f46-d32c-4796-9a95-efb93cdd3445" />
      <xplan:inhaltBPlan xlink:href="#GML_bb101a4b-bfd9-4dee-a952-7edaf0eb42e1" />
      <xplan:inhaltBPlan xlink:href="#GML_93892231-742a-46ad-bc0a-532018ab7236" />
      <xplan:inhaltBPlan xlink:href="#GML_95908282-8125-4a08-87c6-7c25ddefda97" />
      <xplan:inhaltBPlan xlink:href="#GML_24cd4e41-95f6-4c31-8711-6b0673c48a0e" />
      <xplan:inhaltBPlan xlink:href="#GML_deaaa4cb-fef0-40af-b249-a6743aa695dd" />
      <xplan:inhaltBPlan xlink:href="#GML_3ad88da2-f1a7-4d09-8023-6ee4d7b0fa6e" />
      <xplan:inhaltBPlan xlink:href="#GML_a2eeafdd-573a-4ad3-826b-7ec4e2452a0c" />
      <xplan:inhaltBPlan xlink:href="#GML_a0b7e9f5-09b8-412c-9fa7-4da902b89f30" />
      <xplan:inhaltBPlan xlink:href="#GML_6829362e-dfa9-445a-b18b-be68acd05371" />
      <xplan:inhaltBPlan xlink:href="#GML_41292320-d863-4a1d-95d5-c8aa074b141f" />
      <xplan:inhaltBPlan xlink:href="#GML_285ceaa0-1a01-4527-8a53-1f2f23dbc5cf" />
      <xplan:inhaltBPlan xlink:href="#GML_771a4c9b-dbad-42b2-b923-e550ffd1f3b7" />
      <xplan:inhaltBPlan xlink:href="#GML_6e0999cd-b43d-48bd-9039-4dd8373cabaa" />
      <xplan:inhaltBPlan xlink:href="#GML_8b29af24-1126-4ac9-a7bd-24702842fe87" />
      <xplan:inhaltBPlan xlink:href="#GML_8fcaa7c4-056a-4bb4-a8a8-d28fd3829d95" />
      <xplan:inhaltBPlan xlink:href="#GML_9d6fb6b8-2e1a-4eba-80b1-79d191906936" />
      <xplan:inhaltBPlan xlink:href="#GML_9d384060-a110-44cd-b0eb-8daca2c88662" />
      <xplan:inhaltBPlan xlink:href="#GML_a2559c2a-2d26-43b6-b1cf-f711e167627c" />
      <xplan:inhaltBPlan xlink:href="#GML_3dc269a7-9889-48ae-969a-f6653030938c" />
      <xplan:inhaltBPlan xlink:href="#GML_48e8084f-68bf-4a23-aac1-4df9d7eeff57" />
      <xplan:inhaltBPlan xlink:href="#GML_c3036682-fcff-4418-b943-bcee53271588" />
      <xplan:inhaltBPlan xlink:href="#GML_5083db57-c8e8-457e-8dc7-60a7ccb34a0d" />
      <xplan:inhaltBPlan xlink:href="#GML_fa60d8a2-ba3c-4c8c-ab75-f0d83a71c00a" />
      <xplan:inhaltBPlan xlink:href="#GML_784dbaff-c2f6-4519-a40b-7afdf39d1b8f" />
      <xplan:inhaltBPlan xlink:href="#GML_23ed66cd-0484-497a-a0ae-aaafa4a7febc" />
      <xplan:inhaltBPlan xlink:href="#GML_2f828891-0f23-4cb8-a4d5-9c131b2e3089" />
      <xplan:inhaltBPlan xlink:href="#GML_442af384-8831-4bd1-ae12-3e01802b513a" />
      <xplan:inhaltBPlan xlink:href="#GML_f9238a58-ea13-46bf-8414-f7baa3badd69" />
      <xplan:inhaltBPlan xlink:href="#GML_36c586d6-47b1-447d-b11d-cdd263f9c472" />
      <xplan:inhaltBPlan xlink:href="#GML_fbcbb2d0-0fb6-4699-8f79-e1fb8d3be5e9" />
      <xplan:inhaltBPlan xlink:href="#GML_0066135d-0b09-423e-a7bc-12ea23ff7196" />
      <xplan:inhaltBPlan xlink:href="#GML_69086e3c-5a9e-4202-83b3-f88167bbd387" />
      <xplan:inhaltBPlan xlink:href="#GML_a69fedcd-054a-4c17-ba94-b4c5cde51146" />
      <xplan:inhaltBPlan xlink:href="#GML_01b3c2d3-922c-4b45-a801-2b9ac29ab4b5" />
      <xplan:inhaltBPlan xlink:href="#GML_77e5eecd-f6ca-4fb0-98cc-f1d1212b06a6" />
      <xplan:inhaltBPlan xlink:href="#GML_6169224a-3c33-477c-97d8-07b9ca2d0a5a" />
      <xplan:inhaltBPlan xlink:href="#GML_4354adf9-4004-4283-98f9-aca57842955d" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_82d36cb7-9ef4-4f3a-b7f0-694ffaa9dcc4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932731.601</gml:lowerCorner>
          <gml:upperCorner>567245.957 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_46D9EFBF-7EFE-4D41-8DA8-3200015BC96F" srsName="EPSG:25832">
          <gml:posList>567202.283 5932764.51 567229.869 5932757.348 567224.591 5932737.022 
567237.174 5932733.755 567245.957 5932767.583 567193.176 5932781.173 
567181.156 5932734.868 567193.739 5932731.601 567202.283 5932764.51 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_01e35ac7-7837-4b85-8d85-e79ee17191ac">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567108.409 5932752.067</gml:lowerCorner>
          <gml:upperCorner>567140.779 5932786.174</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_8da28984-778e-4e95-b90d-64afa790714a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f3822531-3f07-45c9-9251-1a538887b558" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1149c0ca-dbaf-49f3-b297-d4313ddefa67" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_fd656471-9a50-4e8b-8da2-af82666cde09" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9AC9053E-0B99-498C-9E13-72FF952B5A91" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567137.502 5932780.326 567112.927 5932786.174 567108.409 5932767.187 
567120.277 5932764.106 567118.644 5932757.814 567140.779 5932752.067 
567137.502 5932780.326 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_8da28984-778e-4e95-b90d-64afa790714a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932775.503</gml:lowerCorner>
          <gml:upperCorner>567124.376 5932775.503</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_01e35ac7-7837-4b85-8d85-e79ee17191ac" />
      <xplan:schriftinhalt>(B)</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point gml:id="Gml_BD12E0C2-14F1-4421-9945-989802B8CBC1" srsName="EPSG:25832">
          <gml:pos>567124.376 5932775.503</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f3822531-3f07-45c9-9251-1a538887b558">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932769.996</gml:lowerCorner>
          <gml:upperCorner>567124.376 5932769.996</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_01e35ac7-7837-4b85-8d85-e79ee17191ac" />
      <xplan:position>
        <gml:Point gml:id="Gml_D94A3F85-5E7C-405D-B32F-5B342B08AC4B" srsName="EPSG:25832">
          <gml:pos>567124.376 5932769.996</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1149c0ca-dbaf-49f3-b297-d4313ddefa67">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567124.376 5932764.202</gml:lowerCorner>
          <gml:upperCorner>567124.376 5932764.202</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_01e35ac7-7837-4b85-8d85-e79ee17191ac" />
      <xplan:position>
        <gml:Point gml:id="Gml_3D558CF2-CAF6-4AD5-AC86-B9B5BBB350CD" srsName="EPSG:25832">
          <gml:pos>567124.376 5932764.202</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_5b9f4938-413d-43fb-b861-720b1d5e0601">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566904.86 5932822.968</gml:lowerCorner>
          <gml:upperCorner>566939.864 5932843.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_76D1C89F-5F22-42BB-A30F-422FCECF2E0C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566939.864 5932838.193 566919.2 5932841.614 566906.14 5932843.606 
566904.86 5932835.912 566918.024 5932833.904 566916.859 5932826.266 
566936.789 5932822.968 566939.864 5932838.193 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_4372a665-a76e-41eb-bbb3-e4899011d132">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.101 5932723.312</gml:lowerCorner>
          <gml:upperCorner>567359.658 5932726.674</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_562DB6F0-B327-4BD5-AD3E-4614078A9255" srsName="EPSG:25832">
          <gml:posList>567347.101 5932726.674 567359.658 5932723.312 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9d909301-e146-48ee-a590-c3982c2a5cdf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566978.985 5932794.066</gml:lowerCorner>
          <gml:upperCorner>566995.451 5932815.855</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_babf4409-ca64-454b-8629-74ddfc39a247" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D6670052-A69E-4B40-A2D0-409899EA6B60" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566995.451 5932813.416 566982.682 5932815.855 566978.985 5932796.505 
566991.754 5932794.066 566995.451 5932813.416 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_babf4409-ca64-454b-8629-74ddfc39a247">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566987.218 5932804.96</gml:lowerCorner>
          <gml:upperCorner>566987.218 5932804.96</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d909301-e146-48ee-a590-c3982c2a5cdf" />
      <xplan:position>
        <gml:Point gml:id="Gml_996921F9-1739-4EBC-B73D-D27AC0F4B678" srsName="EPSG:25832">
          <gml:pos>566987.218 5932804.96</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7803ad38-1d7f-40eb-acd0-9f61d0149d94">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567381.24 5932690.361</gml:lowerCorner>
          <gml:upperCorner>567399.746 5932715.941</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_0a93742a-3a7a-4ebb-a203-26f465b0c0d8" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_6f2da464-0064-4c8f-8b74-1970b9476e33" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9467B7A9-07FF-45DB-8587-2DBB3C97A9E0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567399.746 5932712.579 567387.189 5932715.941 567381.24 5932693.724 
567393.798 5932690.361 567399.746 5932712.579 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_0a93742a-3a7a-4ebb-a203-26f465b0c0d8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567390.493 5932699.938</gml:lowerCorner>
          <gml:upperCorner>567390.493 5932699.938</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7803ad38-1d7f-40eb-acd0-9f61d0149d94" />
      <xplan:position>
        <gml:Point gml:id="Gml_94A6E2F1-D6C4-48F6-BF36-0FCC5454500D" srsName="EPSG:25832">
          <gml:pos>567390.493 5932699.938</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6f2da464-0064-4c8f-8b74-1970b9476e33">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567390.493 5932705.278</gml:lowerCorner>
          <gml:upperCorner>567390.493 5932705.278</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7803ad38-1d7f-40eb-acd0-9f61d0149d94" />
      <xplan:position>
        <gml:Point gml:id="Gml_6F9889A3-C09B-464C-A987-E04974B589A4" srsName="EPSG:25832">
          <gml:pos>567390.493 5932705.278</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_087a1120-98af-4731-9053-a097ba5abec0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567261.31 5932723.846</gml:lowerCorner>
          <gml:upperCorner>567279.2 5932747.803</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ba047098-ec96-43b6-aed7-9906e6a1bfff" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3ea9a702-2f78-4b09-9500-ab448539d957" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3C3C9F65-0119-41C8-ADAC-85C9D545F3F8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567279.2 5932744.514 567266.628 5932747.803 567261.31 5932727.488 
567273.793 5932723.846 567279.2 5932744.514 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ba047098-ec96-43b6-aed7-9906e6a1bfff">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567269.576 5932735.532</gml:lowerCorner>
          <gml:upperCorner>567269.576 5932735.532</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_087a1120-98af-4731-9053-a097ba5abec0" />
      <xplan:position>
        <gml:Point gml:id="Gml_B1A9C2CB-5E02-4F2D-967D-5343E648A176" srsName="EPSG:25832">
          <gml:pos>567269.576 5932735.532</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3ea9a702-2f78-4b09-9500-ab448539d957">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567269.576 5932730.191</gml:lowerCorner>
          <gml:upperCorner>567269.576 5932730.191</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_087a1120-98af-4731-9053-a097ba5abec0" />
      <xplan:position>
        <gml:Point gml:id="Gml_3204BA60-EE9E-4A7C-A79F-84BF5F89D62D" srsName="EPSG:25832">
          <gml:pos>567269.576 5932730.191</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_625ce698-8502-4018-ace9-a65c622f4c02">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567100.317 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_39070B67-7353-4885-9033-5E1982E74C5B" srsName="EPSG:25832">
          <gml:posList>567149.128 5932791.971 567103.525 5932802.817 567100.317 5932789.338 
567113.013 5932786.154 567137.502 5932780.326 567141.581 5932745.143 
567155.075 5932741.64 567154.527 5932745.398 567149.128 5932791.971 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7413c4ed-9475-45fd-9492-7a29739d0ef8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566876.538 5932813.399</gml:lowerCorner>
          <gml:upperCorner>566904.004 5932832.072</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b10a18ef-f593-4b72-a792-57081b41793a" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_923CECE3-C332-4275-9903-FCCEB5559415" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566904.004 5932828.228 566878.799 5932832.072 566876.538 5932817.244 
566901.745 5932813.399 566904.004 5932828.228 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>2</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b10a18ef-f593-4b72-a792-57081b41793a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566890.271 5932822.736</gml:lowerCorner>
          <gml:upperCorner>566890.271 5932822.736</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7413c4ed-9475-45fd-9492-7a29739d0ef8" />
      <xplan:position>
        <gml:Point gml:id="Gml_47937549-4115-4E23-965A-DE24800BDC9B" srsName="EPSG:25832">
          <gml:pos>566890.271 5932822.736</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_287dc3f6-c180-4e5b-98bb-1deab55eb469">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567088.12 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932805.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1A6F76CB-3535-4253-AA31-D0D30C71CF5C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567103.525 5932802.817 567091.361 5932805.699 567088.12 5932792.079 
567100.281 5932789.185 567103.525 5932802.817 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_55bfc4f0-7895-42aa-8497-417dd95d70d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567108.409 5932752.067</gml:lowerCorner>
          <gml:upperCorner>567140.779 5932786.174</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F3FC3D62-55C2-4824-BF29-A6A172C4B8C9" srsName="EPSG:25832">
          <gml:posList>567137.502 5932780.326 567112.927 5932786.174 567108.409 5932767.187 
567120.277 5932764.106 567118.644 5932757.814 567140.779 5932752.067 
567137.502 5932780.326 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_bc4f775e-aebc-4cc0-a5f0-84eef160b876">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.222 5932794.066</gml:lowerCorner>
          <gml:upperCorner>566998.078 5932837.394</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A8AECC4B-739B-4AF3-913F-EB1CF6705C12" srsName="EPSG:25832">
          <gml:posList>566998.078 5932827.167 566944.546 5932837.394 566938.222 5932804.293 
566950.991 5932801.853 566954.688 5932821.203 566982.682 5932815.855 
566978.985 5932796.505 566991.754 5932794.066 566995.451 5932813.416 
566998.078 5932827.167 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_361d84aa-b947-414a-8d43-f26733aed4d5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932745.398</gml:lowerCorner>
          <gml:upperCorner>567154.527 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_dfa7c1d9-239c-44d0-9e82-827dc9b09876" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a9ddcedf-e590-476d-bc67-720015f9a23e" />
      <xplan:refTextInhalt xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2" />
      <xplan:refTextInhalt xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a" />
      <xplan:refTextInhalt xlink:href="#GML_4f96ce33-c275-4311-9ea1-737d64b48b78" />
      <xplan:refTextInhalt xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf" />
      <xplan:refTextInhalt xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de" />
      <xplan:refTextInhalt xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1" />
      <xplan:refTextInhalt xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af" />
      <xplan:refTextInhalt xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699" />
      <xplan:refTextInhalt xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62" />
      <xplan:refTextInhalt xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9" />
      <xplan:refTextInhalt xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d" />
      <xplan:refTextInhalt xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830" />
      <xplan:refTextInhalt xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e" />
      <xplan:refTextInhalt xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c" />
      <xplan:refTextInhalt xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd" />
      <xplan:refTextInhalt xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B1770AEA-DAC3-4A02-8BA9-E6B24A8CADC4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567149.128 5932791.971 567103.525 5932802.817 567092.822 5932757.841 
567117.019 5932751.558 567117.89 5932754.911 567154.527 5932745.398 
567149.128 5932791.971 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dfa7c1d9-239c-44d0-9e82-827dc9b09876">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567133.905 5932761.277</gml:lowerCorner>
          <gml:upperCorner>567133.905 5932761.277</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_361d84aa-b947-414a-8d43-f26733aed4d5" />
      <xplan:position>
        <gml:Point gml:id="Gml_54F28426-0431-4795-90CD-BFFC7E542FD0" srsName="EPSG:25832">
          <gml:pos>567133.905 5932761.277</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a9ddcedf-e590-476d-bc67-720015f9a23e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567133.905 5932767.233</gml:lowerCorner>
          <gml:upperCorner>567133.905 5932767.233</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_361d84aa-b947-414a-8d43-f26733aed4d5" />
      <xplan:position>
        <gml:Point gml:id="Gml_B6CFC85C-10D6-484D-A5ED-529569A6A766" srsName="EPSG:25832">
          <gml:pos>567133.905 5932767.233</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_8b10198c-064b-45c4-992a-4c6739d79849">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567306.772 5932734.136</gml:lowerCorner>
          <gml:upperCorner>567318.865 5932737.3</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_FDF58C20-0D66-4F7D-81E8-0B15F93301BB" srsName="EPSG:25832">
          <gml:posList>567306.772 5932737.3 567318.865 5932734.136 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_51cc1bc7-c630-4562-92e3-ac760070eda2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566982.682 5932813.416</gml:lowerCorner>
          <gml:upperCorner>566995.451 5932815.855</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_5F8F8EDF-BBDB-4830-9776-7DB958B67E49" srsName="EPSG:25832">
          <gml:posList>566982.682 5932815.855 566995.451 5932813.416 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_9fec54ea-0a52-48aa-9554-04ace7a1e2cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567470.593 5932666.819</gml:lowerCorner>
          <gml:upperCorner>567491.419 5932705.541</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DB918E23-3647-41E0-8A2F-37FEE76B1C16" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567491.419 5932702.527 567480.162 5932705.541 567470.593 5932669.8 
567481.725 5932666.819 567491.419 5932702.527 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_88948c97-8b3d-46fc-a104-60857bb84be4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566848.591 5932702.527</gml:lowerCorner>
          <gml:upperCorner>567502.35 5932889.4633</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_71E2A713-318B-4F41-A443-D09CFC10603F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567047.196 5932861.151 566943.2511 5932889.4633 566941.899 5932887.112 
566926.109 5932859.655 566925.011 5932857.747 566850.623 5932868.84 
566849.428 5932859.158 566848.719 5932853.417 566848.591 5932852.383 
566919.171 5932841.619 566944.512 5932837.424 567013.667 5932824.189 
567103.525 5932802.817 567149.128 5932791.971 567193.176 5932781.173 
567245.957 5932767.583 567270.174 5932761.347 567338.647 5932743.431 
567427.516 5932719.637 567491.419 5932702.527 567494.443 5932713.666 
567502.35 5932743.169 567485.287 5932746.96 567390.254 5932768.247 
567363.553 5932775.056 567163.038 5932830.198 567122.404 5932841.663 
567047.196 5932861.151 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6f779b16-20fe-4043-a328-c828dba674a7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567022.941 5932794.967</gml:lowerCorner>
          <gml:upperCorner>567079.203 5932821.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_6b8daf18-657d-40aa-89c0-029534abea4b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_b0f15ed7-64ee-4979-b353-8e8c7a36c673" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9F5D0C38-E814-4B58-AA07-345BDCBE7910" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567079.203 5932808.602 567026.182 5932821.212 567022.941 5932807.591 
567035.587 5932804.581 567063.313 5932797.983 567075.983 5932794.967 
567079.203 5932808.602 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_6b8daf18-657d-40aa-89c0-029534abea4b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567051.077 5932810.386</gml:lowerCorner>
          <gml:upperCorner>567051.077 5932810.386</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6f779b16-20fe-4043-a328-c828dba674a7" />
      <xplan:position>
        <gml:Point gml:id="Gml_50862962-CFD2-4A89-AE86-3FE87A3E0029" srsName="EPSG:25832">
          <gml:pos>567051.077 5932810.386</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b0f15ed7-64ee-4979-b353-8e8c7a36c673">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567051.077 5932804.592</gml:lowerCorner>
          <gml:upperCorner>567051.077 5932804.592</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6f779b16-20fe-4043-a328-c828dba674a7" />
      <xplan:position>
        <gml:Point gml:id="Gml_62C6933C-B4C0-4A6D-BC70-E0BFC126E601" srsName="EPSG:25832">
          <gml:pos>567051.077 5932804.592</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_2aa18469-6fc9-4128-86e5-0c27d251ca8e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566842.603 5932757.841</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932852.383</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_a44c6563-dd2f-4734-8c68-1353654452a1" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7c2071c8-7621-4fb4-8943-57d771fa0d20" />
      <xplan:refTextInhalt xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2" />
      <xplan:refTextInhalt xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a" />
      <xplan:refTextInhalt xlink:href="#GML_4f96ce33-c275-4311-9ea1-737d64b48b78" />
      <xplan:refTextInhalt xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf" />
      <xplan:refTextInhalt xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de" />
      <xplan:refTextInhalt xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1" />
      <xplan:refTextInhalt xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af" />
      <xplan:refTextInhalt xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699" />
      <xplan:refTextInhalt xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62" />
      <xplan:refTextInhalt xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9" />
      <xplan:refTextInhalt xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d" />
      <xplan:refTextInhalt xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830" />
      <xplan:refTextInhalt xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e" />
      <xplan:refTextInhalt xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c" />
      <xplan:refTextInhalt xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd" />
      <xplan:refTextInhalt xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6E167BCE-DB38-4F69-9401-F798E6CCE009" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567103.525 5932802.817 567013.667 5932824.189 566944.512 5932837.424 
566919.171 5932841.619 566848.591 5932852.383 566842.603 5932805.784 
566858.262 5932803.586 566899.326 5932797.323 566923.976 5932793.564 
566925.489 5932793.308 566935.752 5932791.578 567004.845 5932778.378 
567038.535 5932771.936 567092.822 5932757.841 567103.525 5932802.817 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.7</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a44c6563-dd2f-4734-8c68-1353654452a1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566930 5932818.487</gml:lowerCorner>
          <gml:upperCorner>566930 5932818.487</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_2aa18469-6fc9-4128-86e5-0c27d251ca8e" />
      <xplan:position>
        <gml:Point gml:id="Gml_929BC6BB-F4D3-415E-A727-892F8A04AE58" srsName="EPSG:25832">
          <gml:pos>566930 5932818.487</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7c2071c8-7621-4fb4-8943-57d771fa0d20">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566928.39 5932812.523</gml:lowerCorner>
          <gml:upperCorner>566928.39 5932812.523</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_2aa18469-6fc9-4128-86e5-0c27d251ca8e" />
      <xplan:position>
        <gml:Point gml:id="Gml_A7AADA09-C191-4586-9212-8F2096D39F23" srsName="EPSG:25832">
          <gml:pos>566928.39 5932812.523</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ca655909-b5cd-4dc4-9fb5-0145ec5a3724">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567377.878 5932674.7</gml:lowerCorner>
          <gml:upperCorner>567405.389 5932693.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_BA96CD5C-A244-4843-9D76-F7AB9CF8A197" srsName="EPSG:25832">
          <gml:posList>567405.389 5932687.258 567393.798 5932690.361 567381.24 5932693.724 
567377.878 5932681.166 567402.027 5932674.7 567405.389 5932687.258 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_11c0cdfb-2b02-4518-8b2e-1458a8f6c9bd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566842.603 5932803.585</gml:lowerCorner>
          <gml:upperCorner>566865.323 5932852.383</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#GML_a5bbf517-2698-419f-a93a-1996ca8c450c" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_494524D9-0528-4DCE-A3FF-07B35E0A9D64" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566865.319 5932849.807 566865.323 5932849.831 566848.591 5932852.383 
566842.603 5932805.784 566858.269 5932803.585 566865.319 5932849.807 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_ee877546-6cf7-4e4d-bcd2-7a419263dc61">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.166</gml:lowerCorner>
          <gml:upperCorner>566944.546 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B4600C94-F19D-4199-A9E2-81603B1D2A21" srsName="EPSG:25832">
          <gml:posList>566944.546 5932837.394 566939.864 5932838.193 566919.2 5932841.614 
566916.859 5932826.266 566941.637 5932822.166 566944.546 5932837.394 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_2ade7eaf-560c-4a1a-a46b-5b3bf3e3b469">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566904.86 5932833.904</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932843.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CEEC068C-9075-4596-B968-672F01113A94" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566919.2 5932841.614 566906.14 5932843.606 566904.86 5932835.912 
566918.024 5932833.904 566919.2 5932841.614 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1400</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_8341fc3c-a7c7-440d-9be7-99259d8c216e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.546 5932778.857</gml:lowerCorner>
          <gml:upperCorner>567004.523 5932796.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_2776E2B4-6977-41BA-88E7-713C50C6DA28" srsName="EPSG:25832">
          <gml:posList>567004.523 5932791.626 566991.754 5932794.066 566978.985 5932796.505 
566976.546 5932783.737 567002.084 5932778.857 567004.523 5932791.626 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ff172cb9-5612-475f-ad24-913df1c76eb1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.374 5932730.73</gml:lowerCorner>
          <gml:upperCorner>567229.869 5932764.51</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung1>(B)</xplan:gliederung1>
      <xplan:gliederung2>(E)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_0772db52-ce5c-46fd-828d-edd7297e9816" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9cb6a2f4-84b8-4323-94ec-9fcdc3bca5c4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7ae7199c-8628-485a-8a8d-bc43f81e4f66" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_fd656471-9a50-4e8b-8da2-af82666cde09" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_ACA2B4F0-0AAC-4EE1-A04A-B90D54CB9AAC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567224.591 5932737.022 567229.869 5932757.348 567202.283 5932764.51 
567195.374 5932737.898 567222.958 5932730.73 567224.591 5932737.022 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_0772db52-ce5c-46fd-828d-edd7297e9816">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932749.916</gml:lowerCorner>
          <gml:upperCorner>567212.621 5932749.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung1</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ff172cb9-5612-475f-ad24-913df1c76eb1" />
      <xplan:schriftinhalt>(B)</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point gml:id="Gml_B1EB993C-4F62-4815-839A-E30BC549222D" srsName="EPSG:25832">
          <gml:pos>567212.621 5932749.916</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9cb6a2f4-84b8-4323-94ec-9fcdc3bca5c4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932744.409</gml:lowerCorner>
          <gml:upperCorner>567212.621 5932744.409</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ff172cb9-5612-475f-ad24-913df1c76eb1" />
      <xplan:position>
        <gml:Point gml:id="Gml_D0188DDB-D8C5-442F-83A6-17038CE3741D" srsName="EPSG:25832">
          <gml:pos>567212.621 5932744.409</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7ae7199c-8628-485a-8a8d-bc43f81e4f66">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567212.621 5932738.615</gml:lowerCorner>
          <gml:upperCorner>567212.621 5932738.615</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_ff172cb9-5612-475f-ad24-913df1c76eb1" />
      <xplan:position>
        <gml:Point gml:id="Gml_03A49630-9BB4-4EB0-97E3-51E18E3CA4D1" srsName="EPSG:25832">
          <gml:pos>567212.621 5932738.615</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_13fdf7ef-5200-4b91-995c-c5de0a4f0093">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567095.823 5932767.187</gml:lowerCorner>
          <gml:upperCorner>567112.927 5932789.338</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_c74c5ea3-a4cf-416f-a0d5-f6b39041a9c7" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f6746f2f-5c38-4a9a-897b-bdf35dc2f0c5" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_41DFCFD5-4703-4522-A9E1-8B4CACA376EB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567112.927 5932786.175 567100.317 5932789.338 567100.281 5932789.185 
567095.823 5932770.455 567108.409 5932767.187 567112.927 5932786.175 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c74c5ea3-a4cf-416f-a0d5-f6b39041a9c7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567104.369 5932780.416</gml:lowerCorner>
          <gml:upperCorner>567104.369 5932780.416</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_13fdf7ef-5200-4b91-995c-c5de0a4f0093" />
      <xplan:position>
        <gml:Point gml:id="Gml_D338E8C1-58D4-4359-B51F-B392DB468520" srsName="EPSG:25832">
          <gml:pos>567104.369 5932780.416</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f6746f2f-5c38-4a9a-897b-bdf35dc2f0c5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567104.369 5932775.075</gml:lowerCorner>
          <gml:upperCorner>567104.369 5932775.075</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_13fdf7ef-5200-4b91-995c-c5de0a4f0093" />
      <xplan:position>
        <gml:Point gml:id="Gml_EBBA1002-2BA1-4087-B421-1E3B38C58185" srsName="EPSG:25832">
          <gml:pos>567104.369 5932775.075</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_5b15ec94-09cd-4e34-8d26-e06cf33a1191">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567254.372 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932764.526</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_210F6D76-8037-4481-9CD9-3B899C1DAA84" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567270.174 5932761.347 567257.829 5932764.526 567254.372 5932750.986 
567266.628 5932747.803 567270.174 5932761.347 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_8ac68803-4f31-48da-ab0e-3752c4d3ddad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567143.706 5932787.913</gml:lowerCorner>
          <gml:upperCorner>567143.706 5932787.913</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.75</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Point gml:id="Gml_7723F548-C460-4F7F-B057-6515C71F08F8" srsName="EPSG:25832">
          <gml:pos>567143.706 5932787.913</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_c45e5acb-2fab-43f3-880d-30526f4c94eb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567266.628 5932696.257</gml:lowerCorner>
          <gml:upperCorner>567325.501 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_D9A27ACC-69E9-494A-B4CF-BA2F9413CCD1" srsName="EPSG:25832">
          <gml:posList>567322.893 5932747.553 567270.174 5932761.347 567266.628 5932747.803 
567279.2 5932744.514 567306.772 5932737.3 567297.868 5932703.269 
567321.864 5932696.257 567325.501 5932708.738 567313.643 5932712.203 
567319.349 5932734.009 567322.893 5932747.553 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_2934b32c-6d07-4d3d-9d73-00d133e28839">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566847.954 5932803.585</gml:lowerCorner>
          <gml:upperCorner>566862.976 5932836.346</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C3AE4E98-4E50-4E75-9772-CE04CE4951DF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566862.976 5932834.447 566851.949 5932836.346 566847.954 5932805.033 
566858.269 5932803.585 566862.976 5932834.447 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_ca387c49-a139-4e28-8c5c-e6c506d9ba9c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932757.841</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_EC47683B-FFAC-46E1-A594-615FA6811D7D" srsName="EPSG:25832">
          <gml:posList>567103.525 5932802.817 567100.317 5932789.337 567100.281 5932789.185 
567095.824 5932770.455 567092.822 5932757.841 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_798d558b-73d7-416d-8862-237cb57c9540">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567242.452 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932767.583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_292CF1E2-F5C2-419C-8569-8C1E8D776EFB" srsName="EPSG:25832">
          <gml:posList>567270.174 5932761.347 567245.957 5932767.583 567242.452 5932754.081 
567266.628 5932747.803 567270.174 5932761.347 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_0d0ea6ef-50a9-47c7-8e8a-4c16fd4fbaf4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567056.034 5932761.11</gml:lowerCorner>
          <gml:upperCorner>567083.489 5932780.007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d433c17c-7f10-4790-8edf-99e8de7dd6d9" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c5cc02f9-a56f-430e-90a5-4e22c26b5593" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AECBA238-1257-46D3-9B81-28868CDC8155" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567083.489 5932773.657 567070.652 5932776.99 567059.035 5932780.007 
567056.034 5932767.393 567080.231 5932761.11 567083.489 5932773.657 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d433c17c-7f10-4790-8edf-99e8de7dd6d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567069.698 5932772.669</gml:lowerCorner>
          <gml:upperCorner>567069.698 5932772.669</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0d0ea6ef-50a9-47c7-8e8a-4c16fd4fbaf4" />
      <xplan:position>
        <gml:Point gml:id="Gml_914D90F2-311F-4955-8617-F9F97E688C4D" srsName="EPSG:25832">
          <gml:pos>567069.698 5932772.669</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c5cc02f9-a56f-430e-90a5-4e22c26b5593">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567069.698 5932767.328</gml:lowerCorner>
          <gml:upperCorner>567069.698 5932767.328</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_0d0ea6ef-50a9-47c7-8e8a-4c16fd4fbaf4" />
      <xplan:position>
        <gml:Point gml:id="Gml_6E98D2F8-F892-408B-8545-2F29CBAC9986" srsName="EPSG:25832">
          <gml:pos>567069.698 5932767.328</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6f1f3557-da54-47ca-89d0-248d8b9c76ae">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567417.947 5932680.533</gml:lowerCorner>
          <gml:upperCorner>567436.453 5932706.113</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_5BDFFB9D-BB4B-4698-8989-9C56104EF593" srsName="EPSG:25832">
          <gml:posList>567436.453 5932702.751 567423.896 5932706.113 567417.947 5932683.896 
567430.505 5932680.533 567436.453 5932702.751 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c5f6c94c-2390-4ee6-a167-73416b289a03">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567242.452 5932747.803</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932767.583</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_85603634-4129-4520-9d5d-0f36ba4ec383" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_eab8154e-4634-4d14-adb1-7b51e589f122" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4929F7ED-9298-4A12-BC61-AFF2F6F44DED" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567270.174 5932761.347 567245.957 5932767.583 567242.452 5932754.081 
567266.628 5932747.803 567270.174 5932761.347 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_85603634-4129-4520-9d5d-0f36ba4ec383">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567256.304 5932754.198</gml:lowerCorner>
          <gml:upperCorner>567256.304 5932754.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c5f6c94c-2390-4ee6-a167-73416b289a03" />
      <xplan:position>
        <gml:Point gml:id="Gml_47DD7037-477D-4FBA-9005-C82379AABABB" srsName="EPSG:25832">
          <gml:pos>567256.304 5932754.198</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_eab8154e-4634-4d14-adb1-7b51e589f122">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567256.304 5932759.992</gml:lowerCorner>
          <gml:upperCorner>567256.304 5932759.992</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c5f6c94c-2390-4ee6-a167-73416b289a03" />
      <xplan:position>
        <gml:Point gml:id="Gml_5679B91C-DC8C-4277-A4ED-E6BF00233114" srsName="EPSG:25832">
          <gml:pos>567256.304 5932759.992</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f750899c-e632-46e7-9864-f2403dfdf334">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567261.31 5932723.846</gml:lowerCorner>
          <gml:upperCorner>567279.2 5932747.803</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_D9FB1BB8-84C5-4E76-9CFC-768A5F58E774" srsName="EPSG:25832">
          <gml:posList>567279.2 5932744.514 567266.628 5932747.803 567261.31 5932727.488 
567273.793 5932723.846 567279.2 5932744.514 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_1fec35eb-3032-4ed6-a9cf-3c36ed5b4652">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567168.773 5932775.928</gml:lowerCorner>
          <gml:upperCorner>567168.773 5932775.928</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Point gml:id="Gml_C1880B08-6DD9-4BA2-9520-41BC7D35575F" srsName="EPSG:25832">
          <gml:pos>567168.773 5932775.928</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8dbfebc6-0a1c-4ee6-be68-f52f642e5991">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932707.901</gml:lowerCorner>
          <gml:upperCorner>567285.65 5932727.488</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_436cc005-c54a-46db-87b4-0009f6121f90" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_69a35e30-7c8a-4da1-867b-beacd1d91a98" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1209139B-C1C3-429E-946B-D82230864245" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567285.65 5932720.382 567273.793 5932723.846 567261.31 5932727.488 
567258.017 5932714.912 567282.014 5932707.901 567285.65 5932720.382 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_436cc005-c54a-46db-87b4-0009f6121f90">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567273.005 5932714.09</gml:lowerCorner>
          <gml:upperCorner>567273.005 5932714.09</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8dbfebc6-0a1c-4ee6-be68-f52f642e5991" />
      <xplan:position>
        <gml:Point gml:id="Gml_97F6E73D-044C-436F-B4A5-D85959960AFE" srsName="EPSG:25832">
          <gml:pos>567273.005 5932714.09</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_69a35e30-7c8a-4da1-867b-beacd1d91a98">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567273.005 5932719.431</gml:lowerCorner>
          <gml:upperCorner>567273.005 5932719.431</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8dbfebc6-0a1c-4ee6-be68-f52f642e5991" />
      <xplan:position>
        <gml:Point gml:id="Gml_ADACD6B6-4225-458E-ACE9-8DC63AD49BA4" srsName="EPSG:25832">
          <gml:pos>567273.005 5932719.431</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_63fb2da6-f94c-4b26-9823-38cfc2d3bfb6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567022.941 5932804.581</gml:lowerCorner>
          <gml:upperCorner>567035.587 5932807.591</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_4531220B-FDDE-4C12-B164-95D7E82CFC83" srsName="EPSG:25832">
          <gml:posList>567022.941 5932807.591 567035.587 5932804.581 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d6b5bede-11cd-4e5f-b074-23920a818290">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566899.289 5932793.365</gml:lowerCorner>
          <gml:upperCorner>566926.952 5932810.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e54969cc-46f2-4361-9965-5040f418f2a5" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F0CE2E49-5B64-46BE-A45E-97C3DD811993" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566926.952 5932806.22 566914.1 5932808.177 566901.249 5932810.137 
566900.314 5932804.008 566899.289 5932797.285 566924.003 5932793.516 
566924.991 5932793.365 566926.952 5932806.22 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e54969cc-46f2-4361-9965-5040f418f2a5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566918.914 5932800.868</gml:lowerCorner>
          <gml:upperCorner>566918.914 5932800.868</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d6b5bede-11cd-4e5f-b074-23920a818290" />
      <xplan:position>
        <gml:Point gml:id="Gml_35D9097A-7400-4CD2-8B36-52E00FA1FB22" srsName="EPSG:25832">
          <gml:pos>566918.914 5932800.868</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_7f8e5623-8052-47a2-8e6f-455baa906726">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.166</gml:lowerCorner>
          <gml:upperCorner>566944.546 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_38f73e57-9eef-4065-b564-07292912b163" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E62D0FCF-24CB-47E5-907B-CCB11D7971BB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566944.546 5932837.394 566939.864 5932838.193 566919.2 5932841.614 
566916.859 5932826.266 566941.637 5932822.166 566944.546 5932837.394 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_38f73e57-9eef-4065-b564-07292912b163">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566929.173 5932832.096</gml:lowerCorner>
          <gml:upperCorner>566929.173 5932832.096</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_7f8e5623-8052-47a2-8e6f-455baa906726" />
      <xplan:position>
        <gml:Point gml:id="Gml_F3B05992-480D-488F-B532-5EBF92D78FAF" srsName="EPSG:25832">
          <gml:pos>566929.173 5932832.096</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_16a5aba2-9c83-4175-a28b-c8d6e5a4a640">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566991.754 5932776.34</gml:lowerCorner>
          <gml:upperCorner>567026.182 5932827.172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1D07ED35-1DA3-4BF7-B6B7-C3E8FCB18291" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567026.182 5932821.212 567013.667 5932824.189 566998.079 5932827.172 
566995.451 5932813.416 566991.754 5932794.066 567004.523 5932791.626 
567002.093 5932778.904 567004.845 5932778.378 567015.503 5932776.34 
567026.182 5932821.212 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_32ac26a6-25b6-472a-9bd0-095fbb07bdf6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.783 5932786.645</gml:lowerCorner>
          <gml:upperCorner>566963.761 5932804.293</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_486C9333-29BB-4C47-AFA4-BF308E03DD99" srsName="EPSG:25832">
          <gml:posList>566963.761 5932799.414 566950.991 5932801.853 566938.222 5932804.293 
566935.783 5932791.524 566961.321 5932786.645 566963.761 5932799.414 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_cfbbc7f5-d158-4fb1-a87c-f795f0011c86">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567414.585 5932664.872</gml:lowerCorner>
          <gml:upperCorner>567442.096 5932683.896</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d30778b9-209d-4939-a2cd-64f03313fc15" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c69485a9-5ff7-4557-9648-fd65bd7e140e" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D4D6AB7E-BE0C-4F46-97FA-7040E0E77BD5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567442.096 5932677.43 567430.505 5932680.533 567417.947 5932683.896 
567414.585 5932671.338 567438.734 5932664.872 567442.096 5932677.43 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d30778b9-209d-4939-a2cd-64f03313fc15">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.341 5932671.17</gml:lowerCorner>
          <gml:upperCorner>567428.341 5932671.17</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cfbbc7f5-d158-4fb1-a87c-f795f0011c86" />
      <xplan:position>
        <gml:Point gml:id="Gml_339A9D0A-552C-431F-9656-FFAD221B7EE9" srsName="EPSG:25832">
          <gml:pos>567428.341 5932671.17</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c69485a9-5ff7-4557-9648-fd65bd7e140e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.341 5932676.511</gml:lowerCorner>
          <gml:upperCorner>567428.341 5932676.511</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cfbbc7f5-d158-4fb1-a87c-f795f0011c86" />
      <xplan:position>
        <gml:Point gml:id="Gml_8294C560-4329-4951-83E2-A1B6EE79C879" srsName="EPSG:25832">
          <gml:pos>567428.341 5932676.511</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_e4e2251e-6329-4655-9aa4-f4bbaa18be04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932714.912</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d5d2be62-0d59-42f6-a64a-84924879b589" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e7af056f-472c-47be-848d-7b7c5f3c41a0" />
      <xplan:refTextInhalt xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2" />
      <xplan:refTextInhalt xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a" />
      <xplan:refTextInhalt xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf" />
      <xplan:refTextInhalt xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de" />
      <xplan:refTextInhalt xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1" />
      <xplan:refTextInhalt xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af" />
      <xplan:refTextInhalt xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699" />
      <xplan:refTextInhalt xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62" />
      <xplan:refTextInhalt xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9" />
      <xplan:refTextInhalt xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d" />
      <xplan:refTextInhalt xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830" />
      <xplan:refTextInhalt xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e" />
      <xplan:refTextInhalt xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c" />
      <xplan:refTextInhalt xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd" />
      <xplan:refTextInhalt xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_07434D99-49D0-4F36-9CA8-3651A93EA1D8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567270.174 5932761.347 567245.957 5932767.583 567193.176 5932781.173 
567181.156 5932734.868 567258.017 5932714.912 567270.174 5932761.347 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.8</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d5d2be62-0d59-42f6-a64a-84924879b589">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567238.324 5932744.791</gml:lowerCorner>
          <gml:upperCorner>567238.324 5932744.791</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4e2251e-6329-4655-9aa4-f4bbaa18be04" />
      <xplan:position>
        <gml:Point gml:id="Gml_DA93A991-B962-4883-8117-2CDA7B9F5A67" srsName="EPSG:25832">
          <gml:pos>567238.324 5932744.791</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e7af056f-472c-47be-848d-7b7c5f3c41a0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567238.325 5932744.79</gml:lowerCorner>
          <gml:upperCorner>567238.325 5932744.79</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4e2251e-6329-4655-9aa4-f4bbaa18be04" />
      <xplan:position>
        <gml:Point gml:id="Gml_D01DAE17-196D-4A32-8D0B-D6D603F8EF93" srsName="EPSG:25832">
          <gml:pos>567238.325 5932744.79</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_a059a827-7a77-4a4e-b930-f77ed029eee0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567423.896 5932654.275</gml:lowerCorner>
          <gml:upperCorner>567481.725 5932719.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_163f2452-281f-402a-b37b-94ceb90c2cbe" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_30e32b73-dff9-483e-9e12-8bcc9200146b" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_857D5696-AAB1-4C22-9708-19975CB5A071" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567481.725 5932666.819 567470.593 5932669.8 567480.162 5932705.541 
567427.517 5932719.637 567423.896 5932706.113 567436.453 5932702.751 
567463.984 5932695.38 567454.673 5932660.605 567478.32 5932654.275 
567481.725 5932666.819 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_163f2452-281f-402a-b37b-94ceb90c2cbe">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.029 5932702.327</gml:lowerCorner>
          <gml:upperCorner>567452.029 5932702.327</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a059a827-7a77-4a4e-b930-f77ed029eee0" />
      <xplan:position>
        <gml:Point gml:id="Gml_AD37DA18-2EF6-4814-9C44-134A31724696" srsName="EPSG:25832">
          <gml:pos>567452.029 5932702.327</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_30e32b73-dff9-483e-9e12-8bcc9200146b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.029 5932708.121</gml:lowerCorner>
          <gml:upperCorner>567452.029 5932708.121</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_a059a827-7a77-4a4e-b930-f77ed029eee0" />
      <xplan:position>
        <gml:Point gml:id="Gml_E179FC2E-1AB7-471F-8B9A-98DC01D9870E" srsName="EPSG:25832">
          <gml:pos>567452.029 5932708.121</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_45452be9-af94-408b-87c8-bfb37aeaae0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567100.317 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932802.817</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_e27efe51-548d-405a-b6f3-3d39fa616b34" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a1ad661d-e2c5-48b3-b576-88ab29efcbaa" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CA621071-64CD-44A3-BE88-DA0FBFB42B57" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567154.527 5932745.398 567149.128 5932791.971 567103.525 5932802.817 
567100.317 5932789.338 567113.013 5932786.154 567137.502 5932780.326 
567141.581 5932745.143 567155.075 5932741.64 567154.527 5932745.398 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e27efe51-548d-405a-b6f3-3d39fa616b34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567128.362 5932786.204</gml:lowerCorner>
          <gml:upperCorner>567128.362 5932786.204</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_45452be9-af94-408b-87c8-bfb37aeaae0d" />
      <xplan:position>
        <gml:Point gml:id="Gml_12052643-FD7F-4B1A-9B21-AE38D72DD8C5" srsName="EPSG:25832">
          <gml:pos>567128.362 5932786.204</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a1ad661d-e2c5-48b3-b576-88ab29efcbaa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567128.362 5932791.999</gml:lowerCorner>
          <gml:upperCorner>567128.362 5932791.999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_45452be9-af94-408b-87c8-bfb37aeaae0d" />
      <xplan:position>
        <gml:Point gml:id="Gml_8814981F-EE52-4AE3-9030-D25403AA91AD" srsName="EPSG:25832">
          <gml:pos>567128.362 5932791.999</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f0acca72-d01e-4a95-ba82-db3675ed653b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567423.896 5932654.275</gml:lowerCorner>
          <gml:upperCorner>567481.725 5932719.637</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_7053FC34-AE7B-41A8-AB0F-3212564D1827" srsName="EPSG:25832">
          <gml:posList>567427.517 5932719.637 567423.896 5932706.113 567436.453 5932702.751 
567463.984 5932695.38 567454.673 5932660.605 567478.32 5932654.275 
567481.725 5932666.819 567470.593 5932669.8 567480.162 5932705.541 
567427.517 5932719.637 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_366f3f01-dd6c-45ca-97ee-4d6dc1372c2c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566875.105 5932804.008</gml:lowerCorner>
          <gml:upperCorner>566901.745 5932817.244</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_dad5dbd9-c264-4272-a68c-86bcd2d75a10" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E682BE06-03CA-40DC-BA77-D8425FDB4FF2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566901.249 5932810.137 566901.745 5932813.399 566876.538 5932817.244 
566875.105 5932807.852 566900.314 5932804.008 566901.249 5932810.137 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dad5dbd9-c264-4272-a68c-86bcd2d75a10">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566888.426 5932810.626</gml:lowerCorner>
          <gml:upperCorner>566888.426 5932810.626</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_366f3f01-dd6c-45ca-97ee-4d6dc1372c2c" />
      <xplan:position>
        <gml:Point gml:id="Gml_C80A944F-7F63-4551-9191-ADBFD4D3A660" srsName="EPSG:25832">
          <gml:pos>566888.426 5932810.626</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="GML_3fc11cdb-b7eb-4c39-a0b8-7f4cf383708d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566841.322 5932638.488</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:refTextInhalt xlink:href="#GML_e6ef7a8a-cdde-4f34-a748-a84cb7831fd4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_0531E713-DB62-4D60-9CB3-42A24E278B68" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567071.225 5932875.978 566952.478 5932905.509 566943.2511 5932889.4633 
566925.077 5932857.666 566850.623 5932868.84 566848.591 5932852.383 
566842.603 5932805.784 566841.516 5932797.098 566841.322 5932795.563 
566856.166 5932793.67 566876.457 5932790.784 566893.555 5932788.149 
566925.816 5932782.812 566925.622 5932781.675 566957.155 5932775.947 
567001.253 5932766.989 567024.668 5932762.288 567034.553 5932760.037 
567099.41 5932743.34 567165.898 5932726.231 567207.598 5932715.718 
567251.407 5932703.902 567309.648 5932687.426 567309.188 5932685.742 
567324.249 5932681.582 567379.905 5932665.733 567462.605 5932641.979 
567474.886 5932638.488 567477.061 5932645.289 567475.978 5932645.649 
567478.319 5932654.273 567481.725 5932666.819 567491.419 5932702.527 
567500.798 5932737.377 567502.35 5932743.169 567509.287 5932769.105 
567416.044 5932790.244 567354.506 5932803.777 567322.897 5932810.727 
567243.981 5932831.427 567071.225 5932875.978 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>4000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_d0188aa3-4f7b-4d35-bd8f-a39cc777b93c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566914.1 5932791.571</gml:lowerCorner>
          <gml:upperCorner>566941.637 5932826.266</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_99BFAB8D-5A39-4852-81E5-E95408E89D6E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566938.222 5932804.293 566941.637 5932822.166 566916.859 5932826.266 
566914.1 5932808.177 566926.952 5932806.22 566924.995 5932793.392 
566925.489 5932793.308 566935.792 5932791.571 566938.222 5932804.293 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d137f77a-1da0-4217-ba41-6dae7c3ebf5e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567347.101 5932712.579</gml:lowerCorner>
          <gml:upperCorner>567403.367 5932740.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_f87f4c4f-5ff8-45b3-b9e5-2001ce450f37" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_ee00dc13-7e0e-44a6-b4c6-990c40489501" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F0D12ED1-904A-42D8-B7C6-CEC97289D82D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567403.367 5932726.103 567350.721 5932740.198 567347.101 5932726.674 
567359.658 5932723.312 567387.189 5932715.941 567399.746 5932712.579 
567403.367 5932726.103 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f87f4c4f-5ff8-45b3-b9e5-2001ce450f37">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567375.234 5932728.683</gml:lowerCorner>
          <gml:upperCorner>567375.234 5932728.683</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d137f77a-1da0-4217-ba41-6dae7c3ebf5e" />
      <xplan:position>
        <gml:Point gml:id="Gml_C119DDD0-620B-497C-947F-1A47758E69D9" srsName="EPSG:25832">
          <gml:pos>567375.234 5932728.683</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ee00dc13-7e0e-44a6-b4c6-990c40489501">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567375.234 5932722.888</gml:lowerCorner>
          <gml:upperCorner>567375.234 5932722.888</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d137f77a-1da0-4217-ba41-6dae7c3ebf5e" />
      <xplan:position>
        <gml:Point gml:id="Gml_927A3FFA-D4DF-40F3-9777-7D0FE20D3EF8" srsName="EPSG:25832">
          <gml:pos>567375.234 5932722.888</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_288452c4-3611-4e09-84c0-2029abab6a39">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932751.558</gml:lowerCorner>
          <gml:upperCorner>567120.277 5932770.455</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_71C19C69-AFA8-47B7-9DEC-CE93BEF905EC" srsName="EPSG:25832">
          <gml:posList>567120.277 5932764.106 567108.409 5932767.187 567095.823 5932770.455 
567092.822 5932757.841 567117.019 5932751.558 567120.277 5932764.106 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_8ebf4ef9-132a-43c2-ac70-4a5a95a49351">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567461.102 5932715.143</gml:lowerCorner>
          <gml:upperCorner>567461.102 5932715.143</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.75</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Point gml:id="Gml_4DD9253E-D440-4030-871C-958254231D54" srsName="EPSG:25832">
          <gml:pos>567461.102 5932715.143</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_992fe65b-4d1b-41a4-834d-49236d0b9d11">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566876.538 5932813.399</gml:lowerCorner>
          <gml:upperCorner>566904.004 5932832.072</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_62AD4A44-B109-4C3B-B4AC-9D9437687CCD" srsName="EPSG:25832">
          <gml:posList>566904.004 5932828.228 566878.799 5932832.072 566876.538 5932817.244 
566901.745 5932813.399 566904.004 5932828.228 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_f303aa68-317d-4992-a7b2-ae920695dbfb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.36 5932853.767</gml:lowerCorner>
          <gml:upperCorner>566869.36 5932853.767</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">8.6</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Point gml:id="Gml_AFE0CF74-1D29-472A-93FC-2717F8042AC4" srsName="EPSG:25832">
          <gml:pos>566869.36 5932853.767</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_cc91cd1a-a10f-4344-ae85-2ad7682a6fcf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566874.087 5932803.585</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_315C9E21-1BA7-4151-B4CA-9DFB897763B5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566874.087 5932801.172 566858.269 5932803.585 566858.112 5932802.554 
566873.929 5932800.141 566874.087 5932801.172 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_1dfc27bb-4892-4c38-aed1-42eabc4a55dd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566938.222 5932801.853</gml:lowerCorner>
          <gml:upperCorner>566998.078 5932837.394</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_220f7fba-87d5-4b75-bbfa-5bb53f6f4f92" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_3e94ac43-7796-4f08-a888-b77863b192dc" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_5B62C35B-5202-417D-B9C7-D79F5C33FCF3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566998.078 5932827.167 566944.546 5932837.394 566938.222 5932804.293 
566950.991 5932801.853 566954.688 5932821.203 566982.682 5932815.855 
566995.451 5932813.416 566998.078 5932827.167 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_220f7fba-87d5-4b75-bbfa-5bb53f6f4f92">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.999 5932827.699</gml:lowerCorner>
          <gml:upperCorner>566969.999 5932827.699</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1dfc27bb-4892-4c38-aed1-42eabc4a55dd" />
      <xplan:position>
        <gml:Point gml:id="Gml_C1F0173B-153F-4CD4-B726-BA59A1C80405" srsName="EPSG:25832">
          <gml:pos>566969.999 5932827.699</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3e94ac43-7796-4f08-a888-b77863b192dc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566969.999 5932821.905</gml:lowerCorner>
          <gml:upperCorner>566969.999 5932821.905</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_1dfc27bb-4892-4c38-aed1-42eabc4a55dd" />
      <xplan:position>
        <gml:Point gml:id="Gml_63261C32-D731-42B9-B8A6-D484822620D1" srsName="EPSG:25832">
          <gml:pos>566969.999 5932821.905</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b99f5f46-d32c-4796-9a95-efb93cdd3445">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932707.901</gml:lowerCorner>
          <gml:upperCorner>567285.65 5932727.488</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_39BE1DB9-93FE-4FA0-BEE9-5A867D11520A" srsName="EPSG:25832">
          <gml:posList>567273.793 5932723.846 567261.31 5932727.488 567258.017 5932714.912 
567282.014 5932707.901 567285.65 5932720.382 567273.793 5932723.846 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_bb101a4b-bfd9-4dee-a952-7edaf0eb42e1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567162.168 5932733.046</gml:lowerCorner>
          <gml:upperCorner>567162.168 5932733.046</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">5.4</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Point gml:id="Gml_3E1B853A-A61D-48FC-909D-005E63420A43" srsName="EPSG:25832">
          <gml:pos>567162.168 5932733.046</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_93892231-742a-46ad-bc0a-532018ab7236">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566916.859 5932822.968</gml:lowerCorner>
          <gml:upperCorner>566939.864 5932841.614</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_06EDC7AC-B490-4746-9C68-36DAD87F14F1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566939.864 5932838.193 566919.2 5932841.614 566916.859 5932826.266 
566936.789 5932822.968 566939.864 5932838.193 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_95908282-8125-4a08-87c6-7c25ddefda97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566875.105 5932804.008</gml:lowerCorner>
          <gml:upperCorner>566901.745 5932817.244</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_2996D7DB-92F9-4AF1-9982-150BB5C134D2" srsName="EPSG:25832">
          <gml:posList>566901.745 5932813.399 566876.538 5932817.244 566875.105 5932807.852 
566900.314 5932804.008 566901.249 5932810.137 566901.745 5932813.399 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_24cd4e41-95f6-4c31-8711-6b0673c48a0e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.983 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932808.602</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_4F7F4A26-62D8-44F3-A1D5-0729985658F3" srsName="EPSG:25832">
          <gml:posList>567103.525 5932802.817 567079.203 5932808.602 567075.983 5932794.967 
567100.281 5932789.185 567103.525 5932802.817 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_deaaa4cb-fef0-40af-b249-a6743aa695dd">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566976.546 5932778.857</gml:lowerCorner>
          <gml:upperCorner>567004.523 5932796.505</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ec583607-38e1-4e4b-81e1-59510e28139d" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CE7DF725-343C-4CC1-924D-7EA58CCE0EF7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567004.523 5932791.626 566991.754 5932794.066 566978.985 5932796.505 
566976.546 5932783.737 567002.084 5932778.857 567004.523 5932791.626 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ec583607-38e1-4e4b-81e1-59510e28139d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566990.534 5932787.681</gml:lowerCorner>
          <gml:upperCorner>566990.534 5932787.681</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_deaaa4cb-fef0-40af-b249-a6743aa695dd" />
      <xplan:position>
        <gml:Point gml:id="Gml_80250E67-E076-4BF8-A913-E3528135BF14" srsName="EPSG:25832">
          <gml:pos>566990.534 5932787.681</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_3ad88da2-f1a7-4d09-8023-6ee4d7b0fa6e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567056.034 5932761.11</gml:lowerCorner>
          <gml:upperCorner>567083.489 5932780.007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6BA28776-0E40-4553-958E-F7B91AE86F64" srsName="EPSG:25832">
          <gml:posList>567083.489 5932773.657 567070.652 5932776.99 567059.035 5932780.007 
567056.034 5932767.393 567080.231 5932761.11 567083.489 5932773.657 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_a2eeafdd-573a-4ad3-826b-7ec4e2452a0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932714.912</gml:lowerCorner>
          <gml:upperCorner>567270.174 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_5447A1BB-9E70-49B7-A6FB-8CF16830DCE1" srsName="EPSG:25832">
          <gml:posList>567270.174 5932761.347 567258.017 5932714.912 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a0b7e9f5-09b8-412c-9fa7-4da902b89f30">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567414.585 5932664.872</gml:lowerCorner>
          <gml:upperCorner>567442.096 5932683.896</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_0A11D77D-CE42-4B83-995C-E17F04693281" srsName="EPSG:25832">
          <gml:posList>567430.505 5932680.533 567417.947 5932683.896 567414.585 5932671.338 
567438.734 5932664.872 567442.096 5932677.43 567430.505 5932680.533 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6829362e-dfa9-445a-b18b-be68acd05371">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567266.628 5932734.009</gml:lowerCorner>
          <gml:upperCorner>567322.893 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b5a075b7-a142-418c-8d84-312f843cb25b" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_dcec9cc3-ede0-40aa-8327-1983aab6647a" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BC864A00-3AC1-466C-BD67-4E6303341922" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567322.893 5932747.553 567270.174 5932761.347 567266.628 5932747.803 
567279.2 5932744.514 567306.772 5932737.3 567319.349 5932734.009 
567322.893 5932747.553 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b5a075b7-a142-418c-8d84-312f843cb25b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567294.761 5932744.178</gml:lowerCorner>
          <gml:upperCorner>567294.761 5932744.178</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6829362e-dfa9-445a-b18b-be68acd05371" />
      <xplan:position>
        <gml:Point gml:id="Gml_3AA360D3-A756-4148-986B-5A36228C4700" srsName="EPSG:25832">
          <gml:pos>567294.761 5932744.178</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dcec9cc3-ede0-40aa-8327-1983aab6647a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567294.761 5932749.973</gml:lowerCorner>
          <gml:upperCorner>567294.761 5932749.973</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6829362e-dfa9-445a-b18b-be68acd05371" />
      <xplan:position>
        <gml:Point gml:id="Gml_6C744050-3C59-4524-AC42-8479E3CC620A" srsName="EPSG:25832">
          <gml:pos>567294.761 5932749.973</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_41292320-d863-4a1d-95d5-c8aa074b141f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567059.035 5932776.739</gml:lowerCorner>
          <gml:upperCorner>567075.96 5932797.983</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_ef5f32ed-c552-4708-b957-8f71d639d5a6" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_27f09a05-2f77-4f71-8daa-237957f2d1ef" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_83491C1C-BA23-4C28-AB53-7E5B27BC989D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567075.96 5932794.973 567063.313 5932797.983 567059.035 5932780.007 
567070.652 5932776.99 567071.62 5932776.739 567075.96 5932794.973 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_ef5f32ed-c552-4708-b957-8f71d639d5a6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567067.482 5932789.553</gml:lowerCorner>
          <gml:upperCorner>567067.482 5932789.553</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_41292320-d863-4a1d-95d5-c8aa074b141f" />
      <xplan:position>
        <gml:Point gml:id="Gml_BDB8D025-B31C-432A-93A3-E04AE81B2FAA" srsName="EPSG:25832">
          <gml:pos>567067.482 5932789.553</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_27f09a05-2f77-4f71-8daa-237957f2d1ef">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567067.482 5932784.212</gml:lowerCorner>
          <gml:upperCorner>567067.482 5932784.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_41292320-d863-4a1d-95d5-c8aa074b141f" />
      <xplan:position>
        <gml:Point gml:id="Gml_6D4DC273-C9F2-4ABE-9465-352E28044E45" srsName="EPSG:25832">
          <gml:pos>567067.482 5932784.212</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_285ceaa0-1a01-4527-8a53-1f2f23dbc5cf">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567381.24 5932690.361</gml:lowerCorner>
          <gml:upperCorner>567399.746 5932715.941</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_745DEBFC-F73A-4B2F-A2A3-3847E8E1D9C2" srsName="EPSG:25832">
          <gml:posList>567399.746 5932712.579 567387.189 5932715.941 567381.24 5932693.724 
567393.798 5932690.361 567399.746 5932712.579 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_771a4c9b-dbad-42b2-b923-e550ffd1f3b7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567075.983 5932789.185</gml:lowerCorner>
          <gml:upperCorner>567103.525 5932808.602</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_08948a85-5183-4b8d-98c1-c493f155491d" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_a9970c7a-79d6-44ed-b6bf-5dbf42d1819a" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A6C50936-2D42-4B7D-AB3D-72E8828D3215" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567103.525 5932802.817 567079.203 5932808.602 567075.983 5932794.967 
567100.281 5932789.185 567103.525 5932802.817 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_08948a85-5183-4b8d-98c1-c493f155491d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567089.748 5932795.393</gml:lowerCorner>
          <gml:upperCorner>567089.748 5932795.393</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_771a4c9b-dbad-42b2-b923-e550ffd1f3b7" />
      <xplan:position>
        <gml:Point gml:id="Gml_DEBCD758-6B6E-45FF-BC23-68978F3FEDE4" srsName="EPSG:25832">
          <gml:pos>567089.748 5932795.393</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a9970c7a-79d6-44ed-b6bf-5dbf42d1819a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567089.748 5932801.187</gml:lowerCorner>
          <gml:upperCorner>567089.748 5932801.187</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_771a4c9b-dbad-42b2-b923-e550ffd1f3b7" />
      <xplan:position>
        <gml:Point gml:id="Gml_AC65C09A-F462-4C01-83D9-0468C26621E3" srsName="EPSG:25832">
          <gml:pos>567089.748 5932801.187</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_6e0999cd-b43d-48bd-9039-4dd8373cabaa">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567417.947 5932680.533</gml:lowerCorner>
          <gml:upperCorner>567436.453 5932706.113</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">34.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_d9086011-c3c5-4582-8d8b-41df2492e8c5" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_81f9c336-b916-4cd2-81db-3bb9973748b9" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C0A4C648-DFC0-4ED6-8C5E-7DA644ECD68D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567436.453 5932702.751 567423.896 5932706.113 567417.947 5932683.896 
567430.505 5932680.533 567436.453 5932702.751 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_d9086011-c3c5-4582-8d8b-41df2492e8c5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567427.2 5932690.109</gml:lowerCorner>
          <gml:upperCorner>567427.2 5932690.109</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6e0999cd-b43d-48bd-9039-4dd8373cabaa" />
      <xplan:position>
        <gml:Point gml:id="Gml_39B0D5EC-9E47-4C9C-8B87-41B76FDD15E3" srsName="EPSG:25832">
          <gml:pos>567427.2 5932690.109</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_81f9c336-b916-4cd2-81db-3bb9973748b9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567427.2 5932695.45</gml:lowerCorner>
          <gml:upperCorner>567427.2 5932695.45</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_6e0999cd-b43d-48bd-9039-4dd8373cabaa" />
      <xplan:position>
        <gml:Point gml:id="Gml_9FADCDA1-FE15-4C04-B9EA-5C707B70DB92" srsName="EPSG:25832">
          <gml:pos>567427.2 5932695.45</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_8b29af24-1126-4ac9-a7bd-24702842fe87">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.526 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.516 5932722.929</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">8</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BEE780A7-AB50-4DFE-9095-7499D93A0E74" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567427.516 5932719.637 567415.221 5932722.929 567411.526 5932709.425 
567423.896 5932706.113 567427.516 5932719.637 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1100</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_8fcaa7c4-056a-4bb4-a8a8-d28fd3829d95">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567337.79 5932688.537</gml:lowerCorner>
          <gml:upperCorner>567359.658 5932726.674</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_dbf21e70-22fa-4917-8444-2918a72b9516" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8EBB8A7E-41B7-4EA4-9F6D-256DC748CED9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567359.658 5932723.312 567347.101 5932726.674 567337.79 5932691.899 
567350.348 5932688.537 567359.658 5932723.312 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_dbf21e70-22fa-4917-8444-2918a72b9516">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567348.724 5932707.606</gml:lowerCorner>
          <gml:upperCorner>567348.724 5932707.606</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_8fcaa7c4-056a-4bb4-a8a8-d28fd3829d95" />
      <xplan:position>
        <gml:Point gml:id="Gml_71D3D753-DE17-4176-BDBD-F65CC9D967B3" srsName="EPSG:25832">
          <gml:pos>567348.724 5932707.606</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9d6fb6b8-2e1a-4eba-80b1-79d191906936">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567181.156 5932731.601</gml:lowerCorner>
          <gml:upperCorner>567245.957 5932781.173</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_71b00e63-fe33-4ba5-b6f1-0529b9525728" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_8ef2b2e5-3f88-4fb5-aee9-2741d7305301" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C2A93497-B27E-4104-B2FC-07D992ADFFB3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567245.957 5932767.583 567193.176 5932781.173 567181.156 5932734.868 
567193.739 5932731.601 567202.283 5932764.51 567229.869 5932757.348 
567224.591 5932737.022 567237.174 5932733.755 567245.957 5932767.583 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_71b00e63-fe33-4ba5-b6f1-0529b9525728">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567217.826 5932764.134</gml:lowerCorner>
          <gml:upperCorner>567217.826 5932764.134</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d6fb6b8-2e1a-4eba-80b1-79d191906936" />
      <xplan:position>
        <gml:Point gml:id="Gml_E78A8832-4915-4650-8F36-9C3961D9B91B" srsName="EPSG:25832">
          <gml:pos>567217.826 5932764.134</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_8ef2b2e5-3f88-4fb5-aee9-2741d7305301">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567217.826 5932769.928</gml:lowerCorner>
          <gml:upperCorner>567217.826 5932769.928</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d6fb6b8-2e1a-4eba-80b1-79d191906936" />
      <xplan:position>
        <gml:Point gml:id="Gml_C83D6328-A3E7-4E39-9E8E-57E99A0A3141" srsName="EPSG:25832">
          <gml:pos>567217.826 5932769.928</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_9d384060-a110-44cd-b0eb-8daca2c88662">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932849.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_23c8e2cb-eea0-4ede-8380-e4bd444a2c27" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_35cf4346-ed26-4a9f-b266-1a4ed64089a9" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_196124BF-7577-4C60-A302-0710FCC0AD5A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566919.2 5932841.614 566865.323 5932849.831 566858.112 5932802.554 
566873.929 5932800.141 566878.799 5932832.072 566904.004 5932828.228 
566901.249 5932810.137 566914.1 5932808.177 566919.2 5932841.614 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_23c8e2cb-eea0-4ede-8380-e4bd444a2c27">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.626 5932821.486</gml:lowerCorner>
          <gml:upperCorner>566869.626 5932821.486</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d384060-a110-44cd-b0eb-8daca2c88662" />
      <xplan:position>
        <gml:Point gml:id="Gml_A51F0492-F916-48A9-AF4B-7A93E576D776" srsName="EPSG:25832">
          <gml:pos>566869.626 5932821.486</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_35cf4346-ed26-4a9f-b266-1a4ed64089a9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566869.626 5932827.281</gml:lowerCorner>
          <gml:upperCorner>566869.626 5932827.281</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9d384060-a110-44cd-b0eb-8daca2c88662" />
      <xplan:position>
        <gml:Point gml:id="Gml_4D1BC1CF-80C7-4CA5-B472-2D3D5DFD9DCF" srsName="EPSG:25832">
          <gml:pos>566869.626 5932827.281</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a2559c2a-2d26-43b6-b1cf-f711e167627c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.746 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.517 5932726.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_BC5FE412-750D-4C04-BCB8-619C4B1E1152" srsName="EPSG:25832">
          <gml:posList>567427.517 5932719.637 567403.367 5932726.103 567399.746 5932712.579 
567423.896 5932706.113 567427.517 5932719.637 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_3dc269a7-9889-48ae-969a-f6653030938c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567377.878 5932674.7</gml:lowerCorner>
          <gml:upperCorner>567405.389 5932693.724</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_cb7de8de-5680-4187-9c99-4f195bfeb807" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7cb373be-48bc-4644-87be-eaf3969051a0" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9DDD0E81-889F-4291-9DF2-C7DBEB24B11E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567405.389 5932687.258 567393.798 5932690.361 567381.24 5932693.724 
567377.878 5932681.166 567402.027 5932674.7 567405.389 5932687.258 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_cb7de8de-5680-4187-9c99-4f195bfeb807">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567391.634 5932680.998</gml:lowerCorner>
          <gml:upperCorner>567391.634 5932680.998</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3dc269a7-9889-48ae-969a-f6653030938c" />
      <xplan:position>
        <gml:Point gml:id="Gml_32FD1074-276E-4835-9618-1919311A3779" srsName="EPSG:25832">
          <gml:pos>567391.634 5932680.998</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7cb373be-48bc-4644-87be-eaf3969051a0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567391.634 5932686.339</gml:lowerCorner>
          <gml:upperCorner>567391.634 5932686.339</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_3dc269a7-9889-48ae-969a-f6653030938c" />
      <xplan:position>
        <gml:Point gml:id="Gml_D3DC810F-F0E7-4CD6-A1EC-66978B7749CA" srsName="EPSG:25832">
          <gml:pos>567391.634 5932686.339</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_48e8084f-68bf-4a23-aac1-4df9d7eeff57">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567059.035 5932776.739</gml:lowerCorner>
          <gml:upperCorner>567075.96 5932797.983</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_75506E0E-F1B5-4DF1-9122-9939694F3871" srsName="EPSG:25832">
          <gml:posList>567075.96 5932794.973 567063.313 5932797.983 567059.035 5932780.007 
567070.652 5932776.99 567071.62 5932776.739 567075.96 5932794.973 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_c3036682-fcff-4418-b943-bcee53271588">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566935.783 5932786.645</gml:lowerCorner>
          <gml:upperCorner>566963.761 5932804.293</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_5e016c20-61a9-45a7-af55-41b9815bf89c" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_405CA2D2-A4AC-4E66-B8AB-8F5778FF4561" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">566963.761 5932799.414 566950.991 5932801.853 566938.222 5932804.293 
566935.783 5932791.524 566961.321 5932786.645 566963.761 5932799.414 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5e016c20-61a9-45a7-af55-41b9815bf89c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566949.772 5932795.469</gml:lowerCorner>
          <gml:upperCorner>566949.772 5932795.469</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c3036682-fcff-4418-b943-bcee53271588" />
      <xplan:position>
        <gml:Point gml:id="Gml_06AE9E7B-0C5B-4DD5-9064-284D8796D514" srsName="EPSG:25832">
          <gml:pos>566949.772 5932795.469</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_5083db57-c8e8-457e-8dc7-60a7ccb34a0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567297.868 5932696.257</gml:lowerCorner>
          <gml:upperCorner>567325.501 5932737.3</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_b50cd749-911b-4edc-a389-6e54140a69c2" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_83E5E7A2-5F6A-4657-B59A-6C7F509D651B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567325.501 5932708.738 567313.643 5932712.203 567319.349 5932734.009 
567306.772 5932737.3 567297.868 5932703.269 567321.864 5932696.257 
567325.501 5932708.738 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_b50cd749-911b-4edc-a389-6e54140a69c2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567311.6 5932716.843</gml:lowerCorner>
          <gml:upperCorner>567311.6 5932716.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_5083db57-c8e8-457e-8dc7-60a7ccb34a0d" />
      <xplan:position>
        <gml:Point gml:id="Gml_24411999-7CE3-42A7-91AF-486A85F2403A" srsName="EPSG:25832">
          <gml:pos>567311.6 5932716.843</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SpezielleBauweise gml:id="GML_fa60d8a2-ba3c-4c8c-ab75-f0d83a71c00a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567141.149 5932741.64</gml:lowerCorner>
          <gml:upperCorner>567155.075 5932748.872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>2500</xplan:hoehenbezug>
          <xplan:bezugspunkt>3500</xplan:bezugspunkt>
          <xplan:hMin uom="m">4.5</xplan:hMin>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1A613D1F-E55F-4674-BA29-1C226997D5AF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567154.527 5932745.398 567141.149 5932748.872 567141.581 5932745.143 
567155.075 5932741.64 567154.527 5932745.398 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1300</xplan:typ>
    </xplan:BP_SpezielleBauweise>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_784dbaff-c2f6-4519-a40b-7afdf39d1b8f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.503 5932773.896</gml:lowerCorner>
          <gml:upperCorner>567079.203 5932821.212</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8CE3827D-BEE6-4BAA-9DBD-A2E8E50901BF" srsName="EPSG:25832">
          <gml:posList>567079.203 5932808.602 567026.182 5932821.212 567022.941 5932807.591 
567015.503 5932776.34 567028.284 5932773.896 567035.587 5932804.581 
567063.313 5932797.983 567075.983 5932794.967 567079.203 5932808.602 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_23ed66cd-0484-497a-a0ae-aaafa4a7febc">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567092.822 5932751.558</gml:lowerCorner>
          <gml:upperCorner>567120.277 5932770.455</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_a82100c3-cc2a-47ed-82b6-a1ad280cbdc3" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_32c05c56-5bfe-4302-b070-239b322ba5a4" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BD06F021-8233-4157-835C-72C02973E210" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567120.277 5932764.106 567108.409 5932767.187 567095.823 5932770.455 
567092.822 5932757.841 567117.019 5932751.558 567120.277 5932764.106 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_a82100c3-cc2a-47ed-82b6-a1ad280cbdc3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567106.486 5932763.117</gml:lowerCorner>
          <gml:upperCorner>567106.486 5932763.117</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_23ed66cd-0484-497a-a0ae-aaafa4a7febc" />
      <xplan:position>
        <gml:Point gml:id="Gml_EA00C82C-4F86-4149-80AB-A78F4B41FBE9" srsName="EPSG:25832">
          <gml:pos>567106.486 5932763.117</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_32c05c56-5bfe-4302-b070-239b322ba5a4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567106.486 5932757.776</gml:lowerCorner>
          <gml:upperCorner>567106.486 5932757.776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_23ed66cd-0484-497a-a0ae-aaafa4a7febc" />
      <xplan:position>
        <gml:Point gml:id="Gml_841AC452-5D58-4117-9C70-D6591D3E906C" srsName="EPSG:25832">
          <gml:pos>567106.486 5932757.776</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_2f828891-0f23-4cb8-a4d5-9c131b2e3089">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567337.79 5932688.537</gml:lowerCorner>
          <gml:upperCorner>567403.367 5932740.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_D787B4AF-076C-47D6-B285-89CD408C92C3" srsName="EPSG:25832">
          <gml:posList>567350.721 5932740.198 567347.101 5932726.674 567337.79 5932691.899 
567350.348 5932688.537 567359.658 5932723.312 567387.189 5932715.941 
567399.746 5932712.579 567403.367 5932726.103 567350.721 5932740.198 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_442af384-8831-4bd1-ae12-3e01802b513a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567195.374 5932730.73</gml:lowerCorner>
          <gml:upperCorner>567229.869 5932764.51</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_387B5F34-45E8-4BD2-824C-5530542C9ADB" srsName="EPSG:25832">
          <gml:posList>567229.869 5932757.348 567202.283 5932764.51 567195.374 5932737.898 
567222.958 5932730.73 567224.591 5932737.022 567229.869 5932757.348 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_f9238a58-ea13-46bf-8414-f7baa3badd69">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567221.324 5932718.156</gml:lowerCorner>
          <gml:upperCorner>567248.789 5932737.022</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_50EDA47C-E9B3-4F16-AF35-3BDF58B2E752" srsName="EPSG:25832">
          <gml:posList>567237.174 5932733.755 567224.591 5932737.022 567222.958 5932730.73 
567221.324 5932724.439 567245.522 5932718.156 567248.789 5932730.739 
567237.174 5932733.755 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerkehrsflaecheBesondererZweckbestimmung gml:id="GML_36c586d6-47b1-447d-b11d-cdd263f9c472">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566841.322 5932638.488</gml:lowerCorner>
          <gml:upperCorner>567478.319 5932805.784</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A6A9E7E4-7D6B-43F7-B972-BB9CDF8AA2A7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567038.535 5932771.936 567004.845 5932778.378 566935.752 5932791.578 
566925.489 5932793.308 566923.976 5932793.564 566899.326 5932797.323 
566858.262 5932803.586 566842.603 5932805.784 566841.516 5932797.098 
566841.322 5932795.563 566856.166 5932793.67 566876.457 5932790.784 
566893.555 5932788.149 566925.816 5932782.812 566925.622 5932781.675 
566957.155 5932775.947 567001.253 5932766.989 567024.668 5932762.288 
567034.553 5932760.037 567099.41 5932743.34 567165.898 5932726.231 
567207.598 5932715.718 567251.407 5932703.902 567309.648 5932687.426 
567309.188 5932685.742 567324.249 5932681.582 567379.905 5932665.733 
567462.605 5932641.979 567474.886 5932638.488 567476.043 5932642.107 
567477.061 5932645.289 567475.978 5932645.649 567478.319 5932654.273 
567414.585 5932671.338 567325.715 5932695.132 567258.017 5932714.912 
567181.156 5932734.868 567193.176 5932781.173 567149.128 5932791.971 
567154.527 5932745.398 567117.89 5932754.911 567117.019 5932751.558 
567038.535 5932771.936 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_VerkehrsflaecheBesondererZweckbestimmung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_fbcbb2d0-0fb6-4699-8f79-e1fb8d3be5e9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567258.017 5932654.273</gml:lowerCorner>
          <gml:upperCorner>567491.419 5932761.347</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_c6d432bc-5dcc-481d-b7c1-a806cd3d767f" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_44ee80ca-f0f0-4aee-acde-ef863d6fedc6" />
      <xplan:refTextInhalt xlink:href="#GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2" />
      <xplan:refTextInhalt xlink:href="#GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a" />
      <xplan:refTextInhalt xlink:href="#GML_4f96ce33-c275-4311-9ea1-737d64b48b78" />
      <xplan:refTextInhalt xlink:href="#GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf" />
      <xplan:refTextInhalt xlink:href="#GML_0958e0ae-8310-4eb4-9586-7df30e9c78de" />
      <xplan:refTextInhalt xlink:href="#GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1" />
      <xplan:refTextInhalt xlink:href="#GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af" />
      <xplan:refTextInhalt xlink:href="#GML_d03d15de-716d-4215-b4bb-f6866dc31699" />
      <xplan:refTextInhalt xlink:href="#GML_4231d2e4-1587-42db-9621-050e3eba8e62" />
      <xplan:refTextInhalt xlink:href="#GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9" />
      <xplan:refTextInhalt xlink:href="#GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d" />
      <xplan:refTextInhalt xlink:href="#GML_c2d35123-8af2-4cf8-8325-13c37fbd6830" />
      <xplan:refTextInhalt xlink:href="#GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e" />
      <xplan:refTextInhalt xlink:href="#GML_40708916-979d-4b80-a8bf-274dac77799c" />
      <xplan:refTextInhalt xlink:href="#GML_d854f146-551d-4d47-a76e-3125c7e50abd" />
      <xplan:refTextInhalt xlink:href="#GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:refTextInhalt xlink:href="#GML_a5d62934-0c67-42df-ab71-478ecb59f350" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2A2343B0-8A9E-4E99-8A0F-7A516B324FE1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567491.419 5932702.527 567427.516 5932719.637 567338.647 5932743.431 
567270.174 5932761.347 567258.017 5932714.912 567325.715 5932695.132 
567414.585 5932671.338 567478.319 5932654.273 567489.76 5932696.417 
567491.419 5932702.527 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.7</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1500</xplan:besondereArtDerBaulNutzung>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c6d432bc-5dcc-481d-b7c1-a806cd3d767f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567374.551 5932707.847</gml:lowerCorner>
          <gml:upperCorner>567374.551 5932707.847</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fbcbb2d0-0fb6-4699-8f79-e1fb8d3be5e9" />
      <xplan:position>
        <gml:Point gml:id="Gml_0D8C0FBA-62B5-439E-A816-17182E050CBB" srsName="EPSG:25832">
          <gml:pos>567374.551 5932707.847</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_44ee80ca-f0f0-4aee-acde-ef863d6fedc6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567368.388 5932699.843</gml:lowerCorner>
          <gml:upperCorner>567368.388 5932699.843</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_fbcbb2d0-0fb6-4699-8f79-e1fb8d3be5e9" />
      <xplan:position>
        <gml:Point gml:id="Gml_624E2722-0AE4-487E-B91D-0E9DF2E9BA36" srsName="EPSG:25832">
          <gml:pos>567368.388 5932699.843</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Wegerecht gml:id="GML_0066135d-0b09-423e-a7bc-12ea23ff7196">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567313.643 5932691.899</gml:lowerCorner>
          <gml:upperCorner>567350.721 5932747.553</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#GML_15fef662-24ed-4ee4-864b-95c4720f68c8" />
      <xplan:refTextInhalt xlink:href="#GML_592b628c-3832-49f2-ac44-4ce50d35556f" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_85A6B7F3-9ADF-46DB-B473-7CE9D86ED85F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567350.721 5932740.198 567338.647 5932743.431 567322.893 5932747.553 
567319.349 5932734.009 567313.643 5932712.203 567325.501 5932708.738 
567321.864 5932696.257 567325.715 5932695.132 567337.79 5932691.899 
567350.721 5932740.198 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_Wegerecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_69086e3c-5a9e-4202-83b3-f88167bbd387">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567015.503 5932773.896</gml:lowerCorner>
          <gml:upperCorner>567035.587 5932807.591</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(F)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_387b0ade-f5d2-4c53-80cc-6ad7bcf40811" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_70A82977-D575-4C7E-8099-04E2762A4355" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567035.587 5932804.581 567022.941 5932807.591 567015.503 5932776.34 
567028.284 5932773.896 567035.587 5932804.581 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_387b0ade-f5d2-4c53-80cc-6ad7bcf40811">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567025.58 5932790.605</gml:lowerCorner>
          <gml:upperCorner>567025.58 5932790.605</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_69086e3c-5a9e-4202-83b3-f88167bbd387" />
      <xplan:position>
        <gml:Point gml:id="Gml_8A651F25-989C-416E-9406-C23E9F5D4BBB" srsName="EPSG:25832">
          <gml:pos>567025.58 5932790.605</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_a69fedcd-054a-4c17-ba94-b4c5cde51146">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566899.289 5932793.365</gml:lowerCorner>
          <gml:upperCorner>566926.952 5932810.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_373533D1-CA67-48E5-BC19-7FDE66E8398D" srsName="EPSG:25832">
          <gml:posList>566926.952 5932806.22 566914.1 5932808.177 566901.249 5932810.137 
566900.314 5932804.008 566899.289 5932797.285 566924.003 5932793.516 
566924.991 5932793.365 566926.952 5932806.22 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_01b3c2d3-922c-4b45-a801-2b9ac29ab4b5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566858.112 5932800.141</gml:lowerCorner>
          <gml:upperCorner>566919.2 5932849.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B8A95F46-D163-4D9C-B8A3-C5298ED78B54" srsName="EPSG:25832">
          <gml:posList>566914.1 5932808.177 566919.2 5932841.614 566865.323 5932849.831 
566858.112 5932802.554 566873.929 5932800.141 566878.799 5932832.072 
566904.004 5932828.228 566901.249 5932810.137 566914.1 5932808.177 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_77e5eecd-f6ca-4fb0-98cc-f1d1212b06a6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567221.324 5932718.156</gml:lowerCorner>
          <gml:upperCorner>567248.789 5932737.022</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(C)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">24.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_bacae243-8786-4292-94fc-651f36ff6494" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_1afbafa0-d4f6-4227-8797-4f2c7bfd93a2" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_bbba1f15-b66e-4b52-89ba-9527927eaa78" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_43D017D9-E061-421B-9B5C-BB89C4965740" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567248.789 5932730.739 567237.174 5932733.755 567224.591 5932737.022 
567222.958 5932730.73 567221.324 5932724.439 567245.522 5932718.156 
567248.789 5932730.739 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_bacae243-8786-4292-94fc-651f36ff6494">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567235.057 5932724.375</gml:lowerCorner>
          <gml:upperCorner>567235.057 5932724.375</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_77e5eecd-f6ca-4fb0-98cc-f1d1212b06a6" />
      <xplan:position>
        <gml:Point gml:id="Gml_B089AD97-7CFF-4119-A0E7-DEEEF287E7DD" srsName="EPSG:25832">
          <gml:pos>567235.057 5932724.375</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_1afbafa0-d4f6-4227-8797-4f2c7bfd93a2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567235.057 5932729.716</gml:lowerCorner>
          <gml:upperCorner>567235.057 5932729.716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_77e5eecd-f6ca-4fb0-98cc-f1d1212b06a6" />
      <xplan:position>
        <gml:Point gml:id="Gml_52102B14-A03A-4956-9908-FEA1EA3FEA56" srsName="EPSG:25832">
          <gml:pos>567235.057 5932729.716</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_6169224a-3c33-477c-97d8-07b9ca2d0a5a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567095.823 5932767.187</gml:lowerCorner>
          <gml:upperCorner>567112.927 5932789.338</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_2E203F55-7755-4559-9022-A1F42E8B3C8E" srsName="EPSG:25832">
          <gml:posList>567112.927 5932786.175 567100.317 5932789.338 567100.281 5932789.185 
567095.823 5932770.455 567108.409 5932767.187 567112.927 5932786.175 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_4354adf9-4004-4283-98f9-aca57842955d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.746 5932706.113</gml:lowerCorner>
          <gml:upperCorner>567427.517 5932726.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_33e3de60-aecb-4593-aa85-bebac94e0bcb" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7f19c2df-be1d-42b6-bae1-a6907a19a10b" />
      <xplan:refTextInhalt xlink:href="#GML_029bc724-b8f1-45e8-b237-8018fbc4680c" />
      <xplan:refTextInhalt xlink:href="#GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed" />
      <xplan:refTextInhalt xlink:href="#GML_d34ab59d-e034-4e3b-a99f-19a491a20963" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B897F832-A52C-4A5F-AA1E-AD63411A81DC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567427.517 5932719.637 567403.367 5932726.103 567399.746 5932712.579 
567423.896 5932706.113 567427.517 5932719.637 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_33e3de60-aecb-4593-aa85-bebac94e0bcb">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567413.631 5932718.402</gml:lowerCorner>
          <gml:upperCorner>567413.631 5932718.402</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_4354adf9-4004-4283-98f9-aca57842955d" />
      <xplan:position>
        <gml:Point gml:id="Gml_45CF08D7-C1B9-471F-B416-E29C16777F07" srsName="EPSG:25832">
          <gml:pos>567413.631 5932718.402</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_7f19c2df-be1d-42b6-bae1-a6907a19a10b">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567413.631 5932712.608</gml:lowerCorner>
          <gml:upperCorner>567413.631 5932712.608</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_4354adf9-4004-4283-98f9-aca57842955d" />
      <xplan:position>
        <gml:Point gml:id="Gml_3AC7EC35-1DB7-43AD-8E18-A64EE42C7628" srsName="EPSG:25832">
          <gml:pos>567413.631 5932712.608</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_ca345e0b-f841-411d-8983-4a72343e6cd1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566849.469 5932728.094</gml:lowerCorner>
          <gml:upperCorner>567498.31 5932859.222</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_DD113A6A-B045-402D-9DB3-095A63453296" srsName="EPSG:25832">
          <gml:posList>566849.469 5932859.222 566896.825 5932852.378 566894.962 5932839.469 
566913.019 5932836.466 566918.283 5932835.601 566931.081 5932833.497 
566932.117 5932839.756 566918.207 5932842.058 566904.297 5932844.361 
566905.266 5932851.083 566916.768 5932849.21 566919.425 5932849.281 
566968.153 5932840.52 566980.426 5932838.241 566987.76 5932836.783 
567057.602 5932824.015 567127.727 5932812.902 567173.834 5932806.143 
567206.34 5932801.096 567210.643 5932800.367 567232.728 5932796.1 
567254.637 5932791.004 567270.059 5932786.989 567293.738 5932780.36 
567328.582 5932770.627 567361.266 5932761.734 567386.531 5932755.262 
567391.412 5932754.053 567403.33 5932751.144 567403.33 5932751.144 
567437.692 5932742.804 567473.816 5932734.037 567498.31 5932728.094 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_303e9eb8-189e-485e-b658-913d8dd568f6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566943.2511 5932743.169</gml:lowerCorner>
          <gml:upperCorner>567509.287 5932905.509</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1DA7DF36-894C-41D2-B39D-5F286EDD6064" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567509.287 5932769.105 567416.044 5932790.244 567354.506 5932803.777 
567322.897 5932810.727 567243.981 5932831.427 567071.225 5932875.978 
566952.478 5932905.509 566943.2511 5932889.4633 567047.196 5932861.151 
567122.404 5932841.663 567163.038 5932830.198 567363.553 5932775.056 
567390.254 5932768.247 567485.287 5932746.96 567502.35 5932743.169 
567509.287 5932769.105 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_1957130e-eb44-4fe1-9900-057e7b1656b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567474.886 5932795.563</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_54874758-FF57-4539-9998-A0F0B3C5BF38" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.886 5932638.488 567462.605 5932641.979 567379.905 5932665.733 
567324.249 5932681.582 567309.188 5932685.742 567309.648 5932687.426 
567251.407 5932703.902 567207.598 5932715.718 567165.898 5932726.231 
567099.41 5932743.34 567034.553 5932760.037 567024.668 5932762.288 
567001.253 5932766.989 566957.155 5932775.947 566925.622 5932781.675 
566925.816 5932782.812 566893.555 5932788.149 566876.457 5932790.784 
566856.166 5932793.67 566841.322 5932795.563 566839.305 5932779.633 
566885.545 5932770.145 566933.255 5932760.356 567084.333 5932725.96 
567189.373 5932700.56 567374.73 5932651.63 567470.445 5932624.6 
567474.886 5932638.488 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:istNatuerlichesUberschwemmungsgebiet>false</xplan:istNatuerlichesUberschwemmungsgebiet>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Wasserrecht gml:id="GML_2ca68aee-6fe5-4f9b-adb8-50541a1ebad6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566839.305 5932624.6</gml:lowerCorner>
          <gml:upperCorner>567478.319 5932805.784</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Hochwassergefährdeter Bereich</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:refTextInhalt xlink:href="#GML_2d7be849-3d58-4bec-a554-3d752d9b2ab9" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_5925A1F6-5F0D-46D6-8199-6BF9B14230AD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567038.607 5932771.88 567004.903 5932778.319 566935.784 5932791.524 
566924.003 5932793.516 566858.263 5932803.542 566842.603 5932805.784 
566841.516 5932797.098 566841.353 5932795.811 566841.322 5932795.563 
566839.305 5932779.633 566885.545 5932770.145 566933.255 5932760.356 
567084.333 5932725.96 567189.373 5932700.56 567374.73 5932651.63 
567470.445 5932624.6 567474.886 5932638.488 567474.962 5932638.726 
567477.061 5932645.289 567475.978 5932645.649 567478.319 5932654.273 
567325.715 5932695.132 567291.866 5932705.022 567258.017 5932714.912 
567181.156 5932734.868 567182.755 5932741.028 567172.637 5932743.881 
567171.049 5932756.863 567159.379 5932755.521 567161.242 5932742.524 
567155.075 5932741.64 567154.642 5932745.368 567117.89 5932754.911 
567117.01 5932751.523 567038.607 5932771.88 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>20002</xplan:artDerFestlegung>
      <xplan:istNatuerlichesUberschwemmungsgebiet>false</xplan:istNatuerlichesUberschwemmungsgebiet>
    </xplan:SO_Wasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Schienenverkehrsrecht gml:id="GML_c0a8d0b0-6902-46d0-b3d5-c220e29edc97">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>566930.194 5932737.377</gml:lowerCorner>
          <gml:upperCorner>567500.798 5932866.541</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_2856259d-c0e0-4b96-9b6e-aa0c6a584156" />
      <xplan:position>
        <gml:LineString gml:id="Gml_664E52AC-16DA-459D-A0CF-2CE252F8F72A" srsName="EPSG:25832">
          <gml:posList>567500.798 5932737.377 567446.666 5932750.517 567434.247 5932753.531 
567393.238 5932763.716 567365.696 5932770.815 567297.461 5932789.308 
567270.403 5932796.898 567224.067 5932809.859 567224.066 5932809.858 
567177.219 5932822.041 567141.654 5932830.401 567119.92 5932835.137 
567056.168 5932847.424 567014.704 5932854.187 566973.112 5932860.188 
566930.194 5932866.541 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
    </xplan:SO_Schienenverkehrsrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_RasterplanBasis gml:id="Gml_5CF4FC84-A8A1-4113-AF42-63641E92805E">
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan001_4-1.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan001_4-1.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
    </xplan:XP_RasterplanBasis>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_c795e4d7-d3e7-4b85-a8d6-b6cc1e6feba2">
      <xplan:schluessel>§2 Nr.27</xplan:schluessel>
      <xplan:text>In den Mischgebieten ist der Erschütterungsschutz der
Gebäude durch bauliche oder technische Maßnahmen
(zum Beispiel an Wänden, Decken und Fundamenten)
so sicherzustellen, dass die Anhaltswerte der DIN 4150
(Erschütterungen im Bauwesen), Teil 2 (Einwirkungen auf
Menschen in Gebäuden), Tabelle 1, Zeile 3 (Mischgebiete
nach BauNVO) eingehalten werden. Zusätzlich ist durch
die baulichen und technischen Maßnahmen zu gewährleisten,
dass der sekundäre Luftschall die Immissionsrichtwerte
der Technischen Anleitung zum Schutz gegen Lärm
vom 26. August 1998 (Gemeinsames Ministerialblatt
S. 503), Nummer 6.2, nicht überschreitet. Einsichtnahmestelle
der DIN 4150: Freie und Hansestadt Hamburg,
Behörde für Stadtentwicklung und Umwelt, Amt für
Immissionsschutz und Betriebe, Bezugsquelle der DIN
4150: Beuth Verlag GmbH, Berlin.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_6450304f-6c22-4cc9-8a19-3e98e46eed8a">
      <xplan:schluessel>§2 Nr.25</xplan:schluessel>
      <xplan:text>In den Mischgebieten sind Dächer als Flachdächer oder
flachgeneigte Dächer mit einer Neigung bis zu 10 Grad
auszuführen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_382e6441-314e-443a-a5a2-290c68c53794">
      <xplan:schluessel>§2 Nr.13.3</xplan:schluessel>
      <xplan:text>Vom Anschluss- und Benutzungsgebot nach Nummer 13.1
kann auf Antrag befreit werden, soweit die Erfüllung der
Anforderungen im Einzelfall wegen besonderer Umstände
zu einer unbilligen Härte führen würde. Die Befreiung soll
zeitlich befristet werden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_4f96ce33-c275-4311-9ea1-737d64b48b78">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Die Aufenthaltsräume für gewerbliche Nutzungen – hier insbesondere
die Pausen- und Ruheräume – sind durch geeignete
Grundrissgestaltung den Verkehrslärm abgewandten
Gebäudeseiten zuzuordnen. Soweit die Anordnung an den
vom Verkehrslärm abgewandten Gebäudeseiten nicht möglich
ist, muss für diese Räume ein ausreichender Schallschutz
an Außentüren, Fenstern, Außenwänden und Dächern der
Gebäude durch bauliche Maßnahmen geschaffen werden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_0a31d433-388a-43b7-8815-b0c3b0eea5cf">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>Das auf den Mischgebietsflächen und den Straßenverkehrsflächen
südlich der Versmannstraße anfallende
Nieder schlagswasser ist direkt in das nächst liegende
Gewässer (Baakenhafen) einzuleiten.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_6a75e8bb-f2d6-4867-8a26-f6c5df60cae1">
      <xplan:schluessel>§2 Nr.13.2</xplan:schluessel>
      <xplan:text>Vom Anschluss- und Benutzungsgebot nach Nummer 13.1
kann ausnahmsweise abgesehen werden, wenn der berechnete
Heizwärmebedarf der Gebäude nach der Energieeinsparverordnung
vom 24. Juli 2007 (BGBl. I S. 1519),
zuletzt geändert am 18. November 2013 (BGBl. I S. 3951),
den Wert von 15 kWh/m² Nutzfläche nicht übersteigt.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_d7e44f97-b0f8-43cb-9ed6-b76d768f9071">
      <xplan:schluessel>§2 Nr.13.1</xplan:schluessel>
      <xplan:text>Neu zu errichtende Gebäude sind an ein Wärmenetz anzuschließen,
das überwiegend mit erneuerbaren Energien
versorgt wird.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_e6ef7a8a-cdde-4f34-a748-a84cb7831fd4">
      <xplan:schluessel>§2 Nr.18</xplan:schluessel>
      <xplan:text>Auf den gekennzeichneten Flächen nördlich des Baakenhafens
(im Bereich Versmannkai und Versmannstraße),
deren Böden erheblich mit umweltgefährdenden Stoffen
belastet sind, sind bauliche Gassicherungsmaßnahmen
vorzusehen, die sowohl Gasansammlungen unter den
baulichen Anlagen und den befestigten Flächen als auch
Gaseintritte in die baulichen Anlagen verhindern.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_0958e0ae-8310-4eb4-9586-7df30e9c78de">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Tiefgaragen sind außerhalb der überbaubaren Grundstücksflächen
zulässig. Stellplätze sind nur in Tiefgaragen
oder Garagengeschossen unterhalb der Höhe von 8,7 m
über Normalnull (NN) zulässig. Geringfügige Abweichungen
sind zulässig, wenn sie durch abweichende Straßenanschlusshöhen
von über 8,7 m über NN begründet
sind.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_b7112969-6eac-47c8-9443-cd9a0ac271ba">
      <xplan:schluessel>§2 Nr.26</xplan:schluessel>
      <xplan:text>Für festgesetzte Anpflanzungen sind standortgerechte
Laubbäume oder belaubte Heckenpflanzen zu verwenden.
Großkronige Bäume müssen einen Stammumfang von
mindestens 18 cm, kleinkronige Bäume von mindestens
14 cm, in 1 m Höhe über dem Erdboden gemessen, aufweisen;
Heckenpflanzen eine Mindesthöhe von 80 cm.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_029bc724-b8f1-45e8-b237-8018fbc4680c">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Werbeanlagen größer als 2 m² und Werbeanlagen oberhalb
der Gebäudetraufen sind unzulässig. Die Gestaltung der
Gesamtbaukörper und der privaten Freiflächen darf nicht
durch Werbeanlagen beeinträchtigt werden. Werbeanlagen
sind nur an der Stätte der Leistung zulässig. Oberhalb der
Brüstung des zweiten Vollgeschosses sind Werbeanlagen
nur ausnahmsweise zulässig, wenn zudem das Ortsbild
nicht beeinträchtigt wird.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_7b2324e6-459c-4c35-81e7-b7cd0b8986ed">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Auf den mit „(A)“ bezeichneten Flächen der Mischgebiete
sind Wohnungen in den Erdgeschossen unzulässig. Auf den
mit „(F)“ bezeichneten Flächen der Mischgebiete sind
Wohnungen in den Erdgeschossen ausnahmsweise zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_7768f4e1-2504-4f08-9c4a-1b9e3ab5ed6e">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Die Gebäudefassaden können in unterschiedlichen Materialien
ausschließlich in den Farben Weiß, Beige, Gelb und
Blaubunt ausgeführt werden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_bbba1f15-b66e-4b52-89ba-9527927eaa78">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Außer auf den mit „(C)“ bezeichneten Flächen muss die
Oberkante des Fußbodens des ersten Obergeschosses mindestens
5,5 m und höchstens 6,5 m über der angrenzenden
Geländeoberfläche liegen. Ausnahmsweise kann im Erdgeschoss
eine Galerie eingebaut werden, wenn das Galeriegeschoss
eine Grundfläche kleiner 50 vom Hundert (v.H.)
der Grundfläche des Erdgeschosses einnimmt. Die Galerieebene
muss einen Abstand von mindestens 4,5 m von der
Innenseite der zu den öffentlichen Straßenverkehrsflächen
und mit Gehrechten belegten Flächen gerichteten Außenfassade
einhalten. Das Erdgeschoss samt einem eventuell
eingezogenen Galeriegeschoss wird als ein Vollgeschoss
gewertet.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_b2d9cad2-18da-4d71-9e2b-e4e3dc0de2d1">
      <xplan:schluessel>§2 Nr.20</xplan:schluessel>
      <xplan:text>In den Baugebieten sind für Einfriedigungen nur Hecken
oder durchbrochene Zäune in Verbindung mit Hecken bis
zu einer Höhe von 1,2 m zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_2c4e8d5b-d2f3-4adb-b554-efe208d803af">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Oberhalb der festgesetzten Vollgeschosse (einschließlich
einem möglichen Galeriegeschoss im Erdgeschoss) sind
weitere Geschosse unzulässig. Technikgeschosse und technische
oder erforderliche Aufbauten, wie Treppenräume, sind ausnahmsweise, auch über der festgesetzten Gebäudehöhe,
zulässig, wenn die Gestaltung des Gesamtbaukörpers
und das Ortsbild nicht beeinträchtigt werden und
diese keine wesentliche Verschattung der Nachbargebäude
und der Umgebung bewirken. Aufbauten, deren Einhausung
und Technikgeschosse sind mindestens 2,5 m von
der Außenfassade zurückzusetzen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_d03d15de-716d-4215-b4bb-f6866dc31699">
      <xplan:schluessel>§2 Nr.2.2</xplan:schluessel>
      <xplan:text>Vergnügungsstätten in den Teilen des Mischgebiets, die
überwiegend durch gewerbliche Nutzungen geprägt sind
sowie Tankstellen sind unzulässig. Ausnahmen für Vergnügungsstätten
in den übrigen Teilen des Mischgebiets werden
ausgeschlossen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_fd656471-9a50-4e8b-8da2-af82666cde09">
      <xplan:schluessel>§2 Nr.23</xplan:schluessel>
      <xplan:text>Tiefgaragen und die Dachflächen der festgesetzten eingeschossigen
Gebäude auf den mit „(E)“ bezeichneten
Flächen sind in den zu begrünenden Bereichen mit einem
mindestens 50 cm starken durchwurzelbaren Substrataufbau
zu versehen. Für Baumpflanzungen muss auf einer
Fläche von 16 m² je Baum die Stärke des durchwurzelbaren
Substrataufbaus mindestens 80 cm betragen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_4231d2e4-1587-42db-9621-050e3eba8e62">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>In den Mischgebieten an der Versmannstraße sind die
Schlafräume zur lärmabgewandten Gebäudeseite zu orientieren.
Wohn- / Schlafräume in Einzimmerwohnungen und
Kinderzimmer sind wie Schlafräume zu beurteilen. Wird an
den Gebäudeseiten ein Pegel von 70 dB(A) am Tag (6.00 Uhr
bis 22.00 Uhr) überschritten, sind vor den Fenstern der
zu dieser Gebäudeseite orientierten Wohnräume bauliche
Schallschutzmaßnahmen in Form von verglasten Vorbauten
(zum Beispiel verglaste Loggien, Wintergärten) oder vergleichbare
Maßnahmen vorzusehen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_d34ab59d-e034-4e3b-a99f-19a491a20963">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Eine Überschreitung der Baugrenzen durch Balkone,
Erker, Loggien und Sichtschutzwände kann zu den öffentlichen
Straßenräumen oder den mit Gehrechten belasteten
Flächen ausnahmsweise bis zu einer Tiefe von 1,5 m zugelassen
werden, wenn die Gestaltung des Gesamtbaukörpers
nicht beeinträchtigt wird und diese keine wesentliche Verschattung
der benachbarten Nutzungen und der Umgebung
bewirken. Dabei ist eine Überbauung der Straßenverkehrsfläche
nur oberhalb einer lichten Höhe von 4,5 m
zulässig</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_4436c60e-ce8d-4fbf-88fa-a464667ec0e9">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Für die Mischgebiete gilt:</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_9a1336c1-2d19-4f4a-8cdb-8ce7b44d7b5d">
      <xplan:schluessel>§2 Nr.21</xplan:schluessel>
      <xplan:text>Die nicht überbauten Grundstücksflächen der Mischgebiete,
mit Ausnahme der Flächen mit festgesetzten
Gehrechten, sowie die Dachflächen der festgesetzten eingeschossigen
Gebäude auf den mit „(E)“ bezeichneten
Flächen sind mit einem Anteil von mindestens 50 v. H.
zu begrünen. Je 300 m² ist mindestens ein großkroniger
Baum oder je 150 m² ein kleinkroniger Baum zu pflanzen
und dauerhaft zu erhalten. Bei Abgang ist eine gleichwertige
Ersatzpflanzung vorzunehmen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_c2d35123-8af2-4cf8-8325-13c37fbd6830">
      <xplan:schluessel>§2 Nr.24</xplan:schluessel>
      <xplan:text>Die übrigen Dachflächen in den Mischgebieten sind mit
Ausnahme der gemäß Nummer 9 zulässigen Anlagen und
technischen Aufbauten zu mindestens 30 v. H. mit einem
mindestens 15 cm starken durchwurzelbaren Substrataufbau
extensiv mit standortangepassten Stauden und Gräsern
zu begrünen. Darüber hinaus müssen mindestens
20 v. H. mit einem mindestens 50 cm starken Substrataufbau
intensiv mit Stauden und Sträuchern begrünt werden.
Die Dachbegrünung ist dauerhaft zu erhalten.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_c0ad16b1-6dcc-423e-ba6c-c8e31f8f867e">
      <xplan:schluessel>§2 Nr.19</xplan:schluessel>
      <xplan:text>Auf den nicht überbauten Grundstücksflächen sind
Nebenanlagen nur ausnahmsweise zulässig, wenn die
Gestaltung der Freiflächen nicht beeinträchtigt ist.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_40708916-979d-4b80-a8bf-274dac77799c">
      <xplan:schluessel>§2 Nr.2.3</xplan:schluessel>
      <xplan:text>Die festgesetzten Grundflächenzahlen können für Nutzungen
nach § 19 Absatz 4 Satz 1 der Baunutzungsverordnung
(BauNVO) in der Fassung vom 23. Januar 1990
(BGBl. I S. 133), zuletzt geändert am 11. Juni 2013 (BGBl. I
S. 1548, 1551), bis 1,0 überschritten werden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_15fef662-24ed-4ee4-864b-95c4720f68c8">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>Die festgesetzten Gehrechte umfassen die Befugnis der
Freien und Hansestadt Hamburg, allgemein zugängige
Gehwege anzulegen und zu unterhalten. Geringfügige
Abweichungen von den festgesetzten Gehrechten sind
zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_8ade82cf-d047-4e3c-a7b8-426429ae8f2a">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Für die Beheizung und Bereitstellung des Warmwassers
gilt:</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_d854f146-551d-4d47-a76e-3125c7e50abd">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Durch geeignete bauliche Schallschutzmaßnahmen wie zum
Beispiel Doppelfassaden, verglaste Vorbauten (zum Beispiel
verglaste Loggien, Wintergärten), besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
ist sicherzustellen, dass durch diese baulichen Maßnahmen
insgesamt eine Schallpegeldifferenz erreicht wird,
die es ermöglicht, dass in Schlafräumen ein Innenraumpegel
bei teilgeöffneten Fenstern von 30 dB(A) während der
Nachtzeit (22.00 Uhr bis 6.00 Uhr) nicht überschritten
wird. Erfolgt die bauliche Schallschutzmaßnahme in Form
von verglasten Vorbauten, muss dieser Innenraumpegel bei
teilgeöffneten Bauteilen erreicht werden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_afff76f6-49c8-449e-8b0a-fe6fdd7bfb1a">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Für einen Außenbereich einer Wohnung ist entweder durch
Orientierung an lärmabgewandten Gebäudeseiten oder
durch bauliche Schallschutzmaßnahmen wie zum Beispiel
verglaste Vorbauten (zum Beispiel verglaste Loggien, Wintergärten)
mit teilgeöffneten Bauteilen sicherzustellen, dass
durch diese baulichen Maßnahmen insgesamt eine Schallpegelminderung
erreicht wird, die es ermöglicht, dass in
dem der Wohnung zugehörigen Außenbereich ein Tagpegel
(6.00 Uhr bis 22.00 Uhr) von kleiner 65 dB(A) erreicht
wird.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_a5bbf517-2698-419f-a93a-1996ca8c450c">
      <xplan:schluessel>§2 Nr.17</xplan:schluessel>
      <xplan:text>Das festgesetzte Geh- und Fahrrecht umfasst die Befugnis
der für die Unterhaltung der Kaianlagen zuständigen
Stelle, diese Flächen zu begehen und zu befahren.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_592b628c-3832-49f2-ac44-4ce50d35556f">
      <xplan:schluessel>§2 Nr.22</xplan:schluessel>
      <xplan:text>Die mit festgesetzten Gehrechten belegten Flächen der
Mischgebiete sind mit einem Anteil von mindestens 20 v. H.
zu begrünen. Je 500 m² ist mindestens ein großkroniger
Baum oder je 250 m² ein kleinkroniger Baum zu pflanzen
und dauerhaft zu erhalten. Bei Abgang ist eine gleichwertige
Ersatzpflanzung vorzunehmen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_2d7be849-3d58-4bec-a554-3d752d9b2ab9">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>An den Rändern der hochwassergefährdeten Bereiche sind
zum Zwecke des Hochwasserschutzes, soweit erforderlich,
zusätzliche besondere bauliche Maßnahmen vorzusehen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_a5d62934-0c67-42df-ab71-478ecb59f350">
      <xplan:schluessel>§2 Nr.2.1</xplan:schluessel>
      <xplan:text>Großflächiger Einzelhandel kann ausnahmsweise auf den
mit „(B)“ bezeichneten Flächen zugelassen werden, wenn
er der Nahversorgung der angrenzenden Quartiere dient.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
</xplan:XPlanAuszug>