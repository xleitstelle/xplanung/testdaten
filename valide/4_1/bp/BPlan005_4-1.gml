﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_5D262569-FB24-4D27-938C-14E96D4D6CDD" xsi:schemaLocation="http://www.xplanung.de/xplangml/4/1 http://www.xplanungwiki.de/upload/XPlanGML/4.1-Kernmodell/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/4/1">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
      <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="GML_88bfe952-199f-4bba-bea2-c2b441737144">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan005_4-1</xplan:name>
      <xplan:beschreibung>Der vorhabenbezogene Bebauungsplan BPlan005_4-1
für den Geltungsbereich Knickweg – Barmbeker Straße –
Gertigstraße
(Bezirk Hamburg-Nord, Ortsteil 412) wird festgestellt.</xplan:beschreibung>
      <xplan:technHerstellDatum>2017-03-20</xplan:technHerstellDatum>
      <xplan:aendert />
      <xplan:wurdeGeaendertVon />
      <xplan:erstellungsMassstab>1000</xplan:erstellungsMassstab>
      <xplan:xPlanGMLVersion>4.1</xplan:xPlanGMLVersion>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_1C22CF46-2563-4D8D-9B5C-9F54AD82F6A7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
	  <xplan:refBegruendung>
        <xplan:XP_ExterneReferenz>
          <xplan:referenzURL>BPlan005_4-1.pdf</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
	  </xplan:refBegruendung>
      <xplan:texte xlink:href="#GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa" />
      <xplan:texte xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:texte xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:texte xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:texte xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:texte xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:texte xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:texte xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:texte xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>412</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:plangeber />
      <xplan:planArt>3000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2016-10-25</xplan:rechtsverordnungsDatum>
      <xplan:veraenderungssperre>false</xplan:veraenderungssperre>
      <xplan:staedtebaulicherVertrag>false</xplan:staedtebaulicherVertrag>
      <xplan:erschliessungsVertrag>false</xplan:erschliessungsVertrag>
      <xplan:durchfuehrungsVertrag>true</xplan:durchfuehrungsVertrag>
      <xplan:gruenordnungsplan>false</xplan:gruenordnungsplan>
      <xplan:bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="GML_6e871e4c-58e1-4b97-bf2a-a696816b2458">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_213A0A74-0359-4952-B033-278A430AED0A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
567446.882 5937692.583 567390.995 5937669.124 567386.293 5937667.145 
567389.479 5937654.684 567404.151 5937595.479 567474.996 5937628.152 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:nachrichtlich xlink:href="#GML_be2c01dd-4d15-4a6a-85c0-dc9565324486" />
      <xplan:nachrichtlich xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <xplan:praesentationsobjekt xlink:href="#GML_de628759-edb3-4f31-9d43-7d4f816bfb0c" />
      <xplan:praesentationsobjekt xlink:href="#GML_82075a17-ae02-4228-a4ac-debedfd66c1e" />
      <xplan:praesentationsobjekt xlink:href="#GML_a435c756-f459-41ce-b27c-d5992aa8f87e" />
      <xplan:praesentationsobjekt xlink:href="#GML_7d7588c9-1211-471e-ad15-8dd9f6605701" />
      <xplan:praesentationsobjekt xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <xplan:praesentationsobjekt xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <xplan:praesentationsobjekt xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <xplan:praesentationsobjekt xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <xplan:praesentationsobjekt xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <xplan:praesentationsobjekt xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <xplan:praesentationsobjekt xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <xplan:praesentationsobjekt xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <xplan:praesentationsobjekt xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <xplan:praesentationsobjekt xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <xplan:praesentationsobjekt xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <xplan:praesentationsobjekt xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <xplan:praesentationsobjekt xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <xplan:praesentationsobjekt xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <xplan:praesentationsobjekt xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <xplan:praesentationsobjekt xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <xplan:praesentationsobjekt xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <xplan:praesentationsobjekt xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <xplan:praesentationsobjekt xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <xplan:praesentationsobjekt xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <xplan:praesentationsobjekt xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <xplan:praesentationsobjekt xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />    
      <xplan:gehoertZuPlan xlink:href="#GML_88bfe952-199f-4bba-bea2-c2b441737144" />
      <xplan:inhaltBPlan xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <xplan:inhaltBPlan xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:inhaltBPlan xlink:href="#GML_5276e223-57f7-4596-9c9c-8fb909e11db2" />
      <xplan:inhaltBPlan xlink:href="#GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e" />
      <xplan:inhaltBPlan xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:inhaltBPlan xlink:href="#GML_354dc6b6-d910-4f29-8aeb-66d238013da6" />
      <xplan:inhaltBPlan xlink:href="#GML_b607f50f-8221-4811-81f0-7f6efef25db3" />
      <xplan:inhaltBPlan xlink:href="#GML_20f74701-9a0b-4c38-89b8-ae81db151e28" />
      <xplan:inhaltBPlan xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:inhaltBPlan xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:inhaltBPlan xlink:href="#GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f" />
      <xplan:inhaltBPlan xlink:href="#GML_9875e68a-2b21-40e4-8f66-ecf5c2290936" />
      <xplan:inhaltBPlan xlink:href="#GML_7b11be88-979b-483f-8042-82c51eff70af" />
      <xplan:inhaltBPlan xlink:href="#GML_21c90a12-5886-40dd-8a86-463267b6e201" />
      <xplan:inhaltBPlan xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:inhaltBPlan xlink:href="#GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299" />
      <xplan:inhaltBPlan xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:inhaltBPlan xlink:href="#GML_8376d624-8ccb-473d-81df-fc95b1ef28b3" />
      <xplan:inhaltBPlan xlink:href="#GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3" />
      <xplan:inhaltBPlan xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:inhaltBPlan xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <xplan:inhaltBPlan xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:inhaltBPlan xlink:href="#GML_5355860b-25fa-401c-a2ea-bb70654d17ab" />
      <xplan:inhaltBPlan xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_TextlicheFestsetzungsFlaeche gml:id="GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gliederung2>(A)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1" />
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F6223721-6B18-4DB6-A0AE-555FDF040F6B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567433.287 5937679.305 567416.015 5937672.057 567419.988 5937662.431 
567432.605 5937667.621 567439.105 5937652.001 567428.275 5937631.879 
567422.612 5937629.344 567428.448 5937616.596 567434.138 5937619.1 
567436.484 5937623.475 567442.246 5937634.25 567452.958 5937654.188 
567450.566 5937668.248 567438.775 5937666.233 567433.287 5937679.305 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_TextlicheFestsetzungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f1d95c57-3d31-44ec-8e8a-4e67b19482d1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567437.222 5937662.769</gml:lowerCorner>
          <gml:upperCorner>567437.222 5937662.769</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>gliederung2</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_c42631d9-5d1f-424d-b02f-d02bfdfaf665" />
      <xplan:position>
        <gml:Point gml:id="Gml_1256F4E1-7EEA-43EC-A0C0-0B788E8E3461" srsName="EPSG:25832">
          <gml:pos>567437.222 5937662.769</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_045ae35f-52f5-48d9-b5a6-3d5f339418be" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_5890c370-a3bc-412c-b650-13894b87a426" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_01C541CC-32EB-46B0-AD1A-DD2383DFE9A3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567452.175 5937661.631 567442.18 5937659.754 
567439.277 5937659.277 567439.035 5937655.06 567438.79 5937652.388 
567432.026 5937639.916 567442.758 5937634.17 567453.455 5937654.108 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>7</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_045ae35f-52f5-48d9-b5a6-3d5f339418be">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567444.114 5937651.76</gml:lowerCorner>
          <gml:upperCorner>567444.114 5937651.76</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:position>
        <gml:Point gml:id="Gml_D831D736-43D6-4E23-9D96-9D3983496215" srsName="EPSG:25832">
          <gml:pos>567444.114 5937651.76</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_5890c370-a3bc-412c-b650-13894b87a426">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.328 5937644.053</gml:lowerCorner>
          <gml:upperCorner>567439.328 5937644.053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_d43355f7-ac29-4763-82ea-57c2b5c4be9f" />
      <xplan:position>
        <gml:Point gml:id="Gml_299EE8B0-FCAA-4B53-A400-1BE9A800E4D8" srsName="EPSG:25832">
          <gml:pos>567439.328 5937644.053</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="GML_5276e223-57f7-4596-9c9c-8fb909e11db2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.015 5937616.596</gml:lowerCorner>
          <gml:upperCorner>567452.958 5937679.305</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F45F533B-D7EF-413A-AF25-630BF5F5A6BA" srsName="EPSG:25832">
          <gml:posList>567416.015 5937672.057 567419.988 5937662.431 567432.605 5937667.621 
567439.105 5937652.001 567428.275 5937631.879 567422.612 5937629.344 
567428.448 5937616.596 567434.138 5937619.1 567436.484 5937623.475 
567442.246 5937634.25 567452.958 5937654.188 567450.566 5937668.248 
567438.775 5937666.233 567433.287 5937679.305 567416.015 5937672.057 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>9999</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_d48b9ec4-07b4-4ad8-9664-407ae6cf9c4e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.23 5937680.271</gml:lowerCorner>
          <gml:upperCorner>567428.23 5937680.271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Point gml:id="Gml_B1B29771-70B3-489B-8B3D-F137B53CDAE4" srsName="EPSG:25832">
          <gml:pos>567428.23 5937680.271</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e21981e2-40a7-4dac-84b0-c88d371030af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">23.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_10c25c52-be4f-422d-8171-469001af2bf4" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_71A03B74-46DE-49BD-9650-5762611BB4C4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567442.758 5937634.17 567432.026 5937639.916 
567427.969 5937632.352 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_10c25c52-be4f-422d-8171-469001af2bf4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.835 5937628.058</gml:lowerCorner>
          <gml:upperCorner>567426.835 5937628.058</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:position>
        <gml:Point gml:id="Gml_02B99364-FDD5-4D3A-8C68-43E98F5E7276" srsName="EPSG:25832">
          <gml:pos>567426.835 5937628.058</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9aacc50d-2f71-4eb8-9729-2340b9f00b0d">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567433.633 5937633.755</gml:lowerCorner>
          <gml:upperCorner>567433.633 5937633.755</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e21981e2-40a7-4dac-84b0-c88d371030af" />
      <xplan:position>
        <gml:Point gml:id="Gml_1A78ACCD-263A-4035-8E5A-D58FE54D75EB" srsName="EPSG:25832">
          <gml:pos>567433.633 5937633.755</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_354dc6b6-d910-4f29-8aeb-66d238013da6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_645FF335-5642-4AAE-ADE9-E319DDAA170A" srsName="EPSG:25832">
          <gml:posList>567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
567437.269 5937671.067 567433.538 5937679.955 567415.353 5937672.325 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_b607f50f-8221-4811-81f0-7f6efef25db3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.935 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567442.758 5937639.916</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6E65B93E-0F3A-4E3A-B5F6-AA760BC4C0F6" srsName="EPSG:25832">
          <gml:posList>567428.223 5937615.867 567434.477 5937618.73 567442.758 5937634.17 
567432.026 5937639.916 567427.969 5937632.352 567421.935 5937629.605 
567428.223 5937615.867 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_20f74701-9a0b-4c38-89b8-ae81db151e28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_BA32D305-FEEF-4E8C-AF05-8A427967AF85" srsName="EPSG:25832">
          <gml:posList>567450.952 5937668.824 567440.924 5937667.111 567442.18 5937659.754 
567452.173 5937661.63 567450.952 5937668.824 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937615.867</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_c34619af-a786-41f0-a899-8a67df7b6750" />
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_81578AF7-EC89-48C6-A81B-ED5C193BA68A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567451.021 5937668.836 567448.434 5937683.72 
567445.762 5937685.099 567443.507 5937686.264 567432.779 5937681.762 
567433.538 5937679.955 567415.353 5937672.325 567418.938 5937663.64 
567419.705 5937661.78 567432.297 5937667.063 567438.475 5937652.392 
567438.319 5937651.648 567428.755 5937633.817 567427.969 5937632.352 
567424.532 5937630.787 567421.935 5937629.605 567428.223 5937615.867 
567434.477 5937618.73 567442.758 5937634.17 567453.455 5937654.108 
567452.176 5937661.63 567450.952 5937668.824 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.9</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9819c21a-42fc-4f0b-9e6f-ba64af501ab8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567425.133 5937653.994</gml:lowerCorner>
          <gml:upperCorner>567425.133 5937653.994</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_E91F03A2-E668-4123-8C49-33869047F204" srsName="EPSG:25832">
          <gml:pos>567425.133 5937653.994</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e8e4d5e7-01ae-4daf-9aa2-0a2cd08b294e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567428.301 5937671.953</gml:lowerCorner>
          <gml:upperCorner>567428.301 5937671.953</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_03B2A354-4AE0-41AA-98BD-D6A46DC63EC9" srsName="EPSG:25832">
          <gml:pos>567428.301 5937671.953</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_c34619af-a786-41f0-a899-8a67df7b6750">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.689 5937670.707</gml:lowerCorner>
          <gml:upperCorner>567434.689 5937670.707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e4f35aaa-4bfd-4e09-af7c-4e5490c2c1ab" />
      <xplan:position>
        <gml:Point gml:id="Gml_9D745692-6778-48CC-A9B5-ABC742ED334E" srsName="EPSG:25832">
          <gml:pos>567434.689 5937670.707</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.924 5937659.754</gml:lowerCorner>
          <gml:upperCorner>567452.173 5937668.824</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">26.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_9036c79b-5e09-4030-8c15-e0d9877d084c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_fba093ee-e774-4846-b286-e46649619852" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_08D59E06-7BAA-4573-8B5A-A95E4C134C55" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567452.173 5937661.63 567450.952 5937668.824 567440.924 5937667.111 
567442.18 5937659.754 567452.173 5937661.63 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_9036c79b-5e09-4030-8c15-e0d9877d084c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567445.543 5937665.568</gml:lowerCorner>
          <gml:upperCorner>567445.543 5937665.568</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:position>
        <gml:Point gml:id="Gml_4C844DB0-F3CE-4411-898D-70A69040A494" srsName="EPSG:25832">
          <gml:pos>567445.543 5937665.568</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_fba093ee-e774-4846-b286-e46649619852">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567443.466 5937662.039</gml:lowerCorner>
          <gml:upperCorner>567443.466 5937662.039</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e0c8b0e8-7b8c-4ead-ae77-f2f1551b9136" />
      <xplan:position>
        <gml:Point gml:id="Gml_B613393C-7AF7-4DF0-BAFE-64272AC26F38" srsName="EPSG:25832">
          <gml:pos>567443.466 5937662.039</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_ea917a61-3dcc-4889-9be0-0d56cbaad94f">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CCF4ED05-10D3-47F7-98B7-0F15C87DFCAA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567416.408 5937613.826 567412.057 5937623.382 567405.121 5937620.224 
567409.472 5937610.668 567416.408 5937613.826 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>1</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_9875e68a-2b21-40e4-8f66-ecf5c2290936">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.026 5937634.17</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937661.631</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_91FE477F-5C36-4B29-A149-23BFAA6A27A5" srsName="EPSG:25832">
          <gml:posList>567439.035 5937655.06 567438.79 5937652.388 567432.026 5937639.916 
567442.758 5937634.17 567453.455 5937654.108 567452.175 5937661.631 
567442.18 5937659.754 567439.277 5937659.277 567439.035 5937655.06 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_7b11be88-979b-483f-8042-82c51eff70af">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_3E57BCC1-C394-4F0C-9919-EE2DBBE24F04" srsName="EPSG:25832">
          <gml:posList>567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
567448.434 5937683.72 567443.507 5937686.264 567432.779 5937681.762 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_VerEntsorgung gml:id="GML_be2c01dd-4d15-4a6a-85c0-dc9565324486">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567431.741 5937675.181</gml:lowerCorner>
          <gml:upperCorner>567432.929 5937679.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>-1</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:LineString gml:id="Gml_2890AC35-0B5E-4524-AA17-08837848FB4B" srsName="EPSG:25832">
          <gml:posList>567432.929 5937675.181 567431.741 5937679.201 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_VerEntsorgung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_21c90a12-5886-40dd-8a86-463267b6e201">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_E1DDE3E9-0BE2-499B-B04C-EB1DC0D60B7A" srsName="EPSG:25832">
          <gml:posList>567407.329 5937656.657 567419.705 5937661.78 567415.353 5937672.325 
567403 5937667.139 567407.329 5937656.657 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UnverbindlicheVormerkung gml:id="GML_cf3318a5-fc25-4f6b-a594-893928f6a3de">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567452.825 5937635.581</gml:lowerCorner>
          <gml:upperCorner>567468.867 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7EE92B5D-4184-49D5-BB07-7ECBA355E90E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567468.867 5937663.607 567465.27 5937684.42 567462.07 5937698.959 
567458.005 5937697.252 567460.825 5937687.181 567456.625 5937685.781 
567452.825 5937636.781 567458.625 5937635.581 567459.825 5937647.581 
567467.425 5937647.381 567468.867 5937663.607 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:vormerkung>vorgesehene Bahnanlage</xplan:vormerkung>
    </xplan:BP_UnverbindlicheVormerkung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_7123beb9-4c5b-4bc8-8f16-ff739a4a1ede">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567457.938 5937683.621</gml:lowerCorner>
          <gml:upperCorner>567457.938 5937683.621</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>vormerkung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_cf3318a5-fc25-4f6b-a594-893928f6a3de" />
      <xplan:schriftinhalt>vorgesehene Bahnanlage</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point gml:id="Gml_E521B927-FB94-4448-98AA-2CCB0A2EF6FA" srsName="EPSG:25832">
          <gml:pos>567457.938 5937683.621</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">282.22</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="GML_9a4506b9-78b2-4dc4-bb55-088a4c627872">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567394.606 5937621.126</gml:lowerCorner>
          <gml:upperCorner>567438.475 5937667.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9FBC96AA-36AC-443B-B628-170E9C413157" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567432.297 5937667.063 567419.705 5937661.78 567405.227 5937655.787 
567401.59 5937657.649 567394.606 5937655.401 567403.316 5937621.126 
567421.935 5937629.605 567424.532 5937630.787 567427.969 5937632.352 
567428.755 5937633.817 567438.319 5937651.648 567438.475 5937652.392 
567432.297 5937667.063 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1600</xplan:zweckbestimmung>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
      <xplan:zugunstenVon>(FHH)</xplan:zugunstenVon>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_3250e7e1-8c6e-4ae7-8b0c-a950129740f6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567409.089 5937633.593</gml:lowerCorner>
          <gml:upperCorner>567409.089 5937633.593</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zugunstenVon</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:position>
        <gml:Point gml:id="Gml_DE683F7F-757D-4921-A0A3-7BF13E423F4D" srsName="EPSG:25832">
          <gml:pos>567409.089 5937633.593</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PTO gml:id="GML_2a0cdc97-fc26-4474-ba0d-e75183fb3a28">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567406.396 5937643.214</gml:lowerCorner>
          <gml:upperCorner>567406.396 5937643.214</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>nutzungsform</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9a4506b9-78b2-4dc4-bb55-088a4c627872" />
      <xplan:schriftinhalt>Spielplatz</xplan:schriftinhalt>
      <xplan:fontSperrung>0</xplan:fontSperrung>
      <xplan:skalierung>1</xplan:skalierung>
      <xplan:position>
        <gml:Point gml:id="Gml_9F0C4B4B-8DCB-4D0F-B6AB-89A992CC3C12" srsName="EPSG:25832">
          <gml:pos>567406.396 5937643.214</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
    </xplan:XP_PTO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="GML_56980ee8-0e1a-42e0-93f5-bd9c5247a299">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567405.121 5937610.668</gml:lowerCorner>
          <gml:upperCorner>567416.408 5937623.382</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_C3CBD03C-A6B2-4A41-9FEB-52139CEDA66B" srsName="EPSG:25832">
          <gml:posList>567409.472 5937610.668 567416.408 5937613.826 567412.057 5937623.382 
567405.121 5937620.224 567409.472 5937610.668 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="GML_321bdb15-2400-4c44-a553-35f402f8743e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937655.401</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_af37976e-6da1-4e93-a376-ad285c900b4c" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12" />
      <xplan:refTextInhalt xlink:href="#GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603" />
      <xplan:refTextInhalt xlink:href="#GML_6bac8f46-a325-4392-b115-07bb0e41e8c1" />
      <xplan:refTextInhalt xlink:href="#GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc" />
      <xplan:refTextInhalt xlink:href="#GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90" />
      <xplan:refTextInhalt xlink:href="#GML_114d1690-3976-42e3-87be-ec97b7ad773d" />
      <xplan:refTextInhalt xlink:href="#GML_2d0243f5-23e4-4f40-8538-d833e9c168dd" />
      <xplan:refTextInhalt xlink:href="#GML_0d37aade-f41e-4ec9-b026-79d4709945bf" />
      <xplan:refTextInhalt xlink:href="#GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2A5712D5-20F5-401C-A6CF-FA1393DA51A4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567418.938 5937663.64 567415.353 5937672.325 
567403 5937667.139 567395.687 5937664.07 567393.37 5937660.265 
567394.606 5937655.401 567401.59 5937657.649 567405.227 5937655.787 
567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:besondereArtDerBaulNutzung>1200</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>2000</xplan:bauweise>
      <xplan:vertikaleDifferenzierung>false</xplan:vertikaleDifferenzierung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_af37976e-6da1-4e93-a376-ad285c900b4c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567399.836 5937653.516</gml:lowerCorner>
          <gml:upperCorner>567399.836 5937653.516</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_4DEE8C4F-830F-4328-8581-4BB7F34D549D" srsName="EPSG:25832">
          <gml:pos>567399.836 5937653.516</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f368d23a-fdb3-46e4-9bdc-9a036fd640ad">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.427 5937662.618</gml:lowerCorner>
          <gml:upperCorner>567416.427 5937662.618</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>bauweise</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_BAD394E8-5274-4C8F-BFCC-43A4F133F9E7" srsName="EPSG:25832">
          <gml:pos>567416.427 5937662.618</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2f2817e2-116c-4e68-9d05-6e5f4ca0cd12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567410.188 5937663.997</gml:lowerCorner>
          <gml:upperCorner>567410.188 5937663.997</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>besondereArtDerBaulNutzung</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_321bdb15-2400-4c44-a553-35f402f8743e" />
      <xplan:position>
        <gml:Point gml:id="Gml_A0B3E607-ED25-4FDD-8F02-66D907D9F636" srsName="EPSG:25832">
          <gml:pos>567410.188 5937663.997</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="GML_8376d624-8ccb-473d-81df-fc95b1ef28b3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567386.293 5937595.479</gml:lowerCorner>
          <gml:upperCorner>567474.996 5937698.959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_692BC81A-D4F6-4292-BF80-BCA3460219E8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567462.07 5937698.959 567446.882 5937692.583 567390.995 5937669.124 
567386.293 5937667.145 567389.479 5937654.684 567404.151 5937595.479 
567474.996 5937628.152 567465.27 5937684.42 567462.07 5937698.959 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_06863C23-D926-4D2E-8146-023403A08B0C" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BDD3C512-E3CB-4C8E-BCC2-CFBAFB12733F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_3A1DF874-3D47-4549-AF34-B3F89C0ACE97" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E1A33D8E-0996-44D3-BD4D-237534C1840B" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_060B162D-B61F-42C5-B7BB-34AD3150494F" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7CA1F2C9-38E4-442D-BCBF-9C39C0B5C865" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_BEF1392A-19F9-46E3-A172-019EA5B56C88" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_0A6A10DA-8ADE-45BB-BCA2-4A364B455F99" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_79D83F63-F512-4718-AB4A-D84EBA1ACE18" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1B5D29FB-9358-4E2C-B64B-BFFD432DA6B9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_CBCEC2B4-F2A2-4713-80DA-19291D0778EA" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1183B547-7399-4D65-B43F-58F16220D370" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_33B58ADF-0D75-4FE2-BAEF-812BA75044AE" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_E8A2528A-E268-423A-A0AD-D9AECB331885" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_5580937C-4623-4D35-AE42-A41A2DABA012" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_6A305CA8-79DE-4C7B-885B-4B650B3900E8" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_7857AFFA-8D9A-4EC5-8D1F-A3841A5B8B07" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_C5AC08A3-4846-43F2-9228-C27D8A2F7E30" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_1573C9F7-933A-4D5B-A905-41D3FC0EC89E" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_FF95C6A2-B654-4254-A33B-B384B18A0EE9" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_HoehenMass gml:id="GML_91e2ff9f-d5ff-4c87-84ae-9a6a02ddeed3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567453.759 5937669.764</gml:lowerCorner>
          <gml:upperCorner>567453.759 5937669.764</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Straßenhöhe bezogen auf NHN</xplan:text>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:h uom="m">6.7</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Point gml:id="Gml_CF3D55FF-26C4-487D-BC03-68E63586BC97" srsName="EPSG:25832">
          <gml:pos>567453.759 5937669.764</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_HoehenMass>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BesondererNutzungszweckFlaeche gml:id="GML_9775f239-210a-4d61-ad8f-251f5a31296e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403.316 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567428.223 5937629.605</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:wirdDargestelltDurch xlink:href="#GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_f44449a4-6628-4c84-9dfe-d82b4dafca06" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_85B96DAB-495D-4F4B-8A75-594B5537ADDB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:Ring>
              <gml:curveMember>
                <gml:Curve gml:id="Gml_9A82A1EA-F3E0-49C2-9647-8E4C5C5B781B" srsName="EPSG:25832">
                  <gml:segments>
                    <gml:Arc interpolation="circularArc3Points" numArc="1">
                      <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567407.834857796 5937607.72206805 567410.377 5937607.7 
</gml:posList>
                    </gml:Arc>
                  </gml:segments>
                </gml:Curve>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_105B6562-4B0C-49B9-B277-2FE05E8AA925" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567428.223 5937615.867 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_403CA5CB-0DA9-4780-A754-E97216E5EA07" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567421.935 5937629.605 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_92887C04-253B-4181-AB12-065F4D25658C" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567421.935 5937629.605 567403.316 5937621.126 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
              <gml:curveMember>
                <gml:LineString gml:id="Gml_4A27E4D3-D5CB-4B48-870B-E50B2DBEAAB4" srsName="EPSG:25832">
                  <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567406.223 5937609.688 </gml:posList>
                </gml:LineString>
              </gml:curveMember>
            </gml:Ring>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>1</xplan:GRZ>
      <xplan:zweckbestimmung>Kiosk</xplan:zweckbestimmung>
    </xplan:BP_BesondererNutzungszweckFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_2eaf4d0b-d178-41ff-801f-748c45f8be2a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567412.297 5937615.306</gml:lowerCorner>
          <gml:upperCorner>567412.297 5937615.306</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:position>
        <gml:Point gml:id="Gml_4BA87EDC-08D7-4CDD-B12E-CF43E5530314" srsName="EPSG:25832">
          <gml:pos>567412.297 5937615.306</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f44449a4-6628-4c84-9dfe-d82b4dafca06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567416.532 5937621.891</gml:lowerCorner>
          <gml:upperCorner>567416.532 5937621.891</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>GRZ</xplan:art>
      <xplan:art>zweckbestimmung</xplan:art>
      <xplan:art>rechtscharakter</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_9775f239-210a-4d61-ad8f-251f5a31296e" />
      <xplan:position>
        <gml:Point gml:id="Gml_84B78C61-EEEE-4A3E-B10D-698119E65CCF" srsName="EPSG:25832">
          <gml:pos>567416.532 5937621.891</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_55177fec-7599-4785-a8e2-806742a9b2d9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567403 5937656.657</gml:lowerCorner>
          <gml:upperCorner>567419.705 5937672.325</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">22.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_796aa811-807f-4550-abab-24b9cc8aa359" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8A1BCB60-2972-469B-85C0-BA32ECFB8384" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567419.705 5937661.78 567415.353 5937672.325 567403 5937667.139 
567407.329 5937656.657 567419.705 5937661.78 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>5</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_796aa811-807f-4550-abab-24b9cc8aa359">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.862 5937659</gml:lowerCorner>
          <gml:upperCorner>567421.862 5937659</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_55177fec-7599-4785-a8e2-806742a9b2d9" />
      <xplan:position>
        <gml:Point gml:id="Gml_B709E801-5B85-4AA6-A8F5-1E50DD728842" srsName="EPSG:25832">
          <gml:pos>567421.862 5937659</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_bff033ce-687f-48fe-b27c-542b545fb292">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567432.779 5937666.793</gml:lowerCorner>
          <gml:upperCorner>567450.952 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">30</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_190f5132-7a3c-4e3f-900e-0705faba810a" />
      <xplan:wirdDargestelltDurch xlink:href="#GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1E9A0330-0555-4A81-AD4F-10F00E672D44" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567448.434 5937683.72 567443.507 5937686.264 
567432.779 5937681.762 567433.538 5937679.955 567437.269 5937671.067 
567437.993 5937669.342 567439.063 5937666.793 567450.952 5937668.824 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>6</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_190f5132-7a3c-4e3f-900e-0705faba810a">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567439.986 5937674.081</gml:lowerCorner>
          <gml:upperCorner>567439.986 5937674.081</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>hoehenangabe</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:position>
        <gml:Point gml:id="Gml_B761A450-BCC1-4D3B-93E0-644F0DD55245" srsName="EPSG:25832">
          <gml:pos>567439.986 5937674.081</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_e6e53967-80ce-45d6-bef0-60b7964ab6ea">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567440.194 5937679.946</gml:lowerCorner>
          <gml:upperCorner>567440.194 5937679.946</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_bff033ce-687f-48fe-b27c-542b545fb292" />
      <xplan:position>
        <gml:Point gml:id="Gml_DA538C17-F52F-4E33-8244-F219A6B6D70D" srsName="EPSG:25832">
          <gml:pos>567440.194 5937679.946</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="GML_5355860b-25fa-401c-a2ea-bb70654d17ab">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567393.37 5937607.4282</gml:lowerCorner>
          <gml:upperCorner>567453.455 5937686.264</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Curve gml:id="Gml_8A968D76-7616-43B3-A0DA-7EF26A76CB76" srsName="EPSG:25832">
          <gml:segments>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567406.223 5937609.688 567403.316 5937621.126 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403.316 5937621.126 567394.606 5937655.401 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567394.606 5937655.401 567393.37 5937660.265 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567393.37 5937660.265 567395.687 5937664.07 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567395.687 5937664.07 567403 5937667.139 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567403 5937667.139 567415.353 5937672.325 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567415.353 5937672.325 567433.538 5937679.955 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567433.538 5937679.955 567432.779 5937681.762 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567432.779 5937681.762 567443.507 5937686.264 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567443.507 5937686.264 567445.762 5937685.099 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567445.762 5937685.099 567448.434 5937683.72 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567448.434 5937683.72 567451.021 5937668.836 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567451.021 5937668.836 567450.952 5937668.824 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567450.952 5937668.824 567452.176 5937661.63 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567452.176 5937661.63 567453.455 5937654.108 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567453.455 5937654.108 567442.758 5937634.17 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567442.758 5937634.17 567434.477 5937618.73 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567434.477 5937618.73 567428.223 5937615.867 </gml:posList>
            </gml:LineStringSegment>
            <gml:LineStringSegment interpolation="linear">
              <gml:posList srsName="EPSG:25832">567428.223 5937615.867 567410.377 5937607.7 </gml:posList>
            </gml:LineStringSegment>
            <gml:ArcString interpolation="circularArc3Points" numArc="1">
              <gml:posList srsName="EPSG:25832">567410.377 5937607.7 567407.834857796 5937607.72206805 567406.223 5937609.688 
</gml:posList>
            </gml:ArcString>
          </gml:segments>
        </gml:Curve>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="GML_e1568a2b-1768-4889-aedb-95449c029c25">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567415.353 5937661.78</gml:lowerCorner>
          <gml:upperCorner>567437.993 5937679.955</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:ebene>0</xplan:ebene>
      <xplan:hoehenangabe>
        <xplan:XP_Hoehenangabe>
          <xplan:hoehenbezug>1000</xplan:hoehenbezug>
          <xplan:bezugspunkt>6000</xplan:bezugspunkt>
          <xplan:h uom="m">22.5</xplan:h>
        </xplan:XP_Hoehenangabe>
      </xplan:hoehenangabe>
      <xplan:wirdDargestelltDurch xlink:href="#GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3B151FFA-08B9-499C-8FC1-E74B1966A660" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">567437.993 5937669.342 567437.269 5937671.067 567433.538 5937679.955 
567415.353 5937672.325 567419.705 5937661.78 567437.993 5937669.342 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:Z>4</xplan:Z>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_PPO gml:id="GML_f6cc864b-fa62-4bc6-93b6-79483ec0ca91">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567421.339 5937668.137</gml:lowerCorner>
          <gml:upperCorner>567421.339 5937668.137</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:art>Z</xplan:art>
      <xplan:index>0</xplan:index>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:dientZurDarstellungVon xlink:href="#GML_e1568a2b-1768-4889-aedb-95449c029c25" />
      <xplan:position>
        <gml:Point gml:id="Gml_08FA53DB-605B-48C0-9332-B562F84C9B4D" srsName="EPSG:25832">
          <gml:pos>567421.339 5937668.137</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:drehwinkel uom="grad">0</xplan:drehwinkel>
      <xplan:skalierung>1</xplan:skalierung>
    </xplan:XP_PPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_de628759-edb3-4f31-9d43-7d4f816bfb0c">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567426.484 5937661.969</gml:lowerCorner>
          <gml:upperCorner>567427.781 5937668.164</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_C628A721-7364-4BCE-9ADF-117C282E0416" srsName="EPSG:25832">
          <gml:posList>567426.484 5937661.969 567427.781 5937668.164 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_82075a17-ae02-4228-a4ac-debedfd66c1e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567411.794 5937658.752</gml:lowerCorner>
          <gml:upperCorner>567417.901 5937660.897</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F53DCF54-363F-4C47-9DB7-F460AF1E5232" srsName="EPSG:25832">
          <gml:posList>567417.901 5937658.752 567411.794 5937660.897 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_a435c756-f459-41ce-b27c-d5992aa8f87e">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567434.065 5937655.343</gml:lowerCorner>
          <gml:upperCorner>567437.337 5937661.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_1285BE9B-0C0B-4E0D-A3C7-3C056730786C" srsName="EPSG:25832">
          <gml:posList>567434.065 5937655.343 567437.337 5937661.831 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_LPO gml:id="GML_7d7588c9-1211-471e-ad15-8dd9f6605701">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>567400.372 5937656.059</gml:lowerCorner>
          <gml:upperCorner>567401.041 5937660.34</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:gehoertZuBereich xlink:href="#GML_6e871e4c-58e1-4b97-bf2a-a696816b2458" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B49B5714-6502-4417-A71A-F92CFCF2799B" srsName="EPSG:25832">
          <gml:posList>567401.041 5937656.059 567400.372 5937660.34 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:XP_LPO>
  </gml:featureMember>  
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_ad1df68a-7a33-4c35-83dd-db236a88f8aa">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Im Plangebiet ist in den Schlafräumen durch geeignete bauliche
Schallschutzmaßnahmen wie zum Beispiel Doppelfassaden,
verglaste Loggien, Wintergärten, besondere Fensterkonstruktionen
oder in ihrer Wirkung vergleichbare Maßnahmen
sicherzustellen, dass ein Innenraumpegel bei
gekipptem Fenster von 30 dB(A) während der Nachtzeit
nicht überschritten wird. Erfolgt die bauliche Schallschutzmaßnahme
in Form von verglasten Loggien oder Wintergärten
muss dieser Innenraumpegel bei gekippten/teilgeöffneten
Bauteilen erreicht werden. Wohn-/Schlafräume in
Einzimmerwohnungen und Kinderzimmer sind wie Schlafräume
zu beurteilen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_0d37aade-f41e-4ec9-b026-79d4709945bf">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die nicht überbauten Flächen
von Tiefgaragen mit einem mindestens 50 cm starken
durchwurzelbaren Substrataufbau zu versehen und gärtnerisch
anzulegen. Hiervon ausgenommen sind die erforderlichen
Flächen für Terrassen, Wege und Freitreppen, Kinderspielflächen
sowie Bereiche, die der Belichtung, Be- und
Entlüftung oder der Aufnahme von technischen Anlagen
dienen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_0e93b05c-9f78-4b4f-af48-fc6029c3d3dc">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Stellplätze nur in Tiefgaragen
zulässig. Tiefgaragen sind auch außerhalb der überbaubaren
Grundstücksflächen zulässig. Tiefgaragen dürfen
einschließlich ihrer Überdeckung nicht über die natürliche
Geländeoberfläche herausragen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_13dc92bd-f3b6-4de4-b61b-f7b5f67a0628">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind die Freiflächen zur öffentlichen
Grünanlage mit einer Hecke abzupflanzen. Für die
Hecke sind standortgerechte einheimische Gehölze zu verwenden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_2d0243f5-23e4-4f40-8538-d833e9c168dd">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind mindestens 55 vom Hundert
(v. H.) der Dachflächen und auf der Fläche für den
besonderen Nutzungszweck – Kiosk – mindestens 80 v. H.
der Dachflächen mit einem mindestens 10 cm starken
durchwurzelbaren Substrataufbau zu versehen und extensiv
zu begrünen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_6bac8f46-a325-4392-b115-07bb0e41e8c1">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind Terrassen im Anschluss
an die Hauptnutzung bis zu einer Tiefe von 4 m auch außerhalb
der überbaubaren Grundstücksfläche zulässig. Untergeordnete
Bauteile wie Vordächer, Balkone und Erker im
Bereich von öffentlichen Grünflächen sind unzulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_7df44b2f-cd2c-4b01-84f7-dfcc69283603">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind innerhalb der mit „(A)“
bezeichneten Fläche (Vorhabengebiet) im Rahmen der festgesetzten
Nutzungen nur solche Vorhaben zulässig, zu
deren Durchführung sich der Vorhabenträger im Durchführungsvertrag
verpflichtet.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_c27ab7dd-8e16-4f88-abae-6b23d49e7a90">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet darf die festgesetzte Grundflächenzahl
für Tiefgaragen bis zu einer Grundflächenzahl
von 1,0 überschritten werden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="GML_114d1690-3976-42e3-87be-ec97b7ad773d">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Im allgemeinen Wohngebiet sind auf den Flurstücken 1477,
1478 und 1480 der Gemarkung Winterhude vor den straßenzugewandten
Fenstern der Wohn- und Schlafräume
lärmgeschützte Außenbereiche durch bauliche Schallschutzmaßnahmen,
wie etwa verglaste Loggien, Wintergärten
oder in ihrer Wirkung vergleichbare Maßnahmen zwingend
vorzusehen. In den lärmgeschützten Außenbereichen
ist bei geöffneten Fenstern/Bauteilen sicherzustellen, dass
ein Tagpegel von weniger als 65 dB(A) erreicht wird.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
</xplan:XPlanAuszug>