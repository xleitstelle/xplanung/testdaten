﻿<?xml version="1.0" encoding="utf-8"?>
<xplan:XPlanAuszug xmlns:wfs="http://www.opengis.net/wfs" xmlns:gml="http://www.opengis.net/gml/3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xlink="http://www.w3.org/1999/xlink" gml:id="Gml_D16AD1BB-EF50-4FF6-9CF2-14C194B171D1" xsi:schemaLocation="http://www.xplanung.de/xplangml/4/1 http://www.xplanungwiki.de/upload/XPlanGML/4.1-Kernmodell/Schema/XPlanung-Operationen.xsd" xmlns:xplan="http://www.xplanung.de/xplangml/4/1">
  <gml:boundedBy>
    <gml:Envelope srsName="EPSG:25832">
      <gml:lowerCorner>582504.1297 5920140.662</gml:lowerCorner>
      <gml:upperCorner>583267.115 5921477.987</gml:upperCorner>
    </gml:Envelope>
  </gml:boundedBy>
  <gml:featureMember>
    <xplan:BP_Plan gml:id="Gml_EBB6FF42-D04B-4807-ADDE-0701B82C96CB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582504.1297 5920140.662</gml:lowerCorner>
          <gml:upperCorner>583267.115 5921477.987</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:name>BPlan002_4-1</xplan:name>
      <xplan:technHerstellDatum>2017-05-19</xplan:technHerstellDatum>
      <xplan:xPlanGMLVersion>4.1</xplan:xPlanGMLVersion>
      <xplan:raeumlicherGeltungsbereich>
        <gml:Polygon gml:id="Gml_705791E6-C26A-47E0-AC7E-024A8C0D2265" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582830.741 5921381.091 582837.271 5921420.932 582840.326 5921441.47 
582843.262 5921452.327 582855.514 5921477.987 582844.732 5921471.354 
582838.561 5921467.936 582820.552 5921457.96 582819.954 5921457.662 
582816.339 5921455.864 582813.623 5921454.351 582801.232 5921447.453 
582798.414 5921445.377 582787.998 5921439.878 582780.876 5921436.12 
582762.736 5921427.772 582755.795 5921392.755 582750.198 5921363.295 
582741.031 5921314.162 582734.092 5921274.782 582730.279 5921255.158 
582723.937 5921221.981 582716.643 5921182.659 582714.201 5921167.851 
582710.816 5921143.032 582708.313 5921126.197 582706.178 5921113.368 
582702.531 5921093.707 582699.84 5921081.135 582700.126 5921073.048 
582699.564 5921071.821 582698.484 5921068.387 582694.708 5921053.648 
582691.226 5921039.05 582686.491 5921021.181 582686.248 5921019.688 
582686.004 5921018.197 582676.721 5920980.839 582666.036 5920941.464 
582665.182 5920938.87 582656.621 5920931.167 582655.135 5920933.48 
582652.232 5920934.846 582644.094 5920940.983 582635.806 5920946.985 
582631.529 5920952.542 582627.708 5920959.844 582624.132 5920971.378 
582615.203 5920982.16 582609.514 5920987.8 582598.9013 5920994.4924 
582594.7425 5920991.736 582592.9099 5920990.5214 582591.806 5920989.7463 
582589.762 5920988.311 582587.955 5920986.585 582584.417 5920986.127 
582578.841 5920986.65 582575.458 5920986.966 582564.846 5920989.077 
582554.287 5920992.388 582547.784 5920994.327 582540.537 5920995.956 
582536.819 5920996.027 582531.217 5920985.964 582523.038 5920975.41 
582510.836 5920961.679 582504.1297 5920953.3219 582516.846 5920921.204 
582518.869 5920916.096 582526.421 5920902.225 582527.141 5920902.581 
582530.539 5920898.147 582534.329 5920893.501 582538.204 5920888.931 
582542.184 5920884.447 582545.873 5920880.461 582546.937 5920879.313 
582557.738 5920869.946 582571.027 5920858.614 582603.81 5920830.584 
582607.003 5920827.854 582593.242 5920795.494 582580.859 5920766.377 
582590.463 5920762.037 582591.806 5920761.521 582595.304 5920760.179 
582593.784 5920757.303 582601.105 5920753.996 582606.638 5920750.438 
582601.501 5920735.991 582599.235 5920729.617 582598.5869 5920728.01 
582649.8483 5920707.3549 582652.0954 5920712.3774 582672.88 5920706.152 
582675.711 5920705.304 582677.365 5920710.188 582690.124 5920705.39 
582698.371 5920702.288 582705.539 5920699.976 582705.645 5920700.36 
582709.24 5920699.339 582708.949 5920698.381 582715.998 5920696.133 
582741.808 5920687.902 582747.9959 5920704.1842 582788.8143 5920686.7506 
582814.5564 5920675.7562 582827.3912 5920670.2744 582823.2849 5920662.241 
582854.7413 5920646.0293 582852.634 5920641.24 582833.67 5920593.696 
582822.401 5920564.804 582811.83 5920538.762 582785.648 5920474.258 
582767.648 5920429.038 582769.1614 5920428.6442 582733.849 5920342.8271 
582752.662 5920335.4463 582782.4258 5920321.9291 582817.1537 5920306.1575 
582834.9571 5920298.0721 582936.8122 5920251.8147 582926.69 5920225.943 
582901.444 5920163.122 582955.761 5920140.662 582995.814 5920241.768 
582997.852 5920246.911 583041.782 5920364.486 583041.95 5920364.945 
583042.258 5920365.789 583059.759 5920413.718 583064.217 5920420.589 
583080.998 5920446.868 583085.175 5920453.395 583089.821 5920459.909 
583104.063 5920478.028 583109.481 5920485.223 583120.546 5920502.245 
583136.98 5920526.743 583155.487 5920548.019 583157.6674 5920551.7079 
583160.41 5920556.348 583167.829 5920559.029 583176.133 5920564.471 
583179.422 5920566.912 583182.191 5920571.135 583188.57 5920611.997 
583190.278 5920622.931 583199.395 5920681.316 583202.118 5920696.869 
583211.448 5920745.352 583243.938 5920912.69 583255.331 5920970.157 
583255.757 5920972.311 583266.641 5921031.183 583267.115 5921033.686 
583230.027 5921040.712 583154.6745 5921055.9394 583178.551 5921181.086 
583190.903 5921243.847 583116.633 5921259.836 583127.76 5921317.799 
583078.016 5921329.358 583051.9814 5921335.3938 583016.4548 5921343.6257 
583006.731 5921293.264 582995.8553 5921237.6242 582976.043 5921243.173 
582973.404 5921243.912 582949.949 5921250.944 582956.954 5921297.447 
582913.421 5921305.128 582894.3956 5921308.2654 582893.99 5921305.588 
582888.696 5921275.205 582888.4641 5921274.0727 582869.357 5921272.848 
582845.382 5921270.747 582809.426 5921268.882 582830.741 5921381.091 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:raeumlicherGeltungsbereich>
      <xplan:texte xlink:href="#Gml_964AAC48-228A-493E-8FF2-91C2100BA7D1" />
      <xplan:texte xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:texte xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:texte xlink:href="#Gml_76C0DE3D-77B7-41CF-BC69-ECE97AB834AD" />
      <xplan:texte xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:texte xlink:href="#Gml_7D3EEA34-335F-4BA2-A0C1-1C2E92B1CC51" />
      <xplan:texte xlink:href="#Gml_6559F02E-60D7-46EF-91A3-802FADD4F761" />
      <xplan:texte xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:texte xlink:href="#Gml_8CE48D01-FBF1-4644-B74A-9D889590E6B3" />
      <xplan:texte xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:texte xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:texte xlink:href="#Gml_DF5100AC-6196-4850-BF5C-09C939E867E8" />
      <xplan:texte xlink:href="#Gml_0A221ACB-19F2-4FB7-BD6C-6F8F8BD844AF" />
      <xplan:texte xlink:href="#Gml_82F37C43-D16E-4A10-98BB-D64E8B71834F" />
      <xplan:texte xlink:href="#Gml_D96C12C7-F959-4137-BCB9-73ADDD01B224" />
      <xplan:texte xlink:href="#Gml_737430E9-0D25-40C6-9136-10AE2114EE9B" />
      <xplan:texte xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:texte xlink:href="#Gml_9EFD0E16-87E5-4526-91ED-08F5A0C2FDEE" />
      <xplan:texte xlink:href="#Gml_CFEF65DC-57B4-467F-94D2-ED01D18CBF72" />
      <xplan:texte xlink:href="#Gml_096CCB49-A671-40E1-A5BE-BBE205AC911C" />
      <xplan:texte xlink:href="#Gml_FAE8F077-0048-4FD9-80A1-1A9EB61D20FD" />
      <xplan:begruendungsTexte xlink:href="#Gml_386949A9-070F-4CA6-AF32-7827F5DD655F" />
      <xplan:gemeinde>
        <xplan:XP_Gemeinde>
          <xplan:ags>02000000</xplan:ags>
          <xplan:gemeindeName>Freie und Hansestadt Hamburg</xplan:gemeindeName>
          <xplan:ortsteilName>605, 606</xplan:ortsteilName>
        </xplan:XP_Gemeinde>
      </xplan:gemeinde>
      <xplan:planArt>1000</xplan:planArt>
      <xplan:verfahren>1000</xplan:verfahren>
      <xplan:rechtsstand>3000</xplan:rechtsstand>
      <xplan:rechtsverordnungsDatum>2006-06-08</xplan:rechtsverordnungsDatum>
      <xplan:bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
    </xplan:BP_Plan>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_964AAC48-228A-493E-8FF2-91C2100BA7D1">
      <xplan:schluessel>§2 Nr.1</xplan:schluessel>
      <xplan:text>In den nach § 172 Absatz 1 Satz 1 Nummer 1 des Baugesetzbuchs als „Erhaltungsbereiche" bezeichneten Gebieten bedürfen zur Erhaltung der städtebaulichen Eigenart des Gebiets auf Grund seiner städtebaulichen Gestalt der Rückbau, die Änderung, die Nutzungsänderung oder die Errichtung baulicher Anlagen einer Genehmigung, und zwar auch dann, wenn nach bauordnungsrechtlichen Vorschriften eine Genehmigung nicht erforderlich ist. Die Genehmigung zum Rückbau, zur Änderung oder zur Nutzungsänderung darf nur versagt werden, wenn die bauliche Anlage allein oder im Zusammenhang mit anderen baulichen Anlagen das Ortsbild oder das Landschaftsbild prägt oder sonst von städtebaulicher, insbesondere geschichtlicher oder künstlerischer Bedeutung ist. Die Genehmigung zur Errichtung der baulichen Anlage darf nur versagt werden, wenn die städtebauliche Gestalt des Gebiets durch die beabsichtigte bauliche Anlage beeinträchtigt wird.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76">
      <xplan:schluessel>§2 Nr.2</xplan:schluessel>
      <xplan:text>In den Dorfgebieten sind außerhalb der überbaubaren Grundstücksflächen bauliche Anlagen, die der landwirtschaftlichen Erzeugung, der landwirtschaftlichen Verarbeitung oder dem Vertrieb dienen, wie zum Beispiel Gewächshäuser, Stallgebäude oder Maschinenhallen, bis zu einer Grundflächenzahl von 0,6 zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_3302262A-F560-4FE6-B941-FC1D727BD319">
      <xplan:schluessel>§2 Nr.3</xplan:schluessel>
      <xplan:text>In den Dorfgebieten darf die Gebäudehöhe 9 m über vorhandenem oder aufgehöhtem Gelände nicht überschreiten.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_76C0DE3D-77B7-41CF-BC69-ECE97AB834AD">
      <xplan:schluessel>§2 Nr.4</xplan:schluessel>
      <xplan:text>In dem mit „(A)" bezeichneten Dorfgebiet ist das Wohnen unzulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE">
      <xplan:schluessel>§2 Nr.5</xplan:schluessel>
      <xplan:text>Auf den Flächen für die Landwirtschaft sind bauliche Anlagen nur innerhalb der Baugrenzen zulässig und wenn sie der landwirtschaftlichen Erzeugung, der landwirtschaftlichen Verarbeitung oder dem Vertrieb landwirtschaftlicher Produkte dienen, wie zum Beispiel Gewächshäuser, Stallgebäude, Maschinenhallen. Die Gebäudehöhe darf 8 m nicht überschreiten. Befestigte landwirtschaftliche Wege sind auch außerhalb der Baugrenzen zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_7D3EEA34-335F-4BA2-A0C1-1C2E92B1CC51">
      <xplan:schluessel>§2 Nr.6</xplan:schluessel>
      <xplan:text>Die Höhe der Erdgeschossfußbodenoberkanten über der vorhandenen beziehungsweise aufgehöhten Geländeoberfläche darf straßenseitig 0,4 m nicht überschreiten.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_6559F02E-60D7-46EF-91A3-802FADD4F761">
      <xplan:schluessel>§2 Nr.7</xplan:schluessel>
      <xplan:text>Auf Flächen im Außendeichsbereich, die tiefer als 2 m über Normalnull (NN) liegen, sind für Wohngebäude die Erdgeschossfußbodenoberkanten in einer Höhe zwischen 2 m und 2,2 m über NN auszuführen. Des Weiteren sind Geländeaufhöhungen nur zulässig
a) für Rampen, die zur Erschließung erforderlich sind, oder
b) bis zur Oberkante der für die Erschließung erforderlichen öffentlichen Straßenverkehrsfläche, sofern der Abstand zwischen Hauptgebäude und Straßenverkehrsfläche weniger als 6 m beträgt.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE">
      <xplan:schluessel>§2 Nr.8</xplan:schluessel>
      <xplan:text>In den Dorfgebieten sind Dächer von Wohngebäuden als Sattel- oder Krüppelwalmdächer mit beiderseits gleicher Neigung zwischen 40 Grad und 50 Grad auszuführen. Balkone, Dachaufbauten und -einschnitte (zum Beispiel Loggien) sowie Zwerchgiebel dürfen insgesamt eine Länge haben, die höchstens 30 vom Hundert (v. H.) der Länge ihrer zugehörigen Gebäudeseite entspricht. Es sind nur rote, braune, graue und schwarze Dacheindeckungen in nicht glänzender Ausführung sowie Reetdächer und begrünte Dächer zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_8CE48D01-FBF1-4644-B74A-9D889590E6B3">
      <xplan:schluessel>§2 Nr.9</xplan:schluessel>
      <xplan:text>Die Außenwände von baulichen Anlagen, mit Ausnahme von baulichen Anlagen, die ausschließlich der landwirtschaftlichen Produktion dienen (zum Beispiel Stallgebäude, Maschinenhallen, Gewächshäuser), sind als rotes oder rotbraunes Ziegelmauerwerk auszuführen. Für Nebengebäude und für untergeordnete Teile von Außenwänden von Wohngebäuden, die 30 v. H. der jeweiligen Fassadenfläche nicht überschreiten, ist außerdem weiß, braun und grün angestrichenes Holz sowie Holz in Naturfarbe zulässig. Für Nebengebäude sind außerdem weiße Putzflächen zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_962D2059-F9B0-47DE-96BA-A704508D92B3">
      <xplan:schluessel>§2 Nr.10</xplan:schluessel>
      <xplan:text>Auf den privaten Grünflächen - Gärten - sind notwendige Zufahrten in einer Breite bis 3 m sowie nicht überdachte Stellplätze bis 25 m² je Grundstück zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C">
      <xplan:schluessel>§2 Nr.11</xplan:schluessel>
      <xplan:text>Auf den privaten Grünflächen - Böschung - sind notwendige Zufahrten in einer Breite bis 4 m je Grundstück zulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_DF5100AC-6196-4850-BF5C-09C939E867E8">
      <xplan:schluessel>§2 Nr.12</xplan:schluessel>
      <xplan:text>Auf den Grundstücksflächen, die ausschließlich dem Wohnen dienen, sind Fahr- und Gehwege sowie Stellplätze in wasser- und luftdurchlässigem Aufbau herzustellen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_0A221ACB-19F2-4FB7-BD6C-6F8F8BD844AF">
      <xplan:schluessel>§2 Nr.13</xplan:schluessel>
      <xplan:text>Außenwände von Gebäuden mit Ausnahme von Wohngebäuden, deren Fensterabstand mehr als 5 m beträgt sowie fensterlose Fassaden sind mit Schling- oder Kletterpflanzen einzugrünen; je 2 m Wandlänge ist mindestens eine Pflanze zu verwenden. Alternativ können vor der jeweiligen Fassade Bäume und Sträucher angepflanzt werden.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_82F37C43-D16E-4A10-98BB-D64E8B71834F">
      <xplan:schluessel>§2 Nr.14</xplan:schluessel>
      <xplan:text>Pro Wohngebäude ist mindestens ein kleinkroniger, standortgerechter, einheimischer Laubbaum oder ein hochstämmiger Obstbaum zu pflanzen.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_D96C12C7-F959-4137-BCB9-73ADDD01B224">
      <xplan:schluessel>§2 Nr.15</xplan:schluessel>
      <xplan:text>Für festgesetzte Baum- und Strauchpflanzungen sind standortgerechte, einheimische Laubgehölze zu verwenden und zu erhalten.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_737430E9-0D25-40C6-9136-10AE2114EE9B">
      <xplan:schluessel>§2 Nr.16</xplan:schluessel>
      <xplan:text>Für die zu erhaltenden Bäume sind bei Abgang Ersatzpflanzungen mit gleichartigen, großkronigen Bäumen vorzunehmen. Außerhalb von öffentlichen Straßenverkehrsflächen und Flächen für wasserwirtschaftliche Maßnahmen sind Geländeaufhöhungen und Abgrabungen im Kronenbereich zu erhaltender Bäume unzulässig.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_15465347-17DC-4025-93FB-F9C758C2A8BA">
      <xplan:schluessel>§2 Nr.17.1</xplan:schluessel>
      <xplan:text>Auf den Flächen zum Schutz, zur Pflege und zur Entwicklung von Boden, Natur und Landschaft gilt:
Der mit „U“ bezeichnete Uferstreifen ist naturnah zu entwickeln. Zulässig sind einheimische, standortgerechte Stauden, Sträucher und Gehölze sowie extensive Grünlandnutzung beziehungsweise Mähwiese.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_9EFD0E16-87E5-4526-91ED-08F5A0C2FDEE">
      <xplan:schluessel>§2 Nr.17.2</xplan:schluessel>
      <xplan:text>Auf den Flächen zum Schutz, zur Pflege und zur Entwicklung von Boden, Natur und Landschaft gilt:
Die mit „O“ bezeichnete Fläche ist als Streuobstwiese mit extensiver Wiesennutzung zu erhalten.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_CFEF65DC-57B4-467F-94D2-ED01D18CBF72">
      <xplan:schluessel>§2 Nr.17.3</xplan:schluessel>
      <xplan:text>Auf den Flächen zum Schutz, zur Pflege und zur Entwicklung von Boden, Natur und Landschaft gilt:
Auf den mit „G“ bezeichneten Flächen sind Gräben anzulegen und zu erhalten.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_096CCB49-A671-40E1-A5BE-BBE205AC911C">
      <xplan:schluessel>§2 Nr.17.4</xplan:schluessel>
      <xplan:text>Auf den Flächen zum Schutz, zur Pflege und zur Entwicklung von Boden, Natur und Landschaft gilt:
Die mit „FG“ bezeichneten Flächen sind als Extensives Feuchtgrünland mit Gräben zu entwickeln und zu erhalten.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_TextAbschnitt gml:id="Gml_FAE8F077-0048-4FD9-80A1-1A9EB61D20FD">
      <xplan:schluessel>§3</xplan:schluessel>
      <xplan:text>Das Ensemble Altengammer Hausdeich 40 bis 84 und 67 bis 85 ist nach § 6 Absätze 2 und 6 des Denkmalschutzgesetzes dem Schutz dieses Gesetzes unterstellt.</xplan:text>
    </xplan:XP_TextAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_BegruendungAbschnitt gml:id="Gml_386949A9-070F-4CA6-AF32-7827F5DD655F">
      <xplan:refText>
        <xplan:XP_ExterneReferenz>
          <xplan:beschreibung>Der Bebauungsplan BPlan002_4-1 für den Geltungsbereich beidseits entlang der Straßen Neuengammer Hausdeich und Altengammer Hausdeich vom Graben westlich von Altengammer Hausdeich 94 bis zum Hauptgraben an der Dove-Elbe auf der Höhe östlich von Altengammer Hausdeich 40 (Bezirk Bergedorf, Ortsteile 605 und 606) wird festgestellt.
Das Plangebiet wird wie folgt begrenzt:
Nordgrenzen der Flurstücke 2454, 3002, 2456 und 180, Ostgrenze des Flurstücks 182, Nordgrenze des Flurstücks 2803, über das Flurstück 2803, Westgrenze des Flurstücks 2464, über das Flurstück 2464, Nord- und Ostgrenze des Flurstücks 2465, Nordgrenzen der Flurstücke 2465 und 2466, über das Flurstück 1638, Nord- und Ostgrenze des Flurstücks 2805, Nord- und Ostgrenze des Flurstücks 2474, über das Flurstück 2883, Nord- und Ostgrenze des Flurstücks 2824, Ostgrenze des Flurstücks 2823 der Gemarkung Altengamme - über den Altengammer Hausdeich - Ostgrenze des Flurstücks 2484 der Gemarkung Altengamme - über die Dove-Elbe - Ostgrenze des Flurstücks 899 der Gemarkung Neuengamme - über den Neuengammer Hausdeich - Ostgrenzen der Flurstücke 4738 und 3535, Süd- und Westgrenzen des Flurstücks 3535, über die Flurstücke 3534, 1360 und 1074, Westgrenze der Flurstücke 1074 und 790, über die Flurstücke 789 und 779, Südgrenzen der Flurstücke 779, 4676, 3591 und 3371, über das Flurstück 776, Westgrenze der Flurstücke 776 und 3371, Süd- und Westgrenze des Flurstücks 3257 der Gemarkung Neuengamme - Neuengammer Hausdeich - West- und Nordgrenze des Flurstücks 3023 der Gemarkung Neuengamme - Dove- Elbe - Westgrenze der Flurstücke 2459, 2462, und 2463 der Gemarkung Altengamme - über den Altengammer Hausdeich - Westgrenze des Flurstücks 2454 der Gemarkung Altengamme.</xplan:beschreibung>
        </xplan:XP_ExterneReferenz>
      </xplan:refText>
    </xplan:XP_BegruendungAbschnitt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Bereich gml:id="Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582504.1297 5920140.662</gml:lowerCorner>
          <gml:upperCorner>583267.115 5921477.987</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:nummer>0</xplan:nummer>
      <xplan:geltungsbereich>
        <gml:Polygon gml:id="Gml_A3B2E2D5-55B3-4B52-BF5A-D2FFD18C540D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582830.741 5921381.091 582837.271 5921420.932 582840.326 5921441.47 
582843.262 5921452.327 582855.514 5921477.987 582844.732 5921471.354 
582838.561 5921467.936 582820.552 5921457.96 582819.954 5921457.662 
582816.339 5921455.864 582813.623 5921454.351 582801.232 5921447.453 
582798.414 5921445.377 582787.998 5921439.878 582780.876 5921436.12 
582762.736 5921427.772 582755.795 5921392.755 582750.198 5921363.295 
582741.031 5921314.162 582734.092 5921274.782 582730.279 5921255.158 
582723.937 5921221.981 582716.643 5921182.659 582714.201 5921167.851 
582710.816 5921143.032 582708.313 5921126.197 582706.178 5921113.368 
582702.531 5921093.707 582699.84 5921081.135 582700.126 5921073.048 
582699.564 5921071.821 582698.484 5921068.387 582694.708 5921053.648 
582691.226 5921039.05 582686.491 5921021.181 582686.248 5921019.688 
582686.004 5921018.197 582676.721 5920980.839 582666.036 5920941.464 
582665.182 5920938.87 582656.621 5920931.167 582655.135 5920933.48 
582652.232 5920934.846 582644.094 5920940.983 582635.806 5920946.985 
582631.529 5920952.542 582627.708 5920959.844 582624.132 5920971.378 
582615.203 5920982.16 582609.514 5920987.8 582598.9013 5920994.4924 
582594.7425 5920991.736 582592.9099 5920990.5214 582591.806 5920989.7463 
582589.762 5920988.311 582587.955 5920986.585 582584.417 5920986.127 
582578.841 5920986.65 582575.458 5920986.966 582564.846 5920989.077 
582554.287 5920992.388 582547.784 5920994.327 582540.537 5920995.956 
582536.819 5920996.027 582531.217 5920985.964 582523.038 5920975.41 
582510.836 5920961.679 582504.1297 5920953.3219 582516.846 5920921.204 
582518.869 5920916.096 582526.421 5920902.225 582527.141 5920902.581 
582530.539 5920898.147 582534.329 5920893.501 582538.204 5920888.931 
582542.184 5920884.447 582545.873 5920880.461 582546.937 5920879.313 
582557.738 5920869.946 582571.027 5920858.614 582603.81 5920830.584 
582607.003 5920827.854 582593.242 5920795.494 582580.859 5920766.377 
582590.463 5920762.037 582591.806 5920761.521 582595.304 5920760.179 
582593.784 5920757.303 582601.105 5920753.996 582606.638 5920750.438 
582601.501 5920735.991 582599.235 5920729.617 582598.5869 5920728.01 
582649.8483 5920707.3549 582652.0954 5920712.3774 582672.88 5920706.152 
582675.711 5920705.304 582677.365 5920710.188 582690.124 5920705.39 
582698.371 5920702.288 582705.539 5920699.976 582705.645 5920700.36 
582709.24 5920699.339 582708.949 5920698.381 582715.998 5920696.133 
582741.808 5920687.902 582747.9959 5920704.1842 582788.8143 5920686.7506 
582814.5564 5920675.7562 582827.3912 5920670.2744 582823.2849 5920662.241 
582854.7413 5920646.0293 582852.634 5920641.24 582833.67 5920593.696 
582822.401 5920564.804 582811.83 5920538.762 582785.648 5920474.258 
582767.648 5920429.038 582769.1614 5920428.6442 582733.849 5920342.8271 
582752.662 5920335.4463 582782.4258 5920321.9291 582817.1537 5920306.1575 
582834.9571 5920298.0721 582936.8122 5920251.8147 582926.69 5920225.943 
582901.444 5920163.122 582955.761 5920140.662 582995.814 5920241.768 
582997.852 5920246.911 583041.782 5920364.486 583041.95 5920364.945 
583042.258 5920365.789 583059.759 5920413.718 583064.217 5920420.589 
583080.998 5920446.868 583085.175 5920453.395 583089.821 5920459.909 
583104.063 5920478.028 583109.481 5920485.223 583120.546 5920502.245 
583136.98 5920526.743 583155.487 5920548.019 583157.6674 5920551.7079 
583160.41 5920556.348 583167.829 5920559.029 583176.133 5920564.471 
583179.422 5920566.912 583182.191 5920571.135 583188.57 5920611.997 
583190.278 5920622.931 583199.395 5920681.316 583202.118 5920696.869 
583211.448 5920745.352 583243.938 5920912.69 583255.331 5920970.157 
583255.757 5920972.311 583266.641 5921031.183 583267.115 5921033.686 
583230.027 5921040.712 583154.6745 5921055.9394 583178.551 5921181.086 
583190.903 5921243.847 583116.633 5921259.836 583127.76 5921317.799 
583078.016 5921329.358 583051.9814 5921335.3938 583016.4548 5921343.6257 
583006.731 5921293.264 582995.8553 5921237.6242 582976.043 5921243.173 
582973.404 5921243.912 582949.949 5921250.944 582956.954 5921297.447 
582913.421 5921305.128 582894.3956 5921308.2654 582893.99 5921305.588 
582888.696 5921275.205 582888.4641 5921274.0727 582869.357 5921272.848 
582845.382 5921270.747 582809.426 5921268.882 582830.741 5921381.091 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:geltungsbereich>
      <xplan:nachrichtlich xlink:href="#Gml_7CDC3589-D0B0-4A24-8636-D4F72F06F86C" />
      <xplan:nachrichtlich xlink:href="#Gml_4232C0D7-B164-4371-B042-2C578259000D" />
      <xplan:nachrichtlich xlink:href="#Gml_C676CEBD-9AE3-4307-B14A-C5C074760AE4" />
      <xplan:nachrichtlich xlink:href="#Gml_C3D9B659-F00A-4D6A-8DAF-711787F9DED8" />
      <xplan:nachrichtlich xlink:href="#Gml_6E7FD6BC-CB53-4785-AE8C-3769A814FF4B" />
      <xplan:nachrichtlich xlink:href="#Gml_60650F3D-E6B3-4A83-B909-623BC489DF34" />
      <xplan:nachrichtlich xlink:href="#Gml_C777D19A-BDFC-4C15-8339-6A65654C58F6" />
      <xplan:nachrichtlich xlink:href="#Gml_0BB9D2D4-7432-43EF-A7CF-2B33081B1F85" />
      <xplan:nachrichtlich xlink:href="#Gml_A44FD3DF-FA35-474D-8BC7-9CCD4E032F0F" />
      <xplan:nachrichtlich xlink:href="#Gml_4404C3BB-114A-440E-806F-071545E41456" />
      <xplan:nachrichtlich xlink:href="#Gml_847BC031-F811-43CF-97A3-5047EA8286EE" />
      <xplan:nachrichtlich xlink:href="#Gml_0D40F9A5-70FA-4D1B-9405-A5295E478DED" />
      <xplan:nachrichtlich xlink:href="#Gml_2BA51C00-88CF-4E3A-A9B8-E54AC5D0E13E" />
      <xplan:nachrichtlich xlink:href="#Gml_AB977C79-545B-4CCF-B8EB-ECE3EE83A96B" />
      <xplan:nachrichtlich xlink:href="#Gml_E7C38BB7-E30C-4891-9E6F-9FE6EA95F566" />
      <xplan:nachrichtlich xlink:href="#Gml_EECAF159-EF2B-4672-BE4E-A6D740370C11" />
      <xplan:nachrichtlich xlink:href="#Gml_9420EF39-C7CB-49A5-A360-5B036DF347A7" />
      <xplan:nachrichtlich xlink:href="#Gml_A99ECD9B-C083-40D9-A656-0DCAC3D8BE3E" />
      <xplan:nachrichtlich xlink:href="#Gml_1670ABBF-6E11-413F-845E-96C6DD5E6560" />
      <xplan:nachrichtlich xlink:href="#Gml_C41851F4-3FBB-48DC-AD78-E07660AB30D1" />
      <xplan:nachrichtlich xlink:href="#Gml_38DA9DD7-2BE2-437F-9160-7CF850B54C9D" />
      <xplan:nachrichtlich xlink:href="#Gml_266239ED-17A3-4396-8C72-54BF8E49B9EC" />
      <xplan:nachrichtlich xlink:href="#Gml_13A00ED1-E9CB-4C89-A60F-AF76B2706544" />
      <xplan:nachrichtlich xlink:href="#Gml_739BF3E0-FBF5-4D1D-BCED-8C7D1AFEF27B" />
      <xplan:nachrichtlich xlink:href="#Gml_241B55EA-483E-49C4-8739-6C2D72AD3039" />
      <xplan:nachrichtlich xlink:href="#Gml_C83DEF03-89B1-498A-9BC3-0100806E43A5" />
      <xplan:nachrichtlich xlink:href="#Gml_298508B3-18B3-4D85-ADE5-61590AFA97A2" />
      <xplan:nachrichtlich xlink:href="#Gml_B01EA6A6-1580-4933-B961-BFA3791DCA29" />
      <xplan:nachrichtlich xlink:href="#Gml_2CE98EB0-E21D-44C4-A130-F420ADDDBD0F" />
      <xplan:nachrichtlich xlink:href="#Gml_098DE625-DDCE-4758-AD4F-3141CCA1A22E" />
      <xplan:nachrichtlich xlink:href="#Gml_BB01FCB5-2F1B-4B64-A170-8B3A9D62828D" />
      <xplan:nachrichtlich xlink:href="#Gml_EF7DFEDE-8E9B-4E17-A809-50787F26F4C2" />
      <xplan:nachrichtlich xlink:href="#Gml_9FCD43EB-B623-468E-8F9A-48588730BCBD" />
      <xplan:nachrichtlich xlink:href="#Gml_9406ED89-6C48-41A9-B349-CC39AE5877DB" />
      <xplan:nachrichtlich xlink:href="#Gml_FF9248E4-3C00-4316-8F74-B79C7C4B7B38" />
      <xplan:nachrichtlich xlink:href="#Gml_F41A1ABC-DA78-4037-987A-1BDC1FFAB971" />
      <xplan:nachrichtlich xlink:href="#Gml_1A0BBFA2-505D-4E1C-A051-895B5B193B06" />
      <xplan:nachrichtlich xlink:href="#Gml_2BB1683F-FEFB-4717-89A4-E4C798ECBD4D" />
      <xplan:nachrichtlich xlink:href="#Gml_329548F3-FCE5-4654-837C-1017B9DA124E" />
      <xplan:nachrichtlich xlink:href="#Gml_B1CA608C-0B38-42AF-93DA-1BDA347E2E09" />
      <xplan:nachrichtlich xlink:href="#Gml_04BFBB16-7C42-4502-8FDA-E299497D7724" />
      <xplan:nachrichtlich xlink:href="#Gml_AC0FC1B1-6569-4312-AF86-67B24D4224B7" />
      <xplan:nachrichtlich xlink:href="#Gml_F7E9CF25-352F-4359-B1F8-FAC9CBF80326" />
      <xplan:nachrichtlich xlink:href="#Gml_1B094C04-F342-4AE5-BF96-5DB167E99EAE" />
      <xplan:nachrichtlich xlink:href="#Gml_6FF7FDDE-AB91-4E2A-A3B2-69FBAAF9A989" />
      <xplan:nachrichtlich xlink:href="#Gml_C1DD0303-38D1-4427-AFF3-A49C887BCCFF" />
      <xplan:nachrichtlich xlink:href="#Gml_355A8E0D-C66D-4A1E-B66B-C3A5517CE449" />
      <xplan:nachrichtlich xlink:href="#Gml_278FC9E7-1292-494F-9086-1E1C9907C9E0" />
      <xplan:nachrichtlich xlink:href="#Gml_2050326C-F95C-4842-9CC6-9245790942B4" />
      <xplan:nachrichtlich xlink:href="#Gml_9C53D967-10B0-4A4A-A98F-F9D1BFC6B710" />
      <xplan:nachrichtlich xlink:href="#Gml_DE2E2B52-0C2B-4F74-90EE-F8065E5A44A2" />
      <xplan:nachrichtlich xlink:href="#Gml_45F948CB-9075-42A1-9FBF-D2C5B488DF36" />
      <xplan:nachrichtlich xlink:href="#Gml_CCB1BA79-3B07-4168-98E9-B8295DFC45E7" />
      <xplan:nachrichtlich xlink:href="#Gml_BF555C69-FFA9-46F8-9A7C-A05CD7996C3C" />
      <xplan:nachrichtlich xlink:href="#Gml_D3D9693F-4C81-41F7-8542-188B40467DF4" />
      <xplan:nachrichtlich xlink:href="#Gml_A6C66B2A-49EA-4597-A02D-3026B761D3A6" />
      <xplan:nachrichtlich xlink:href="#Gml_90CEF720-3FEB-47E2-98C3-F599A4D5CFF1" />
      <xplan:nachrichtlich xlink:href="#Gml_BFCEA877-5FD3-421D-AF2A-7C6AC1A2260F" />
      <xplan:nachrichtlich xlink:href="#Gml_0F7C092B-1C82-4EED-9B49-44C078B9AE74" />
      <xplan:nachrichtlich xlink:href="#Gml_5E0DA474-6775-4641-9879-F2D78FB27D06" />
      <xplan:nachrichtlich xlink:href="#Gml_9F96CEB1-DADC-4BD9-AD55-D95CD6CF4EDC" />
      <xplan:nachrichtlich xlink:href="#Gml_065B82F9-797E-47E1-901A-C0053BBA701D" />
      <xplan:rasterBasis xlink:href="#Gml_F4923FF6-A593-4F4F-8023-797D17967B53" />
      <xplan:versionBauNVO>4000</xplan:versionBauNVO>
      <xplan:gehoertZuPlan xlink:href="#Gml_EBB6FF42-D04B-4807-ADDE-0701B82C96CB" />
      <xplan:inhaltBPlan xlink:href="#Gml_7F656595-9DC3-4FF2-9935-6355A61DEFA4" />
      <xplan:inhaltBPlan xlink:href="#Gml_3BF0B945-AAB5-42C2-B83A-3DB529F1FB6C" />
      <xplan:inhaltBPlan xlink:href="#Gml_1F01CE68-54FB-4651-989D-1D78C685CEDE" />
      <xplan:inhaltBPlan xlink:href="#Gml_03F80BE8-0C00-4222-9175-29A1A210DF11" />
      <xplan:inhaltBPlan xlink:href="#Gml_C1913AE5-87B3-4BE6-A440-B317DEA26A1B" />
      <xplan:inhaltBPlan xlink:href="#Gml_AEE988A1-DB3C-4563-BC54-EFC14206897F" />
      <xplan:inhaltBPlan xlink:href="#Gml_0039AEA5-B351-48DC-B073-7338D4A08203" />
      <xplan:inhaltBPlan xlink:href="#Gml_932150EF-C4C1-4C4C-9DA4-C426CE0DFE6E" />
      <xplan:inhaltBPlan xlink:href="#Gml_12BEDFB9-7CF8-460A-B431-7764FC67CA24" />
      <xplan:inhaltBPlan xlink:href="#Gml_34E03C07-AEE0-4436-B814-71FAC7EA6E47" />
      <xplan:inhaltBPlan xlink:href="#Gml_88ED56F3-F505-4EA9-8C77-C74290435620" />
      <xplan:inhaltBPlan xlink:href="#Gml_7C1704A6-AE05-4100-9DBA-D26AB392E3D7" />
      <xplan:inhaltBPlan xlink:href="#Gml_F76D205C-6B3E-4E63-9EFF-C5A0F77A576F" />
      <xplan:inhaltBPlan xlink:href="#Gml_A4EE6257-AEA2-4109-A4E2-6107565283F2" />
      <xplan:inhaltBPlan xlink:href="#Gml_ABF39B2B-24D1-4A5F-91AE-1E6F53DA195C" />
      <xplan:inhaltBPlan xlink:href="#Gml_5A2B61D3-7F7F-4446-AC2F-0D11F6B643F9" />
      <xplan:inhaltBPlan xlink:href="#Gml_6556E1FA-8A8B-4CCB-89E7-C0584C036AF0" />
      <xplan:inhaltBPlan xlink:href="#Gml_CDD16986-C3CB-4089-8F59-73EDFE62E932" />
      <xplan:inhaltBPlan xlink:href="#Gml_44F52906-86C6-4AD0-829F-26E6AA1EF019" />
      <xplan:inhaltBPlan xlink:href="#Gml_A660A02F-F4B6-45EE-820B-AB25D62A8C6A" />
      <xplan:inhaltBPlan xlink:href="#Gml_78C7B883-9581-4F87-A09C-947D9EBB120D" />
      <xplan:inhaltBPlan xlink:href="#Gml_8D370490-9C82-4E1C-99E0-CA65D64F317F" />
      <xplan:inhaltBPlan xlink:href="#Gml_732DAF27-1F40-438C-AD90-C25DBCF40B98" />
      <xplan:inhaltBPlan xlink:href="#Gml_248BA7AD-524F-411E-92A0-9B3673A77CF3" />
      <xplan:inhaltBPlan xlink:href="#Gml_47D549FF-1B04-4897-BEAE-211A1B1DD026" />
      <xplan:inhaltBPlan xlink:href="#Gml_E93822BC-6528-492C-B2ED-FCF8F70D2A0F" />
      <xplan:inhaltBPlan xlink:href="#Gml_73823297-65B1-4537-A27A-84861B7934E1" />
      <xplan:inhaltBPlan xlink:href="#Gml_1B2FE4EA-C636-4539-9152-3357ECB6DEF1" />
      <xplan:inhaltBPlan xlink:href="#Gml_51CB47AC-0BCC-46A8-A414-4CA7EAD99B62" />
      <xplan:inhaltBPlan xlink:href="#Gml_8A56A418-EEA1-4520-94ED-E295008F861F" />
      <xplan:inhaltBPlan xlink:href="#Gml_819E8C4A-AF97-49EC-A0B6-F413A5195E5B" />
      <xplan:inhaltBPlan xlink:href="#Gml_9997F827-0127-4F01-A923-AAD435A8F349" />
      <xplan:inhaltBPlan xlink:href="#Gml_C9F8753C-C5F6-4AC1-9369-4B7A7F1B65A7" />
      <xplan:inhaltBPlan xlink:href="#Gml_E013B261-7E28-432E-85DD-CCECB9AA4A3B" />
      <xplan:inhaltBPlan xlink:href="#Gml_3B7FF1BB-BD16-4F7B-9E71-BA91DA33BDB8" />
      <xplan:inhaltBPlan xlink:href="#Gml_C10C0644-9E98-47FF-927E-DE8B2C5EB36C" />
      <xplan:inhaltBPlan xlink:href="#Gml_2F3DDB5A-48F1-44D7-893E-CF754550336A" />
      <xplan:inhaltBPlan xlink:href="#Gml_0C68A1A6-4372-4634-AF27-095A920B637A" />
      <xplan:inhaltBPlan xlink:href="#Gml_4150523E-910C-42E7-ADA3-F58C7417FE93" />
      <xplan:inhaltBPlan xlink:href="#Gml_C5C9158D-8A5D-4303-8CA6-2060B48ABC29" />
      <xplan:inhaltBPlan xlink:href="#Gml_7FDE3A32-C91A-45EA-9492-57AA240677CB" />
      <xplan:inhaltBPlan xlink:href="#Gml_A77FDE91-83E7-4EF1-BD95-62D21459C322" />
      <xplan:inhaltBPlan xlink:href="#Gml_4E5CCB44-4116-4F49-B96B-46B917F12F56" />
      <xplan:inhaltBPlan xlink:href="#Gml_C9110F3F-73C1-43D0-9A0F-A7D94F659FDC" />
      <xplan:inhaltBPlan xlink:href="#Gml_8BDCCB74-D08A-4283-BB47-872FDB7045FF" />
      <xplan:inhaltBPlan xlink:href="#Gml_0BA48C00-C8FA-4A5C-8F90-E1F687113220" />
      <xplan:inhaltBPlan xlink:href="#Gml_F8CD6A6C-484A-493A-A906-7B7245EBADB6" />
      <xplan:inhaltBPlan xlink:href="#Gml_AB54C146-1C38-44C1-B340-E48300867A4E" />
      <xplan:inhaltBPlan xlink:href="#Gml_37FE2FBA-8620-4B5E-AD12-E3FB484BC510" />
      <xplan:inhaltBPlan xlink:href="#Gml_B606F979-4736-4983-82E8-4FD4796C0C05" />
      <xplan:inhaltBPlan xlink:href="#Gml_82E500D7-C44F-4D81-A8FD-009844AB5668" />
      <xplan:inhaltBPlan xlink:href="#Gml_885703C9-1C57-413B-8805-B8C94C8F5B1E" />
      <xplan:inhaltBPlan xlink:href="#Gml_ED504845-CE02-43DB-B5CF-5820855956D0" />
      <xplan:inhaltBPlan xlink:href="#Gml_118DE24C-1F13-405C-AB02-DE1746A5AC98" />
      <xplan:inhaltBPlan xlink:href="#Gml_949678C3-8E62-466D-950F-C67FB568D6B7" />
      <xplan:inhaltBPlan xlink:href="#Gml_787E188A-A9A8-41B2-B28A-B8E20CAEA6EC" />
      <xplan:inhaltBPlan xlink:href="#Gml_86A56640-99D4-44B6-ACF1-C9D70A28B8C0" />
      <xplan:inhaltBPlan xlink:href="#Gml_61E482EA-DBAC-4434-A977-770629F1D774" />
      <xplan:inhaltBPlan xlink:href="#Gml_1C52C00D-28DA-4418-873A-59054DA3C4FD" />
      <xplan:inhaltBPlan xlink:href="#Gml_BCD2D953-B159-4F16-A502-0D87FE80E5A4" />
      <xplan:inhaltBPlan xlink:href="#Gml_43B4BEC4-EAB2-4484-AC5A-4B91E49AA8CF" />
      <xplan:inhaltBPlan xlink:href="#Gml_BD28730B-DB9C-4CFC-821E-660C1E88E7F7" />
      <xplan:inhaltBPlan xlink:href="#Gml_AC04E845-F802-4A80-BB85-0BB9CFE8BFA1" />
      <xplan:inhaltBPlan xlink:href="#Gml_49F651E3-0519-4217-B5CC-EA24B996E192" />
      <xplan:inhaltBPlan xlink:href="#Gml_B80A3470-9211-4DCD-89F3-FF408BFBD33E" />
      <xplan:inhaltBPlan xlink:href="#Gml_58F4716B-7056-4937-8EF0-5960884912CF" />
      <xplan:inhaltBPlan xlink:href="#Gml_C7E4BB56-8C13-47F2-A6D1-4C8228D23206" />
      <xplan:inhaltBPlan xlink:href="#Gml_9B73FA32-609F-4083-95E8-77A931922BE9" />
      <xplan:inhaltBPlan xlink:href="#Gml_E3200FFE-CFFF-4EF4-A34C-30F1E540A6EC" />
      <xplan:inhaltBPlan xlink:href="#Gml_F786EB17-8F70-4E59-B168-18D6BE864BC8" />
      <xplan:inhaltBPlan xlink:href="#Gml_99185F97-C72B-4C41-A503-921165760BA7" />
      <xplan:inhaltBPlan xlink:href="#Gml_1864745D-0331-4FA0-AB8F-E189363261AC" />
      <xplan:inhaltBPlan xlink:href="#Gml_67D5211A-5AD6-40DE-ACD7-5DF12927C0F7" />
      <xplan:inhaltBPlan xlink:href="#Gml_4932D072-3051-4C1F-AB00-A53D586351B3" />
      <xplan:inhaltBPlan xlink:href="#Gml_292A2607-CE68-4446-8FC2-3A1EA23A6166" />
      <xplan:inhaltBPlan xlink:href="#Gml_D3561EDC-7ADD-498D-8858-F8E59600B523" />
      <xplan:inhaltBPlan xlink:href="#Gml_492D9730-A0F3-489A-8E6E-7DFCD669C8A8" />
      <xplan:inhaltBPlan xlink:href="#Gml_AE84CEA5-78ED-45C9-97A2-1762527D07E5" />
      <xplan:inhaltBPlan xlink:href="#Gml_EB61E825-9A93-4099-A7D6-7E71E0B06FE4" />
      <xplan:inhaltBPlan xlink:href="#Gml_383FC3C0-B475-405A-8772-443CD79823D7" />
      <xplan:inhaltBPlan xlink:href="#Gml_FE8DC829-DD49-4BF7-BA7E-D3B7D7955A83" />
      <xplan:inhaltBPlan xlink:href="#Gml_484E9CBC-B902-4744-A365-0449B95CD9B7" />
      <xplan:inhaltBPlan xlink:href="#Gml_7A05B7F5-7EAD-40E7-893D-97E09DC65592" />
      <xplan:inhaltBPlan xlink:href="#Gml_8090BDC0-67BD-42C1-B03E-97F436C30015" />
      <xplan:inhaltBPlan xlink:href="#Gml_CB652C63-3402-4BDE-A74C-71B559E49DA3" />
      <xplan:inhaltBPlan xlink:href="#Gml_D3494573-2816-4EF1-89F4-5C60DD80BF14" />
      <xplan:inhaltBPlan xlink:href="#Gml_80D81B91-51E8-4A99-8A08-9DD15596C804" />
      <xplan:inhaltBPlan xlink:href="#Gml_ADA120DB-63D1-4F64-A78B-3DD86886442D" />
      <xplan:inhaltBPlan xlink:href="#Gml_D9DC048D-E2ED-40F0-8B9D-A7654D8D0887" />
      <xplan:inhaltBPlan xlink:href="#Gml_A3366757-B189-4B02-A2B3-9D77C83E6E71" />
      <xplan:inhaltBPlan xlink:href="#Gml_23C66647-D7C0-43E8-ACDD-73523A64CE85" />
      <xplan:inhaltBPlan xlink:href="#Gml_872422DB-293D-4289-9099-47E947427CE2" />
      <xplan:inhaltBPlan xlink:href="#Gml_BCDA5A71-5FB7-44D4-8ACD-B476DCE4F466" />
      <xplan:inhaltBPlan xlink:href="#Gml_F6D32183-3815-413B-BD9A-2641C2FB66A3" />
      <xplan:inhaltBPlan xlink:href="#Gml_0701EDAC-C5FE-48D4-8753-29732C7C06FB" />
      <xplan:inhaltBPlan xlink:href="#Gml_7D7AF5B5-9721-4CF5-A9D0-563707E9D12F" />
      <xplan:inhaltBPlan xlink:href="#Gml_C5A0ACCF-437E-4CA0-8697-EF9D08AC7F07" />
      <xplan:inhaltBPlan xlink:href="#Gml_3E86C0F2-6796-4B74-86F0-0C31BDC86B85" />
      <xplan:inhaltBPlan xlink:href="#Gml_3CB511B1-507F-4996-BAFA-7B0CB44699C4" />
      <xplan:inhaltBPlan xlink:href="#Gml_5F29B674-7C79-461A-AEDA-26E4A9ED8634" />
      <xplan:inhaltBPlan xlink:href="#Gml_20138AB3-A17D-4713-A87B-6B984D0386F9" />
      <xplan:inhaltBPlan xlink:href="#Gml_AA360C07-A8DB-4F23-9B93-8BF51BC1E714" />
      <xplan:inhaltBPlan xlink:href="#Gml_1202D959-E965-43E8-A9C0-274BAD07C0AD" />
      <xplan:inhaltBPlan xlink:href="#Gml_AF5BE1E1-D638-480D-9FCD-D3F620CE26D7" />
      <xplan:inhaltBPlan xlink:href="#Gml_15C4A4D7-47C9-4B54-B35F-DCAD781414F5" />
      <xplan:inhaltBPlan xlink:href="#Gml_DDBD348C-F587-4BBC-A741-0618D20A27F7" />
      <xplan:inhaltBPlan xlink:href="#Gml_69E4F920-D1C8-41AD-8D9C-F3008DA32FBA" />
      <xplan:inhaltBPlan xlink:href="#Gml_2F71C6F6-7FF5-486B-928A-10638EBE283C" />
      <xplan:inhaltBPlan xlink:href="#Gml_F6CA0C26-EA14-430F-A547-EE06CDAFD398" />
      <xplan:inhaltBPlan xlink:href="#Gml_FB489797-1F58-434F-A245-86F10E2315DA" />
      <xplan:inhaltBPlan xlink:href="#Gml_0D64139C-8F97-413B-97E3-0F3DF7F0A4E8" />
      <xplan:inhaltBPlan xlink:href="#Gml_DFFF5B72-C41A-4523-8578-6C1C2F797F61" />
      <xplan:inhaltBPlan xlink:href="#Gml_499F7BB0-C9D7-479B-8A8C-FF97728592FC" />
      <xplan:inhaltBPlan xlink:href="#Gml_00F95776-A5C1-4B06-8B60-795FA42AF7E4" />
      <xplan:inhaltBPlan xlink:href="#Gml_C8CCF2DA-FAAF-4F42-A59C-653B935FEF57" />
      <xplan:inhaltBPlan xlink:href="#Gml_839A71BA-BC16-46A7-A24A-D518F51163CA" />
      <xplan:inhaltBPlan xlink:href="#Gml_CABD50EE-B47A-4D8A-82A7-FF6A6661920A" />
      <xplan:inhaltBPlan xlink:href="#Gml_A3275B07-D68A-472C-99E0-834E33200F04" />
      <xplan:inhaltBPlan xlink:href="#Gml_2EDC9AE7-69F2-4EFA-8ABC-57A76B40DD6E" />
      <xplan:inhaltBPlan xlink:href="#Gml_65E42267-695C-4A8C-BD4A-BC059A74B998" />
      <xplan:inhaltBPlan xlink:href="#Gml_33FA752A-0A03-424B-B6A5-B80E614830AD" />
      <xplan:inhaltBPlan xlink:href="#Gml_8928C350-D995-47B7-B27D-1A54995BA76A" />
      <xplan:inhaltBPlan xlink:href="#Gml_7B929873-985A-4C34-B96B-ACD7937F5C17" />
      <xplan:inhaltBPlan xlink:href="#Gml_6891A9A3-9183-456D-A601-79F6FDB3BF03" />
      <xplan:inhaltBPlan xlink:href="#Gml_443B9F63-69ED-4D88-888C-744D472F3DE2" />
      <xplan:inhaltBPlan xlink:href="#Gml_D41B9F10-9D96-4AE8-A4E3-6386E1C0E00C" />
      <xplan:inhaltBPlan xlink:href="#Gml_20FAC8A4-7BF0-4D61-A914-59F1E965C850" />
      <xplan:inhaltBPlan xlink:href="#Gml_A6C5C021-DA89-4A34-94DA-461BF87B4066" />
      <xplan:inhaltBPlan xlink:href="#Gml_730F17CC-8FCD-4218-8E86-A6311B750941" />
      <xplan:inhaltBPlan xlink:href="#Gml_B55E80C9-49C9-4A7B-9243-9BC79AA21A88" />
      <xplan:inhaltBPlan xlink:href="#Gml_DB7A3F51-9E0D-4BA2-B971-12FCD7151C8C" />
      <xplan:inhaltBPlan xlink:href="#Gml_935647A4-F8CB-4298-9C48-7DE6ED962C9A" />
      <xplan:inhaltBPlan xlink:href="#Gml_5C3E276E-20B4-4748-B376-8F75072E17F3" />
      <xplan:inhaltBPlan xlink:href="#Gml_5B6CF5A2-C73F-4EFE-9DED-60872A665E7D" />
      <xplan:inhaltBPlan xlink:href="#Gml_F32EAC8D-748C-4D4B-B13E-F578ED6F9E01" />
      <xplan:inhaltBPlan xlink:href="#Gml_247929DF-F54C-4AE0-BA3E-2A4FDBF296B8" />
      <xplan:inhaltBPlan xlink:href="#Gml_C5D319CF-6058-424E-9272-76C0F6D8E6CE" />
      <xplan:inhaltBPlan xlink:href="#Gml_50318B8F-BAF9-4206-83F4-BFD3AE199A04" />
      <xplan:inhaltBPlan xlink:href="#Gml_9D20DFC9-BF9F-49D7-8324-91EA37837768" />
      <xplan:inhaltBPlan xlink:href="#Gml_9D48561F-1084-4CCC-9319-D5C323155433" />
      <xplan:inhaltBPlan xlink:href="#Gml_4F23A2A7-EB06-4494-8FC4-B1D855C85637" />
      <xplan:inhaltBPlan xlink:href="#Gml_82967860-B0D6-41FA-AFEE-28745CE77742" />
      <xplan:inhaltBPlan xlink:href="#Gml_0568A964-04AF-45B7-AA1C-4EA2053C56A1" />
      <xplan:inhaltBPlan xlink:href="#Gml_43B112E2-1285-497C-B3AB-0AC735AA5AB1" />
      <xplan:inhaltBPlan xlink:href="#Gml_D3D672D6-2C8F-4FBC-B6EE-FD40D4E662AA" />
      <xplan:inhaltBPlan xlink:href="#Gml_EE8E3818-BE6F-4FB2-AF7F-83E68B73D548" />
      <xplan:inhaltBPlan xlink:href="#Gml_771A8D80-8C5F-46CE-B7EB-32843E6A828C" />
      <xplan:inhaltBPlan xlink:href="#Gml_C3046567-4CEF-4F27-97FC-5CDF5A734A12" />
      <xplan:inhaltBPlan xlink:href="#Gml_C1FBA8D7-EA9D-415B-BEE0-BBBD6B8874CE" />
      <xplan:inhaltBPlan xlink:href="#Gml_4E5F13CC-971D-4F14-B68F-C6AAE79294B3" />
      <xplan:inhaltBPlan xlink:href="#Gml_45F0A9C4-FC6A-4B13-ADB7-404F1E3054A9" />
      <xplan:inhaltBPlan xlink:href="#Gml_C5CB2CD7-0B5F-4EB5-9FAD-B83FF79674A9" />
      <xplan:inhaltBPlan xlink:href="#Gml_F2AF656F-59CE-46D5-93EF-5AD944019F67" />
      <xplan:inhaltBPlan xlink:href="#Gml_D38B84DB-B16F-4BC4-ADCE-D54979E7EBE6" />
      <xplan:inhaltBPlan xlink:href="#Gml_4D11765A-CDBE-40A5-B734-DD67CC95135B" />
      <xplan:inhaltBPlan xlink:href="#Gml_D60CEC04-1037-46B7-AD79-D63C754085A2" />
      <xplan:inhaltBPlan xlink:href="#Gml_3229D559-5EC0-4F64-98F7-D28A59D92898" />
      <xplan:inhaltBPlan xlink:href="#Gml_09997A6F-1559-4C60-8A83-3AE0FDFAA6A9" />
      <xplan:inhaltBPlan xlink:href="#Gml_BCD88296-9B74-4F87-B8C9-23FB2C6D6A4A" />
      <xplan:inhaltBPlan xlink:href="#Gml_38B45EF1-F862-4ABD-941D-2518963E8FF6" />
      <xplan:inhaltBPlan xlink:href="#Gml_A33B904F-F9A1-4258-98EC-37FA7AFAABEA" />
      <xplan:inhaltBPlan xlink:href="#Gml_34726489-D1C2-4B91-A17C-04CD805B580F" />
      <xplan:inhaltBPlan xlink:href="#Gml_5A918DCF-8FC1-4C40-BF68-2B61A25253E5" />
      <xplan:inhaltBPlan xlink:href="#Gml_D5EDCF78-53E0-4E39-96B7-F57C1FA18404" />
      <xplan:inhaltBPlan xlink:href="#Gml_AC5E0518-D64A-4E65-B30B-7103063638E9" />
      <xplan:inhaltBPlan xlink:href="#Gml_290464CA-1182-4D33-A801-79920343C505" />
      <xplan:inhaltBPlan xlink:href="#Gml_2A69D419-CE9A-47C3-A11E-DF3BBC1481BF" />
      <xplan:inhaltBPlan xlink:href="#Gml_B2DA34CF-47BB-407F-9A8E-80D7541D23F4" />
      <xplan:inhaltBPlan xlink:href="#Gml_A4F7E8AE-D986-4FD4-B496-74BDF3832EA2" />
      <xplan:inhaltBPlan xlink:href="#Gml_AECF2D18-7B38-41C1-A90E-042A51921CE7" />
      <xplan:inhaltBPlan xlink:href="#Gml_2063EBA8-5375-4C27-A306-3005D6344910" />
      <xplan:inhaltBPlan xlink:href="#Gml_9F2C5ABE-06CD-4876-B70B-5DABD3EC0B22" />
      <xplan:inhaltBPlan xlink:href="#Gml_3572422C-1B7C-4465-8C22-C7B2613C9BCF" />
      <xplan:inhaltBPlan xlink:href="#Gml_9F6F70EE-F698-49CF-B06C-E432317CBD73" />
      <xplan:inhaltBPlan xlink:href="#Gml_723681A8-5C2F-4AC9-B227-8B15F19E7C2D" />
      <xplan:inhaltBPlan xlink:href="#Gml_A6B2E664-983D-4295-B84A-5F480B308CC1" />
      <xplan:inhaltBPlan xlink:href="#Gml_36CB206F-7241-43E9-8BA1-3F87B6C8725A" />
      <xplan:inhaltBPlan xlink:href="#Gml_EA5287F8-B212-4DE1-92CE-B07C6D010CEE" />
      <xplan:inhaltBPlan xlink:href="#Gml_27E5766B-AB15-441B-A3EE-E4214537C12A" />
      <xplan:inhaltBPlan xlink:href="#Gml_FC9DC0BB-8EA8-4BB8-9272-758FBAF6D531" />
      <xplan:inhaltBPlan xlink:href="#Gml_BD3B8DE3-87F7-4E1E-BD14-5350ECB972BF" />
      <xplan:inhaltBPlan xlink:href="#Gml_E273153E-4724-4D63-A034-194B214E0AC1" />
      <xplan:inhaltBPlan xlink:href="#Gml_CC3FB500-0008-4EAB-90CC-CD469CA2466D" />
      <xplan:inhaltBPlan xlink:href="#Gml_EA66B65E-C7BB-49E0-A828-EFCDB9F135AC" />
      <xplan:inhaltBPlan xlink:href="#Gml_1355442B-6878-47A1-9780-4EB28C36DB8A" />
      <xplan:inhaltBPlan xlink:href="#Gml_8680EC34-AD6C-4FC3-8CF1-F6C638E4409D" />
      <xplan:inhaltBPlan xlink:href="#Gml_E04E3CBB-C1E2-4491-B474-4EA5801CB0D4" />
      <xplan:inhaltBPlan xlink:href="#Gml_5CEF6008-0959-4781-954A-6A3319343827" />
      <xplan:inhaltBPlan xlink:href="#Gml_3C288F10-4131-44E4-AD38-767F43E853CC" />
      <xplan:inhaltBPlan xlink:href="#Gml_2D2B0D63-B699-414C-ACF9-21EBA99BC31B" />
      <xplan:inhaltBPlan xlink:href="#Gml_B622746D-0C64-4BC4-85B1-6F355447E6C6" />
      <xplan:inhaltBPlan xlink:href="#Gml_E331EFA0-6F91-4E54-B7B0-6390555A4ADC" />
      <xplan:inhaltBPlan xlink:href="#Gml_142A537A-38D4-45DF-8A0A-8CFE33444711" />
      <xplan:inhaltBPlan xlink:href="#Gml_96477643-84F7-4864-B605-E59753BEDA1A" />
      <xplan:inhaltBPlan xlink:href="#Gml_F31C7BE3-83D0-49F4-BAAA-5881A0500743" />
      <xplan:inhaltBPlan xlink:href="#Gml_75B1933E-1FA4-4737-9A4A-1647D77CF25A" />
      <xplan:inhaltBPlan xlink:href="#Gml_A20495D1-89CA-4B5F-9D4A-171C6FCDBD49" />
      <xplan:inhaltBPlan xlink:href="#Gml_C7CB8952-BFD7-43F3-AAAE-E62EFDA317C6" />
      <xplan:inhaltBPlan xlink:href="#Gml_7405A9C0-B4F6-4054-AAA0-DBD51C3E6E5F" />
      <xplan:inhaltBPlan xlink:href="#Gml_47E34855-150B-49A8-A677-5C9B152EB829" />
      <xplan:inhaltBPlan xlink:href="#Gml_54A97277-8BD6-4E22-90DF-FEE741836B5E" />
      <xplan:inhaltBPlan xlink:href="#Gml_7F17E171-2C90-4B0D-AB8C-71B8E6FE5E9C" />
      <xplan:inhaltBPlan xlink:href="#Gml_A7EA2BFC-90D8-487D-8C30-B75AEDC1503A" />
      <xplan:inhaltBPlan xlink:href="#Gml_78F821F9-27BD-49CF-AE2E-0AFB91F661CB" />
      <xplan:inhaltBPlan xlink:href="#Gml_3A12C489-7F68-41EB-88DA-E93953D1481E" />
      <xplan:inhaltBPlan xlink:href="#Gml_3D817FEA-A099-4FAB-B5E1-754CFE64CF94" />
      <xplan:inhaltBPlan xlink:href="#Gml_909217E9-43FE-4080-9E2E-65F8D99BBD55" />
      <xplan:inhaltBPlan xlink:href="#Gml_F53E7952-6F69-41DF-A5FF-E6866D070D33" />
      <xplan:inhaltBPlan xlink:href="#Gml_9FDF477E-EA26-4119-9EC4-6EBF7CCB17AE" />
      <xplan:inhaltBPlan xlink:href="#Gml_A04F7BC8-0A03-43FE-801B-D53A4AB7FF47" />
      <xplan:inhaltBPlan xlink:href="#Gml_8FF6F7E0-ECD9-45B1-ABE8-7B1C6DC3350E" />
      <xplan:inhaltBPlan xlink:href="#Gml_960B7221-25CC-4AD4-8678-05C5A4AEF074" />
      <xplan:inhaltBPlan xlink:href="#Gml_2956B184-2C34-4E68-83F9-5CFC82BC1321" />
      <xplan:inhaltBPlan xlink:href="#Gml_105E0D56-980B-4676-ACB3-A57712D24E77" />
      <xplan:inhaltBPlan xlink:href="#Gml_4CC3A535-5DB5-4EF0-B6A4-037CEA469D66" />
      <xplan:inhaltBPlan xlink:href="#Gml_F0CC3E2C-C418-4138-8315-2A683C4BFA88" />
      <xplan:inhaltBPlan xlink:href="#Gml_2E38AD03-F091-4D89-8999-6EDA55408994" />
      <xplan:inhaltBPlan xlink:href="#Gml_8C4A6CF4-FB17-44BF-9E9A-3B8810F218B2" />
      <xplan:inhaltBPlan xlink:href="#Gml_FFBB5688-4171-49C0-9C60-50BF384CDE34" />
      <xplan:inhaltBPlan xlink:href="#Gml_AAD7A215-B07D-441B-AE07-05B3180D567C" />
      <xplan:inhaltBPlan xlink:href="#Gml_0DE0D3C8-DD15-470B-8517-B3803072F2B2" />
      <xplan:inhaltBPlan xlink:href="#Gml_E560A0C0-77BB-4237-B354-7D3443D96656" />
      <xplan:inhaltBPlan xlink:href="#Gml_ED95E471-FA07-4832-A5AB-FFCD1FAE2D1F" />
      <xplan:inhaltBPlan xlink:href="#Gml_5C8066FA-7EB9-4591-BB03-51FBA32EB085" />
      <xplan:inhaltBPlan xlink:href="#Gml_DD821931-EF9B-4829-AF57-2D9A32946F60" />
      <xplan:inhaltBPlan xlink:href="#Gml_FD4CC5D8-F5AD-475C-8C98-0A3E990305CC" />
      <xplan:inhaltBPlan xlink:href="#Gml_97388113-5F6F-4B6C-A1E8-F7B2FE8583C8" />
      <xplan:inhaltBPlan xlink:href="#Gml_1D028A3B-2C51-4E36-B3FC-1473B758DF9E" />
      <xplan:inhaltBPlan xlink:href="#Gml_D09A066C-C2CB-4673-A8DF-7A9D6CFE71A8" />
      <xplan:inhaltBPlan xlink:href="#Gml_09FE2EE5-845A-4AFD-BD50-4F7F8FA2FE93" />
      <xplan:inhaltBPlan xlink:href="#Gml_FEAA4C5E-0F70-4D53-A65E-3FEC0C50B73E" />
      <xplan:inhaltBPlan xlink:href="#Gml_3FF79758-AB47-4524-AC8A-5527919BCF42" />
      <xplan:inhaltBPlan xlink:href="#Gml_8379A2BB-CF12-4EF2-8FC1-1A8EE89065BB" />
      <xplan:inhaltBPlan xlink:href="#Gml_410224BD-07D1-4B0A-B804-933322D7F8BB" />
      <xplan:inhaltBPlan xlink:href="#Gml_ABA60C6F-9B39-4DC4-9FA7-7F120289D4AE" />
      <xplan:inhaltBPlan xlink:href="#Gml_C6A0A69F-AE37-4EE1-9199-E3DF341CF800" />
      <xplan:inhaltBPlan xlink:href="#Gml_1075F0EE-DE6F-40DE-8A42-725B18748271" />
      <xplan:inhaltBPlan xlink:href="#Gml_54E8804F-4146-46FB-99C3-7263A967F693" />
      <xplan:inhaltBPlan xlink:href="#Gml_3BF0D75A-3F8C-48EC-B198-DDDF0FB29C7C" />
      <xplan:inhaltBPlan xlink:href="#Gml_707AA158-D473-4C1B-85FF-10FEDC0DB004" />
      <xplan:inhaltBPlan xlink:href="#Gml_E8224DA1-B10B-4663-838F-9731C905BFB9" />
      <xplan:inhaltBPlan xlink:href="#Gml_2D4C57D0-2D84-40E5-9CD3-105D8FF13954" />
      <xplan:inhaltBPlan xlink:href="#Gml_C36EC8BA-EDE9-4C4D-9015-736FC102B036" />
      <xplan:inhaltBPlan xlink:href="#Gml_AA045F42-5C35-438E-AE50-05DAAF57BAB3" />
      <xplan:inhaltBPlan xlink:href="#Gml_8989E65B-8F2A-445D-A361-D16A9D8BA193" />
      <xplan:inhaltBPlan xlink:href="#Gml_CD607F1C-6E01-430F-8656-336FC08C1B92" />
      <xplan:inhaltBPlan xlink:href="#Gml_BB098B6B-8551-46CF-863D-1FE9434098FE" />
      <xplan:inhaltBPlan xlink:href="#Gml_F1E3992D-B474-4C75-AFA0-CF14CEDC1921" />
      <xplan:inhaltBPlan xlink:href="#Gml_FBF6380F-4BFA-4626-82EA-20BBA222DEEF" />
      <xplan:inhaltBPlan xlink:href="#Gml_52F51803-730E-4E0C-A340-B3865126E622" />
      <xplan:inhaltBPlan xlink:href="#Gml_2913E35A-3541-434B-AA45-3E727C11E91B" />
      <xplan:inhaltBPlan xlink:href="#Gml_AFEE98DA-62DA-462F-A3E2-AE53E808AAB0" />
      <xplan:inhaltBPlan xlink:href="#Gml_3340A70A-9E8E-482A-9956-3882E33C3952" />
      <xplan:inhaltBPlan xlink:href="#Gml_14C9ED6F-6484-4281-ACAA-5DF5C31C9139" />
      <xplan:inhaltBPlan xlink:href="#Gml_05327455-614A-467B-9A32-9B546930983D" />
      <xplan:inhaltBPlan xlink:href="#Gml_8CE10650-F7DA-41CD-8781-9C8AD47B204D" />
      <xplan:inhaltBPlan xlink:href="#Gml_F34EC4F4-5CC0-4613-90A7-06C92E411C69" />
      <xplan:inhaltBPlan xlink:href="#Gml_A8C28D74-D2C5-4C35-A568-BB46D30A06A2" />
      <xplan:inhaltBPlan xlink:href="#Gml_13785B6E-2C3E-4F00-B06C-DB5416036BBA" />
      <xplan:inhaltBPlan xlink:href="#Gml_34BE27EB-885D-4420-BD37-6CE0F5DB5414" />
      <xplan:inhaltBPlan xlink:href="#Gml_7E20AFA9-B00F-40BD-A9B5-3FC30D7D7F2E" />
      <xplan:inhaltBPlan xlink:href="#Gml_1097F74F-7977-4716-87F7-0667107DF748" />
      <xplan:inhaltBPlan xlink:href="#Gml_1B306314-F20D-4FE0-B81B-D0B73607B90F" />
      <xplan:inhaltBPlan xlink:href="#Gml_672430CE-CA3D-4596-B75B-4CA82886EFB4" />
      <xplan:inhaltBPlan xlink:href="#Gml_ED575ADB-D55E-4697-A87D-94C48DE455CB" />
      <xplan:inhaltBPlan xlink:href="#Gml_01240569-8FD3-4BB1-8498-5D39D9CFDFAC" />
      <xplan:inhaltBPlan xlink:href="#Gml_E1A7E7E6-AF61-4159-9761-4EE8C6970B98" />
      <xplan:inhaltBPlan xlink:href="#Gml_C0742194-4509-4FC9-BAE7-AB2060FB2E71" />
      <xplan:inhaltBPlan xlink:href="#Gml_8E484666-D75F-497B-B92C-09BC23078EAB" />
      <xplan:inhaltBPlan xlink:href="#Gml_B71C5E2F-1BE3-4B1E-AAD5-F3CCAD4C557C" />
      <xplan:inhaltBPlan xlink:href="#Gml_3A416646-5A30-431F-B765-018AC537E9D2" />
      <xplan:inhaltBPlan xlink:href="#Gml_B53FA1D6-4401-4239-85EC-0B83EEEED98B" />
      <xplan:inhaltBPlan xlink:href="#Gml_6F657295-97A7-4010-BD20-CEAD61F7B9F9" />
      <xplan:inhaltBPlan xlink:href="#Gml_44ED1E54-97AB-47FE-B2B0-F31F370D605E" />
      <xplan:inhaltBPlan xlink:href="#Gml_96614508-4856-480C-8F45-52A26FF62BFC" />
      <xplan:inhaltBPlan xlink:href="#Gml_A2B461C6-501B-441F-A7D7-E0460B120EA0" />
      <xplan:inhaltBPlan xlink:href="#Gml_256F1540-757E-4D50-B33C-4DE30CD90EDA" />
      <xplan:inhaltBPlan xlink:href="#Gml_53DB315A-2C35-4A89-A5E3-A7B585D631B5" />
      <xplan:inhaltBPlan xlink:href="#Gml_E1CA7728-1691-4DBE-9BB2-343081D577A7" />
      <xplan:inhaltBPlan xlink:href="#Gml_1C0999A6-1B00-4BF1-A38C-C2B0DD009287" />
      <xplan:inhaltBPlan xlink:href="#Gml_6FE3506D-C86D-4BA6-9D5F-444DFC75E5B6" />
      <xplan:inhaltBPlan xlink:href="#Gml_71581799-C1C2-4BE9-98EC-88DA9FB5A0D3" />
      <xplan:inhaltBPlan xlink:href="#Gml_2E48B104-249B-428F-9DE1-B665DE28C156" />
      <xplan:inhaltBPlan xlink:href="#Gml_4906B690-8273-49AE-9BF5-2608E952364C" />
      <xplan:inhaltBPlan xlink:href="#Gml_FA27A49A-AA08-4A70-B71F-C4CE5903C04F" />
      <xplan:inhaltBPlan xlink:href="#Gml_10A2863D-287B-405A-8C74-877609CB8792" />
      <xplan:inhaltBPlan xlink:href="#Gml_2D10A095-207E-45AA-93D3-7FE207AD170A" />
      <xplan:inhaltBPlan xlink:href="#Gml_A148CD72-11C3-43E4-89A8-E6614E055115" />
      <xplan:inhaltBPlan xlink:href="#Gml_6D648862-6D06-4311-93F5-ACB522E4C5D3" />
      <xplan:inhaltBPlan xlink:href="#Gml_6A1932F1-771B-4A20-B947-2D936B11C1F9" />
      <xplan:inhaltBPlan xlink:href="#Gml_2441751E-827C-4EDE-A4DB-5806C348C2F2" />
      <xplan:inhaltBPlan xlink:href="#Gml_BFA9EDD6-B918-43F0-8A0F-5188E02F0062" />
      <xplan:inhaltBPlan xlink:href="#Gml_1086031A-D192-4595-802B-1D64DA93B627" />
      <xplan:inhaltBPlan xlink:href="#Gml_72E541E3-4766-47D0-A2BD-C27ACE32CD95" />
      <xplan:inhaltBPlan xlink:href="#Gml_08852B53-BC70-415D-94D3-1111653369EC" />
      <xplan:inhaltBPlan xlink:href="#Gml_6ED95C22-3BD6-4FE1-82C5-AA0900942E4F" />
      <xplan:inhaltBPlan xlink:href="#Gml_F31C2FB4-5C73-4714-A653-A547BC0E0E06" />
      <xplan:inhaltBPlan xlink:href="#Gml_34FF8A72-EC81-41E9-B0E8-5DBDAFE180EE" />
      <xplan:inhaltBPlan xlink:href="#Gml_7260FAE5-8799-450D-83DC-D2AB75317719" />
      <xplan:inhaltBPlan xlink:href="#Gml_0B431741-9B6B-4D64-85F3-A18158EDA4A0" />
      <xplan:inhaltBPlan xlink:href="#Gml_C1EA050F-55C3-4CE9-A005-83311B171B5A" />
      <xplan:inhaltBPlan xlink:href="#Gml_848CA123-66F0-44D7-957E-6B8836F0B951" />
      <xplan:inhaltBPlan xlink:href="#Gml_797DFDD5-B2DE-4173-880D-A2232C570312" />
      <xplan:inhaltBPlan xlink:href="#Gml_EA30DD16-2702-4A4D-AF84-AE7417EF707F" />
      <xplan:inhaltBPlan xlink:href="#Gml_B80EB37D-DA85-4E7A-9958-B55B9BB02063" />
      <xplan:inhaltBPlan xlink:href="#Gml_F71E309A-6EDE-4803-85E8-218999DC0CB1" />
      <xplan:inhaltBPlan xlink:href="#Gml_CE3A3DFC-260C-4163-9C74-FC8D64E21324" />
      <xplan:inhaltBPlan xlink:href="#Gml_79B9A9E9-D2B0-499F-A5A4-8932931930C7" />
      <xplan:inhaltBPlan xlink:href="#Gml_D061E510-B702-4A74-8C4B-58D9E6947EA4" />
      <xplan:inhaltBPlan xlink:href="#Gml_F423BD1A-9CBE-47BD-9F5C-034C16281AE3" />
      <xplan:inhaltBPlan xlink:href="#Gml_7DE6E128-28B9-425F-A3D5-9E619B6903BC" />
      <xplan:inhaltBPlan xlink:href="#Gml_AB9FFD19-11F8-45F3-AD17-A1B2927BFB9A" />
      <xplan:inhaltBPlan xlink:href="#Gml_01B279AC-12F4-4B0A-92B8-CB7298D0837C" />
      <xplan:inhaltBPlan xlink:href="#Gml_788C062F-E0D0-465A-A7D4-AABA4E65C0AB" />
      <xplan:inhaltBPlan xlink:href="#Gml_A6DEFF9A-F5F7-4915-837B-F9279111207B" />
      <xplan:inhaltBPlan xlink:href="#Gml_0234064A-B7DC-41CB-9A46-64E35CB2AB18" />
      <xplan:inhaltBPlan xlink:href="#Gml_F6DAC905-DA18-46AE-BDE6-36FADF8DDECD" />
      <xplan:inhaltBPlan xlink:href="#Gml_CFAC41FF-ADCE-49C8-B871-4B0AB26E6E05" />
      <xplan:inhaltBPlan xlink:href="#Gml_0E962F37-105D-462B-870F-361683C8F77D" />
      <xplan:inhaltBPlan xlink:href="#Gml_AA276AF7-7241-4618-9600-BFBAD010B795" />
      <xplan:inhaltBPlan xlink:href="#Gml_6F43291A-365E-4EFF-BDB7-994F228B6CD8" />
      <xplan:inhaltBPlan xlink:href="#Gml_557C4D56-1C67-4C5A-A13A-72028A58A668" />
      <xplan:inhaltBPlan xlink:href="#Gml_012690D9-CCF1-4A04-8E62-3441FF449021" />
      <xplan:inhaltBPlan xlink:href="#Gml_382F40C2-E909-464F-8B22-B2A2E327A4D8" />
      <xplan:inhaltBPlan xlink:href="#Gml_F03831A9-AB2C-49B2-BF3C-D75FCD14B3AD" />
      <xplan:inhaltBPlan xlink:href="#Gml_4E7B6147-60C0-400C-82DD-C53FF562D154" />
      <xplan:inhaltBPlan xlink:href="#Gml_6BEFDF10-91DE-4A51-A32C-0C97BEAB119A" />
      <xplan:inhaltBPlan xlink:href="#Gml_DE592534-B185-470B-8761-CFAD832887E6" />
      <xplan:inhaltBPlan xlink:href="#Gml_F186D997-6601-4ECE-94C9-0BB186F3FC0E" />
      <xplan:inhaltBPlan xlink:href="#Gml_F79D1EAF-FAF3-4DF1-8B03-EB15CD85FB68" />
      <xplan:inhaltBPlan xlink:href="#Gml_BB9B750A-F34F-414E-AB07-FA0801833485" />
      <xplan:inhaltBPlan xlink:href="#Gml_59F45590-42E4-4040-B4E8-456DEEFE2780" />
      <xplan:inhaltBPlan xlink:href="#Gml_8D41EFEC-3AD9-40D5-9F10-D0814823CB0A" />
      <xplan:inhaltBPlan xlink:href="#Gml_93405E7F-A984-4A03-856E-1A10494B6EE6" />
      <xplan:inhaltBPlan xlink:href="#Gml_3E9223A2-BDBA-4E30-8912-8CE35C7CEE13" />
      <xplan:inhaltBPlan xlink:href="#Gml_255EA12C-EEB8-40C2-9CE9-126D7CF78C43" />
      <xplan:inhaltBPlan xlink:href="#Gml_066BB715-3449-47C5-A7E1-67A59443399A" />
      <xplan:inhaltBPlan xlink:href="#Gml_4031B3D4-490A-4F51-8141-6164EA34121A" />
      <xplan:inhaltBPlan xlink:href="#Gml_3F0F099C-942B-4639-8A03-EE6E1A3A6DCC" />
      <xplan:inhaltBPlan xlink:href="#Gml_14F6C3FC-0A0D-44CA-A353-2E6165107571" />
      <xplan:inhaltBPlan xlink:href="#Gml_6DB00543-02B4-4E85-B5EB-494C015C8FA1" />
      <xplan:inhaltBPlan xlink:href="#Gml_52930595-9C20-48FF-AFA9-8E20CE0050AA" />
      <xplan:inhaltBPlan xlink:href="#Gml_E93A4B23-FC6D-4253-A6CB-DB357CF7DD4C" />
      <xplan:inhaltBPlan xlink:href="#Gml_09665564-EE0D-4295-A41A-750566E8A343" />
      <xplan:inhaltBPlan xlink:href="#Gml_E551AABE-91D8-4A2A-90FE-3FE28EC9AC04" />
      <xplan:inhaltBPlan xlink:href="#Gml_A14C6FB8-5A7B-4FFD-87A4-76DFF3F4D5A6" />
      <xplan:inhaltBPlan xlink:href="#Gml_05859DE9-E1E4-4D68-AC23-D9B29A4FBE06" />
      <xplan:inhaltBPlan xlink:href="#Gml_80E942B9-AFEE-4023-8D39-D44FBF3DCEE8" />
      <xplan:inhaltBPlan xlink:href="#Gml_F47E854B-A551-4ADE-8FBC-1849347393C1" />
      <xplan:inhaltBPlan xlink:href="#Gml_6805C200-09A0-4AC8-B2AA-216518443D19" />
      <xplan:inhaltBPlan xlink:href="#Gml_08FF3A4C-7820-4E18-9CC6-2B5135A41DA4" />
      <xplan:inhaltBPlan xlink:href="#Gml_6B759DDD-9524-4EC5-92D4-35F499410162" />
      <xplan:inhaltBPlan xlink:href="#Gml_EE93B9FC-6FC3-46AD-8809-5752AEA736F8" />
      <xplan:inhaltBPlan xlink:href="#Gml_6E6644F5-C068-4D67-A139-E9AD7E235821" />
      <xplan:inhaltBPlan xlink:href="#Gml_2D70706D-65E8-4C51-BFAA-201B36309775" />
      <xplan:inhaltBPlan xlink:href="#Gml_26CDCA61-A935-4833-85C0-8B2A51DA4752" />
      <xplan:inhaltBPlan xlink:href="#Gml_2D6AA0A5-1CC1-4A15-AD27-6A48E767980E" />
      <xplan:inhaltBPlan xlink:href="#Gml_47B4F2D5-CDC5-4A3D-B9E4-9EBADEE00168" />
      <xplan:inhaltBPlan xlink:href="#Gml_4BE4D312-EB9A-40AE-8257-F99F93734DD1" />
      <xplan:inhaltBPlan xlink:href="#Gml_F14E004B-1EF0-40F5-9A20-E80550445503" />
    </xplan:BP_Bereich>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="Gml_7F656595-9DC3-4FF2-9935-6355A61DEFA4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582504.1297 5920413.718</gml:lowerCorner>
          <gml:upperCorner>583064.217 5920961.679</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0890854F-2104-4D9D-BB3D-2F790470C25D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582594.963 5920848.586 582574.577 5920866.1187 582565.091 5920874.277 
582558.777 5920879.757 582552.129 5920885.898 582545.6503 5920893.4694 
582538.651 5920901.649 582535.71 5920905.683 582531.305 5920912.45 
582529.267 5920915.862 582527.396 5920919.435 582524.118 5920926.5497 
582524.034 5920926.732 582521.926 5920931.643 582511.764 5920959.166 
582510.836 5920961.679 582504.1297 5920953.3219 582516.846 5920921.204 
582518.869 5920916.096 582526.421 5920902.225 582527.141 5920902.581 
582530.539 5920898.147 582534.329 5920893.501 582538.204 5920888.931 
582542.184 5920884.447 582545.873 5920880.461 582546.937 5920879.313 
582557.738 5920869.946 582571.027 5920858.614 582603.81 5920830.584 
582607.003 5920827.854 582616.276 5920819.926 582620.459 5920816.501 
582623.507 5920814.11 582624.687 5920813.185 582629.097 5920809.983 
582632.4782 5920807.605 582633.472 5920806.906 582638.037 5920804.02 
582638.63 5920803.661 582642.679 5920801.226 582647.368 5920798.627 
582652.121 5920796.117 582656.928 5920793.738 582666.5469 5920789.2136 
582666.6687 5920789.1563 582671.576 5920786.848 582674.353 5920785.49 
582678.875 5920783.279 582681.754 5920781.872 582686.78 5920779.303 
582691.79 5920776.618 582696.76 5920773.886 582701.687 5920771.092 
582711.528 5920765.307 582716.6192 5920762.2158 582717.612 5920761.613 
582720.603 5920759.795 582741.4735 5920747.116 582742.7886 5920746.317 
582751.9613 5920740.7445 582760.823 5920735.361 582762.7496 5920734.1907 
582799.516 5920711.857 582823.685 5920697.173 582825.621 5920695.997 
582828.6149 5920694.137 582829.859 5920693.364 582834.001 5920690.649 
582836.9238 5920688.5499 582836.964 5920688.521 582838.017 5920687.764 
582842.014 5920684.727 582842.5386 5920684.2967 582845.886 5920681.551 
582849.6099 5920678.351 582849.689 5920678.283 582853.394 5920674.928 
582857.004 5920671.486 582860.514 5920667.911 582863.135 5920665.103 
582863.971 5920664.209 582867.324 5920660.525 582870.559 5920656.687 
582873.61 5920652.742 582876.588 5920648.726 582891.782 5920627.4895 
582894.6518 5920623.4785 582902.7418 5920612.1713 582904.5971 5920609.5782 
582905.43 5920608.414 582908.912 5920603.535 582912.31 5920598.567 
582915.555 5920593.625 582918.753 5920588.532 582921.854 5920583.388 
582921.8773 5920583.347 582923.24 5920580.951 582924.228 5920579.213 
582924.722 5920578.344 582938.1742 5920553.7561 582938.7 5920552.795 
582942.865 5920545.184 582946.8119 5920538.1405 582948.802 5920534.589 
582951.97 5920529.249 582955.258 5920524.091 582958.708 5920518.977 
582962.258 5920514.042 582965.934 5920509.058 582969.706 5920504.221 
582980.1535 5920491.6103 582989.5445 5920480.2748 582996.3166 5920472.1004 
583010.028 5920455.55 583012.811 5920452.352 583016.088 5920448.835 
583019.3394 5920445.4971 583019.397 5920445.438 583021.37 5920443.514 
583022.369 5920442.54 583026.315 5920438.792 583029.914 5920435.645 
583033.665 5920432.576 583037.564 5920429.643 583054.935 5920417.281 
583054.974 5920417.253 583059.759 5920413.718 583064.217 5920420.589 
583061.495 5920422.579 583043.095 5920435.176 583039.477 5920437.652 
583028.836 5920446.818 583029.48 5920447.584 583025.547 5920451.731 
583022.11 5920455.356 583014.965 5920463.411 583003.067 5920477.553 
583000.4335 5920480.7425 582995.395 5920486.845 582992.1832 5920490.734 
582986.1578 5920498.0296 582976.787 5920509.376 582973.06 5920514.089 
582969.449 5920518.936 582965.971 5920523.798 582962.548 5920528.779 
582959.318 5920533.791 582956.167 5920538.939 582953.176 5920544.153 
582951.8373 5920546.5844 582950.309 5920549.36 582941.2263 5920566.0806 
582932.053 5920582.968 582929.841 5920586.951 582927.463 5920590.911 
582925.052 5920594.776 582923.071 5920597.877 582922.598 5920598.618 
582920.101 5920602.431 582917.504 5920606.234 582914.893 5920609.967 
582914.7164 5920610.2127 582912.207 5920613.705 582906.1786 5920622.1482 
582899.0582 5920632.1207 582883.55 5920653.841 582880.9264 5920657.4793 
582880.579 5920657.961 582877.537 5920661.928 582874.409 5920665.792 
582871.13 5920669.721 582870.1788 5920670.775 582867.429 5920673.822 
582867.3792 5920673.8742 582864.3 5920677.098 582863.6698 5920677.7103 
582860.695 5920680.601 582857.964 5920683.107 582856.96 5920684.028 
582853.162 5920687.273 582851.5991 5920688.5399 582849.138 5920690.535 
582848.0472 5920691.3755 582845.255 5920693.527 582844.327 5920694.19 
582841.118 5920696.483 582837.033 5920699.304 582832.797 5920702.018 
582828.465 5920704.578 582820.282 5920709.471 582803.077 5920719.757 
582782.194 5920732.2424 582770.4895 5920739.2403 582764.216 5920742.991 
582737.9858 5920758.6742 582729.279 5920763.88 582729.499 5920764.27 
582726.1071 5920766.3177 582711.475 5920775.151 582702.558 5920780.378 
582693.589 5920785.292 582685.992 5920789.173 582678.449 5920792.794 
582665.1594 5920798.995 582650.241 5920805.956 582645.586 5920808.621 
582638.955 5920812.772 582634.9026 5920815.5755 582631.226 5920818.119 
582628.78 5920820.112 582615.493 5920830.933 582614.1269 5920832.1077 
582600.7633 5920843.5985 582594.963 5920848.586 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenVerkehrsFlaeche gml:id="Gml_3BF0B945-AAB5-42C2-B83A-3DB529F1FB6C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582699.564 5920566.912</gml:lowerCorner>
          <gml:upperCorner>583182.191 5921081.135</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F62049E3-61A7-4D59-95DF-31551BA72518" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582760.4429 5921005.5348 582757.468 5921008.096 582751.641 5921013.636 
582744.72 5921021.272 582733.481 5921035.782 582727.001 5921043.553 
582719.648 5921053.119 582712.626 5921062.471 582709.024 5921067.267 
582700.882 5921079.101 582699.84 5921081.135 582700.126 5921073.048 
582699.564 5921071.821 582705.178 5921063.742 582717.306 5921047.473 
582722.875 5921040.422 582729.0978 5921032.5406 582729.133 5921032.496 
582736.782 5921022.88 582742.149 5921016.14 582748.617 5921009.203 
582754.522 5921004.145 582773.475 5920988.338 582775.2966 5920986.8188 
582777.27 5920985.173 582788.663 5920975.693 582791.717 5920972.968 
582806.492 5920961.846 582807.2267 5920961.2934 582807.291 5920961.245 
582814.3317 5920956.1329 582823.956 5920949.145 582824.759 5920948.583 
582858.8662 5920924.7166 582865.288 5920920.223 582883.7132 5920907.3349 
582890.1134 5920902.8581 582901.7454 5920894.7217 582920.273 5920881.762 
582924.5731 5920878.831 582925.022 5920878.525 582927.206 5920877.035 
582936.5551 5920869.9685 582945.236 5920863.407 582949.8215 5920859.5477 
582964.495 5920847.198 582989.834 5920825.876 582998.8785 5920817.3989 
583008.644 5920808.246 583019.0386 5920797.9164 583022.1673 5920794.8072 
583027.68 5920789.329 583029.1111 5920787.6916 583046.621 5920767.657 
583050.2945 5920763.0175 583059.49 5920751.404 583069.84 5920736.284 
583079.5 5920720.489 583083.676 5920713.257 583092.274 5920698.3698 
583101.756 5920681.952 583108.0785 5920669.5148 583108.149 5920669.376 
583110.1316 5920664.8399 583114.1835 5920655.5691 583118.1532 5920646.4865 
583120.98 5920640.019 583125.997 5920630.34 583130.291 5920624.031 
583137.679 5920614.318 583141.516 5920609.969 583179.422 5920566.912 
583182.191 5920571.135 583176.108 5920578.332 583165.291 5920590.578 
583150.228 5920607.636 583141.635 5920617.38 583135.365 5920625.353 
583130.023 5920633.595 583125.355 5920642.435 583121.9523 5920649.6783 
583120.243 5920653.317 583117.331 5920660.843 583116.9913 5920661.6608 
583114.245 5920668.273 583110.2204 5920676.4831 583109.84 5920677.259 
583105.612 5920685.202 583104.5673 5920687.0518 583103.0288 5920689.7761 
583090.866 5920711.313 583087.497 5920717.102 583083.825 5920723.41 
583073.531 5920739.418 583069.2019 5920745.7302 583063.719 5920753.725 
583055.8264 5920764.0597 583050.455 5920771.093 583036.4249 5920787.0461 
583029.531 5920794.885 583028.603 5920796.067 583018.3976 5920805.8352 
583002.5711 5920820.9836 582994.83 5920828.393 582992.666 5920830.95 
582986.117 5920838.31 582975.063 5920846.266 582970.5751 5920849.4954 
582967.856 5920851.452 582941.857 5920872.615 582941.099 5920873.272 
582930.8543 5920880.8049 582925.168 5920884.986 582924.443 5920885.519 
582917.802 5920890.093 582908.749 5920896.712 582901.293 5920902.355 
582897.946 5920904.597 582877.585 5920918.229 582868.1313 5920924.6091 
582856.245 5920932.631 582842.839 5920941.852 582837.2471 5920945.6967 
582829.4934 5920951.0277 582826.651 5920952.982 582824.469 5920954.433 
582816.984 5920960.082 582805.547 5920968.714 582800.9147 5920972.365 
582795.488 5920976.642 582778.0586 5920990.5617 582774.432 5920993.458 
582771.716 5920995.627 582769.032 5920998.093 582768.139 5920998.914 
582761.186 5921004.895 582760.4429 5921005.5348 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:nutzungsform>2000</xplan:nutzungsform>
    </xplan:BP_StrassenVerkehrsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_1F01CE68-54FB-4651-989D-1D78C685CEDE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582699.84 5920571.135</gml:lowerCorner>
          <gml:upperCorner>583182.191 5921081.135</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_1095999D-25BE-4A0A-8359-13B0A972EA06" srsName="EPSG:25832">
          <gml:posList>582699.84 5921081.135 582700.882 5921079.101 582709.024 5921067.267 
582712.626 5921062.471 582719.648 5921053.119 582727.001 5921043.553 
582733.481 5921035.782 582744.72 5921021.272 582751.641 5921013.636 
582757.468 5921008.096 582761.186 5921004.895 582768.139 5920998.914 
582769.032 5920998.093 582771.716 5920995.627 582774.432 5920993.458 
582795.488 5920976.642 582800.9147 5920972.365 582805.547 5920968.714 
582816.984 5920960.082 582824.469 5920954.433 582826.651 5920952.982 
582829.4934 5920951.0277 582837.2471 5920945.6967 582842.839 5920941.852 
582856.245 5920932.631 582868.1313 5920924.6091 582877.585 5920918.229 
582897.946 5920904.597 582901.293 5920902.355 582908.749 5920896.712 
582917.802 5920890.093 582924.443 5920885.519 582925.168 5920884.986 
582930.8543 5920880.8049 582941.099 5920873.272 582941.857 5920872.615 
582967.856 5920851.452 582970.5751 5920849.4954 582975.063 5920846.266 
582986.117 5920838.31 582992.666 5920830.95 582994.83 5920828.393 
583002.5711 5920820.9836 583018.3976 5920805.8352 583028.603 5920796.067 
583029.531 5920794.885 583036.4249 5920787.0461 583050.455 5920771.093 
583063.719 5920753.725 583069.2019 5920745.7302 583073.531 5920739.418 
583083.825 5920723.41 583087.497 5920717.102 583090.866 5920711.313 
583104.5673 5920687.0518 583105.612 5920685.202 583109.84 5920677.259 
583110.2204 5920676.4831 583114.245 5920668.273 583116.9913 5920661.6608 
583117.331 5920660.843 583120.243 5920653.317 583121.9523 5920649.6783 
583125.355 5920642.435 583130.023 5920633.595 583135.365 5920625.353 
583141.635 5920617.38 583150.228 5920607.636 583165.291 5920590.578 
583176.108 5920578.332 583182.191 5920571.135 583182.191 5920571.135 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_03F80BE8-0C00-4222-9175-29A1A210DF11">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582699.564 5920566.912</gml:lowerCorner>
          <gml:upperCorner>583179.422 5921071.821</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6A6A3CD5-8DF3-4DC3-8601-E8710ABE43F5" srsName="EPSG:25832">
          <gml:posList>583179.422 5920566.912 583141.516 5920609.969 583137.679 5920614.318 
583130.291 5920624.031 583125.997 5920630.34 583120.98 5920640.019 
583118.1532 5920646.4865 583114.1835 5920655.5691 583110.1316 5920664.8399 
583108.149 5920669.376 583108.0785 5920669.5148 583101.756 5920681.952 
583083.676 5920713.257 583079.5 5920720.489 583069.84 5920736.284 
583059.49 5920751.404 583050.2945 5920763.0175 583046.621 5920767.657 
583029.1111 5920787.6916 583027.68 5920789.329 583022.1673 5920794.8072 
583008.644 5920808.246 582998.8785 5920817.3989 582989.834 5920825.876 
582964.495 5920847.198 582945.236 5920863.407 582936.5551 5920869.9685 
582927.206 5920877.035 582925.022 5920878.525 582920.273 5920881.762 
582901.7454 5920894.7217 582883.7132 5920907.3349 582865.288 5920920.223 
582858.8662 5920924.7166 582824.759 5920948.583 582823.956 5920949.145 
582814.3317 5920956.1329 582807.291 5920961.245 582807.2267 5920961.2934 
582806.492 5920961.846 582791.717 5920972.968 582788.663 5920975.693 
582777.27 5920985.173 582775.2966 5920986.8188 582773.475 5920988.338 
582754.522 5921004.145 582748.617 5921009.203 582742.149 5921016.14 
582736.782 5921022.88 582729.133 5921032.496 582729.0978 5921032.5406 
582722.875 5921040.422 582717.306 5921047.473 582705.178 5921063.742 
582699.564 5921071.821 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_C1913AE5-87B3-4BE6-A440-B317DEA26A1B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582510.836 5920420.589</gml:lowerCorner>
          <gml:upperCorner>583064.217 5920961.679</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_991853F3-53A5-42E0-80F7-3CC289A014A6" srsName="EPSG:25832">
          <gml:posList>582510.836 5920961.679 582511.764 5920959.166 582521.926 5920931.643 
582524.034 5920926.732 582524.118 5920926.5497 582527.396 5920919.435 
582529.267 5920915.862 582531.305 5920912.45 582535.71 5920905.683 
582538.651 5920901.649 582545.6503 5920893.4694 582552.129 5920885.898 
582558.777 5920879.757 582565.091 5920874.277 582574.577 5920866.1187 
582594.963 5920848.586 582600.7633 5920843.5985 582614.1269 5920832.1077 
582615.493 5920830.933 582628.78 5920820.112 582631.226 5920818.119 
582634.9026 5920815.5755 582638.955 5920812.772 582645.586 5920808.621 
582650.241 5920805.956 582665.1594 5920798.995 582678.449 5920792.794 
582685.992 5920789.173 582693.589 5920785.292 582702.558 5920780.378 
582711.475 5920775.151 582726.1071 5920766.3177 582729.499 5920764.27 
582729.279 5920763.88 582764.216 5920742.991 582770.4895 5920739.2403 
582782.194 5920732.2424 582803.077 5920719.757 582820.282 5920709.471 
582828.465 5920704.578 582832.797 5920702.018 582837.033 5920699.304 
582841.118 5920696.483 582844.327 5920694.19 582845.255 5920693.527 
582848.0472 5920691.3755 582849.138 5920690.535 582853.162 5920687.273 
582856.96 5920684.028 582857.964 5920683.107 582860.695 5920680.601 
582863.6698 5920677.7103 582864.3 5920677.098 582867.429 5920673.822 
582870.1788 5920670.775 582871.13 5920669.721 582874.409 5920665.792 
582877.537 5920661.928 582880.579 5920657.961 582880.9264 5920657.4793 
582883.55 5920653.841 582899.0582 5920632.1207 582906.1786 5920622.1482 
582912.207 5920613.705 582914.7164 5920610.2127 582914.893 5920609.967 
582917.504 5920606.234 582920.101 5920602.431 582922.598 5920598.618 
582923.071 5920597.877 582925.052 5920594.776 582927.463 5920590.911 
582929.841 5920586.951 582932.053 5920582.968 582941.2263 5920566.0806 
582950.309 5920549.36 582951.8373 5920546.5844 582953.176 5920544.153 
582956.167 5920538.939 582959.318 5920533.791 582962.548 5920528.779 
582965.971 5920523.798 582969.449 5920518.936 582973.06 5920514.089 
582976.787 5920509.376 582986.1578 5920498.0296 582995.395 5920486.845 
583000.4335 5920480.7425 583003.067 5920477.553 583014.965 5920463.411 
583022.11 5920455.356 583025.547 5920451.731 583029.48 5920447.584 
583028.836 5920446.818 583039.477 5920437.652 583043.095 5920435.176 
583061.495 5920422.579 583064.217 5920420.589 583064.217 5920420.589 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_StrassenbegrenzungsLinie gml:id="Gml_AEE988A1-DB3C-4563-BC54-EFC14206897F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582504.1297 5920413.718</gml:lowerCorner>
          <gml:upperCorner>583059.759 5920953.3219</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_3A8FFAEA-580D-4BDB-BE5D-308B36D6D78B" srsName="EPSG:25832">
          <gml:posList>583059.759 5920413.718 583054.974 5920417.253 583054.935 5920417.281 
583037.564 5920429.643 583033.665 5920432.576 583029.914 5920435.645 
583026.315 5920438.792 583022.369 5920442.54 583021.37 5920443.514 
583019.397 5920445.438 583016.088 5920448.835 583012.811 5920452.352 
583010.028 5920455.55 582996.3166 5920472.1004 582989.5445 5920480.2748 
582969.706 5920504.221 582965.934 5920509.058 582962.258 5920514.042 
582958.708 5920518.977 582955.258 5920524.091 582951.97 5920529.249 
582948.802 5920534.589 582946.8119 5920538.1405 582942.865 5920545.184 
582938.7 5920552.795 582938.1742 5920553.7561 582924.722 5920578.344 
582924.228 5920579.213 582923.24 5920580.951 582921.8773 5920583.347 
582921.854 5920583.388 582918.753 5920588.532 582915.555 5920593.625 
582912.31 5920598.567 582908.912 5920603.535 582905.43 5920608.414 
582904.5971 5920609.5782 582894.6518 5920623.4785 582891.782 5920627.4895 
582876.588 5920648.726 582873.61 5920652.742 582870.559 5920656.687 
582867.324 5920660.525 582863.971 5920664.209 582863.135 5920665.103 
582860.514 5920667.911 582857.004 5920671.486 582853.394 5920674.928 
582849.689 5920678.283 582845.886 5920681.551 582842.5386 5920684.2967 
582842.014 5920684.727 582838.017 5920687.764 582836.964 5920688.521 
582834.001 5920690.649 582829.859 5920693.364 582828.6149 5920694.137 
582825.621 5920695.997 582823.685 5920697.173 582799.516 5920711.857 
582762.7496 5920734.1907 582760.823 5920735.361 582742.7886 5920746.317 
582720.603 5920759.795 582717.612 5920761.613 582716.6192 5920762.2158 
582711.528 5920765.307 582701.687 5920771.092 582696.76 5920773.886 
582691.79 5920776.618 582686.78 5920779.303 582681.754 5920781.872 
582678.875 5920783.279 582674.353 5920785.49 582671.576 5920786.848 
582666.6687 5920789.1563 582666.5469 5920789.2136 582656.928 5920793.738 
582652.121 5920796.117 582647.368 5920798.627 582642.679 5920801.226 
582638.63 5920803.661 582638.037 5920804.02 582633.472 5920806.906 
582632.4782 5920807.605 582629.097 5920809.983 582624.687 5920813.185 
582623.507 5920814.11 582620.459 5920816.501 582616.276 5920819.926 
582607.003 5920827.854 582603.81 5920830.584 582571.027 5920858.614 
582557.738 5920869.946 582546.937 5920879.313 582545.873 5920880.461 
582542.184 5920884.447 582538.204 5920888.931 582534.329 5920893.501 
582530.539 5920898.147 582527.141 5920902.581 582526.421 5920902.225 
582518.869 5920916.096 582516.846 5920921.204 582504.1297 5920953.3219 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_StrassenbegrenzungsLinie>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_0039AEA5-B351-48DC-B073-7338D4A08203">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582603.9204 5920799.9838</gml:lowerCorner>
          <gml:upperCorner>582692.568 5920871.2808</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F3EA7B70-89AC-45B7-8917-4C1C22531DBD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582655.6066 5920838.3576 582648.144 5920843.767 582615.5669 5920871.2808 
582603.9204 5920858.4524 582608.769 5920854.144 582615.518 5920848.147 
582627.0335 5920837.9146 582626.358 5920837.082 582620.711 5920830.121 
582627.97 5920824.234 582632.788 5920820.344 582640.212 5920814.386 
582644.92 5920820.254 582651.004 5920815.3727 582652.8764 5920817.7059 
582664.686 5920810.677 582669.718 5920807.682 582682.6519 5920799.9838 
582692.568 5920816.947 582655.6066 5920838.3576 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_932150EF-C4C1-4C4C-9DA4-C426CE0DFE6E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582594.963 5920792.794</gml:lowerCorner>
          <gml:upperCorner>582682.6519 5920858.4524</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_390AB8B6-A33B-406D-8CC3-AB96F80E3BDE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582682.6519 5920799.9838 582669.718 5920807.682 582664.686 5920810.677 
582652.8764 5920817.7059 582651.004 5920815.3727 582644.92 5920820.254 
582640.212 5920814.386 582632.788 5920820.344 582627.97 5920824.234 
582620.711 5920830.121 582626.358 5920837.082 582627.0335 5920837.9146 
582615.518 5920848.147 582608.769 5920854.144 582603.9204 5920858.4524 
582596.2446 5920849.9977 582594.963 5920848.586 582600.7633 5920843.5985 
582614.1269 5920832.1077 582615.493 5920830.933 582628.78 5920820.112 
582631.226 5920818.119 582634.9026 5920815.5755 582638.955 5920812.772 
582645.586 5920808.621 582650.241 5920805.956 582665.1594 5920798.995 
582678.449 5920792.794 582679.3294 5920794.3001 582682.6519 5920799.9838 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_12BEDFB9-7CF8-460A-B431-7764FC67CA24">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582608.769 5920847.2458</gml:lowerCorner>
          <gml:upperCorner>582626.1896 5920865.014</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_955806A9-CA23-4AB0-8377-6A79401B4911" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582619.972 5920851.118 582623.886 5920855.524 582626.1896 5920858.1172 
582625.15 5920859.041 582618.428 5920865.014 582608.769 5920854.144 
582615.518 5920848.147 582616.5322 5920847.2458 582619.972 5920851.118 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_34E03C07-AEE0-4436-B814-71FAC7EA6E47">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582620.711 5920822.5862</gml:lowerCorner>
          <gml:upperCorner>582639.414 5920841.7185</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B1DCFABE-A51F-4157-90DE-922A0BCCB2DA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582639.414 5920834.181 582631.156 5920840.878 582630.1196 5920841.7185 
582627.0335 5920837.9146 582626.358 5920837.082 582620.711 5920830.121 
582627.97 5920824.234 582630.0109 5920822.5862 582639.414 5920834.181 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_88ED56F3-F505-4EA9-8C77-C74290435620">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582632.788 5920814.386</gml:lowerCorner>
          <gml:upperCorner>582651.106 5920833.921</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0DB82E7F-AD64-4477-AC3B-4642F4949C24" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582644.92 5920820.254 582651.106 5920827.9641 582648.135 5920830.349 
582643.685 5920833.921 582632.788 5920820.344 582640.212 5920814.386 
582644.92 5920820.254 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_7C1704A6-AE05-4100-9DBA-D26AB392E3D7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582664.686 5920805.9801</gml:lowerCorner>
          <gml:upperCorner>582680.201 5920823.484</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AB7E969D-017C-41EA-8D33-96FE59542B05" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582674.906 5920809.892 582680.201 5920818.787 582672.309 5920823.484 
582664.686 5920810.677 582669.718 5920807.682 582672.5773 5920805.9801 
582674.906 5920809.892 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F76D205C-6B3E-4E63-9EFF-C5A0F77A576F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582608.769 5920847.2458</gml:lowerCorner>
          <gml:upperCorner>582626.1896 5920865.014</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_11227403-C573-437D-8CF6-6C2941F49896" srsName="EPSG:25832">
          <gml:posList>582618.428 5920865.014 582625.15 5920859.041 582626.1896 5920858.1172 
582623.886 5920855.524 582619.972 5920851.118 582616.5322 5920847.2458 
582615.518 5920848.147 582608.769 5920854.144 582618.428 5920865.014 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_A4EE6257-AEA2-4109-A4E2-6107565283F2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582620.711 5920822.5862</gml:lowerCorner>
          <gml:upperCorner>582639.414 5920841.7185</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_4AD26643-1135-490C-B3BA-D1EC9DC85B38" srsName="EPSG:25832">
          <gml:posList>582639.414 5920834.181 582630.0109 5920822.5862 582627.97 5920824.234 
582620.711 5920830.121 582626.358 5920837.082 582627.0335 5920837.9146 
582630.1196 5920841.7185 582631.156 5920840.878 582639.414 5920834.181 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_ABF39B2B-24D1-4A5F-91AE-1E6F53DA195C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582632.788 5920814.386</gml:lowerCorner>
          <gml:upperCorner>582651.106 5920833.921</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F53BB2CD-D047-4790-88A9-0F1673BD7FAE" srsName="EPSG:25832">
          <gml:posList>582643.685 5920833.921 582648.135 5920830.349 582651.106 5920827.9641 
582644.92 5920820.254 582640.212 5920814.386 582632.788 5920820.344 
582643.685 5920833.921 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_5A2B61D3-7F7F-4446-AC2F-0D11F6B643F9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582664.686 5920805.9801</gml:lowerCorner>
          <gml:upperCorner>582680.201 5920823.484</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B0B0B9B0-6A18-4751-992B-3C948BD21C8B" srsName="EPSG:25832">
          <gml:posList>582680.201 5920818.787 582674.906 5920809.892 582672.5773 5920805.9801 
582669.718 5920807.682 582664.686 5920810.677 582672.309 5920823.484 
582680.201 5920818.787 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_6556E1FA-8A8B-4CCB-89E7-C0584C036AF0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582580.859 5920648.4638</gml:lowerCorner>
          <gml:upperCorner>582856.516 5920821.9855</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7A158A1A-61B5-4601-923D-537C740F4676" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582670.529 5920759.36 582644.212 5920774.444 582630.998 5920781.69 
582627.9141 5920783.3811 582636.8199 5920800.2353 582612.5934 5920813.9212 
582614.092 5920816.574 582606.639 5920820.782 582604.5074 5920821.9855 
582593.242 5920795.494 582580.859 5920766.377 582590.463 5920762.037 
582591.806 5920761.521 582595.304 5920760.179 582593.784 5920757.303 
582601.105 5920753.996 582606.638 5920750.438 582601.501 5920735.991 
582599.235 5920729.617 582598.5869 5920728.01 582649.8483 5920707.3549 
582652.0954 5920712.3774 582672.88 5920706.152 582675.711 5920705.304 
582677.365 5920710.188 582690.124 5920705.39 582698.371 5920702.288 
582705.539 5920699.976 582705.645 5920700.36 582709.24 5920699.339 
582708.949 5920698.381 582715.998 5920696.133 582741.808 5920687.902 
582747.9959 5920704.1842 582788.8143 5920686.7506 582814.5564 5920675.7562 
582827.3912 5920670.2744 582823.2849 5920662.241 582850.0175 5920648.4638 
582852.287 5920652.691 582856.516 5920660.568 582852.176 5920662.9 
582843.749 5920667.426 582849.6099 5920678.351 582845.886 5920681.551 
582842.5386 5920684.2967 582842.014 5920684.727 582838.017 5920687.764 
582836.964 5920688.521 582836.9238 5920688.5499 582835.1714 5920685.0218 
582795.3178 5920702.0087 582797.941 5920708.163 582794.786 5920709.438 
582781.15 5920714.949 582780.3874 5920713.063 582755.8626 5920722.9794 
582758.0141 5920728.3498 582752.173 5920730.74 582748.1385 5920732.3909 
582751.481 5920739.695 582751.9613 5920740.7445 582742.7886 5920746.317 
582741.4735 5920747.116 582740.399 5920744.768 582733.483 5920729.655 
582688.038 5920753.183 582687.105 5920753.502 582693.055 5920771.482 
582679.078 5920776.111 582672.7738 5920758.5475 582670.529 5920759.36 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_CDD16986-C3CB-4089-8F59-73EDFE62E932">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582604.5074 5920729.655</gml:lowerCorner>
          <gml:upperCorner>582741.4735 5920827.854</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_63318A02-8306-444F-9319-B1D30DDB36BC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582691.79 5920776.618 582686.78 5920779.303 582681.754 5920781.872 
582678.875 5920783.279 582674.353 5920785.49 582671.576 5920786.848 
582666.6687 5920789.1563 582666.5469 5920789.2136 582656.928 5920793.738 
582652.121 5920796.117 582647.368 5920798.627 582642.679 5920801.226 
582638.63 5920803.661 582638.037 5920804.02 582633.472 5920806.906 
582632.4782 5920807.605 582629.097 5920809.983 582624.687 5920813.185 
582623.507 5920814.11 582620.459 5920816.501 582616.276 5920819.926 
582607.003 5920827.854 582604.5074 5920821.9855 582606.639 5920820.782 
582614.092 5920816.574 582612.5934 5920813.9212 582636.8199 5920800.2353 
582627.9141 5920783.3811 582630.998 5920781.69 582644.212 5920774.444 
582670.529 5920759.36 582672.7738 5920758.5475 582679.078 5920776.111 
582693.055 5920771.482 582687.105 5920753.502 582688.038 5920753.183 
582733.483 5920729.655 582740.399 5920744.768 582741.4735 5920747.116 
582720.603 5920759.795 582717.612 5920761.613 582716.6192 5920762.2158 
582711.528 5920765.307 582701.687 5920771.092 582696.76 5920773.886 
582691.79 5920776.618 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_44F52906-86C6-4AD0-829F-26E6AA1EF019">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582833.8041 5920554.9273</gml:lowerCorner>
          <gml:upperCorner>582896.867 5920635.9001</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9CA0FA08-3220-4338-89B5-4ED0298167B8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582878.64 5920577.442 582896.867 5920609.87 582892.215 5920612.484 
582888.844 5920614.379 582884.066 5920617.061 582850.504 5920635.9001 
582833.8041 5920594.0323 582862.966 5920581.9539 582852.1468 5920562.7054 
582865.985 5920554.9273 582878.64 5920577.442 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_A660A02F-F4B6-45EE-820B-AB25D62A8C6A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582748.1385 5920685.0218</gml:lowerCorner>
          <gml:upperCorner>582836.9238 5920740.7445</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_53C1C357-EE4F-49A6-BA17-FAD7C1DA329C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582762.7496 5920734.1907 582760.823 5920735.361 582751.9613 5920740.7445 
582751.481 5920739.695 582748.1385 5920732.3909 582752.173 5920730.74 
582758.0141 5920728.3498 582755.8626 5920722.9794 582780.3874 5920713.063 
582781.15 5920714.949 582794.786 5920709.438 582797.941 5920708.163 
582795.3178 5920702.0087 582835.1714 5920685.0218 582836.9238 5920688.5499 
582834.001 5920690.649 582829.859 5920693.364 582828.6149 5920694.137 
582825.621 5920695.997 582823.685 5920697.173 582799.516 5920711.857 
582762.7496 5920734.1907 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_78C7B883-9581-4F87-A09C-947D9EBB120D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582843.749 5920552.8006</gml:lowerCorner>
          <gml:upperCorner>582902.7418 5920678.351</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CF1D1ED1-B287-4F49-86B3-1CBF052B87DD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582902.7418 5920612.1713 582894.6518 5920623.4785 582891.782 5920627.4895 
582876.588 5920648.726 582873.61 5920652.742 582870.559 5920656.687 
582867.324 5920660.525 582863.971 5920664.209 582863.135 5920665.103 
582860.514 5920667.911 582857.004 5920671.486 582853.394 5920674.928 
582849.689 5920678.283 582849.6099 5920678.351 582843.749 5920667.426 
582852.176 5920662.9 582856.516 5920660.568 582852.287 5920652.691 
582850.0175 5920648.4638 582854.7413 5920646.0293 582852.634 5920641.24 
582850.504 5920635.9001 582884.066 5920617.061 582888.844 5920614.379 
582892.215 5920612.484 582896.867 5920609.87 582878.64 5920577.442 
582865.985 5920554.9273 582869.7687 5920552.8006 582902.7418 5920612.1713 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8D370490-9C82-4E1C-99E0-CA65D64F317F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582594.562 5920791.572</gml:lowerCorner>
          <gml:upperCorner>582614.092 5920820.782</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0B1A9B5D-3506-4157-8128-91A4C37A073C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582609.993 5920809.318 582614.092 5920816.574 582606.639 5920820.782 
582602.543 5920813.524 582594.562 5920794.927 582602.376 5920791.572 
582609.993 5920809.318 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_732DAF27-1F40-438C-AD90-C25DBCF40B98">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582594.562 5920791.572</gml:lowerCorner>
          <gml:upperCorner>582614.092 5920820.782</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_1F07C714-35A2-43E3-B782-B76503F30DEE" srsName="EPSG:25832">
          <gml:posList>582609.993 5920809.318 582602.376 5920791.572 582594.562 5920794.927 
582602.543 5920813.524 582606.639 5920820.782 582614.092 5920816.574 
582609.993 5920809.318 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_248BA7AD-524F-411E-92A0-9B3673A77CF3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582613.0719 5920794.8873</gml:lowerCorner>
          <gml:upperCorner>582627.7919 5920810.3167</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_65F921AA-AF54-4C11-A14B-D32CA46E556B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582627.7919 5920805.3354 582618.9741 5920810.3167 582613.0719 5920799.8686 
582621.8896 5920794.8873 582627.7919 5920805.3354 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_47D549FF-1B04-4897-BEAE-211A1B1DD026">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582622.6275 5920759.1752</gml:lowerCorner>
          <gml:upperCorner>582644.212 5920781.69</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C3CBF1FA-5370-4D80-8387-8FDD4A980609" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582639.526 5920765.898 582644.212 5920774.444 582630.998 5920781.69 
582626.314 5920773.145 582622.6275 5920766.4197 582635.8397 5920759.1752 
582639.526 5920765.898 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_E93822BC-6528-492C-B2ED-FCF8F70D2A0F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582613.0719 5920794.8873</gml:lowerCorner>
          <gml:upperCorner>582627.7919 5920810.3167</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_7EEFA83B-1DE6-4B2F-B8AC-F60243187266" srsName="EPSG:25832">
          <gml:posList>582618.9741 5920810.3167 582627.7919 5920805.3354 582621.8896 5920794.8873 
582613.0719 5920799.8686 582618.9741 5920810.3167 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_73823297-65B1-4537-A27A-84861B7934E1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582622.6275 5920759.1752</gml:lowerCorner>
          <gml:upperCorner>582644.212 5920781.69</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_139A9B9B-539E-4275-892F-EACC5531CC33" srsName="EPSG:25832">
          <gml:posList>582635.8397 5920759.1752 582622.6275 5920766.4197 582626.314 5920773.145 
582630.998 5920781.69 582644.212 5920774.444 582639.526 5920765.898 
582635.8397 5920759.1752 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_1B2FE4EA-C636-4539-9152-3357ECB6DEF1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582622.735 5920714.407</gml:lowerCorner>
          <gml:upperCorner>582652.502 5920737.225</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 3 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CD4B8039-6D8C-4396-ACD8-6347255D8F3A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582652.502 5920727.151 582627.827 5920737.225 582622.735 5920724.722 
582637.233 5920718.836 582636.159 5920716.194 582640.551 5920714.407 
582640.98 5920715.462 582641.6242 5920717.0462 582647.413 5920714.683 
582652.502 5920727.151 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_51CB47AC-0BCC-46A8-A414-4CA7EAD99B62">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582622.735 5920714.407</gml:lowerCorner>
          <gml:upperCorner>582652.502 5920737.225</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_57B21AB4-C109-4991-A520-4D45541CC528" srsName="EPSG:25832">
          <gml:posList>582627.827 5920737.225 582652.502 5920727.151 582647.413 5920714.683 
582641.6242 5920717.0462 582640.98 5920715.462 582640.551 5920714.407 
582636.159 5920716.194 582637.233 5920718.836 582622.735 5920724.722 
582627.827 5920737.225 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8A56A418-EEA1-4520-94ED-E295008F861F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582665.938 5920733.722</gml:lowerCorner>
          <gml:upperCorner>582693.055 5920776.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 5 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FC866C2A-BFAE-44FB-8D9D-2A9615C4F446" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582688.038 5920753.183 582687.105 5920753.502 582693.055 5920771.482 
582679.078 5920776.111 582665.938 5920739.503 582681.412 5920733.722 
582688.038 5920753.183 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_819E8C4A-AF97-49EC-A0B6-F413A5195E5B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582665.938 5920733.722</gml:lowerCorner>
          <gml:upperCorner>582693.055 5920776.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_9084DE9C-E9EE-4AC9-A8E1-DDE3B04EFF95" srsName="EPSG:25832">
          <gml:posList>582681.412 5920733.722 582665.938 5920739.503 582679.078 5920776.111 
582693.055 5920771.482 582687.105 5920753.502 582688.038 5920753.183 
582681.412 5920733.722 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_9997F827-0127-4F01-A923-AAD435A8F349">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582710.2799 5920710.4487</gml:lowerCorner>
          <gml:upperCorner>582724.8821 5920725.772</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_395B5821-BF8E-4A3E-99E9-D256E0DDB005" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582724.8777 5920721.3226 582724.8821 5920721.332 582723.097 5920722.165 
582715.367 5920725.772 582710.488 5920715.343 582710.2799 5920714.8982 
582719.7905 5920710.4487 582724.8777 5920721.3226 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_C9F8753C-C5F6-4AC1-9369-4B7A7F1B65A7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582733.483 5920724.582</gml:lowerCorner>
          <gml:upperCorner>582751.481 5920744.768</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0E7F5AC9-B9C9-413B-A899-9DB3714089E6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582751.481 5920739.695 582740.399 5920744.768 582733.483 5920729.655 
582744.565 5920724.582 582751.481 5920739.695 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_E013B261-7E28-432E-85DD-CCECB9AA4A3B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582731.171 5920699.8787</gml:lowerCorner>
          <gml:upperCorner>582746.6774 5920713.6082</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0F11FCAD-C417-4042-BE53-8D0FF0806EC8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582746.6774 5920709.7293 582739.772 5920711.888 582735.863 5920713.11 
582734.2696 5920713.6082 582733.448 5920711.006 582731.171 5920703.794 
582741.66 5920700.48 582743.5674 5920699.8787 582746.6774 5920709.7293 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_3B7FF1BB-BD16-4F7B-9E71-BA91DA33BDB8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582754.6583 5920707.5703</gml:lowerCorner>
          <gml:upperCorner>582769.7443 5920721.8647</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B112AEB9-9C18-42FA-BAD3-58035BE17DDF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582769.7443 5920717.3664 582758.6193 5920721.8647 582754.6583 5920712.0686 
582765.7834 5920707.5703 582769.7443 5920717.3664 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_C10C0644-9E98-47FF-927E-DE8B2C5EB36C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582710.2799 5920710.4487</gml:lowerCorner>
          <gml:upperCorner>582724.8821 5920725.772</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_4EF93286-AAE8-44ED-95B5-CA67D5EE5BE7" srsName="EPSG:25832">
          <gml:posList>582715.367 5920725.772 582723.097 5920722.165 582724.8821 5920721.332 
582724.8777 5920721.3226 582719.7905 5920710.4487 582710.2799 5920714.8982 
582710.488 5920715.343 582715.367 5920725.772 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_2F3DDB5A-48F1-44D7-893E-CF754550336A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582733.483 5920724.582</gml:lowerCorner>
          <gml:upperCorner>582751.481 5920744.768</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_378213B3-5E44-4AEC-A1C3-1A4B00C1FD96" srsName="EPSG:25832">
          <gml:posList>582744.565 5920724.582 582733.483 5920729.655 582740.399 5920744.768 
582751.481 5920739.695 582744.565 5920724.582 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_0C68A1A6-4372-4634-AF27-095A920B637A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582731.171 5920699.8787</gml:lowerCorner>
          <gml:upperCorner>582746.6774 5920713.6082</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_EE827CD9-033D-43A1-9041-FBC295BDF1FA" srsName="EPSG:25832">
          <gml:posList>582731.171 5920703.794 582733.448 5920711.006 582734.2696 5920713.6082 
582735.863 5920713.11 582739.772 5920711.888 582746.6774 5920709.7293 
582743.5674 5920699.8787 582741.66 5920700.48 582731.171 5920703.794 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_4150523E-910C-42E7-ADA3-F58C7417FE93">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582754.6583 5920707.5703</gml:lowerCorner>
          <gml:upperCorner>582769.7443 5920721.8647</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B2D07EEC-DC64-427D-85A2-1D7571D3ACD7" srsName="EPSG:25832">
          <gml:posList>582758.6193 5920721.8647 582769.7443 5920717.3664 582765.7834 5920707.5703 
582754.6583 5920712.0686 582758.6193 5920721.8647 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_C5C9158D-8A5D-4303-8CA6-2060B48ABC29">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582799.6669 5920683.0694</gml:lowerCorner>
          <gml:upperCorner>582813.8527 5920698.1495</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FE6C5F33-71C1-43E2-BACB-4D63E9DA70BA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582813.8527 5920694.1085 582804.3721 5920698.1495 582799.6669 5920687.1104 
582809.1475 5920683.0694 582813.8527 5920694.1085 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_7FDE3A32-C91A-45EA-9492-57AA240677CB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582830.3681 5920652.691</gml:lowerCorner>
          <gml:upperCorner>582856.516 5920672.219</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0F402249-195A-4B45-BC8F-E22FD212A2FA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582856.516 5920660.568 582852.176 5920662.9 582843.749 5920667.426 
582834.825 5920672.219 582830.3681 5920663.9159 582834.437 5920661.743 
582839.343 5920659.123 582847.999 5920654.989 582852.287 5920652.691 
582856.516 5920660.568 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_A77FDE91-83E7-4EF1-BD95-62D21459C322">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582799.6669 5920683.0694</gml:lowerCorner>
          <gml:upperCorner>582813.8527 5920698.1495</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_3A4293D5-5F72-4DF1-A6A0-8B87BCCCD29C" srsName="EPSG:25832">
          <gml:posList>582813.8527 5920694.1085 582809.1475 5920683.0694 582799.6669 5920687.1104 
582804.3721 5920698.1495 582813.8527 5920694.1085 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_4E5CCB44-4116-4F49-B96B-46B917F12F56">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582830.3681 5920652.691</gml:lowerCorner>
          <gml:upperCorner>582856.516 5920672.219</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8423513F-357A-4F5C-93D9-C2A84D5E5B4C" srsName="EPSG:25832">
          <gml:posList>582834.825 5920672.219 582843.749 5920667.426 582852.176 5920662.9 
582856.516 5920660.568 582852.287 5920652.691 582847.999 5920654.989 
582839.343 5920659.123 582834.437 5920661.743 582830.3681 5920663.9159 
582834.825 5920672.219 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_C9110F3F-73C1-43D0-9A0F-A7D94F659FDC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582844.3607 5920601.2061</gml:lowerCorner>
          <gml:upperCorner>582858.5313 5920616.2467</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0CCF2E4B-8EC9-4691-B796-23F8741C6EE0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582858.5313 5920612.3217 582848.8822 5920616.2467 582844.3607 5920605.1311 
582854.0098 5920601.2061 582858.5313 5920612.3217 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8BDCCB74-D08A-4283-BB47-872FDB7045FF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582865.85 5920577.442</gml:lowerCorner>
          <gml:upperCorner>582896.867 5920617.061</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 4 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_791014DB-66BA-4A2B-8A11-E06B8D673B91" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582896.867 5920609.87 582892.215 5920612.484 582888.844 5920614.379 
582884.066 5920617.061 582865.85 5920584.628 582878.64 5920577.442 
582896.867 5920609.87 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_0BA48C00-C8FA-4A5C-8F90-E1F687113220">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582844.3607 5920601.2061</gml:lowerCorner>
          <gml:upperCorner>582858.5313 5920616.2467</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_47E94164-BD49-4356-AE6A-79A5B21A3AD6" srsName="EPSG:25832">
          <gml:posList>582848.8822 5920616.2467 582858.5313 5920612.3217 582854.0098 5920601.2061 
582844.3607 5920605.1311 582848.8822 5920616.2467 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F8CD6A6C-484A-493A-A906-7B7245EBADB6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582865.85 5920577.442</gml:lowerCorner>
          <gml:upperCorner>582896.867 5920617.061</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B9695876-8FC7-4322-9017-B926D1955C18" srsName="EPSG:25832">
          <gml:posList>582865.85 5920584.628 582884.066 5920617.061 582888.844 5920614.379 
582892.215 5920612.484 582896.867 5920609.87 582878.64 5920577.442 
582865.85 5920584.628 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_AB54C146-1C38-44C1-B340-E48300867A4E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582661.7747 5920766.274</gml:lowerCorner>
          <gml:upperCorner>582661.7747 5920766.274</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_FB90423D-4C98-4692-81C7-4D338210A438" srsName="EPSG:25832">
          <gml:pos>582661.7747 5920766.274</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_37FE2FBA-8620-4B5E-AD12-E3FB484BC510">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582673.7246 5920768.8251</gml:lowerCorner>
          <gml:upperCorner>582673.7246 5920768.8251</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_B41CBEC2-E16D-4CD7-B769-A75997207250" srsName="EPSG:25832">
          <gml:pos>582673.7246 5920768.8251</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_B606F979-4736-4983-82E8-4FD4796C0C05">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582674.933 5920772.4503</gml:lowerCorner>
          <gml:upperCorner>582674.933 5920772.4503</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_D3CD8786-D7F5-4892-B231-E737A36D6328" srsName="EPSG:25832">
          <gml:pos>582674.933 5920772.4503</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_82E500D7-C44F-4D81-A8FD-009844AB5668">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582676.4099 5920775.807</gml:lowerCorner>
          <gml:upperCorner>582676.4099 5920775.807</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_938893BD-D29C-4F44-9CC0-A82269265BE6" srsName="EPSG:25832">
          <gml:pos>582676.4099 5920775.807</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_885703C9-1C57-413B-8805-B8C94C8F5B1E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582698.0271 5920749.2219</gml:lowerCorner>
          <gml:upperCorner>582698.0271 5920749.2219</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_6EB73BD5-6877-473A-982B-43E180F0DE08" srsName="EPSG:25832">
          <gml:pos>582698.0271 5920749.2219</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_ED504845-CE02-43DB-B5CF-5820855956D0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582665.938 5920733.722</gml:lowerCorner>
          <gml:upperCorner>582717.612 5920785.49</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9A436DDA-8858-4E4C-B1B4-66DCC099D00B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582691.79 5920776.618 582686.78 5920779.303 582681.754 5920781.872 
582678.875 5920783.279 582674.353 5920785.49 582665.938 5920739.503 
582672.7738 5920758.5475 582679.078 5920776.111 582693.055 5920771.482 
582687.105 5920753.502 582688.038 5920753.183 582681.412 5920733.722 
582710.325 5920748.016 582705.183 5920750.901 582708.287 5920756.349 
582713.36 5920753.395 582717.612 5920761.613 582716.6192 5920762.2158 
582711.528 5920765.307 582701.687 5920771.092 582696.76 5920773.886 
582691.79 5920776.618 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_118DE24C-1F13-405C-AB02-DE1746A5AC98">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582720.603 5920724.582</gml:lowerCorner>
          <gml:upperCorner>582760.823 5920759.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_033F31A4-D170-47CA-B075-18A06852A61F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582742.7886 5920746.317 582741.4735 5920747.116 582720.603 5920759.795 
582733.483 5920729.655 582740.399 5920744.768 582751.481 5920739.695 
582748.1385 5920732.3909 582744.565 5920724.582 582760.823 5920735.361 
582751.9613 5920740.7445 582742.7886 5920746.317 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_949678C3-8E62-466D-950F-C67FB568D6B7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582852.1468 5920552.8006</gml:lowerCorner>
          <gml:upperCorner>582902.7418 5920665.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6D4142FC-00E5-43B3-8808-31130EED96F8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582894.6518 5920623.4785 582891.782 5920627.4895 582876.588 5920648.726 
582873.61 5920652.742 582870.559 5920656.687 582867.324 5920660.525 
582863.971 5920664.209 582863.135 5920665.103 582857.598 5920584.1772 
582862.966 5920581.9539 582852.1468 5920562.7054 582853.2036 5920562.1114 
582865.85 5920584.628 582884.066 5920617.061 582888.844 5920614.379 
582892.215 5920612.484 582896.867 5920609.87 582878.64 5920577.442 
582865.985 5920554.9273 582869.7687 5920552.8006 582902.7418 5920612.1713 
582894.6518 5920623.4785 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_ErhaltungsBereichFlaeche gml:id="Gml_787E188A-A9A8-41B2-B28A-B8E20CAEA6EC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582598.5869 5920687.902</gml:lowerCorner>
          <gml:upperCorner>582760.823 5920803.661</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_964AAC48-228A-493E-8FF2-91C2100BA7D1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_755565FC-52C0-4E14-BD76-53F2D623AC32" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582720.603 5920759.795 582717.612 5920761.613 582716.6192 5920762.2158 
582711.528 5920765.307 582701.687 5920771.092 582701.5329 5920771.1794 
582696.76 5920773.886 582691.79 5920776.618 582686.78 5920779.303 
582681.754 5920781.872 582678.875 5920783.279 582674.353 5920785.49 
582674.1471 5920785.5907 582671.576 5920786.848 582666.6687 5920789.1563 
582666.5469 5920789.2136 582656.928 5920793.738 582652.121 5920796.117 
582647.368 5920798.627 582642.679 5920801.226 582638.63 5920803.661 
582636.8199 5920800.2353 582627.9141 5920783.3811 582623.764 5920775.527 
582609.526 5920748.58 582606.638 5920750.438 582601.501 5920735.991 
582599.235 5920729.617 582598.5869 5920728.01 582649.8483 5920707.3549 
582652.0954 5920712.3774 582672.88 5920706.152 582675.711 5920705.304 
582677.365 5920710.188 582690.124 5920705.39 582698.371 5920702.288 
582705.539 5920699.976 582705.645 5920700.36 582709.24 5920699.339 
582708.949 5920698.381 582715.998 5920696.133 582741.808 5920687.902 
582747.9959 5920704.1842 582752.736 5920715.175 582755.8626 5920722.9794 
582758.0141 5920728.3498 582760.823 5920735.361 582751.9613 5920740.7445 
582742.7886 5920746.317 582741.4735 5920747.116 582720.603 5920759.795 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:grund>1000</xplan:grund>
    </xplan:BP_ErhaltungsBereichFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_ErhaltungsBereichFlaeche gml:id="Gml_86A56640-99D4-44B6-ACF1-C9D70A28B8C0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582833.8041 5920552.8006</gml:lowerCorner>
          <gml:upperCorner>582902.7418 5920665.103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_964AAC48-228A-493E-8FF2-91C2100BA7D1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DE648527-8487-45E5-AF15-FA332D0A9877" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582878.3273 5920568.211 582902.7418 5920612.1713 582898.6273 5920617.9221 
582894.6518 5920623.4785 582891.782 5920627.4895 582876.588 5920648.726 
582873.61 5920652.742 582870.559 5920656.687 582867.324 5920660.525 
582863.971 5920664.209 582863.135 5920665.103 582854.7413 5920646.0293 
582852.634 5920641.24 582850.504 5920635.9001 582833.8041 5920594.0323 
582856.8168 5920584.5008 582857.598 5920584.1772 582862.966 5920581.9539 
582860.826 5920578.1466 582852.1468 5920562.7054 582853.2036 5920562.1114 
582865.985 5920554.9273 582869.7687 5920552.8006 582878.3273 5920568.211 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:grund>1000</xplan:grund>
    </xplan:BP_ErhaltungsBereichFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_61E482EA-DBAC-4434-A977-770629F1D774">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582678.449 5920764.27</gml:lowerCorner>
          <gml:upperCorner>582730.5858 5920794.3001</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D136609C-3C9A-463D-BED2-37CC0F653576" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582730.5858 5920766.1889 582710.3483 5920777.7423 582679.3294 5920794.3001 
582678.449 5920792.794 582685.992 5920789.173 582693.589 5920785.292 
582702.558 5920780.378 582711.475 5920775.151 582726.1071 5920766.3177 
582729.499 5920764.27 582730.5858 5920766.1889 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_1C52C00D-28DA-4418-873A-59054DA3C4FD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582678.449 5920792.794</gml:lowerCorner>
          <gml:upperCorner>582679.3294 5920794.3001</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_EF2B2CCB-C8A9-48FB-93B9-00C5B48F174B" srsName="EPSG:25832">
          <gml:posList>582679.3294 5920794.3001 582678.449 5920792.794 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_BCD2D953-B159-4F16-A502-0D87FE80E5A4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582729.279 5920750.5922</gml:lowerCorner>
          <gml:upperCorner>582771.7871 5920782.6716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_070A01A4-3ABA-4B59-BA9A-B16BC59784E5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582771.7655 5920765.026 582768.555 5920766.766 582760.027 5920771.388 
582744.931 5920779.926 582740.727 5920782.23 582739.9212 5920782.6716 
582734.8099 5920773.6471 582730.5858 5920766.1889 582729.499 5920764.27 
582729.279 5920763.88 582737.9858 5920758.6742 582739.562 5920761.489 
582741.4838 5920764.9208 582765.4667 5920750.5922 582771.7871 5920765.0143 
582771.7655 5920765.026 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_43B4BEC4-EAB2-4484-AC5A-4B91E49AA8CF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582730.7279 5920761.489</gml:lowerCorner>
          <gml:upperCorner>582748.6946 5920782.6716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_5995496D-367C-4824-8B10-AC3FB88BC989" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582741.4838 5920764.9208 582748.6946 5920777.7974 582744.931 5920779.926 
582740.727 5920782.23 582739.9212 5920782.6716 582734.8099 5920773.6471 
582730.7279 5920766.4399 582733.017 5920765.157 582739.562 5920761.489 
582741.4838 5920764.9208 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_BD28730B-DB9C-4CFC-821E-660C1E88E7F7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582730.7279 5920761.489</gml:lowerCorner>
          <gml:upperCorner>582748.6946 5920782.6716</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_CDAAA502-2AD6-4526-A148-E27E2922F39C" srsName="EPSG:25832">
          <gml:posList>582739.9212 5920782.6716 582740.727 5920782.23 582744.931 5920779.926 
582748.6946 5920777.7974 582741.4838 5920764.9208 582739.562 5920761.489 
582733.017 5920765.157 582730.7279 5920766.4399 582734.8099 5920773.6471 
582739.9212 5920782.6716 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_AC04E845-F802-4A80-BB85-0BB9CFE8BFA1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582768.2855 5920716.8557</gml:lowerCorner>
          <gml:upperCorner>582834.061 5920766.4809</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_EE52A4B7-1CDC-4210-AB6F-7D11AFEC9B22" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582834.061 5920736.939 582816.192 5920745.904 582776.1495 5920766.4809 
582771.0938 5920756.5801 582768.2855 5920748.8987 582806.1934 5920725.9701 
582810.987 5920723.399 582819.353 5920719.188 582823.9865 5920716.8557 
582834.061 5920736.939 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_49F651E3-0519-4217-B5CC-EA24B996E192">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582737.9858 5920709.471</gml:lowerCorner>
          <gml:upperCorner>582823.9865 5920764.9208</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FBFEDEAA-E612-47BE-BDCB-C15DB7AC7F19" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582765.4667 5920750.5922 582741.4838 5920764.9208 582739.562 5920761.489 
582737.9858 5920758.6742 582764.216 5920742.991 582770.4895 5920739.2403 
582782.194 5920732.2424 582803.077 5920719.757 582820.282 5920709.471 
582821.3492 5920711.5984 582823.9865 5920716.8557 582819.353 5920719.188 
582810.987 5920723.399 582806.1934 5920725.9701 582768.2855 5920748.8987 
582767.5126 5920746.7846 582766.2487 5920744.8887 582763.6155 5920746.3633 
582765.4667 5920750.5922 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_B80A3470-9211-4DCD-89F3-FF408BFBD33E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582782.828 5920741.1609</gml:lowerCorner>
          <gml:upperCorner>582801.7836 5920759.549</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_892BFB30-D741-4D7D-8A73-1A6ADB08E989" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582795.498 5920741.412 582801.231 5920747.134 582801.7836 5920747.6856 
582798.466 5920750.8 582789.146 5920759.549 582782.828 5920752.818 
582792.142 5920744.075 582795.2464 5920741.1609 582795.498 5920741.412 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_58F4716B-7056-4937-8EF0-5960884912CF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582810.987 5920718.2286</gml:lowerCorner>
          <gml:upperCorner>582826.2879 5920733.355</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_26DF539E-5DFE-4B03-AA98-BADF7F41C843" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582824.4035 5920724.4544 582826.2879 5920728.1762 582824.365 5920729.146 
582816.02 5920733.355 582814.141 5920729.644 582810.987 5920723.399 
582819.353 5920719.188 582821.2591 5920718.2286 582824.4035 5920724.4544 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_C7E4BB56-8C13-47F2-A6D1-4C8228D23206">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582782.828 5920741.1609</gml:lowerCorner>
          <gml:upperCorner>582801.7836 5920759.549</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_845C1897-D407-443C-ABE4-42D1779C3767" srsName="EPSG:25832">
          <gml:posList>582782.828 5920752.818 582789.146 5920759.549 582798.466 5920750.8 
582801.7836 5920747.6856 582801.231 5920747.134 582795.498 5920741.412 
582795.2464 5920741.1609 582792.142 5920744.075 582782.828 5920752.818 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_9B73FA32-609F-4083-95E8-77A931922BE9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582810.987 5920718.2286</gml:lowerCorner>
          <gml:upperCorner>582826.2879 5920733.355</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_BB774EF4-EA9C-4D83-9D8B-C76EDC83E309" srsName="EPSG:25832">
          <gml:posList>582810.987 5920723.399 582814.141 5920729.644 582816.02 5920733.355 
582824.365 5920729.146 582826.2879 5920728.1762 582824.4035 5920724.4544 
582821.2591 5920718.2286 582819.353 5920719.188 582810.987 5920723.399 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_E3200FFE-CFFF-4EF4-A34C-30F1E540A6EC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582820.282 5920709.471</gml:lowerCorner>
          <gml:upperCorner>582821.3492 5920711.5984</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A8894323-F1D1-4E71-824E-A9FB251B9B2D" srsName="EPSG:25832">
          <gml:posList>582821.3492 5920711.5984 582820.282 5920709.471 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_F786EB17-8F70-4E59-B168-18D6BE864BC8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582805.0094 5920746.4686</gml:lowerCorner>
          <gml:upperCorner>582805.0094 5920746.4686</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_27305C4B-F25D-4040-9778-7DEA03C9CEDB" srsName="EPSG:25832">
          <gml:pos>582805.0094 5920746.4686</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_99185F97-C72B-4C41-A503-921165760BA7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582820.282 5920688.5399</gml:lowerCorner>
          <gml:upperCorner>582853.1734 5920711.5984</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_29209D5D-4B7E-424B-A075-8E1188FAC9CE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582853.1734 5920690.6233 582845.6184 5920696.3314 582835.4605 5920703.0166 
582821.3492 5920711.5984 582820.282 5920709.471 582828.465 5920704.578 
582832.797 5920702.018 582837.033 5920699.304 582841.118 5920696.483 
582844.327 5920694.19 582845.255 5920693.527 582848.0472 5920691.3755 
582849.138 5920690.535 582851.5991 5920688.5399 582853.1734 5920690.6233 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_1864745D-0331-4FA0-AB8F-E189363261AC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582851.5991 5920688.5399</gml:lowerCorner>
          <gml:upperCorner>582853.1734 5920690.6233</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_E75115C0-B0F9-4351-894A-084B75BB1DF2" srsName="EPSG:25832">
          <gml:posList>582853.1734 5920690.6233 582851.5991 5920688.5399 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_67D5211A-5AD6-40DE-ACD7-5DF12927C0F7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582851.5991 5920673.8742</gml:lowerCorner>
          <gml:upperCorner>582870.3621 5920690.6233</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_40CF0DA9-CBBE-4DD2-AE52-6D3137E76329" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582869.3185 5920676.4518 582870.3621 5920677.8388 582869.148 5920679.234 
582853.1734 5920690.6233 582851.5991 5920688.5399 582853.162 5920687.273 
582856.96 5920684.028 582857.964 5920683.107 582860.695 5920680.601 
582863.6698 5920677.7103 582864.3 5920677.098 582867.3792 5920673.8742 
582869.3185 5920676.4518 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_4932D072-3051-4C1F-AB00-A53D586351B3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582867.3792 5920673.822</gml:lowerCorner>
          <gml:upperCorner>582869.3185 5920676.4518</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A5718BE2-3C79-4ED6-8176-6F3447F55617" srsName="EPSG:25832">
          <gml:posList>582869.3185 5920676.4518 582867.3792 5920673.8742 582867.429 5920673.822 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_292A2607-CE68-4446-8FC2-3A1EA23A6166">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582853.1734 5920669.4842</gml:lowerCorner>
          <gml:upperCorner>582880.238 5920704.753</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_49174BE9-2853-461E-A634-05B7B3A26800" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582877.5681 5920672.2823 582870.4249 5920679.5697 582880.238 5920693.8609 
582863.8506 5920704.753 582853.1734 5920690.6233 582869.148 5920679.234 
582870.3621 5920677.8388 582869.3185 5920676.4518 582874.1822 5920671.3273 
582875.681 5920669.4842 582877.5681 5920672.2823 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_D3561EDC-7ADD-498D-8858-F8E59600B523">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582858.8284 5920680.4748</gml:lowerCorner>
          <gml:upperCorner>582874.374 5920696.3624</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9EFA9467-E98C-4CCE-8D38-9DEE27B6C115" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582874.374 5920690.2457 582865.7947 5920696.3624 582858.8284 5920686.5915 
582867.4077 5920680.4748 582874.374 5920690.2457 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_492D9730-A0F3-489A-8E6E-7DFCD669C8A8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582858.8284 5920680.4748</gml:lowerCorner>
          <gml:upperCorner>582874.374 5920696.3624</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_1CD9B276-8E70-454F-AA41-9DD4BDC52B41" srsName="EPSG:25832">
          <gml:posList>582874.374 5920690.2457 582867.4077 5920680.4748 582858.8284 5920686.5915 
582865.7947 5920696.3624 582874.374 5920690.2457 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_AE84CEA5-78ED-45C9-97A2-1762527D07E5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582883.8435 5920448.7934</gml:lowerCorner>
          <gml:upperCorner>582968.453 5920570.9206</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E597F26C-D5F7-44CD-A993-881C4E1AD3FC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582949.5866 5920463.0176 582950.207 5920464.081 582968.453 5920495.357 
582955.01 5920503.201 582930.3383 5920517.597 582934.8786 5920525.8132 
582944.8822 5920532.4411 582929.9604 5920554.9629 582931.325 5920555.867 
582925.076 5920565.299 582921.3515 5920570.9206 582915.7419 5920558.5659 
582909.399 5920544.596 582904.8285 5920532.2261 582904.7604 5920532.0417 
582906.4547 5920531.4157 582898.0192 5920508.5854 582883.8435 5920472.0592 
582887.121 5920470.798 582892.188 5920468.848 582900.537 5920465.636 
582917.935 5920458.8471 582919.1239 5920461.894 582922.648 5920460.5188 
582921.4591 5920457.472 582928.235 5920454.828 582936.924 5920450.868 
582941.2885 5920448.7934 582949.5866 5920463.0176 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_EB61E825-9A93-4099-A7D6-7E71E0B06FE4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582921.3515 5920444.652</gml:lowerCorner>
          <gml:upperCorner>582980.1535 5920578.344</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AA762CF7-6A7C-4DBE-8422-6B2F5072EBE0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582980.1535 5920491.6103 582969.706 5920504.221 582965.934 5920509.058 
582962.258 5920514.042 582958.708 5920518.977 582955.258 5920524.091 
582951.97 5920529.249 582948.802 5920534.589 582946.8119 5920538.1405 
582942.865 5920545.184 582938.7 5920552.795 582938.1742 5920553.7561 
582924.722 5920578.344 582921.4437 5920571.1237 582921.3515 5920570.9206 
582925.076 5920565.299 582931.325 5920555.867 582929.9604 5920554.9629 
582944.8822 5920532.4411 582934.8786 5920525.8132 582930.3383 5920517.597 
582955.01 5920503.201 582968.453 5920495.357 582950.207 5920464.081 
582949.5866 5920463.0176 582941.2885 5920448.7934 582950.001 5920444.652 
582951.1255 5920447.6947 582954.234 5920446.5459 582980.1535 5920491.6103 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_383FC3C0-B475-405A-8772-443CD79823D7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582916.2575 5920549.5154</gml:lowerCorner>
          <gml:upperCorner>582931.325 5920565.299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_24A71877-51DC-4B66-A79C-B8215733CFBC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582929.9604 5920554.9629 582931.325 5920555.867 582925.076 5920565.299 
582918.84 5920561.168 582916.2575 5920557.7877 582921.7382 5920549.5154 
582929.9604 5920554.9629 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_FE8DC829-DD49-4BF7-BA7E-D3B7D7955A83">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582916.2575 5920549.5154</gml:lowerCorner>
          <gml:upperCorner>582931.325 5920565.299</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_FB0CEDBE-7A3E-4829-AD6E-B2F34E989144" srsName="EPSG:25832">
          <gml:posList>582931.325 5920555.867 582929.9604 5920554.9629 582921.7382 5920549.5154 
582916.2575 5920557.7877 582918.84 5920561.168 582925.076 5920565.299 
582931.325 5920555.867 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_484E9CBC-B902-4744-A365-0449B95CD9B7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582929.0483 5920525.8132</gml:lowerCorner>
          <gml:upperCorner>582944.8822 5920541.2409</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_30B4E44B-E1DA-4456-8CB1-7073468A67E9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582944.8822 5920532.4411 582939.0519 5920541.2409 582929.0483 5920534.6131 
582930.8106 5920531.9532 582934.8786 5920525.8132 582944.8822 5920532.4411 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_7A05B7F5-7EAD-40E7-893D-97E09DC65592">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582929.0483 5920525.8132</gml:lowerCorner>
          <gml:upperCorner>582944.8822 5920541.2409</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_44A64C07-CE69-4286-86CB-641289E55EDC" srsName="EPSG:25832">
          <gml:posList>582944.8822 5920532.4411 582934.8786 5920525.8132 582930.8106 5920531.9532 
582929.0483 5920534.6131 582939.0519 5920541.2409 582944.8822 5920532.4411 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8090BDC0-67BD-42C1-B03E-97F436C30015">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582898.698 5920468.644</gml:lowerCorner>
          <gml:upperCorner>582932.915 5920509.445</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 3 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2978A7AD-50C8-4CD1-B5C2-EA958ED21254" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582932.915 5920500.249 582917.213 5920509.445 582898.698 5920477.844 
582914.407 5920468.644 582932.915 5920500.249 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_CB652C63-3402-4BDE-A74C-71B559E49DA3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582898.698 5920468.644</gml:lowerCorner>
          <gml:upperCorner>582932.915 5920509.445</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_CF9A02C7-CF3B-4626-B0A9-01602ACF9A0B" srsName="EPSG:25832">
          <gml:posList>582914.407 5920468.644 582898.698 5920477.844 582917.213 5920509.445 
582932.915 5920500.249 582914.407 5920468.644 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_D3494573-2816-4EF1-89F4-5C60DD80BF14">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582936.133 5920463.0177</gml:lowerCorner>
          <gml:upperCorner>582968.453 5920503.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 5 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3198929F-390F-4FAA-B1B7-B0C3D2DB79DA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582950.207 5920464.081 582968.453 5920495.357 582955.01 5920503.201 
582936.133 5920470.871 582949.5864 5920463.0177 582950.207 5920464.081 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_80D81B91-51E8-4A99-8A08-9DD15596C804">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582936.133 5920463.0177</gml:lowerCorner>
          <gml:upperCorner>582968.453 5920503.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_45F0297E-32CE-47F2-8D7B-922735C2602C" srsName="EPSG:25832">
          <gml:posList>582949.5864 5920463.0177 582936.133 5920470.871 582955.01 5920503.201 
582968.453 5920495.357 582950.207 5920464.081 582949.5864 5920463.0177 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_ErhaltungsBereichFlaeche gml:id="Gml_ADA120DB-63D1-4F64-A78B-3DD86886442D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582887.121 5920444.652</gml:lowerCorner>
          <gml:upperCorner>582980.1535 5920552.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_964AAC48-228A-493E-8FF2-91C2100BA7D1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_63297149-B8A4-49B9-9671-E6AB694C137C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582962.258 5920514.042 582958.708 5920518.977 582955.258 5920524.091 
582951.97 5920529.249 582948.802 5920534.589 582946.8119 5920538.1405 
582942.865 5920545.184 582938.7 5920552.795 582934.9062 5920547.4981 
582908.879 5920511.159 582903.553 5920514.341 582887.121 5920470.798 
582892.188 5920468.848 582900.537 5920465.636 582917.935 5920458.8471 
582919.1239 5920461.894 582922.648 5920460.5188 582921.4591 5920457.472 
582928.235 5920454.828 582936.924 5920450.868 582941.2885 5920448.7934 
582950.001 5920444.652 582951.1255 5920447.6947 582954.234 5920446.5459 
582980.1535 5920491.6103 582969.706 5920504.221 582965.934 5920509.058 
582962.258 5920514.042 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:grund>1000</xplan:grund>
    </xplan:BP_ErhaltungsBereichFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_D9DC048D-E2ED-40F0-8B9D-A7654D8D0887">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582910.3932 5920444.652</gml:lowerCorner>
          <gml:upperCorner>582980.1535 5920552.795</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D0464A51-DD68-4E13-9BB5-CBF9C7CA5C0E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582942.865 5920545.184 582938.7 5920552.795 582937.3592 5920540.1194 
582939.0519 5920541.2409 582944.8822 5920532.4411 582935.7451 5920526.3873 
582932.915 5920500.249 582914.407 5920468.644 582910.3932 5920461.79 
582917.935 5920458.8471 582919.1239 5920461.894 582922.648 5920460.5188 
582921.4591 5920457.472 582927.0383 5920455.2949 582936.133 5920470.871 
582955.01 5920503.201 582968.453 5920495.357 582950.207 5920464.081 
582949.5864 5920463.0177 582941.2885 5920448.7934 582950.001 5920444.652 
582951.1255 5920447.6947 582954.234 5920446.5459 582980.1535 5920491.6103 
582969.706 5920504.221 582965.934 5920509.058 582962.258 5920514.042 
582958.708 5920518.977 582955.258 5920524.091 582951.97 5920529.249 
582948.802 5920534.589 582946.8119 5920538.1405 582942.865 5920545.184 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_A3366757-B189-4B02-A2B3-9D77C83E6E71">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582971.536 5920491.6074</gml:lowerCorner>
          <gml:upperCorner>582971.536 5920491.6074</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_E21D7FDB-B9A2-45AB-A492-836E1B48843F" srsName="EPSG:25832">
          <gml:pos>582971.536 5920491.6074</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_23C66647-D7C0-43E8-ACDD-73523A64CE85">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582968.5753 5920486.0148</gml:lowerCorner>
          <gml:upperCorner>582968.5753 5920486.0148</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_01045548-3E63-4E6F-B912-AAA9C7483BDF" srsName="EPSG:25832">
          <gml:pos>582968.5753 5920486.0148</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_872422DB-293D-4289-9099-47E947427CE2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582965.45 5920480.4223</gml:lowerCorner>
          <gml:upperCorner>582965.45 5920480.4223</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_88130A9E-5D13-4910-AE2D-F0848599A873" srsName="EPSG:25832">
          <gml:pos>582965.45 5920480.4223</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_BCDA5A71-5FB7-44D4-8ACD-B476DCE4F466">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582962.6537 5920474.9942</gml:lowerCorner>
          <gml:upperCorner>582962.6537 5920474.9942</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_E01942F4-7F71-4494-87D3-22DF2E606C26" srsName="EPSG:25832">
          <gml:pos>582962.6537 5920474.9942</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_F6D32183-3815-413B-BD9A-2641C2FB66A3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583008.8016 5920386.7003</gml:lowerCorner>
          <gml:upperCorner>583059.759 5920445.4971</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6C5CD0ED-AEF4-47EB-A5FF-4C51410A5E56" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583059.759 5920413.718 583054.974 5920417.253 583054.935 5920417.281 
583037.564 5920429.643 583033.665 5920432.576 583029.914 5920435.645 
583026.315 5920438.792 583022.369 5920442.54 583021.37 5920443.514 
583019.397 5920445.438 583019.3394 5920445.4971 583018.445 5920444.664 
583013.109 5920439.694 583008.8016 5920435.7032 583015.071 5920431.7276 
583016.2223 5920430.9975 583009.344 5920418.0813 583021.878 5920410.319 
583031.479 5920425.822 583041.962 5920419.328 583029.556 5920399.295 
583049.8936 5920386.7003 583059.759 5920413.718 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_0701EDAC-C5FE-48D4-8753-29732C7C06FB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582996.226 5920427.074</gml:lowerCorner>
          <gml:upperCorner>583015.071 5920436.738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_738B33C4-F88E-418B-B6F1-0F9EBE6C692E" srsName="EPSG:25832">
          <gml:posList>583015.071 5920431.7276 583012.584 5920427.074 582996.226 5920436.738 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_7D7AF5B5-9721-4CF5-A9D0-563707E9D12F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582996.226 5920427.074</gml:lowerCorner>
          <gml:upperCorner>583019.3394 5920455.55</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DCF968C4-ED4E-41BF-9C85-0EE907E679F1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583015.071 5920431.7276 583008.8016 5920435.7032 583013.109 5920439.694 
583018.445 5920444.664 583019.3394 5920445.4971 583016.088 5920448.835 
583012.811 5920452.352 583010.028 5920455.55 583005.7632 5920449.7372 
582996.226 5920436.738 583012.584 5920427.074 583015.071 5920431.7276 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.45</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_C5A0ACCF-437E-4CA0-8697-EF9D08AC7F07">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582981.0904 5920353.5659</gml:lowerCorner>
          <gml:upperCorner>583049.8936 5920439.063</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3AB11419-300A-45B9-8774-4D10ACB37DB6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582996.226 5920436.738 582992.292 5920439.063 582989.1472 5920430.7325 
582989.0718 5920430.5328 582990.2054 5920430.1048 582981.0904 5920405.9593 
582998.133 5920397.029 582995.352 5920394.328 582987.5702 5920377.3706 
582987.0424 5920376.2205 582987.6573 5920375.9384 582985.4388 5920371.1041 
583036.6901 5920353.5659 583041.1151 5920365.7982 583041.95 5920364.945 
583042.258 5920365.789 583049.8936 5920386.7003 583029.556 5920399.295 
583041.962 5920419.328 583031.479 5920425.822 583021.878 5920410.319 
583009.344 5920418.0813 583016.2223 5920430.9975 583015.071 5920431.7276 
583012.584 5920427.074 582996.226 5920436.738 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_3E86C0F2-6796-4B74-86F0-0C31BDC86B85">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583003.0052 5920435.4744</gml:lowerCorner>
          <gml:upperCorner>583018.445 5920452.207</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0D0D1088-C1CA-4934-B1D3-18A6AEC285F3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583008.8016 5920435.7032 583013.109 5920439.694 583018.445 5920444.664 
583011.298 5920452.207 583006.0323 5920447.2177 583003.0052 5920441.3314 
583008.5547 5920435.4744 583008.8016 5920435.7032 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_3CB511B1-507F-4996-BAFA-7B0CB44699C4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582992.6074 5920378.3067</gml:lowerCorner>
          <gml:upperCorner>583007.7326 5920392.678</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B4FEC92C-4DD4-4CF6-A742-61E436169C22" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583004.007 5920379.279 583007.115 5920386.527 583007.7326 5920387.9672 
583001.429 5920390.671 582996.75 5920392.678 582993.025 5920383.989 
582992.6074 5920383.015 583003.5901 5920378.3067 583004.007 5920379.279 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_5F29B674-7C79-461A-AEDA-26E4A9ED8634">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583030.0599 5920373.5411</gml:lowerCorner>
          <gml:upperCorner>583045.4692 5920389.399</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8472EFBD-44CF-4C1A-997A-5E25D8B85265" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583039.1469 5920374.028 583045.4692 5920383.6164 583043.798 5920384.719 
583036.705 5920389.399 583030.381 5920379.808 583030.0599 5920379.3211 
583038.8259 5920373.5411 583039.1469 5920374.028 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_20138AB3-A17D-4713-A87B-6B984D0386F9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583003.0052 5920435.4744</gml:lowerCorner>
          <gml:upperCorner>583018.445 5920452.207</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_88D0EB2C-A404-4CAF-AEA2-C85E2D327A03" srsName="EPSG:25832">
          <gml:posList>583011.298 5920452.207 583018.445 5920444.664 583013.109 5920439.694 
583008.8016 5920435.7032 583008.5547 5920435.4744 583003.0052 5920441.3314 
583006.0323 5920447.2177 583011.298 5920452.207 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_AA360C07-A8DB-4F23-9B93-8BF51BC1E714">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582992.6074 5920378.3067</gml:lowerCorner>
          <gml:upperCorner>583007.7326 5920392.678</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_5E97D5F1-3D12-4164-949C-256A07A38D56" srsName="EPSG:25832">
          <gml:posList>582996.75 5920392.678 583001.429 5920390.671 583007.7326 5920387.9672 
583007.115 5920386.527 583004.007 5920379.279 583003.5901 5920378.3067 
582992.6074 5920383.015 582993.025 5920383.989 582996.75 5920392.678 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_1202D959-E965-43E8-A9C0-274BAD07C0AD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583030.0599 5920373.5411</gml:lowerCorner>
          <gml:upperCorner>583045.4692 5920389.399</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_965DE571-C8DC-4C36-92F1-C3F921CA80E1" srsName="EPSG:25832">
          <gml:posList>583039.1469 5920374.028 583038.8259 5920373.5411 583030.0599 5920379.3211 
583030.381 5920379.808 583036.705 5920389.399 583043.798 5920384.719 
583045.4692 5920383.6164 583039.1469 5920374.028 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_AF5BE1E1-D638-480D-9FCD-D3F620CE26D7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583021.878 5920399.295</gml:lowerCorner>
          <gml:upperCorner>583041.962 5920425.822</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C102A25A-607E-4970-890F-170743041569" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583041.962 5920419.328 583031.479 5920425.822 583021.878 5920410.319 
583028.47 5920406.237 583025.664 5920401.705 583029.556 5920399.295 
583041.962 5920419.328 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_15C4A4D7-47C9-4B54-B35F-DCAD781414F5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583021.878 5920399.295</gml:lowerCorner>
          <gml:upperCorner>583041.962 5920425.822</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B11604CD-5A7C-4001-A27C-964885D22F8D" srsName="EPSG:25832">
          <gml:posList>583029.556 5920399.295 583025.664 5920401.705 583028.47 5920406.237 
583021.878 5920410.319 583031.479 5920425.822 583041.962 5920419.328 
583029.556 5920399.295 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_ErhaltungsBereichFlaeche gml:id="Gml_DDBD348C-F587-4BBC-A741-0618D20A27F7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582998.133 5920364.945</gml:lowerCorner>
          <gml:upperCorner>583059.759 5920442.54</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_964AAC48-228A-493E-8FF2-91C2100BA7D1" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_57E3EF59-273C-410B-BD5B-96F8A19BF934" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583042.258 5920365.789 583049.8936 5920386.7003 583059.759 5920413.718 
583054.974 5920417.253 583054.935 5920417.281 583037.564 5920429.643 
583033.665 5920432.576 583029.914 5920435.645 583026.315 5920438.792 
583022.369 5920442.54 583016.2223 5920430.9975 583009.344 5920418.0813 
582998.133 5920397.029 583023.531 5920379.266 583037.487 5920369.506 
583041.1151 5920365.7982 583041.95 5920364.945 583042.258 5920365.789 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:grund>1000</xplan:grund>
    </xplan:BP_ErhaltungsBereichFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_69E4F920-D1C8-41AD-8D9C-F3008DA32FBA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582981.0904 5920370.4888</gml:lowerCorner>
          <gml:upperCorner>583059.759 5920445.4971</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1C1E2A29-DE29-470E-8874-3AD7A446592D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583037.564 5920429.643 583033.665 5920432.576 583029.914 5920435.645 
583026.315 5920438.792 583022.369 5920442.54 583021.37 5920443.514 
583019.397 5920445.438 583019.3394 5920445.4971 583018.445 5920444.664 
583013.109 5920439.694 583008.8016 5920435.7032 583008.5547 5920435.4744 
583004.5876 5920431.7981 582983.4501 5920412.2102 582981.0904 5920405.9593 
582998.133 5920397.029 582995.352 5920394.328 582987.5702 5920377.3706 
582987.0424 5920376.2205 582987.6573 5920375.9384 582985.4388 5920371.1041 
582987.2368 5920370.4888 582992.6074 5920383.015 582993.025 5920383.989 
582996.75 5920392.678 583021.878 5920410.319 583031.479 5920425.822 
583041.962 5920419.328 583032.4786 5920404.0144 583059.759 5920413.718 
583054.974 5920417.253 583054.935 5920417.281 583037.564 5920429.643 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_2F71C6F6-7FF5-486B-928A-10638EBE283C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582867.3792 5920490.734</gml:lowerCorner>
          <gml:upperCorner>582994.1785 5920676.4518</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F86537B2-F5EB-4940-BDB1-70A6091C3D4F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582992.9524 5920491.5129 582994.1785 5920492.7544 582978.1 5920511.5919 
582969.2427 5920522.4017 582966.0522 5920526.7351 582961.1949 5920535.4971 
582960.9092 5920537.0686 582959.1473 5920539.8782 582951.1471 5920555.1165 
582940.8613 5920573.4978 582935.9564 5920582.2122 582932.1944 5920589.1171 
582928.6229 5920595.022 582921.0514 5920606.4508 582913.5274 5920616.8319 
582903.1463 5920631.0703 582897.4795 5920639.8799 582887.4317 5920654.6897 
582880.6068 5920663.4267 582875.681 5920669.4842 582874.1822 5920671.3273 
582869.3185 5920676.4518 582867.3792 5920673.8742 582867.429 5920673.822 
582870.1788 5920670.775 582871.13 5920669.721 582874.409 5920665.792 
582877.537 5920661.928 582880.579 5920657.961 582880.9264 5920657.4793 
582883.55 5920653.841 582899.0582 5920632.1207 582906.1786 5920622.1482 
582912.207 5920613.705 582914.7164 5920610.2127 582914.893 5920609.967 
582917.504 5920606.234 582920.101 5920602.431 582922.598 5920598.618 
582923.071 5920597.877 582925.052 5920594.776 582927.463 5920590.911 
582929.841 5920586.951 582932.053 5920582.968 582941.2263 5920566.0806 
582950.309 5920549.36 582951.8373 5920546.5844 582953.176 5920544.153 
582956.167 5920538.939 582959.318 5920533.791 582962.548 5920528.779 
582965.971 5920523.798 582969.449 5920518.936 582973.06 5920514.089 
582976.787 5920509.376 582986.1578 5920498.0296 582992.1832 5920490.734 
582992.9524 5920491.5129 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_F6CA0C26-EA14-430F-A547-EE06CDAFD398">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583009.9628 5920457.0711</gml:lowerCorner>
          <gml:upperCorner>583064.4129 5920508.7369</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_02D07DFE-48E4-416E-B2C2-EDBFE5D123E7" srsName="EPSG:25832">
          <gml:posList>583064.4129 5920457.0711 583009.9628 5920508.7369 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_FB489797-1F58-434F-A245-86F10E2315DA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582992.1832 5920420.589</gml:lowerCorner>
          <gml:upperCorner>583078.0385 5920508.7369</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_ACC41A25-F8CA-40B8-854A-1B59BA74630B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583074.0803 5920436.0348 583073.3306 5920436.5826 583078.0385 5920444.1239 
583064.4129 5920457.0711 583009.9628 5920508.7369 582994.1785 5920492.7544 
582992.9524 5920491.5129 582992.1832 5920490.734 582995.395 5920486.845 
583000.4335 5920480.7425 583003.067 5920477.553 583014.965 5920463.411 
583022.11 5920455.356 583025.547 5920451.731 583029.48 5920447.584 
583028.836 5920446.818 583039.477 5920437.652 583043.095 5920435.176 
583061.495 5920422.579 583064.217 5920420.589 583074.0803 5920436.0348 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_0D64139C-8F97-413B-97E3-0F3DF7F0A4E8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583009.9628 5920457.0711</gml:lowerCorner>
          <gml:upperCorner>583105.441 5920569.3435</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung1>(A)</xplan:gliederung1>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_76C0DE3D-77B7-41CF-BC69-ECE97AB834AD" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_68BD72BA-5497-470D-AC7F-5B704EC2AFFE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583087.6682 5920487.8213 583089.327 5920490.0147 583079.2001 5920499.9715 
583105.441 5920528.2891 583101.7233 5920531.7341 583104.192 5920534.413 
583105.1058 5920535.4046 583092.2392 5920547.642 583069.4221 5920569.3435 
583055.2689 5920554.612 583009.9628 5920508.7369 583064.4129 5920457.0711 
583087.6682 5920487.8213 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GenerischesObjekt gml:id="Gml_DFFF5B72-C41A-4523-8578-6C1C2F797F61">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582992.1832 5920451.731</gml:lowerCorner>
          <gml:upperCorner>583105.441 5920569.3435</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung2>(Z)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_ECA0485B-4E54-46AD-A2ED-1B67F50043BE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583079.2001 5920499.9715 583105.441 5920528.2891 583101.7233 5920531.7341 
583104.192 5920534.413 583105.1058 5920535.4046 583092.2392 5920547.642 
583069.4221 5920569.3435 583055.2689 5920554.612 583009.9628 5920508.7369 
582994.1785 5920492.7544 582992.9524 5920491.5129 582992.1832 5920490.734 
582995.395 5920486.845 583000.4335 5920480.7425 583003.067 5920477.553 
583014.965 5920463.411 583022.11 5920455.356 583025.547 5920451.731 
583044.322 5920472.022 583046.4294 5920474.135 583072.1332 5920499.9078 
583075.6946 5920496.4061 583075.8001 5920496.3024 583079.2001 5920499.9715 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung codeSpace="www.mysynergis.com/XPlanungR/4.1/0">4000</xplan:zweckbestimmung>
    </xplan:BP_GenerischesObjekt>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_499F7BB0-C9D7-479B-8A8C-FF97728592FC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582994.1785 5920485.993</gml:lowerCorner>
          <gml:upperCorner>583105.441 5920569.3435</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_D96C12C7-F959-4137-BCB9-73ADDD01B224" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DDF31E63-5F1C-49B7-9AE8-807D0CD208A1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583069.4221 5920569.3435 583055.2689 5920554.612 583009.9628 5920508.7369 
582994.1785 5920492.7544 582995.9934 5920490.6281 582999.0644 5920490.5861 
583013.5905 5920505.2947 583058.8506 5920551.1231 583066.1169 5920558.6863 
583098.2141 5920528.1586 583098.3678 5920528.0124 583072.2332 5920499.8094 
583075.6946 5920496.4061 583075.8001 5920496.3024 583085.8215 5920486.4493 
583086.2856 5920485.993 583087.6682 5920487.8213 583089.327 5920490.0147 
583079.2001 5920499.9715 583105.441 5920528.2891 583101.7233 5920531.7341 
583104.192 5920534.413 583105.1058 5920535.4046 583092.2392 5920547.642 
583069.4221 5920569.3435 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
      <xplan:gegenstand>2000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_00F95776-A5C1-4B06-8B60-795FA42AF7E4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583048.199 5920425.7274</gml:lowerCorner>
          <gml:upperCorner>583066.172 5920443.7007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7F4DBD03-8CA4-4A37-AF80-1CDBDB2559DD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583058.599 5920427.048 583066.172 5920434.811 583064.06 5920436.871 
583059.593 5920441.231 583057.0627 5920443.7007 583053.422 5920439.975 
583048.199 5920434.63 583050.826 5920432.064 583052.784 5920430.151 
583055.197 5920427.793 583057.3108 5920425.7274 583058.599 5920427.048 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_C8CCF2DA-FAAF-4F42-A59C-653B935FEF57">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583028.836 5920437.652</gml:lowerCorner>
          <gml:upperCorner>583054.7074 5920464.132</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0C19022C-53D1-416B-8187-0B97263335CA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583047.079 5920446.406 583054.7074 5920455.1903 583050.642 5920458.662 
583046.485 5920462.212 583044.223 5920464.132 583040.748 5920460.05 
583029.48 5920447.584 583028.836 5920446.818 583039.477 5920437.652 
583047.079 5920446.406 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_839A71BA-BC16-46A7-A24A-D518F51163CA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583014.358 5920455.081</gml:lowerCorner>
          <gml:upperCorner>583034.392 5920487.181</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_042E236E-4267-4872-B119-A10AB608B47F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583034.392 5920462.804 583027.16 5920469.933 583033.69 5920476.497 
583022.947 5920487.181 583018.788 5920483.002 583024.457 5920477.363 
583014.358 5920467.212 583019.124 5920462.627 583018.879 5920462.378 
583024.908 5920456.432 583025.154 5920456.682 583026.776 5920455.081 
583034.392 5920462.804 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_CABD50EE-B47A-4D8A-82A7-FF6A6661920A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583053.593 5920466.697</gml:lowerCorner>
          <gml:upperCorner>583075.881 5920489.41</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9BF094D8-7795-4D70-92E0-1979866BA89D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583075.881 5920481.215 583066.999 5920489.41 583053.593 5920474.886 
583062.467 5920466.697 583075.881 5920481.215 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A3275B07-D68A-472C-99E0-834E33200F04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583021.3894 5920477.456</gml:lowerCorner>
          <gml:upperCorner>583088.9027 5920546.2422</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9CFB4423-62DE-4CE3-90C1-F681CEE492F1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583046.4169 5920478.1096 583052.2382 5920484.3176 583066.245 5920499.255 
583068.262 5920501.406 583088.9027 5920523.418 583086.0035 5920526.0514 
583064.7957 5920546.2422 583044.01 5920524.377 583023.06 5920502.339 
583021.3894 5920500.5816 583043.098 5920479.914 583044.298 5920478.824 
583045.804 5920477.456 583046.4169 5920478.1096 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_2EDC9AE7-69F2-4EFA-8ABC-57A76B40DD6E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583048.199 5920425.7274</gml:lowerCorner>
          <gml:upperCorner>583066.172 5920443.7007</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6329379F-176F-406B-BF6F-4FA608591355" srsName="EPSG:25832">
          <gml:posList>583066.172 5920434.811 583058.599 5920427.048 583057.3108 5920425.7274 
583055.197 5920427.793 583052.784 5920430.151 583050.826 5920432.064 
583048.199 5920434.63 583053.422 5920439.975 583057.0627 5920443.7007 
583059.593 5920441.231 583064.06 5920436.871 583066.172 5920434.811 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_65E42267-695C-4A8C-BD4A-BC059A74B998">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583028.836 5920437.652</gml:lowerCorner>
          <gml:upperCorner>583054.7074 5920464.132</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_FBE5478B-6067-492B-AE43-FD75BFEEE336" srsName="EPSG:25832">
          <gml:posList>583044.223 5920464.132 583046.485 5920462.212 583050.642 5920458.662 
583054.7074 5920455.1903 583047.079 5920446.406 583039.477 5920437.652 
583028.836 5920446.818 583029.48 5920447.584 583040.748 5920460.05 
583044.223 5920464.132 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_33FA752A-0A03-424B-B6A5-B80E614830AD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583014.358 5920455.081</gml:lowerCorner>
          <gml:upperCorner>583034.392 5920487.181</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_E091265F-0C53-4952-92BF-5A986BF80F61" srsName="EPSG:25832">
          <gml:posList>583027.16 5920469.933 583034.392 5920462.804 583026.776 5920455.081 
583025.154 5920456.682 583024.908 5920456.432 583018.879 5920462.378 
583019.124 5920462.627 583014.358 5920467.212 583024.457 5920477.363 
583018.788 5920483.002 583022.947 5920487.181 583033.69 5920476.497 
583027.16 5920469.933 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_8928C350-D995-47B7-B27D-1A54995BA76A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583053.593 5920466.697</gml:lowerCorner>
          <gml:upperCorner>583075.881 5920489.41</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_63511C7D-55F2-45E7-8057-C0CD8550069D" srsName="EPSG:25832">
          <gml:posList>583062.467 5920466.697 583053.593 5920474.886 583066.999 5920489.41 
583075.881 5920481.215 583062.467 5920466.697 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_7B929873-985A-4C34-B96B-ACD7937F5C17">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583021.3894 5920477.456</gml:lowerCorner>
          <gml:upperCorner>583088.9027 5920546.2422</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_60BC43C6-BD68-420E-8ABD-89F645AC20A0" srsName="EPSG:25832">
          <gml:posList>583064.7957 5920546.2422 583086.0035 5920526.0514 583088.9027 5920523.418 
583068.262 5920501.406 583066.245 5920499.255 583052.2382 5920484.3176 
583046.4169 5920478.1096 583045.804 5920477.456 583044.298 5920478.824 
583043.098 5920479.914 583021.3894 5920500.5816 583023.06 5920502.339 
583044.01 5920524.377 583064.7957 5920546.2422 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_6891A9A3-9183-456D-A601-79F6FDB3BF03">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583107.7892 5920593.3855</gml:lowerCorner>
          <gml:upperCorner>583190.278000001 5920759.1838</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_003FFBC2-8318-4AAD-8BD8-B9425FDD04F7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583152.9525 5920745.3347 583112.257 5920759.1838 583107.7892 5920725.5805 
583118.538 5920720.556 583121.918 5920718.823 583113.48 5920702.875 
583116.273 5920701.398 583109.21 5920688.048 583122.716 5920680.903 
583146.1719 5920668.4943 583141.869 5920646.461 583139.816 5920636.327 
583139.6372 5920635.4445 583142.613 5920634.895 583154.9929 5920632.6089 
583158.6956 5920631.9251 583157.835 5920627.405 583156.658 5920621.223 
583167.697 5920619.119 583174.5576 5920617.8114 583174.089 5920615.355 
583172.6632 5920607.8811 583170.4106 5920596.0732 583171.473 5920595.886 
583180.355 5920594.321 583185.6645 5920593.3855 583188.57 5920611.997 
583190.278 5920622.931 583176.052 5920625.645 583181.0583 5920651.8867 
583159.4575 5920656.0077 583165.093 5920685.543 583172.972 5920726.83 
583170.54 5920736.292 583169.4833 5920736.4882 583170.0458 5920739.5177 
583154.9354 5920744.6599 583152.9525 5920745.3347 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_443B9F63-69ED-4D88-888C-744D472F3DE2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583103.0288 5920571.135</gml:lowerCorner>
          <gml:upperCorner>583185.6645 5920725.5805</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_313FD659-5556-4B8E-874A-1F8EAFAF2C1F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583185.6645 5920593.3855 583180.355 5920594.321 583171.473 5920595.886 
583170.4106 5920596.0732 583172.6632 5920607.8811 583174.089 5920615.355 
583174.5576 5920617.8114 583167.697 5920619.119 583156.658 5920621.223 
583157.835 5920627.405 583158.6956 5920631.9251 583154.9929 5920632.6089 
583142.613 5920634.895 583139.6372 5920635.4445 583139.816 5920636.327 
583141.869 5920646.461 583146.1719 5920668.4943 583122.716 5920680.903 
583109.21 5920688.048 583116.273 5920701.398 583113.48 5920702.875 
583121.918 5920718.823 583118.538 5920720.556 583107.7892 5920725.5805 
583103.0288 5920689.7761 583104.5673 5920687.0518 583105.612 5920685.202 
583109.84 5920677.259 583110.2204 5920676.4831 583114.245 5920668.273 
583116.9913 5920661.6608 583117.331 5920660.843 583120.243 5920653.317 
583121.9523 5920649.6783 583125.355 5920642.435 583130.023 5920633.595 
583135.365 5920625.353 583141.635 5920617.38 583150.228 5920607.636 
583165.291 5920590.578 583176.108 5920578.332 583182.191 5920571.135 
583185.6645 5920593.3855 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_D41B9F10-9D96-4AE8-A4E3-6386E1C0E00C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583170.4106 5920594.321</gml:lowerCorner>
          <gml:upperCorner>583182.9591 5920610.8089</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FCB98C63-E694-4BC6-83CF-40F0A8373EF1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583182.439 5920606.143 583182.9591 5920609.0933 583173.2217 5920610.8089 
583172.6632 5920607.8811 583170.4106 5920596.0732 583171.473 5920595.886 
583180.355 5920594.321 583182.439 5920606.143 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_20FAC8A4-7BF0-4D61-A914-59F1E965C850">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583156.658 5920619.119</gml:lowerCorner>
          <gml:upperCorner>583170.6837 5920636.9407</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B5373462-C95A-4E9D-B30D-32C625743CB2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583170.169 5920632.129 583170.6837 5920634.8378 583159.6536 5920636.9401 
583159.6506 5920636.9407 583158.6956 5920631.9251 583157.835 5920627.405 
583156.658 5920621.223 583167.697 5920619.119 583170.169 5920632.129 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A6C5C021-DA89-4A34-94DA-461BF87B4066">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583139.6372 5920633.5314</gml:lowerCorner>
          <gml:upperCorner>583152.5393 5920649.1714</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4643DCB8-A5D0-4F18-8577-968F454969AB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583152.5393 5920647.2987 583142.3983 5920649.1714 583141.869 5920646.461 
583139.816 5920636.327 583139.6372 5920635.4445 583142.613 5920634.895 
583149.997 5920633.5314 583152.5393 5920647.2987 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_730F17CC-8FCD-4218-8E86-A6311B750941">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583170.4106 5920594.321</gml:lowerCorner>
          <gml:upperCorner>583182.9591 5920610.8089</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_562F4996-4BE8-47BF-861A-E8908A51E0FD" srsName="EPSG:25832">
          <gml:posList>583170.4106 5920596.0732 583172.6632 5920607.8811 583173.2217 5920610.8089 
583182.9591 5920609.0933 583182.439 5920606.143 583180.355 5920594.321 
583171.473 5920595.886 583170.4106 5920596.0732 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_B55E80C9-49C9-4A7B-9243-9BC79AA21A88">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583156.658 5920619.119</gml:lowerCorner>
          <gml:upperCorner>583170.6837 5920636.9407</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_668F1F66-588B-4A36-83AD-29B7DE81251D" srsName="EPSG:25832">
          <gml:posList>583156.658 5920621.223 583157.835 5920627.405 583158.6956 5920631.9251 
583159.6506 5920636.9407 583159.6536 5920636.9401 583170.6837 5920634.8378 
583170.169 5920632.129 583167.697 5920619.119 583156.658 5920621.223 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_DB7A3F51-9E0D-4BA2-B971-12FCD7151C8C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583139.6372 5920633.5314</gml:lowerCorner>
          <gml:upperCorner>583152.5393 5920649.1714</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_FEF2485A-0042-4BC5-BC2F-100468CE2E8D" srsName="EPSG:25832">
          <gml:posList>583139.6372 5920635.4445 583139.816 5920636.327 583141.869 5920646.461 
583142.3983 5920649.1714 583152.5393 5920647.2987 583149.997 5920633.5314 
583142.613 5920634.895 583139.6372 5920635.4445 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_935647A4-F8CB-4298-9C48-7DE6ED962C9A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583109.21 5920680.903</gml:lowerCorner>
          <gml:upperCorner>583138.177 5920718.823</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 5 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1CF6FDA1-6E28-4CB6-B849-0EA79BE1D00E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583138.177 5920710.127 583121.918 5920718.823 583113.48 5920702.875 
583116.273 5920701.398 583109.21 5920688.048 583122.716 5920680.903 
583138.177 5920710.127 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_5C3E276E-20B4-4748-B376-8F75072E17F3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583109.21 5920680.903</gml:lowerCorner>
          <gml:upperCorner>583138.177 5920718.823</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_CDEC054B-307F-4D43-AE3F-F9223E33AB21" srsName="EPSG:25832">
          <gml:posList>583109.21 5920688.048 583116.273 5920701.398 583113.48 5920702.875 
583121.918 5920718.823 583138.177 5920710.127 583122.716 5920680.903 
583109.21 5920688.048 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_5B6CF5A2-C73F-4EFE-9DED-60872A665E7D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583142.535 5920709.8</gml:lowerCorner>
          <gml:upperCorner>583167.549 5920739.424</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 3 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8267AABC-0283-4799-A130-0DD898EA2473" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583167.549 5920729.342 583157.076 5920734.123 583152.819 5920736.067 
583145.582 5920739.424 583142.535 5920732.75 583149.819 5920729.425 
583143.923 5920716.513 583148.155 5920714.582 583158.628 5920709.8 
583167.549 5920729.342 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F32EAC8D-748C-4D4B-B13E-F578ED6F9E01">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583142.535 5920709.8</gml:lowerCorner>
          <gml:upperCorner>583167.549 5920739.424</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B857ECAB-06AE-4EE2-85D5-5DDFA8F9AF18" srsName="EPSG:25832">
          <gml:posList>583167.549 5920729.342 583158.628 5920709.8 583148.155 5920714.582 
583143.923 5920716.513 583149.819 5920729.425 583142.535 5920732.75 
583145.582 5920739.424 583152.819 5920736.067 583157.076 5920734.123 
583167.549 5920729.342 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_247929DF-F54C-4AE0-BA3E-2A4FDBF296B8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583111.5883 5920733.988</gml:lowerCorner>
          <gml:upperCorner>583123.8827 5920747.5881</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3E2B0842-0EE2-4E27-9AB9-610025E780A3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583123.599 5920744.197 583123.8827 5920745.8055 583123.8592 5920745.8096 
583113.6591 5920747.5881 583113.014 5920743.906 583112.62 5920741.657 
583111.5883 5920735.7682 583112.18 5920735.665 583121.798 5920733.988 
583123.599 5920744.197 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_C5D319CF-6058-424E-9272-76C0F6D8E6CE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583111.5883 5920733.988</gml:lowerCorner>
          <gml:upperCorner>583123.8827 5920747.5881</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_CE857499-856B-4A67-AEFE-FE7069FBFF8B" srsName="EPSG:25832">
          <gml:posList>583121.798 5920733.988 583112.18 5920735.665 583111.5883 5920735.7682 
583112.62 5920741.657 583113.014 5920743.906 583113.6591 5920747.5881 
583123.8592 5920745.8096 583123.8827 5920745.8055 583123.599 5920744.197 
583121.798 5920733.988 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_50318B8F-BAF9-4206-83F4-BFD3AE199A04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583103.0288 5920607.636</gml:lowerCorner>
          <gml:upperCorner>583172.972 5920759.1838</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B5D900C2-3515-4C63-B99A-5CAF6C972D95" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583109.21 5920688.048 583116.273 5920701.398 583113.48 5920702.875 
583121.918 5920718.823 583111.5883 5920735.7682 583112.62 5920741.657 
583113.014 5920743.906 583113.6591 5920747.5881 583115.4974 5920758.081 
583112.257 5920759.1838 583107.7892 5920725.5805 583103.0288 5920689.7761 
583104.5673 5920687.0518 583105.612 5920685.202 583109.84 5920677.259 
583110.2204 5920676.4831 583114.245 5920668.273 583116.9913 5920661.6608 
583117.331 5920660.843 583120.243 5920653.317 583121.9523 5920649.6783 
583125.355 5920642.435 583130.023 5920633.595 583135.365 5920625.353 
583141.635 5920617.38 583150.228 5920607.636 583154.9929 5920632.6089 
583159.4575 5920656.0077 583165.093 5920685.543 583172.972 5920726.83 
583170.6054 5920736.0374 583167.549 5920729.342 583158.628 5920709.8 
583138.177 5920710.127 583122.716 5920680.903 583109.21 5920688.048 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583142.613 5920634.895 583139.6372 5920635.4445 583139.816 5920636.327 
583141.869 5920646.461 583142.3983 5920649.1714 583152.5393 5920647.2987 
583149.997 5920633.5314 583142.613 5920634.895 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_9D20DFC9-BF9F-49D7-8324-91EA37837768">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583098.3764 5920669.8081</gml:lowerCorner>
          <gml:upperCorner>583149.0886 5920727.9924</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_79C93DFF-38FC-459F-935A-30A10764D777" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583149.0886 5920713.5725 583121.1751 5920727.9924 583098.3764 5920684.3264 
583126.4805 5920669.8081 583149.0886 5920713.5725 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_9D48561F-1084-4CCC-9319-D5C323155433">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583055.8264 5920717.102</gml:lowerCorner>
          <gml:upperCorner>583095.5064 5920786.0444</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D8ACD4AF-D31B-4382-A467-69A19E0DB715" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583088.587 5920720.0013 583089.2845 5920721.8567 583090.255 5920723.2 
583089.8183 5920723.2766 583087.862 5920723.62 583094.3452 5920746.3515 
583095.258 5920749.552 583095.5064 5920750.8609 583077.366 5920757.51 
583064.606 5920762.187 583072.236 5920782.992 583063.9099 5920786.0444 
583055.8264 5920764.0597 583063.719 5920753.725 583069.2019 5920745.7302 
583073.531 5920739.418 583083.825 5920723.41 583087.497 5920717.102 
583088.587 5920720.0013 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_4F23A2A7-EB06-4494-8FC4-B1D855C85637">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583063.9099 5920750.8609</gml:lowerCorner>
          <gml:upperCorner>583107.5232 5920820.0565</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_80AB7892-7182-4F9C-97E1-6DDAFFCE0E3D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583072.925 5920819.9847 583072.4968 5920820.0565 583070.93 5920810.7142 
583069.21 5920800.459 583063.9099 5920786.0444 583072.236 5920782.992 
583064.606 5920762.187 583077.366 5920757.51 583095.5064 5920750.8609 
583096.7068 5920757.1862 583103.0593 5920790.6603 583107.5232 5920814.1819 
583106.9905 5920814.2712 583091.4134 5920816.8838 583089.2616 5920817.2447 
583072.925 5920819.9847 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_82967860-B0D6-41FA-AFEE-28745CE77742">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583064.606 5920757.51</gml:lowerCorner>
          <gml:upperCorner>583084.996 5920782.992</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E536D7DB-D217-488F-AC4E-0D5A22F69459" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583084.996 5920778.314 583072.236 5920782.992 583064.606 5920762.187 
583077.366 5920757.51 583084.996 5920778.314 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_0568A964-04AF-45B7-AA1C-4EA2053C56A1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583064.606 5920757.51</gml:lowerCorner>
          <gml:upperCorner>583084.996 5920782.992</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_3A7622B7-4E82-4621-A0B7-1F5C6A70F29E" srsName="EPSG:25832">
          <gml:posList>583077.366 5920757.51 583064.606 5920762.187 583072.236 5920782.992 
583084.996 5920778.314 583077.366 5920757.51 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_43B112E2-1285-497C-B3AB-0AC735AA5AB1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583088.605 5920774.915</gml:lowerCorner>
          <gml:upperCorner>583098.553 5920790.6706</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 1 Wohnung</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DD979993-56D9-4070-B81B-99869575B4A5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583096.0157 5920789.8438 583093.2778 5920790.6706 583092.5016 5920788.1004 
583092.036 5920788.241 583090.825 5920784.232 583088.605 5920776.883 
583095.123 5920774.915 583097.343 5920782.264 583098.553 5920786.273 
583095.2396 5920787.2736 583096.0157 5920789.8438 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_D3D672D6-2C8F-4FBC-B6EE-FD40D4E662AA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583082.4604 5920798.4236</gml:lowerCorner>
          <gml:upperCorner>583096.5116 5920813.3329</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9952BD60-A983-474E-8BDD-C90BC44AD367" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583096.5116 5920809.6825 583086.612 5920813.3329 583082.4604 5920802.074 
583092.36 5920798.4236 583096.5116 5920809.6825 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_EE8E3818-BE6F-4FB2-AF7F-83E68B73D548">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583088.605 5920774.915</gml:lowerCorner>
          <gml:upperCorner>583098.553 5920790.6706</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_44208B55-2692-45F5-B61E-88C3F1F99451" srsName="EPSG:25832">
          <gml:posList>583095.123 5920774.915 583088.605 5920776.883 583090.825 5920784.232 
583092.036 5920788.241 583092.5016 5920788.1004 583093.2778 5920790.6706 
583096.0157 5920789.8438 583095.2396 5920787.2736 583098.553 5920786.273 
583097.343 5920782.264 583095.123 5920774.915 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_771A8D80-8C5F-46CE-B7EB-32843E6A828C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583082.4604 5920798.4236</gml:lowerCorner>
          <gml:upperCorner>583096.5116 5920813.3329</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_9662AD85-92AF-49E9-8B4C-6DCB8F0B4C85" srsName="EPSG:25832">
          <gml:posList>583092.36 5920798.4236 583082.4604 5920802.074 583086.612 5920813.3329 
583096.5116 5920809.6825 583092.36 5920798.4236 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_C3046567-4CEF-4F27-97FC-5CDF5A734A12">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583055.8264 5920717.102</gml:lowerCorner>
          <gml:upperCorner>583107.5232 5920820.0565</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B8764105-88E6-41EB-B4E1-79D023BDE58F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583064.606 5920762.187 583072.236 5920782.992 583072.4968 5920820.0565 
583069.21 5920800.459 583063.9099 5920786.0444 583055.8264 5920764.0597 
583063.719 5920753.725 583069.2019 5920745.7302 583073.531 5920739.418 
583083.825 5920723.41 583087.497 5920717.102 583088.587 5920720.0013 
583089.2845 5920721.8567 583090.255 5920723.2 583089.8183 5920723.2766 
583087.862 5920723.62 583094.3452 5920746.3515 583095.258 5920749.552 
583095.5064 5920750.8609 583096.7068 5920757.1862 583103.0593 5920790.6603 
583107.5232 5920814.1819 583098.7156 5920815.6591 583096.5116 5920809.6825 
583092.36 5920798.4236 583098.553 5920786.273 583097.343 5920782.264 
583095.123 5920774.915 583088.605 5920776.883 583084.996 5920778.314 
583077.366 5920757.51 583064.606 5920762.187 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_C1FBA8D7-EA9D-415B-BEE0-BBBD6B8874CE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583077.6854 5920742.9619</gml:lowerCorner>
          <gml:upperCorner>583077.6854 5920742.9619</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_3A7B8F71-A427-4D0E-9B80-B3A6186EEA3C" srsName="EPSG:25832">
          <gml:pos>583077.6854 5920742.9619</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_4E5F13CC-971D-4F14-B68F-C6AAE79294B3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583079.6611 5920747.9012</gml:lowerCorner>
          <gml:upperCorner>583079.6611 5920747.9012</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_86957518-F22D-4B5E-AAAB-06097A04E037" srsName="EPSG:25832">
          <gml:pos>583079.6611 5920747.9012</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_45F0A9C4-FC6A-4B13-ADB7-404F1E3054A9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583081.5134 5920753.2109</gml:lowerCorner>
          <gml:upperCorner>583081.5134 5920753.2109</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_E1C65A11-FDA5-44C3-A83D-B329744972F9" srsName="EPSG:25832">
          <gml:pos>583081.5134 5920753.2109</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_C5CB2CD7-0B5F-4EB5-9FAD-B83FF79674A9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583083.6126 5920758.7676</gml:lowerCorner>
          <gml:upperCorner>583083.6126 5920758.7676</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_DB675B24-D602-4F8D-B46D-5EB2381D33D8" srsName="EPSG:25832">
          <gml:pos>583083.6126 5920758.7676</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_F2AF656F-59CE-46D5-93EF-5AD944019F67">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583090.0593 5920564.5272</gml:lowerCorner>
          <gml:upperCorner>583179.422 5920698.3698</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_801A70A6-E79A-48E6-ACD5-A10A562FE33A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583179.422 5920566.912 583141.516 5920609.969 583137.679 5920614.318 
583130.291 5920624.031 583125.997 5920630.34 583120.98 5920640.019 
583118.1532 5920646.4865 583114.1835 5920655.5691 583110.1316 5920664.8399 
583108.149 5920669.376 583108.0785 5920669.5148 583101.756 5920681.952 
583092.274 5920698.3698 583090.0593 5920694.0271 583096.5508 5920683.3853 
583101.3004 5920673.8862 583107.5113 5920660.4901 583112.6262 5920648.4335 
583121.6382 5920628.461 583129.067 5920616.2827 583134.0601 5920609.7064 
583140.5146 5920602.1558 583148.4306 5920593.2656 583156.2247 5920585.1061 
583164.6278 5920575.8506 583173.8833 5920565.8643 583176.2087 5920564.5272 
583179.422 5920566.912 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_D38B84DB-B16F-4BC4-ADCE-D54979E7EBE6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583174.333 5920570.2924</gml:lowerCorner>
          <gml:upperCorner>583174.333 5920570.2924</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_94C557A1-995E-4C70-9256-CBDF1B11BD59" srsName="EPSG:25832">
          <gml:pos>583174.333 5920570.2924</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_4D11765A-CDBE-40A5-B734-DD67CC95135B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583163.1369 5920583.6468</gml:lowerCorner>
          <gml:upperCorner>583163.1369 5920583.6468</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_FA245A86-24F4-40A8-B10B-A9E6C33AB3A7" srsName="EPSG:25832">
          <gml:pos>583163.1369 5920583.6468</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_D60CEC04-1037-46B7-AD79-D63C754085A2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583146.5451 5920601.9922</gml:lowerCorner>
          <gml:upperCorner>583146.5451 5920601.9922</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_01C0F53A-761A-4E01-96AC-A17D76EFB1F7" srsName="EPSG:25832">
          <gml:pos>583146.5451 5920601.9922</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_3229D559-5EC0-4F64-98F7-D28A59D92898">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583135.4839 5920615.0768</gml:lowerCorner>
          <gml:upperCorner>583135.4839 5920615.0768</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_FEBC992E-EDCD-4391-A216-DC4DA653AE77" srsName="EPSG:25832">
          <gml:pos>583135.4839 5920615.0768</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_09997A6F-1559-4C60-8A83-3AE0FDFAA6A9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583124.9623 5920629.2406</gml:lowerCorner>
          <gml:upperCorner>583124.9623 5920629.2406</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_0EFE9D0C-A983-488E-99AD-989F03374360" srsName="EPSG:25832">
          <gml:pos>583124.9623 5920629.2406</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_BCD88296-9B74-4F87-B8C9-23FB2C6D6A4A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583119.8364 5920638.8179</gml:lowerCorner>
          <gml:upperCorner>583119.8364 5920638.8179</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_0FEBA321-0AF2-4A53-B9C6-DC704B6E72CC" srsName="EPSG:25832">
          <gml:pos>583119.8364 5920638.8179</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_38B45EF1-F862-4ABD-941D-2518963E8FF6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583116.0594 5920646.7766</gml:lowerCorner>
          <gml:upperCorner>583116.0594 5920646.7766</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_E9FA857E-4107-45BF-B908-ADABD4C6DAFB" srsName="EPSG:25832">
          <gml:pos>583116.0594 5920646.7766</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_A33B904F-F9A1-4258-98EC-37FA7AFAABEA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583113.3615 5920653.9259</gml:lowerCorner>
          <gml:upperCorner>583113.3615 5920653.9259</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_DAB72BBD-C5C6-4218-BFE7-1120A38AD70D" srsName="EPSG:25832">
          <gml:pos>583113.3615 5920653.9259</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_34726489-D1C2-4B91-A17C-04CD805B580F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583108.2356 5920667.4152</gml:lowerCorner>
          <gml:upperCorner>583108.2356 5920667.4152</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_FD60A740-C8B7-438A-9969-8F1C51FF4096" srsName="EPSG:25832">
          <gml:pos>583108.2356 5920667.4152</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_5A918DCF-8FC1-4C40-BF68-2B61A25253E5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583104.9982 5920674.4296</gml:lowerCorner>
          <gml:upperCorner>583104.9982 5920674.4296</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_D71FBC74-834B-4DB6-8515-CC0B093588CD" srsName="EPSG:25832">
          <gml:pos>583104.9982 5920674.4296</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_D5EDCF78-53E0-4E39-96B7-F57C1FA18404">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583099.0629 5920684.8164</gml:lowerCorner>
          <gml:upperCorner>583099.0629 5920684.8164</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_CC165C38-958A-41C6-A77E-59A5526027DE" srsName="EPSG:25832">
          <gml:pos>583099.0629 5920684.8164</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_AC5E0518-D64A-4E65-B30B-7103063638E9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582947.3994 5920791.3231</gml:lowerCorner>
          <gml:upperCorner>583019.0386 5920859.5477</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DAAD0CB6-BFBC-408F-A929-6059846C833E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583019.0386 5920797.9164 583008.644 5920808.246 582998.8785 5920817.3989 
582989.834 5920825.876 582964.495 5920847.198 582949.8215 5920859.5477 
582947.3994 5920856.2711 582966.0413 5920840.8466 582980.8786 5920828.1882 
582990.3205 5920818.2275 583008.063 5920801.2113 583016.336 5920791.3231 
583019.0386 5920797.9164 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_290464CA-1182-4D33-A801-79920343C505">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582920.0149 5920851.8274</gml:lowerCorner>
          <gml:upperCorner>582949.8215 5920878.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_69ECEE15-E43D-441B-B6D4-15D2A687BD3A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582949.8215 5920859.5477 582945.236 5920863.407 582936.5551 5920869.9685 
582927.206 5920877.035 582925.022 5920878.525 582924.5731 5920878.831 
582923.3437 5920876.0598 582922.3595 5920873.8414 582920.0149 5920864.9098 
582926.902 5920859.72 582933.731 5920854.574 582936.151 5920857.792 
582941.135 5920854.059 582944.1144 5920851.8274 582947.3994 5920856.2711 
582949.8215 5920859.5477 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_2A69D419-CE9A-47C3-A11E-DF3BBC1481BF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582947.3994 5920856.2711</gml:lowerCorner>
          <gml:upperCorner>582949.8215 5920859.5477</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8A714A08-0238-4182-AE4E-583500F95529" srsName="EPSG:25832">
          <gml:posList>582949.8215 5920859.5477 582947.3994 5920856.2711 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_B2DA34CF-47BB-407F-9A8E-80D7541D23F4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582916.1776 5920837.8376</gml:lowerCorner>
          <gml:upperCorner>582944.1144 5920864.9098</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_606C1478-1BC7-4C45-8A20-949710E30E9F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582926.902 5920859.72 582920.0149 5920864.9098 582918.0017 5920857.2403 
582917.1716 5920853.7125 582916.1776 5920850.8069 582933.7728 5920837.8376 
582944.1144 5920851.8274 582941.135 5920854.059 582936.151 5920857.792 
582933.731 5920854.574 582926.902 5920859.72 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.45</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_A4F7E8AE-D986-4FD4-B496-74BDF3832EA2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583014.3922 5920799.6549</gml:lowerCorner>
          <gml:upperCorner>583014.3922 5920799.6549</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_0667EB81-E3C5-4BAC-968D-21FE49FC330F" srsName="EPSG:25832">
          <gml:pos>583014.3922 5920799.6549</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_AECF2D18-7B38-41C1-A90E-042A51921CE7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583004.4315 5920809.6156</gml:lowerCorner>
          <gml:upperCorner>583004.4315 5920809.6156</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_4783C96A-EBE5-4E18-8572-ABFC6D3616AD" srsName="EPSG:25832">
          <gml:pos>583004.4315 5920809.6156</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_2063EBA8-5375-4C27-A306-3005D6344910">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582993.952 5920819.4726</gml:lowerCorner>
          <gml:upperCorner>582993.952 5920819.4726</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_CEC2A45E-7B19-465C-BEC3-F0CACF5E428E" srsName="EPSG:25832">
          <gml:pos>582993.952 5920819.4726</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_9F2C5ABE-06CD-4876-B70B-5DABD3EC0B22">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582960.8534 5920848.4209</gml:lowerCorner>
          <gml:upperCorner>582960.8534 5920848.4209</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_B5B873F3-25E0-4D49-8C90-33C91A5513B3" srsName="EPSG:25832">
          <gml:pos>582960.8534 5920848.4209</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_3572422C-1B7C-4465-8C22-C7B2613C9BCF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582950.374 5920857.9666</gml:lowerCorner>
          <gml:upperCorner>582950.374 5920857.9666</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_94C91C5E-F27F-4629-AE72-D53EB9B8D661" srsName="EPSG:25832">
          <gml:pos>582950.374 5920857.9666</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_9F6F70EE-F698-49CF-B06C-E432317CBD73">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582924.3309 5920876.1241</gml:lowerCorner>
          <gml:upperCorner>582924.3309 5920876.1241</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_0C2E494C-6557-4212-9E53-3ADADC7B535C" srsName="EPSG:25832">
          <gml:pos>582924.3309 5920876.1241</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_723681A8-5C2F-4AC9-B227-8B15F19E7C2D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582921.323 5920843.3969</gml:lowerCorner>
          <gml:upperCorner>582941.135 5920859.72</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7FC7ACC8-482C-4C00-BA1F-FC919C9414E2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582926.902 5920859.72 582921.323 5920852.321 582926.369 5920848.516 
582933.1577 5920843.3969 582933.606 5920843.996 582941.135 5920854.059 
582936.151 5920857.792 582933.731 5920854.574 582926.902 5920859.72 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_A6B2E664-983D-4295-B84A-5F480B308CC1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582921.323 5920843.3969</gml:lowerCorner>
          <gml:upperCorner>582941.135 5920859.72</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_93D4D63F-9C39-498B-B917-4259F0FB3D8A" srsName="EPSG:25832">
          <gml:posList>582933.731 5920854.574 582936.151 5920857.792 582941.135 5920854.059 
582933.606 5920843.996 582933.1577 5920843.3969 582926.369 5920848.516 
582921.323 5920852.321 582926.902 5920859.72 582933.731 5920854.574 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_36CB206F-7241-43E9-8BA1-3F87B6C8725A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582888.3606 5920876.0598</gml:lowerCorner>
          <gml:upperCorner>582924.5731 5920902.8581</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AD7FDDFE-0EF2-481B-A49C-D42442878EC7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582924.5731 5920878.831 582920.273 5920881.762 582901.7454 5920894.7217 
582890.1134 5920902.8581 582888.3606 5920900.4558 582923.3437 5920876.0598 
582924.5731 5920878.831 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_EA5287F8-B212-4DE1-92CE-B07C6D010CEE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582859.1341 5920895.3523</gml:lowerCorner>
          <gml:upperCorner>582890.1134 5920920.223</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A882B093-465F-49EA-B96B-0D956A886F2E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582888.3606 5920900.4558 582890.1134 5920902.8581 582883.7132 5920907.3349 
582865.288 5920920.223 582859.1341 5920909.6948 582884.637 5920895.3523 
582888.3606 5920900.4558 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_27E5766B-AB15-441B-A3EE-E4214537C12A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582923.3437 5920876.0598</gml:lowerCorner>
          <gml:upperCorner>582924.5731 5920878.831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_ECAEBE28-D373-4B27-B2CD-17FAFF0B9CC6" srsName="EPSG:25832">
          <gml:posList>582924.5731 5920878.831 582923.3437 5920876.0598 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_FC9DC0BB-8EA8-4BB8-9272-758FBAF6D531">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582888.3606 5920900.4558</gml:lowerCorner>
          <gml:upperCorner>582890.1134 5920902.8581</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_203B5294-C201-40C3-A50D-24471DAB0544" srsName="EPSG:25832">
          <gml:posList>582890.1134 5920902.8581 582888.3606 5920900.4558 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_BD3B8DE3-87F7-4E1E-BD14-5350ECB972BF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582916.2664 5920882.6386</gml:lowerCorner>
          <gml:upperCorner>582916.2664 5920882.6386</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_172F2FA1-E78A-460A-A62A-2402F2E7571C" srsName="EPSG:25832">
          <gml:pos>582916.2664 5920882.6386</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_E273153E-4724-4D63-A034-194B214E0AC1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582900.7166 5920893.3042</gml:lowerCorner>
          <gml:upperCorner>582900.7166 5920893.3042</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_7552D708-56C1-4349-9204-87686993A489" srsName="EPSG:25832">
          <gml:pos>582900.7166 5920893.3042</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_CC3FB500-0008-4EAB-90CC-CD469CA2466D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582887.4594 5920902.1756</gml:lowerCorner>
          <gml:upperCorner>582887.4594 5920902.1756</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_9AF9C5ED-1BAE-4156-BA0B-B639DD95B5C9" srsName="EPSG:25832">
          <gml:pos>582887.4594 5920902.1756</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_EA66B65E-C7BB-49E0-A828-EFCDB9F135AC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582868.9192 5920914.0373</gml:lowerCorner>
          <gml:upperCorner>582868.9192 5920914.0373</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_F70B2D41-DE57-468C-A179-A1CF101A04EA" srsName="EPSG:25832">
          <gml:pos>582868.9192 5920914.0373</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_1355442B-6878-47A1-9780-4EB28C36DB8A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582850.443 5920879.4627</gml:lowerCorner>
          <gml:upperCorner>582884.637 5920909.6948</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3239B2EA-D433-42E7-9B33-F272A3E268D8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582884.637 5920895.3523 582859.1341 5920909.6948 582850.443 5920894.826 
582873.0437 5920879.4627 582884.637 5920895.3523 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8680EC34-AD6C-4FC3-8CF1-F6C638E4409D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582856.8413 5920893.055</gml:lowerCorner>
          <gml:upperCorner>582872.4523 5920908.087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B4CCD85D-5F68-4D2D-928D-04C102550BF7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582871.309 5920900.176 582872.4523 5920902.2048 582861.993 5920908.087 
582856.8413 5920898.9456 582859.481 5920897.458 582867.296 5920893.055 
582871.309 5920900.176 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_E04E3CBB-C1E2-4491-B474-4EA5801CB0D4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582856.8413 5920893.055</gml:lowerCorner>
          <gml:upperCorner>582872.4523 5920908.087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_72F2F3FF-8BB2-4BF3-BED1-0A6DDEF7C38E" srsName="EPSG:25832">
          <gml:posList>582867.296 5920893.055 582859.481 5920897.458 582856.8413 5920898.9456 
582861.993 5920908.087 582872.4523 5920902.2048 582871.309 5920900.176 
582867.296 5920893.055 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_5CEF6008-0959-4781-954A-6A3319343827">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582737.101 5920955.304</gml:lowerCorner>
          <gml:upperCorner>582787.3096 5921001.7745</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7AD52E20-AE29-4525-8283-1B69D07E3C4D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582787.3096 5920972.8096 582783.051 5920976.391 582775.373 5920982.848 
582768.7449 5920987.533 582767.931 5920988.254 582759.71 5920995.537 
582752.6692 5921001.7745 582752.285 5921001.283 582753.255 5921000.234 
582752.4684 5920999.4884 582737.101 5920984.922 582738.3721 5920983.9846 
582757.758 5920969.688 582758.594 5920969.072 582779.093 5920955.304 
582787.3096 5920972.8096 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_3C288F10-4131-44E4-AD38-767F43E853CC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582787.3096 5920917.651</gml:lowerCorner>
          <gml:upperCorner>582865.288 5920975.693</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7D82432B-09D5-4194-9AE3-F28E996FA9E9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582865.288 5920920.223 582858.8662 5920924.7166 582824.759 5920948.583 
582823.956 5920949.145 582814.3317 5920956.1329 582807.291 5920961.245 
582807.2267 5920961.2934 582806.492 5920961.846 582791.717 5920972.968 
582788.663 5920975.693 582787.3096 5920972.8096 582805.1775 5920958.8612 
582822.7179 5920946.388 582839.8685 5920934.3047 582851.9519 5920925.8593 
582863.7846 5920917.651 582865.288 5920920.223 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_2D2B0D63-B699-414C-ACF9-21EBA99BC31B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582752.6692 5920972.8096</gml:lowerCorner>
          <gml:upperCorner>582788.663 5921004.145</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_19B97B79-0B54-4BDE-8BF7-54C0A8B3D598" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582788.663 5920975.693 582777.27 5920985.173 582775.2966 5920986.8188 
582773.475 5920988.338 582754.522 5921004.145 582752.6692 5921001.7745 
582759.71 5920995.537 582767.931 5920988.254 582768.7449 5920987.533 
582775.373 5920982.848 582783.051 5920976.391 582787.3096 5920972.8096 
582788.663 5920975.693 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_B622746D-0C64-4BC4-85B1-6F355447E6C6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582863.7846 5920917.651</gml:lowerCorner>
          <gml:upperCorner>582865.288 5920920.223</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A9B6B543-3B9D-4196-B5E7-DB310CF7E065" srsName="EPSG:25832">
          <gml:posList>582863.7846 5920917.651 582865.288 5920920.223 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_E331EFA0-6F91-4E54-B7B0-6390555A4ADC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582787.3096 5920972.8096</gml:lowerCorner>
          <gml:upperCorner>582788.663 5920975.693</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6DD5BA50-5D16-4234-B04B-3994477F917F" srsName="EPSG:25832">
          <gml:posList>582787.3096 5920972.8096 582788.663 5920975.693 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_142A537A-38D4-45DF-8A0A-8CFE33444711">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582857.4089 5920923.2607</gml:lowerCorner>
          <gml:upperCorner>582857.4089 5920923.2607</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_D6F82375-157B-4F7D-8639-A673E4BE29CB" srsName="EPSG:25832">
          <gml:pos>582857.4089 5920923.2607</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_96477643-84F7-4864-B605-E59753BEDA1A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582846.3649 5920931.0564</gml:lowerCorner>
          <gml:upperCorner>582846.3649 5920931.0564</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_91D1B439-1B82-41AC-A016-CEC1791AE3E4" srsName="EPSG:25832">
          <gml:pos>582846.3649 5920931.0564</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_F31C7BE3-83D0-49F4-BAAA-5881A0500743">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582835.0611 5920939.7617</gml:lowerCorner>
          <gml:upperCorner>582835.0611 5920939.7617</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_AE30D4FF-8658-4BC4-A18C-D33F300D931E" srsName="EPSG:25832">
          <gml:pos>582835.0611 5920939.7617</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_75B1933E-1FA4-4737-9A4A-1647D77CF25A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582824.0172 5920947.6873</gml:lowerCorner>
          <gml:upperCorner>582824.0172 5920947.6873</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_68DED79D-87BC-4A92-AC9C-8CB941CE6EC9" srsName="EPSG:25832">
          <gml:pos>582824.0172 5920947.6873</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_A20495D1-89CA-4B5F-9D4A-171C6FCDBD49">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582813.4929 5920955.4831</gml:lowerCorner>
          <gml:upperCorner>582813.4929 5920955.4831</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_B60FEDD2-5619-4B26-B1C7-5798E0933778" srsName="EPSG:25832">
          <gml:pos>582813.4929 5920955.4831</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_C7CB8952-BFD7-43F3-AAAE-E62EFDA317C6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582804.1381 5920962.889</gml:lowerCorner>
          <gml:upperCorner>582804.1381 5920962.889</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_BE990C7D-5600-4DA9-9D5C-6A67EF33D709" srsName="EPSG:25832">
          <gml:pos>582804.1381 5920962.889</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_7405A9C0-B4F6-4054-AAA0-DBD51C3E6E5F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582794.5233 5920970.0351</gml:lowerCorner>
          <gml:upperCorner>582794.5233 5920970.0351</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_56D6D59C-C629-4FD6-BD83-266CA77042F5" srsName="EPSG:25832">
          <gml:pos>582794.5233 5920970.0351</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_47E34855-150B-49A8-A677-5C9B152EB829">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582756.0644 5920999.0092</gml:lowerCorner>
          <gml:upperCorner>582756.0644 5920999.0092</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_54F3DFDC-1059-4A63-B018-1928200A0117" srsName="EPSG:25832">
          <gml:pos>582756.0644 5920999.0092</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_54A97277-8BD6-4E22-90DF-FEE741836B5E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582742.2919 5920983.2878</gml:lowerCorner>
          <gml:upperCorner>582742.2919 5920983.2878</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_E6BBF3A3-0CDB-4436-AA13-F422666FDD42" srsName="EPSG:25832">
          <gml:pos>582742.2919 5920983.2878</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_7F17E171-2C90-4B0D-AB8C-71B8E6FE5E9C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582741.3824 5920985.8864</gml:lowerCorner>
          <gml:upperCorner>582741.3824 5920985.8864</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_11B63B9E-0C58-44AE-AD1B-A14AE2EDA9EA" srsName="EPSG:25832">
          <gml:pos>582741.3824 5920985.8864</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A7EA2BFC-90D8-487D-8C30-B75AEDC1503A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582768.6092 5920967.04</gml:lowerCorner>
          <gml:upperCorner>582784.6146 5920982.848</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B8AB73D2-F9DF-4620-B410-15E2F174565E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582784.6146 5920975.076 582783.051 5920976.391 582775.373 5920982.848 
582769.572 5920975.96 582768.6092 5920974.8168 582768.6149 5920974.812 
582777.8564 5920967.04 582784.6146 5920975.076 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_78F821F9-27BD-49CF-AE2E-0AFB91F661CB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582752.7498 5920979.6734</gml:lowerCorner>
          <gml:upperCorner>582768.745 5920995.537</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6A8644C9-E754-4579-A665-329BE0A524A1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582768.745 5920987.5329 582767.931 5920988.254 582759.71 5920995.537 
582754.892 5920990.095 582752.7498 5920987.6753 582761.7824 5920979.6734 
582768.745 5920987.5329 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_3A12C489-7F68-41EB-88DA-E93953D1481E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582768.6092 5920967.04</gml:lowerCorner>
          <gml:upperCorner>582784.6146 5920982.848</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_07EE34C9-8EFF-4564-845E-8011B6388F4A" srsName="EPSG:25832">
          <gml:posList>582777.8564 5920967.04 582768.6149 5920974.812 582768.6092 5920974.8168 
582769.572 5920975.96 582775.373 5920982.848 582783.051 5920976.391 
582784.6146 5920975.076 582777.8564 5920967.04 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_3D817FEA-A099-4FAB-B5E1-754CFE64CF94">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582752.7498 5920979.6734</gml:lowerCorner>
          <gml:upperCorner>582768.745 5920995.537</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_50A0D276-D68C-486F-A1E1-5B72CDC1AA96" srsName="EPSG:25832">
          <gml:posList>582761.7824 5920979.6734 582752.7498 5920987.6753 582754.892 5920990.095 
582759.71 5920995.537 582767.931 5920988.254 582768.745 5920987.5329 
582761.7824 5920979.6734 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_909217E9-43FE-4080-9E2E-65F8D99BBD55">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582691.2674 5921021.3293</gml:lowerCorner>
          <gml:upperCorner>582716.7372 5921055.6726</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C9E7FD6A-4C06-47EB-BAF3-96C679513A7E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582716.648 5921034.55 582716.7372 5921034.6494 582712.463 5921038.847 
582712.4459 5921038.8651 582711.559 5921039.803 582705.145 5921046.586 
582696.5527 5921055.6726 582691.2674 5921032.9564 582708.1855 5921021.3293 
582716.648 5921034.55 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_F53E7952-6F69-41DF-A5FF-E6866D070D33">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582720.4467 5921001.7745</gml:lowerCorner>
          <gml:upperCorner>582754.522 5921040.422</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B66ED97F-8D1E-476E-954F-C50222BC25FE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582754.522 5921004.145 582748.617 5921009.203 582742.149 5921016.14 
582736.782 5921022.88 582729.133 5921032.496 582729.0978 5921032.5406 
582722.875 5921040.422 582720.4467 5921038.5564 582725.5103 5921032.3493 
582735.6983 5921018.6592 582746.4169 5921006.8793 582752.6692 5921001.7745 
582754.522 5921004.145 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_9FDF477E-EA26-4119-9EC4-6EBF7CCB17AE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582696.2496 5921034.6494</gml:lowerCorner>
          <gml:upperCorner>582722.875 5921071.821</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0922C3E1-7974-44A1-8AA6-A19AE6709286" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582719.785 5921038.048 582720.4467 5921038.5564 582722.875 5921040.422 
582717.306 5921047.473 582705.178 5921063.742 582699.564 5921071.821 
582698.484 5921068.387 582696.2496 5921059.6653 582697.4235 5921059.4156 
582696.5527 5921055.6726 582705.145 5921046.586 582711.559 5921039.803 
582712.4459 5921038.8651 582712.463 5921038.847 582716.7372 5921034.6494 
582719.785 5921038.048 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A04F7BC8-0A03-43FE-801B-D53A4AB7FF47">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582693.4167 5921027.696</gml:lowerCorner>
          <gml:upperCorner>582712.4459 5921046.586</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_967B6B66-7A7B-44FA-8CB8-C744233B2A5D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582705.814 5921032.576 582712.4459 5921038.8651 582711.559 5921039.803 
582705.145 5921046.586 582699.323 5921041.083 582693.4167 5921035.5003 
582694.185 5921034.673 582697.87 5921030.705 582700.668 5921027.696 
582705.814 5921032.576 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_8FF6F7E0-ECD9-45B1-ABE8-7B1C6DC3350E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582693.4167 5921027.696</gml:lowerCorner>
          <gml:upperCorner>582712.4459 5921046.586</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_83E122A4-25B4-47C6-A5B8-9A3295F5B861" srsName="EPSG:25832">
          <gml:posList>582700.668 5921027.696 582697.87 5921030.705 582694.185 5921034.673 
582693.4167 5921035.5003 582699.323 5921041.083 582705.145 5921046.586 
582711.559 5921039.803 582712.4459 5921038.8651 582705.814 5921032.576 
582700.668 5921027.696 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_960B7221-25CC-4AD4-8678-05C5A4AEF074">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582752.6692 5921001.7745</gml:lowerCorner>
          <gml:upperCorner>582754.522 5921004.145</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_CECEBD7D-6BFE-4F91-BFBD-A88B4536A30E" srsName="EPSG:25832">
          <gml:posList>582754.522 5921004.145 582752.6692 5921001.7745 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_2956B184-2C34-4E68-83F9-5CFC82BC1321">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582720.4467 5921038.5564</gml:lowerCorner>
          <gml:upperCorner>582722.875 5921040.422</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8C170303-BCD8-4B1F-B468-93BE133DAF71" srsName="EPSG:25832">
          <gml:posList>582722.875 5921040.422 582720.4467 5921038.5564 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_105E0D56-980B-4676-ACB3-A57712D24E77">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582744.7189 5921009.7447</gml:lowerCorner>
          <gml:upperCorner>582744.7189 5921009.7447</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_6C20C0EE-5155-440D-B933-63312F8241D2" srsName="EPSG:25832">
          <gml:pos>582744.7189 5921009.7447</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_4CC3A535-5DB5-4EF0-B6A4-037CEA469D66">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582734.7432 5921022.0552</gml:lowerCorner>
          <gml:upperCorner>582734.7432 5921022.0552</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_5EBB6B64-DC12-4056-959A-32811C324BE8" srsName="EPSG:25832">
          <gml:pos>582734.7432 5921022.0552</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_F0CC3E2C-C418-4138-8315-2A683C4BFA88">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582724.1307 5921035.7453</gml:lowerCorner>
          <gml:upperCorner>582724.1307 5921035.7453</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_41A13640-941B-4435-ACA5-2EC5E9658E7A" srsName="EPSG:25832">
          <gml:pos>582724.1307 5921035.7453</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_2E38AD03-F091-4D89-8999-6EDA55408994">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582510.836 5920848.586</gml:lowerCorner>
          <gml:upperCorner>582596.2446 5920963.8651</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_6E6F464B-4D46-435F-A6A4-755160ED6C7C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B712BF16-F112-48C6-8359-DE6DAD965073" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582596.2446 5920849.9977 582567.995 5920873.7938 582553.3735 5920887.4155 
582547.9998 5920893.0392 582535.3779 5920909.6601 582529.1294 5920920.0326 
582521.7562 5920937.6533 582518.507 5920946.9011 582512.7786 5920963.8651 
582510.836 5920961.679 582511.764 5920959.166 582521.926 5920931.643 
582524.034 5920926.732 582524.118 5920926.5497 582527.396 5920919.435 
582529.267 5920915.862 582531.305 5920912.45 582535.71 5920905.683 
582538.651 5920901.649 582545.6503 5920893.4694 582552.129 5920885.898 
582558.777 5920879.757 582565.091 5920874.277 582574.577 5920866.1187 
582594.963 5920848.586 582596.2446 5920849.9977 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:besondereZweckbestimmung>24001</xplan:besondereZweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_8C4A6CF4-FB17-44BF-9E9A-3B8810F218B2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582594.963 5920848.586</gml:lowerCorner>
          <gml:upperCorner>582596.2446 5920849.9977</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_794E1559-E66E-4875-8B48-B4FABFD69452" srsName="EPSG:25832">
          <gml:posList>582596.2446 5920849.9977 582594.963 5920848.586 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_FFBB5688-4171-49C0-9C60-50BF384CDE34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582595.9882 5920848.7999</gml:lowerCorner>
          <gml:upperCorner>582595.9882 5920848.7999</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_EB2E3688-FB89-4C9B-9D9E-B2B34F823489" srsName="EPSG:25832">
          <gml:pos>582595.9882 5920848.7999</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_AAD7A215-B07D-441B-AE07-05B3180D567C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582582.6164 5920860.5471</gml:lowerCorner>
          <gml:upperCorner>582582.6164 5920860.5471</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_BC8302BF-2DF4-4F6E-B0DB-DFE42C6D76C9" srsName="EPSG:25832">
          <gml:pos>582582.6164 5920860.5471</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_0DE0D3C8-DD15-470B-8517-B3803072F2B2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582568.9947 5920872.1692</gml:lowerCorner>
          <gml:upperCorner>582568.9947 5920872.1692</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_1C0BF463-013D-477B-A4EB-B26FAA191B77" srsName="EPSG:25832">
          <gml:pos>582568.9947 5920872.1692</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_E560A0C0-77BB-4237-B354-7D3443D96656">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582554.8732 5920884.7912</gml:lowerCorner>
          <gml:upperCorner>582554.8732 5920884.7912</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_7E9C8E75-5889-4985-A0EC-B8479684FBD7" srsName="EPSG:25832">
          <gml:pos>582554.8732 5920884.7912</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>2000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_ED95E471-FA07-4832-A5AB-FFCD1FAE2D1F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582778.0586 5920794.885</gml:lowerCorner>
          <gml:upperCorner>583035.8594 5921018.6974</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A4BBC953-71B9-41B2-AE58-340799ADBC09" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582828.2504 5920971.2085 582807.073 5920977.136 582794.94 5920980.532 
582804.023 5921007.966 582782.9065 5921018.6974 582778.0586 5920990.5617 
582795.488 5920976.642 582800.9147 5920972.365 582805.547 5920968.714 
582816.984 5920960.082 582824.469 5920954.433 582826.651 5920952.982 
582829.4934 5920951.0277 582837.2471 5920945.6967 582842.839 5920941.852 
582856.245 5920932.631 582868.1313 5920924.6091 582877.585 5920918.229 
582897.946 5920904.597 582901.293 5920902.355 582908.749 5920896.712 
582917.802 5920890.093 582924.443 5920885.519 582925.168 5920884.986 
582930.8543 5920880.8049 582941.099 5920873.272 582941.857 5920872.615 
582967.856 5920851.452 582970.5751 5920849.4954 582975.063 5920846.266 
582986.117 5920838.31 582992.666 5920830.95 582994.83 5920828.393 
583002.5711 5920820.9836 583018.3976 5920805.8352 583028.603 5920796.067 
583029.531 5920794.885 583030.2576 5920797.7513 583030.965 5920800.542 
583031.2945 5920802.4332 583031.319 5920802.5738 583030.4538 5920802.7246 
583035.8594 5920833.7494 583010.639 5920842.688 582996.128 5920847.831 
582999.5253 5920858.0088 582972.9749 5920864.0267 582971.496 5920856.251 
582970.953 5920856.358 582968.7864 5920856.7349 582970.1821 5920864.6597 
582952.4937 5920868.669 582943.166 5920874.728 582933.4 5920881.404 
582937.376 5920887.222 582928.2155 5920893.4823 582923.7832 5920896.1178 
582920.2418 5920890.6705 582918.686 5920891.682 582911.421 5920896.405 
582910.181 5920897.2111 582914.2893 5920903.5308 582916.1764 5920906.4336 
582916.3417 5920909.6173 582914.8596 5920910.2357 582915.7284 5920912.3181 
582910.942 5920914.315 582913.837 5920921.249 582879.666 5920939.537 
582876.456 5920941.094 582871.287 5920930.441 582858.791 5920936.504 
582865.0132 5920949.3377 582840.617 5920954.499 582832.052 5920956.311 
582834.053 5920965.751 582834.661 5920968.601 582834.8256 5920969.3681 
582832.2232 5920970.0965 582831.6023 5920966.9559 582829.378 5920955.705 
582827.993 5920956.562 582827.14 5920957.132 582825.339 5920958.428 
582828.2504 5920971.2085 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_5C8066FA-7EB9-4591-BB03-51FBA32EB085">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582972.9749 5920833.7494</gml:lowerCorner>
          <gml:upperCorner>583049.4641 5920929.3514</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2EC82D3E-6E78-4EC7-A403-B3C4BBD77E0E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583011.999 5920920.5447 583012.3903 5920925.2597 582984.0361 5920929.3514 
582983.8562 5920926.7959 582983.651 5920923.881 582978.8239 5920896.6528 
582976.6433 5920884.3527 582973.932 5920869.059 582972.9749 5920864.0267 
582999.5253 5920858.0088 582996.128 5920847.831 583010.639 5920842.688 
583035.8594 5920833.7494 583043.9661 5920880.2775 583049.4641 5920919.9097 
583014.8932 5920924.8985 583013.7452 5920920.3066 583011.999 5920920.5447 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_DD821931-EF9B-4829-AF57-2D9A32946F60">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582782.9065 5920971.2085</gml:lowerCorner>
          <gml:upperCorner>582842.886 5921079.9216</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E993D37F-C04A-497E-8A11-1AB8CEB3F71A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582808.2945 5921070.6714 582809.5016 5921077.1984 582793.4557 5921079.9216 
582782.9065 5921018.6974 582804.023 5921007.966 582794.94 5920980.532 
582807.073 5920977.136 582828.2504 5920971.2085 582831.777 5920986.69 
582833.088 5920992.444 582838.143 5921014.632 582842.886 5921042.7284 
582825.8908 5921046.5553 582827.938 5921058.269 582830.3598 5921073.6585 
582812.5702 5921076.6776 582811.3778 5921070.1897 582808.2945 5921070.6714 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_FD4CC5D8-F5AD-475C-8C98-0A3E990305CC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582996.128 5920842.688</gml:lowerCorner>
          <gml:upperCorner>583021.352 5920880.734</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 4 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_8253457D-9FFE-4420-9A75-BDE1586BCFBD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583021.352 5920876.181 583007.111 5920880.734 582996.128 5920847.831 
583010.639 5920842.688 583021.352 5920876.181 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_97388113-5F6F-4B6C-A1E8-F7B2FE8583C8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582996.128 5920842.688</gml:lowerCorner>
          <gml:upperCorner>583021.352 5920880.734</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8E55C342-04A4-4AB7-B55C-3AE0A2908776" srsName="EPSG:25832">
          <gml:posList>583010.639 5920842.688 582996.128 5920847.831 583007.111 5920880.734 
583021.352 5920876.181 583010.639 5920842.688 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_1D028A3B-2C51-4E36-B3FC-1473B758DF9E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583022.871 5920888.114</gml:lowerCorner>
          <gml:upperCorner>583042.241 5920916.966</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_20405F2C-4B00-4259-870F-BFFDF1461110" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583041.967 5920914.171 583032.436 5920916.966 583029.833 5920908.096 
583028.45 5920908.538 583022.871 5920893.002 583036.491 5920888.114 
583042.241 5920904.128 583038.735 5920905.249 583041.967 5920914.171 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_D09A066C-C2CB-4673-A8DF-7A9D6CFE71A8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583022.871 5920888.114</gml:lowerCorner>
          <gml:upperCorner>583042.241 5920916.966</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A98F6054-74EC-4519-84E4-D0E8104E1F67" srsName="EPSG:25832">
          <gml:posList>583041.967 5920914.171 583038.735 5920905.249 583042.241 5920904.128 
583036.491 5920888.114 583022.871 5920893.002 583028.45 5920908.538 
583029.833 5920908.096 583032.436 5920916.966 583041.967 5920914.171 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_09FE2EE5-845A-4AFD-BD50-4F7F8FA2FE93">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582984.8258 5920900.6798</gml:lowerCorner>
          <gml:upperCorner>582997.6332 5920919.5893</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4759B07E-9A99-478C-8AA3-853CB2FF9795" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582997.6332 5920917.9465 582987.674 5920919.5893 582984.8258 5920902.3226 
582994.785 5920900.6798 582997.6332 5920917.9465 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_FEAA4C5E-0F70-4D53-A65E-3FEC0C50B73E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582980.5078 5920875.3258</gml:lowerCorner>
          <gml:upperCorner>582993.1593 5920889.0053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_11D2A4C7-4CD4-4C99-8A1C-13FBD4E09306" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582993.1593 5920887.1439 582982.5888 5920889.0053 582980.5078 5920877.1871 
582991.0783 5920875.3258 582993.1593 5920887.1439 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_3FF79758-AB47-4524-AC8A-5527919BCF42">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582956.0777 5920879.6063</gml:lowerCorner>
          <gml:upperCorner>582969.9726 5920892.2608</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F4B55A16-22A1-4B14-84E6-E5CD89B85C17" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582969.9726 5920889.7708 582958.2338 5920892.2608 582956.0777 5920882.0963 
582967.8165 5920879.6063 582969.9726 5920889.7708 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8379A2BB-CF12-4EF2-8FC1-1A8EE89065BB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582933.4 5920874.728</gml:lowerCorner>
          <gml:upperCorner>582949.2715 5920890.3357</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CE171936-551D-4E68-BCCE-4B4E7D096030" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582947.142 5920880.544 582949.2715 5920883.6589 582945.974 5920885.913 
582942.482 5920888.3 582939.5039 5920890.3357 582937.376 5920887.222 
582933.4 5920881.404 582943.166 5920874.728 582947.142 5920880.544 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_410224BD-07D1-4B0A-B804-933322D7F8BB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582984.8258 5920900.6798</gml:lowerCorner>
          <gml:upperCorner>582997.6332 5920919.5893</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_23EE11E4-A01B-406B-BB00-658060B8AC75" srsName="EPSG:25832">
          <gml:posList>582994.785 5920900.6798 582984.8258 5920902.3226 582987.674 5920919.5893 
582997.6332 5920917.9465 582994.785 5920900.6798 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_ABA60C6F-9B39-4DC4-9FA7-7F120289D4AE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582980.5078 5920875.3258</gml:lowerCorner>
          <gml:upperCorner>582993.1593 5920889.0053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_ED02BB23-445F-495E-9E7D-F1DEE9514B79" srsName="EPSG:25832">
          <gml:posList>582993.1593 5920887.1439 582991.0783 5920875.3258 582980.5078 5920877.1871 
582982.5888 5920889.0053 582993.1593 5920887.1439 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_C6A0A69F-AE37-4EE1-9199-E3DF341CF800">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582956.0777 5920879.6063</gml:lowerCorner>
          <gml:upperCorner>582969.9726 5920892.2608</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_5A9BB94F-9DC7-4236-9F39-C19CEC51036F" srsName="EPSG:25832">
          <gml:posList>582967.8165 5920879.6063 582956.0777 5920882.0963 582958.2338 5920892.2608 
582969.9726 5920889.7708 582967.8165 5920879.6063 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_1075F0EE-DE6F-40DE-8A42-725B18748271">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582933.4 5920874.728</gml:lowerCorner>
          <gml:upperCorner>582949.2715 5920890.3357</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_C4CA5C58-8282-4A78-BDF9-777A5A1534F0" srsName="EPSG:25832">
          <gml:posList>582933.4 5920881.404 582937.376 5920887.222 582939.5039 5920890.3357 
582942.482 5920888.3 582945.974 5920885.913 582949.2715 5920883.6589 
582947.142 5920880.544 582943.166 5920874.728 582933.4 5920881.404 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_54E8804F-4146-46FB-99C3-7263A967F693">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582910.181 5920890.6705</gml:lowerCorner>
          <gml:upperCorner>582926.2376 5920906.4336</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7EA077EA-AC4B-4DE8-A7E7-9AF11CEAFCF6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582923.7832 5920896.1178 582926.2376 5920899.8928 582916.1764 5920906.4336 
582910.181 5920897.2111 582911.421 5920896.405 582918.686 5920891.682 
582920.2418 5920890.6705 582923.7832 5920896.1178 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_3BF0D75A-3F8C-48EC-B198-DDDF0FB29C7C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582910.181 5920890.6705</gml:lowerCorner>
          <gml:upperCorner>582926.2376 5920906.4336</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_83890DD4-F373-4433-B44D-216320738F87" srsName="EPSG:25832">
          <gml:posList>582910.181 5920897.2111 582916.1764 5920906.4336 582926.2376 5920899.8928 
582923.7832 5920896.1178 582920.2418 5920890.6705 582918.686 5920891.682 
582911.421 5920896.405 582910.181 5920897.2111 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_707AA158-D473-4C1B-85FF-10FEDC0DB004">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582910.942 5920909.4883</gml:lowerCorner>
          <gml:upperCorner>582926.3616 5920923.5435</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_10AE3F71-FDAE-412D-80FC-4AADEBC8303C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582926.3616 5920918.7173 582914.7936 5920923.5435 582913.837 5920921.249 
582910.942 5920914.315 582915.7284 5920912.3181 582922.5113 5920909.4883 
582926.3616 5920918.7173 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_E8224DA1-B10B-4663-838F-9731C905BFB9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582910.942 5920909.4883</gml:lowerCorner>
          <gml:upperCorner>582926.3616 5920923.5435</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_C564AC40-662C-4A4D-B67B-538814005332" srsName="EPSG:25832">
          <gml:posList>582922.5113 5920909.4883 582915.7284 5920912.3181 582910.942 5920914.315 
582913.837 5920921.249 582914.7936 5920923.5435 582926.3616 5920918.7173 
582922.5113 5920909.4883 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_2D4C57D0-2D84-40E5-9CD3-105D8FF13954">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582911.383 5920961.918</gml:lowerCorner>
          <gml:upperCorner>582926.868 5920984.003</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 3 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DE7E6614-D30B-429D-A752-42C3D5AB5832" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582926.868 5920981.844 582916.487 5920984.003 582914.058 5920972.326 
582913.044 5920972.537 582911.383 5920964.569 582924.194 5920961.918 
582925.832 5920969.872 582924.438 5920970.165 582926.868 5920981.844 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_C36EC8BA-EDE9-4C4D-9015-736FC102B036">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582911.383 5920961.918</gml:lowerCorner>
          <gml:upperCorner>582926.868 5920984.003</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_E7AC5D18-80B2-4F9C-85B1-9B73191B442E" srsName="EPSG:25832">
          <gml:posList>582924.194 5920961.918 582911.383 5920964.569 582913.044 5920972.537 
582914.058 5920972.326 582916.487 5920984.003 582926.868 5920981.844 
582924.438 5920970.165 582925.832 5920969.872 582924.194 5920961.918 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_AA045F42-5C35-438E-AE50-05DAAF57BAB3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582858.791 5920930.441</gml:lowerCorner>
          <gml:upperCorner>582885.496 5920959.186</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 3 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2C3B0A2C-9E27-4FA9-82F1-FF3D2EA953E4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582885.496 5920951.574 582869.788 5920959.186 582858.791 5920936.504 
582871.287 5920930.441 582876.456 5920941.094 582879.666 5920939.537 
582885.496 5920951.574 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_8989E65B-8F2A-445D-A361-D16A9D8BA193">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582858.791 5920930.441</gml:lowerCorner>
          <gml:upperCorner>582885.496 5920959.186</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_4E5F9CB6-8520-40B4-BD48-A4F77C613ED7" srsName="EPSG:25832">
          <gml:posList>582871.287 5920930.441 582858.791 5920936.504 582869.788 5920959.186 
582885.496 5920951.574 582879.666 5920939.537 582876.456 5920941.094 
582871.287 5920930.441 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_CD607F1C-6E01-430F-8656-336FC08C1B92">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582832.052 5920954.0587</gml:lowerCorner>
          <gml:upperCorner>582846.6657 5920974.85</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_07A2753D-A897-4570-817A-F546C8A2E675" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582846.6657 5920972.5638 582840.657 5920973.852 582836.002 5920974.85 
582835.1488 5920970.8741 582834.8256 5920969.3681 582834.661 5920968.601 
582834.053 5920965.751 582832.052 5920956.311 582840.617 5920954.499 
582842.6984 5920954.0587 582846.6657 5920972.5638 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_BB098B6B-8551-46CF-863D-1FE9434098FE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582832.052 5920954.0587</gml:lowerCorner>
          <gml:upperCorner>582846.6657 5920974.85</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F88F699A-F647-4161-8DB9-BE3221BB1135" srsName="EPSG:25832">
          <gml:posList>582846.6657 5920972.5638 582842.6984 5920954.0587 582840.617 5920954.499 
582832.052 5920956.311 582834.053 5920965.751 582834.661 5920968.601 
582834.8256 5920969.3681 582835.1488 5920970.8741 582836.002 5920974.85 
582840.657 5920973.852 582846.6657 5920972.5638 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_F1E3992D-B474-4C75-AFA0-CF14CEDC1921">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582928.2155 5920893.4823</gml:lowerCorner>
          <gml:upperCorner>582928.6871 5920894.9702</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_06846C79-65FA-41C4-9978-3328F5370D97" srsName="EPSG:25832">
          <gml:posList>582928.6871 5920894.9702 582928.578 5920894.493 582928.2155 5920893.4823 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_FBF6380F-4BFA-4626-82EA-20BBA222DEEF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582914.2893 5920900.862</gml:lowerCorner>
          <gml:upperCorner>582929.7168 5920909.491</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F4DC5034-79C9-41FC-B9C1-3184CA2988A2" srsName="EPSG:25832">
          <gml:posList>582929.7168 5920907.9836 582924.859 5920909.491 582922.458 5920905.204 
582921.2858 5920903.112 582920.025 5920900.862 582914.2893 5920903.5308 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_52F51803-730E-4E0C-A340-B3865126E622">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582928.2155 5920864.6597</gml:lowerCorner>
          <gml:upperCorner>582977.3833 5920915.8284</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7155B4DF-331E-4FC8-87AE-295F795B918D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582977.3833 5920905.5494 582934.2309 5920915.8284 582929.5095 5920894.7265 
582928.6871 5920894.9702 582928.578 5920894.493 582928.2155 5920893.4823 
582937.376 5920887.222 582933.4 5920881.404 582943.166 5920874.728 
582952.4937 5920868.669 582970.1821 5920864.6597 582977.3833 5920905.5494 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_2913E35A-3541-434B-AA45-3E727C11E91B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582910.181 5920890.6705</gml:lowerCorner>
          <gml:upperCorner>582929.7168 5920909.491</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_93E204FA-9DF9-41FD-B1BD-19C18160F5CF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582929.7168 5920907.9836 582924.859 5920909.491 582922.458 5920905.204 
582921.2858 5920903.112 582920.025 5920900.862 582914.2893 5920903.5308 
582910.181 5920897.2111 582911.421 5920896.405 582918.686 5920891.682 
582920.2418 5920890.6705 582923.7832 5920896.1178 582928.2155 5920893.4823 
582928.578 5920894.493 582928.6871 5920894.9702 582926.9079 5920895.4974 
582929.7168 5920907.9836 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.6</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_AFEE98DA-62DA-462F-A3E2-AE53E808AAB0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582832.052 5920900.862</gml:lowerCorner>
          <gml:upperCorner>582944.7337 5921056.1277</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CD78EADD-B072-42A3-92F2-926FCB72546F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582883.0662 5921021.0484 582881.1391 5921021.4339 582886.535 5921052.0749 
582864.632 5921056.1277 582859.315 5921029.001 582853.1365 5920996.3199 
582837.9361 5920998.9901 582836.464 5920991.546 582835.32 5920985.761 
582832.2232 5920970.0965 582834.8256 5920969.3681 582834.661 5920968.601 
582834.053 5920965.751 582832.052 5920956.311 582840.617 5920954.499 
582865.0132 5920949.3377 582858.791 5920936.504 582871.287 5920930.441 
582876.456 5920941.094 582879.666 5920939.537 582913.837 5920921.249 
582910.942 5920914.315 582915.7284 5920912.3181 582914.8596 5920910.2357 
582916.3417 5920909.6173 582916.1764 5920906.4336 582914.2893 5920903.5308 
582920.025 5920900.862 582921.2858 5920903.112 582922.458 5920905.204 
582924.859 5920909.491 582929.7168 5920907.9836 582940.1086 5920954.1778 
582944.7337 5920981.061 582877.9594 5920993.009 582883.0662 5921021.0484 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_3340A70A-9E8E-482A-9956-3882E33C3952">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582864.7259 5920999.721</gml:lowerCorner>
          <gml:upperCorner>582877.142 5921013.3344</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A492852A-733B-4C8D-8F41-B6198C420D35" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582877.142 5921011.515 582871.241 5921012.553 582866.8008 5921013.3344 
582864.7259 5921001.5408 582869.164 5921000.76 582875.067 5920999.721 
582877.142 5921011.515 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_14C9ED6F-6484-4281-ACAA-5DF5C31C9139">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582870.578 5921023.401</gml:lowerCorner>
          <gml:upperCorner>582882.358 5921049.67</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_43147716-2C72-4677-820C-FF7B1A3EE46C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582882.358 5921048.379 582874.915 5921049.67 582870.578 5921024.691 
582878.023 5921023.401 582882.358 5921048.379 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_05327455-614A-467B-9A32-9B546930983D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582825.3828 5921025.252</gml:lowerCorner>
          <gml:upperCorner>582837.9479 5921038.9371</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_895453A9-A0B7-4305-B848-92990FC21112" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582837.9479 5921037.0636 582827.5007 5921038.9371 582825.3828 5921027.1255 
582826.791 5921026.873 582832.852 5921025.786 582835.8296 5921025.252 
582837.9479 5921037.0636 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_8CE10650-F7DA-41CD-8781-9C8AD47B204D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582794.94 5920977.136</gml:lowerCorner>
          <gml:upperCorner>582817.119 5921007.966</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>max. 3 Wohnungen</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6D5B33BA-D12F-4DC7-A758-A3CB7028A0E6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582817.119 5921003.628 582804.023 5921007.966 582794.94 5920980.532 
582807.073 5920977.136 582811.647 5920991.261 582812.888 5920990.854 
582817.119 5921003.628 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F34EC4F4-5CC0-4613-90A7-06C92E411C69">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582864.7259 5920999.721</gml:lowerCorner>
          <gml:upperCorner>582877.142 5921013.3344</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_2AA7C384-6CB8-49EE-9398-12C379DF8339" srsName="EPSG:25832">
          <gml:posList>582875.067 5920999.721 582869.164 5921000.76 582864.7259 5921001.5408 
582866.8008 5921013.3344 582871.241 5921012.553 582877.142 5921011.515 
582875.067 5920999.721 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_A8C28D74-D2C5-4C35-A568-BB46D30A06A2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582870.578 5921023.401</gml:lowerCorner>
          <gml:upperCorner>582882.358 5921049.67</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_1A46AEBD-3971-46CA-9B0D-E40C821203DB" srsName="EPSG:25832">
          <gml:posList>582878.023 5921023.401 582870.578 5921024.691 582874.915 5921049.67 
582882.358 5921048.379 582878.023 5921023.401 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_13785B6E-2C3E-4F00-B06C-DB5416036BBA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582825.3828 5921025.252</gml:lowerCorner>
          <gml:upperCorner>582837.9479 5921038.9371</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_BECB65EB-619A-45EE-8E0C-D6F013F9DF78" srsName="EPSG:25832">
          <gml:posList>582835.8296 5921025.252 582832.852 5921025.786 582826.791 5921026.873 
582825.3828 5921027.1255 582827.5007 5921038.9371 582837.9479 5921037.0636 
582835.8296 5921025.252 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_34BE27EB-885D-4420-BD37-6CE0F5DB5414">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582794.94 5920977.136</gml:lowerCorner>
          <gml:upperCorner>582817.119 5921007.966</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_953D825B-F10D-448C-8AC5-53EA02A8C4C9" srsName="EPSG:25832">
          <gml:posList>582807.073 5920977.136 582794.94 5920980.532 582804.023 5921007.966 
582817.119 5921003.628 582812.888 5920990.854 582811.647 5920991.261 
582807.073 5920977.136 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_7E20AFA9-B00F-40BD-A9B5-3FC30D7D7F2E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582787.072 5921031.825</gml:lowerCorner>
          <gml:upperCorner>582804.1207 5921064.2494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F58D797B-E35E-472A-9D17-BA1C575DF1E9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582803.593 5921059.27 582804.1207 5921062.4146 582793.1864 5921064.2494 
582792.686 5921061.312 582789.798 5921044.359 582788.898 5921044.517 
582787.072 5921034.033 582799.741 5921031.825 582801.568 5921042.313 
582800.771 5921042.453 582803.593 5921059.27 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_1097F74F-7977-4716-87F7-0667107DF748">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582787.072 5921031.825</gml:lowerCorner>
          <gml:upperCorner>582804.1207 5921064.2494</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_51454A67-2D4E-4BEB-A5C8-5EA9E263456B" srsName="EPSG:25832">
          <gml:posList>582799.741 5921031.825 582787.072 5921034.033 582788.898 5921044.517 
582789.798 5921044.359 582792.686 5921061.312 582793.1864 5921064.2494 
582804.1207 5921062.4146 582803.593 5921059.27 582800.771 5921042.453 
582801.568 5921042.313 582799.741 5921031.825 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_1B306314-F20D-4FE0-B81B-D0B73607B90F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583006.4789 5920839.9113</gml:lowerCorner>
          <gml:upperCorner>583006.4789 5920839.9113</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_7316A34B-8A53-4029-A6A2-6ADB0C9DD1F1" srsName="EPSG:25832">
          <gml:pos>583006.4789 5920839.9113</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_672430CE-CA3D-4596-B75B-4CA82886EFB4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582999.6246 5920842.7534</gml:lowerCorner>
          <gml:upperCorner>582999.6246 5920842.7534</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_86F57CD6-F022-4A75-B556-494E17C4CE2D" srsName="EPSG:25832">
          <gml:pos>582999.6246 5920842.7534</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_ED575ADB-D55E-4697-A87D-94C48DE455CB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582975.063 5920794.885</gml:lowerCorner>
          <gml:upperCorner>583049.4641 5920920.62</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_344EE7DC-E74B-4DA9-AE3F-59ED02ED593B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583043.9661 5920880.2775 583049.4641 5920919.9097 583044.5419 5920920.62 
583042.241 5920904.128 583036.491 5920888.114 583021.352 5920876.181 
583010.639 5920842.688 582996.128 5920847.831 582999.5253 5920858.0088 
583007.111 5920880.734 582975.063 5920846.266 582986.117 5920838.31 
582992.666 5920830.95 582994.83 5920828.393 583002.5711 5920820.9836 
583018.3976 5920805.8352 583028.603 5920796.067 583029.531 5920794.885 
583030.2576 5920797.7513 583030.965 5920800.542 583031.2945 5920802.4332 
583031.319 5920802.5738 583030.4538 5920802.7246 583035.8594 5920833.7494 
583043.9661 5920880.2775 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_01240569-8FD3-4BB1-8498-5D39D9CFDFAC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582812.2143 5920982.3323</gml:lowerCorner>
          <gml:upperCorner>582812.2143 5920982.3323</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_39DE0121-0501-43B5-BADB-F7FE08BD85EA" srsName="EPSG:25832">
          <gml:pos>582812.2143 5920982.3323</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_E1A7E7E6-AF61-4159-9761-4EE8C6970B98">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582842.839 5920896.1631</gml:lowerCorner>
          <gml:upperCorner>582915.8965 5920988.2066</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7A63350A-8726-4009-A4D2-132E3DC858DB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582911.383 5920964.569 582913.044 5920972.537 582915.8965 5920986.2209 
582904.7987 5920988.2066 582904.248 5920984.95 582901.296 5920967.492 
582885.496 5920951.574 582879.666 5920939.537 582876.456 5920941.094 
582871.287 5920930.441 582858.791 5920936.504 582865.0132 5920949.3377 
582869.788 5920959.186 582842.839 5920941.852 582856.245 5920932.631 
582868.1313 5920924.6091 582877.585 5920918.229 582897.946 5920904.597 
582901.293 5920902.355 582908.749 5920896.712 582909.4997 5920896.1631 
582910.181 5920897.2111 582911.1814 5920898.75 582910.942 5920914.315 
582911.383 5920964.569 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_C0742194-4509-4FC9-BAE7-AB2060FB2E71">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582787.1211 5920993.4951</gml:lowerCorner>
          <gml:upperCorner>582787.1211 5920993.4951</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_586FD429-A2F3-48E7-86B4-C4D3464DBECC" srsName="EPSG:25832">
          <gml:pos>582787.1211 5920993.4951</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_AnpflanzungBindungErhaltung gml:id="Gml_8E484666-D75F-497B-B92C-09BC23078EAB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582791.542 5921002.8571</gml:lowerCorner>
          <gml:upperCorner>582791.542 5921002.8571</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Point gml:id="Gml_1C8254F3-9D99-402A-89C0-E3C8F8FC9573" srsName="EPSG:25832">
          <gml:pos>582791.542 5921002.8571</gml:pos>
        </gml:Point>
      </xplan:position>
      <xplan:massnahme>1000</xplan:massnahme>
      <xplan:gegenstand>1000</xplan:gegenstand>
    </xplan:BP_AnpflanzungBindungErhaltung>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_FreiFlaeche gml:id="Gml_B71C5E2F-1BE3-4B1E-AAD5-F3CCAD4C557C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582778.0586 5920960.082</gml:lowerCorner>
          <gml:upperCorner>582817.119 5921079.9216</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_15275D85-A35F-4864-8C1C-5FBB23F2BBD9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582792.686 5921061.312 582793.1864 5921064.2494 582795.7888 5921079.5256 
582793.4557 5921079.9216 582782.9065 5921018.6974 582778.0586 5920990.5617 
582795.488 5920976.642 582800.9147 5920972.365 582805.547 5920968.714 
582816.984 5920960.082 582817.119 5921003.628 582812.888 5920990.854 
582811.647 5920991.261 582807.073 5920977.136 582794.94 5920980.532 
582804.023 5921007.966 582787.072 5921034.033 582788.898 5921044.517 
582789.798 5921044.359 582792.686 5921061.312 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_FreiFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BaugebietsTeilFlaeche gml:id="Gml_3A416646-5A30-431F-B765-018AC537E9D2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582703.5959 5921027.3709</gml:lowerCorner>
          <gml:upperCorner>582770.761 5921151.4154</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_27C2B0F8-F202-4B84-9DD3-5DAF053F8B76" />
      <xplan:refTextInhalt xlink:href="#Gml_3302262A-F560-4FE6-B941-FC1D727BD319" />
      <xplan:refTextInhalt xlink:href="#Gml_65B86B70-757C-47C4-A01D-FC68E61F14BE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_57D292BF-5290-4856-889D-CF8DF781418D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582770.761 5921062.5303 582738.742 5921068.0599 582740.509 5921080.233 
582741.584 5921087.652 582742.1067 5921091.2593 582724.3373 5921097.7807 
582724.8896 5921101.4665 582731.3919 5921148.8453 582711.9594 5921151.4154 
582710.816 5921143.032 582708.313 5921126.197 582706.178 5921113.368 
582703.5959 5921099.4476 582710.337 5921098.378 582715.845 5921097.504 
582715.531 5921095.499 582719.597 5921094.872 582723.442 5921094.125 
582718.617 5921080.346 582715.4389 5921076.013 582722.5589 5921068.6821 
582718.233 5921064.671 582717.3717 5921063.8724 582722.132 5921058.7385 
582719.8174 5921056.606 582725.3677 5921050.5818 582725.6776 5921050.805 
582732.609 5921047.29 582736.543 5921045.295 582735.338 5921042.919 
582742.36 5921039.283 582766.0255 5921027.3709 582767.0592 5921034.0158 
582770.761 5921062.5303 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:MaxZahlWohnungen>2</xplan:MaxZahlWohnungen>
      <xplan:GRZ>0.4</xplan:GRZ>
      <xplan:Z>1</xplan:Z>
      <xplan:allgArtDerBaulNutzung>2000</xplan:allgArtDerBaulNutzung>
      <xplan:besondereArtDerBaulNutzung>1400</xplan:besondereArtDerBaulNutzung>
      <xplan:bauweise>1000</xplan:bauweise>
      <xplan:bebauungsArt>1000</xplan:bebauungsArt>
    </xplan:BP_BaugebietsTeilFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GruenFlaeche gml:id="Gml_B53FA1D6-4401-4239-85EC-0B83EEEED98B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582699.84 5921005.5348</gml:lowerCorner>
          <gml:upperCorner>582766.0255 5921099.4476</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Garten</xplan:text>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_962D2059-F9B0-47DE-96BA-A704508D92B3" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F536D15E-1018-4CD6-880B-040212A4A491" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582719.8174 5921056.606 582722.132 5921058.7385 582717.3717 5921063.8724 
582718.233 5921064.671 582722.5589 5921068.6821 582715.4389 5921076.013 
582718.617 5921080.346 582723.442 5921094.125 582719.597 5921094.872 
582715.531 5921095.499 582715.845 5921097.504 582710.337 5921098.378 
582703.5959 5921099.4476 582702.531 5921093.707 582699.84 5921081.135 
582700.882 5921079.101 582709.024 5921067.267 582712.626 5921062.471 
582719.648 5921053.119 582727.001 5921043.553 582733.481 5921035.782 
582744.72 5921021.272 582751.641 5921013.636 582757.468 5921008.096 
582760.4429 5921005.5348 582761.8565 5921009.4033 582761.3184 5921009.5999 
582764.9581 5921020.5089 582766.0255 5921027.3709 582742.36 5921039.283 
582735.338 5921042.919 582736.543 5921045.295 582732.609 5921047.29 
582725.6776 5921050.805 582725.3677 5921050.5818 582719.8174 5921056.606 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
      <xplan:nutzungsform>1000</xplan:nutzungsform>
    </xplan:BP_GruenFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_6F657295-97A7-4010-BD20-CEAD61F7B9F9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582751.969 5921041.488</gml:lowerCorner>
          <gml:upperCorner>582764.8371 5921056.089</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_60E6E3B1-D05C-4D3D-B0AA-6CDA5E260AE7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582764.8371 5921054.3414 582764.049 5921054.469 582754.044 5921056.089 
582751.969 5921043.277 582761.836 5921041.679 582763.0156 5921041.488 
582764.8371 5921054.3414 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_44ED1E54-97AB-47FE-B2B0-F31F370D605E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582732.609 5921038.8207</gml:lowerCorner>
          <gml:upperCorner>582751.1814 5921060.3271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_814288EF-2487-4676-89B9-63070F3B0836" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582744.35 5921040.934 582748.99 5921050.086 582751.1814 5921054.4084 
582746.927 5921056.565 582739.985 5921060.084 582739.5054 5921060.3271 
582737.337 5921056.05 582736.501 5921054.401 582734.627 5921051.27 
582732.609 5921047.29 582736.543 5921045.295 582735.338 5921042.919 
582742.36 5921039.283 582743.2785 5921038.8207 582744.35 5921040.934 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_96614508-4856-480C-8F45-52A26FF62BFC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582717.3717 5921055.0729</gml:lowerCorner>
          <gml:upperCorner>582732.6988 5921070.5188</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E930BCC3-55F8-4330-9229-18520066112A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582730.718 5921059.8827 582732.6988 5921061.7194 582724.5397 5921070.5188 
582722.5589 5921068.6821 582718.233 5921064.671 582717.3717 5921063.8724 
582722.132 5921058.7385 582725.5306 5921055.0729 582730.718 5921059.8827 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A2B461C6-501B-441F-A7D7-E0460B120EA0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582722.4338 5921079.7965</gml:lowerCorner>
          <gml:upperCorner>582736.5501 5921094.689</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A4A5104B-29CD-42C1-A73B-0216845AB596" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582736.5501 5921091.0958 582735.21 5921091.575 582726.501 5921094.689 
582723.044 5921085.093 582722.4338 5921083.3992 582722.4608 5921083.3896 
582732.5096 5921079.7965 582736.5501 5921091.0958 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_256F1540-757E-4D50-B33C-4DE30CD90EDA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582710.337 5921094.872</gml:lowerCorner>
          <gml:upperCorner>582723.556 5921120.089</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F9E2AD27-E13F-4D6C-81DD-A90D53C03022" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582723.556 5921119.14 582718.153 5921120.089 582717.851 5921117.868 
582717.383 5921117.932 582716.045 5921109.862 582712.252 5921110.459 
582710.337 5921098.378 582715.845 5921097.504 582715.531 5921095.499 
582719.597 5921094.872 582721.77 5921108.973 582723.556 5921119.14 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_53DB315A-2C35-4A89-A5E3-A7B585D631B5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582715.5076 5921132.521</gml:lowerCorner>
          <gml:upperCorner>582727.4917 5921146.3596</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4764D88C-4BFF-493D-B9B1-87A293EDBA52" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582727.4917 5921144.8941 582717.284 5921146.3596 582715.5076 5921133.9865 
582725.7153 5921132.521 582727.4917 5921144.8941 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_E1CA7728-1691-4DBE-9BB2-343081D577A7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582751.969 5921041.488</gml:lowerCorner>
          <gml:upperCorner>582764.8371 5921056.089</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_7A384E55-4F72-4075-B588-2C5EACA19C5E" srsName="EPSG:25832">
          <gml:posList>582751.969 5921043.277 582754.044 5921056.089 582764.049 5921054.469 
582764.8371 5921054.3414 582763.0156 5921041.488 582761.836 5921041.679 
582751.969 5921043.277 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_1C0999A6-1B00-4BF1-A38C-C2B0DD009287">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582732.609 5921038.8207</gml:lowerCorner>
          <gml:upperCorner>582751.1814 5921060.3271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_07B988BD-33A3-427A-A23D-457FA7AECDB4" srsName="EPSG:25832">
          <gml:posList>582736.543 5921045.295 582732.609 5921047.29 582734.627 5921051.27 
582736.501 5921054.401 582737.337 5921056.05 582739.5054 5921060.3271 
582739.985 5921060.084 582746.927 5921056.565 582751.1814 5921054.4084 
582748.99 5921050.086 582744.35 5921040.934 582743.2785 5921038.8207 
582742.36 5921039.283 582735.338 5921042.919 582736.543 5921045.295 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_6FE3506D-C86D-4BA6-9D5F-444DFC75E5B6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582717.3717 5921055.0729</gml:lowerCorner>
          <gml:upperCorner>582732.6988 5921070.5188</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_CB919BE4-92BD-4487-BFC7-1340D5A98961" srsName="EPSG:25832">
          <gml:posList>582732.6988 5921061.7194 582730.718 5921059.8827 582725.5306 5921055.0729 
582722.132 5921058.7385 582717.3717 5921063.8724 582718.233 5921064.671 
582722.5589 5921068.6821 582724.5397 5921070.5188 582732.6988 5921061.7194 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_71581799-C1C2-4BE9-98EC-88DA9FB5A0D3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582722.4338 5921079.7965</gml:lowerCorner>
          <gml:upperCorner>582736.5501 5921094.689</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_B853B11A-7DFC-4190-AC0E-AE11C1A5AEA1" srsName="EPSG:25832">
          <gml:posList>582736.5501 5921091.0958 582732.5096 5921079.7965 582722.4608 5921083.3896 
582722.4338 5921083.3992 582723.044 5921085.093 582726.501 5921094.689 
582735.21 5921091.575 582736.5501 5921091.0958 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_2E48B104-249B-428F-9DE1-B665DE28C156">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582710.337 5921094.872</gml:lowerCorner>
          <gml:upperCorner>582723.556 5921120.089</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_BC631A65-4CC4-4D87-8042-C4F6E109BC6E" srsName="EPSG:25832">
          <gml:posList>582716.045 5921109.862 582717.383 5921117.932 582717.851 5921117.868 
582718.153 5921120.089 582723.556 5921119.14 582721.77 5921108.973 
582719.597 5921094.872 582715.531 5921095.499 582715.845 5921097.504 
582710.337 5921098.378 582712.252 5921110.459 582716.045 5921109.862 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_4906B690-8273-49AE-9BF5-2608E952364C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582715.5076 5921132.521</gml:lowerCorner>
          <gml:upperCorner>582727.4917 5921146.3596</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_C0DD97D4-6849-4821-B88B-B827470A2C68" srsName="EPSG:25832">
          <gml:posList>582725.7153 5921132.521 582715.5076 5921133.9865 582717.284 5921146.3596 
582727.4917 5921144.8941 582725.7153 5921132.521 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_FA27A49A-AA08-4A70-B71F-C4CE5903C04F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582711.9594 5920990.5617</gml:lowerCorner>
          <gml:upperCorner>582855.514 5921477.987</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7742A0DB-6E8B-4344-AAF1-BA81D7F20581" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582812.5686 5921292.4935 582823.152 5921348.162 582835.6403 5921418.0121 
582839.662 5921444.3647 582842.0962 5921453.3605 582849.0812 5921467.3305 
582855.514 5921477.987 582844.732 5921471.354 582838.561 5921467.936 
582820.552 5921457.96 582821.2066 5921448.9067 582791.6267 5921275.9162 
582789.9069 5921276.1807 582818.6159 5921448.8664 582818.3482 5921456.8633 
582816.339 5921455.864 582813.623 5921454.351 582801.232 5921447.453 
582798.414 5921445.377 582787.998 5921439.878 582780.876 5921436.12 
582762.736 5921427.772 582755.795 5921392.755 582750.198 5921363.295 
582741.031 5921314.162 582734.092 5921274.782 582730.279 5921255.158 
582723.937 5921221.981 582716.643 5921182.659 582714.201 5921167.851 
582711.9594 5921151.4154 582731.3919 5921148.8453 582742.3833 5921223.8645 
582744.19 5921223.8645 582736.0598 5921170.2656 582726.1763 5921101.2899 
582724.8896 5921101.4665 582724.3373 5921097.7807 582742.1067 5921091.2593 
582741.584 5921087.652 582740.509 5921080.233 582738.742 5921068.0599 
582770.761 5921062.5303 582772.4994 5921074.7087 582773.2972 5921074.6053 
582766.1231 5921019.2259 582762.7575 5921009.0741 582761.8565 5921009.4033 
582760.4429 5921005.5348 582761.186 5921004.895 582768.139 5920998.914 
582769.032 5920998.093 582771.716 5920995.627 582774.432 5920993.458 
582778.0586 5920990.5617 582782.9065 5921018.6974 582792.4852 5921074.2892 
582790.4715 5921074.7432 582805.9661 5921157.4846 582827.1163 5921268.0388 
582814.3526 5921269.1375 582809.426 5921268.882 582798.5082 5921214.4108 
582787.8719 5921157.5782 582778.1088 5921101.9362 582774.14 5921079.7905 
582773.4256 5921079.9493 582785.8082 5921151.7044 582796.1269 5921209.1721 
582802.8738 5921242.2715 582812.5686 5921292.4935 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582800.4145 5921436.6769 582768.7698 5921255.3404 582753.2577 5921156.0629 
582742.5544 5921095.7207 582741.6236 5921095.8759 582750.7758 5921155.4424 
582767.5289 5921257.5121 582798.5531 5921436.9872 582800.4145 5921436.6769 
</gml:posList>
            </gml:LinearRing>
          </gml:interior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582780.0937 5921425.0428 582771.562 5921376.8002 582768.925 5921377.1104 
582776.9913 5921425.198 582780.0937 5921425.0428 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_10A2863D-287B-405A-8C74-877609CB8792">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582715.0552 5921062.7724</gml:lowerCorner>
          <gml:upperCorner>582842.3849 5921464.6623</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7CCF6B54-7046-4564-A923-B588884450BD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582789.2573 5921180.0193 582796.3841 5921217.7053 582804.2669 5921256.9031 
582830.3987 5921393.7175 582838.3895 5921445.8733 582842.3849 5921464.6623 
582765.0691 5921421.0372 582750.9233 5921351.6042 582735.4818 5921268.7813 
582723.6036 5921206.5831 582718.0965 5921171.4887 582715.0552 5921151.006 
582731.3919 5921148.8453 582732.9579 5921148.6175 582726.1763 5921101.2899 
582724.8896 5921101.4665 582724.3373 5921097.7807 582742.1067 5921091.2593 
582741.584 5921087.652 582740.509 5921080.233 582738.742 5921068.0599 
582769.3589 5921062.7724 582771.2241 5921074.9519 582771.98 5921080.3511 
582789.2573 5921180.0193 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">25549</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_2D10A095-207E-45AA-93D3-7FE207AD170A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582715.0552 5921062.7724</gml:lowerCorner>
          <gml:upperCorner>582842.3849 5921464.6623</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8273F03B-C98E-42F1-AA41-5A82F07B175B" srsName="EPSG:25832">
          <gml:posList>582715.0552 5921151.006 582718.0965 5921171.4887 582723.6036 5921206.5831 
582735.4818 5921268.7813 582750.9233 5921351.6042 582765.0691 5921421.0372 
582842.3849 5921464.6623 582838.3895 5921445.8733 582830.3987 5921393.7175 
582804.2669 5921256.9031 582796.3841 5921217.7053 582789.2573 5921180.0193 
582771.98 5921080.3511 582771.2241 5921074.9519 582769.3589 5921062.7724 
582738.742 5921068.0599 582740.509 5921080.233 582741.584 5921087.652 
582742.1067 5921091.2593 582724.3373 5921097.7807 582724.8896 5921101.4665 
582726.1763 5921101.2899 582732.9579 5921148.6175 582731.3919 5921148.8453 
582715.0552 5921151.006 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_A148CD72-11C3-43E4-89A8-E6614E055115">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582793.4557 5921077.1984</gml:lowerCorner>
          <gml:upperCorner>582845.6324 5921269.3558</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C6AB7240-B54E-4160-BFFF-E1B5FD94128B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582813.4977 5921098.8072 582823.1652 5921150.6669 582833.4691 5921204.3558 
582845.6324 5921267.8064 582845.0901 5921269.3558 582829.5954 5921268.8135 
582793.4557 5921079.9216 582809.5016 5921077.1984 582813.4977 5921098.8072 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_6D648862-6D06-4311-93F5-ACB522E4C5D3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582812.5702 5921073.6585</gml:lowerCorner>
          <gml:upperCorner>582865.9304 5921270.9053</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4E1481A4-4C3D-4E73-890B-D2CEA59D9B1E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582835.3285 5921099.8445 582845.9423 5921162.8302 582859.8875 5921236.972 
582865.9304 5921270.9053 582848.8088 5921270.208 582836.2581 5921201.7217 
582825.8767 5921148.6526 582814.6539 5921088.0154 582812.5702 5921076.6776 
582830.3598 5921073.6585 582835.3285 5921099.8445 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_6A1932F1-771B-4A20-B947-2D936B11C1F9">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582869.1388 5921268.945</gml:lowerCorner>
          <gml:upperCorner>582886.5286 5921273.9486</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_CFEF65DC-57B4-467F-94D2-ED01D18CBF72" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A9FC1BCB-8CF5-44CD-8914-D5DF872488A6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582885.6749 5921269.651 582886.5286 5921273.9486 582869.9136 5921272.8837 
582869.1388 5921268.945 582885.6749 5921269.651 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>9999</xplan:klassifizMassnahme>
          <xplan:massnahmeText>Erhaltung von Gräben</xplan:massnahmeText>
          <xplan:massnahmeKuerzel>G</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_2441751E-827C-4EDE-A4DB-5806C348C2F2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582825.8908 5921042.7284</gml:lowerCorner>
          <gml:upperCorner>582886.5286 5921273.9486</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3888CA91-C24D-4441-B5ED-DD9963556922" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582859.9435 5921136.2091 582866.7573 5921172.7279 582882.2988 5921252.6558 
582886.5286 5921273.9486 582869.9136 5921272.8837 582848.189 5921162.4428 
582837.8851 5921098.295 582830.3702 5921061.6502 582828.5287 5921062.0226 
582827.938 5921058.269 582825.8908 5921046.5553 582842.886 5921042.7284 
582859.9435 5921136.2091 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_BFA9EDD6-B918-43F0-8A0F-5188E02F0062">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582793.4557 5921043.0097</gml:lowerCorner>
          <gml:upperCorner>582883.5314 5921266.4225</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AC049F18-1B7F-4ADA-9E29-3419B1DED566" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582848.7122 5921083.013 582859.0838 5921139.5281 582868.2914 5921188.4232 
582883.5314 5921266.4225 582828.627 5921263.752 582821.079 5921224.3008 
582819.0598 5921213.7469 582807.1277 5921151.3814 582806.0251 5921145.6183 
582793.4557 5921079.9216 582795.7888 5921079.5256 582809.5016 5921077.1984 
582812.5702 5921076.6776 582830.3598 5921073.6585 582828.5287 5921062.0226 
582827.938 5921058.269 582825.8908 5921046.5553 582841.637 5921043.0097 
582848.7122 5921083.013 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">10769</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_1086031A-D192-4595-802B-1D64DA93B627">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582793.4557 5921043.0097</gml:lowerCorner>
          <gml:upperCorner>582883.5314 5921266.4225</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_E25AF3D0-6B13-4AA8-9D24-BF228C75CBB5" srsName="EPSG:25832">
          <gml:posList>582793.4557 5921079.9216 582806.0251 5921145.6183 582807.1277 5921151.3814 
582819.0598 5921213.7469 582821.079 5921224.3008 582828.627 5921263.752 
582883.5314 5921266.4225 582868.2914 5921188.4232 582859.0838 5921139.5281 
582848.7122 5921083.013 582841.637 5921043.0097 582825.8908 5921046.5553 
582827.938 5921058.269 582828.5287 5921062.0226 582830.3598 5921073.6585 
582812.5702 5921076.6776 582809.5016 5921077.1984 582795.7888 5921079.5256 
582793.4557 5921079.9216 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_72E541E3-4766-47D0-A2BD-C27ACE32CD95">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582888.9036 5921047.9426</gml:lowerCorner>
          <gml:upperCorner>582930.6115 5921158.3624</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_9EFD0E16-87E5-4526-91ED-08F5A0C2FDEE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0B71A91D-4165-43F0-9D93-5E9C2998725B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582930.6115 5921154.2548 582908.7951 5921158.3624 582888.9036 5921051.8323 
582910.8386 5921047.9426 582930.6115 5921154.2548 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1400</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>O</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_08852B53-BC70-415D-94D3-1111653369EC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582837.9361 5920996.3199</gml:lowerCorner>
          <gml:upperCorner>582955.4881 5921307.3738</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AD5F55CC-D190-4822-BBA6-FD0C9730FC5D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582955.4881 5921297.7056 582913.421 5921305.128 582899.802 5921307.3738 
582899.5247 5921306.4005 582895.3905 5921306.0942 582889.036 5921269.5754 
582875.4085 5921206.1077 582862.3168 5921136.2856 582854.3547 5921094.7905 
582845.3972 5921039.6678 582837.9361 5920998.9901 582853.1365 5920996.3199 
582859.315 5921029.001 582864.632 5921056.1277 582886.535 5921052.0749 
582931.4493 5921292.851 582934.5435 5921292.2985 582917.4147 5921204.5549 
582908.7951 5921158.3624 582930.6115 5921154.2548 582947.8716 5921249.9122 
582955.4881 5921297.7056 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582870.3416 5921092.3402 582909.0767 5921295.1172 582912.1458 5921294.6939 
582872.0349 5921092.0227 582870.3416 5921092.3402 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_6ED95C22-3BD6-4FE1-82C5-AA0900942E4F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582877.9594 5920981.061</gml:lowerCorner>
          <gml:upperCorner>582993.9858 5921249.3188</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_54DA7880-AFA8-4AE3-A0DB-0BBBDA991DD4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582993.9858 5921238.1478 582976.043 5921243.173 582973.404 5921243.912 
582955.3698 5921249.3188 582954.594 5921248.1862 582950.506 5921247.8229 
582934.5177 5921164.4292 582921.7089 5921096.1154 582903.1769 5920999.7312 
582901.9168 5920999.9735 582910.8386 5921047.9426 582888.9036 5921051.8323 
582883.0662 5921021.0484 582877.9594 5920993.009 582944.7337 5920981.061 
582971.7155 5921119.7774 582983.092 5921178.007 582993.9858 5921238.1478 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582972.3948 5921234.2424 582975.173 5921233.7132 582927.4156 5920987.7825 
582925.2989 5920987.9148 582972.3948 5921234.2424 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_F31C2FB4-5C73-4714-A653-A547BC0E0E06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582839.4291 5920996.5738</gml:lowerCorner>
          <gml:upperCorner>582883.7895 5921172.0059</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CD55E80D-C1B2-46BD-B345-0DC71FE5E897" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582883.7895 5921169.6246 582870.719 5921172.0059 582856.5903 5921099.1924 
582848.9703 5921052.6786 582839.4291 5920998.7278 582851.691 5920996.5738 
582883.7895 5921169.6246 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">2315</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_34FF8A72-EC81-41E9-B0E8-5DBDAFE180EE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582839.4291 5920996.5738</gml:lowerCorner>
          <gml:upperCorner>582883.7895 5921172.0059</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_D485E766-A923-4D59-AEB0-A1B55B790820" srsName="EPSG:25832">
          <gml:posList>582839.4291 5920998.7278 582848.9703 5921052.6786 582856.5903 5921099.1924 
582870.719 5921172.0059 582883.7895 5921169.6246 582851.691 5920996.5738 
582839.4291 5920998.7278 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_7260FAE5-8799-450D-83DC-D2AB75317719">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582877.9594 5920989.0852</gml:lowerCorner>
          <gml:upperCorner>582909.8811 5921046.8479</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F45A7BC8-B7E5-4B26-BA7A-2D56FF5C8C60" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582901.9168 5920999.9735 582909.8811 5921042.7945 582887.9584 5921046.8479 
582883.0662 5921021.0484 582877.9594 5920993.009 582899.8886 5920989.0852 
582901.9168 5920999.9735 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">1197</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_0B431741-9B6B-4D64-85F3-A18158EDA4A0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582877.9594 5920989.0852</gml:lowerCorner>
          <gml:upperCorner>582909.8811 5921046.8479</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F2324369-3AF4-48A8-8359-F8FE487865EA" srsName="EPSG:25832">
          <gml:posList>582909.8811 5921042.7945 582901.9168 5920999.9735 582899.8886 5920989.0852 
582877.9594 5920993.009 582883.0662 5921021.0484 582887.9584 5921046.8479 
582909.8811 5921042.7945 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_C1EA050F-55C3-4CE9-A005-83311B171B5A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582915.1408 5920981.2934</gml:lowerCorner>
          <gml:upperCorner>582975.5234 5921151.3484</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1744CBC5-232C-4D8B-857F-7E696CA60D8C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582975.5234 5921145.8286 582947.2342 5921151.3484 582915.1408 5920986.3561 
582915.8965 5920986.2209 582943.4348 5920981.2934 582975.5234 5921145.8286 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">4812</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_848CA123-66F0-44D7-957E-6B8836F0B951">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582915.1408 5920981.2934</gml:lowerCorner>
          <gml:upperCorner>582975.5234 5921151.3484</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_841F1A1E-FCE3-4006-BAF5-EB1B5EFFFCA2" srsName="EPSG:25832">
          <gml:posList>582915.1408 5920986.3561 582947.2342 5921151.3484 582975.5234 5921145.8286 
582943.4348 5920981.2934 582915.8965 5920986.2209 582915.1408 5920986.3561 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_797DFDD5-B2DE-4173-880D-A2232C570312">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582934.2309 5920905.5494</gml:lowerCorner>
          <gml:upperCorner>583051.0474 5921343.3352</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_36B40755-1C52-400D-979D-1A8A83862FF6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582980.9233 5920928.4617 582981.3202 5920933.4888 582979.9972 5920963.7837 
583007.7231 5921107.6698 583051.0474 5921331.8525 583047.7819 5921336.3669 
583017.7086 5921343.3352 582994.6182 5921225.908 582975.1584 5921125.0166 
582951.3576 5921001.6715 582948.0644 5920982.5111 582943.2743 5920952.573 
582934.2309 5920915.8284 582977.3833 5920905.5494 582980.9233 5920928.4617 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_EA30DD16-2702-4A4D-AF84-AE7417EF707F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582984.0361 5920925.2597</gml:lowerCorner>
          <gml:upperCorner>583013.5865 5920952.8034</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_708ED29E-B809-49D0-9F03-1ED238D633BF" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583013.5865 5920939.6741 583011.0859 5920941.8232 582984.2306 5920952.8034 
582984.0361 5920929.3514 583012.3903 5920925.2597 583013.5865 5920939.6741 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_B80EB37D-DA85-4E7A-9958-B55B9BB02063">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582935.5344 5920905.9255</gml:lowerCorner>
          <gml:upperCorner>583033.8911 5921256.1898</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AE73D66B-23CE-4254-ABF2-ABC47E054C50" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582979.4791 5920926.6224 582979.7173 5920932.9922 582978.5862 5920963.1748 
582978.7648 5920965.3775 583033.8911 5921249.6413 583002.2798 5921256.1898 
582958.1387 5921030.8912 582947.6892 5920971.8069 582944.5936 5920952.1019 
582935.5344 5920915.5179 582975.8044 5920905.9255 582979.4791 5920926.6224 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">11554</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F71E309A-6EDE-4803-85E8-218999DC0CB1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582935.5344 5920905.9255</gml:lowerCorner>
          <gml:upperCorner>583033.8911 5921256.1898</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_C935D93F-CFC6-4735-99F8-7B10AC743525" srsName="EPSG:25832">
          <gml:posList>582935.5344 5920915.5179 582944.5936 5920952.1019 582947.6892 5920971.8069 
582958.1387 5921030.8912 583002.2798 5921256.1898 583033.8911 5921249.6413 
582978.7648 5920965.3775 582978.5862 5920963.1748 582979.7173 5920932.9922 
582979.4791 5920926.6224 582975.8044 5920905.9255 582935.5344 5920915.5179 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_CE3A3DFC-260C-4163-9C74-FC8D64E21324">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582982.7754 5920919.9097</gml:lowerCorner>
          <gml:upperCorner>583125.8586 5921334.7657</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2919A117-7B9F-44F4-B5C3-63859EA9E753" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583078.016 5921329.358 583054.6906 5921334.7657 583019.9847 5921155.2856 
582987.6959 5920989.7542 582982.7754 5920963.5191 582982.7754 5920956.5076 
582988.4639 5920953.994 583012.2765 5920943.6753 583016.9067 5920939.1774 
583015.4121 5920926.9741 583014.8932 5920924.8985 583049.4641 5920919.9097 
583050.8915 5920928.9685 583053.0896 5920936.845 583073.6967 5921042.2619 
583088.6254 5921119.5615 583125.8586 5921318.2408 583078.016 5921329.358 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582999.0088 5920956.1078 583015.233 5921040.2775 583069.0232 5921323.6016 
583071.5276 5921323.1661 583016.9752 5921039.9509 583001.4044 5920955.7812 
582999.0088 5920956.1078 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583088.7318 5921322.5127 583090.6917 5921322.1861 583047.7902 5921099.4032 
583035.9215 5921039.5153 583018.8263 5920947.6147 583015.8863 5920948.3769 
583088.7318 5921322.5127 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583039.9364 5920968.4207 583106.6974 5921314.7975 583108.6482 5921314.5808 
583042.1039 5920968.204 583039.9364 5920968.4207 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_79B9A9E9-D2B0-499F-A5A4-8932931930C7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582984.4266 5920920.5798</gml:lowerCorner>
          <gml:upperCorner>583067.3376 5921106.3959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_14524A29-419F-4619-A40A-1466893E4A8C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583045.8061 5920928.2979 583051.8466 5920954.9931 583067.3376 5921034.4942 
583036.1248 5921040.5409 583040.8149 5921064.207 583047.7902 5921099.4032 
583012.0961 5921106.3959 582984.4266 5920964.1513 582984.524 5920957.7211 
582987.8365 5920955.7725 583013.0703 5920944.958 583015.506 5920943.2043 
583018.8186 5920939.2098 583016.5459 5920924.66 583044.5419 5920920.62 
583044.8203 5920920.5798 583045.8061 5920928.2979 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">9272</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_D061E510-B702-4A74-8C4B-58D9E6947EA4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582984.4266 5920920.5798</gml:lowerCorner>
          <gml:upperCorner>583067.3376 5921106.3959</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_826FE684-804B-4FBE-AE9A-BBFB0E197255" srsName="EPSG:25832">
          <gml:posList>583016.5459 5920924.66 583018.8186 5920939.2098 583015.506 5920943.2043 
583013.0703 5920944.958 582987.8365 5920955.7725 582984.524 5920957.7211 
582984.4266 5920964.1513 583012.0961 5921106.3959 583047.7902 5921099.4032 
583040.8149 5921064.207 583036.1248 5921040.5409 583067.3376 5921034.4942 
583051.8466 5920954.9931 583045.8061 5920928.2979 583044.8203 5920920.5798 
583044.5419 5920920.62 583016.5459 5920924.66 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_F423BD1A-9CBE-47BD-9F5C-034C16281AE3">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583029.531 5920764.0597</gml:lowerCorner>
          <gml:upperCorner>583189.9326 5921259.5849</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_DDC6F918-E9DB-41E8-8B68-9CD1652ED7C0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583151.3885 5921250.4608 583149.3042 5921252.8025 583117.7994 5921259.5849 
583077.5434 5921051.1459 583063.4389 5920976.8686 583052.7232 5920918.9855 
583046.3121 5920879.603 583039.1683 5920843.7924 583032.0245 5920802.395 
583031.2945 5920802.4332 583030.965 5920800.542 583030.2576 5920797.7513 
583029.531 5920794.885 583036.4249 5920787.0461 583050.455 5920771.093 
583055.8264 5920764.0597 583063.9099 5920786.0444 583069.21 5920800.459 
583070.93 5920810.7142 583069.3489 5920810.922 583075.2484 5920848.3225 
583084.5985 5920894.5165 583086.1569 5920894.1825 583080.2574 5920862.0137 
583072.925 5920819.9847 583089.2616 5920817.2447 583092.2416 5920837.5236 
583101.6439 5920889.6971 583103.4874 5920889.5127 583094.9148 5920841.2107 
583091.4134 5920816.8838 583106.9905 5920814.2712 583136.8792 5920971.5057 
583189.9326 5921244.0559 583157.7094 5921250.993 583154.9783 5921249.2642 
583087.0717 5920898.5132 583085.4264 5920898.5132 583117.1361 5921068.878 
583151.8372 5921247.918 583151.3885 5921250.4608 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583066.7296 5920904.0475 583132.243 5921250.4608 583134.9354 5921250.0121 
583068.9732 5920903.5987 583066.7296 5920904.0475 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583048.3112 5920805.6904 583066.0096 5920899.9707 583068.1245 5920899.6368 
583050.0921 5920805.4678 583048.3112 5920805.6904 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583102.777 5920894.6243 583169.7861 5921241.1872 583172.3289 5921240.888 
583104.5718 5920894.4747 583102.777 5920894.6243 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_7DE6E128-28B9-425F-A3D5-9E619B6903BC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583072.925 5920815.1131</gml:lowerCorner>
          <gml:upperCorner>583125.0523 5920938.4768</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9C9F6195-7B51-4D36-B767-DB3765CE2C24" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583125.0523 5920932.0797 583094.8088 5920938.4768 583087.0717 5920898.5132 
583086.1569 5920894.1825 583080.2574 5920862.0137 583072.925 5920819.9847 
583089.2616 5920817.2447 583091.4134 5920816.8838 583098.7156 5920815.6591 
583101.9713 5920815.1131 583125.0523 5920932.0797 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">3641</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_AB9FFD19-11F8-45F3-AD17-A1B2927BFB9A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583072.925 5920815.1131</gml:lowerCorner>
          <gml:upperCorner>583125.0523 5920938.4768</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_6CCC5E1D-BD14-4EF8-9CB2-3E4167CF63A6" srsName="EPSG:25832">
          <gml:posList>583101.9713 5920815.1131 583098.7156 5920815.6591 583091.4134 5920816.8838 
583089.2616 5920817.2447 583072.925 5920819.9847 583080.2574 5920862.0137 
583086.1569 5920894.1825 583087.0717 5920898.5132 583094.8088 5920938.4768 
583125.0523 5920932.0797 583101.9713 5920815.1131 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_01B279AC-12F4-4B0A-92B8-CB7298D0837C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583087.497 5920689.7761</gml:lowerCorner>
          <gml:upperCorner>583208.1957 5921055.7749</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AB9622A3-76B1-461F-BCF9-F9006E2BA2A2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583161.446 5920908.8326 583186.7332 5921049.4609 583155.4885 5921055.7749 
583148.582 5921019.6042 583121.6655 5920879.8727 583109.2605 5920813.1666 
583098.3769 5920749.3863 583091.8233 5920723.055 583090.7703 5920722.8962 
583089.8178 5920721.1896 583089.0816 5920721.3169 583088.587 5920720.0013 
583087.497 5920717.102 583090.866 5920711.313 583103.0288 5920689.7761 
583107.7892 5920725.5805 583112.257 5920759.1838 583152.9525 5920745.3347 
583179.0333 5920894.9167 583208.1957 5921045.1237 583188.3345 5921049.1373 
583161.446 5920895.6648 583145.1919 5920799.375 583143.7517 5920799.2721 
583161.446 5920908.8326 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_788C062F-E0D0-465A-A7D4-AABA4E65C0AB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583154.9354 5920622.931</gml:lowerCorner>
          <gml:upperCorner>583267.115 5921044.7135</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D58054F3-709A-40B3-8115-7B944B6AEB18" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583267.115 5921033.686 583230.027 5921040.712 583210.2257 5921044.7135 
583193.359 5920962.3218 583185.2544 5920919.5159 583175.4947 5920865.1238 
583168.3603 5920824.9433 583161.8538 5920786.5892 583154.9354 5920744.6599 
583170.0458 5920739.5177 583227.1838 5921033.0612 583229.0088 5921032.757 
583219.0723 5920980.8435 583203.3563 5920900.6412 583188.9584 5920827.1309 
583171.4444 5920736.1241 583170.54 5920736.292 583172.972 5920726.83 
583165.093 5920685.543 583159.4575 5920656.0077 583181.0583 5920651.8867 
583176.052 5920625.645 583190.278 5920622.931 583199.395 5920681.316 
583202.118 5920696.869 583211.448 5920745.352 583243.938 5920912.69 
583255.331 5920970.157 583255.757 5920972.311 583266.641 5921031.183 
583267.115 5921033.686 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_A6DEFF9A-F5F7-4915-837B-F9279111207B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583112.257 5920745.3347</gml:lowerCorner>
          <gml:upperCorner>583179.0333 5920902.3671</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B8F9D5FF-5819-4FC9-9A75-C7E203A8E379" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583179.0333 5920894.9167 583136.8152 5920902.3671 583112.257 5920759.1838 
583115.4974 5920758.081 583152.9525 5920745.3347 583179.0333 5920894.9167 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">6316</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_0234064A-B7DC-41CB-9A46-64E35CB2AB18">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583112.257 5920745.3347</gml:lowerCorner>
          <gml:upperCorner>583179.0333 5920902.3671</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_1AF2AD3A-6616-4479-BE16-7FD381790DC1" srsName="EPSG:25832">
          <gml:posList>583112.257 5920759.1838 583136.8152 5920902.3671 583179.0333 5920894.9167 
583152.9525 5920745.3347 583115.4974 5920758.081 583112.257 5920759.1838 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_F6DAC905-DA18-46AE-BDE6-36FADF8DDECD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582901.8775 5920141.0486</gml:lowerCorner>
          <gml:upperCorner>583036.6901 5920371.1041</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A1E72646-DE82-4321-8993-058306A1B51C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583004.8362 5920269.0353 583036.6901 5920353.5659 582985.4388 5920371.1041 
582979.9737 5920359.4232 582972.3043 5920340.424 582903.2796 5920165.7705 
582901.8775 5920162.9427 582954.8261 5920141.0486 583004.8362 5920269.0353 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_CFAC41FF-ADCE-49C8-B871-4B0AB26E6E05">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582903.2664 5920144.3464</gml:lowerCorner>
          <gml:upperCorner>583035.2919 5920371.1041</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4E3F593B-4413-4784-A493-8756A20FAC5E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582976.844 5920201.4303 583001.1196 5920263.8721 583023.5431 5920322.6097 
583035.2919 5920354.0444 582987.2368 5920370.4888 582985.4388 5920371.1041 
582979.9737 5920359.4232 582972.3043 5920340.424 582968.7195 5920331.3534 
582966.9185 5920326.7964 582953.5623 5920293.0009 582951.7501 5920288.4157 
582939.9856 5920258.6479 582935.349 5920246.9158 582926.663 5920224.9377 
582923.0245 5920215.731 582903.2796 5920165.7705 582903.2664 5920165.7439 
582954.4206 5920144.3464 582976.844 5920201.4303 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">12222</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_0E962F37-105D-462B-870F-361683C8F77D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582903.2664 5920144.3464</gml:lowerCorner>
          <gml:upperCorner>583035.2919 5920371.1041</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_A4DABA12-9801-4CAA-A03C-F58EA401A0B9" srsName="EPSG:25832">
          <gml:posList>583035.2919 5920354.0444 583023.5431 5920322.6097 583001.1196 5920263.8721 
582976.844 5920201.4303 582954.4206 5920144.3464 582903.2664 5920165.7439 
582903.2796 5920165.7705 582923.0245 5920215.731 582926.663 5920224.9377 
582935.349 5920246.9158 582939.9856 5920258.6479 582951.7501 5920288.4157 
582953.5623 5920293.0009 582966.9185 5920326.7964 582968.7195 5920331.3534 
582972.3043 5920340.424 582979.9737 5920359.4232 582985.4388 5920371.1041 
582987.2368 5920370.4888 583035.2919 5920354.0444 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_AA276AF7-7241-4618-9600-BFBAD010B795">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582921.845 5920252.2291</gml:lowerCorner>
          <gml:upperCorner>582998.133 5920405.9593</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6C9E040A-21EB-4444-BE7A-B1EC174027BD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582987.5702 5920377.3706 582995.352 5920394.328 582998.133 5920397.029 
582981.0904 5920405.9593 582921.845 5920258.612 582935.8997 5920252.2291 
582958.5342 5920309.572 582978.4921 5920360.2947 582985.8339 5920376.7749 
582987.0424 5920376.2205 582987.5702 5920377.3706 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_6F43291A-365E-4EFF-BDB7-994F228B6CD8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582853.8431 5920259.6822</gml:lowerCorner>
          <gml:upperCorner>583010.028 5920491.6103</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A56FCA92-0C50-43B5-9CEA-7250B554EF11" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583005.7632 5920449.7372 583010.028 5920455.55 582996.3166 5920472.1004 
582989.5445 5920480.2748 582980.1535 5920491.6103 582954.234 5920446.5459 
582937.5847 5920400.1205 582930.3619 5920382.9919 582923.2422 5920364.1093 
582920.8689 5920366.2761 582932.2192 5920395.6835 582950.001 5920444.652 
582941.2885 5920448.7934 582936.924 5920450.868 582928.235 5920454.828 
582921.4591 5920457.472 582888.0784 5920376.7772 582853.8431 5920289.495 
582868.9363 5920282.6405 582902.1906 5920366.9658 582903.3811 5920366.7278 
582870.542 5920281.9112 582885.7645 5920274.998 582918.4994 5920359.5852 
582920.4041 5920358.871 582887.8546 5920274.0487 582901.929 5920267.6569 
582935.6415 5920353.395 582936.5938 5920353.0379 582902.8887 5920267.221 
582919.4885 5920259.6822 582988.0749 5920430.9092 582989.0718 5920430.5328 
582989.1472 5920430.7325 582992.292 5920439.063 582996.226 5920436.738 
583005.7632 5920449.7372 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582936.7593 5920447.2755 582906.4232 5920371.848 582903.7404 5920372.364 
582912.6142 5920395.4772 582933.9733 5920448.3074 582936.7593 5920447.2755 
</gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_557C4D56-1C67-4C5A-A13A-72028A58A668">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582880.9084 5920472.0592</gml:lowerCorner>
          <gml:upperCorner>582892.8671 5920496.2821</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_CFEF65DC-57B4-467F-94D2-ED01D18CBF72" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_A25C8DC0-509E-459E-9A63-733310AD5C85" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582892.8671 5920495.3102 582890.8803 5920495.9558 582889.6567 5920496.2821 
582880.9084 5920473.1646 582883.8435 5920472.0592 582892.8671 5920495.3102 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>9999</xplan:klassifizMassnahme>
          <xplan:massnahmeText>Erhaltung von Gräben</xplan:massnahmeText>
          <xplan:massnahmeKuerzel>G</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_012690D9-CCF1-4A04-8E62-3441FF449021">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582818.1451 5920290.5698</gml:lowerCorner>
          <gml:upperCorner>582917.935 5920472.0592</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7DBB2FD9-7FDA-4678-ADFF-F63582CF0CA1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582871.3519 5920390.386 582900.537 5920465.636 582892.188 5920468.848 
582887.121 5920470.798 582883.8435 5920472.0592 582818.1451 5920305.7072 
582834.9571 5920298.0721 582851.4766 5920290.5698 582887.4822 5920383.5349 
582917.935 5920458.8471 582901.8052 5920465.1411 582872.378 5920390.1295 
582871.3519 5920390.386 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_382F40C2-E909-464F-8B22-B2A2E327A4D8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582819.2777 5920291.2253</gml:lowerCorner>
          <gml:upperCorner>582916.1143 5920471.5295</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F9CDDECD-81CE-4EF4-BC3E-CB535F6A8B7E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582916.1143 5920459.5576 582910.3932 5920461.79 582901.8052 5920465.1411 
582900.537 5920465.636 582892.188 5920468.848 582887.121 5920470.798 
582885.2201 5920471.5295 582819.2777 5920305.1928 582834.9571 5920298.0721 
582850.0331 5920291.2253 582916.1143 5920459.5576 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">5928</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F03831A9-AB2C-49B2-BF3C-D75FCD14B3AD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582819.2777 5920291.2253</gml:lowerCorner>
          <gml:upperCorner>582916.1143 5920471.5295</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_C3185DE5-B99B-4D67-8A3E-FE0D53ABFDFB" srsName="EPSG:25832">
          <gml:posList>582885.2201 5920471.5295 582887.121 5920470.798 582892.188 5920468.848 
582900.537 5920465.636 582901.8052 5920465.1411 582910.3932 5920461.79 
582916.1143 5920459.5576 582850.0331 5920291.2253 582834.9571 5920298.0721 
582819.2777 5920305.1928 582885.2201 5920471.5295 </gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_4E7B6147-60C0-400C-82DD-C53FF562D154">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582854.1824 5920519.0669</gml:lowerCorner>
          <gml:upperCorner>582869.5472 5920550.7697</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_CFEF65DC-57B4-467F-94D2-ED01D18CBF72" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E6ACCDF4-F5A3-42ED-B215-A5651F4CF84C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582869.5472 5920549.6429 582866.884 5920550.7697 582854.1824 5920520.1425 
582857.1017 5920519.0669 582869.5472 5920549.6429 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>9999</xplan:klassifizMassnahme>
          <xplan:massnahmeText>Erhaltung von Gräben</xplan:massnahmeText>
          <xplan:massnahmeKuerzel>G</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_6BEFDF10-91DE-4A51-A32C-0C97BEAB119A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582733.849 5920306.6285</gml:lowerCorner>
          <gml:upperCorner>582924.722 5920612.1713</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C380A41B-C2F0-4179-9864-FF264C6FCC6F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582915.7419 5920558.5659 582921.3515 5920570.9206 582921.4437 5920571.1237 
582924.722 5920578.344 582924.228 5920579.213 582923.24 5920580.951 
582921.8773 5920583.347 582921.854 5920583.388 582918.753 5920588.532 
582915.555 5920593.625 582912.31 5920598.567 582908.912 5920603.535 
582905.43 5920608.414 582904.5971 5920609.5782 582902.7418 5920612.1713 
582869.7687 5920552.8006 582865.985 5920554.9273 582852.1468 5920562.7054 
582862.966 5920581.9539 582833.8041 5920594.0323 582833.67 5920593.696 
582831.932 5920589.24 582833.1012 5920588.8392 582786.255 5920473.2561 
582785.3794 5920470.6292 582769.1614 5920428.6442 582733.849 5920342.8271 
582752.662 5920335.4463 582779.3461 5920323.3278 582790.8759 5920354.6499 
582821.6881 5920429.6263 582854.6573 5920515.1817 582855.1293 5920519.7936 
582854.1824 5920520.1425 582866.884 5920550.7697 582869.5472 5920549.6429 
582857.1017 5920519.0669 582854.9654 5920512.3059 582837.0943 5920461.7738 
582791.3894 5920348.1793 582781.9157 5920322.1608 582782.4258 5920321.9291 
582816.1167 5920306.6285 582831.5763 5920346.676 582857.0273 5920410.0588 
582889.6567 5920496.2821 582890.8803 5920495.9558 582904.7604 5920532.0417 
582904.8285 5920532.2261 582909.399 5920544.596 582915.7419 5920558.5659 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_DE592534-B185-470B-8761-CFAD832887E6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582789.751 5920462.8114</gml:lowerCorner>
          <gml:upperCorner>582866.6313 5920592.9778</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7350DD27-8EC5-4B3C-9FD9-E3165991E9D5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582866.6313 5920554.564 582865.985 5920554.9273 582853.2036 5920562.1114 
582852.1468 5920562.7054 582860.826 5920578.1466 582862.966 5920581.9539 
582857.598 5920584.1772 582856.8168 5920584.5008 582836.3501 5920592.9778 
582789.751 5920477.7587 582827.9498 5920462.8114 582866.6313 5920554.564 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">4823</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_F186D997-6601-4ECE-94C9-0BB186F3FC0E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582789.751 5920462.8114</gml:lowerCorner>
          <gml:upperCorner>582866.6313 5920592.9778</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_4AAE39F4-B655-4F0A-99B4-0A55F837FF9B" srsName="EPSG:25832">
          <gml:posList>582836.3501 5920592.9778 582856.8168 5920584.5008 582857.598 5920584.1772 
582862.966 5920581.9539 582860.826 5920578.1466 582852.1468 5920562.7054 
582853.2036 5920562.1114 582865.985 5920554.9273 582866.6313 5920554.564 
582827.9498 5920462.8114 582789.751 5920477.7587 582836.3501 5920592.9778 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_F79D1EAF-FAF3-4DF1-8B03-EB15CD85FB68">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583055.7533 5920560.2494</gml:lowerCorner>
          <gml:upperCorner>583176.2087 5920694.0271</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_71A901CD-924C-432D-A12E-57F33CD95690" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583176.133 5920564.471 583176.2087 5920564.5272 583173.8833 5920565.8643 
583164.6278 5920575.8506 583156.2247 5920585.1061 583148.4306 5920593.2656 
583140.5146 5920602.1558 583134.0601 5920609.7064 583129.067 5920616.2827 
583121.6382 5920628.461 583112.6262 5920648.4335 583107.5113 5920660.4901 
583101.3004 5920673.8862 583096.5508 5920683.3853 583090.0593 5920694.0271 
583055.7533 5920626.756 583059.6683 5920622.4124 583064.8277 5920617.253 
583069.987 5920612.0937 583075.411 5920607.9926 583079.2475 5920606.4051 
583090.6246 5920605.8759 583102.9277 5920604.9499 583115.6278 5920602.5686 
583121.4486 5920600.0551 583134.2809 5920592.779 583149.1313 5920582.2727 
583165.1208 5920567.7865 583168.4918 5920562.0922 583169.6913 5920560.2494 
583176.133 5920564.471 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_BB9B750A-F34F-414E-AB07-FA0801833485">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582896.4368 5920708.3059</gml:lowerCorner>
          <gml:upperCorner>583016.336 5920856.2711</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D3BE03F0-BE2B-46DA-8677-920EF7B3A6A3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582947.3994 5920856.2711 582944.1144 5920851.8274 582933.7728 5920837.8376 
582916.1776 5920850.8069 582914.0751 5920844.1727 582907.8045 5920824.8846 
582906.9675 5920822.3076 582903.3927 5920811.3008 582901.772 5920806.3108 
582900.0971 5920800.3471 582896.4368 5920787.3137 582899.8808 5920787.5485 
582905.1476 5920784.7592 582913.6478 5920793.07 582928.1882 5920807.2865 
582938.2313 5920817.1058 582939.913 5920818.75 582955.09 5920833.831 
582960.427 5920842.049 582961.058 5920842.848 582961.113 5920842.8921 
582961.6208 5920843.2996 582962.0981 5920842.8921 582964.2849 5920841.0254 
582964.8752 5920840.5215 582964.688 5920839.804 582964.2849 5920839.3101 
582957.607 5920831.128 582945.7838 5920819.9901 582942.063 5920816.485 
582929.8061 5920803.955 582928.825 5920802.952 582927.9273 5920802.0139 
582914.3414 5920787.8164 582911.1411 5920784.472 582909.3077 5920782.5561 
582916.7914 5920777.9807 582925.0242 5920771.3054 582934.1471 5920763.0726 
582945.7175 5920751.9472 582949.9452 5920745.7169 582960.8481 5920735.2591 
582971.0834 5920725.2462 582984.8303 5920708.3059 582998.7581 5920744.4491 
583006.7004 5920767.8156 583016.336 5920791.3231 583008.063 5920801.2113 
582990.3205 5920818.2275 582980.8786 5920828.1882 582966.0413 5920840.8466 
582947.3994 5920856.2711 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_59F45590-42E4-4040-B4E8-456DEEFE2780">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582664.7677 5920785.1009</gml:lowerCorner>
          <gml:upperCorner>582923.3437 5920983.9846</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D66704AE-DB19-4DF9-8220-22E5C627FAC2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582738.3721 5920983.9846 582736.644 5920982.859 582730.8446 5920979.0259 
582728.5115 5920977.4839 582698.1866 5920957.4411 582695.342 5920955.561 
582691.0345 5920952.8102 582686.1507 5920949.6913 582678.3825 5920944.7305 
582674.063 5920941.972 582669.887 5920939.113 582666.928 5920936.854 
582664.7677 5920935.2054 582670.6971 5920926.3937 582678.4849 5920914.3783 
582686.7177 5920901.2503 582691.6129 5920891.9049 582702.2933 5920882.3371 
582712.0837 5920875.8843 582723.2091 5920867.874 582732.9995 5920856.5261 
582741.0098 5920843.8431 582748.5751 5920832.9402 582754.8053 5920825.1524 
582765.4857 5920817.3646 582767.8318 5920816.4162 582774.464 5920831.7796 
582778.462 5920841.041 582780.6408 5920846.1973 582783.8028 5920853.6804 
582784.8789 5920856.2272 582787.1195 5920861.5296 582787.9356 5920863.4609 
582794.014 5920877.846 582804.655 5920902.2421 582804.9798 5920902.9867 
582806.032 5920905.399 582810.059 5920915.619 582810.2133 5920915.9967 
582811.8063 5920919.8966 582814.087 5920925.48 582816.036 5920930.54 
582817.041 5920932.964 582821.542 5920944.233 582822.179 5920944.051 
582823.143 5920943.774 582823.86 5920943.569 582819.873 5920931.813 
582818.868 5920929.308 582817.069 5920924.509 582816.1097 5920922.0952 
582815.6787 5920921.0108 582813.091 5920914.5 582808.815 5920904.196 
582806.2748 5920898.3066 582805.072 5920895.5179 582796.975 5920876.745 
582794.9814 5920872.0837 582794.4051 5920870.7361 582787.9282 5920855.5922 
582785.794 5920850.6021 582781.225 5920839.919 582776.4681 5920828.2268 
582775.5625 5920826.0009 582771.122 5920815.0861 582775.9436 5920813.137 
582786.4015 5920808.4643 582799.0845 5920802.6791 582813.325 5920796.2263 
582825.3405 5920792.4437 582843.3636 5920788.4386 582863.3894 5920785.991 
582875.4048 5920785.1009 582890.0904 5920786.881 582893.2008 5920787.0931 
582895.8982 5920796.5476 582900.3122 5920810.2543 582903.4388 5920819.9633 
582906.7055 5920830.9065 582909.7889 5920841.2358 582915.9007 5920857.349 
582919.9489 5920873.7797 582922.3595 5920873.8414 582923.3437 5920876.0598 
582888.3606 5920900.4558 582884.637 5920895.3523 582873.0437 5920879.4627 
582850.443 5920894.826 582859.1341 5920909.6948 582863.7846 5920917.651 
582851.9519 5920925.8593 582839.8685 5920934.3047 582822.7179 5920946.388 
582805.1775 5920958.8612 582787.3096 5920972.8096 582779.093 5920955.304 
582758.594 5920969.072 582757.758 5920969.688 582738.3721 5920983.9846 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
          <gml:interior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582736.3614 5920975.8314 582737.8002 5920977.4009 582754.6293 5920963.0569 
582753.5393 5920961.4438 582736.3614 5920975.8314 </gml:posList>
            </gml:LinearRing>
          </gml:interior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_8D41EFEC-3AD9-40D5-9F10-D0814823CB0A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582667.033 5920942.54</gml:lowerCorner>
          <gml:upperCorner>582753.255 5921038.5564</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D1129B02-4269-4DFD-80C4-02D3425E706C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582725.5103 5921032.3493 582720.4467 5921038.5564 582719.785 5921038.048 
582716.7372 5921034.6494 582716.648 5921034.55 582708.1855 5921021.3293 
582691.2674 5921032.9564 582689.22 5921025.965 582688.6998 5921023.2857 
582687.567 5921017.451 582684.9641 5921006.733 582683.312 5920999.93 
582678.573 5920980.385 582678.284 5920979.3053 582674.8117 5920966.3311 
582671.39 5920953.546 582667.033 5920943.374 582667.718 5920942.54 
582693.845 5920958.205 582699.5728 5920962.0826 582701.2337 5920963.2069 
582705.8486 5920966.3311 582731.145 5920983.456 582712.4622 5921000.3037 
582713.8116 5921001.653 582733.921 5920985.106 582745.542 5920995.532 
582751.597 5921000.427 582752.055 5920999.937 582752.4684 5920999.4884 
582753.255 5921000.234 582752.285 5921001.283 582752.6692 5921001.7745 
582746.4169 5921006.8793 582735.6983 5921018.6592 582725.5103 5921032.3493 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_93405E7F-A984-4A03-856E-1A10494B6EE6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582512.7786 5920765.026</gml:lowerCorner>
          <gml:upperCorner>582777.8782 5920994.1124</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BC43E7E6-2DAE-4911-BA7A-D3E1F19C3A17" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582599.266 5920925.111 582601.54 5920931.9 582603.663 5920938.187 
582606.8971 5920945.1211 582608.181 5920947.874 582610.5164 5920952.781 
582607.105 5920959.4243 582602.4593 5920967.2089 582596.558 5920973.3613 
582590.6568 5920977.2536 582580.9888 5920979.7648 582576.0957 5920985.3811 
582570.4601 5920986.4924 582566.0944 5920987.1274 582561.1732 5920988.4768 
582555.22 5920990.5405 582549.4256 5920992.2868 582543.4725 5920993.398 
582539.8212 5920994.1124 582535.5666 5920993.7773 582531.217 5920985.964 
582523.038 5920975.41 582512.7786 5920963.8651 582518.507 5920946.9011 
582521.7562 5920937.6533 582529.1294 5920920.0326 582535.3779 5920909.6601 
582547.9998 5920893.0392 582553.3735 5920887.4155 582567.995 5920873.7938 
582596.2446 5920849.9977 582603.9204 5920858.4524 582615.5669 5920871.2808 
582648.144 5920843.767 582655.6066 5920838.3576 582692.568 5920816.947 
582682.6519 5920799.9838 582679.3294 5920794.3001 582710.3483 5920777.7423 
582730.5858 5920766.1889 582734.8099 5920773.6471 582739.9212 5920782.6716 
582740.727 5920782.23 582744.931 5920779.926 582760.027 5920771.388 
582768.555 5920766.766 582771.7655 5920765.026 582772.513 5920766.83 
582773.5969 5920769.1437 582775.602 5920773.424 582777.772 5920779.689 
582777.8782 5920780.0162 582773.7214 5920782.0098 582766.8157 5920783.7676 
582758.6544 5920786.6555 582751.7487 5920789.5433 582749.8653 5920789.5433 
582744.0896 5920792.6823 582740.4484 5920795.1935 582738.8161 5920798.5835 
582733.7937 5920804.2337 582726.0091 5920814.5295 582721.6146 5920818.5474 
582718.6011 5920823.9464 582715.5877 5920828.7176 582711.821 5920834.9956 
582706.2964 5920842.1524 582703.6597 5920845.1658 582693.4894 5920853.2016 
582683.8214 5920860.2329 582677.9201 5920863.7485 582667.1221 5920873.9188 
582666.6198 5920877.3089 582664.3598 5920881.829 582658.7096 5920891.7481 
582652.6828 5920901.2906 582643.517 5920912.5909 582637.3646 5920918.7433 
582631.0867 5920923.6401 582620.2886 5920934.6892 582614.2618 5920945.4873 
582613.0492 5920947.8488 582611.622 5920944.62 582607.221 5920934.615 
582604.459 5920928.805 582601.816 5920923.676 582599.266 5920925.111 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_UeberbaubareGrundstuecksFlaeche gml:id="Gml_3E9223A2-BDBA-4E30-8912-8CE35C7CEE13">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582557.923 5920852.056</gml:lowerCorner>
          <gml:upperCorner>582627.509 5920916.24</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_1BE7BDB4-F6D0-4D36-B56A-B93334FFD8A5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582594.873 5920907.514 582593.377 5920908.221 582584.504 5920916.24 
582557.923 5920886.828 582566.796 5920878.809 582567.982 5920877.9 
582580.406 5920866.612 582584.306 5920863.069 582596.427 5920852.056 
582627.509 5920886.259 582615.824 5920896.877 582617.834 5920899.089 
582612.729 5920903.725 582605.51 5920908.519 582601.134 5920912.283 
582597.65 5920908.232 582595.9629 5920906.5242 582594.873 5920907.514 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:GR uom="m2">10448</xplan:GR>
    </xplan:BP_UeberbaubareGrundstuecksFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_BauGrenze gml:id="Gml_255EA12C-EEB8-40C2-9CE9-126D7CF78C43">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582557.923 5920852.056</gml:lowerCorner>
          <gml:upperCorner>582627.509 5920916.24</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F5A89817-17FE-4E10-AA7E-6C369187C1E9" srsName="EPSG:25832">
          <gml:posList>582596.427 5920852.056 582584.306 5920863.069 582580.406 5920866.612 
582567.982 5920877.9 582566.796 5920878.809 582557.923 5920886.828 
582584.504 5920916.24 582593.377 5920908.221 582594.873 5920907.514 
582595.9629 5920906.5242 582597.65 5920908.232 582601.134 5920912.283 
582605.51 5920908.519 582612.729 5920903.725 582617.834 5920899.089 
582615.824 5920896.877 582627.509 5920886.259 582596.427 5920852.056 
</gml:posList>
        </gml:LineString>
      </xplan:position>
    </xplan:BP_BauGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_066BB715-3449-47C5-A7E1-67A59443399A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582776.1495 5920690.6233</gml:lowerCorner>
          <gml:upperCorner>582913.6421 5920778.4983</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F8364A9D-1C3A-47BB-A3C5-1AD98B05B0BE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582862.3659 5920757.7769 582839.8909 5920760.4137 582826.5817 5920762.2971 
582813.9002 5920765.6871 582802.3488 5920769.705 582794.4386 5920772.0906 
582786.0262 5920776.1085 582781.0432 5920778.4983 582780.987 5920778.36 
582778.487 5920771.841 582778.1653 5920771.1931 582776.1495 5920766.4809 
582816.192 5920745.904 582834.061 5920736.939 582823.9865 5920716.8557 
582821.3492 5920711.5984 582835.4605 5920703.0166 582845.6184 5920696.3314 
582853.1734 5920690.6233 582863.8506 5920704.753 582880.238 5920693.8609 
582883.212 5920698.192 582894.4671 5920714.6681 582908.479 5920735.18 
582910.0854 5920737.5719 582913.6421 5920742.8676 582906.8137 5920749.8667 
582901.038 5920753.3824 582894.6345 5920755.768 582891.1189 5920756.1447 
582882.7064 5920756.6469 582872.9128 5920756.3958 582862.3659 5920757.7769 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_4031B3D4-490A-4F51-8141-6164EA34121A">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582871.8541 5920492.7544</gml:lowerCorner>
          <gml:upperCorner>583077.448 5920741.4051</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_77B32044-F431-419A-B50D-B8957D06C98B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583009.9628 5920508.7369 583055.2689 5920554.612 583069.4221 5920569.3435 
583077.448 5920577.2235 583071.9234 5920578.6046 583063.8877 5920581.8691 
583051.4574 5920589.4027 583045.4306 5920594.9273 583040.4082 5920601.4563 
583035.8881 5920606.7298 583014.6687 5920630.2093 582985.9157 5920662.4779 
582970.3464 5920679.4283 582965.1985 5920685.7062 582962.0595 5920690.603 
582955.1538 5920700.7733 582946.7414 5920710.0646 582940.589 5920714.5847 
582936.4456 5920719.9838 582932.6788 5920722.6205 582929.4143 5920727.2662 
582916.8584 5920739.5709 582915.0689 5920741.4051 582909.96 5920733.898 
582909.3249 5920732.9681 582908.9157 5920732.3691 582907.4898 5920730.2815 
582884.752 5920696.991 582882.511 5920693.703 582877.2309 5920685.978 
582871.8541 5920678.1117 582877.5681 5920672.2823 582875.681 5920669.4842 
582880.6068 5920663.4267 582887.4317 5920654.6897 582897.4795 5920639.8799 
582903.1463 5920631.0703 582913.5274 5920616.8319 582921.0514 5920606.4508 
582928.6229 5920595.022 582932.1944 5920589.1171 582935.9564 5920582.2122 
582940.8613 5920573.4978 582951.1471 5920555.1165 582959.1473 5920539.8782 
582960.9092 5920537.0686 582961.1949 5920535.4971 582966.0522 5920526.7351 
582969.2427 5920522.4017 582978.1 5920511.5919 582994.1785 5920492.7544 
583009.9628 5920508.7369 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_Landwirtschaft gml:id="Gml_3F0F099C-942B-4639-8A03-EE6E1A3A6DCC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583064.4129 5920444.1283</gml:lowerCorner>
          <gml:upperCorner>583148.7361 5920560.9905</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_F3CC57F5-6233-4793-A62F-FF968DBAE3DE" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_14132009-D405-43E2-953B-42FCC42FA3A3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583083.591 5920453.115 583087.2242 5920458.2136 583088.974 5920460.669 
583102.229 5920477.33 583103.933 5920479.6572 583106.1596 5920482.6982 
583108.17 5920485.444 583110.2908 5920488.7202 583119.9981 5920503.7157 
583129.1727 5920517.8884 583135.387 5920527.488 583135.9282 5920528.1008 
583138.3564 5920530.8502 583145.0323 5920538.409 583145.669 5920539.13 
583148.7361 5920543.7074 583136.6436 5920555.689 583134.2624 5920557.9115 
583129.5881 5920560.9905 583129.4264 5920560.818 583123.275 5920554.256 
583114.057 5920545.118 583105.1058 5920535.4046 583104.192 5920534.413 
583101.7233 5920531.7341 583105.441 5920528.2891 583079.2001 5920499.9715 
583089.327 5920490.0147 583087.6682 5920487.8213 583064.4129 5920457.0711 
583078.0339 5920444.1283 583083.591 5920453.115 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1000</xplan:zweckbestimmung>
    </xplan:BP_Landwirtschaft>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_14F6C3FC-0A0D-44CA-A353-2E6165107571">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582576.0957 5920952.781</gml:lowerCorner>
          <gml:upperCorner>582618.237 5920987.5822</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0E6B1C84-DFDB-4692-875C-FC8B31A90E9C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582595.193 5920986.533 582592.135 5920987.5341 582591.9883 5920987.5822 
582590.2245 5920985.5399 582586.097 5920984.7461 582576.0957 5920985.3811 
582580.9888 5920979.7648 582590.6568 5920977.2536 582596.558 5920973.3613 
582602.4593 5920967.2089 582607.105 5920959.4243 582610.5164 5920952.781 
582612.779 5920957.535 582617.058 5920962.574 582618.237 5920963.136 
582617.56 5920964.424 582617.168 5920965.2198 582615.5 5920968.606 
582614.466 5920968.241 582612.961 5920970.395 582611.34 5920973.684 
582609.003 5920976.565 582603.519 5920981.001 582599.91 5920983.91 
582595.193 5920986.533 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_6DB00543-02B4-4E85-B5EB-494C015C8FA1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582613.0492 5920780.0162</gml:lowerCorner>
          <gml:upperCorner>582780.656 5920956.084</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_276458C4-1F05-438A-A653-9C7CD764D66A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582751.143 5920800.459 582748.831 5920802.381 582748.3522 5920803.2151 
582747.962 5920803.895 582740.967 5920812.681 582737.0028 5920817.7921 
582732.986 5920822.971 582730.292 5920825.689 582725.775 5920832.413 
582724.8277 5920834.0921 582721.832 5920839.402 582721.2031 5920840.281 
582718.723 5920843.747 582716.022 5920847.653 582712.143 5920852.235 
582708.9956 5920854.78 582703.12 5920859.531 582688.557 5920870.404 
582684.666 5920872.511 582680.685 5920876.178 582677.6211 5920879.574 
582677.069 5920880.186 582676.427 5920882.186 582673.177 5920888.727 
582671.6638 5920891.651 582671.316 5920892.323 582664.341 5920903.866 
582660.69 5920908.935 582658.0303 5920912.2766 582654.484 5920916.732 
582651.826 5920920.457 582651.3535 5920920.8958 582648.158 5920923.863 
582644.705 5920927.475 582639.006 5920931.705 582629.671 5920940.752 
582626.508 5920944.64 582623.779 5920948.944 582621.992 5920951.105 
582620.292 5920954.042 582618.791 5920955.994 582617.704 5920956.084 
582616.618 5920955.477 582615.864 5920954.217 582613.0492 5920947.8488 
582614.2618 5920945.4873 582620.2886 5920934.6892 582631.0867 5920923.6401 
582637.3646 5920918.7433 582643.517 5920912.5909 582652.6828 5920901.2906 
582658.7096 5920891.7481 582664.3598 5920881.829 582666.6198 5920877.3089 
582667.1221 5920873.9188 582677.9201 5920863.7485 582683.8214 5920860.2329 
582693.4894 5920853.2016 582703.6597 5920845.1658 582706.2964 5920842.1524 
582711.821 5920834.9956 582715.5877 5920828.7176 582718.6011 5920823.9464 
582721.6146 5920818.5474 582726.0091 5920814.5295 582733.7937 5920804.2337 
582738.8161 5920798.5835 582740.4484 5920795.1935 582744.0896 5920792.6823 
582749.8653 5920789.5433 582751.7487 5920789.5433 582758.6544 5920786.6555 
582766.8157 5920783.7676 582773.7214 5920782.0098 582777.8782 5920780.0162 
582780.656 5920788.58 582779.703 5920789.976 582772.506 5920793.244 
582759.698 5920797.359 582756.777 5920798.869 582752.303 5920800.43 
582751.309 5920800.321 582751.143 5920800.459 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_52930595-9C20-48FF-AFA9-8E20CE0050AA">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582657.792 5920807.056</gml:lowerCorner>
          <gml:upperCorner>582767.8318 5920935.2054</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C49E539B-A828-4137-B19B-A7ABB8696AFC" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582765.321 5920810.6 582767.8318 5920816.4162 582765.4857 5920817.3646 
582754.8053 5920825.1524 582748.5751 5920832.9402 582741.0098 5920843.8431 
582732.9995 5920856.5261 582723.2091 5920867.874 582712.0837 5920875.8843 
582702.2933 5920882.3371 582691.6129 5920891.9049 582686.7177 5920901.2503 
582678.4849 5920914.3783 582670.6971 5920926.3937 582664.7677 5920935.2054 
582660.781 5920932.163 582657.792 5920929.486 582661.3446 5920924.3416 
582663.4978 5920920.9973 582668.8059 5920912.7529 582673.955 5920904.039 
582674.756 5920902.974 582675.581 5920901.924 582676.438 5920900.908 
582677.277 5920899.893 582678.065 5920898.846 582678.789 5920897.762 
582679.438 5920896.643 582680.001 5920895.49 582680.463 5920894.297 
582680.807 5920893.06 582681.019 5920891.772 582682.4584 5920889.8928 
582688.3179 5920882.8614 582688.4115 5920882.749 582690.656 5920880.185 
582694.2685 5920877.1028 582697.6984 5920874.1765 582714.923 5920862.448 
582715.756 5920861.752 582716.582 5920861.05 582717.44 5920860.339 
582718.353 5920859.617 582725.0035 5920851.3165 582727.7727 5920846.8947 
582729.9247 5920843.4583 582737.8079 5920831.7796 582740.6404 5920827.5833 
582747.2285 5920819.6458 582754.0548 5920812.8195 582757.932 5920809.533 
582758.78 5920809.164 582759.669 5920808.739 582760.571 5920808.28 
582761.442 5920807.872 582762.368 5920807.482 582763.344 5920807.056 
582765.321 5920810.6 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_E93A4B23-FC6D-4253-A6CB-DB357CF7DD4C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582766.982 5920775.58</gml:lowerCorner>
          <gml:upperCorner>582893.2008 5920815.0861</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_14AC2565-2C62-4366-A69A-B03ED0C8C02E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582863.3894 5920785.991 582843.3636 5920788.4386 582825.3405 5920792.4437 
582813.325 5920796.2263 582799.0845 5920802.6791 582786.4015 5920808.4643 
582775.9436 5920813.137 582771.122 5920815.0861 582769.8726 5920812.0152 
582768.687 5920809.101 582766.982 5920805.15 582790.434 5920796.09 
582797.1762 5920792.8778 582798.026 5920792.473 582805.174 5920789.318 
582811.5112 5920786.7845 582819.064 5920783.765 582828.847 5920781.474 
582838.667 5920779.48 582848.58 5920778.179 582854.12 5920777.2829 
582858.03 5920776.6504 582858.453 5920776.582 582868.403 5920775.58 
582871.0191 5920775.6028 582878.498 5920775.668 582890.222 5920776.652 
582893.2008 5920787.0931 582890.0904 5920786.881 582875.4048 5920785.1009 
582863.3894 5920785.991 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_09665564-EE0D-4295-A41A-750566E8A343">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582781.0432 5920742.8676</gml:lowerCorner>
          <gml:upperCorner>582920.065 5920789.128</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6EBCBB1C-83CD-4B42-BADB-4B94FE77D9D1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582866.892 5920767.7276 582854.024 5920769.187 582853.1944 5920769.3057 
582851.7656 5920769.5101 582829.623 5920772.678 582823.997 5920774.001 
582815.976 5920776.016 582802.928 5920780.716 582797.203 5920782.573 
582794.335 5920783.904 582794.459 5920784.996 582790.104 5920787.401 
582785.36 5920789.128 582781.0432 5920778.4983 582786.0262 5920776.1085 
582794.4386 5920772.0906 582802.3488 5920769.705 582813.9002 5920765.6871 
582826.5817 5920762.2971 582839.8909 5920760.4137 582862.3659 5920757.7769 
582872.9128 5920756.3958 582882.7064 5920756.6469 582891.1189 5920756.1447 
582894.6345 5920755.768 582901.038 5920753.3824 582906.8137 5920749.8667 
582913.6421 5920742.8676 582920.065 5920752.431 582918.48 5920753.995 
582916.1206 5920755.8201 582912.58 5920758.559 582907.17 5920762.177 
582901.949 5920764.395 582897.463 5920765.926 582893.623 5920766.212 
582886.376 5920766.928 582877.833 5920767.23 582873.096 5920767.024 
582866.892 5920767.7276 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_E551AABE-91D8-4A2A-90FE-3FE28EC9AC04">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582893.652 5920777.398</gml:lowerCorner>
          <gml:upperCorner>582905.1476 5920787.5485</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0454C645-04BD-475E-B75F-A2FB4DD9E750" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582897.636 5920777.415 582905.1476 5920784.7592 582899.8808 5920787.5485 
582896.4368 5920787.3137 582893.652 5920777.398 582897.636 5920777.415 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_A14C6FB8-5A7B-4FFD-87A4-76DFF3F4D5A6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582984.8303 5920699.507</gml:lowerCorner>
          <gml:upperCorner>582992.4826 5920708.3059</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_92612553-FFDC-40D4-BFC2-07434DC9D41D" srsName="EPSG:25832">
          <gml:posList>582984.8303 5920708.3059 582992.4826 5920699.507 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_05859DE9-E1E4-4D68-AC23-D9B29A4FBE06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582996.0283 5920637.3349</gml:lowerCorner>
          <gml:upperCorner>583046.5529 5920695.43</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_F1AC224D-48EB-472A-8042-B60573F6731A" srsName="EPSG:25832">
          <gml:posList>582996.0283 5920695.43 583046.5529 5920637.3349 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_80E942B9-AFEE-4023-8D39-D44FBF3DCEE8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583049.0379 5920626.756</gml:lowerCorner>
          <gml:upperCorner>583055.7533 5920634.4776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_4E1E3FC8-A30A-41F8-A549-27A25A87399F" srsName="EPSG:25832">
          <gml:posList>583055.7533 5920626.756 583049.0379 5920634.4776 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_F47E854B-A551-4ADE-8FBC-1849347393C1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582902.983 5920690.569</gml:lowerCorner>
          <gml:upperCorner>582992.4826 5920782.5561</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_53D13ED3-FAF6-4EF7-BC71-C33C958D103E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582988.561 5920690.782 582989.212 5920692.062 582990.624 5920695.082 
582992.4826 5920699.507 582984.8303 5920708.3059 582971.0834 5920725.2462 
582960.8481 5920735.2591 582949.9452 5920745.7169 582945.7175 5920751.9472 
582934.1471 5920763.0726 582925.0242 5920771.3054 582916.7914 5920777.9807 
582909.3077 5920782.5561 582906.769 5920779.903 582902.983 5920774.619 
582909.859 5920770.436 582913.254 5920768.1717 582916.66 5920765.9 
582925.752 5920758.014 582929.9416 5920753.7644 582939.787 5920743.778 
582943.073 5920739.123 582949.0117 5920733.5255 582953.535 5920729.262 
582956.5931 5920726.0331 582965.9722 5920716.13 582967.283 5920714.746 
582974.0893 5920706.2187 582979.816 5920699.044 582987.597 5920690.569 
582988.561 5920690.782 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_6805C200-09A0-4AC8-B2AA-216518443D19">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582984.8303 5920626.756</gml:lowerCorner>
          <gml:upperCorner>583092.274 5920797.9164</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_096CCB49-A671-40E1-A5BE-BBE205AC911C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_487AD36F-D7A1-4E92-8295-5D2A4AAE2711" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583029.1111 5920787.6916 583027.68 5920789.329 583022.1673 5920794.8072 
583019.0386 5920797.9164 583016.336 5920791.3231 583006.7004 5920767.8156 
582998.7581 5920744.4491 582984.8303 5920708.3059 582992.4826 5920699.507 
582994.0065 5920703.1353 582994.097 5920703.3506 582994.153 5920703.484 
582998.9203 5920716.885 582999.016 5920717.154 583003.1962 5920727.5025 
583003.5721 5920728.4332 583007.967 5920739.313 583009.4792 5920743.7526 
583009.9424 5920745.1125 583016.187 5920763.446 583019.6887 5920772.0389 
583019.895 5920772.545 583023.237 5920780.622 583024.042 5920780.338 
583024.985 5920780.005 583025.587 5920779.792 583019.762 5920762.279 
583019.297 5920759.325 583014.429 5920747.769 583009.8484 5920734.674 
583006.5929 5920725.3671 583002.984 5920715.05 583002.137 5920712.5687 
583000.6973 5920708.3512 582998.431 5920701.712 582996.0283 5920695.43 
583046.5529 5920637.3349 583054.841 5920653.237 583054.8634 5920653.2791 
583065.7389 5920673.6972 583073.092 5920687.502 583072.568 5920688.19 
583067.932 5920694.277 583067.9181 5920694.3124 583065.666 5920700.049 
583065.2848 5920701.3768 583071.419 5920705.185 583075.591 5920708.187 
583078.7786 5920710.505 583081.5567 5920708.3618 583077.886 5920705.65 
583070.306 5920700.418 583068.8567 5920699.3131 583069.2535 5920694.868 
583075.278 5920687.506 583074.38 5920684.67 583068.5449 5920673.0479 
583065.7061 5920667.3937 583058.9278 5920653.8932 583057.788 5920651.623 
583053.5844 5920643.5123 583053.068 5920642.516 583049.7601 5920635.918 
583049.0379 5920634.4776 583055.7533 5920626.756 583090.0593 5920694.0271 
583092.274 5920698.3698 583083.676 5920713.257 583079.5 5920720.489 
583069.84 5920736.284 583059.49 5920751.404 583050.2945 5920763.0175 
583046.621 5920767.657 583029.1111 5920787.6916 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>9999</xplan:klassifizMassnahme>
          <xplan:massnahmeText>Extensives Feuchtgrünland mit Gräben</xplan:massnahmeText>
          <xplan:massnahmeKuerzel>FG</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_08FF3A4C-7820-4E18-9CC6-2B5135A41DA4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582992.912 5920628.309</gml:lowerCorner>
          <gml:upperCorner>583046.5529 5920695.43</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_93E4A02F-B68B-4E96-A0A1-43749E8C17AD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583042.337 5920629.246 583046.5529 5920637.3349 582996.0283 5920695.43 
582994.083 5920690.344 582993.1813 5920687.4816 582992.965 5920686.795 
582992.912 5920683.193 583001.0089 5920672.815 583001.264 5920672.488 
583002.6363 5920670.7274 583004.304 5920668.588 583005.5258 5920667.0867 
583005.607 5920666.987 583009.6428 5920663.1974 583016.6278 5920656.0536 
583019.8822 5920652.5611 583026.391 5920646.7667 583029.7247 5920644.3061 
583030.4391 5920641.9248 583031.9472 5920639.3848 583035.0429 5920633.9873 
583039.929 5920628.309 583042.337 5920629.246 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_6B759DDD-9524-4EC5-92D4-35F499410162">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583129.5881 5920560.9905</gml:lowerCorner>
          <gml:upperCorner>583136.279 5920568.296</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_8AEDCD5E-A3EF-419B-8A90-7F47680A7DB0" srsName="EPSG:25832">
          <gml:posList>583136.279 5920568.296 583131.484 5920563.013 583129.5881 5920560.9905 
</gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_EE93B9FC-6FC3-46AD-8809-5752AEA736F8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583077.448 5920577.2235</gml:lowerCorner>
          <gml:upperCorner>583085.2843 5920585.8603</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_7A361E4B-2981-463E-B154-3CBC3EF65191" srsName="EPSG:25832">
          <gml:posList>583077.448 5920577.2235 583085.2843 5920585.8603 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_NutzungsartenGrenze gml:id="Gml_6E6644F5-C068-4D67-A139-E9AD7E235821">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583077.448 5920560.9905</gml:lowerCorner>
          <gml:upperCorner>583129.5881 5920577.2235</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:LineString gml:id="Gml_642454C9-B096-4D94-8FC2-F2D6D3F1910E" srsName="EPSG:25832">
          <gml:posList>583129.5881 5920560.9905 583120.8414 5920569.1575 583111.7056 5920573.9554 
583107.039 5920575.1385 583101.1237 5920575.7958 583081.7347 5920576.1901 
583077.448 5920577.2235 </gml:posList>
        </gml:LineString>
      </xplan:position>
      <xplan:typ>1000</xplan:typ>
    </xplan:BP_NutzungsartenGrenze>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_2D70706D-65E8-4C51-BFAA-201B36309775">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582915.0689 5920577.2235</gml:lowerCorner>
          <gml:upperCorner>583085.2843 5920750.9302</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B1AEA6AF-AF17-4C75-912F-B3655645C42D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583042.5041 5920610.3335 583042.9058 5920612.6554 583042.6208 5920615.0894 
583041.819 5920616.221 583036.537 5920621.958 583027.247 5920632.6763 
583025.077 5920635.18 583023.2105 5920637.26 583019.2178 5920641.7092 
583019.1668 5920641.7661 583015.729 5920645.597 583003.9032 5920658.0433 
583001.263 5920660.822 582993.9054 5920669.0214 582991.115 5920672.131 
582988.516 5920675.628 582982.889 5920681.002 582979.429 5920682.464 
582975.548 5920686.639 582974.839 5920689.226 582973.3269 5920692.2487 
582971.964 5920694.973 582961.955 5920708.824 582960.8863 5920709.9975 
582955.6611 5920715.7351 582952.8 5920718.877 582950.1859 5920720.6445 
582948.02 5920722.109 582944.171 5920726.85 582940.104 5920730.183 
582937.9843 5920732.9681 582937.097 5920734.134 582931.331 5920740.131 
582927.569 5920744.756 582921.5512 5920750.9302 582915.0689 5920741.4051 
582916.8584 5920739.5709 582929.4143 5920727.2662 582932.6788 5920722.6205 
582936.4456 5920719.9838 582940.589 5920714.5847 582946.7414 5920710.0646 
582955.1538 5920700.7733 582962.0595 5920690.603 582965.1985 5920685.7062 
582970.3464 5920679.4283 582985.9157 5920662.4779 583014.6687 5920630.2093 
583035.8881 5920606.7298 583040.4082 5920601.4563 583045.4306 5920594.9273 
583051.4574 5920589.4027 583063.8877 5920581.8691 583071.9234 5920578.6046 
583077.448 5920577.2235 583085.2843 5920585.8603 583083.69 5920585.933 
583080.9217 5920586.6059 583078.346 5920587.232 583073.131 5920588.195 
583071.2379 5920588.1085 583068.7773 5920587.156 583067.5867 5920588.426 
583068.5392 5920588.9816 583068.961 5920590.609 583063.654 5920594.578 
583057.864 5920597.441 583052.594 5920602.116 583047.87 5920607.681 
583047.3313 5920608.4412 583046.1429 5920608.7882 583045.2507 5920609.8583 
583043.3772 5920609.0635 583042.5041 5920610.3335 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_26CDCA61-A935-4833-85C0-8B2A51DA4752">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583044.373 5920560.1684</gml:lowerCorner>
          <gml:upperCorner>583168.4918 5920634.4776</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E4380384-22AB-465A-B3F3-86E2D3284B55" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583166.9644 5920561.6765 583168.4918 5920562.0922 583165.1208 5920567.7865 
583149.1313 5920582.2727 583134.2809 5920592.779 583121.4486 5920600.0551 
583115.6278 5920602.5686 583102.9277 5920604.9499 583090.6246 5920605.8759 
583079.2475 5920606.4051 583075.411 5920607.9926 583069.987 5920612.0937 
583064.8277 5920617.253 583059.6683 5920622.4124 583055.7533 5920626.756 
583049.0379 5920634.4776 583046.8766 5920630.1667 583044.373 5920625.173 
583046.711 5920622.2398 583052.9816 5920615.096 583063.4592 5920604.5391 
583066.2039 5920602.3061 583068.1423 5920600.7291 583077.0323 5920596.2047 
583081.7948 5920596.1254 583090.4467 5920595.7285 583102.1149 5920594.9347 
583109.5414 5920593.4612 583112.1161 5920592.9503 583116.039 5920590.82 
583117.223 5920590.466 583118.389 5920590.076 583119.536 5920589.641 
583120.658 5920589.159 583121.756 5920588.625 583122.666 5920588.13 
583123.556 5920587.6 583124.433 5920587.039 583125.297 5920586.454 
583126.15 5920585.848 583126.997 5920585.227 583127.839 5920584.596 
583128.68 5920583.959 583129.522 5920583.322 583130.368 5920582.691 
583131.22 5920582.069 583132.08 5920581.461 583132.666 5920581.063 
583133.257 5920580.671 583133.85 5920580.284 583134.446 5920579.901 
583135.041 5920579.52 583135.635 5920579.139 583136.229 5920578.757 
583136.819 5920578.371 583137.1676 5920578.1374 583137.404 5920577.979 
583137.985 5920577.581 583138.558 5920577.173 583139.123 5920576.755 
583139.616 5920576.375 583140.102 5920575.985 583140.58 5920575.584 
583141.05 5920575.173 583144.7393 5920572.7891 583150.4436 5920567.8572 
583152.3593 5920566.2009 583156.5662 5920562.1528 583161.2494 5920560.1684 
583166.9644 5920561.6765 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_2D6AA0A5-1CC1-4A15-AD27-6A48E767980E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583129.5881 5920543.7074</gml:lowerCorner>
          <gml:upperCorner>583152.934 5920568.296</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_74ADD42C-E417-49A3-B43C-F8ABB7C17429" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583149.972 5920545.552 583152.934 5920547.986 583152.8356 5920550.0878 
583152.3317 5920552.9219 583151.439 5920554.309 583148.558 5920558.395 
583141.699 5920564.618 583136.279 5920568.296 583131.484 5920563.013 
583129.5881 5920560.9905 583134.2624 5920557.9115 583136.6436 5920555.689 
583148.7361 5920543.7074 583149.972 5920545.552 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_47B4F2D5-CDC5-4A3D-B9E4-9EBADEE00168">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583077.448 5920560.9905</gml:lowerCorner>
          <gml:upperCorner>583136.279 5920585.8603</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung2>(Z)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_15465347-17DC-4025-93FB-F9C758C2A8BA" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9C0425B9-ED78-4CFA-9990-D77255C70D45" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583085.2843 5920585.8603 583077.448 5920577.2235 583081.7347 5920576.1901 
583101.1237 5920575.7958 583107.039 5920575.1385 583111.7056 5920573.9554 
583120.8414 5920569.1575 583129.5881 5920560.9905 583131.484 5920563.013 
583136.279 5920568.296 583134.289 5920570.23 583133.5761 5920570.9005 
583131.903 5920572.474 583126.305 5920577.21 583123.33 5920578.853 
583120.6736 5920580.3191 583116.001 5920582.898 583109.0205 5920584.7483 
583108.554 5920584.872 583107.8456 5920584.9317 583098.296 5920585.736 
583092.311 5920585.54 583085.2843 5920585.8603 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>1500</xplan:klassifizMassnahme>
          <xplan:massnahmeKuerzel>U</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_SchutzPflegeEntwicklungsFlaeche gml:id="Gml_4BE4D312-EB9A-40AE-8257-F99F93734DD1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583069.4221 5920535.4046</gml:lowerCorner>
          <gml:upperCorner>583129.5881 5920577.2235</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:gliederung2>(Z)</xplan:gliederung2>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_096CCB49-A671-40E1-A5BE-BBE205AC911C" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C484C03C-D026-445E-809A-3E4A4F8E47E4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583114.057 5920545.118 583123.275 5920554.256 583129.4264 5920560.818 
583129.5881 5920560.9905 583120.8414 5920569.1575 583111.7056 5920573.9554 
583107.039 5920575.1385 583101.1237 5920575.7958 583081.7347 5920576.1901 
583077.448 5920577.2235 583069.4221 5920569.3435 583092.2392 5920547.642 
583105.1058 5920535.4046 583114.057 5920545.118 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:ziel>4000</xplan:ziel>
      <xplan:massnahme>
        <xplan:XP_SPEMassnahmenDaten>
          <xplan:klassifizMassnahme>9999</xplan:klassifizMassnahme>
          <xplan:massnahmeText>Extensives Feuchtgrünland mit Gräben</xplan:massnahmeText>
          <xplan:massnahmeKuerzel>FG</xplan:massnahmeKuerzel>
        </xplan:XP_SPEMassnahmenDaten>
      </xplan:massnahme>
    </xplan:BP_SchutzPflegeEntwicklungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_DenkmalschutzEnsembleFlaeche gml:id="Gml_F14E004B-1EF0-40F5-9A20-E80550445503">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582648.158 5920548.019</gml:lowerCorner>
          <gml:upperCorner>583264.515 5921341.6845</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:refTextInhalt xlink:href="#Gml_FAE8F077-0048-4FD9-80A1-1A9EB61D20FD" />
      <xplan:rechtscharakter>1000</xplan:rechtscharakter>
      <xplan:gehoertZuBP_Bereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_756C5BAC-1098-4EDB-AE70-079F26CD2709" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583188.3148 5921242.5417 583114.5988 5921258.1576 583125.4902 5921316.2752 
583017.384 5921341.6845 583009.0215 5921299.1567 582997.3167 5921239.6312 
582996.5445 5921235.7041 582947.7584 5921249.2846 582947.8716 5921249.9122 
582955.156 5921295.6215 582895.3905 5921306.0942 582889.5217 5921272.3669 
582809.1219 5921267.3646 582801.6583 5921230.1273 582798.5082 5921214.4108 
582795.5531 5921198.6207 582787.9381 5921157.9321 582787.8719 5921157.5782 
582781.146 5921119.2456 582778.1088 5921101.9362 582774.14 5921079.7905 
582773.2972 5921074.6053 582766.1231 5921019.2259 582762.7575 5921009.0741 
582761.186 5921004.895 582754.522 5921004.145 582752.6692 5921001.7745 
582752.285 5921001.283 582753.255 5921000.234 582752.4684 5920999.4884 
582737.101 5920984.922 582733.8921 5920983.4876 582732.4304 5920984.3262 
582731.145 5920983.456 582705.8486 5920966.3311 582701.2337 5920963.2069 
582699.5728 5920962.0826 582693.845 5920958.205 582667.718 5920942.54 
582666.036 5920941.464 582665.182 5920938.87 582656.621 5920931.167 
582648.158 5920923.863 582651.3535 5920920.8958 582651.826 5920920.457 
582654.484 5920916.732 582658.0303 5920912.2766 582660.69 5920908.935 
582664.341 5920903.866 582671.316 5920892.323 582671.6638 5920891.651 
582673.177 5920888.727 582676.427 5920882.186 582677.069 5920880.186 
582677.6211 5920879.574 582680.685 5920876.178 582684.666 5920872.511 
582688.557 5920870.404 582703.12 5920859.531 582708.9956 5920854.78 
582712.143 5920852.235 582716.022 5920847.653 582718.723 5920843.747 
582721.2031 5920840.281 582721.6582 5920839.645 582721.832 5920839.402 
582722.3008 5920838.5711 582724.8277 5920834.0921 582725.775 5920832.413 
582730.292 5920825.689 582732.986 5920822.971 582737.0028 5920817.7921 
582740.967 5920812.681 582743.7756 5920809.1533 582747.962 5920803.895 
582748.3522 5920803.2151 582748.831 5920802.381 582751.143 5920800.459 
582751.309 5920800.321 582752.303 5920800.43 582756.777 5920798.869 
582759.698 5920797.359 582772.506 5920793.244 582777.2399 5920791.0944 
582779.703 5920789.976 582784.8388 5920787.8447 582785.36 5920789.128 
582790.104 5920787.401 582794.459 5920784.996 582794.335 5920783.904 
582797.203 5920782.573 582802.928 5920780.716 582815.976 5920776.016 
582823.997 5920774.001 582825.3861 5920773.6744 582829.623 5920772.678 
582851.7656 5920769.5101 582853.1944 5920769.3057 582854.024 5920769.187 
582854.5423 5920769.1282 582866.892 5920767.7276 582873.096 5920767.024 
582877.833 5920767.23 582886.376 5920766.928 582893.623 5920766.212 
582897.463 5920765.926 582901.949 5920764.395 582907.17 5920762.177 
582912.58 5920758.559 582916.1206 5920755.8201 582918.48 5920753.995 
582920.065 5920752.431 582921.5512 5920750.9302 582922.3873 5920750.0723 
582927.569 5920744.756 582931.331 5920740.131 582937.097 5920734.134 
582937.9843 5920732.9681 582940.104 5920730.183 582944.171 5920726.85 
582948.02 5920722.109 582950.1859 5920720.6445 582951.9387 5920719.4594 
582952.8 5920718.877 582955.6611 5920715.7351 582960.8863 5920709.9975 
582961.955 5920708.824 582965.5468 5920703.8535 582971.964 5920694.973 
582973.3269 5920692.2487 582974.5234 5920689.8569 582974.839 5920689.226 
582975.548 5920686.639 582979.429 5920682.464 582982.889 5920681.002 
582983.4904 5920680.4276 582988.516 5920675.628 582991.115 5920672.131 
582993.9054 5920669.0214 583001.263 5920660.822 583003.9032 5920658.0433 
583005.632 5920656.2238 583015.729 5920645.597 583019.1668 5920641.7661 
583019.2178 5920641.7092 583023.2105 5920637.26 583025.077 5920635.18 
583027.247 5920632.6763 583029.4157 5920630.1742 583036.537 5920621.958 
583041.819 5920616.221 583042.6208 5920615.0894 583047.3313 5920608.4412 
583047.87 5920607.681 583052.594 5920602.116 583057.864 5920597.441 
583063.654 5920594.578 583068.961 5920590.609 583071.0211 5920589.4164 
583073.131 5920588.195 583078.346 5920587.232 583080.9217 5920586.6059 
583083.69 5920585.933 583085.2843 5920585.8603 583092.311 5920585.54 
583098.296 5920585.736 583106.4897 5920585.0459 583107.8456 5920584.9317 
583108.554 5920584.872 583109.0205 5920584.7483 583116.001 5920582.898 
583120.6736 5920580.3191 583123.33 5920578.853 583126.305 5920577.21 
583131.903 5920572.474 583133.5761 5920570.9005 583134.289 5920570.23 
583136.279 5920568.296 583141.699 5920564.618 583148.558 5920558.395 
583151.439 5920554.309 583152.3317 5920552.9219 583155.487 5920548.019 
583157.6674 5920551.7079 583160.41 5920556.348 583167.829 5920559.029 
583168.464 5920559.4452 583169.6913 5920560.2494 583176.133 5920564.471 
583176.144 5920564.4792 583176.2087 5920564.5272 583179.422 5920566.912 
583182.191 5920571.135 583185.6645 5920593.3855 583186.8955 5920601.2707 
583188.57 5920611.997 583190.278 5920622.931 583193.5349 5920643.7884 
583199.395 5920681.316 583201.7395 5920694.707 583202.118 5920696.869 
583205.2433 5920713.1094 583211.448 5920745.352 583216.0575 5920769.0929 
583221.0872 5920794.9981 583228.046 5920830.8393 583230.2907 5920842.4004 
583230.6273 5920844.1338 583228.9285 5920845.0044 583264.515 5921031.6683 
583152.0668 5921053.8934 583188.3148 5921242.5417 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
    </xplan:BP_DenkmalschutzEnsembleFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_7CDC3589-D0B0-4A24-8636-D4F72F06F86C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582789.9069 5921275.9162</gml:lowerCorner>
          <gml:upperCorner>582821.2066 5921457.96</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3ADFFE96-2DE3-412D-BF9E-AB4699C547A8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582821.2066 5921448.9067 582820.552 5921457.96 582819.954 5921457.662 
582818.3482 5921456.8633 582818.6159 5921448.8664 582789.9069 5921276.1807 
582791.6267 5921275.9162 582821.2066 5921448.9067 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="Gml_4232C0D7-B164-4371-B042-2C578259000D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582665.938 5920733.722</gml:lowerCorner>
          <gml:upperCorner>582693.055 5920776.111</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_BBBA8FCB-E82D-4E88-B063-4FA880DF6BE1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582688.038 5920753.183 582687.105 5920753.502 582693.055 5920771.482 
582679.078 5920776.111 582665.938 5920739.503 582681.412 5920733.722 
582688.038 5920753.183 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="Gml_C676CEBD-9AE3-4307-B14A-C5C074760AE4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582733.483 5920724.582</gml:lowerCorner>
          <gml:upperCorner>582751.481 5920744.768</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_00883F72-AB23-467B-909D-1BE83D99BCFD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582751.481 5920739.695 582740.399 5920744.768 582733.483 5920729.655 
582744.565 5920724.582 582751.481 5920739.695 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="Gml_C3D9B659-F00A-4D6A-8DAF-711787F9DED8">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582705.183 5920748.016</gml:lowerCorner>
          <gml:upperCorner>582713.36 5920756.349</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F07523D2-8F4E-458C-98D0-F7FB256C28BD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582713.36 5920753.395 582708.287 5920756.349 582705.183 5920750.901 
582710.325 5920748.016 582713.36 5920753.395 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_6E7FD6BC-CB53-4785-AE8C-3769A814FF4B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582655.7326 5920723.5767</gml:lowerCorner>
          <gml:upperCorner>582703.2635 5920786.2799</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_8C7CFEC6-6AC3-4360-B0E9-C03DCEB4187D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582698.4299 5920758.0836 582697.49 5920758.4864 582703.2635 5920776.6126 
582674.3959 5920786.2799 582655.7326 5920734.5867 582686.6143 5920723.5767 
582698.4299 5920758.0836 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_60650F3D-E6B3-4A83-B909-623BC489DF34">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582855.1075 5920566.391</gml:lowerCorner>
          <gml:upperCorner>582907.5457 5920628.1882</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_48A19C19-9384-4C26-B807-6B934084027D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582907.5457 5920612.7733 582880.8449 5920628.1882 582855.1075 5920581.393 
582881.5331 5920566.391 582907.5457 5920612.7733 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="Gml_C777D19A-BDFC-4C15-8339-6A65654C58F6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582936.133 5920463.0177</gml:lowerCorner>
          <gml:upperCorner>582968.453 5920503.201</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_424872C1-4FB5-4CE4-9CC5-E855AE1A106E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582950.207 5920464.081 582968.453 5920495.357 582955.01 5920503.201 
582936.133 5920470.871 582949.5864 5920463.0177 582950.207 5920464.081 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_0BB9D2D4-7432-43EF-A7CF-2B33081B1F85">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582925.3174 5920451.4478</gml:lowerCorner>
          <gml:upperCorner>582979.5959 5920514.3065</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_381E7063-C244-445F-B42F-8E57DE2F41D9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582962.3838 5920466.3853 582961.1733 5920467.0989 582979.5959 5920498.3513 
582951.7976 5920514.3065 582925.3174 5920468.107 582953.5785 5920451.4478 
582962.3838 5920466.3853 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="Gml_A44FD3DF-FA35-474D-8BC7-9CCD4E032F0F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583021.878 5920399.295</gml:lowerCorner>
          <gml:upperCorner>583041.962 5920425.822</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2CCF6858-38DB-4BBC-9891-9267730F0FF2" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583041.962 5920419.328 583031.479 5920425.822 583021.878 5920410.319 
583028.47 5920406.237 583025.664 5920401.705 583029.556 5920399.295 
583041.962 5920419.328 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_4404C3BB-114A-440E-806F-071545E41456">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583010.9175 5920388.2977</gml:lowerCorner>
          <gml:upperCorner>583052.9261 5920437.196</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_9BBF6B1D-2041-4121-8F0A-83A1206531F6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583052.9261 5920422.1181 583028.7548 5920437.196 583010.9175 5920407.8685 
583017.4937 5920403.8688 583014.5939 5920399.101 583032.3563 5920388.2977 
583052.9261 5920422.1181 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_847BC031-F811-43CF-97A3-5047EA8286EE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583054.3034 5920747.2476</gml:lowerCorner>
          <gml:upperCorner>583095.0964 5920793.466</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_7C989FDE-8000-436B-A90C-88B9C0E19480" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583095.0964 5920783.3405 583067.3129 5920793.466 583054.3034 5920757.3205 
583081.9425 5920747.2476 583095.0964 5920783.3405 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="Gml_0D40F9A5-70FA-4D1B-9405-A5295E478DED">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582996.128 5920842.688</gml:lowerCorner>
          <gml:upperCorner>583021.352 5920880.734</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F28F86D5-27EC-478C-AEA8-DE0957507C69" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583021.352 5920876.181 583007.111 5920880.734 582996.128 5920847.831 
583010.639 5920842.688 583021.352 5920876.181 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_Denkmalschutzrecht gml:id="Gml_2BA51C00-88CF-4E3A-A9B8-E54AC5D0E13E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582911.383 5920961.918</gml:lowerCorner>
          <gml:upperCorner>582926.868 5920984.003</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_210E7843-6582-4E5E-A6B4-759448622499" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582926.868 5920981.844 582916.487 5920984.003 582914.058 5920972.326 
582913.044 5920972.537 582911.383 5920964.569 582924.194 5920961.918 
582925.832 5920969.872 582924.438 5920970.165 582926.868 5920981.844 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1100</xplan:artDerFestlegung>
    </xplan:SO_Denkmalschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_AB977C79-545B-4CCF-B8EB-ECE3EE83A96B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582986.083 5920832.8898</gml:lowerCorner>
          <gml:upperCorner>583031.2215 5920891.0682</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_82F0BF3A-7761-4190-ACFC-19679A7AA71F" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583031.2215 5920881.539 583001.9651 5920891.0682 582986.083 5920843.0877 
583015.841 5920832.8898 583031.2215 5920881.539 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_E7C38BB7-E30C-4891-9E6F-9FE6EA95F566">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582848.1884 5920919.9597</gml:lowerCorner>
          <gml:upperCorner>582895.8864 5920970.0765</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_B4E8D603-E23E-4BA9-B8C8-18EDC80A96C3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582895.8864 5920955.5511 582865.7766 5920970.0765 582848.1884 5920932.8973 
582875.007 5920919.9597 582880.1567 5920930.6345 582883.166 5920929.1827 
582895.8864 5920955.5511 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_EECAF159-EF2B-4672-BE4E-A6D740370C11">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582784.8751 5920967.3493</gml:lowerCorner>
          <gml:upperCorner>582827.1411 5921018.0519</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:text>Archäologische Vorbehaltsfläche</xplan:text>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_45692A9C-E867-4352-B7B4-5A7BB26EFE14" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582827.1411 5921008.8874 582798.9638 5921018.0519 582784.8751 5920975.102 
582812.2845 5920967.3493 582816.7458 5920981.0659 582817.9635 5920980.6699 
582827.1411 5921008.8874 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>9999</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_9420EF39-C7CB-49A5-A360-5B036DF347A7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582761.3184 5921009.0741</gml:lowerCorner>
          <gml:upperCorner>582773.2972 5921074.7087</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6A1A5168-574B-4035-88FE-E5916A8CC41D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582766.1231 5921019.2259 582773.2972 5921074.6053 582772.4994 5921074.7087 
582770.761 5921062.5303 582767.0592 5921034.0158 582766.0255 5921027.3709 
582764.9581 5921020.5089 582761.3184 5921009.5999 582761.8565 5921009.4033 
582762.7575 5921009.0741 582766.1231 5921019.2259 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_A99ECD9B-C083-40D9-A656-0DCAC3D8BE3E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582790.4715 5921061.6502</gml:lowerCorner>
          <gml:upperCorner>582869.9136 5921272.8837</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_46F7CCB3-B2F6-4EB9-80B6-3E2FE7CFA14B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582869.9136 5921272.8837 582869.357 5921272.848 582845.382 5921270.747 
582814.3526 5921269.1375 582827.1163 5921268.0388 582805.9661 5921157.4846 
582790.4715 5921074.7432 582792.4852 5921074.2892 582793.4557 5921079.9216 
582829.5954 5921268.8135 582845.0901 5921269.3558 582845.6324 5921267.8064 
582833.4691 5921204.3558 582823.1652 5921150.6669 582813.4977 5921098.8072 
582809.5016 5921077.1984 582808.2945 5921070.6714 582811.3778 5921070.1897 
582812.5702 5921076.6776 582814.6539 5921088.0154 582825.8767 5921148.6526 
582836.2581 5921201.7217 582848.8088 5921270.208 582865.9304 5921270.9053 
582859.8875 5921236.972 582845.9423 5921162.8302 582835.3285 5921099.8445 
582830.3598 5921073.6585 582828.5287 5921062.0226 582830.3702 5921061.6502 
582837.8851 5921098.295 582848.189 5921162.4428 582869.9136 5921272.8837 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_1670ABBF-6E11-413F-845E-96C6DD5E6560">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582773.4256 5921079.7905</gml:lowerCorner>
          <gml:upperCorner>582855.514 5921477.987</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F4A420FB-B969-4F5D-A76A-E110559C5899" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582778.1088 5921101.9362 582787.8719 5921157.5782 582798.5082 5921214.4108 
582809.426 5921268.882 582830.741 5921381.091 582837.271 5921420.932 
582840.326 5921441.47 582843.262 5921452.327 582855.514 5921477.987 
582849.0812 5921467.3305 582842.0962 5921453.3605 582839.662 5921444.3647 
582835.6403 5921418.0121 582823.152 5921348.162 582812.5686 5921292.4935 
582802.8738 5921242.2715 582796.1269 5921209.1721 582785.8082 5921151.7044 
582773.4256 5921079.9493 582774.14 5921079.7905 582778.1088 5921101.9362 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_C41851F4-3FBB-48DC-AD78-E07660AB30D1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582768.925 5921376.8002</gml:lowerCorner>
          <gml:upperCorner>582780.0937 5921425.198</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_321DE7C3-2988-4FBC-9597-EC647EB6D6E7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582780.0937 5921425.0428 582776.9913 5921425.198 582768.925 5921377.1104 
582771.562 5921376.8002 582780.0937 5921425.0428 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_38DA9DD7-2BE2-437F-9160-7CF850B54C9D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582741.6236 5921095.7207</gml:lowerCorner>
          <gml:upperCorner>582800.4145 5921436.9872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_18613D61-1BA7-4031-9AF7-3FECD068FD66" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582753.2577 5921156.0629 582768.7698 5921255.3404 582800.4145 5921436.6769 
582798.5531 5921436.9872 582767.5289 5921257.5121 582750.7758 5921155.4424 
582741.6236 5921095.8759 582742.5544 5921095.7207 582753.2577 5921156.0629 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_266239ED-17A3-4396-8C72-54BF8E49B9EC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582724.8896 5921101.2899</gml:lowerCorner>
          <gml:upperCorner>582744.19 5921223.8645</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_11C187C3-2A48-4DFB-829D-924EB350D163" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582736.0598 5921170.2656 582744.19 5921223.8645 582742.3833 5921223.8645 
582731.3919 5921148.8453 582724.8896 5921101.4665 582726.1763 5921101.2899 
582736.0598 5921170.2656 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_13A00ED1-E9CB-4C89-A60F-AF76B2706544">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582825.339 5920955.705</gml:lowerCorner>
          <gml:upperCorner>582899.802 5921308.2654</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2FBCA5AE-2CCF-4746-93F0-97679C0485EE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582831.6023 5920966.9559 582832.2232 5920970.0965 582835.32 5920985.761 
582836.464 5920991.546 582837.9361 5920998.9901 582845.3972 5921039.6678 
582854.3547 5921094.7905 582862.3168 5921136.2856 582875.4085 5921206.1077 
582889.036 5921269.5754 582895.3905 5921306.0942 582899.5247 5921306.4005 
582899.802 5921307.3738 582894.3956 5921308.2654 582893.99 5921305.588 
582888.696 5921275.205 582888.4641 5921274.0727 582886.5286 5921273.9486 
582882.2988 5921252.6558 582866.7573 5921172.7279 582859.9435 5921136.2091 
582842.886 5921042.7284 582838.143 5921014.632 582833.088 5920992.444 
582831.777 5920986.69 582828.2504 5920971.2085 582825.339 5920958.428 
582827.14 5920957.132 582827.993 5920956.562 582829.378 5920955.705 
582831.6023 5920966.9559 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_739BF3E0-FBF5-4D1D-BCED-8C7D1AFEF27B">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582926.9079 5920894.7265</gml:lowerCorner>
          <gml:upperCorner>583017.7086 5921343.6257</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_ACD580DF-9495-4AF6-94AE-1E2E61F49543" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583017.7086 5921343.3352 583016.4548 5921343.6257 583006.731 5921293.264 
582995.8553 5921237.6242 582993.9858 5921238.1478 582983.092 5921178.007 
582971.7155 5921119.7774 582944.7337 5920981.061 582940.1086 5920954.1778 
582929.7168 5920907.9836 582926.9079 5920895.4974 582928.6871 5920894.9702 
582929.5095 5920894.7265 582934.2309 5920915.8284 582943.2743 5920952.573 
582948.0644 5920982.5111 582951.3576 5921001.6715 582975.1584 5921125.0166 
582994.6182 5921225.908 583017.7086 5921343.3352 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_241B55EA-483E-49C4-8739-6C2D72AD3039">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582925.2989 5920987.7825</gml:lowerCorner>
          <gml:upperCorner>582975.173 5921234.2424</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_3D9BCB9E-40EE-4F96-81F5-7B0E1A26B0E1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582975.173 5921233.7132 582972.3948 5921234.2424 582925.2989 5920987.9148 
582927.4156 5920987.7825 582975.173 5921233.7132 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_C83DEF03-89B1-498A-9BC3-0100806E43A5">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582901.9168 5920999.7312</gml:lowerCorner>
          <gml:upperCorner>582956.954 5921297.7056</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_D4580DA0-7FB9-4AED-B142-311154CB1109" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582955.3698 5921249.3188 582949.949 5921250.944 582956.954 5921297.447 
582955.4881 5921297.7056 582947.8716 5921249.9122 582930.6115 5921154.2548 
582910.8386 5921047.9426 582901.9168 5920999.9735 582903.1769 5920999.7312 
582921.7089 5921096.1154 582934.5177 5921164.4292 582950.506 5921247.8229 
582954.594 5921248.1862 582955.3698 5921249.3188 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_298508B3-18B3-4D85-ADE5-61590AFA97A2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582881.1391 5921021.0484</gml:lowerCorner>
          <gml:upperCorner>582934.5435 5921292.851</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_49FAC821-316B-4C22-A5DA-DB290A3CA6EE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582888.9036 5921051.8323 582908.7951 5921158.3624 582917.4147 5921204.5549 
582934.5435 5921292.2985 582931.4493 5921292.851 582886.535 5921052.0749 
582881.1391 5921021.4339 582883.0662 5921021.0484 582888.9036 5921051.8323 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_B01EA6A6-1580-4933-B961-BFA3791DCA29">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582870.3416 5921092.0227</gml:lowerCorner>
          <gml:upperCorner>582912.1458 5921295.1172</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_096D7DA1-B52F-4E56-8E37-2FFCBCFC1E3D" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582912.1458 5921294.6939 582909.0767 5921295.1172 582870.3416 5921092.3402 
582872.0349 5921092.0227 582912.1458 5921294.6939 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_2CE98EB0-E21D-44C4-A130-F420ADDDBD0F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582968.7864 5920856.251</gml:lowerCorner>
          <gml:upperCorner>583054.6906 5921336.3669</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_FA7F2EB0-6A72-4244-8F93-82DE498738B4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582988.4639 5920953.994 582982.7754 5920956.5076 582982.7754 5920963.5191 
582987.6959 5920989.7542 583019.9847 5921155.2856 583054.6906 5921334.7657 
583051.9814 5921335.3938 583047.7819 5921336.3669 583051.0474 5921331.8525 
583007.7231 5921107.6698 582979.9972 5920963.7837 582981.3202 5920933.4888 
582980.9233 5920928.4617 582977.3833 5920905.5494 582970.1821 5920864.6597 
582968.7864 5920856.7349 582970.953 5920856.358 582971.496 5920856.251 
582972.9749 5920864.0267 582973.932 5920869.059 582976.6433 5920884.3527 
582978.8239 5920896.6528 582983.651 5920923.881 582983.8562 5920926.7959 
582984.0361 5920929.3514 582984.2306 5920952.8034 583011.0859 5920941.8232 
583013.5865 5920939.6741 583012.3903 5920925.2597 583011.999 5920920.5447 
583013.7452 5920920.3066 583014.8932 5920924.8985 583015.4121 5920926.9741 
583016.9067 5920939.1774 583012.2765 5920943.6753 582988.4639 5920953.994 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_098DE625-DDCE-4758-AD4F-3141CCA1A22E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583030.4538 5920802.395</gml:lowerCorner>
          <gml:upperCorner>583127.76 5921318.2408</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_C38DF72E-43C0-49D4-811E-CF165A2CA4F6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583039.1683 5920843.7924 583046.3121 5920879.603 583052.7232 5920918.9855 
583063.4389 5920976.8686 583077.5434 5921051.1459 583117.7994 5921259.5849 
583116.633 5921259.836 583127.76 5921317.799 583125.8586 5921318.2408 
583088.6254 5921119.5615 583073.6967 5921042.2619 583053.0896 5920936.845 
583050.8915 5920928.9685 583049.4641 5920919.9097 583043.9661 5920880.2775 
583035.8594 5920833.7494 583030.4538 5920802.7246 583031.319 5920802.5738 
583031.2945 5920802.4332 583032.0245 5920802.395 583039.1683 5920843.7924 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_BB01FCB5-2F1B-4B64-A170-8B3A9D62828D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583039.9364 5920968.204</gml:lowerCorner>
          <gml:upperCorner>583108.6482 5921314.7975</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_042A3683-CEB9-4B53-B863-F26F7F656897" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583108.6482 5921314.5808 583106.6974 5921314.7975 583039.9364 5920968.4207 
583042.1039 5920968.204 583108.6482 5921314.5808 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_EF7DFEDE-8E9B-4E17-A809-50787F26F4C2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583015.8863 5920947.6147</gml:lowerCorner>
          <gml:upperCorner>583090.6917 5921322.5127</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7D50C66D-E705-4AB4-BE64-0E589AB442DA" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583035.9215 5921039.5153 583047.7902 5921099.4032 583090.6917 5921322.1861 
583088.7318 5921322.5127 583015.8863 5920948.3769 583018.8263 5920947.6147 
583035.9215 5921039.5153 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_9FCD43EB-B623-468E-8F9A-48588730BCBD">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582999.0088 5920955.7812</gml:lowerCorner>
          <gml:upperCorner>583071.5276 5921323.6016</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_92638A66-DD78-4A6A-9D87-AE36864577F0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583016.9752 5921039.9509 583071.5276 5921323.1661 583069.0232 5921323.6016 
583015.233 5921040.2775 582999.0088 5920956.1078 583001.4044 5920955.7812 
583016.9752 5921039.9509 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_9406ED89-6C48-41A9-B349-CC39AE5877DB">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583087.862 5920721.1896</gml:lowerCorner>
          <gml:upperCorner>583190.903 5921244.0559</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_B4E03A22-4BBD-4335-8539-D783F3FFB9A9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583090.7703 5920722.8962 583091.8233 5920723.055 583098.3769 5920749.3863 
583109.2605 5920813.1666 583121.6655 5920879.8727 583148.582 5921019.6042 
583155.4885 5921055.7749 583154.6745 5921055.9394 583178.551 5921181.086 
583190.903 5921243.847 583189.9326 5921244.0559 583136.8792 5920971.5057 
583106.9905 5920814.2712 583107.5232 5920814.1819 583103.0593 5920790.6603 
583096.7068 5920757.1862 583095.5064 5920750.8609 583095.258 5920749.552 
583094.3452 5920746.3515 583087.862 5920723.62 583089.8183 5920723.2766 
583090.255 5920723.2 583089.2845 5920721.8567 583089.0816 5920721.3169 
583089.8178 5920721.1896 583090.7703 5920722.8962 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_FF9248E4-3C00-4316-8F74-B79C7C4B7B38">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583089.2616 5920816.8838</gml:lowerCorner>
          <gml:upperCorner>583103.4874 5920889.6971</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6A464BAB-F860-4AB2-8D62-4FA2F2A549FD" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583094.9148 5920841.2107 583103.4874 5920889.5127 583101.6439 5920889.6971 
583092.2416 5920837.5236 583089.2616 5920817.2447 583091.4134 5920816.8838 
583094.9148 5920841.2107 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_F41A1ABC-DA78-4037-987A-1BDC1FFAB971">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583069.3489 5920810.7142</gml:lowerCorner>
          <gml:upperCorner>583086.1569 5920894.5165</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_88CC0A9C-B873-44CA-8D55-8B6769DE4CC7" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583080.2574 5920862.0137 583086.1569 5920894.1825 583084.5985 5920894.5165 
583075.2484 5920848.3225 583069.3489 5920810.922 583070.93 5920810.7142 
583072.4968 5920820.0565 583072.925 5920819.9847 583080.2574 5920862.0137 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_1A0BBFA2-505D-4E1C-A051-895B5B193B06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583048.3112 5920805.4678</gml:lowerCorner>
          <gml:upperCorner>583068.1245 5920899.9707</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_0E34BB16-D793-421F-8559-9D2119EE18D1" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583068.1245 5920899.6368 583066.0096 5920899.9707 583048.3112 5920805.6904 
583050.0921 5920805.4678 583068.1245 5920899.6368 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_2BB1683F-FEFB-4717-89A4-E4C798ECBD4D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583102.777 5920894.4747</gml:lowerCorner>
          <gml:upperCorner>583172.3289 5921241.1872</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_831C8F88-10A2-4391-BBA1-3E621F5D4AF4" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583172.3289 5921240.888 583169.7861 5921241.1872 583102.777 5920894.6243 
583104.5718 5920894.4747 583172.3289 5921240.888 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_329548F3-FCE5-4654-837C-1017B9DA124E">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583085.4264 5920898.5132</gml:lowerCorner>
          <gml:upperCorner>583157.7094 5921252.8025</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_18535D25-87A7-4E20-9B89-A093E830A2AB" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583087.0717 5920898.5132 583154.9783 5921249.2642 583157.7094 5921250.993 
583149.3042 5921252.8025 583151.3885 5921250.4608 583151.8372 5921247.918 
583117.1361 5921068.878 583085.4264 5920898.5132 583087.0717 5920898.5132 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_B1CA608C-0B38-42AF-93DA-1BDA347E2E09">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583066.7296 5920903.5987</gml:lowerCorner>
          <gml:upperCorner>583134.9354 5921250.4608</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_ECEC2742-C5C5-47A2-8DC4-C0C0C1DE7388" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583134.9354 5921250.0121 583132.243 5921250.4608 583066.7296 5920904.0475 
583068.9732 5920903.5987 583134.9354 5921250.0121 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_04BFBB16-7C42-4502-8FDA-E299497D7724">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583152.9525 5920744.6599</gml:lowerCorner>
          <gml:upperCorner>583210.2257 5921045.1237</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_6CF0A335-D455-4867-94E1-399C40FACEF8" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583161.8538 5920786.5892 583168.3603 5920824.9433 583175.4947 5920865.1238 
583185.2544 5920919.5159 583193.359 5920962.3218 583210.2257 5921044.7135 
583208.1957 5921045.1237 583179.0333 5920894.9167 583152.9525 5920745.3347 
583154.9354 5920744.6599 583161.8538 5920786.5892 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_AC0FC1B1-6569-4312-AF86-67B24D4224B7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583143.7517 5920799.2721</gml:lowerCorner>
          <gml:upperCorner>583188.3345 5921049.4609</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4F85B494-6118-4FA0-BC3B-2A516BB951A3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583145.1919 5920799.375 583161.446 5920895.6648 583188.3345 5921049.1373 
583186.7332 5921049.4609 583161.446 5920908.8326 583143.7517 5920799.2721 
583145.1919 5920799.375 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_F7E9CF25-352F-4359-B1F8-FAC9CBF80326">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>583169.4833 5920736.1241</gml:lowerCorner>
          <gml:upperCorner>583229.0088 5921033.0612</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_7F40E97A-9649-49E2-9A5B-0218EFBFCF21" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">583188.9584 5920827.1309 583203.3563 5920900.6412 583219.0723 5920980.8435 
583229.0088 5921032.757 583227.1838 5921033.0612 583170.0458 5920739.5177 
583169.4833 5920736.4882 583170.54 5920736.292 583171.4444 5920736.1241 
583188.9584 5920827.1309 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_SchutzgebietWasserrecht gml:id="Gml_1B094C04-F342-4AE5-BF96-5DB167E99EAE">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582699.84 5920842.4004</gml:lowerCorner>
          <gml:upperCorner>583267.115 5921477.987</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_E6A0008E-FA99-4180-8941-3F72E6DC418B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582830.741 5921381.091 582837.271 5921420.932 582840.326 5921441.47 
582843.262 5921452.327 582855.514 5921477.987 582844.732 5921471.354 
582838.561 5921467.936 582820.552 5921457.96 582819.954 5921457.662 
582818.3482 5921456.8633 582816.339 5921455.864 582813.623 5921454.351 
582801.232 5921447.453 582798.414 5921445.377 582787.998 5921439.878 
582780.876 5921436.12 582762.736 5921427.772 582755.795 5921392.755 
582750.198 5921363.295 582741.031 5921314.162 582734.092 5921274.782 
582730.279 5921255.158 582723.937 5921221.981 582716.643 5921182.659 
582714.201 5921167.851 582711.9594 5921151.4154 582711.0545 5921144.7809 
582710.816 5921143.032 582708.313 5921126.197 582706.178 5921113.368 
582703.5959 5921099.4476 582702.531 5921093.707 582699.84 5921081.135 
582700.0573 5921074.9916 582717.9531 5921050.4073 582744.5764 5921016.4114 
582751.1299 5921009.858 582782.5317 5920982.8251 582794.0002 5920973.8142 
582820.3504 5920953.7443 582900.6299 5920898.4497 582914.0098 5920926.0287 
582918.9249 5920924.6634 582931.6222 5920978.0466 582946.782 5920974.496 
583230.2907 5920842.4004 583234.0364 5920861.6923 583242.4986 5920905.2762 
583243.938 5920912.69 583252.7135 5920956.9542 583255.331 5920970.157 
583255.5682 5920971.3566 583255.757 5920972.311 583266.641 5921031.183 
583267.115 5921033.686 583230.027 5921040.712 583210.2257 5921044.7135 
583208.1957 5921045.1237 583188.3345 5921049.1373 583186.7332 5921049.4609 
583155.4885 5921055.7749 583154.6745 5921055.9394 583155.8018 5921061.8482 
583157.7003 5921071.7988 583173.1906 5921152.99 583174.8032 5921161.4423 
583178.551 5921181.086 583190.903 5921243.847 583189.9326 5921244.0559 
583157.7094 5921250.993 583149.3042 5921252.8025 583117.7994 5921259.5849 
583116.633 5921259.836 583127.76 5921317.799 583125.8586 5921318.2408 
583078.016 5921329.358 583054.6906 5921334.7657 583051.9814 5921335.3938 
583047.7819 5921336.3669 583017.7086 5921343.3352 583016.4548 5921343.6257 
583006.731 5921293.264 582995.8553 5921237.6242 582993.9858 5921238.1478 
582976.043 5921243.173 582973.404 5921243.912 582955.3698 5921249.3188 
582949.949 5921250.944 582956.954 5921297.447 582955.4881 5921297.7056 
582913.421 5921305.128 582899.802 5921307.3738 582894.3956 5921308.2654 
582893.99 5921305.588 582888.696 5921275.205 582888.4641 5921274.0727 
582886.5286 5921273.9486 582869.9136 5921272.8837 582869.357 5921272.848 
582845.382 5921270.747 582814.3526 5921269.1375 582809.426 5921268.882 
582830.741 5921381.091 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1000</xplan:artDerFestlegung>
      <xplan:zone>1200</xplan:zone>
    </xplan:SO_SchutzgebietWasserrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_6FF7FDDE-AB91-4E2A-A3B2-69FBAAF9A989">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582901.444 5920162.9427</gml:lowerCorner>
          <gml:upperCorner>582987.6573 5920376.7749</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_07AC5D68-E9ED-4FB6-99B1-1C5D10ABE7CE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582903.2796 5920165.7705 582972.3043 5920340.424 582979.9737 5920359.4232 
582985.4388 5920371.1041 582987.6573 5920375.9384 582987.0424 5920376.2205 
582985.8339 5920376.7749 582978.4921 5920360.2947 582958.5342 5920309.572 
582935.8997 5920252.2291 582936.8122 5920251.8147 582926.69 5920225.943 
582901.444 5920163.122 582901.8775 5920162.9427 582903.2796 5920165.7705 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_C1DD0303-38D1-4427-AFF3-A49C887BCCFF">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582954.8261 5920140.662</gml:lowerCorner>
          <gml:upperCorner>583041.95 5920365.7982</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_AA575080-C593-4E6E-A962-14C9608CBBB3" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582995.814 5920241.768 582997.852 5920246.911 583041.782 5920364.486 
583041.95 5920364.945 583041.1151 5920365.7982 583036.6901 5920353.5659 
583004.8362 5920269.0353 582954.8261 5920141.0486 582955.761 5920140.662 
582995.814 5920241.768 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_355A8E0D-C66D-4A1E-B66B-C3A5517CE449">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582919.4885 5920258.612</gml:lowerCorner>
          <gml:upperCorner>582990.2054 5920430.9092</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_17A45E98-0482-4259-AD25-40E42FF0A485" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582981.0904 5920405.9593 582990.2054 5920430.1048 582989.0718 5920430.5328 
582988.0749 5920430.9092 582919.4885 5920259.6822 582921.845 5920258.612 
582981.0904 5920405.9593 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_278FC9E7-1292-494F-9086-1E1C9907C9E0">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582851.4766 5920289.495</gml:lowerCorner>
          <gml:upperCorner>582922.648 5920461.894</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_292ACCDC-F5AB-40A9-84A8-AD646BFE3E9B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582888.0784 5920376.7772 582921.4591 5920457.472 582922.648 5920460.5188 
582919.1239 5920461.894 582917.935 5920458.8471 582887.4822 5920383.5349 
582851.4766 5920290.5698 582853.8431 5920289.495 582888.0784 5920376.7772 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_2050326C-F95C-4842-9CC6-9245790942B4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582868.9363 5920281.9112</gml:lowerCorner>
          <gml:upperCorner>582903.3811 5920366.9658</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_ED32CC38-DEAC-4ADA-BBFF-D1DF096D1F8E" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582903.3811 5920366.7278 582902.1906 5920366.9658 582868.9363 5920282.6405 
582870.542 5920281.9112 582903.3811 5920366.7278 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_9C53D967-10B0-4A4A-A98F-F9D1BFC6B710">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582885.7645 5920274.0487</gml:lowerCorner>
          <gml:upperCorner>582920.4041 5920359.5852</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_4BF3006F-0469-420A-8400-CE9EBA39531C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582920.4041 5920358.871 582918.4994 5920359.5852 582885.7645 5920274.998 
582887.8546 5920274.0487 582920.4041 5920358.871 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_DE2E2B52-0C2B-4F74-90EE-F8065E5A44A2">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582901.929 5920267.221</gml:lowerCorner>
          <gml:upperCorner>582936.5938 5920353.395</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_603F89BF-39A4-4DC3-9F4A-0274143FD33B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582936.5938 5920353.0379 582935.6415 5920353.395 582901.929 5920267.6569 
582902.8887 5920267.221 582936.5938 5920353.0379 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_45F948CB-9075-42A1-9FBF-D2C5B488DF36">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582920.8689 5920364.1093</gml:lowerCorner>
          <gml:upperCorner>582954.234 5920447.6947</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9AB03244-23CA-44A1-846F-8518A6D007D6" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582930.3619 5920382.9919 582937.5847 5920400.1205 582954.234 5920446.5459 
582951.1255 5920447.6947 582950.001 5920444.652 582932.2192 5920395.6835 
582920.8689 5920366.2761 582923.2422 5920364.1093 582930.3619 5920382.9919 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_CCB1BA79-3B07-4168-98E9-B8295DFC45E7">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582903.7404 5920371.848</gml:lowerCorner>
          <gml:upperCorner>582936.7593 5920448.3074</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_46FF16FD-B79B-4CDB-80ED-B91DB3FE2306" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582936.7593 5920447.2755 582933.9733 5920448.3074 582912.6142 5920395.4772 
582903.7404 5920372.364 582906.4232 5920371.848 582936.7593 5920447.2755 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_BF555C69-FFA9-46F8-9A7C-A05CD7996C3C">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582816.1167 5920305.7072</gml:lowerCorner>
          <gml:upperCorner>582906.4547 5920532.0417</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_92630AC8-8F9F-407D-9689-54F36B4F38CE" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582898.0192 5920508.5854 582906.4547 5920531.4157 582904.7604 5920532.0417 
582890.8803 5920495.9558 582889.6567 5920496.2821 582857.0273 5920410.0588 
582831.5763 5920346.676 582816.1167 5920306.6285 582817.1537 5920306.1575 
582818.1451 5920305.7072 582883.8435 5920472.0592 582898.0192 5920508.5854 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_D3D9693F-4C81-41F7-8542-188B40467DF4">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582871.3519 5920390.1295</gml:lowerCorner>
          <gml:upperCorner>582901.8052 5920465.636</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_986DD131-A932-4E29-A4A7-025FB89D5F1A" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582901.8052 5920465.1411 582900.537 5920465.636 582871.3519 5920390.386 
582872.378 5920390.1295 582901.8052 5920465.1411 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_A6C66B2A-49EA-4597-A02D-3026B761D3A6">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582779.3461 5920322.1608</gml:lowerCorner>
          <gml:upperCorner>582869.5472 5920550.7697</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_CE6CF026-38AE-4ACD-9C92-97B02F30381C" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582791.3894 5920348.1793 582837.0943 5920461.7738 582854.9654 5920512.3059 
582857.1017 5920519.0669 582869.5472 5920549.6429 582866.884 5920550.7697 
582854.1824 5920520.1425 582855.1293 5920519.7936 582854.6573 5920515.1817 
582821.6881 5920429.6263 582790.8759 5920354.6499 582779.3461 5920323.3278 
582781.9157 5920322.1608 582791.3894 5920348.1793 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_90CEF720-3FEB-47E2-98C3-F599A4D5CFF1">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582767.648 5920428.6442</gml:lowerCorner>
          <gml:upperCorner>582833.1012 5920589.24</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_10A28478-9EC0-4D91-96F5-B535B673978B" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582785.3794 5920470.6292 582786.255 5920473.2561 582833.1012 5920588.8392 
582831.932 5920589.24 582822.401 5920564.804 582811.83 5920538.762 
582785.648 5920474.258 582767.648 5920429.038 582769.1614 5920428.6442 
582785.3794 5920470.6292 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_BFCEA877-5FD3-421D-AF2A-7C6AC1A2260F">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582535.5666 5920436.0348</gml:lowerCorner>
          <gml:upperCorner>583169.6913 5921059.6653</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_2F7A4E8C-F5A3-49B4-8EB3-7487BD5C7F65" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582696.5527 5921055.6726 582697.4235 5921059.4156 582696.2496 5921059.6653 
582694.708 5921053.648 582691.226 5921039.05 582686.491 5921021.181 
582686.248 5921019.688 582686.004 5921018.197 582676.721 5920980.839 
582666.036 5920941.464 582665.182 5920938.87 582656.621 5920931.167 
582655.135 5920933.48 582652.232 5920934.846 582644.094 5920940.983 
582635.806 5920946.985 582631.529 5920952.542 582627.708 5920959.844 
582624.132 5920971.378 582615.203 5920982.16 582609.514 5920987.8 
582598.9013 5920994.4924 582594.7425 5920991.736 582592.9099 5920990.5214 
582591.806 5920989.7463 582589.762 5920988.311 582587.955 5920986.585 
582584.417 5920986.127 582578.841 5920986.65 582575.458 5920986.966 
582564.846 5920989.077 582554.287 5920992.388 582547.784 5920994.327 
582540.537 5920995.956 582536.819 5920996.027 582535.5666 5920993.7773 
582539.8212 5920994.1124 582543.4725 5920993.398 582549.4256 5920992.2868 
582555.22 5920990.5405 582561.1732 5920988.4768 582566.0944 5920987.1274 
582570.4601 5920986.4924 582576.0957 5920985.3811 582586.097 5920984.7461 
582590.2245 5920985.5399 582591.9883 5920987.5822 582592.135 5920987.5341 
582595.193 5920986.533 582599.91 5920983.91 582603.519 5920981.001 
582609.003 5920976.565 582611.34 5920973.684 582612.961 5920970.395 
582614.466 5920968.241 582615.5 5920968.606 582617.168 5920965.2198 
582617.56 5920964.424 582618.237 5920963.136 582617.058 5920962.574 
582612.779 5920957.535 582610.5164 5920952.781 582608.181 5920947.874 
582606.8971 5920945.1211 582603.663 5920938.187 582601.54 5920931.9 
582599.266 5920925.111 582601.816 5920923.676 582604.459 5920928.805 
582607.221 5920934.615 582611.622 5920944.62 582613.0492 5920947.8488 
582615.864 5920954.217 582616.618 5920955.477 582617.704 5920956.084 
582618.791 5920955.994 582620.292 5920954.042 582621.992 5920951.105 
582623.779 5920948.944 582626.508 5920944.64 582629.671 5920940.752 
582639.006 5920931.705 582644.705 5920927.475 582648.158 5920923.863 
582651.3535 5920920.8958 582651.826 5920920.457 582654.484 5920916.732 
582658.0303 5920912.2766 582660.69 5920908.935 582664.341 5920903.866 
582671.316 5920892.323 582671.6638 5920891.651 582673.177 5920888.727 
582676.427 5920882.186 582677.069 5920880.186 582677.6211 5920879.574 
582680.685 5920876.178 582684.666 5920872.511 582688.557 5920870.404 
582703.12 5920859.531 582708.9956 5920854.78 582712.143 5920852.235 
582716.022 5920847.653 582718.723 5920843.747 582721.2031 5920840.281 
582721.832 5920839.402 582724.8277 5920834.0921 582725.775 5920832.413 
582730.292 5920825.689 582732.986 5920822.971 582737.0028 5920817.7921 
582740.967 5920812.681 582747.962 5920803.895 582748.3522 5920803.2151 
582748.831 5920802.381 582751.143 5920800.459 582751.309 5920800.321 
582752.303 5920800.43 582756.777 5920798.869 582759.698 5920797.359 
582772.506 5920793.244 582779.703 5920789.976 582780.656 5920788.58 
582777.8782 5920780.0162 582777.772 5920779.689 582775.602 5920773.424 
582773.5969 5920769.1437 582772.513 5920766.83 582771.7655 5920765.026 
582771.7871 5920765.0143 582765.4667 5920750.5922 582763.6155 5920746.3633 
582766.2487 5920744.8887 582767.5126 5920746.7846 582768.2855 5920748.8987 
582771.0938 5920756.5801 582776.1495 5920766.4809 582778.1653 5920771.1931 
582778.487 5920771.841 582780.987 5920778.36 582781.0432 5920778.4983 
582785.36 5920789.128 582790.104 5920787.401 582794.459 5920784.996 
582794.335 5920783.904 582797.203 5920782.573 582802.928 5920780.716 
582815.976 5920776.016 582823.997 5920774.001 582829.623 5920772.678 
582851.7656 5920769.5101 582853.1944 5920769.3057 582854.024 5920769.187 
582866.892 5920767.7276 582873.096 5920767.024 582877.833 5920767.23 
582886.376 5920766.928 582893.623 5920766.212 582897.463 5920765.926 
582901.949 5920764.395 582907.17 5920762.177 582912.58 5920758.559 
582916.1206 5920755.8201 582918.48 5920753.995 582920.065 5920752.431 
582913.6421 5920742.8676 582910.0854 5920737.5719 582908.479 5920735.18 
582894.4671 5920714.6681 582883.212 5920698.192 582880.238 5920693.8609 
582870.4249 5920679.5697 582871.8541 5920678.1117 582877.2309 5920685.978 
582882.511 5920693.703 582884.752 5920696.991 582907.4898 5920730.2815 
582908.9157 5920732.3691 582909.3249 5920732.9681 582909.96 5920733.898 
582915.0689 5920741.4051 582921.5512 5920750.9302 582927.569 5920744.756 
582931.331 5920740.131 582937.097 5920734.134 582937.9843 5920732.9681 
582940.104 5920730.183 582944.171 5920726.85 582948.02 5920722.109 
582950.1859 5920720.6445 582952.8 5920718.877 582955.6611 5920715.7351 
582960.8863 5920709.9975 582961.955 5920708.824 582971.964 5920694.973 
582973.3269 5920692.2487 582974.839 5920689.226 582975.548 5920686.639 
582979.429 5920682.464 582982.889 5920681.002 582988.516 5920675.628 
582991.115 5920672.131 582993.9054 5920669.0214 583001.263 5920660.822 
583003.9032 5920658.0433 583015.729 5920645.597 583019.1668 5920641.7661 
583019.2178 5920641.7092 583023.2105 5920637.26 583025.077 5920635.18 
583027.247 5920632.6763 583036.537 5920621.958 583041.819 5920616.221 
583042.6208 5920615.0894 583042.9058 5920612.6554 583042.5041 5920610.3335 
583043.3772 5920609.0635 583045.2507 5920609.8583 583046.1429 5920608.7882 
583047.3313 5920608.4412 583047.87 5920607.681 583052.594 5920602.116 
583057.864 5920597.441 583063.654 5920594.578 583068.961 5920590.609 
583068.5392 5920588.9816 583067.5867 5920588.426 583068.7773 5920587.156 
583071.2379 5920588.1085 583073.131 5920588.195 583078.346 5920587.232 
583080.9217 5920586.6059 583083.69 5920585.933 583085.2843 5920585.8603 
583092.311 5920585.54 583098.296 5920585.736 583107.8456 5920584.9317 
583108.554 5920584.872 583109.0205 5920584.7483 583116.001 5920582.898 
583120.6736 5920580.3191 583123.33 5920578.853 583126.305 5920577.21 
583131.903 5920572.474 583133.5761 5920570.9005 583134.289 5920570.23 
583136.279 5920568.296 583141.699 5920564.618 583148.558 5920558.395 
583151.439 5920554.309 583152.3317 5920552.9219 583152.8356 5920550.0878 
583152.934 5920547.986 583149.972 5920545.552 583148.7361 5920543.7074 
583145.669 5920539.13 583145.0323 5920538.409 583138.3564 5920530.8502 
583135.9282 5920528.1008 583135.387 5920527.488 583129.1727 5920517.8884 
583119.9981 5920503.7157 583110.2908 5920488.7202 583108.17 5920485.444 
583106.1596 5920482.6982 583103.933 5920479.6572 583102.229 5920477.33 
583088.974 5920460.669 583087.2242 5920458.2136 583083.591 5920453.115 
583078.0339 5920444.1283 583078.0385 5920444.1239 583073.3306 5920436.5826 
583074.0803 5920436.0348 583080.998 5920446.868 583085.175 5920453.395 
583089.821 5920459.909 583104.063 5920478.028 583109.481 5920485.223 
583120.546 5920502.245 583136.98 5920526.743 583155.487 5920548.019 
583157.6674 5920551.7079 583160.41 5920556.348 583167.829 5920559.029 
583169.6913 5920560.2494 583168.4918 5920562.0922 583166.9644 5920561.6765 
583161.2494 5920560.1684 583156.5662 5920562.1528 583152.3593 5920566.2009 
583150.4436 5920567.8572 583144.7393 5920572.7891 583141.05 5920575.173 
583140.58 5920575.584 583140.102 5920575.985 583139.616 5920576.375 
583139.123 5920576.755 583138.558 5920577.173 583137.985 5920577.581 
583137.404 5920577.979 583137.1676 5920578.1374 583136.819 5920578.371 
583136.229 5920578.757 583135.635 5920579.139 583135.041 5920579.52 
583134.446 5920579.901 583133.85 5920580.284 583133.257 5920580.671 
583132.666 5920581.063 583132.08 5920581.461 583131.22 5920582.069 
583130.368 5920582.691 583129.522 5920583.322 583128.68 5920583.959 
583127.839 5920584.596 583126.997 5920585.227 583126.15 5920585.848 
583125.297 5920586.454 583124.433 5920587.039 583123.556 5920587.6 
583122.666 5920588.13 583121.756 5920588.625 583120.658 5920589.159 
583119.536 5920589.641 583118.389 5920590.076 583117.223 5920590.466 
583116.039 5920590.82 583112.1161 5920592.9503 583109.5414 5920593.4612 
583102.1149 5920594.9347 583090.4467 5920595.7285 583081.7948 5920596.1254 
583077.0323 5920596.2047 583068.1423 5920600.7291 583066.2039 5920602.3061 
583063.4592 5920604.5391 583052.9816 5920615.096 583046.711 5920622.2398 
583044.373 5920625.173 583046.8766 5920630.1667 583049.0379 5920634.4776 
583049.7601 5920635.918 583053.068 5920642.516 583053.5844 5920643.5123 
583057.788 5920651.623 583058.9278 5920653.8932 583065.7061 5920667.3937 
583068.5449 5920673.0479 583074.38 5920684.67 583075.278 5920687.506 
583069.2535 5920694.868 583068.8567 5920699.3131 583070.306 5920700.418 
583077.886 5920705.65 583081.5567 5920708.3618 583078.7786 5920710.505 
583075.591 5920708.187 583071.419 5920705.185 583065.2848 5920701.3768 
583065.666 5920700.049 583067.9181 5920694.3124 583067.932 5920694.277 
583072.568 5920688.19 583073.092 5920687.502 583065.7389 5920673.6972 
583054.8634 5920653.2791 583054.841 5920653.237 583046.5529 5920637.3349 
583042.337 5920629.246 583039.929 5920628.309 583035.0429 5920633.9873 
583031.9472 5920639.3848 583030.4391 5920641.9248 583029.7247 5920644.3061 
583026.391 5920646.7667 583019.8822 5920652.5611 583016.6278 5920656.0536 
583009.6428 5920663.1974 583005.607 5920666.987 583005.5258 5920667.0867 
583004.304 5920668.588 583002.6363 5920670.7274 583001.264 5920672.488 
583001.0089 5920672.815 582992.912 5920683.193 582992.965 5920686.795 
582993.1813 5920687.4816 582994.083 5920690.344 582996.0283 5920695.43 
582998.431 5920701.712 583000.6973 5920708.3512 583002.137 5920712.5687 
583002.984 5920715.05 583006.5929 5920725.3671 583009.8484 5920734.674 
583014.429 5920747.769 583019.297 5920759.325 583019.762 5920762.279 
583025.587 5920779.792 583024.985 5920780.005 583024.042 5920780.338 
583023.237 5920780.622 583019.895 5920772.545 583019.6887 5920772.0389 
583016.187 5920763.446 583009.9424 5920745.1125 583009.4792 5920743.7526 
583007.967 5920739.313 583003.5721 5920728.4332 583003.1962 5920727.5025 
582999.016 5920717.154 582998.9203 5920716.885 582994.153 5920703.484 
582994.097 5920703.3506 582994.0065 5920703.1353 582992.4826 5920699.507 
582990.624 5920695.082 582989.212 5920692.062 582988.561 5920690.782 
582987.597 5920690.569 582979.816 5920699.044 582974.0893 5920706.2187 
582967.283 5920714.746 582965.9722 5920716.13 582956.5931 5920726.0331 
582953.535 5920729.262 582949.0117 5920733.5255 582943.073 5920739.123 
582939.787 5920743.778 582929.9416 5920753.7644 582925.752 5920758.014 
582916.66 5920765.9 582913.254 5920768.1717 582909.859 5920770.436 
582902.983 5920774.619 582906.769 5920779.903 582909.3077 5920782.5561 
582911.1411 5920784.472 582914.3414 5920787.8164 582927.9273 5920802.0139 
582928.825 5920802.952 582929.8061 5920803.955 582942.063 5920816.485 
582945.7838 5920819.9901 582957.607 5920831.128 582964.2849 5920839.3101 
582964.688 5920839.804 582964.8752 5920840.5215 582964.2849 5920841.0254 
582962.0981 5920842.8921 582961.6208 5920843.2996 582961.113 5920842.8921 
582961.058 5920842.848 582960.427 5920842.049 582955.09 5920833.831 
582939.913 5920818.75 582938.2313 5920817.1058 582928.1882 5920807.2865 
582913.6478 5920793.07 582905.1476 5920784.7592 582897.636 5920777.415 
582893.652 5920777.398 582896.4368 5920787.3137 582900.0971 5920800.3471 
582901.772 5920806.3108 582903.3927 5920811.3008 582906.9675 5920822.3076 
582907.8045 5920824.8846 582914.0751 5920844.1727 582916.1776 5920850.8069 
582917.1716 5920853.7125 582918.0017 5920857.2403 582920.0149 5920864.9098 
582922.3595 5920873.8414 582919.9489 5920873.7797 582915.9007 5920857.349 
582909.7889 5920841.2358 582906.7055 5920830.9065 582903.4388 5920819.9633 
582900.3122 5920810.2543 582895.8982 5920796.5476 582893.2008 5920787.0931 
582890.222 5920776.652 582878.498 5920775.668 582871.0191 5920775.6028 
582868.403 5920775.58 582858.453 5920776.582 582858.03 5920776.6504 
582854.12 5920777.2829 582848.58 5920778.179 582838.667 5920779.48 
582828.847 5920781.474 582819.064 5920783.765 582811.5112 5920786.7845 
582805.174 5920789.318 582798.026 5920792.473 582797.1762 5920792.8778 
582790.434 5920796.09 582766.982 5920805.15 582768.687 5920809.101 
582769.8726 5920812.0152 582771.122 5920815.0861 582775.5625 5920826.0009 
582776.4681 5920828.2268 582781.225 5920839.919 582785.794 5920850.6021 
582787.9282 5920855.5922 582794.4051 5920870.7361 582794.9814 5920872.0837 
582796.975 5920876.745 582805.072 5920895.5179 582806.2748 5920898.3066 
582808.815 5920904.196 582813.091 5920914.5 582815.6787 5920921.0108 
582816.1097 5920922.0952 582817.069 5920924.509 582818.868 5920929.308 
582819.873 5920931.813 582823.86 5920943.569 582823.143 5920943.774 
582822.179 5920944.051 582821.542 5920944.233 582817.041 5920932.964 
582816.036 5920930.54 582814.087 5920925.48 582811.8063 5920919.8966 
582810.2133 5920915.9967 582810.059 5920915.619 582806.032 5920905.399 
582804.9798 5920902.9867 582804.655 5920902.2421 582794.014 5920877.846 
582787.9356 5920863.4609 582787.1195 5920861.5296 582784.8789 5920856.2272 
582783.8028 5920853.6804 582780.6408 5920846.1973 582778.462 5920841.041 
582774.464 5920831.7796 582767.8318 5920816.4162 582765.321 5920810.6 
582763.344 5920807.056 582762.368 5920807.482 582761.442 5920807.872 
582760.571 5920808.28 582759.669 5920808.739 582758.78 5920809.164 
582757.932 5920809.533 582754.0548 5920812.8195 582747.2285 5920819.6458 
582740.6404 5920827.5833 582737.8079 5920831.7796 582729.9247 5920843.4583 
582727.7727 5920846.8947 582725.0035 5920851.3165 582718.353 5920859.617 
582717.44 5920860.339 582716.582 5920861.05 582715.756 5920861.752 
582714.923 5920862.448 582697.6984 5920874.1765 582694.2685 5920877.1028 
582690.656 5920880.185 582688.4115 5920882.749 582688.3179 5920882.8614 
582682.4584 5920889.8928 582681.019 5920891.772 582680.807 5920893.06 
582680.463 5920894.297 582680.001 5920895.49 582679.438 5920896.643 
582678.789 5920897.762 582678.065 5920898.846 582677.277 5920899.893 
582676.438 5920900.908 582675.581 5920901.924 582674.756 5920902.974 
582673.955 5920904.039 582668.8059 5920912.7529 582663.4978 5920920.9973 
582661.3446 5920924.3416 582657.792 5920929.486 582660.781 5920932.163 
582664.7677 5920935.2054 582666.928 5920936.854 582669.887 5920939.113 
582674.063 5920941.972 582678.3825 5920944.7305 582686.1507 5920949.6913 
582691.0345 5920952.8102 582695.342 5920955.561 582698.1866 5920957.4411 
582728.5115 5920977.4839 582730.8446 5920979.0259 582736.644 5920982.859 
582738.3721 5920983.9846 582737.101 5920984.922 582752.4684 5920999.4884 
582752.055 5920999.937 582751.597 5921000.427 582745.542 5920995.532 
582733.921 5920985.106 582713.8116 5921001.653 582712.4622 5921000.3037 
582731.145 5920983.456 582705.8486 5920966.3311 582701.2337 5920963.2069 
582699.5728 5920962.0826 582693.845 5920958.205 582667.718 5920942.54 
582667.033 5920943.374 582671.39 5920953.546 582674.8117 5920966.3311 
582678.284 5920979.3053 582678.573 5920980.385 582683.312 5920999.93 
582684.9641 5921006.733 582687.567 5921017.451 582688.6998 5921023.2857 
582689.22 5921025.965 582691.2674 5921032.9564 582696.5527 5921055.6726 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_GewaesserFlaeche gml:id="Gml_0F7C092B-1C82-4EED-9B49-44C078B9AE74">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582736.3614 5920961.4438</gml:lowerCorner>
          <gml:upperCorner>582754.6293 5920977.4009</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_9A3427A4-3AF4-47CD-89F1-046A230B05F0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582754.6293 5920963.0569 582737.8002 5920977.4009 582736.3614 5920975.8314 
582753.5393 5920961.4438 582754.6293 5920963.0569 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>true</xplan:flaechenschluss>
      <xplan:zweckbestimmung>1100</xplan:zweckbestimmung>
    </xplan:BP_GewaesserFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:BP_KennzeichnungsFlaeche gml:id="Gml_5E0DA474-6775-4641-9879-F2D78FB27D06">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582504.1297 5920140.662</gml:lowerCorner>
          <gml:upperCorner>583267.115 5921477.987</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>1000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:rechtscharakter>5000</xplan:rechtscharakter>
      <xplan:position>
        <gml:Polygon gml:id="Gml_96A64C9B-AF4C-4897-90A8-FA611EA81BF0" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582830.741 5921381.091 582837.271 5921420.932 582840.326 5921441.47 
582843.262 5921452.327 582855.514 5921477.987 582844.732 5921471.354 
582838.561 5921467.936 582820.552 5921457.96 582819.954 5921457.662 
582816.339 5921455.864 582813.623 5921454.351 582801.232 5921447.453 
582798.414 5921445.377 582787.998 5921439.878 582780.876 5921436.12 
582762.736 5921427.772 582755.795 5921392.755 582750.198 5921363.295 
582741.031 5921314.162 582734.092 5921274.782 582730.279 5921255.158 
582723.937 5921221.981 582716.643 5921182.659 582714.201 5921167.851 
582710.816 5921143.032 582708.313 5921126.197 582706.178 5921113.368 
582702.531 5921093.707 582699.84 5921081.135 582700.126 5921073.048 
582699.564 5921071.821 582698.484 5921068.387 582694.708 5921053.648 
582691.226 5921039.05 582686.491 5921021.181 582686.248 5921019.688 
582686.004 5921018.197 582676.721 5920980.839 582666.036 5920941.464 
582665.182 5920938.87 582656.621 5920931.167 582655.135 5920933.48 
582652.232 5920934.846 582644.094 5920940.983 582635.806 5920946.985 
582631.529 5920952.542 582627.708 5920959.844 582624.132 5920971.378 
582615.203 5920982.16 582609.514 5920987.8 582598.9013 5920994.4924 
582594.7425 5920991.736 582592.9099 5920990.5214 582591.806 5920989.7463 
582589.762 5920988.311 582587.955 5920986.585 582584.417 5920986.127 
582578.841 5920986.65 582575.458 5920986.966 582564.846 5920989.077 
582554.287 5920992.388 582547.784 5920994.327 582540.537 5920995.956 
582536.819 5920996.027 582531.217 5920985.964 582523.038 5920975.41 
582510.836 5920961.679 582504.1297 5920953.3219 582516.846 5920921.204 
582518.869 5920916.096 582526.421 5920902.225 582527.141 5920902.581 
582530.539 5920898.147 582534.329 5920893.501 582538.204 5920888.931 
582542.184 5920884.447 582545.873 5920880.461 582546.937 5920879.313 
582557.738 5920869.946 582571.027 5920858.614 582603.81 5920830.584 
582607.003 5920827.854 582593.242 5920795.494 582580.859 5920766.377 
582590.463 5920762.037 582591.806 5920761.521 582595.304 5920760.179 
582593.784 5920757.303 582601.105 5920753.996 582606.638 5920750.438 
582601.501 5920735.991 582599.235 5920729.617 582598.5869 5920728.01 
582649.8483 5920707.3549 582652.0954 5920712.3774 582672.88 5920706.152 
582675.711 5920705.304 582677.365 5920710.188 582690.124 5920705.39 
582698.371 5920702.288 582705.539 5920699.976 582705.645 5920700.36 
582709.24 5920699.339 582708.949 5920698.381 582715.998 5920696.133 
582741.808 5920687.902 582747.9959 5920704.1842 582788.8143 5920686.7506 
582814.5564 5920675.7562 582827.3912 5920670.2744 582823.2849 5920662.241 
582854.7413 5920646.0293 582852.634 5920641.24 582833.67 5920593.696 
582822.401 5920564.804 582811.83 5920538.762 582785.648 5920474.258 
582767.648 5920429.038 582769.1614 5920428.6442 582733.849 5920342.8271 
582752.662 5920335.4463 582782.4258 5920321.9291 582817.1537 5920306.1575 
582834.9571 5920298.0721 582936.8122 5920251.8147 582926.69 5920225.943 
582901.444 5920163.122 582955.761 5920140.662 582995.814 5920241.768 
582997.852 5920246.911 583041.782 5920364.486 583041.95 5920364.945 
583042.258 5920365.789 583059.759 5920413.718 583064.217 5920420.589 
583080.998 5920446.868 583085.175 5920453.395 583089.821 5920459.909 
583104.063 5920478.028 583109.481 5920485.223 583120.546 5920502.245 
583136.98 5920526.743 583155.487 5920548.019 583157.6674 5920551.7079 
583160.41 5920556.348 583167.829 5920559.029 583176.133 5920564.471 
583179.422 5920566.912 583182.191 5920571.135 583188.57 5920611.997 
583190.278 5920622.931 583199.395 5920681.316 583202.118 5920696.869 
583211.448 5920745.352 583243.938 5920912.69 583255.331 5920970.157 
583255.757 5920972.311 583266.641 5921031.183 583267.115 5921033.686 
583230.027 5921040.712 583154.6745 5921055.9394 583178.551 5921181.086 
583190.903 5921243.847 583116.633 5921259.836 583127.76 5921317.799 
583078.016 5921329.358 583051.9814 5921335.3938 583016.4548 5921343.6257 
583006.731 5921293.264 582995.8553 5921237.6242 582976.043 5921243.173 
582973.404 5921243.912 582949.949 5921250.944 582956.954 5921297.447 
582913.421 5921305.128 582894.3956 5921308.2654 582893.99 5921305.588 
582888.696 5921275.205 582888.4641 5921274.0727 582869.357 5921272.848 
582845.382 5921270.747 582809.426 5921268.882 582830.741 5921381.091 
</gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:zweckbestimmung>7000</xplan:zweckbestimmung>
    </xplan:BP_KennzeichnungsFlaeche>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_SchutzgebietNaturschutzrecht gml:id="Gml_9F96CEB1-DADC-4BD9-AD55-D95CD6CF4EDC">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582590.7027 5920548.019</gml:lowerCorner>
          <gml:upperCorner>583160.41 5920994.4924</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_62DF4CC6-8560-40E6-A1E5-B5586DE4CCA5" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582624.132 5920971.378 582615.203 5920982.16 582609.514 5920987.8 
582598.9013 5920994.4924 582594.7425 5920991.736 582592.9099 5920990.5214 
582591.806 5920989.7463 582590.7027 5920988.9716 582592.135 5920987.5341 
582595.193 5920986.533 582599.91 5920983.91 582603.519 5920981.001 
582609.003 5920976.565 582611.34 5920973.684 582612.961 5920970.395 
582614.466 5920968.241 582615.5 5920968.606 582617.168 5920965.2198 
582617.56 5920964.424 582618.237 5920963.136 582620.292 5920954.042 
582621.992 5920951.105 582623.779 5920948.944 582626.508 5920944.64 
582629.671 5920940.752 582639.006 5920931.705 582644.705 5920927.475 
582648.158 5920923.863 582651.3535 5920920.8958 582651.826 5920920.457 
582654.484 5920916.732 582658.0303 5920912.2766 582660.69 5920908.935 
582664.341 5920903.866 582671.316 5920892.323 582671.6638 5920891.651 
582673.177 5920888.727 582676.427 5920882.186 582677.069 5920880.186 
582677.6211 5920879.574 582680.685 5920876.178 582684.666 5920872.511 
582688.557 5920870.404 582703.12 5920859.531 582708.9956 5920854.78 
582712.143 5920852.235 582716.022 5920847.653 582718.723 5920843.747 
582721.2031 5920840.281 582721.832 5920839.402 582722.3008 5920838.5711 
582724.8277 5920834.0921 582725.775 5920832.413 582730.292 5920825.689 
582732.986 5920822.971 582737.0028 5920817.7921 582740.967 5920812.681 
582747.962 5920803.895 582748.3522 5920803.2151 582748.831 5920802.381 
582751.143 5920800.459 582751.309 5920800.321 582752.303 5920800.43 
582756.777 5920798.869 582759.698 5920797.359 582772.506 5920793.244 
582777.2399 5920791.0944 582779.703 5920789.976 582794.335 5920783.904 
582797.203 5920782.573 582802.928 5920780.716 582815.976 5920776.016 
582823.997 5920774.001 582829.623 5920772.678 582851.7656 5920769.5101 
582853.1944 5920769.3057 582854.024 5920769.187 582866.892 5920767.7276 
582873.096 5920767.024 582877.833 5920767.23 582886.376 5920766.928 
582893.623 5920766.212 582897.463 5920765.926 582901.949 5920764.395 
582907.17 5920762.177 582912.58 5920758.559 582916.1206 5920755.8201 
582918.48 5920753.995 582920.065 5920752.431 582921.5512 5920750.9302 
582927.569 5920744.756 582931.331 5920740.131 582937.097 5920734.134 
582937.9843 5920732.9681 582940.104 5920730.183 582944.171 5920726.85 
582948.02 5920722.109 582950.1859 5920720.6445 582952.8 5920718.877 
582955.6611 5920715.7351 582960.8863 5920709.9975 582961.955 5920708.824 
582965.5468 5920703.8535 582971.964 5920694.973 582973.3269 5920692.2487 
582974.5234 5920689.8569 582974.839 5920689.226 582975.548 5920686.639 
582979.429 5920682.464 582982.889 5920681.002 582988.516 5920675.628 
582991.115 5920672.131 582993.9054 5920669.0214 583001.263 5920660.822 
583003.9032 5920658.0433 583015.729 5920645.597 583019.1668 5920641.7661 
583019.2178 5920641.7092 583023.2105 5920637.26 583025.077 5920635.18 
583027.247 5920632.6763 583036.537 5920621.958 583041.819 5920616.221 
583042.6208 5920615.0894 583047.3313 5920608.4412 583047.87 5920607.681 
583052.594 5920602.116 583057.864 5920597.441 583063.654 5920594.578 
583068.961 5920590.609 583073.131 5920588.195 583078.346 5920587.232 
583080.9217 5920586.6059 583083.69 5920585.933 583085.2843 5920585.8603 
583092.311 5920585.54 583098.296 5920585.736 583107.8456 5920584.9317 
583108.554 5920584.872 583109.0205 5920584.7483 583116.001 5920582.898 
583120.6736 5920580.3191 583123.33 5920578.853 583126.305 5920577.21 
583131.903 5920572.474 583133.5761 5920570.9005 583134.289 5920570.23 
583136.279 5920568.296 583141.699 5920564.618 583148.558 5920558.395 
583151.439 5920554.309 583152.3317 5920552.9219 583155.487 5920548.019 
583157.6674 5920551.7079 583160.41 5920556.348 583156.5662 5920562.1528 
583152.3593 5920566.2009 583150.4436 5920567.8572 583144.7393 5920572.7891 
583141.05 5920575.173 583133.257 5920580.671 583132.666 5920581.063 
583132.08 5920581.461 583131.22 5920582.069 583130.368 5920582.691 
583129.522 5920583.322 583128.68 5920583.959 583127.839 5920584.596 
583126.997 5920585.227 583126.15 5920585.848 583125.297 5920586.454 
583124.433 5920587.039 583123.556 5920587.6 583122.666 5920588.13 
583121.756 5920588.625 583120.658 5920589.159 583119.536 5920589.641 
583118.389 5920590.076 583117.223 5920590.466 583116.039 5920590.82 
583112.1161 5920592.9503 583109.5414 5920593.4612 583102.1149 5920594.9347 
583090.4467 5920595.7285 583081.7948 5920596.1254 583077.0323 5920596.2047 
583068.1423 5920600.7291 583066.2039 5920602.3061 583063.4592 5920604.5391 
583052.9816 5920615.096 583046.711 5920622.2398 583044.373 5920625.173 
583041.2613 5920628.8274 583034.6319 5920637.9503 583029.7247 5920644.3061 
583026.391 5920646.7667 583019.8822 5920652.5611 583016.6278 5920656.0536 
583009.6428 5920663.1974 583005.9803 5920666.6365 583005.607 5920666.987 
583005.5258 5920667.0867 583004.304 5920668.588 583002.6363 5920670.7274 
583001.264 5920672.488 583001.0089 5920672.815 582992.912 5920683.193 
582987.597 5920690.569 582979.816 5920699.044 582974.0893 5920706.2187 
582967.283 5920714.746 582965.9722 5920716.13 582956.5931 5920726.0331 
582953.535 5920729.262 582949.0117 5920733.5255 582943.073 5920739.123 
582939.787 5920743.778 582929.9416 5920753.7644 582925.752 5920758.014 
582916.66 5920765.9 582913.254 5920768.1717 582909.859 5920770.436 
582902.983 5920774.619 582897.636 5920777.415 582893.652 5920777.398 
582890.222 5920776.652 582878.498 5920775.668 582871.0191 5920775.6028 
582868.403 5920775.58 582858.453 5920776.582 582858.03 5920776.6504 
582854.12 5920777.2829 582848.58 5920778.179 582838.667 5920779.48 
582828.847 5920781.474 582819.064 5920783.765 582811.5112 5920786.7845 
582805.174 5920789.318 582798.026 5920792.473 582797.1762 5920792.8778 
582790.434 5920796.09 582766.982 5920805.15 582763.344 5920807.056 
582762.368 5920807.482 582761.442 5920807.872 582760.571 5920808.28 
582759.669 5920808.739 582758.78 5920809.164 582757.932 5920809.533 
582754.0548 5920812.8195 582747.2285 5920819.6458 582740.6404 5920827.5833 
582737.8079 5920831.7796 582729.9247 5920843.4583 582727.7727 5920846.8947 
582726.6307 5920848.7183 582725.0035 5920851.3165 582718.353 5920859.617 
582717.44 5920860.339 582716.582 5920861.05 582715.756 5920861.752 
582714.923 5920862.448 582697.6984 5920874.1765 582694.2685 5920877.1028 
582690.656 5920880.185 582688.4115 5920882.749 582688.3179 5920882.8614 
582682.4584 5920889.8928 582681.019 5920891.772 582680.807 5920893.06 
582680.463 5920894.297 582680.001 5920895.49 582679.438 5920896.643 
582679.1806 5920897.0868 582678.789 5920897.762 582678.065 5920898.846 
582677.277 5920899.893 582676.438 5920900.908 582675.581 5920901.924 
582674.756 5920902.974 582673.955 5920904.039 582668.8059 5920912.7529 
582663.4978 5920920.9973 582661.3446 5920924.3416 582657.792 5920929.486 
582656.621 5920931.167 582655.135 5920933.48 582652.232 5920934.846 
582644.094 5920940.983 582635.806 5920946.985 582631.529 5920952.542 
582627.708 5920959.844 582624.132 5920971.378 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1700</xplan:artDerFestlegung>
    </xplan:SO_SchutzgebietNaturschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:SO_SchutzgebietNaturschutzrecht gml:id="Gml_065B82F9-797E-47E1-901A-C0053BBA701D">
      <gml:boundedBy>
        <gml:Envelope srsName="EPSG:25832">
          <gml:lowerCorner>582510.836 5920420.589</gml:lowerCorner>
          <gml:upperCorner>583179.422 5921071.821</gml:upperCorner>
        </gml:Envelope>
      </gml:boundedBy>
      <xplan:rechtsstand>2000</xplan:rechtsstand>
      <xplan:ebene>0</xplan:ebene>
      <xplan:gehoertNachrichtlichZuBereich xlink:href="#Gml_CCD06AE8-2DD1-42AF-B4AE-06930B3939E4" />
      <xplan:position>
        <gml:Polygon gml:id="Gml_F790AB61-9AA8-4EFF-91AC-2076628B81A9" srsName="EPSG:25832">
          <gml:exterior>
            <gml:LinearRing>
              <gml:posList srsName="EPSG:25832">582824.759 5920948.583 582823.956 5920949.145 582814.3317 5920956.1329 
582807.291 5920961.245 582807.2267 5920961.2934 582806.492 5920961.846 
582801.261 5920965.7837 582791.717 5920972.968 582791.3908 5920973.259 
582788.663 5920975.693 582785.8396 5920978.0423 582777.27 5920985.173 
582775.2966 5920986.8188 582773.475 5920988.338 582754.522 5921004.145 
582748.617 5921009.203 582742.149 5921016.14 582736.782 5921022.88 
582729.133 5921032.496 582729.0978 5921032.5406 582722.875 5921040.422 
582722.106 5921041.3956 582717.306 5921047.473 582705.178 5921063.742 
582699.564 5921071.821 582698.484 5921068.387 582696.2496 5921059.6653 
582694.708 5921053.648 582691.226 5921039.05 582686.491 5921021.181 
582686.248 5921019.688 582686.004 5921018.197 582676.721 5920980.839 
582666.036 5920941.464 582665.182 5920938.87 582656.621 5920931.167 
582655.135 5920933.48 582652.232 5920934.846 582644.094 5920940.983 
582635.806 5920946.985 582631.529 5920952.542 582627.708 5920959.844 
582624.132 5920971.378 582615.203 5920982.16 582609.514 5920987.8 
582598.9013 5920994.4924 582594.7425 5920991.736 582592.9099 5920990.5214 
582591.806 5920989.7463 582590.7027 5920988.9716 582589.762 5920988.311 
582587.955 5920986.585 582584.417 5920986.127 582578.841 5920986.65 
582575.458 5920986.966 582564.846 5920989.077 582554.287 5920992.388 
582547.784 5920994.327 582540.537 5920995.956 582536.819 5920996.027 
582535.5666 5920993.7773 582531.217 5920985.964 582523.038 5920975.41 
582512.7786 5920963.8651 582510.836 5920961.679 582511.764 5920959.166 
582521.926 5920931.643 582524.034 5920926.732 582524.118 5920926.5497 
582527.396 5920919.435 582529.267 5920915.862 582531.305 5920912.45 
582535.71 5920905.683 582538.651 5920901.649 582545.6503 5920893.4694 
582552.129 5920885.898 582558.777 5920879.757 582565.091 5920874.277 
582570.0563 5920870.0067 582574.577 5920866.1187 582594.9101 5920848.6315 
582594.963 5920848.586 582600.7633 5920843.5985 582614.1269 5920832.1077 
582615.493 5920830.933 582628.78 5920820.112 582631.226 5920818.119 
582634.9026 5920815.5755 582638.955 5920812.772 582645.586 5920808.621 
582650.241 5920805.956 582665.1594 5920798.995 582678.449 5920792.794 
582685.992 5920789.173 582693.589 5920785.292 582702.558 5920780.378 
582711.475 5920775.151 582726.1071 5920766.3177 582729.499 5920764.27 
582729.279 5920763.88 582737.9858 5920758.6742 582760.02 5920745.4998 
582764.216 5920742.991 582770.4895 5920739.2403 582782.194 5920732.2424 
582789.6482 5920727.7857 582803.077 5920719.757 582820.282 5920709.471 
582828.465 5920704.578 582832.797 5920702.018 582837.033 5920699.304 
582841.118 5920696.483 582844.327 5920694.19 582845.255 5920693.527 
582848.0472 5920691.3755 582849.138 5920690.535 582851.5991 5920688.5399 
582853.162 5920687.273 582856.96 5920684.028 582857.964 5920683.107 
582860.695 5920680.601 582863.6698 5920677.7103 582864.3 5920677.098 
582867.3792 5920673.8742 582867.429 5920673.822 582870.1788 5920670.775 
582871.13 5920669.721 582874.409 5920665.792 582877.537 5920661.928 
582880.579 5920657.961 582880.9264 5920657.4793 582883.55 5920653.841 
582890.2149 5920644.5064 582897.3262 5920634.5465 582899.0582 5920632.1207 
582906.1786 5920622.1482 582909.8463 5920617.0113 582912.207 5920613.705 
582914.7164 5920610.2127 582914.893 5920609.967 582917.504 5920606.234 
582919.0795 5920603.9269 582920.101 5920602.431 582922.598 5920598.618 
582923.071 5920597.877 582925.052 5920594.776 582927.463 5920590.911 
582929.841 5920586.951 582931.5588 5920583.8579 582932.053 5920582.968 
582937.1196 5920573.6407 582941.2263 5920566.0806 582946.9319 5920555.5769 
582950.309 5920549.36 582951.8373 5920546.5844 582952.5932 5920545.2116 
582953.176 5920544.153 582956.167 5920538.939 582956.3058 5920538.7122 
582959.318 5920533.791 582962.548 5920528.779 582965.971 5920523.798 
582966.2363 5920523.4271 582969.449 5920518.936 582970.3438 5920517.7349 
582973.06 5920514.089 582976.787 5920509.376 582986.1578 5920498.0296 
582992.1727 5920490.7466 582992.1832 5920490.734 582995.395 5920486.845 
583000.4335 5920480.7425 583003.067 5920477.553 583014.965 5920463.411 
583022.11 5920455.356 583025.547 5920451.731 583029.48 5920447.584 
583028.836 5920446.818 583039.477 5920437.652 583043.095 5920435.176 
583061.495 5920422.579 583064.217 5920420.589 583074.0803 5920436.0348 
583080.998 5920446.868 583085.175 5920453.395 583089.821 5920459.909 
583096.7448 5920468.7176 583104.063 5920478.028 583109.481 5920485.223 
583109.8676 5920485.8178 583120.546 5920502.245 583126.3824 5920510.9452 
583126.996 5920511.8599 583136.98 5920526.743 583148.1453 5920539.5788 
583155.487 5920548.019 583157.6674 5920551.7079 583160.41 5920556.348 
583167.829 5920559.029 583168.464 5920559.4452 583169.6913 5920560.2494 
583176.133 5920564.471 583176.2087 5920564.5272 583179.422 5920566.912 
583141.516 5920609.969 583137.679 5920614.318 583130.291 5920624.031 
583125.997 5920630.34 583120.98 5920640.019 583118.1532 5920646.4865 
583114.1835 5920655.5691 583110.1316 5920664.8399 583108.149 5920669.376 
583108.0785 5920669.5148 583101.756 5920681.952 583101.2386 5920682.8478 
583099.3299 5920686.1527 583092.274 5920698.3698 583083.676 5920713.257 
583079.5 5920720.489 583077.9884 5920722.9606 583069.84 5920736.284 
583059.49 5920751.404 583055.0089 5920757.0634 583054.4603 5920757.7563 
583050.2945 5920763.0175 583046.621 5920767.657 583029.1111 5920787.6916 
583027.68 5920789.329 583022.1673 5920794.8072 583019.0386 5920797.9164 
583008.644 5920808.246 582998.8785 5920817.3989 582989.834 5920825.876 
582984.3234 5920830.513 582964.495 5920847.198 582949.8215 5920859.5477 
582945.236 5920863.407 582936.5551 5920869.9685 582930.3023 5920874.6947 
582927.206 5920877.035 582925.022 5920878.525 582924.5731 5920878.831 
582920.273 5920881.762 582901.7454 5920894.7217 582890.1134 5920902.8581 
582883.7132 5920907.3349 582865.288 5920920.223 582858.8662 5920924.7166 
582824.759 5920948.583 </gml:posList>
            </gml:LinearRing>
          </gml:exterior>
        </gml:Polygon>
      </xplan:position>
      <xplan:flaechenschluss>false</xplan:flaechenschluss>
      <xplan:artDerFestlegung>1300</xplan:artDerFestlegung>
    </xplan:SO_SchutzgebietNaturschutzrecht>
  </gml:featureMember>
  <gml:featureMember>
    <xplan:XP_RasterplanBasis gml:id="Gml_F4923FF6-A593-4F4F-8023-797D17967B53">
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan002_4-1Blatt1.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan002_4-1Blatt1.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
      <xplan:refScan>
        <xplan:XP_ExterneReferenz>
          <xplan:georefURL>BPlan002_4-1Blatt2.pgw</xplan:georefURL>
          <xplan:referenzURL>BPlan002_4-1Blatt2.png</xplan:referenzURL>
        </xplan:XP_ExterneReferenz>
      </xplan:refScan>
    </xplan:XP_RasterplanBasis>
  </gml:featureMember>
</xplan:XPlanAuszug>